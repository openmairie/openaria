<?php
/**
 * Ce scrip permet de configurer le listing 'Mes visites à réaliser'.
 *
 * L'objectif de ce listing est de présenter à l'utilisateur toutes les
 * visites du jour et à venir pour lesquelles il est le technicien.
 *
 * @package openaria
 * @version SVN : $Id$
 */

//
include "../sql/pgsql/dossier_instruction.inc.php";

//
require_once "../obj/pilotage.class.php";
$p = new pilotage();
$conf = $p->get_config_di_a_qualifier_affecter();

// Titre de la page
$ent = __("dossiers")." -> ".__("DI")." -> ".__("dossiers a affecter");
if (isset($idx) && $idx == '0') {
	$ent .= " -> ".__("affectation par lot");
}

// SELECT
// On enlève la colonne technicien inutile dans ce contexte
$champAffiche = array_diff(
    $champAffiche,
    $champaffiche_di_technicien
);

// Filtre du listing
$selection = " WHERE ".$conf["query_ct_where_a_affecter"];

// Pas de recherche avancée
$options = array();
$options[] = $option_condition_cloture;

// Lien vers l'interface d'affectation des dossiers par lot
$tab_actions['corner']['affecter-par-lot'] = array(
    "lib" => '<span class="om-icon om-icon-16 om-icon-fix affectation-16" title="'.__('Affecter par lot').'">'.__('Affecter par lot').'</span>',
    "lien" => OM_ROUTE_FORM."&obj=".$obj."&amp;action=8&amp;idx=0",
    "id" => "",
    "rights" => array('list' => array($obj, $obj.'_affecter_par_lot'), 'operator' => 'OR'),
);

// Filtre sur le service de l'utilisateur
include "../sql/pgsql/filter_service.inc.php";
