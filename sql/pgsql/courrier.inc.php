<?php
/**
 * @package openaria
 * @version SVN : $Id$
 */

include "../sql/pgsql/app_om_tab_common_select.inc.php";
include "../gen/sql/pgsql/courrier.inc.php";

// Fil d'ariane
$ent = __("suivi")." -> ".__("documents generes")." -> ".__("gestion");
if (!empty($retourformulaire)) {
    $ent = __("documents generes");
}
//
$tab_title = __("document généré");

//
$table .= " 
LEFT JOIN ".DB_PREFIXE."lien_courrier_contact
    ON lien_courrier_contact.courrier = courrier.courrier
    AND courrier.mailing IS FALSE
LEFT JOIN ".DB_PREFIXE."contact
    ON lien_courrier_contact.contact = contact.contact 
LEFT JOIN ".DB_PREFIXE."acteur
    ON acteur.acteur = dossier_instruction.technicien
 ";

// TRI
$tri = "
ORDER BY courrier.date_finalisation DESC NULLS LAST
";

/**
 * SELECT
 */
//
$displayed_fields_begin = array(
    'courrier.courrier as "'.__("courrier").'"',
    'courrier.code_barres as "'.__("code_barres").'"',
    'modele_edition.libelle as "'.__("modele_edition").'"',
);
$displayed_fields_end = array(
    "concat(contact.nom, ' ', contact.prenom, ' ', contact.raison_sociale, ' ',contact.denomination) as \"".__("destinataire")."\"",
    "case courrier.finalise when 't' then 'Oui' else 'Non' end as \"".__("finalise")."\"",
    'to_char(courrier.om_date_creation ,\'DD/MM/YYYY\') as "'.__("om_date_creation").'"',
    'to_char(courrier.date_finalisation ,\'DD/MM/YYYY\') as "'.__("date_finalisation").'"',
    "case courrier.mailing when 't' then 'Oui' else 'Non' end as \"".__("mailing")."\"",
);

//
$displayed_field_dc = 'dossier_coordination.libelle as "'.__("dc").'"';
$displayed_field_di = 'dossier_instruction.libelle as "'.__("di").'"';
$displayed_field_di_or_dc = "CASE WHEN dossier_instruction.libelle <> '' 
                    THEN  dossier_instruction.libelle ELSE dossier_coordination.libelle END as \"".__("di ou dc")."\"";
$displayed_field_technicien = 'acteur.acronyme as "'.__("technicien").'"';

// SELECT
$champAffiche = array_merge(
    $displayed_fields_begin,
    array(
        $select__etab_label__column__as,
        $displayed_field_di_or_dc,
        $displayed_field_technicien,
    ),
    $displayed_fields_end
);

// Dans le contexte de l'établissement, la colonne 'etablissement' est inutile
if (isset($retourformulaire)
    && in_array($retourformulaire, $foreign_keys_extended["etablissement"])) {
    //
    $champAffiche = array_diff(
        $champAffiche,
        array($select__etab_label__column__as, )
    );
}
// Dans le contexte du DC, les colonnes 'etablissement' et 'di_dc' sont inutiles
// mais les colonne 'di' et 'technicien' est utile
if (isset($retourformulaire)
    && in_array($retourformulaire, $foreign_keys_extended["dossier_coordination"])) {
    //
    $champAffiche = array_merge(
        $displayed_fields_begin,
        array(
            $displayed_field_di,
            $displayed_field_technicien,
        ),
        $displayed_fields_end
    );
}
// Dans le contexte du DI, les colonnes 'etablissement' , 'di_dc' et
// 'technicien' sont inutiles
if (isset($retourformulaire)
    && in_array($retourformulaire, $foreign_keys_extended["dossier_instruction"])) {
    //
    $champAffiche = array_diff(
        $champAffiche,
        array(
            $select__etab_label__column__as,
            $displayed_field_di_or_dc,
            $displayed_field_technicien,
        )
    );
}

// Champs de la recherche simple
$champRecherche = array(
    'courrier.code_barres as "'.__("code_barres").'"',
    'modele_edition.libelle as "'.__("modele_edition").'"',
    $select__etab_label__column__as,
    "CASE WHEN dossier_instruction.libelle <> '' 
        THEN  dossier_instruction.libelle
        ELSE dossier_coordination.libelle
    END as \"".__("di ou dc")."\"",
    'signataire.nom as "'.__("signataire").'"',
    'courrier_type.libelle as "'.__("courrier_type").'"',
    'proces_verbal.numero as "'.__("proces_verbal").'"',
    'courrier.arrete_numero as "'.__("arrete_numero").'"',
    'acteur.acronyme as "'.__("technicien").'"',
);

/**
 *
 */
// Si ce n'est pas un sous-formulaire
if (empty($retourformulaire)
    || in_array($retourformulaire, $foreign_keys_extended["courrier"])) {
    // Cache le bouton ajouter
    if (isset($tab_actions) === true
        && is_array($tab_actions) === true
        && array_key_exists("corner", $tab_actions) === true
        && is_array($tab_actions["corner"]) === true
        && array_key_exists("ajouter", $tab_actions["corner"]) === true) {
        //
        unset($tab_actions["corner"]["ajouter"]);
    }
}

// Cache les onglets
$sousformulaire = array();

// Si contexte DI
if (in_array($retourformulaire, $foreign_keys_extended["dossier_instruction"])) {

    // Instanciation du DI ou de sa surcharge
    if (isset($f)) {
        $di = $f->get_inst__om_dbform(array(
            "obj" => $retourformulaire,
            "idx" => $idxformulaire,
        ));
        // Si service DI différent de celui de l'utilisateur
        if ($di->is_from_good_service() === false) {
            // Désactivation du bouton ajout du soustab
            if (isset($tab_actions) === true
                && is_array($tab_actions) === true
                && array_key_exists("corner", $tab_actions) === true
                && is_array($tab_actions["corner"]) === true
                && array_key_exists("ajouter", $tab_actions["corner"]) === true) {
                //
                unset($tab_actions["corner"]["ajouter"]);
            }
        }
    }
}
