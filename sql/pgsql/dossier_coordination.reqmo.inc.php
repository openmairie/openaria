<?php
/**
 * @package openaria
 * @version SVN : $Id$
 */

//
$reqmo['libelle']=__('reqmo-libelle-dossier_coordination');
$reqmo['reqmo_libelle']=__('reqmo-libelle-dossier_coordination');
$ent=__('dossier_coordination');
$reqmo['sql']="select dossier_coordination, etablissement, libelle, dossier_coordination_type, date_demande, date_butoir, dossier_autorisation_ads, dossier_instruction_ads, a_qualifier, etablissement_type, etablissement_categorie, erp, dossier_cloture, contraintes_urba_om_html, etablissement_locaux_sommeil, references_cadastrales, dossier_coordination_parent, description, dossier_instruction_secu, dossier_instruction_acc, autorite_police_encours, date_cloture, geom_point, geom_emprise from ".DB_PREFIXE."dossier_coordination  order by [tri]";
$reqmo['tri']=array('dossier_coordination','etablissement','libelle','dossier_coordination_type','date_demande','date_butoir','dossier_autorisation_ads','dossier_instruction_ads','a_qualifier','etablissement_type','etablissement_categorie','erp','dossier_cloture','contraintes_urba_om_html','etablissement_locaux_sommeil','references_cadastrales','dossier_coordination_parent','description','dossier_instruction_secu','dossier_instruction_acc','autorite_police_encours','date_cloture','geom_point','geom_emprise');
