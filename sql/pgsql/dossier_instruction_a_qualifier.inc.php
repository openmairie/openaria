<?php
/**
 * Ce scrip permet de configurer le listing 'Mes visites à réaliser'.
 *
 * L'objectif de ce listing est de présenter à l'utilisateur toutes les
 * visites du jour et à venir pour lesquelles il est le technicien.
 *
 * @package openaria
 * @version SVN : $Id$
 */

//
include "../sql/pgsql/dossier_instruction.inc.php";

//
require_once "../obj/pilotage.class.php";
$p = new pilotage();
$conf = $p->get_config_di_a_qualifier_affecter();

// Titre de la page
$ent = __("dossiers")." -> ".__("DI")." -> ".__("dossiers a qualifier");

// SELECT
// On enlève la colonne a_qualifier inutile dans ce contexte
$champAffiche = array_diff(
    $champAffiche,
    $champaffiche_di_marqueur_a_qualifier
);

// Filtre du listing
$selection = " WHERE ".$conf["query_ct_where_a_qualifier"];

// Pas de recherche avancée
$options = array();
$options[] = $option_condition_cloture;

// Filtre sur le service de l'utilisateur
include "../sql/pgsql/filter_service.inc.php";
