<?php
/**
 *
 * @package openaria
 * @version SVN : $Id$
 */

//
include('../sql/pgsql/etablissement_tous.inc.php');

// Fil d'ariane
$ent = __("etablissements")." -> ".__("referentiel ERP");

// Affiche uniquement les établissements ERP et masque les établissements archivés le cas échéant
// On vérifie l'existance de f dans deux contextes car la carte SIG interne
// n'est pas dans le contexte de $f mais dans le contexte de $this->f
$etablissement_nature_erpr_code = "";
if (isset($f) === true) {
    $etablissement_nature_erpr_code = $f->getParameter("etablissement_nature_erpr");
} elseif (isset($this) === true && isset($this->f) === true) {
    $etablissement_nature_erpr_code = $this->f->getParameter("etablissement_nature_erpr");
}
if (isset($_GET['valide'])
    && $_GET['valide'] == 'false') {
    //
    $selection = "WHERE LOWER(etablissement_nature.code) = '".strtolower($etablissement_nature_erpr_code)."'";
} else {
    $selection=" WHERE
    (LOWER(etablissement_nature.code) = '".strtolower($etablissement_nature_erpr_code)."')
    AND 
    (((etablissement.om_validite_debut IS NULL AND (etablissement.om_validite_fin IS NULL OR etablissement.om_validite_fin > CURRENT_DATE)) OR (etablissement.om_validite_debut <= CURRENT_DATE AND (etablissement.om_validite_fin IS NULL OR etablissement.om_validite_fin > CURRENT_DATE))))";
}

// Dans le contexte "Référentiel ERP" on ne souhaite pas afficher la colonne nature
// On enlève la colonne en question et on renumérote les clés du tableau
$champAffiche = array_values(array_diff(
    $champAffiche,
     array($select__etab_nature_code__column__as, )
));

// Affichage du bouton de redirection vers le SIG externe si configuré
if (isset($f) === true
    && $f->getParameter('option_sig') == 'sig_externe') {
    $tmp_advsearch_options['export']['geoaria'] = array(
        'right' => 'etablissement_referentiel_erp_geoaria',
        'url' => OM_ROUTE_FORM.'&obj=etablissement_referentiel_erp&action=31&idx=0',
    );
}

unset($tmp_advsearch_options["advanced"]["etablissement_nature"]);
$options["advsearch"] = $tmp_advsearch_options;
