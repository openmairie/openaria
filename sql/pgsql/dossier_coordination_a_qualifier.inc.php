<?php
/**
 * Surcharge de la classe dossier_coordination pour afficher directement
 * seulement les dossiers de coordination à qualifier.
 * 
 * @package openaria
 * @version SVN : $Id$
 */

//
include "../sql/pgsql/dossier_coordination.inc.php";

// Fil d'Ariane
$ent = __("dossiers")." -> ".__("DC")." -> ".__("dossiers a qualifier");

//
$redlimit = 15;
if (isset($f) === true
    && $f->getParameter("dc_a_qualifier_redlimit")) {
    $redlimit = intval($f->getParameter("dc_a_qualifier_redlimit"));
}
//
$case_ddp = "case 
when dossier_coordination.date_demande < now() - interval '".intval($redlimit)." days'
    then 'rouge'
when dossier_coordination.depot_de_piece is TRUE
    then 'vert'
end";

// SELECT 
// On enlève la colonne affichant le marqueur à qualifier inutile dans ce contexte
$champAffiche = array_diff(
    $champAffiche,
    $champaffiche_dc_marqueur_a_qualifier
);
// On ajoute la colonne date de demande périmée afin de marquer les lignes périmées avec un style différent
$champAffiche[] = $case_ddp." as ddp";
// On renumérote les clés du tableau pour que la classe om_table retrouve
// ses éléments dans les options : tri, condition, ...
$champAffiche = array_values($champAffiche);

// Filtre du listing
$selection = " WHERE dossier_coordination.a_qualifier = TRUE";

// Pas de recherche avancée
$options = array();
// On affiche le champ lu en gras
$options[] = array(
    "type" => "condition",
    "field" => $case_ddp,
    "case" => array(
        "0" => array(
            "values" => array("rouge", ),
            "style" => "ligne-rouge",
        ),
        "1" => array(
            "values" => array("vert", ),
            "style" => "ligne-vert",
        ),
    ),
);
$options[] = $option_condition_cloture;

$tri = "ORDER BY dossier_coordination.date_demande ASC NULLS LAST, dossier_coordination.libelle ASC NULLS LAST";
