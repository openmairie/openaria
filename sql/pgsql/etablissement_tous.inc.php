<?php
/**
 *
 * @package openaria
 * @version SVN : $Id$
 */

//
include('../sql/pgsql/etablissement.inc.php');

// Fil d'ariane
$ent = __("etablissements")." -> ".__("etablissement_tous");

// Masquage de toutes les voies publiques et des établissements archivés le cas échéant

if (isset($_GET['valide']) and $_GET['valide'] == 'false'){

	$selection = "WHERE etablissement_nature.nature != 'Aménagement voie publique'";
}
else{
    
	$selection=" WHERE
	(etablissement_nature.nature != 'Aménagement voie publique')
	AND	
	(((etablissement.om_validite_debut IS NULL AND (etablissement.om_validite_fin IS NULL OR etablissement.om_validite_fin > CURRENT_DATE)) OR (etablissement.om_validite_debut <= CURRENT_DATE AND (etablissement.om_validite_fin IS NULL OR etablissement.om_validite_fin > CURRENT_DATE))))";
}

$case_validite = 'CASE WHEN ((etablissement.om_validite_debut IS NULL AND ';
$case_validite .= '(etablissement.om_validite_fin IS NULL OR etablissement.om_validite_fin > CURRENT_DATE))';
$case_validite .= ' OR ';
$case_validite .= '(etablissement.om_validite_debut <= CURRENT_DATE AND ';
$case_validite .= '(etablissement.om_validite_fin IS NULL OR etablissement.om_validite_fin > CURRENT_DATE)))';
$case_validite .= ' IS true THEN \''.__("Non").'\' ELSE \''.__("Oui").'\' END';

// On affiche le code de la nature de l'établissement dans le listing, au survol
// de la souris le code + le libellé s'affichent
$select__etab_nature_code__column__as = 'concat(\'<span title="\', etablissement_nature.code, \' - \', etablissement_nature.nature, \'">\', etablissement_nature.code, \'</span>\') as "'.__("etablissement_nature").'"';

// On affiche le code du statut juridique de l'établissement dans le listing, au
// survol de la souris le code + le libellé s'affichent
$select__etab_statut_juridique_code__column__as = 'CASE WHEN etablissement.etablissement_statut_juridique IS NULL THEN NULL ELSE concat(\'<span title="\', etablissement_statut_juridique.code, \' - \', etablissement_statut_juridique.libelle, \'">\', etablissement_statut_juridique.code, \'</span>\') END as "'.__("etablissement_statut_juridique").'"';

// SELECT
$champAffiche = array(
    'etablissement.etablissement as "'.__("etablissement").'"',
    $select__etab_code__column__as,
    'etablissement.libelle as "'.__("libelle").'"',
    'concat(etablissement.adresse_numero, 
        \' \', etablissement.adresse_numero2, \' \', voie.libelle,
        \' \', etablissement.adresse_cp, \' (\', arrondissement.libelle,\')\')
        as "'.__("adresse").'"',
    $select__etab_nature_code__column__as,
    $select__etab_statut_juridique_code__column__as,
    'etablissement_type.libelle as "'.__("etablissement_type").'"',
    'etablissement_categorie.libelle as "'.__("etablissement_categorie").'"',
    'to_char(etablissement.si_prochaine_visite_date,\'DD/MM/YYYY\') as "'.__("si_prochaine_visite_date").'"',
    'to_char(si_prochaine_visite_periodique_date_previsionnelle,\'DD/MM/YYYY\') as "'.__("si_prochaine_visite_periodique_date_previsionnelle").'"',
    // .lastcol !important
    $case_validite.' as "'.__("archive").'"',
);
//
$champRecherche = array(
    $select__etab_code__column__as,
    'etablissement.libelle as "'.__("libelle").'"',
);

//
$sousformulaire_parameters["piece"] = array(
    "title" => __("documents entrants"),
);

// Recherche avancée
if (!isset($options)) {
    $options = array();
}

// Affichage du bouton de redirection vers le SIG externe si configuré
if (isset($f) === true
    && $f->getParameter('option_sig') == 'sig_externe'
    && ($obj == 'etablissement_tous' || $obj == 'etablissement_referentiel_erp')) {
    //
    $tab_actions['left']["localiser-sig-externe"] = array(
                'lien' => OM_ROUTE_FORM.'&obj=etablissement&amp;action=30&amp;idx=',
                'id' => '',
                'lib' => '<span class="om-icon om-icon-16 om-icon-fix sig-16" title="'.__('Localiser').'">'.__('Localiser').'</span>',
                'rights' => array('list' => array('etablissement_tous', 'etablissement_tous_consulter'), 'operator' => 'OR'),
                'ordre' => 20,
                'target' => "blank",
                'ajax' => false);
}

if (isset($f) === true
    && $f->is_option_sig_interne_enabled() === true) {
    //
    $tab_actions['left']["localiser-sig-interne"] = array(
        'lien' => OM_ROUTE_FORM.'&obj='.$obj.'&amp;action=11&amp;idx=',
        'id' => '&amp;advs_id='.$advs_id.'&amp;premier='.$premier.'&amp;tricol='.$tricol.'&amp;valide='.$valide.'&amp;retour=tab',
        'lib' => '<span class="om-icon om-icon-16 om-icon-fix localiser-sig-interne-16" title="'.__('géolocaliser').'">'.__('géolocaliser').'</span>',
        'rights' => array('list' => array('etablissement_tous', 'etablissement_tous_consulter'), 'operator' => 'OR'),
        'ordre' => 20,
    );
}

// Options pour les select de faux booléens
$args = array(
    0 => array("", "t", "f", ),
    1 => array(__("choisir"), __("Oui"), __("Non"), ),
);

$champs['etablissement'] = array(
    'table' => 'etablissement',
    'colonne' => 'code',
    'type' => 'text',
    'taille' => 40,
    'max' => 100,
    'libelle' => __('code')
);
$champs['libelle'] = array(
    'table' => 'etablissement',
    'colonne' => 'libelle',
    'type' => 'text',
    'taille' => 40,
    'max' => 100,
    'libelle' => __('libelle')
);
$champs['adresse_numero'] = array(
    'table' => 'etablissement',
    'colonne' => 'adresse_numero',
    'type' => 'text',
    'taille' => 40,
    'max' => 100,
    'libelle' => __('numero')
);
$champs['adresse_voie'] = array(
    'table' => 'voie',
    'colonne' => 'libelle',
    'type' => 'text',
    'taille' => 40,
    'max' => 100,
    'libelle' => __('voie')
);
$champs['adresse_arrondissement'] = array(
    'table' => 'etablissement',
    'colonne' => 'adresse_arrondissement',
    'type' => 'select',
    'libelle' => __('arrondissement')
);
$champs['etablissement_nature'] = array(
    'table' => 'etablissement_nature',
    'colonne' => 'etablissement_nature',
    'type' => 'select',
    'libelle' => __('etablissement_nature')
);
$champs['etablissement_statut_juridique'] = array(
    'table' => 'etablissement_statut_juridique',
    'colonne' => 'etablissement_statut_juridique',
    'type' => 'select',
    'libelle' => __('etablissement_statut_juridique')
);
$champs['etablissement_type'] = array(
    'table' => 'etablissement_type',
    'colonne' => 'etablissement_type',
    'type' => 'select',
    'libelle' => __('etablissement_type')
);
$champs['etablissement_categorie'] = array(
    'table' => 'etablissement_categorie',
    'colonne' => 'etablissement_categorie',
    'type' => 'select',
    'libelle' => __('etablissement_categorie')
);
$champs['etablissement_etat'] = array(
    'table' => 'etablissement_etat',
    'colonne' => 'etablissement_etat',
    'type' => 'select',
    'libelle' => __('etablissement_etat')
);
$champs['ref_patrimoine'] = array(
    'table' => 'etablissement',
    'colonne' => 'ref_patrimoine',
    'type' => 'text',
    'taille' => 40,
    'max' => 100,
    'libelle' => __('ref_patrimoine')
);
$champs['siret'] = array(
    'table' => 'etablissement',
    'colonne' => 'siret',
    'type' => 'text',
    'taille' => 40,
    'max' => 100,
    'libelle' => __('siret')
);
$champs['nom'] = array(
    'table' => 'contact',
    'where' => 'injoin',
    'tablejoin' => 'INNER JOIN (
                SELECT DISTINCT etablissement 
                FROM '.DB_PREFIXE.'contact 
                WHERE lower(contact.nom) like %s ) 
            AS A1 
          ON A1.etablissement = etablissement.etablissement' ,
    'colonne' => 'nom',
    'type' => 'text',
    'taille' => 40,
    'max' => 100,
    'libelle' => __('contact')
);
// Affichage du champ de recherche avancée 'localisé' seulement lorsque le SIG
// externe est configuré
if (isset($f) === true
    && $f->getParameter('option_sig') == 'sig_externe'
    && ($obj == 'etablissement_tous' || $obj == 'etablissement_referentiel_erp')) {
    //
    $champs['geolocalise'] = array(
        'table' => 'etablissement',
        'colonne' => 'geolocalise',
        'type' => 'select',
        'libelle' => __('localise'),
        "subtype" => "manualselect",
        "args" => $args,
    );
}
$champs['si_autorite_competente_visite'] = array(
    'table' => 'autorite_competente12',
    'colonne' => 'autorite_competente',
    'type' => 'select',
    'libelle' => __('si_autorite_competente_visite'),
);
$champs['si_prochaine_visite_periodique_date_previsionnelle'] = array(
    'table' => 'etablissement',
    'colonne' => 'si_prochaine_visite_periodique_date_previsionnelle',
    'type' => 'date',
    'taille' => 8,
    'libelle' => __('si_prochaine_visite_periodique_date_previsionnelle'),
    'where' => 'intervaldate'
);

$tmp_advsearch_options = array(
    'type' => 'search',
    'display' => true,
    'advanced'  => $champs,
    'default_form'  => 'advanced',
    'export' => array('csv', ),
    'absolute_object' => 'etablissement',
);

if (isset($f) === true
    && $f->is_option_sig_interne_enabled() === true) {
    //
    $tmp_advsearch_options["export"][] = "sig";
}
// Affichage du bouton de redirection vers le SIG externe si configuré
// Le bouton est affiché seulement pour l'objet etablissement_tous
if (isset($f) === true
    && $f->getParameter('option_sig') == 'sig_externe'
    && $obj == 'etablissement_tous') {
    //
    $tmp_advsearch_options['export']['geoaria'] = array(
        'right' => 'etablissement_tous_geoaria',
        'url' => OM_ROUTE_FORM.'&obj=etablissement_tous&action=31&idx=0',
    );
}

$options["advsearch"] = $tmp_advsearch_options;

/**
 * Options
 */

// Code couleur ouvert/fermé
$options[] = array(
    "type" => "condition",
    "field" => "etablissement_statut.statut_administratif",
    "case" => array(
        "0" => array(
            "values" => array("Ouvert", ),
            "style" => "etablissement_ouvert",
        ),
        "1" => array(
            "values" => array("Fermé", ),
            "style" => "etablissement_ferme",
        ),
    ),
);
// Candidature interne
$options[] = array(
    "type" => "condition",
    "field" => $case_validite,
    "case" => array(
            array(
                "values" => array("Oui", ),
                "style" => "etablissement_archive",
                ),
            ),
    );
//
$options[] = array(
    "type" => "pagination_select",
    "display" => ""
);
