<?php
/**
 * Surcharge 'autorite_police'.
 *
 * Listing des autorités de police.
 *
 * L'objectif de la surcharge est :
 * - la suppression de l'action 'ajouter'
 * - le renommage de l'onglet principal
 *
 * @package openaria
 * @version SVN : $Id$
 */

include "../sql/pgsql/app_om_tab_common_select.inc.php";
include "../gen/sql/pgsql/autorite_police.inc.php";

// Fil d'Ariane
$ent = __("dossiers")." -> ".__("autorites de police")." -> ".__("toutes les AP");

// Suppression de l'action 'ajouter'
if (isset($tab_actions) === true
    && is_array($tab_actions) === true
    && array_key_exists("corner", $tab_actions) === true
    && is_array($tab_actions["corner"]) === true
    && array_key_exists("ajouter", $tab_actions["corner"]) === true) {
    //
    unset($tab_actions["corner"]["ajouter"]);
}

// Renommage de l'onglet principal
$tab_title = __("AP");

// Désactivation des sous-formulaires
$sousformulaire = array();

// Définition des variables pour la customisation du listing
$displayed_field_di_or_dc = "CASE WHEN dossier_instruction.libelle <> '' THEN dossier_instruction.libelle ELSE dossier_coordination.libelle END as \"".__("di ou dc")."\"";

// FROM
$table = DB_PREFIXE."autorite_police
    LEFT JOIN ".DB_PREFIXE."autorite_police_decision 
        ON autorite_police.autorite_police_decision=autorite_police_decision.autorite_police_decision 
    LEFT JOIN ".DB_PREFIXE."autorite_police_motif 
        ON autorite_police.autorite_police_motif=autorite_police_motif.autorite_police_motif 
    LEFT JOIN ".DB_PREFIXE."dossier_coordination 
        ON autorite_police.dossier_coordination=dossier_coordination.dossier_coordination 
    LEFT JOIN ".DB_PREFIXE."dossier_instruction_reunion as dossier_instruction_reunion_prise_decision
        ON autorite_police.dossier_instruction_reunion=dossier_instruction_reunion_prise_decision.dossier_instruction_reunion 
    LEFT JOIN ".DB_PREFIXE."dossier_instruction_reunion as dossier_instruction_reunion_prochain
        ON autorite_police.dossier_instruction_reunion_prochain=dossier_instruction_reunion_prochain.dossier_instruction_reunion 
    LEFT JOIN ".DB_PREFIXE."etablissement 
        ON autorite_police.etablissement=etablissement.etablissement 
    LEFT JOIN ".DB_PREFIXE."service 
        ON autorite_police.service=service.service
    LEFT JOIN ".DB_PREFIXE."reunion as reunion_prise_decision
        ON dossier_instruction_reunion_prise_decision.reunion = reunion_prise_decision.reunion
    LEFT JOIN ".DB_PREFIXE."dossier_instruction
        ON dossier_instruction_reunion_prise_decision.dossier_instruction = dossier_instruction.dossier_instruction
    LEFT JOIN ".DB_PREFIXE."reunion as reunion_prochain
        ON dossier_instruction_reunion_prochain.reunion = reunion_prochain.reunion
";

// SELECT 
$champAffiche = array(
    'autorite_police.autorite_police as "'.__("id").'"',
    'to_char(autorite_police.date_decision ,\'DD/MM/YYYY\') as "'.__("date_decision").'"',
    $select__etab_label__column__as,
    $displayed_field_di_or_dc,
    'autorite_police_decision.libelle as "'.__("autorite_police_decision").'"',
    'autorite_police.delai as "'.__("delai").'"',
    'autorite_police_motif.libelle as "'.__("autorite_police_motif").'"',
    'to_char(autorite_police.date_notification ,\'DD/MM/YYYY\') as "'.__("date_notification").'"',
    'to_char(autorite_police.date_butoir ,\'DD/MM/YYYY\') as "'.__("date_butoir").'"',
    'CASE WHEN reunion_prise_decision.reunion IS NOT NULL THEN concat(
            \''.__("Demande de passage n°").'\',
            dossier_instruction_reunion_prise_decision.dossier_instruction_reunion,
            \' '.__("planifiée").' (\',
            reunion_prise_decision.code,
            \')\'
        )
        WHEN dossier_instruction_reunion_prise_decision.dossier_instruction_reunion IS NOT NULL THEN CONCAT(
            \''.__("Demande de passage n°").'\',
            dossier_instruction_reunion_prise_decision.dossier_instruction_reunion,
            \' '.__("souhaitée le").' \',
            to_char(dossier_instruction_reunion_prise_decision.date_souhaitee, \'DD/MM/YYYY\')
        )
        ELSE \'\'
    END as "'.__("demande passage en reunion").'"',
    'CASE WHEN reunion_prochain.reunion IS NOT NULL THEN concat(
            \''.__("Demande de passage n°").'\',
            dossier_instruction_reunion_prochain.dossier_instruction_reunion,
            \' '.__("planifiée").' (\',
            reunion_prochain.code,
            \')\'
        )
        WHEN dossier_instruction_reunion_prochain.dossier_instruction_reunion IS NOT NULL THEN CONCAT(
            \''.__("Demande de passage n°").'\',
            dossier_instruction_reunion_prochain.dossier_instruction_reunion,
            \' '.__("souhaitée le").' \',
            to_char(dossier_instruction_reunion_prochain.date_souhaitee, \'DD/MM/YYYY\')
        )
        ELSE \'\'
    END as "'.__("dossier_instruction_reunion_prochain").'"',
    "case autorite_police.cloture when 't' then 'Oui' else 'Non' end as \"".__("cloture")."\"",
    'service.libelle as "'.__("service").'"',
);

//
$champRecherche = array(
    'autorite_police.autorite_police as "'.__("id").'"',
    $select__etab_label__column__as,
    $displayed_field_di_or_dc,
    'autorite_police_decision.libelle as "'.__("autorite_police_decision").'"',
    'autorite_police.delai as "'.__("delai").'"',
    'autorite_police_motif.libelle as "'.__("autorite_police_motif").'"',
    'dossier_instruction_reunion_prise_decision.dossier_instruction as "'.__("dossier_instruction_reunion").'"',
    'dossier_instruction_reunion_prochain.dossier_instruction as "'.__("dossier_instruction_reunion_prochain").'"',
    'service.libelle as "'.__("service").'"',
);

// Dans le contexte du DC, les colonnes 'etablissement' et 'dc' sont inutiles
if (isset($retourformulaire)
    && in_array($retourformulaire, $foreign_keys_extended["dossier_coordination"])) {
    $champAffiche = array_diff(
        $champAffiche,
        array($select__etab_label__column__as, $displayed_field_di_or_dc, )
    );
    $champRecherche = array_diff(
        $champRecherche,
        array($select__etab_label__column__as, $displayed_field_di_or_dc, )
    );
}
// Dans le contexte de l'établissement, la colonne 'etablissement' est inutile
if (isset($retourformulaire)
    && in_array($retourformulaire, $foreign_keys_extended["etablissement"])) {
    $champAffiche = array_diff(
        $champAffiche,
        array($select__etab_label__column__as, )
    );
    $champRecherche = array_diff(
        $champRecherche,
        array($select__etab_label__column__as, )
    );
}

// TRI
$tri = " ORDER BY autorite_police.date_decision ASC, autorite_police.date_notification ASC NULLS LAST, autorite_police.date_butoir ASC
NULLS LAST, etablissement.code ASC ";

// Filtre sur le service de l'utilisateur
include "../sql/pgsql/filter_service.inc.php";
