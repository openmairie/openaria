<?php
//$Id$ 
//gen openMairie le 10/08/2016 17:34

include "../gen/sql/pgsql/contrainte.inc.php";

// SELECT
$champAffiche = array(
    'contrainte.contrainte as "'.__("contrainte").'"',
    'contrainte.libelle as "'.__("libelle").'"',
    'contrainte.nature as "'.__("nature").'"',
    'contrainte.groupe as "'.__("groupe").'"',
    'contrainte.sousgroupe as "'.__("sousgroupe").'"',
    "case contrainte.lie_a_un_referentiel when 't' then 'Oui' else 'Non' end as \"".__("lie_a_un_referentiel")."\"",
    'to_char(contrainte.om_validite_debut ,\'DD/MM/YYYY\') as "'.__("om_validite_debut").'"',
    'to_char(contrainte.om_validite_fin ,\'DD/MM/YYYY\') as "'.__("om_validite_fin").'"',
);

// Supprime les sous-formulaires
$sousformulaire = array();
