<?php
/**
 * Définition des variables communes permettant de définir des éléments de la
 * clause SELECT des requêtes des listings.
 *
 * @package openaria
 * @version SVN : $Id$
 */

// Colonne représentant le code de l'établissement : elle se compose du code
// de l'établissement auquel on ajoute un id html pour obtenir un tri
// plus naturel pour l'utilisateur sur cette colonne (T1 > T2 > T10 et non pas
// T1 > T10 > T2).
$select__etab_code__column = sprintf(
    'CONCAT(
        \'%1$s\',
        etablissement.code,
        \'%2$s\'
    )',
    (isset($f) === true ? sprintf(
        '<span id="%1$s\', LPAD(replace(etablissement.code, \'%1$s\', \'\'), %2$s, \'0\'), \'">',
        $f->getParameter("etablissement_code_prefixe"),
        25 - (strlen($f->getParameter("etablissement_code_prefixe")))
    ) : ""),
    (isset($f) === true ? sprintf('</span>') : "")
);
$select__etab_code__column__as = $select__etab_code__column." as \""._("code")."\"";

// Colonne représentant le label de l'établissement : elle se compose du code
// et du libellé de l'établissement. On ajoute un id html pour obtenir un tri
// plus naturel pour l'utilisateur sur cette colonne (T1 > T2 > T10 et non pas
// T1 > T10 > T2).
$select__etab_label__column = sprintf(
    'CONCAT(
        \'%1$s\',
        etablissement.code,
        \' - \',
        etablissement.libelle,
        \'%2$s\'
    )',
    (isset($f) === true ? sprintf(
        '<span id="%1$s\', LPAD(replace(etablissement.code, \'%1$s\', \'\'), %2$s, \'0\'), \'">',
        $f->getParameter("etablissement_code_prefixe"),
        25 - (strlen($f->getParameter("etablissement_code_prefixe")))
    ) : ""),
    (isset($f) === true ? sprintf('</span>') : "")
);
$select__etab_label__column__as = $select__etab_label__column." as \""._("etablissement")."\"";

