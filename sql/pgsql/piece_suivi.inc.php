<?php
/**
 * Surcharge de la classe piece pour afficher seulement les pièces qui sont
 * suivi et dont la date butoir est dépassée.
 *
 * @package openaria
 * @version SVN : $Id$
 */

include('../sql/pgsql/piece.inc.php');

// Fil d'ariane
$ent = __("suivi")." -> ".__("documents entrants")." -> ".__("suivi");

// SELECT
$champAffiche = array(
    'piece.piece as "'.__("id").'"',
    'piece.nom as "'.__("nom").'"',
    'piece_type.libelle as "'.__("piece_type").'"',
    'to_char(piece.date_reception ,\'DD/MM/YYYY\') as "'.__("date_reception").'"',
    'to_char(piece.date_emission ,\'DD/MM/YYYY\') as "'.__("date_emission").'"',
    $select__etab_label__column__as,
    $select__di_or_dc_label__column__as,
    'piece_statut.libelle as "'.__("piece_statut").'"',
    $select__docent_lu__column__as,
    'to_char(piece.date_butoir ,\'DD/MM/YYYY\') as "'.__("date_butoir").'"',
    'to_char(piece.om_date_creation ,\'DD/MM/YYYY\') as "'.__("om_date_creation").'"',
);

// Cache le bouton ajouter
if (isset($tab_actions) === true
    && is_array($tab_actions) === true
    && array_key_exists("corner", $tab_actions) === true
    && is_array($tab_actions["corner"]) === true
    && array_key_exists("ajouter", $tab_actions["corner"]) === true) {
    //
    unset($tab_actions["corner"]["ajouter"]);
}

//
$selection = " WHERE piece.suivi = true
    AND (piece.date_butoir <= NOW() OR piece.date_butoir is NULL)";

//
$tri = " ORDER BY piece.date_butoir ";

// Pas de recherche avancée sur ce listing
if (isset($options) === true
    && is_array($options) === true
    && array_key_exists("advsearch", $options) === true) {
    unset($options["advsearch"]);
}

// Filtre sur le service de l'utilisateur
include "../sql/pgsql/filter_service.inc.php";
