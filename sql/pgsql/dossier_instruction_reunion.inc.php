<?php
/**
 * Script de paramétrage du listing des 'passages en réunion'.
 *
 * @package openaria
 * @version SVN : $Id$
 */

include "../sql/pgsql/app_om_tab_common_select.inc.php";
include "../gen/sql/pgsql/dossier_instruction_reunion.inc.php";

//
$ent = "-> ".__("passage en reunion");

$table .= "
LEFT JOIN ".DB_PREFIXE."dossier_coordination
    ON dossier_instruction.dossier_coordination=dossier_coordination.dossier_coordination 
LEFT JOIN ".DB_PREFIXE."etablissement
    ON dossier_coordination.etablissement=etablissement.etablissement ";


$displayed_fields_begin = array(

);
$displayed_field_di = 'dossier_instruction.libelle as "'.__("di").'"';

// SELECT 
$champAffiche = array(
    'dossier_instruction_reunion.dossier_instruction_reunion as "'.__("dossier_instruction_reunion").'"',
    'to_char(dossier_instruction_reunion.date_souhaitee ,\'DD/MM/YYYY\') as "'.__("date_souhaitee").'"',
    'reunion_type.libelle as "'.__("reunion_type").'"',
    'reunion_categorie.libelle as "'.__("categorie").'"',
    $select__etab_label__column__as,
    $displayed_field_di,
    'reunion.code as "'.__("reunion").'"',
    'to_char(reunion.date_reunion ,\'DD/MM/YYYY\') as "'.__("date").'"',
    'dossier_instruction_reunion.ordre as "'.__("ordre").'"',
    'reunion_avis0.libelle as "'.__("avis").'"',
    );
//
$champRecherche = array(
    'dossier_instruction_reunion.dossier_instruction_reunion as "'.__("dossier_instruction_reunion").'"',
    $displayed_field_di,
    'reunion_type.libelle as "'.__("reunion_type").'"',
    'reunion_categorie.libelle as "'.__("categorie").'"',
    'reunion.code as "'.__("reunion").'"',
    'dossier_instruction_reunion.ordre as "'.__("ordre").'"',
    'reunion_avis0.libelle as "'.__("avis").'"',
    );

// Dans le contexte du DI, les colonnes 'etablissement' et 'di' sont inutiles
if (isset($retourformulaire)
    && in_array($retourformulaire, $foreign_keys_extended["dossier_instruction"])) {
    //
    $champAffiche = array_diff(
        $champAffiche,
        array($select__etab_label__column__as, $displayed_field_di, )
    );
}

// Si contexte DI
if (in_array($retourformulaire, $foreign_keys_extended["dossier_instruction"])) {

    // Instanciation du DI ou de sa surcharge
    if (isset($f)) {
        $di = $f->get_inst__om_dbform(array(
            "obj" => $retourformulaire,
            "idx" => $idxformulaire,
        ));
        // Si service DI différent de celui de l'utilisateur
        if ($di->is_from_good_service() === false) {
            // Désactivation du bouton ajout du soustab
            if (isset($tab_actions) === true
                && is_array($tab_actions) === true
                && array_key_exists("corner", $tab_actions) === true
                && is_array($tab_actions["corner"]) === true
                && array_key_exists("ajouter", $tab_actions["corner"]) === true) {
                //
                unset($tab_actions["corner"]["ajouter"]);
            }
        }
    }
}
