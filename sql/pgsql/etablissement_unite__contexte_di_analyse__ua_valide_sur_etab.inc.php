<?php
/**
 * Listing des UA validées sur l'établissement dans le contexte d'une analyse 
 * de dossier d'instruction. 
 *
 * Ce listing est un tableau qui fait apparaître toutes les UA validées et non
 * archivées qui concernent l'établissement lié à l'analyse sur laquelle on se
 * trouve.
 *
 * @package openaria
 * @version SVN : $Id: etablissement_unite.inc.php 1172 2015-03-17 13:45:35Z nmeucci $
 */

//
include "../sql/pgsql/etablissement_unite.inc.php";

// Fil d'ariane
$ent = __("etablissements")." -> ".__("unites d'accessibilite");

//
$tab_title = __("UA");

//
$champAffiche = array_merge(
    $champAffiche_ua, 
    $champAffiche_cas1, 
    $champAffiche_derogation
);

// Filtre listing sous formulaire - dossier_instruction
if (in_array($retourformulaire, $foreign_keys_extended["dossier_instruction"])
    && isset($idx)) {
    //
    $selection = " 
WHERE 
    etablissement_unite.etat='valide' 
    AND etablissement_unite.archive IS FALSE
    AND etablissement_unite.etablissement=(
        SELECT 
            etablissement.etablissement
        FROM
            ".DB_PREFIXE."etablissement
            LEFT JOIN ".DB_PREFIXE."dossier_coordination
                ON dossier_coordination.etablissement=etablissement.etablissement
            LEFT JOIN ".DB_PREFIXE."dossier_instruction
                ON dossier_instruction.dossier_coordination=dossier_coordination.dossier_coordination
        WHERE
            dossier_instruction.dossier_instruction=".$idx."
    )
";
} else {
    $selection = " WHERE NULL ";
}

// Aucune recherche dans ce contexte
$champRecherche= array(
);

// Aucune action d'ajout d'une UA n'est possible depuis ce listing 
if (isset($tab_actions) === true
    && is_array($tab_actions) === true
    && array_key_exists("corner", $tab_actions) === true
    && is_array($tab_actions["corner"]) === true
    && array_key_exists("ajouter", $tab_actions["corner"]) === true) {
    //
    unset($tab_actions["corner"]["ajouter"]);
}
