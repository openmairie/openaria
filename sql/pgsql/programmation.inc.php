<?php
/**
 * @package openaria
 * @version SVN : $Id$
 */

//
include('../gen/sql/pgsql/programmation.inc.php');

/*
 * /!\ Fil d'ariane surchargé dans l'objet
 */
// Fil d'ariane
$ent = __("suivi")." -> ".__("programmations")." -> ".__("gestion");

//
$case_convocation_exploitants = " CASE programmation.convocation_exploitants
    WHEN 'non_effectuees' THEN '".__('non_effectuees')."'
    WHEN 'a_envoyer' THEN '". __('a_envoyer')."'
    WHEN 'envoyees' THEN '".__('envoyees')."'
    WHEN 'a_completer' THEN '".__('a_completer')."'
END ";

//
$case_convocation_membres = " CASE programmation.convocation_membres
    WHEN 'non_envoyees' THEN '".__('non_envoyees')."'
    WHEN 'a_envoyer' THEN '". __('a_envoyer')."'
    WHEN 'envoyees' THEN '".__('envoyees')."'
    WHEN 'a_renvoyer' THEN '".__('a_renvoyer')."'
END ";

//
$champAffiche = array(
    'programmation.programmation as "'.__("programmation").'"',
    'programmation.semaine_annee as "'.__("semaine_annee").'"',
    'programmation.version as "'.__("version").'"',
    'programmation_etat.libelle as "'.__("programmation_etat").'"',
    'to_char(programmation.date_modification ,\'DD/MM/YYYY\') as "'.__("date_modification").'"',
    $case_convocation_exploitants.' as "'.__("convocation_exploitants").'"',
    'to_char(programmation.date_envoi_ce ,\'DD/MM/YYYY\') as "'.__("date_envoi_ce").'"',
    $case_convocation_membres.' as "'.__("convocation_membres").'"',
    'to_char(programmation.date_envoi_cm ,\'DD/MM/YYYY\') as "'.__("date_envoi_cm").'"', 
);

$tri = "ORDER BY programmation.annee DESC, programmation.numero_semaine DESC";

// Filtre sur le service de l'utilisateur
include "../sql/pgsql/filter_service.inc.php";
