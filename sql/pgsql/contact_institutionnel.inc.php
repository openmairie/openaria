<?php
/**
 * Surcharge de la classe contact pour afficher les contacts sans lien.
 * 
 * @package openaria
 * @version SVN : $Id$
 */

//
include "../sql/pgsql/contact.inc.php";

// Fil d'Ariane
$ent = __("suivi")." -> ".__("documents generes")." -> ".__("contact_institutionnel");

// SELECT
$champAffiche = array(
    'contact.contact as "'.__("contact").'"',
    //
    "CASE  
        WHEN contact.qualite = 'particulier'
        THEN
            TRIM(CONCAT_WS(' ', contact_civilite.libelle, contact.nom, contact.prenom))
        ELSE
            CASE
                WHEN contact.nom IS NOT NULL OR contact.prenom IS NOT NULL
                THEN 
                    TRIM(CONCAT_WS(' ', contact.raison_sociale, contact.denomination, 'représenté(e) par', contact_civilite.libelle, contact.nom, contact.prenom))
                ELSE 
                    TRIM(CONCAT_WS(' ', contact.raison_sociale, contact.denomination))
            END
    END as \"".__("contact")."\"",
    //
    'concat(contact.adresse_numero, 
    \' \', contact.adresse_numero2, \' \', contact.adresse_voie,
    \' \', contact.adresse_cp, \' \', contact.adresse_ville)
    as "'.__("adresse").'"',
    //
    "CASE
        WHEN contact.qualite = 'particulier'
        THEN
            '".__("particulier")."'
        ELSE
            '".__("personne morale")."'
    END as \"".__("qualite")."\"",
    //
    'contact.courriel as "'.__("courriel").'"',
);

// Filtre du listing
$selection = " WHERE LOWER(contact_type.code) = LOWER('inst') ";

// Pas d'onglet
$sousformulaire = array();
