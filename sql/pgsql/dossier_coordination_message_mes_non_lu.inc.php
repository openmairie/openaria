<?php
/**
 * @package openaria
 * @version SVN : $Id$
 */

//
include "../sql/pgsql/dossier_coordination_message.inc.php";

/**
 * Récupération de la configuration de la requête à partir du widget.
 */
//
require_once "../obj/pilotage.class.php";
$p = new pilotage();
$conf = $p->get_config_message_mes_non_lu();

// Fil d'Ariane
$ent = __("dossiers")." -> ".__("Messages")." -> ".__("Mes non lus");
//
$tab_description = $conf["message_help"];

/**
 * Composition de la requête
 */
// SELECT
$champAffiche = $conf["query_ct_select_champaffiche"];
$table = $conf["query_ct_from"];
$selection = " WHERE ".$conf["query_ct_where"];

//
$consult_obj = sprintf(
    'dossier_coordination_message_contexte_%s',
    $conf["arguments"]["dest_obj_abrege"]
);

// Actions a gauche : consulter
$tab_actions['left']['consulter'] = array(
    'lien' => sprintf(
        OM_ROUTE_FORM.'&obj=%s&amp;action=21&amp;idx=',
        $consult_obj
    ),
    'id' => '',
    'lib' => '<span class="om-icon om-icon-16 om-icon-fix consult-16" title="'.__('Consulter').'">'.__('Consulter').'</span>',
    'rights' => array('list' => array($consult_obj, $consult_obj.'_consulter'), 'operator' => 'OR'),
    'ordre' => 10,
);
// Action du contenu : consulter
$tab_actions['content'] = $tab_actions['left']['consulter'];

// Options
if (!isset($options)) {
    $options = array();
}
// La recherche simple n'est pas affichée
$champRecherche = array();
$options[] = array(
    'type' => 'search',
    'display' => false,
);

// Mise en valeur d'un message important (reconsultation)
$case_reconsultation = 
$options[] = array(
    "type" => "condition",
    "field" => $conf["query_case_reconsultation"],
    "case" => array(
        "0" => array(
            "values" => array("rouge", ),
            "style" => "ligne-rouge",
        ),
    ),
);

