<?php
/**
 * @package openaria
 * @version SVN : $Id$
 */

// Si l'utilisateur est sur un service
if (!is_null($_SESSION['service'])) {
    // alors on lui enlève la colonne service inutile dans son contexte
    $champAffiche = array_diff(
        $champAffiche, 
        array("service.libelle as \"".__("service")."\"", )
    );
    // alors on lui enlève la recherche sur cette colonne service inutile dans son contexte
    $champRecherche = array_diff(
        $champRecherche,
        array("service.libelle as \"".__("service")."\"", )
    );
    // et on filtre le listing sur son service
    if (preg_match("/ where /i", str_replace("\n", " ", $selection)) === 1) {
        $selection .= " AND service.service=".$_SESSION["service"]." ";
    } else {
        $selection .= " WHERE service.service=".$_SESSION["service"]." ";
    }
} else {
    // On ajoute la colonne service dans le listing
    if (array_search("service.libelle as \"".__("service")."\"", $champAffiche) === false) {
        $champAffiche[] = "service.libelle as \"".__("service")."\"";
    } else {
        // alors on lui enlève la colonne service inutile dans son contexte
        $champAffiche = array_diff(
            $champAffiche, 
            array("service.libelle as \"".__("service")."\"", )
        );
        $champAffiche[] = "service.libelle as \"".__("service")."\"";
    }
    // On ajoute la colonne service dans la recherche
    if (array_search("service.libelle as \"".__("service")."\"", $champRecherche) === false) {
        $champRecherche[] = "service.libelle as \"".__("service")."\"";
    }
}

// Reconstruit les index du tableau des champs affichés
$champAffiche = array_combine(range(0, count($champAffiche)-1), array_values($champAffiche));
