<?php
/**
 * @package openaria
 * @version SVN : $Id$
 */

include "../sql/pgsql/app_om_tab_common_select.inc.php";
include "../gen/sql/pgsql/dossier_coordination_message.inc.php";

//
$tab_title = __("message");

//
$table .= " LEFT JOIN ".DB_PREFIXE."etablissement
        ON dossier_coordination.etablissement = etablissement.etablissement ";

// Cache le bouton ajouter
// Il n'est pas prévu d'ajouter des messages via l'interface utilisateur
if (isset($tab_actions) === true
    && is_array($tab_actions) === true
    && array_key_exists("corner", $tab_actions) === true
    && is_array($tab_actions["corner"]) === true
    && array_key_exists("ajouter", $tab_actions["corner"]) === true) {
    //
    unset($tab_actions["corner"]["ajouter"]);
}

if (isset($f) === true && $f->is_option_display_ads_field_in_tables_enabled() !== true) {
    $champaffiche_dossier_ads = array();
} else {
    $champaffiche_dossier_ads = array(
        "CASE 
        WHEN dossier_coordination.dossier_autorisation_ads is null AND dossier_coordination.dossier_instruction_ads is not null THEN dossier_coordination.dossier_instruction_ads
        WHEN dossier_coordination.dossier_autorisation_ads is not null AND dossier_coordination.dossier_instruction_ads is not null THEN dossier_coordination.dossier_instruction_ads
        ELSE dossier_coordination.dossier_autorisation_ads
        END as \"".__("dossier ADS")."\"",
    );
}
$champaffiche_message_infos = array(
    'dossier_coordination_message.dossier_coordination_message as "'.__("identifiant").'"',
    'to_char(dossier_coordination_message.date_emission ,\'DD/MM/YYYY HH24:MI:SS\') as "'.__("date").'"',
    'dossier_coordination_message.type as "'.__("type").'"',
    'dossier_coordination_message.categorie as "'.__("categorie").'"',
    'dossier_coordination_message.emetteur as "'.__("emetteur").'"',
);
$champaffiche_dc_libelle = array(
    'dossier_coordination.libelle as "'.__("dc").'"',
);
$champaffiche_etablissement = array($select__etab_label__column__as, );

//
$case_lu_non_lu = "
WHEN 't' THEN '<span class=''om-icon om-icon-16 om-icon-fix read-16'' title=''".__('Lu')."''>Lu</span>' 
ELSE '<span class=''om-icon om-icon-16 om-icon-fix unread-16'' title=''".__('Non Lu')."''>Non Lu</span>'
";
$empty_marqueur = "-";
$case_si_cadre_lu = "
CASE 
    dossier_coordination_message.si_mode_lecture 
    WHEN 'mode0' THEN '".$empty_marqueur."'
    WHEN 'mode1' THEN CASE dossier_coordination_message.si_cadre_lu ".$case_lu_non_lu." END
    WHEN 'mode2' THEN '".$empty_marqueur."'
    WHEN 'mode3' THEN CASE dossier_coordination_message.si_cadre_lu ".$case_lu_non_lu." END
END
";
$case_si_technicien_lu = "
CASE 
    dossier_coordination_message.si_mode_lecture 
    WHEN 'mode0' THEN '".$empty_marqueur."'
    WHEN 'mode1' THEN '".$empty_marqueur."'
    WHEN 'mode2' THEN CASE dossier_coordination_message.si_technicien_lu  ".$case_lu_non_lu." END
    WHEN 'mode3' THEN CASE dossier_coordination_message.si_technicien_lu  ".$case_lu_non_lu." END
END
";
$case_acc_cadre_lu = "
CASE 
    dossier_coordination_message.acc_mode_lecture 
    WHEN 'mode0' THEN '".$empty_marqueur."'
    WHEN 'mode1' THEN CASE dossier_coordination_message.acc_cadre_lu ".$case_lu_non_lu." END
    WHEN 'mode2' THEN '".$empty_marqueur."'
    WHEN 'mode3' THEN CASE dossier_coordination_message.acc_cadre_lu ".$case_lu_non_lu." END
END
";
$case_acc_technicien_lu = "
CASE 
    dossier_coordination_message.acc_mode_lecture 
    WHEN 'mode0' THEN '".$empty_marqueur."'
    WHEN 'mode1' THEN '".$empty_marqueur."'
    WHEN 'mode2' THEN CASE dossier_coordination_message.acc_technicien_lu  ".$case_lu_non_lu." END
    WHEN 'mode3' THEN CASE dossier_coordination_message.acc_technicien_lu  ".$case_lu_non_lu." END
END
";

//
$champRecherche = array(
    'dossier_coordination_message.type as "'.__("type").'"',
    'dossier_coordination_message.categorie as "'.__("categorie").'"',
    'dossier_coordination_message.emetteur as "'.__("emetteur").'"',
);

// Tri par date
$tri = "order by date_emission desc";

// Options
if (!isset($options)) {
    $options = array();
}
// On affiche le champ lu en gras
$options[] = array(
    "type" => "condition",
    "field" => "dossier_coordination_message.categorie",
    "case" => array(
        "0" => array(
            "values" => array("interne", ),
            "style" => "message-categorie-interne",
        ),
        "1" => array(
            "values" => array("entrant", ),
            "style" => "message-categorie-entrant",
        ),
        "2" => array(
            "values" => array("sortant", ),
            "style" => "message-categorie-sortant",
        ),
    ),
);
