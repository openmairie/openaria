<?php
/**
 * Surcharge de la classe dossier_coordination pour afficher directement
 * seulement les dossiers de coordination à clore.
 * 
 * @package openaria
 * @version SVN : $Id: dossier_coordination_a_cloturer.inc.php 1151 2015-03-12 18:29:52Z fmichon $
 */

//
include "../sql/pgsql/dossier_coordination.inc.php";

// Fil d'Ariane
$ent = __("dossiers")." -> ".__("DC")." -> ".__("dossiers a cloturer");

// SELECT
// On enlève la colonne affichant le marqueur de clôture inutile dans ce contexte
$champAffiche = array_diff(
    $champAffiche,
    $champaffiche_dc_marqueur_cloture
);

// Filtre du listing
$selection = " WHERE dossier_coordination.dossier_cloture = FALSE
                 AND dossier_coordination.a_qualifier = FALSE
                 AND dossier_coordination.dossier_coordination NOT IN (
                   SELECT dossier_coordination
                   FROM ".DB_PREFIXE."dossier_instruction
                   WHERE  dossier_instruction.dossier_cloture IS FALSE
                   GROUP BY dossier_coordination HAVING count(*)>0
                 )";

// Pas de recherche avancée
$options = array();
$options[] = $option_condition_cloture;
