<?php
/**
 * @package openaria
 * @version SVN : $Id$
 */

//
include('../sql/pgsql/visite.inc.php');

$champAffiche = array(
    'visite.visite as "'.__("visite").'"',
    'dossier_instruction.libelle as "'.__("dossier_instruction").'"',
    'visite_etat.libelle as "'.__("visite_etat").'"',
    'visite.convocation_exploitants as "'.__("convocation_exploitants").'"',
    'visite.programmation_version_creation as "'.__("programmation_version_creation").'"',
    'to_char(visite.date_creation ,\'DD/MM/YYYY\') as "'.__("date_creation").'"',
);

if (isset($tab_actions) === true
    && is_array($tab_actions) === true
    && array_key_exists("corner", $tab_actions) === true
    && is_array($tab_actions["corner"]) === true
    && array_key_exists("ajouter", $tab_actions["corner"]) === true) {
    //
    unset($tab_actions["corner"]["ajouter"]);
}
