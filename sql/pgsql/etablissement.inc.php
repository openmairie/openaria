<?php
/**
 * @package openaria
 * @version SVN : $Id$
 */

//
include "../sql/pgsql/app_om_tab_common_select.inc.php";
include "../gen/sql/pgsql/etablissement.inc.php";

//
$tab_title = __("etablissement");

/**
 * Gestion SOUSFORMULAIRE => $sousformulaire
 */
$sousformulaire = array();
if (isset($f) === true
    && $f->is_option_contrainte_enabled() === true) {
    $sousformulaire[] = 'lien_contrainte_etablissement';
}
if (isset($f) === true
    && $f->is_option_unite_accessibilite_enabled() === true) {
    $sousformulaire[] = 'etablissement_unite';
}
$sousformulaire = array_merge(
    $sousformulaire,
    array(
        'contact',
        'dossier_coordination',
        'piece',
        'courrier',
        'autorite_police',
        //'etablissement_parcelle',
        //'etablissement_type_secondaire',
    )
);
$sousformulaire_parameters = array(
    "lien_contrainte_etablissement" => array(
        "title" => __("Contraintes"),
        "href" => OM_ROUTE_FORM."&obj=etablissement&action=4&idx=".((isset($idx))?$idx:"")."&retourformulaire=".(isset($obj) ? $obj : "etablissement")."&",
    ),
    "autorite_police" => array(
        "title" => __("AP"),
    ),
    "dossier_coordination" => array(
        "title" => __("DC"),
    ),
    "contact" => array(
        "title" => __("Contacts"),
    ),
    "piece" => array(
        "title" => __("Documents entrants"),
    ),
    "courrier" => array(
        "title" => __("Documents generes"),
    ),
    "etablissement_unite" => array(
        "title" => __("UA"),
        "href" => OM_ROUTE_FORM."&obj=".(isset($obj) ? $obj : "etablissement")."&action=20&idx=".((isset($idx))?$idx:"")."&",
    ),
);
