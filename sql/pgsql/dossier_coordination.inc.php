<?php
/**
 * @package openaria
 * @version SVN : $Id$
 */

include "../sql/pgsql/app_om_tab_common_select.inc.php";
include "../gen/sql/pgsql/dossier_coordination.inc.php";

// Fil d'Ariane
$ent = __("dossiers")." -> ".__("DC")." -> ".__("tous les dossiers");

// FROM 
$table = DB_PREFIXE."dossier_coordination
    LEFT JOIN ".DB_PREFIXE."dossier_coordination as dossier_coordination_2
        ON dossier_coordination.dossier_coordination_parent=dossier_coordination_2.dossier_coordination 
    LEFT JOIN ".DB_PREFIXE."dossier_coordination_type 
        ON dossier_coordination.dossier_coordination_type=dossier_coordination_type.dossier_coordination_type 
    LEFT JOIN ".DB_PREFIXE."etablissement 
        ON dossier_coordination.etablissement=etablissement.etablissement 
    LEFT JOIN ".DB_PREFIXE."arrondissement 
        ON etablissement.adresse_arrondissement=arrondissement.arrondissement 
    LEFT JOIN ".DB_PREFIXE."voie 
        ON etablissement.adresse_voie=voie.voie 
    LEFT JOIN ".DB_PREFIXE."etablissement_categorie 
        ON dossier_coordination.etablissement_categorie=etablissement_categorie.etablissement_categorie 
    LEFT JOIN ".DB_PREFIXE."etablissement_type 
        ON dossier_coordination.etablissement_type=etablissement_type.etablissement_type ";

// SELECT
$champaffiche_dc_id = array(
    "dossier_coordination.dossier_coordination as \"".__("dossier_coordination")."\"",
);
$champaffiche_dc_libelle = array(
    "dossier_coordination.libelle as \"".__("DC")."\"",
);
$champaffiche_etablissement = array($select__etab_label__column__as, );
$champaffiche_etablissement_adresse = array(
    "CONCAT(etablissement.adresse_numero, ' ', etablissement.adresse_numero2, ' ', voie.libelle, ' ', etablissement.adresse_cp, ' (', arrondissement.libelle, ')') as \"".__("adresse")."\"",
);
$champaffiche_dc_infos = array(
    "CASE dossier_coordination.erp WHEN 't' THEN 'Oui' ELSE 'Non' END as \"".__("erp")."\"",
    "to_char(dossier_coordination.date_demande,'DD/MM/YYYY') as \"".__("date_demande")."\"",
    "to_char(dossier_coordination.date_butoir,'DD/MM/YYYY') as \"".__("date_butoir")."\"",
);
$champaffiche_dc_marqueur_a_qualifier = array(
    "CASE dossier_coordination.a_qualifier WHEN 't' THEN 'Oui' ELSE 'Non' END as \"".__("a_qualifier")."\"",
);
$champaffiche_dc_marqueur_cloture_case = "CASE dossier_coordination.dossier_cloture WHEN 't' THEN 'Oui' ELSE 'Non' END";
$champaffiche_dc_marqueur_cloture = array(
    $champaffiche_dc_marqueur_cloture_case." as \"".__("dossier_cloture")."\"",
);
$champaffiche_dossier_ads  = array();
if (isset($f) === true
    && $f->is_option_display_ads_field_in_tables_enabled() === true) {
    //
    $champaffiche_dossier_ads = array(
        "CASE 
            WHEN dossier_coordination.dossier_autorisation_ads IS NULL AND dossier_coordination.dossier_instruction_ads IS NOT NULL
                THEN dossier_coordination.dossier_instruction_ads
            WHEN dossier_coordination.dossier_autorisation_ads IS NOT NULL AND dossier_coordination.dossier_instruction_ads IS NOT NULL
                THEN dossier_coordination.dossier_instruction_ads
            ELSE 
                dossier_coordination.dossier_autorisation_ads
        END as \"".__("dossier ADS")."\"",
    );
}
$champAffiche = array_merge(
    $champaffiche_dc_id,
    $champaffiche_dc_libelle,
    $champaffiche_dossier_ads,
    $champaffiche_etablissement,
    $champaffiche_etablissement_adresse,
    $champaffiche_dc_infos,
    $champaffiche_dc_marqueur_a_qualifier,
    $champaffiche_dc_marqueur_cloture
);

// Champs de recherche en mode simple
$champRecherche= array_merge(
    $champaffiche_dc_libelle,
    $champaffiche_dossier_ads,
    $champaffiche_etablissement
);

//
$tab_title = __("DC");

/**
 * Gestion SOUSFORMULAIRE => $sousformulaire
 */
$sousformulaire = array();
if ($f->is_option_contrainte_enabled() === true) {
    $sousformulaire[] = 'lien_contrainte_dossier_coordination';
}
$sousformulaire = array_merge(
    $sousformulaire,
    array(
        'contact_contexte_dossier_coordination',
        'dossier_coordination',
        'piece',
        'courrier',
        'autorite_police',
        //'dossier_coordination_parcelle',
        //'dossier_instruction',
        //'lien_dossier_coordination_contact',
    )
);
if ($f->is_option_referentiel_ads_enabled() === true) {
    $sousformulaire[] = 'dossier_coordination_message_contexte_dc';
}
$sousformulaire_parameters = array(
    "lien_contrainte_dossier_coordination" => array(
        "title" => __("Contraintes"),
        "href" => OM_ROUTE_FORM."&obj=dossier_coordination&action=4&idx=".((isset($idx))?$idx:"")."&retourformulaire=".((isset($_GET['obj']))?$_GET['obj']:"")."&",
    ),
    "contact_contexte_dossier_coordination" => array(
        "title" => __("Contacts"),
    ),
    "autorite_police" => array(
        "title" => __("AP"),
    ),
    "dossier_coordination" => array(
        "title" => __("DC Fils"),
    ),
    "piece" => array(
        "title" => __("Documents entrants"),
        "href" => OM_ROUTE_FORM."&obj=".(isset($obj) ? $obj : "dossier_coordination")."&action=21&idx=".((isset($idx))?$idx:"")."&",
    ),
    "courrier" => array(
        "title" => __("Documents generes"),
    ),
    "dossier_coordination_message_contexte_dc" => array(
        "title" => __("Messages"),
    ),
);

// Cache le bouton ajouter
// Une entrée de menu dédiée permet d'ajouter un DC
if (isset($tab_actions) === true
    && is_array($tab_actions) === true
    && array_key_exists("corner", $tab_actions) === true
    && is_array($tab_actions["corner"]) === true
    && array_key_exists("ajouter", $tab_actions["corner"]) === true) {
    //
    unset($tab_actions["corner"]["ajouter"]);
}

// Affichage du bouton de redirection vers le SIG externe si configuré
// On n'affiche pas le lien de redirection dans les sous-formulaires
if ($f->getParameter('option_sig') == 'sig_externe'
    && $retourformulaire === '') {
    try {
        $inst_geoaria = $f->get_inst_geoaria();
        if ($inst_geoaria->is_localization_dc_enabled() === true) {
            $tab_actions['left']["localiser-sig-externe"] = array(
                'lien' => OM_ROUTE_FORM.'&obj=dossier_coordination&amp;action=30&amp;idx=',
                'id' => '',
                'lib' => '<span class="om-icon om-icon-16 om-icon-fix sig-16" title="'.__('Localiser').'">'.__('Localiser').'</span>',
                'rights' => array('list' => array('dossier_coordination', 'dossier_coordination_consulter'), 'operator' => 'OR'),
                'ordre' => 20,
                'target' => "blank",
                'ajax' => false,
            );
        }
    } catch (geoaria_exception $e) {
        //
    }
}

// Option condition clôture : permet d'ajouter la classe css dc-cloture
// sur chaque enregistrement qui est clôturé.
$option_condition_cloture = array(
    "type" => "condition",
    "field" => $champaffiche_dc_marqueur_cloture_case,
    "case" => array(
        array(
            "values" => array("Oui", ),
            "style" => "dc-cloture",
        ),
    ),
);

// Recherche avancée
if (!isset($options)) {
    $options = array();
}

// Options pour les select de faux booléens
$args = array(
    0 => array("", "t", "f", ),
    1 => array(__("choisir"), __("Oui"), __("Non"), ),
);

// Champs pour la recherche avancée
$champs['libelle'] = array(
    'table' => 'dossier_coordination',
    'colonne' => 'libelle',
    'type' => 'text',
    'taille' => 30,
    'max' => 500,
    'libelle' => __('DC')
);
$champs['etablissement'] = array(
    'table' => 'etablissement',
    'colonne' => array('libelle','code'),
    'type' => 'text',
    'taille' => 30,
    'max' => 100,
    'libelle' => __('etablissement')
);
$champs['adresse_numero'] = array(
    'table' => 'etablissement',
    'colonne' => 'adresse_numero',
    'type' => 'text',
    'taille' => 40,
    'max' => 100,
    'libelle' => __('numero')
);
$champs['adresse_voie'] = array(
    'table' => 'voie',
    'colonne' => 'libelle',
    'type' => 'text',
    'taille' => 40,
    'max' => 100,
    'libelle' => __('voie')
);
$champs['adresse_arrondissement'] = array(
    'table' => 'arrondissement',
    'colonne' => 'arrondissement',
    'libelle' => __('arrondissement'),
    'type' => 'select',
    'subtype' => 'sqlselect',
    'sql' => 'SELECT arrondissement, libelle FROM '.DB_PREFIXE.'arrondissement ORDER BY code::int' ,
);
$champs['dossier_coordination_type'] = array(
    'table' => 'dossier_coordination',
    'colonne' => 'dossier_coordination_type',
    'type' => 'select',
    'libelle' => __('dossier_coordination_type')
);
$champs['date_demande'] = array(
    'table' => 'dossier_coordination',
    'colonne' => 'date_demande',
    'libelle' => __('date_demande'),
    'lib1'=> __("debut"),
    'lib2' => __("fin"),
    'type' => 'date',
    'taille' => 8,
    'where' => 'intervaldate'
);
$champs['date_butoir'] = array(
    'table' => 'dossier_coordination',
    'colonne' => 'date_butoir',
    'libelle' => __('date_butoir'),
    'lib1'=> __("debut"),
    'lib2' => __("fin"),
    'type' => 'date',
    'taille' => 8,
    'where' => 'intervaldate'
);
$champs['erp'] = array(
    'table' => 'dossier_coordination',
    'colonne' => 'erp',
    'type' => 'select',
    'libelle' => __('erp'),
    "subtype" => "manualselect",
    "args" => $args,
);
$champs['a_qualifier'] = array(
    'table' => 'dossier_coordination',
    'colonne' => 'a_qualifier',
    'type' => 'select',
    'libelle' => __('a_qualifier'),
    "subtype" => "manualselect",
    "args" => $args,
);
$champs['dossier_cloture'] = array(
    'table' => 'dossier_coordination',
    'colonne' => 'dossier_cloture',
    'type' => 'select',
    'libelle' => __('dossier_cloture'),
    "subtype" => "manualselect",
    "args" => $args,
);
$champs['dossier_coordination_parent'] = array(
    'table' => 'dossier_coordination_2',
    'colonne' => 'libelle',
    'type' => 'text',
    'taille' => 30,
    'max' => 100,
    'libelle' => __('dossier_coordination_parent')
);
$champs['etablissement_type'] = array(
    'table' => 'dossier_coordination',
    'colonne' => 'etablissement_type',
    'type' => 'select',
    'libelle' => __('etablissement_type')
);
$champs['etablissement_categorie'] = array(
    'table' => 'dossier_coordination',
    'colonne' => 'etablissement_categorie',
    'type' => 'select',
    'libelle' => __('etablissement_categorie')
);
$champs['etablissement_locaux_sommeil'] = array(
    'table' => 'dossier_coordination',
    'colonne' => 'etablissement_locaux_sommeil',
    'type' => 'select',
    'libelle' => __('locaux_a_sommeil'),
    "subtype" => "manualselect",
    "args" => $args,
);
$champs['dossier_ads'] = array(
    'table' => 'dossier_coordination',
    'colonne' => array('dossier_instruction_ads', 'dossier_autorisation_ads', ),
    'type' => 'text',
    'taille' => 30,
    'max' => 100,
    'libelle' => __('dossier ADS')
);
// Affichage du champ de recherche avancée 'localisé' seulement lorsque le SIG
// externe est configuré
if (isset($f) === true
    && $f->getParameter('option_sig') == 'sig_externe'
    && $retourformulaire === '') {
    try {
        $inst_geoaria = $f->get_inst_geoaria();
        if ($inst_geoaria->is_localization_dc_enabled() === true) {
            $champs['geolocalise'] = array(
                'table' => 'dossier_coordination',
                'colonne' => 'geolocalise',
                'type' => 'select',
                'libelle' => __('localise'),
                "subtype" => "manualselect",
                "args" => $args,
            );
        }
    } catch (geoaria_exception $e) {
        //
    }
}
$tmp_advsearch_options = array(
    'type' => 'search',
    'display' => true,
    'advanced'  => $champs,
    'default_form'  => 'advanced',
    'absolute_object' => 'dossier_coordination'
);

if ($f->getParameter('option_sig') == 'sig_externe') {
    try {
        $inst_geoaria = $f->get_inst_geoaria();
        if ($inst_geoaria->is_localization_dc_enabled() === true) {
            $tmp_advsearch_options['export'] = array(
                'geoaria' => array(
                    'right' => 'dossier_coordination_geoaria',
                    'url' => OM_ROUTE_FORM.'&obj=dossier_coordination&action=31&idx=0',
                ),
            );
        }
    } catch (geoaria_exception $e) {
        //
    }
}

$options[] = $tmp_advsearch_options;
$options[] = $option_condition_cloture;

