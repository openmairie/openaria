<?php
/**
 * Ce script permet...
 *
 * @package openaria
 * @version SVN : $Id$
 */

//
include('../sql/pgsql/dossier_instruction.inc.php');

// Fil d'ariane
$ent = __("dossiers")." -> ".__("DI")." -> ".__("toutes les visites");

// Modification du WHERE
// Filtre sur les dossiers de type VISIT
$selection = " WHERE dossier_type.code = 'VISIT' ";

// Filtre sur le service de l'utilisateur
include "../sql/pgsql/filter_service.inc.php";
