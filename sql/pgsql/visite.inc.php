<?php
/**
 * Ce script permet de configurer le listing 'Visites'.
 *
 * @package openaria
 * @version SVN : $Id$
 */


include "../sql/pgsql/app_om_tab_common_select.inc.php";
include "../gen/sql/pgsql/visite.inc.php";

// Fil d'ariane
$ent = __("suivi")." -> ".__("programmations")." -> ".__("visites");
if (!empty($retourformulaire)) {
    $ent = " -> ".__("visites");
}

$select__prog_semaine_annee__column__as = "programmation.semaine_annee as \"".__("programmation")."\"";
$select__di_label__column__as = "CONCAT('<span class=\"di-libelle\">', dossier_instruction.libelle, '</span>') as \"".__("DI")."\"";

$champAffiche = array(
    'visite.visite as "'.__("visite").'"',
    $select__prog_semaine_annee__column__as,
    "to_char(visite.date_visite,'TMDay') as \"".__("jour")."\"",
    'to_char(visite.date_visite ,\'DD/MM/YYYY\') as "'.__("date").'"',
    'CONCAT(\''.__(" de ").'\', visite.heure_debut, \''.__(" à ").'\', visite.heure_fin) as "'.__("horaire").'"',
    'acteur.nom_prenom as "'.__("acteur").'"',
    $select__etab_label__column__as,
    $select__di_label__column__as,
    'CONCAT(
        \'<span class="label\',
        CASE 
            WHEN 
                visite.programmation_version_annulation IS NOT NULL
                AND visite.programmation_version_annulation > 1 
            THEN
                \' state-ann bg-danger">'.__("annulée").'\'
            WHEN
                visite.programmation_version_creation IS NOT NULL
                AND visite.programmation_version_creation=programmation.version
                AND programmation_etat.code != \'VAL\' 
            THEN
                \' state-enc bg-warning">'.__("en cours de création").'\'
            ELSE
                \' state-pla bg-valid">'.__("planifiée").'\'
        END,
        \'</span>\'
    ) as "'.__("etat").'"',
    'visite.convocation_exploitants as "'.__("convocation_exploitants").'"',
    'to_char(visite.date_annulation ,\'DD/MM/YYYY\') as "'.__("date_annulation").'"',
    "visite_motif_annulation.libelle as \"".__("motif d'annulation")."\"",
    'programmation_version_creation as "'.__("créée en v").'"',
    'programmation_version_modification as "'.__("modifiée en v").'"',
    'programmation_version_annulation as "'.__("annulée en v").'"',
);

$champRecherche = array(
    $select__prog_semaine_annee__column__as,
    $select__etab_label__column__as,
    'acteur.nom_prenom as "'.__("acteur").'"',
);

$table .= sprintf(
    '
    LEFT JOIN %1$sprogrammation_etat
        ON programmation.programmation_etat=programmation_etat.programmation_etat
    LEFT JOIN %1$sdossier_coordination
        ON dossier_instruction.dossier_coordination=dossier_coordination.dossier_coordination
    LEFT JOIN %1$setablissement 
        ON dossier_coordination.etablissement=etablissement.etablissement
    ',
    DB_PREFIXE
);

$tri = " ORDER BY visite.date_visite DESC, visite.heure_debut DESC, visite.heure_fin DESC, visite_etat.libelle ";

// Dans le contexte du DI, la colonne 'etablissement' est inutile
if (isset($retourformulaire)
    && in_array($retourformulaire, $foreign_keys_extended["dossier_instruction"])) {
    //
    $champAffiche = array_diff(
        $champAffiche,
        array(
            $select__etab_label__column__as,
            $select__di_label__column__as,
        )
    );
}

// Dans le contexte de la programmation, la colonne 'programmation' est inutile
// et le tri se fait par ordre chronologique
if (isset($retourformulaire)
    && in_array($retourformulaire, $foreign_keys_extended["programmation"])) {
    //
    $champAffiche = array_diff(
        $champAffiche,
        array(
            $select__prog_semaine_annee__column__as,
        )
    );
    $tri = " ORDER BY visite.date_visite, visite.heure_debut, visite.heure_fin, visite_etat.libelle ";
}

// L'action 'ajouter' est cachée. Seule la programmation des visites permet
// d'ajouter une nouvelle visite via un glisser/déposer.
if (isset($tab_actions) === true
    && is_array($tab_actions) === true
    && array_key_exists("corner", $tab_actions) === true
    && is_array($tab_actions["corner"]) === true
    && array_key_exists("ajouter", $tab_actions["corner"]) === true) {
    //
    unset($tab_actions["corner"]["ajouter"]);
}

// Permet de passer le numéro du dossier d'instruction au formulaire de demande
if ($obj == "visite" && isset($extra_parameters)) {
    $date_visite = "";
    if (isset($_GET['date_visite'])) {
        $date_visite = $_GET['date_visite'];
    }
    $extra_parameters["date_visite"] = $date_visite;
    //
    $heure_debut = "";
    if (isset($_GET['heure_debut'])) {
        $heure_debut = $_GET['heure_debut'];
    }
    $extra_parameters["heure_debut"] = $heure_debut;
    //
    $heure_fin = "";
    if (isset($_GET['heure_fin'])) {
        $heure_fin = $_GET['heure_fin'];
    }
    $extra_parameters["heure_fin"] = $heure_fin;
    //
    $id_technicien = "";
    if (isset($_GET['id_technicien'])) {
        $id_technicien = $_GET['id_technicien'];
    }
    $extra_parameters["id_technicien"] = $id_technicien;
    //
    $id_programmation = "";
    if (isset($_GET['id_programmation'])) {
        $id_programmation = $_GET['id_programmation'];
    }
    $extra_parameters["id_programmation"] = $id_programmation;
    //
    $id_di = "";
    if (isset($_GET['id_di'])) {
        $id_di = $_GET['id_di'];
    }
    $extra_parameters["id_di"] = $id_di;
}

/**
 * ADVS
 */
$advsearch_fields = array(
    "programmation" => array(
        "table" => "programmation",
        "colonne" => array("semaine_annee", ),
        "type" => "text",
        "taille" => 10,
        "max" => 100,
        "libelle" => __("programmation"),
    ),
    "date" => array(
        "table" => "visite",
        "colonne" => "date_visite",
        "libelle" => __("date"),
        "lib1"=> __("debut"),
        "lib2" => __("fin"),
        "type" => "date",
        "taille" => 8,
        "where" => "intervaldate"
    ),
    "acteur" => array(
        'table' => 'acteur',
        'colonne' => 'acteur',
        'type' => 'select',
        'subtype' => 'sqlselect',
        // QUERY liste des acteurs qui peuvent instruire un dossier
        'sql' => sprintf(
            'SELECT 
                acteur.acteur,
                concat(acteur.nom_prenom, \' (\', acteur.acronyme, \')\')
            FROM
                %1$sacteur
            WHERE
                (acteur.role=\'technicien\' OR acteur.role=\'cadre\')
                %2$s
            ',
            DB_PREFIXE,
            (!is_null($_SESSION["service"]) ? 'AND acteur.service=\''.$_SESSION["service"].'\'' : '')
        ),
        'libelle' => __('acteur'),
    ),
    "etablissement" => array(
        "table" => "etablissement",
        "colonne" => array("libelle", "code", ),
        "type" => "text",
        "taille" => 30,
        "max" => 100,
        "libelle" => __("etablissement"),
    ),
    "dossier_instruction" => array(
        "table" => "dossier_instruction",
        "colonne" => array("libelle", ),
        "type" => "text",
        "taille" => 30,
        "max" => 100,
        "libelle" => __("dossier d'instruction"),
    ),
    "visite_etat" => array(
        'table' => 'visite',
        'colonne' => 'visite_etat',
        'type' => 'select',
        'libelle' => __('etat')
    ),
);
// Si l'utilisateur n'a pas de service alors on lui autorise
// à filtrer par service
if (is_null($_SESSION["service"])) {
    $advsearch_fields['service'] = array(
        'table' => 'dossier_instruction',
        'colonne' => 'service',
        'type' => 'select',
        'libelle' => __('service'),
        'subtype' => 'sqlselect',
        'sql' => sprintf(
            'SELECT service.service, service.libelle FROM %1$sservice',
            DB_PREFIXE
        ),
    );
}
$tab_options_advsearch = array(
    "type" => "search",
    "display" => true,
    "advanced"  => $advsearch_fields,
    "default_form"  => "advanced",
    "absolute_object" => "visite",
    "export" => array("csv", ),
);

/**
 * OPTIONS
 */
if (!isset($options)) {
    $options = array();
}
$options[] = $tab_options_advsearch;

// Dans le contexte du DI, ou de la programmation, la colonne 'service' est inutile
// donc on gère la colonne seuement dans les autres contextes
if (isset($retourformulaire) === false
    || (
        isset($retourformulaire) === true
        && in_array($retourformulaire, $foreign_keys_extended["dossier_instruction"]) === false
        && in_array($retourformulaire, $foreign_keys_extended["programmation"]) === false)) {
    // Filtre sur le service de l'utilisateur
    $table .= " LEFT JOIN ".DB_PREFIXE."service 
                    ON service.service=programmation.service ";
    include "../sql/pgsql/filter_service.inc.php";
}
