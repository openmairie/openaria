<?php
/**
 * Surcharge 'autorite_police'.
 *
 * Listing des autorités de police non notifiées ou exécutées.
 *
 * L'objectif de la surcharge est :
 * - le filtre du listing standard
 *
 * @package openaria
 * @version SVN : $Id$
 */

include "../sql/pgsql/autorite_police.inc.php";

// Fil d'Ariane
$ent = __("dossiers")." -> ".__("autorites de police")." -> ".__("AP non notifiees ou executees");

// Filtre du listing
$selection = " WHERE
(
    autorite_police.date_butoir < NOW()
    AND (
        autorite_police.dossier_instruction_reunion_prochain IS NULL
        OR dossier_instruction_reunion_prochain.reunion IS NULL
    )
)
OR ((LOWER(autorite_police_decision.avis) = LOWER('favorable')
    OR LOWER(autorite_police_decision.avis) = LOWER('defavorable'))
AND autorite_police.autorite_police NOT IN 
    (SELECT autorite_police 
    FROM ".DB_PREFIXE."lien_autorite_police_courrier 
    WHERE courrier IS NOT NULL)
)
";

