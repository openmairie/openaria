<?php
/**
 * Script de paramétrage du listing des 'passages en réunion'.
 *
 * Dans le contexte d'une réunion. 
 *
 * @package openaria
 * @version SVN : $Id$
 */

include "../sql/pgsql/dossier_instruction_reunion.inc.php";

//
$ent = "-> ".__("passage en reunion");

// Cache le bouton ajouter
if (isset($tab_actions) === true
    && is_array($tab_actions) === true
    && array_key_exists("corner", $tab_actions) === true
    && is_array($tab_actions["corner"]) === true
    && array_key_exists("ajouter", $tab_actions["corner"]) === true) {
    //
    unset($tab_actions["corner"]["ajouter"]);
}
