<?php
/**
 * @package openaria
 * @version SVN : $Id$
 */

//
include "../gen/sql/pgsql/modele_edition.inc.php";

// SELECT 
$champAffiche = array(
    'modele_edition.modele_edition as "'.__("modele_edition").'"',
    'modele_edition.code as "'.__("code").'"',
    'modele_edition.libelle as "'.__("libelle").'"',
    'courrier_type.libelle as "'.__("courrier_type").'"',
    'modele_edition.om_lettretype_id as "'.__("om_lettretype").'"',
    );
// On affiche les champs de date de validité uniquement lorsque le paramètre
// d'affichage des éléments expirés est activé
if ($om_validite == true && isset($_GET['valide']) && $_GET['valide'] === 'false') {
    $champAffiche = array_merge($champAffiche, $displayed_fields_validite);
}
//
$champNonAffiche = array(
    'modele_edition.description as "'.__("description").'"',
    );
//
$champRecherche = array(
    'modele_edition.modele_edition as "'.__("modele_edition").'"',
    'modele_edition.code as "'.__("code").'"',
    'modele_edition.libelle as "'.__("libelle").'"',
    'courrier_type.libelle as "'.__("courrier_type").'"',
    'modele_edition.om_lettretype_id as "'.__("om_lettretype").'"',
    );

/**
 * Gestion SOUSFORMULAIRE => $sousformulaire
 */
$sousformulaire = array(
    "om_lettretype",
);
