<?php
/**
 * @package openaria
 * @version SVN : $Id$
 */

//
include "../sql/pgsql/dossier_coordination_message.inc.php";

/**
 * Récupération de la configuration de la requête à partir du widget.
 */
//
require_once "../obj/pilotage.class.php";
$p = new pilotage();
$conf = $p->get_config_message_mes_non_lu(array(
    "dossier_instruction_id" => (isset($idxformulaire) ? $idxformulaire : -1),
));

// Fil d'Ariane
$ent = __("message");

// SELECT
$champAffiche = $champaffiche_message_infos;
$table = $conf["query_ct_from"];
if (isset($idxformulaire)) {
    $selection = " WHERE dossier_instruction.dossier_instruction = ".$idxformulaire." ";
}

if (!isset($f)) {
    return;
}

$case_specific_lu = "''";
if ($f->is_user_with_role_and_service("cadre", "si") === true) {
    $champAffiche[] = $case_si_cadre_lu." as \"".__("cadre si")."\" ";
    $champAffiche[] = $case_si_technicien_lu." as \"".__("tech si")."\" ";
    $case_specific_lu = "CASE WHEN ".$conf["query_ct_where"]." THEN 'Non' END";
} elseif ($f->is_user_with_role_and_service("technicien", "si") === true) {
    $champAffiche[] = $case_si_cadre_lu." as \"".__("cadre si")."\" ";
    $champAffiche[] = $case_si_technicien_lu." as \"".__("tech si")."\" ";
    $case_specific_lu = "CASE WHEN ".$conf["query_ct_where"]." THEN 'Non' END";
} elseif ($f->is_user_with_role_and_service("cadre", "acc") === true) {
    $champAffiche[] = $case_acc_cadre_lu." as \"".__("cadre acc")."\" ";
    $champAffiche[] = $case_acc_technicien_lu." as \"".__("tech acc")."\" ";
    $case_specific_lu = "CASE WHEN ".$conf["query_ct_where"]." THEN 'Non' END";
} elseif ($f->is_user_with_role_and_service("technicien", "acc") === true) {
    $champAffiche[] = $case_acc_cadre_lu." as \"".__("cadre acc")."\" ";
    $champAffiche[] = $case_acc_technicien_lu." as \"".__("tech acc")."\" ";
    $case_specific_lu = "CASE WHEN ".$conf["query_ct_where"]." THEN 'Non' END";
}
$champAffiche[] = $case_specific_lu." as \"".__("lu")."\" ";
$options[] = array(
    "type" => "condition",
    "field" => $case_specific_lu,
    "case" => array(
        "0" => array(
            "values" => array("Non", ),
            "style" => "non_lu",
        ),
    ),
);
