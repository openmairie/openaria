<?php
/**
 * TAB_EXPORT_CSV - etablissement.
 *
 * @package openaria
 * @version SVN : $Id$
 */

// On surcharge le tableau standard pour supprimer les balises html utiles pour l'affichage HTML
// mais polluantes pour l'export CSV
$champAffiche = array(
    'etablissement.etablissement as "'.__("id").'"',
    'etablissement.code as "'.__("code").'"',
    'etablissement.libelle as "'.__("libelle").'"',
    'trim(concat(
        etablissement.adresse_numero, 
        \' \', etablissement.adresse_numero2, \' \', voie.libelle,
        \' \', etablissement.adresse_cp, \' \', etablissement.adresse_ville,
        CASE WHEN arrondissement.libelle is null
            THEN \'\'
            ELSE CONCAT(\' (\', arrondissement.libelle, \')\')
        END
    )) as "'.__("adresse").'"',
    'etablissement_nature.nature as "'.__("etablissement_nature").'"',
    'etablissement_type.libelle as "'.__("etablissement_type").'"',
    'etablissement_categorie.libelle as "'.__("etablissement_categorie").'"',
    'etablissement_statut_juridique.libelle as "'.__("etablissement_statut_juridique").'"',
    'etablissement_etat.statut_administratif as "'.__("etablissement_etat").'"',
    'to_char(etablissement.date_arrete_ouverture,\'DD/MM/YYYY\') as "'.__("date_arrete_ouverture").'"',
    'to_char(etablissement.si_prochaine_visite_date,\'DD/MM/YYYY\') as "'.__("si_prochaine_visite_date").'"',
    'to_char(si_prochaine_visite_periodique_date_previsionnelle,\'DD/MM/YYYY\') as "'.__("si_prochaine_visite_periodique_date_previsionnelle").'"',
    'to_char(etablissement.om_validite_debut,\'DD/MM/YYYY\') as "'.__("date de création de la fiche").'"',
    $case_validite.' as "'.__("archive").'"',
);
