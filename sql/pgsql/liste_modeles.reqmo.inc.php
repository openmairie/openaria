<?php
//$Id$ 
//gen openMairie le 14/02/2015 06:38

// base générable depuis le générateur

//en -tête
$reqmo['libelle'] = __('liste_modeles');
$reqmo['reqmo_libelle'] = __('liste_modeles');
$ent = __('liste_modeles');

// requête d'export avec champs masquables entre crochets

$reqmo['sql'] = "SELECT categorie.code as categorie_code, categorie.libelle as categorie_lib "
.",coalesce(service.code,'ACC & SI') as type_service, type.code as type_code, type.libelle as type_lib "
.",modele.code as modele_code, modele.libelle as modele_lib, modele.description as modele_desc "
.",lettretype.id as lettre_type_code, lettretype.libelle as lettre_type_lib "
."FROM ".DB_PREFIXE."courrier_type_categorie categorie "
."INNER JOIN ".DB_PREFIXE."courrier_type type ON categorie.courrier_type_categorie =type.courrier_type_categorie "
."INNER JOIN ".DB_PREFIXE."modele_edition modele ON type.courrier_type = modele.courrier_type "
."LEFT OUTER JOIN ".DB_PREFIXE."om_lettretype lettretype ON modele.om_lettretype_id = lettretype.id "
."LEFT OUTER JOIN ".DB_PREFIXE."service ON type.service = service.service "
."WHERE  coalesce(lettretype.actif,true) "
."ORDER BY coalesce(service.code,'ACC & SI'), categorie.code, type.code, modele.code, lettretype.id ";
