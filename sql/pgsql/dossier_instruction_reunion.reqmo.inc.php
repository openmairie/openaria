<?php
/**
 * @package openaria
 * @version SVN : $Id$
 */

//
$reqmo['libelle']=__('reqmo-libelle-dossier_instruction_reunion');
$reqmo['reqmo_libelle']=__('reqmo-libelle-dossier_instruction_reunion');
$ent=__('dossier_instruction_reunion');
// Requête
$reqmo['sql']="SELECT
    dossier_instruction_reunion.dossier_instruction_reunion as dossier_instruction_reunion,
    dossier_instruction.libelle as dossier_instruction,
    reunion_type.libelle as reunion_type,
    reunion_categorie.libelle as reunion_type_categorie,
    dossier_instruction_reunion.date_souhaitee as date_souhaitee,
    dossier_instruction_reunion.motivation as motivation,
    reunion_avis_proposition.libelle as proposition_avis,
    dossier_instruction_reunion.proposition_avis_complement as proposition_avis_complement,
    reunion.libelle as reunion,
    dossier_instruction_reunion.ordre as ordre,
    reunion_avis_rendu.libelle as avis,
    dossier_instruction_reunion.avis_complement as avis_complement,
    dossier_instruction_reunion.avis_motivation as avis_motivation
FROM ".DB_PREFIXE."dossier_instruction_reunion
LEFT OUTER JOIN ".DB_PREFIXE."dossier_instruction ON
dossier_instruction_reunion.dossier_instruction=dossier_instruction.dossier_instruction
LEFT OUTER JOIN ".DB_PREFIXE."reunion_type ON
dossier_instruction_reunion.reunion_type=reunion_type.reunion_type
LEFT OUTER JOIN ".DB_PREFIXE."reunion_categorie ON
dossier_instruction_reunion.reunion_type_categorie=reunion_categorie.reunion_categorie
LEFT OUTER JOIN ".DB_PREFIXE."reunion_avis as reunion_avis_proposition ON
dossier_instruction_reunion.proposition_avis=reunion_avis_proposition.reunion_avis
LEFT OUTER JOIN ".DB_PREFIXE."reunion ON
dossier_instruction_reunion.reunion=reunion.reunion
LEFT OUTER JOIN ".DB_PREFIXE."reunion_avis as reunion_avis_rendu ON
dossier_instruction_reunion.avis=reunion_avis_rendu.reunion_avis
ORDER BY [tri]";
// Possibilités de tri
$reqmo['tri'] = array(
    'dossier_instruction_reunion',
    'dossier_instruction',
    'reunion_type',
    'reunion_type_categorie',
    'date_souhaitee',
    'motivation',
    'proposition_avis',
    'proposition_avis_complement',
    'reunion',
    'ordre',
    'avis',
    'avis_complement',
    'avis_motivation',
);
