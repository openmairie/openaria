<?php
/**
 * @package openaria
 * @version SVN : $Id$
 */

//
include "../sql/pgsql/dossier_coordination_message.inc.php";

// Fil d'Ariane
$ent = __("dossiers")." -> ".__("Messages")." -> ".__("Tous les messages");

//
$champAffiche = array_merge(
    $champaffiche_message_infos,
    $champaffiche_dc_libelle,
    $champaffiche_dossier_ads,
    $champaffiche_etablissement,
    array(
        $case_si_cadre_lu." as \"".__("cadre si")."\"",
        $case_si_technicien_lu." as \"".__("tech si")."\"",
        $case_acc_cadre_lu." as \"".__("cadre acc")."\"",
        $case_acc_technicien_lu." as \"".__("tech acc")."\"",
    )
);

// Options
if (!isset($options)) {
    $options = array();
}
// Recherche avancée
$options[] = array(
    'type' => 'search',
    'display' => true,
    'advanced'  => array(
        'type' => array(
            'table' => 'dossier_coordination_message',
            'colonne' => 'type',
            'type' => 'text',
            'taille' => 100,
            'max' => 100,
            'libelle' => __('type'),
        ),
        'emetteur' => array(
            'table' => 'dossier_coordination_message',
            'colonne' => 'emetteur',
            'type' => 'text',
            'taille' => 40,
            'max' => 100,
            'libelle' => __('émetteur'),
        ),
        'categorie' => array(
            'table' => 'dossier_coordination_message',
            'colonne' => 'categorie',
            'type' => 'select',
            'libelle' => __('catégorie'),
            'subtype' => "manualselect",
            'args' => array(
                0 => array("", "interne", "entrant", "sortant", ),
                1 => array(__("Toutes"), "interne", "entrant", "sortant", ),
            ),
        ),
    ),
    'default_form'  => 'advanced',
    'absolute_object' => 'dossier_coordination_message'
);
