<?php
/**
 * @package openaria
 * @version SVN : $Id$
 */

include "../sql/pgsql/app_om_tab_common_select.inc.php";
include "../gen/sql/pgsql/dossier_instruction.inc.php";

// Titre de la page
$ent = __("dossiers")." -> ".__("DI")." -> ".__("tous les dossiers");

// Complément pour le FROM
$table = DB_PREFIXE."dossier_instruction
    LEFT JOIN ".DB_PREFIXE."analyses
        ON dossier_instruction.dossier_instruction = analyses.dossier_instruction
    LEFT JOIN ".DB_PREFIXE."dossier_coordination
        ON dossier_instruction.dossier_coordination = dossier_coordination.dossier_coordination
    LEFT JOIN ".DB_PREFIXE."dossier_coordination_type
        ON dossier_coordination.dossier_coordination_type = dossier_coordination_type.dossier_coordination_type
    LEFT JOIN ".DB_PREFIXE."dossier_type
        ON dossier_coordination_type.dossier_type = dossier_type.dossier_type
    LEFT JOIN ".DB_PREFIXE."autorite_competente 
        ON dossier_instruction.autorite_competente=autorite_competente.autorite_competente 
    LEFT JOIN ".DB_PREFIXE."service 
        ON dossier_instruction.service=service.service 
    LEFT JOIN ".DB_PREFIXE."acteur 
        ON dossier_instruction.technicien=acteur.acteur
    LEFT JOIN ".DB_PREFIXE."etablissement 
        ON dossier_coordination.etablissement=etablissement.etablissement
    LEFT JOIN ".DB_PREFIXE."arrondissement 
        ON etablissement.adresse_arrondissement=arrondissement.arrondissement 
    LEFT JOIN ".DB_PREFIXE."voie 
        ON etablissement.adresse_voie=voie.voie 
 ";

//
$tab_title = __("DI");

/**
 * Gestion SOUSFORMULAIRE => $sousformulaire
 */
$sousformulaire = array(
    'analyses',
    'proces_verbal',
    'piece',
    'courrier',
    'dossier_instruction_reunion_contexte_di',
    'visite',
);
if ($f->is_option_referentiel_ads_enabled() === true) {
    $sousformulaire[] = 'dossier_coordination_message_contexte_di';
}
//
$sousformulaire_parameters = array(
    "dossier_instruction_reunion_contexte_di" => array(
        "title" => __("reunions"),
    ),
    "analyses" => array(
        "title" => __("analyse"),
        "href" => OM_ROUTE_FORM."&obj=dossier_instruction&action=7&idx=".((isset($idx))?$idx:"")."&",
        //"href" => OM_ROUTE_SOUSFORM."&obj=analyses&action=4&objsf=".$obj.((isset($retourformulaire))?"&retourformulaire=".$retourformulaire:"")."&idxformulaire=".$idx."&retour=form&idx=]&",
    ),
    "proces_verbal" => array(
        "title" => __("PV"),
        "href" => OM_ROUTE_FORM."&obj=dossier_instruction&action=9&idx=".((isset($idx))?$idx:"")."&",
    ),
    "piece" => array(
        "title" => __("Documents entrants"),
        "href" => OM_ROUTE_FORM."&obj=".(isset($obj) ? $obj : "dossier_instruction")."&action=21&idx=".((isset($idx))?$idx:"")."&",
    ),
    "courrier" => array(
        "title" => __("Documents generes"),
    ),
    "visite" => array(
        "title" => __("Visites"),
    ),
    "dossier_coordination_message_contexte_di" => array(
        "title" => __("Messages"),
    ),
);

// SELECT
$champaffiche_di_id = array(
    "dossier_instruction.dossier_instruction as \"".__("id")."\"",
);
$champaffiche_di_libelle = array(
    "dossier_instruction.libelle as \"".__("DI")."\"",
);
$champaffiche_etablissement = array($select__etab_label__column__as, );
$champaffiche_etablissement_adresse = array(
    "CONCAT(etablissement.adresse_numero, ' ', etablissement.adresse_numero2, ' ', voie.libelle, ' ', etablissement.adresse_cp, ' (', arrondissement.libelle, ')') as \"".__("adresse")."\"",
);
$champaffiche_di_technicien = array(
    "CASE WHEN acteur.acronyme <> ''
        THEN CONCAT('<span title=''', acteur.nom_prenom, '''>', acteur.acronyme, '</span>')
        ELSE acteur.nom_prenom
    END as \"".__("technicien")."\"",
);
$champaffiche_di_marqueur_a_qualifier = array(
    "CASE dossier_instruction.a_qualifier WHEN 't' THEN 'Oui' ELSE 'Non' END as \"".__("a_qualifier")."\"",
);
$champaffiche_di_marqueur_cloture_case = "CASE dossier_instruction.dossier_cloture WHEN 't' THEN 'Oui' ELSE 'Non' END";
$champaffiche_di_marqueur_cloture = array(
    $champaffiche_di_marqueur_cloture_case." as \"".__("dossier_cloture")."\"",
);
$champaffiche_di_marqueur_incompletude = array(
    "CASE dossier_instruction.incompletude WHEN 't' THEN 'Oui' ELSE 'Non' END as \"".__("incompletude")."\"",
);
$champaffiche_dossier_ads  = array();
if (isset($f) === true
    && $f->is_option_display_ads_field_in_tables_enabled() === true) {
    //
    $champaffiche_dossier_ads = array(
        "CASE 
            WHEN dossier_coordination.dossier_autorisation_ads IS NULL AND dossier_coordination.dossier_instruction_ads IS NOT NULL
                THEN dossier_coordination.dossier_instruction_ads
            WHEN dossier_coordination.dossier_autorisation_ads IS NOT NULL AND dossier_coordination.dossier_instruction_ads IS NOT NULL
                THEN dossier_coordination.dossier_instruction_ads
            ELSE 
                dossier_coordination.dossier_autorisation_ads
        END as \"".__("dossier ADS")."\"",
    );
}
$champAffiche = array_merge(
    $champaffiche_di_id,
    $champaffiche_di_libelle,
    $champaffiche_dossier_ads,
    $champaffiche_etablissement,
    $champaffiche_etablissement_adresse,
    array(
        'to_char(dossier_coordination.date_demande ,\'DD/MM/YYYY\') as "'.__("date_demande").'"',
        'to_char(dossier_coordination.date_butoir ,\'DD/MM/YYYY\') as "'.__("date_butoir").'"',
        'autorite_competente.code as "'.__("autorite_competente").'"',
    ),
    $champaffiche_di_technicien,
    $champaffiche_di_marqueur_a_qualifier,
    $champaffiche_di_marqueur_incompletude,
    array(
        'analyses.analyses_etat as "'.__("analyses_etat").'"',
    ),
    $champaffiche_di_marqueur_cloture
);

//
$champRecherche= array_merge(
    $champaffiche_di_libelle,
    $champaffiche_dossier_ads,
    $champaffiche_etablissement
);

// Recherche avancée
if (!isset($options)) {
    $options = array();
}

// Options pour les select des booléens
$args = array(
    0 => array("", "t", "f", ),
    1 => array(__("choisir"), __("Oui"), __("Non"), ),
);

//
$champs['dossier'] = array(
    'table' => 'dossier_instruction',
    'colonne' => 'libelle',
    'type' => 'text',
    'taille' => 30,
    'max' => 100,
    'libelle' => __('DI')
);
//
$champs['etablissement'] = array(
    'table' => 'etablissement',
    'colonne' => array('libelle','code'),
    'type' => 'text',
    'taille' => 100,
    'max' => 100,
    'libelle' => __('etablissement')
);

$champs['adresse_numero'] = array(
    'table' => 'etablissement',
    'colonne' => 'adresse_numero',
    'type' => 'text',
    'taille' => 40,
    'max' => 100,
    'libelle' => __('numero')
);
$champs['adresse_voie'] = array(
    'table' => 'voie',
    'colonne' => 'libelle',
    'type' => 'text',
    'taille' => 40,
    'max' => 100,
    'libelle' => __('voie')
);
$champs['adresse_arrondissement'] = array(
    'table' => 'arrondissement',
    'colonne' => 'arrondissement',
    'libelle' => __('arrondissement'),
    'type' => 'select',
    'subtype' => 'sqlselect',
    'sql' => 'SELECT arrondissement, libelle FROM '.DB_PREFIXE.'arrondissement ORDER BY code::int' ,
);
$champs['autorite_competente'] = array(
    'table' => 'dossier_instruction',
    'colonne' => 'autorite_competente',
    'type' => 'select',
    'libelle' => __('autorite_competente')
);
// Si l'utilisateur n'a pas de service alors on lui autorise
// à filtrer par service
if (is_null($_SESSION["service"])) {
    //
    $champs['service'] = array(
        'table' => 'dossier_instruction',
        'colonne' => 'service',
        'type' => 'select',
        'libelle' => __('service')
    );
}
$champs['technicien'] = array(
    'table' => 'acteur',
    'colonne' => 'acteur',
    'type' => 'select',
    'subtype' => 'sqlselect',
    // QUERY liste des acteurs qui peuvent instruire un dossier
    'sql' => '
    SELECT 
        acteur,
        concat (nom_prenom, \' (\', acronyme, \')\')
    FROM
        '.DB_PREFIXE.'acteur
    WHERE
        (acteur.role=\'technicien\'
         OR acteur.role=\'cadre\')
        '.(!is_null($_SESSION["service"]) ? 'AND acteur.service=\''.$_SESSION["service"].'\'' : ''),
    'libelle' => __('technicien')
);
$champs['a_affecter'] = array(
    'table' => 'CASE WHEN dossier_instruction',
    'colonne' => ' technicien IS NULL THEN \'t\' ELSE \'f\' END',
    'type' => 'select',
    "subtype" => "manualselect",
    'libelle' => __('à affecter'),
    "args" => $args,
);
$champs['a_qualifier'] = array(
    'table' => 'dossier_instruction',
    'colonne' => 'a_qualifier',
    'type' => 'select',
    "subtype" => "manualselect",
    'libelle' => __('a_qualifier'),
    "args" => $args,
);
$champs['analyses_etat'] = array(
    'table' => 'analyses',
    'colonne' => 'analyses_etat',
    'type' => 'select',
    "subtype" => "manualselect",
    'libelle' => __('analyses_etat'),
    "args" => array(
        0 => array("", "en_cours", "termine", "valide", "acte"),
        1 => array(__("choisir"), "en_cours", "termine", "valide", "acte"),
    ),
);
$champs['incompletude'] = array(
    'table' => 'dossier_instruction',
    'colonne' => 'incompletude',
    'type' => 'select',
    "subtype" => "manualselect",
    'libelle' => __('incompletude'),
    "args" => $args,
);
$champs['dossier_cloture'] = array(
    'table' => 'dossier_instruction',
    'colonne' => 'dossier_cloture',
    'type' => 'select',
    "subtype" => "manualselect",
    'libelle' => __('dossier_cloture'),
    "args" => $args,
);
$champs['dossier_ads'] = array(
    'table' => 'dossier_coordination',
    'colonne' => array('dossier_instruction_ads', 'dossier_autorisation_ads', ),
    'type' => 'text',
    'taille' => 30,
    'max' => 100,
    'libelle' => __('dossier ADS'),
);
$options["advsearch"] = array(
    'type' => 'search',
    'display' => true,
    'advanced'  => $champs,
    'default_form'  => 'advanced',
    'absolute_object' => 'dossier_instruction'
);

// Il n'est pas possible d'ajouter un DI manuellement
if (isset($tab_actions) === true
    && is_array($tab_actions) === true
    && array_key_exists("corner", $tab_actions) === true
    && is_array($tab_actions["corner"]) === true
    && array_key_exists("ajouter", $tab_actions["corner"]) === true) {
    //
    unset($tab_actions["corner"]["ajouter"]);
}

// Affichage du bouton de redirection vers le SIG externe si configuré
if ($f->getParameter('option_sig') == 'sig_externe'
    && ($obj == 'dossier_instruction'
        || $obj == 'dossier_instruction_mes_plans'
        || $obj == 'dossier_instruction_tous_plans'
        || $obj == 'dossier_instruction_mes_visites'
        || $obj == 'dossier_instruction_tous_visites'
        || $obj == 'dossier_instruction_a_affecter'
        || $obj == 'dossier_instruction_a_qualifier')
    ) {
    $inst_geoaria = $f->get_inst_geoaria();
    if ($inst_geoaria->is_localization_dc_enabled() === true) {
        $tab_actions['left']["localiser-sig-externe"] = array(
            'lien' => OM_ROUTE_FORM.'&obj=dossier_instruction&amp;action=30&amp;idx=',
            'id' => '',
            'lib' => '<span class="om-icon om-icon-16 om-icon-fix sig-16" title="'.__('Localiser').'">'.__('Localiser').'</span>',
            'rights' => array('list' => array('dossier_coordination', 'dossier_coordination_consulter'), 'operator' => 'OR'),
            'ordre' => 20,
            'target' => "blank",
            'ajax' => false,
        );
    }
}

// Option condition clôture : permet d'ajouter la classe css di-cloture
// sur chaque enregistrement qui est clôturé.
$option_condition_cloture = array(
    "type" => "condition",
    "field" => $champaffiche_di_marqueur_cloture_case,
    "case" => array(
        array(
            "values" => array("Oui", ),
            "style" => "di-cloture",
        ),
    ),
);
$options[] = $option_condition_cloture;

// Filtre sur le service de l'utilisateur
include "../sql/pgsql/filter_service.inc.php";
