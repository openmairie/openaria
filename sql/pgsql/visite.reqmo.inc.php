<?php
/**
 * @package openaria
 * @version SVN : $Id$
 */

//
$reqmo['libelle']=__('reqmo-libelle-visite');
$reqmo['reqmo_libelle']=__('reqmo-libelle-visite');
$ent=__('visite');
$reqmo['sql']="select visite, visite_motif_annulation, visite_etat, dossier_instruction, acteur, programmation, date_creation, date_annulation, observation, programmation_version_creation, programmation_version_annulation, heure_debut, heure_fin, convocation_exploitants, date_visite, a_poursuivre, courrier_convocation_exploitants, courrier_annulation, programmation_version_modification from ".DB_PREFIXE."visite  order by [tri]";
$reqmo['tri']=array('visite','visite_motif_annulation','visite_etat','dossier_instruction','acteur','programmation','date_creation','date_annulation','observation','programmation_version_creation','programmation_version_annulation','heure_debut','heure_fin','convocation_exploitants','date_visite','a_poursuivre','courrier_convocation_exploitants','courrier_annulation','programmation_version_modification');
