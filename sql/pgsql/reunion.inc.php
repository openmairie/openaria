<?php
/**
 * @package openaria
 * @version SVN : $Id$
 */

//
include "../gen/sql/pgsql/reunion.inc.php";

// Fil d'ariane
$ent = __("suivi")." -> ".__("reunions")." -> ".__("gestion");

//
$champRecherche[] = "case reunion.reunion_cloture when 't' then 'Oui' else 'Non' end as \"".__("reunion_cloture")."\"";

//
$tri = " ORDER BY reunion.date_reunion DESC NULLS LAST ";

//
$sousformulaire = array(
    "lien_reunion_r_instance_r_i_membre",
);

//
$sousformulaire_parameters = array(
    "lien_reunion_r_instance_r_i_membre" => array(
        "title" => __("signataires"),
    ),
);

// Filtre sur le service de l'utilisateur
$table .= " LEFT JOIN ".DB_PREFIXE."service 
                ON service.service=reunion_type.service ";
include "../sql/pgsql/filter_service.inc.php";
