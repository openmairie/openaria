<?php
/**
 * TAB_EXPORT_CSV - etablissement_referentiel_erp.
 *
 * Point d'entrée export CSV pour le listing des établissements.
 *
 * @package openaria
 * @version SVN : $Id$
 */

include "../sql/pgsql/etablissement.export_csv.inc.php";
