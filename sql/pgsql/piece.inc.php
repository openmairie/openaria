<?php
/**
 * @package openaria
 * @version SVN : $Id$
 */

include "../sql/pgsql/app_om_tab_common_select.inc.php";
include('../gen/sql/pgsql/piece.inc.php');

// Fil d'Ariane
$ent = __("suivi")." -> ".__("documents entrants")." -> ".__("tous les documents");
if (in_array($retourformulaire, $foreign_keys_extended["etablissement"])
    || in_array($retourformulaire, $foreign_keys_extended["dossier_coordination"])
    || in_array($retourformulaire, $foreign_keys_extended["dossier_instruction"])) {
    $ent = " -> ".__("document entrant");
}

$select__etab_code__column = sprintf(
    'CONCAT(
        \'%1$s\',
        etablissement.code,
        \'%2$s\'
    )',
    (isset($f) === true ? sprintf(
        '<span id="%1$s\', LPAD(replace(etablissement.code, \'%1$s\', \'\'), %2$s, \'0\'), \'">',
        $f->getParameter("etablissement_code_prefixe"),
        25 - (strlen($f->getParameter("etablissement_code_prefixe")))
    ) : ""),
    (isset($f) === true ? sprintf('</span>') : "")
);
$select__etab_code__column__as = $select__etab_code__column." as \""._("code")."\"";


// Définition des variables pour la customisation du listing
$select__di_or_dc_label__column = "CASE WHEN dossier_instruction.libelle <> '' THEN dossier_instruction.libelle ELSE dossier_coordination.libelle END";
$select__di_or_dc_label__column__as = $select__di_or_dc_label__column." as \""._("di ou dc")."\"";
$select__docent_lu__column = "CASE 
    WHEN piece.dossier_instruction IS NOT NULL AND piece.lu = 't' 
        THEN 'Oui'
    WHEN piece.dossier_instruction IS NOT NULL AND piece.lu <> 't'
        THEN 'Non' 
    ELSE '' END";
$select__docent_lu__column__as = $select__docent_lu__column." as \""._("lu")."\"";
$select__docent_suivi__column = "CASE piece.suivi WHEN 't' THEN 'Oui' ELSE 'Non' END";
$select__docent_suivi__column__as = $select__docent_suivi__column." as \""._("suivi")."\"";

// SELECT
$champAffiche = array(
    'piece.piece as "'.__("id").'"',
    'piece.nom as "'.__("nom").'"',
    'piece_type.libelle as "'.__("piece_type").'"',
    'to_char(piece.date_reception ,\'DD/MM/YYYY\') as "'.__("date_reception").'"',
    'to_char(piece.date_emission ,\'DD/MM/YYYY\') as "'.__("date_emission").'"',
    $select__etab_label__column__as,
    $select__di_or_dc_label__column__as,
    'piece_statut.libelle as "'.__("piece_statut").'"',
    $select__docent_lu__column__as,
    $select__docent_suivi__column__as,
    'to_char(piece.date_butoir ,\'DD/MM/YYYY\') as "'.__("date_butoir").'"',
    'to_char(piece.om_date_creation ,\'DD/MM/YYYY\') as "'.__("om_date_creation").'"',
);
$champRecherche = array(
    'piece.piece as "'.__("id").'"',
    'piece.nom as "'.__("nom").'"',
    'piece_type.libelle as "'.__("piece_type").'"',
    $select__etab_label__column__as,
    $select__di_or_dc_label__column__as,
);
//
$tab_title = __("document entrant");

// Actions a gauche : consulter
$tab_actions['left']['download'] = array(
    'lien' => OM_ROUTE_FORM.'&snippet=file&obj='.$obj.'&champ=uid&id=',
    'id' => '',
    'lib' => '<span class="om-icon om-icon-16 om-icon-fix download-16" title="'.__('Télécharger').'">'.__('Télécharger').'</span>',
    'rights' => array('list' => array($obj, $obj.'_uid_telecharger'), 'operator' => 'OR'),
    'ordre' => 15,
    'target' => '_blank',
);

$tri = " ORDER BY piece.date_reception DESC NULLS LAST, piece.date_emission DESC NULLS LAST , piece.nom ";

// Dans le contexte de l'établissement, la colonne 'etablissement' est inutile
if (isset($retourformulaire)
    && in_array($retourformulaire, $foreign_keys_extended["etablissement"])) {
    $champAffiche = array_diff(
        $champAffiche,
        array($select__etab_label__column__as, )
    );
    $champRecherche = array_diff(
        $champRecherche,
        array($select__etab_label__column__as, )
    );
}

// Si contexte DI
if (in_array($retourformulaire, $foreign_keys_extended["dossier_coordination"])) {
    $champAffiche = array_diff(
        $champAffiche,
        array($select__etab_label__column__as, )
    );
    $champRecherche = array_diff(
        $champRecherche,
        array($select__etab_label__column__as, )
    );
}
// Si contexte DI
if (in_array($retourformulaire, $foreign_keys_extended["dossier_instruction"])) {
    $champAffiche = array_diff(
        $champAffiche,
        array($select__etab_label__column__as, $select__di_or_dc_label__column__as, )
    );
    $champRecherche = array_diff(
        $champRecherche,
        array($select__etab_label__column__as, $select__di_or_dc_label__column__as, )
    );
    // Instanciation du DI ou de sa surcharge
    if (isset($f)) {
        $di = $f->get_inst__om_dbform(array(
            "obj" => $retourformulaire,
            "idx" => $idxformulaire,
        ));
        // Si service DI différent de celui de l'utilisateur
        if ($di->is_from_good_service() === false) {
            // Désactivation du bouton ajout du soustab
            if (isset($tab_actions) === true
                && is_array($tab_actions) === true
                && array_key_exists("corner", $tab_actions) === true
                && is_array($tab_actions["corner"]) === true
                && array_key_exists("ajouter", $tab_actions["corner"]) === true) {
                //
                unset($tab_actions["corner"]["ajouter"]);
            }
        }
    }
}

// Gestion des options
if (!isset($options)) {
    $options = array();
}

// Option condition non lu : permet d'ajouter la classe css document-entrant-non-lu
// sur chaque enregistrement qui est non lu.
$option_condition_non_lu = array(
    "type" => "condition",
    "field" => $select__docent_lu__column,
    "case" => array(
        array(
            "values" => array("Non", ),
            "style" => "document-entrant-non-lu",
        ),
    ),
);
$options[] = $option_condition_non_lu;

// Option recherche avancée
$champs['nom'] = array(
    'table' => 'piece',
    'colonne' => array('nom'),
    'type' => 'text',
    'taille' => 100,
    'max' => 100,
    'libelle' => __('nom')
);
$champs['piece_type'] = array(
    'table' => 'piece',
    'colonne' => 'piece_type',
    'type' => 'select',
    'libelle' => __('type')
);
$champs['date_reception'] = array(
    'table' => 'piece',
    'colonne' => 'date_reception',
    'libelle' => __('date_reception'),
    'lib1'=> __("debut"),
    'lib2' => __("fin"),
    'type' => 'date',
    'taille' => 8,
    'where' => 'intervaldate'
);
$champs['date_emission'] = array(
    'table' => 'piece',
    'colonne' => 'date_emission',
    'libelle' => __('date_emission'),
    'lib1'=> __("debut"),
    'lib2' => __("fin"),
    'type' => 'date',
    'taille' => 8,
    'where' => 'intervaldate'
);
$champs['etablissement'] = array(
    'table' => 'etablissement',
    'colonne' => array('libelle','code'),
    'type' => 'text',
    'taille' => 100,
    'max' => 100,
    'libelle' => __('etablissement')
);
$champs['dc'] = array(
    'table' => 'dossier_coordination',
    'colonne' => array('libelle','code'),
    'type' => 'text',
    'taille' => 100,
    'max' => 100,
    'libelle' => __('dc')
);
$champs['di'] = array(
    'table' => 'dossier_instruction',
    'colonne' => array('libelle','code'),
    'type' => 'text',
    'taille' => 100,
    'max' => 100,
    'libelle' => __('di')
);
$champs['piece_statut'] = array(
    'table' => 'piece',
    'colonne' => 'piece_statut',
    'type' => 'select',
    'libelle' => __('statut')
);
$args = array(
    0 => array("", "t", "f", ),
    1 => array(__("choisir"), __("Oui"), __("Non"), ),
);
$champs['lu'] = array(
    'table' => 'CASE WHEN piece',
    'colonne' => ' dossier_instruction IS NOT NULL AND piece.lu IS FALSE THEN \'f\' WHEN piece.dossier_instruction IS NOT NULL AND piece.lu IS TRUE THEN \'t\' ELSE \'\' END',
    'type' => 'select',
    "subtype" => "manualselect",
    'libelle' => __('lu ?'),
    "args" => $args,
);
$champs['suivi'] = array(
    'table' => 'piece',
    'colonne' => 'suivi',
    'type' => 'select',
    "subtype" => "manualselect",
    'libelle' => __('suivi ?'),
    "args" => $args,
);
$champs['date_butoir'] = array(
    'table' => 'piece',
    'colonne' => 'date_butoir',
    'libelle' => __('date_butoir'),
    'lib1'=> __("debut"),
    'lib2' => __("fin"),
    'type' => 'date',
    'taille' => 8,
    'where' => 'intervaldate'
);
$champs['date_creation'] = array(
    'table' => 'piece',
    'colonne' => 'om_date_creation',
    'libelle' => __('date_creation'),
    'lib1'=> __("debut"),
    'lib2' => __("fin"),
    'type' => 'date',
    'taille' => 8,
    'where' => 'intervaldate'
);
// Si l'utilisateur n'a pas de service alors on lui autorise
// à filtrer par service
if (is_null($_SESSION["service"])) {
    $champs['service'] = array(
        'table' => 'piece',
        'colonne' => 'service',
        'type' => 'select',
        'libelle' => __('service')
    );
}
$options["advsearch"] = array(
    'type' => 'search',
    'display' => true,
    'advanced'  => $champs,
    'default_form'  => 'advanced',
    'absolute_object' => 'piece'
);

// Filtre sur le service de l'utilisateur
include "../sql/pgsql/filter_service.inc.php";
