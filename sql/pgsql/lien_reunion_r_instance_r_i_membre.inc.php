<?php
/**
 * @package openaria
 * @version SVN : $Id$
 */

//
include "../gen/sql/pgsql/lien_reunion_r_instance_r_i_membre.inc.php";

// En sous-formulaire de réunion
if (!empty($retourformulaire)
    && $retourformulaire == 'reunion') {
    // Cache le bouton ajouter
    if (isset($tab_actions) === true
        && is_array($tab_actions) === true
        && array_key_exists("corner", $tab_actions) === true
        && is_array($tab_actions["corner"]) === true
        && array_key_exists("ajouter", $tab_actions["corner"]) === true) {
        //
        unset($tab_actions["corner"]["ajouter"]);
    }
    // On supprime la colonne réunion inutile dans ce contexte
    $champAffiche = array(
    'lien_reunion_r_instance_r_i_membre.lien_reunion_r_instance_r_i_membre as "'.__("lien_reunion_r_instance_r_i_membre").'"',
    'reunion_instance.libelle as "'.__("instance").'"',
    'reunion_instance_membre.membre as "'.__("membre").'"',
    'lien_reunion_r_instance_r_i_membre.observation as "'.__("observation").'"',
    );
    //
    $tri = " ORDER BY reunion_instance.ordre ASC ";
}
