<?php
/**
 * @package openaria
 * @version SVN : $Id$
 */

//
include "../gen/sql/pgsql/reunion_instance.inc.php";

// Aucun onglet
$sousformulaire = array(
    "reunion_instance_membre",
);

// Aucun onglet
$sousformulaire_parameters = array(
    "reunion_instance_membre" => array(
        "title" => __("membres"),
    ),
);

// Filtre sur le service de l'utilisateur
include "../sql/pgsql/filter_service.inc.php";
