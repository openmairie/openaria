<?php
/**
 * Surcharge de la classe piece pour afficher seulement les pièces qui sont non
 * lu, c'est-à-dire dont le champ lu est à false.
 *
 * @package openaria
 * @version SVN : $Id$
 */

include('../sql/pgsql/dossier_instruction.inc.php');

// Fil d'ariane
$ent = __("dossiers")." -> ".__("documents entrants")." -> ".__("mes non lus");

// Titre de l'onglet
$tab_title = __("Document entrant");

// SELECT 
$champAffiche = array(
    'dossier_instruction.dossier_instruction as "'.__("id").'"',
    'dossier_instruction.libelle as "'.__("DI").'"',
    $select__etab_label__column__as,
    'concat(etablissement.adresse_numero, 
        \' \', etablissement.adresse_numero2, \' \', voie.libelle,
        \' \', etablissement.adresse_cp, \' (\', arrondissement.libelle,\')\')
        as "'.__("adresse").'"',
    'piece.nom as "'.__("nom").'"',
    'to_char(piece.om_date_creation ,\'DD/MM/YYYY\') as "'.__("om_date_creation").'"',
    'to_char(piece.date_butoir ,\'DD/MM/YYYY\') as "'.__("date_butoir").'"',
    );
//
$champRecherche = array();

// FROM
$table .= " 
    LEFT JOIN ".DB_PREFIXE."piece
        ON piece.dossier_instruction = dossier_instruction.dossier_instruction
    LEFT JOIN ".DB_PREFIXE."om_utilisateur
        ON acteur.om_utilisateur = om_utilisateur.om_utilisateur ";

// WHERE
$selection = " WHERE piece.lu = false 
        AND om_utilisateur.login = '".$_SESSION["login"]."'";

// ORDER BY
$tri = " ORDER BY piece.date_butoir ";

//
$obj = "dossier_instruction_mes_visites";
// Cache le bouton ajouter
if (isset($tab_actions) === true
    && is_array($tab_actions) === true
    && array_key_exists("corner", $tab_actions) === true
    && is_array($tab_actions["corner"]) === true
    && array_key_exists("ajouter", $tab_actions["corner"]) === true) {
    //
    unset($tab_actions["corner"]["ajouter"]);
}
// Actions a gauche : consulter
$tab_actions['left']['consulter'] =
    array('lien' => OM_ROUTE_FORM.'&obj='.$obj.'&amp;action=3'.'&amp;idx=',
          'id' => '&amp;premier='.$premier.'&amp;advs_id='.$advs_id.'&amp;tricol='.$tricol.'&amp;valide='.$valide.'&amp;retour=tab',
          'lib' => '<span class="om-icon om-icon-16 om-icon-fix consult-16" title="'.__('Consulter').'">'.__('Consulter').'</span>',
          'rights' => array('list' => array($obj, $obj.'_consulter'), 'operator' => 'OR'),
          'ordre' => 10,);
// Action du contenu : consulter
$tab_actions['content'] = $tab_actions['left']['consulter'];
$obj = "piece_non_lu";

// Initialisation des options de l'écran de listing
$options = array();
// Désactivation de la recherche simple
$option = array(
  "type" => "search",
  "display" => false,
);
$options[] = $option;
