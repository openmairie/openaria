<?php
/**
 * @package openaria
 * @version SVN : $Id$
 */

//
$reqmo['libelle']=__('reqmo-libelle-etablissement');
$reqmo['reqmo_libelle']=__('reqmo-libelle-etablissement');
$ent=__('etablissement');
// Requête
$reqmo['sql']="SELECT
    concat(etablissement.code,' - ',etablissement.libelle) as libelle,
    etablissement.adresse_numero as adresse_numero,
    etablissement.adresse_numero2 as adresse_numero2,
    voie.libelle as adresse_voie,
    etablissement.adresse_complement as adresse_complement,
    etablissement.lieu_dit as lieu_dit,
    etablissement.boite_postale as boite_postale,
    etablissement.adresse_cp as adresse_cp,
    etablissement.adresse_ville as adresse_ville,
    arrondissement.libelle as adresse_arrondissement,
    etablissement.cedex as cedex,
    etablissement.npai as npai,
    etablissement.telephone as telephone,
    etablissement.fax as fax,
    etablissement_nature.nature as etablissement_nature,
    etablissement.siret as siret,
    etablissement.annee_de_construction as annee_de_construction,
    etablissement_statut_juridique.libelle as etablissement_statut_juridique,
    etablissement_tutelle_adm.libelle as etablissement_tutelle_adm,
    etablissement.ref_patrimoine as ref_patrimoine,
    etablissement_type.libelle as etablissement_type,
    etablissement_categorie.libelle as etablissement_categorie,
    etablissement_etat.statut_administratif as etablissement_etat,
    etablissement.date_arrete_ouverture as date_arrete_ouverture,
    etablissement.autorite_police_encours as autorite_police_encours,
    etablissement.om_validite_debut as om_validite_debut,
    etablissement.om_validite_fin as om_validite_fin,
    etablissement.si_effectif_public as si_effectif_public,
    etablissement.si_effectif_personnel as si_effectif_personnel,
    etablissement.si_locaux_sommeil as si_locaux_sommeil,
    si_periodicite_visites.periodicite as si_periodicite_visites,
    etablissement.si_prochaine_visite_periodique_date_previsionnelle as si_prochaine_visite_periodique_date_previsionnelle,
    si_visite_duree.libelle as si_visite_duree,
    etablissement.si_derniere_visite_periodique_date as si_derniere_visite_periodique_date,
    etablissement.si_derniere_visite_date as si_derniere_visite_date,
    si_derniere_visite_avis.libelle as si_derniere_visite_avis,
    si_derniere_visite_technicien.nom_prenom as si_derniere_visite_technicien,
    etablissement.si_prochaine_visite_date as si_prochaine_visite_date,
    si_prochaine_visite_type.libelle as si_prochaine_visite_type,
    etablissement.acc_derniere_visite_date as acc_derniere_visite_date,
    acc_derniere_visite_avis.libelle as acc_derniere_visite_avis,
    acc_derniere_visite_technicien.nom_prenom as acc_derniere_visite_technicien,
    etablissement.acc_consignes_om_html as acc_consignes_om_html,
    etablissement.acc_descriptif_om_html as acc_descriptif_om_html,
    etablissement.si_consignes_om_html as si_consignes_om_html,
    etablissement.si_descriptif_om_html as si_descriptif_om_html,
    si_autorite_competente_visite.libelle as si_autorite_competente_visite,
    si_autorite_competente_plan.libelle as si_autorite_competente_plan,
    si_dernier_plan_avis.libelle as si_dernier_plan_avis,
    etablissement.si_type_alarme as si_type_alarme,
    etablissement.si_type_ssi as si_type_ssi,
    etablissement.si_conformite_l16 as si_conformite_l16,
    etablissement.si_alimentation_remplacement as si_alimentation_remplacement,
    etablissement.si_service_securite as si_service_securite,
    etablissement.si_personnel_jour as si_personnel_jour,
    etablissement.si_personnel_nuit as si_personnel_nuit,
    etablissement.references_cadastrales as references_cadastrales
FROM ".DB_PREFIXE."etablissement
LEFT OUTER JOIN ".DB_PREFIXE."voie ON
etablissement.adresse_voie=voie.voie
LEFT OUTER JOIN ".DB_PREFIXE."arrondissement ON
etablissement.adresse_arrondissement=arrondissement.arrondissement
LEFT OUTER JOIN ".DB_PREFIXE."etablissement_nature ON
etablissement.etablissement_nature=etablissement_nature.etablissement_nature
LEFT OUTER JOIN ".DB_PREFIXE."etablissement_statut_juridique ON
etablissement.etablissement_statut_juridique=etablissement_statut_juridique.etablissement_statut_juridique
LEFT OUTER JOIN ".DB_PREFIXE."etablissement_tutelle_adm ON
etablissement.etablissement_tutelle_adm=etablissement_tutelle_adm.etablissement_tutelle_adm
LEFT OUTER JOIN ".DB_PREFIXE."etablissement_type ON
etablissement.etablissement_type=etablissement_type.etablissement_type
LEFT OUTER JOIN ".DB_PREFIXE."etablissement_categorie ON
etablissement.etablissement_categorie=etablissement_categorie.etablissement_categorie
LEFT OUTER JOIN ".DB_PREFIXE."etablissement_etat ON
etablissement.etablissement_etat=etablissement_etat.etablissement_etat
LEFT OUTER JOIN ".DB_PREFIXE."periodicite_visites as si_periodicite_visites ON
etablissement.si_periodicite_visites=si_periodicite_visites.periodicite_visites
LEFT OUTER JOIN ".DB_PREFIXE."reunion_avis as si_derniere_visite_avis ON
etablissement.si_derniere_visite_avis=si_derniere_visite_avis.reunion_avis
LEFT OUTER JOIN ".DB_PREFIXE."reunion_avis as acc_derniere_visite_avis ON
etablissement.acc_derniere_visite_avis=acc_derniere_visite_avis.reunion_avis
LEFT OUTER JOIN ".DB_PREFIXE."visite_duree as si_visite_duree ON
etablissement.si_visite_duree=si_visite_duree.visite_duree
LEFT OUTER JOIN ".DB_PREFIXE."acteur as si_derniere_visite_technicien ON
etablissement.si_derniere_visite_technicien=si_derniere_visite_technicien.acteur
LEFT OUTER JOIN ".DB_PREFIXE."acteur as acc_derniere_visite_technicien ON
etablissement.acc_derniere_visite_technicien=acc_derniere_visite_technicien.acteur
LEFT OUTER JOIN ".DB_PREFIXE."analyses_type as si_prochaine_visite_type ON
etablissement.si_prochaine_visite_type=si_prochaine_visite_type.analyses_type
LEFT OUTER JOIN ".DB_PREFIXE."reunion_avis as si_dernier_plan_avis ON
etablissement.si_dernier_plan_avis=si_dernier_plan_avis.reunion_avis
LEFT OUTER JOIN ".DB_PREFIXE."autorite_competente as si_autorite_competente_visite ON
etablissement.si_autorite_competente_visite=si_autorite_competente_visite.autorite_competente
LEFT OUTER JOIN ".DB_PREFIXE."autorite_competente as si_autorite_competente_plan ON
etablissement.si_autorite_competente_plan=si_autorite_competente_plan.autorite_competente
ORDER BY [tri]";
// Possibilités de tri
$reqmo['tri'] = array(
    'libelle',
    'adresse_numero',
    'adresse_numero2',
    'adresse_voie',
    'adresse_complement',
    'lieu_dit',
    'boite_postale',
    'adresse_cp',
    'adresse_ville',
    'adresse_arrondissement',
    'cedex',
    'npai',
    'telephone',
    'fax',
    'etablissement_nature',
    'siret',
    'annee_de_construction',
    'etablissement_statut_juridique',
    'etablissement_tutelle_adm',
    'ref_patrimoine',
    'etablissement_type',
    'etablissement_categorie',
    'etablissement_etat',
    'date_arrete_ouverture',
    'autorite_police_encours',
    'om_validite_debut',
    'om_validite_fin',
    'si_effectif_public',
    'si_effectif_personnel',
    'si_locaux_sommeil',
    'si_periodicite_visites',
    'si_prochaine_visite_periodique_date_previsionnelle',
    'si_visite_duree',
    'si_derniere_visite_periodique_date',
    'si_derniere_visite_date',
    'si_derniere_visite_avis',
    'si_derniere_visite_technicien',
    'si_prochaine_visite_date',
    'si_prochaine_visite_type',
    'acc_derniere_visite_date',
    'acc_derniere_visite_avis',
    'acc_derniere_visite_technicien',
    'acc_consignes_om_html',
    'acc_descriptif_om_html',
    'si_consignes_om_html',
    'si_descriptif_om_html',
    'si_autorite_competente_visite',
    'si_autorite_competente_plan',
    'si_dernier_plan_avis',
    'si_type_alarme',
    'si_type_ssi',
    'si_conformite_l16',
    'si_alimentation_remplacement',
    'si_service_securite',
    'si_personnel_jour',
    'si_personnel_nuit',
    'references_cadastrales',
    'geom_point',
    'geom_emprise',
);
