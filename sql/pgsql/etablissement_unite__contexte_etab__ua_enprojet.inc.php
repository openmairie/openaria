<?php
/**
 * Listing des UA en projet dans le contexte d'un établissement.
 *
 * @package openaria
 * @version SVN : $Id: etablissement_unite.inc.php 1172 2015-03-17 13:45:35Z nmeucci $
 */

//
include "../sql/pgsql/etablissement_unite.inc.php";

// Fil d'ariane
$ent = __("etablissements")." -> ".__("unites d'accessibilite");

//
$tab_title = __("UA");

//
$champAffiche = array_merge(
    $champAffiche_ua, 
    $champAffiche_cas1, 
    $champAffiche_derogation
);

// Filtre listing sous formulaire - etablissement
if (in_array($retourformulaire, $foreign_keys_extended["etablissement"])
    && isset($idx)) {
    $selection = " 
WHERE 
    (etablissement_unite.etablissement = '".$idx."') 
    AND etablissement_unite.etat='enprojet' 
    AND etablissement_unite.archive IS FALSE 
";
} else {
    $selection = " WHERE NULL ";
}

// Aucune recherche dans ce contexte
$champRecherche= array(
);

// Aucune action d'ajout d'une UA n'est possible depuis ce listing 
if (isset($tab_actions) === true
    && is_array($tab_actions) === true
    && array_key_exists("corner", $tab_actions) === true
    && is_array($tab_actions["corner"]) === true
    && array_key_exists("ajouter", $tab_actions["corner"]) === true) {
    //
    unset($tab_actions["corner"]["ajouter"]);
}
