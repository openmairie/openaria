<?php
/**
 * @package openaria
 * @version SVN : $Id$
 */

include "../sql/pgsql/app_om_tab_common_select.inc.php";
include "../gen/sql/pgsql/etablissement_unite.inc.php";

// Fil d'ariane
$ent = __("etablissements")." -> ".__("unites d'accessibilite");

//
$tab_title = __("UA");

// FROM
$table = DB_PREFIXE."etablissement_unite
    LEFT JOIN ".DB_PREFIXE."derogation_scda 
        ON etablissement_unite.acc_derogation_scda=derogation_scda.derogation_scda 
    LEFT JOIN ".DB_PREFIXE."dossier_instruction 
        ON etablissement_unite.dossier_instruction=dossier_instruction.dossier_instruction 
    LEFT JOIN ".DB_PREFIXE."etablissement 
        ON etablissement_unite.etablissement=etablissement.etablissement 
    LEFT JOIN ".DB_PREFIXE."etablissement_unite as eu2
        ON etablissement_unite.etablissement_unite_lie=eu2.etablissement_unite
    LEFT JOIN ".DB_PREFIXE."arrondissement 
        ON etablissement.adresse_arrondissement=arrondissement.arrondissement 
    LEFT JOIN ".DB_PREFIXE."voie 
        ON etablissement.adresse_voie=voie.voie 
 ";

//
$champAffiche_ua = array(
    'etablissement_unite.etablissement_unite as "'.__("etablissement_unite").'"',
    'etablissement_unite.libelle as "'.__("libelle").'"',
);

//
$champAffiche_infos_etab = array(
    $select__etab_label__column__as,
    'concat(etablissement.adresse_numero, 
        \' \', etablissement.adresse_numero2, \' \', voie.libelle,
        \' \', etablissement.adresse_cp, \' (\', arrondissement.libelle,\')\')
        as "'.__("adresse").'"',
);

//
$champAffiche_cas1 = array(
    "CASE etablissement_unite.acc_handicap_auditif when 't' then 'Oui' when 'f' then 'Non' else '' end as \"".__("acc. auditif")."\"",
    "case etablissement_unite.acc_handicap_mental when 't' then 'Oui' when 'f' then 'Non' else '' end as \"".__("acc. mental")."\"",
    "case etablissement_unite.acc_handicap_physique when 't' then 'Oui' when 'f' then 'Non' else '' end as \"".__("acc. physique")."\"",
    "case etablissement_unite.acc_handicap_visuel when 't' then 'Oui' when 'f' then 'Non' else '' end as \"".__("acc. visuel")."\"",
);

//
$champAffiche_cas2 = array(
    "CONCAT(
        'auditif&nbsp;:&nbsp;', case etablissement_unite.acc_handicap_auditif when 't' then 'Oui' when 'f' then 'Non' else ''  end, '<br/>',
        'mental&nbsp;:&nbsp;', case etablissement_unite.acc_handicap_mental when 't' then 'Oui' when 'f' then 'Non' else ''  end, '<br/>',
        'physique&nbsp;:&nbsp;', case etablissement_unite.acc_handicap_physique when 't' then 'Oui' when 'f' then 'Non' else ''  end, '<br/>',
        'visuel&nbsp;:&nbsp;', case etablissement_unite.acc_handicap_visuel when 't' then 'Oui' when 'f' then 'Non' else '' end
        )
    as \"".__("accessible")."\"",
);

//
$champAffiche_etat = array(
    "CASE etablissement_unite.etat 
        when 'valide' then '".__("validé")."' 
        when 'enprojet' then '".__("en projet")."'
        else '-'
    end as \"".__("etat")."\"",
);

//
$champAffiche_archive = array(
    "'Non' as \"".__("archive")."\"",
);

//
$champAffiche_derogation = array(
    "'' as \"".__("dérogation")."\"",
);

//
$champAffiche = array_merge(
    $champAffiche_ua, 
    $champAffiche_infos_etab,
    $champAffiche_cas2, 
    $champAffiche_etat
);

//
$champRecherche= array(
    'etablissement_unite.libelle as "'.__("libelle").'"',
    $select__etab_label__column__as,
);

//
$selection = " WHERE etablissement_unite.archive IS FALSE ";

$retourformulaire = (isset($_GET['retourformulaire']) ? $_GET['retourformulaire'] : "");

// Supprime le bouton ajouter si on accède depuis le menu
if (!isset($retourformulaire)
    || $retourformulaire == null
    || $retourformulaire == ''){

    if (isset($tab_actions) === true
        && is_array($tab_actions) === true
        && array_key_exists("corner", $tab_actions) === true
        && is_array($tab_actions["corner"]) === true
        && array_key_exists("ajouter", $tab_actions["corner"]) === true) {
        //
        unset($tab_actions["corner"]["ajouter"]);
    }
}

// Affichage du bouton de redirection vers le SIG externe si configuré
if ($f->getParameter('option_sig') == 'sig_externe'
    AND $retourformulaire != 'etablissement_tous'
    AND $retourformulaire != 'etablissement') {
    $tab_actions['left']["localiser-sig-externe"] = array(
                'lien' => OM_ROUTE_FORM.'&obj=etablissement_unite&amp;action=30&amp;idx=',
                'id' => '',
                'lib' => '<span class="om-icon om-icon-16 om-icon-fix sig-16" title="'.__('Localiser').'">'.__('Localiser').'</span>',
                'rights' => array('list' => array('etablissement_unite', 'etablissement_unite_consulter'), 'operator' => 'OR'),
                'ordre' => 20,
                'target' => "blank",
                'ajax' => false);
}

/**
 * Gestion SOUSFORMULAIRE => $sousformulaire
 */
$sousformulaire = array(
);


/**
 * OPTIONS
 */
if (!isset($options)) {
    $options = array();
}

/**
 * Recherche avancée
 */
// Options pour les select des booléens
$args = array(
    0 => array("", "t", "f", ),
    1 => array(__("choisir"), __("Oui"), __("Non") ),
);
//
$args_etat = array(
    0 => array("", "enprojet", "valide", ),
    1 => array(__("choisir"), __("en projet"), __("validé"), ),
);
//
$champs = array();
//
$champs['libelle'] = array(
    'table' => 'etablissement_unite',
    'colonne' => 'libelle',
    'type' => 'text',
    'taille' => 30,
    'max' => 100,
    'libelle' => __('libelle')
);
//
$champs['etablissement'] = array(
    'table' => 'etablissement',
    'colonne' => array('libelle','code'),
    'type' => 'text',
    'taille' => 100,
    'max' => 100,
    'libelle' => __('etablissement')
);
//
$champs['adresse_numero'] = array(
    'table' => 'etablissement',
    'colonne' => 'adresse_numero',
    'type' => 'text',
    'taille' => 40,
    'max' => 100,
    'libelle' => __('numero')
);
//
$champs['adresse_voie'] = array(
    'table' => 'voie',
    'colonne' => 'libelle',
    'type' => 'text',
    'taille' => 40,
    'max' => 100,
    'libelle' => __('voie')
);
//
$champs['adresse_arrondissement'] = array(
    'table' => 'arrondissement',
    'colonne' => 'arrondissement',
    'libelle' => __('arrondissement'),
    'type' => 'select',
    'subtype' => 'sqlselect',
    'sql' => 'SELECT arrondissement, libelle FROM '.DB_PREFIXE.'arrondissement ORDER BY code::int' ,
);
//
$champs['etat'] = array(
    'table' => 'etablissement_unite',
    'colonne' => 'etat',
    'type' => 'select',
    "subtype" => "manualselect",
    'libelle' => __('etat'),
    "args" => $args_etat,
);
//
$champs['acc_handicap_auditif'] = array(
    'table' => 'etablissement_unite',
    'colonne' => 'acc_handicap_auditif',
    'type' => 'select',
    "subtype" => "manualselect",
    'libelle' => __('acc_handicap_auditif'),
    "args" => $args,
);
//
$champs['acc_handicap_mental'] = array(
    'table' => 'etablissement_unite',
    'colonne' => 'acc_handicap_mental',
    'type' => 'select',
    "subtype" => "manualselect",
    'libelle' => __('acc_handicap_mental'),
    "args" => $args,
);
//
$champs['acc_handicap_physique'] = array(
    'table' => 'etablissement_unite',
    'colonne' => 'acc_handicap_physique',
    'type' => 'select',
    "subtype" => "manualselect",
    'libelle' => __('acc_handicap_physique'),
    "args" => $args,
);
//
$champs['acc_handicap_visuel'] = array(
    'table' => 'etablissement_unite',
    'colonne' => 'acc_handicap_visuel',
    'type' => 'select',
    "subtype" => "manualselect",
    'libelle' => __('acc_handicap_visuel'),
    "args" => $args,
);
//
$options["advsearch"] = array(
    'type' => 'search',
    'display' => true,
    'advanced'  => $champs,
    'default_form'  => 'advanced',
    'absolute_object' => 'etablissement_unite'
);
