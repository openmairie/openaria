<?php
//
include PATH_OPENMAIRIE."sql/pgsql/om_lettretype.inc.php";
//
if ($_SESSION['niveau'] == '2') {
    $selection = "";
    if ($retourformulaire== 'om_requete') {
        $selection .= " WHERE (".$obj.".om_sql = ".intval($idxformulaire).")";
    } elseif ($retourformulaire == 'modele_edition') {
    	$selection .= " WHERE (".$obj.".id = (SELECT modele_edition.om_lettretype_id FROM ".DB_PREFIXE."modele_edition WHERE modele_edition=".intval($idxformulaire)."))";
    }
} else {
    $selection = " WHERE (".$obj.".om_collectivite='".$_SESSION['collectivite']."' or niveau='2')";
    if ($retourformulaire== 'om_requete') {
        $selection .= " AND (".$obj.".om_sql = ".intval($idxformulaire).")";
    } elseif ($retourformulaire == 'modele_edition') {
    	$selection .= " AND (".$obj.".id = (SELECT modele_edition.om_lettretype_id FROM ".DB_PREFIXE."modele_edition WHERE modele_edition=".intval($idxformulaire)."))";
    }
}
