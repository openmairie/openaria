<?php
/**
 * @package openaria
 * @version SVN : $Id$
 */

//
include "../gen/sql/pgsql/prescription_reglementaire.inc.php";

$sousformulaire = array(
    'prescription_specifique',
);

// Filtre sur le service de l'utilisateur
include "../sql/pgsql/filter_service.inc.php";

$tri = " ORDER BY prescription_reglementaire.service ASC NULLS LAST, prescription_reglementaire.tete_de_chapitre1 ASC NULLS LAST, prescription_reglementaire.tete_de_chapitre2 ASC NULLS LAST, prescription_reglementaire.libelle ASC NULLS LAST ";
