<?php
/**
 * @package openaria
 * @version SVN : $Id$
 */

//
include "../sql/pgsql/courrier.inc.php";

// Fil d'ariane
$ent = __("suivi")." -> ".__("documents generes")." -> ".__("attente signature");
if (!empty($retourformulaire)) {
    $ent = __("documents générés en attente de signature");
}

// Nom de l'onglet
$tab_title = __("document généré en attente de signature");

// CONDITION
$selection = "
WHERE courrier.finalise IS TRUE
    AND courrier.date_envoi_signature IS NOT NULL
    AND courrier.date_retour_signature IS NULL
    AND courrier.date_envoi_rar IS NULL
    AND courrier.date_retour_rar IS NULL
";
// Filtre par service
if (!is_null($_SESSION['service'])) {
    $selection .= "
        AND (dossier_instruction.service = ".$_SESSION['service']."  
            OR dossier_instruction.service is NULL)
    ";
}

// TRI
$tri = "
ORDER BY courrier.date_finalisation ASC NULLS LAST
";
