<?php
/**
 * Listing des UA analysées / en analyse dans le contexte d'une analyse de 
 * dossier d'instruction. 
 *
 * Ce listing est un tableau qui fait apparaître toutes les UA qui concernent 
 * l'analyse du dossier d'instruction sur lequel on se trouve. Ce sont les UA
 * ajoutées directement depuis cette analyse ainsi que les UA existantes 
 * sélectionnées. 
 *
 * @package openaria
 * @version SVN : $Id: etablissement_unite.inc.php 1172 2015-03-17 13:45:35Z nmeucci $
 */

//
include "../sql/pgsql/etablissement_unite.inc.php";

// Fil d'ariane
$ent = __("etablissements")." -> ".__("unites d'accessibilite");

//
$tab_title = __("UA");

//
$champAffiche = array_merge(
    $champAffiche_ua, 
    $champAffiche_etat, 
    $champAffiche_archive
);

// Filtre listing sous formulaire - dossier_instruction
if (in_array($retourformulaire, $foreign_keys_extended["dossier_instruction"])
    && isset($idx)) {
    $selection = " 
WHERE 
    etablissement_unite.dossier_instruction = '".$idx."' 
";
} else {
    $selection = " WHERE NULL ";
}

// Aucune recherche dans ce contexte
$champRecherche= array(
);
