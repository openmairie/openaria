<?php
/**
 * Ce script permet de configurer le listing 'Mes visites à réaliser'.
 *
 * L'objectif de ce listing est de présenter à l'utilisateur toutes les
 * visites du jour et à venir pour lesquelles il est le technicien.
 *
 * @package openaria
 * @version SVN : $Id$
 */

//
include "../sql/pgsql/dossier_instruction_tous_visites.inc.php";

// Titre de la page
$ent = __("dossiers")." -> ".__("visites")." -> ".__("mes visites a realiser");

// Titre de l'onglet
$tab_title = __("Visite");

// SELECT 
$champAffiche = array(
    // Id nécessaire pour le lien consulter vers le DI
    'dossier_instruction.dossier_instruction as "'.__("id").'"',
    //
    'dossier_instruction.libelle as "'.__("DI").'"',
    $select__etab_label__column__as,
    //
    'concat(etablissement.adresse_numero, 
        \' \', etablissement.adresse_numero2, \' \', voie.libelle,
        \' \', etablissement.adresse_cp, \' (\', arrondissement.libelle,\')\')
        as "'.__("adresse").'"',
    'autorite_competente.code as "'.__("autorite_competente").'"',
    "to_char(visite.date_visite,'TMDay') as \"".__("jour")."\"",
    "to_char(visite.date_visite,'DD/MM/YYYY') as \"".__("date")."\"",
    'CONCAT(\''.__(" de ").'\', visite.heure_debut, \''.__(" à ").'\', visite.heure_fin) as "'.__("horaire").'"',
    'CONCAT(
        \'<span class="label\',
        CASE 
            WHEN 
                visite.programmation_version_annulation IS NOT NULL
                AND visite.programmation_version_annulation > 1 
            THEN
                \' state-ann bg-danger">'.__("annulée").'\'
            WHEN
                visite.programmation_version_modification IS NOT NULL
                AND visite.programmation_version_modification=programmation.version
                AND programmation_etat.code != \'VAL\' 
            THEN
                \' state-enc bg-warning">'.__("en cours de modification").'\'
            ELSE
                \' state-pla bg-valid">'.__("planifiée").'\'
        END,
        \'</span>\'
    ) as "'.__("etat").'"',
);

// FROM 
$table = DB_PREFIXE."visite
LEFT JOIN ".DB_PREFIXE."dossier_instruction
    ON dossier_instruction.dossier_instruction = visite.dossier_instruction
LEFT JOIN ".DB_PREFIXE."autorite_competente 
    ON dossier_instruction.autorite_competente=autorite_competente.autorite_competente 
LEFT JOIN ".DB_PREFIXE."dossier_coordination
    ON dossier_coordination.dossier_coordination = dossier_instruction.dossier_coordination
LEFT JOIN ".DB_PREFIXE."etablissement
    ON etablissement.etablissement = dossier_coordination.etablissement
LEFT JOIN ".DB_PREFIXE."arrondissement 
    ON etablissement.adresse_arrondissement=arrondissement.arrondissement 
LEFT JOIN ".DB_PREFIXE."voie 
    ON etablissement.adresse_voie=voie.voie 
LEFT JOIN ".DB_PREFIXE."acteur
    ON acteur.acteur = visite.acteur 
LEFT JOIN ".DB_PREFIXE."om_utilisateur
    ON acteur.om_utilisateur = om_utilisateur.om_utilisateur 
LEFT JOIN ".DB_PREFIXE."programmation
    ON visite.programmation = programmation.programmation
LEFT JOIN ".DB_PREFIXE."programmation_etat
    ON programmation.programmation_etat = programmation_etat.programmation_etat
";

// ORDER BY
$tri = "ORDER BY visite.date_visite ASC, visite.heure_debut ASC NULLS LAST";

// Modification du WHERE
$selection = sprintf(
    '
        WHERE
            visite.date_visite >= CURRENT_DATE
            AND (
                (
                    programmation_etat.code=\'VAL\'
                    AND visite.programmation_version_creation<=programmation.version
                )
                OR
                (
                    programmation_etat.code!=\'VAL\'
                    AND visite.programmation_version_creation<programmation.version
                )
            )
            AND (
                visite.programmation_version_annulation!=1
                OR visite.programmation_version_annulation IS NULL
            )
            AND om_utilisateur.login=\'%1$s\'
    ',
    $_SESSION["login"]
);
//
$champRecherche = array();

//
$obj = "dossier_instruction_tous_visites";
// Cache le bouton ajouter
if (isset($tab_actions) === true
    && is_array($tab_actions) === true
    && array_key_exists("corner", $tab_actions) === true
    && is_array($tab_actions["corner"]) === true
    && array_key_exists("ajouter", $tab_actions["corner"]) === true) {
    //
    unset($tab_actions["corner"]["ajouter"]);
}
// Actions a gauche : consulter
$tab_actions['left']['consulter'] =
    array('lien' => OM_ROUTE_FORM.'&obj='.$obj.'&amp;action=3'.'&amp;idx=',
          'id' => '&amp;premier='.$premier.'&amp;advs_id='.$advs_id.'&amp;tricol='.$tricol.'&amp;valide='.$valide.'&amp;retour=tab',
          'lib' => '<span class="om-icon om-icon-16 om-icon-fix consult-16" title="'.__('Consulter').'">'.__('Consulter').'</span>',
          'rights' => array('list' => array($obj, $obj.'_consulter'), 'operator' => 'OR'),
          'ordre' => 10,);
// Action du contenu : consulter
$tab_actions['content'] = $tab_actions['left']['consulter'];
$obj = "visite_mes_visites_a_realiser";

// Initialisation des options de l'écran de listing
$options = array();
// Désactivation de la recherche simple
$option = array(
  "type" => "search",
  "display" => false,
);
$options[] = $option;
