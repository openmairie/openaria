<?php
/**
 * Surcharge de contrainte pour afficher directement
 * le formulaire spécifique de synchronisation depuis le menu Administration & Paramétrage.
 * 
 * @package openaria
 * @version SVN : $Id$
 */

//
include "../sql/pgsql/contrainte.inc.php";

// Fil d'Ariane
$ent = __("administration_parametrage")." -> ".__("contraintes")." -> ".__("synchronisation");
