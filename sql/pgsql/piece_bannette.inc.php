<?php
/**
 * Surcharge de la classe piece pour afficher seulement les pièces qui ne sont
 * pas liés à un etab/DC/DI.
 *
 * @package openaria
 * @version SVN : $Id$
 */

include('../sql/pgsql/piece.inc.php');

// Fil d'ariane
$ent = __("suivi")." -> ".__("documents entrants")." -> ".__("bannette");

// SELECT 
$champAffiche = array(
    'piece.piece as "'.__("id").'"',
    'piece.nom as "'.__("nom").'"',
    'piece_type.libelle as "'.__("piece_type").'"',
    'to_char(piece.date_reception ,\'DD/MM/YYYY\') as "'.__("date_reception").'"',
    'to_char(piece.date_emission ,\'DD/MM/YYYY\') as "'.__("date_emission").'"',
    $select__docent_suivi__column__as,
    'to_char(piece.date_butoir ,\'DD/MM/YYYY\') as "'.__("date_butoir").'"',
    'to_char(piece.om_date_creation ,\'DD/MM/YYYY\') as "'.__("om_date_creation").'"',
);
$champRecherche = array(
    'piece.piece as "'.__("id").'"',
    'piece.nom as "'.__("nom").'"',
    'piece_type.libelle as "'.__("piece_type").'"',
);

//
$selection = " WHERE piece.etablissement IS NULL
    AND piece.dossier_coordination IS NULL
    AND piece.dossier_instruction IS NULL ";

//
$tri = " ORDER BY piece.om_date_creation ASC, piece.piece ";

// Pas de recherche avancée sur ce listing
if (isset($options) === true
    && is_array($options) === true
    && array_key_exists("advsearch", $options) === true) {
    unset($options["advsearch"]);
}

// Filtre sur le service de l'utilisateur
include "../sql/pgsql/filter_service.inc.php";
