<?php
/**
 * Ce script permet...
 *
 * @package openaria
 * @version SVN : $Id$
 */


//
include('../sql/pgsql/dossier_instruction_tous_plans.inc.php');

// Fil d'ariane
$ent = __("dossiers")." -> ".__("DI")." -> ".__("mes plans");

// Modification du WHERE
$selection .= " AND om_utilisateur.login = '".$_SESSION["login"]."' ";

// On enlève la colonne technicien inutile dans ce contexte
$champAffiche = array_diff(
    $champAffiche,
    $champaffiche_di_technicien
);

//
$table .= "LEFT JOIN ".DB_PREFIXE."om_utilisateur
        ON acteur.om_utilisateur = om_utilisateur.om_utilisateur";

// Pas de recherche avancée
//$options = array();
unset($options["advsearch"]["advanced"]["technicien"]);
unset($options["advsearch"]["advanced"]["a_affecter"]);

// Filtre sur le service de l'utilisateur
include "../sql/pgsql/filter_service.inc.php";
