<?php
/**
 * TAB_EXPORT_CSV - visite.
 *
 * @package openaria
 * @version SVN : $Id$
 */

// On surcharge le tableau standard pour supprimer les balises html utiles pour l'affichage HTML
// mais polluantes pour l'export CSV
$select__etab_label__column__as = "CONCAT(etablissement.code, ' - ', etablissement.libelle) as \""._("etablissement")."\"";
$select__di_label__column__as = "dossier_instruction.libelle as \"".__("DI")."\"";
$champAffiche = array(
    'visite.visite as "'.__("visite").'"',
    $select__prog_semaine_annee__column__as,
    "to_char(visite.date_visite,'TMDay') as \"".__("jour")."\"",
    'to_char(visite.date_visite ,\'DD/MM/YYYY\') as "'.__("date").'"',
    'CONCAT(\''.__("de ").'\', visite.heure_debut, \''.__(" à ").'\', visite.heure_fin) as "'.__("horaire").'"',
    'acteur.nom_prenom as "'.__("acteur").'"',
    $select__etab_label__column__as,
    $select__di_label__column__as,
    'CONCAT(
        \'\',
        CASE 
            WHEN 
                visite.programmation_version_annulation IS NOT NULL
                AND visite.programmation_version_annulation > 1 
            THEN
                \''.__("annulée").'\'
            WHEN
                visite.programmation_version_creation IS NOT NULL
                AND visite.programmation_version_creation=programmation.version
                AND programmation_etat.code != \'VAL\' 
            THEN
                \''.__("en cours de création").'\'
            ELSE
                \''.__("planifiée").'\'
        END,
        \'\'
    ) as "'.__("etat").'"',
    'visite.convocation_exploitants as "'.__("convocation_exploitants").'"',
    'to_char(visite.date_annulation ,\'DD/MM/YYYY\') as "'.__("date_annulation").'"',
    "visite_motif_annulation.libelle as \"".__("motif d'annulation")."\"",
    'programmation_version_creation as "'.__("créée en v").'"',
    'programmation_version_modification as "'.__("modifiée en v").'"',
    'programmation_version_annulation as "'.__("annulée en v").'"',
);
