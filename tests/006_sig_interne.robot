*** Settings ***
Documentation  Ce 'Test Suite' permet de vérifier les fonctionnalités SIG interne.
...
...  Attention : Ce 'Test Suite' est dépendant de l'état de la base. Si des éléments
...  sont déjà géolocalisés, il se peut que ces tests échouent. C'est une limitation
...  liée à la fonctionnalité MAP du framework et à la librairie openLayers.
...
...  La problématique se situe sur le positionnement sur la carte et la sélection
...  des puces qui est dépendante des éléments déjà géolocalisés. Les tests passent
...  si aucun établissement n'a été géolocalisé. C'est pour cette raison que ce
...  'Test Suite' est le 006, car il intervient avant les tests métiers.

# On inclut les mots-clefs
Resource  resources/resources.robot
# On ouvre/ferme le navigateur au début/à la fin du Test Suite.
Suite Setup  For Suite Setup
Suite Teardown  For Suite Teardown


*** Test Cases ***
Constitution d'un jeu de données
    [Documentation]  L'objet de ce 'Test Case' est de constituer un jeu de de données
    ...  cohérent pour les scénarios fonctionnels qui suivent.

    Depuis la page d'accueil  admin  admin

    # On importe les établissements
    Depuis l'import  import_etablissement
    Add File  fic1  import_etablissements_006.csv
    Click On Submit Button In Import CSV
    WUX  Valid Message Should Contain  16 ligne(s) importée(s)

    # Établissement référence (doit être référentiel)
    &{etab_g_01} =  Create Dictionary
    ...  code=T1006560
    ...  libelle=ESPACE LOUIS PASQUET
    Depuis le contexte de l'établissement  ${etab_g_01.code}  ${etab_g_01.libelle}
    ${etab_g_01.id} =  Get Mandatory Value  css=#etablissement
    Set Suite Variable  ${etab_g_01}

    # Activation de l'option
    &{values} =  Create Dictionary
    ...  libelle=option_localisation
    ...  valeur=sig_interne
    Ajouter ou modifier l'enregistrement de type 'paramètre' (om_parametre)  ${values}

    # Géolocalisation de l'établissement référence pour vérification de l'intégration
    # dans les 'Test Case' qui suivent. Il ne faut pas modifier les coordonnées.
    Depuis la vue 'géolocaliser' (SIG interne) de l'établissement  ${etab_g_01.code}  ${etab_g_01.libelle}
    # Se positionner dans l'iframe
    Se positionner dans la carte (SIG interne)  iframe
    # La carte s'est affichée sur le bon contexte
    WUX  Element Should Contain  css=#map-titre  ETABLISSEMENT ${etab_g_01.id}
    # CLiquer sur l'onglet Outils
    # xpath à remplacer dès que possible par un identifiant
    WUX  Click Element  xpath=//html/body/div/div[5]/div[1]/div/ul/li[1]/a[@onclick="affiche_tools()"]
    # Cliquer sur l'action Éditer
    WUX  Click Element  css=#map-tools-edit
    # Cliquer sur l'action Dessiner Point
    WUX  Click Element  css=#map-edit-draw-point
    # Cliquer sur la carte pour positionner le point
    # coordonnées 0 0 (en plein mileu de la carte)
    WUX  Click Element At Coordinates  css=#map-id  0  0
    # Cliquer sur l'action Sélection Géométrie
    WUX  Click Element  css=#map-edit-select
    # Cliquer sur le point positionné juste avant pour le sélectionner
    WUX  Click Element At Coordinates  css=#map-id  0  0
    # Cliquer sur le bouton Enregistrer
    WUX  Click Element  css=#map-edit-record
    # Cliquer sur le bouton Valider dans l'overlay
    WUX  Click Element  css=div.ui-dialog input.om-button
    # Cliquer sur le bouton retour dans l'overlay
    WUX  Click Element  css=div.ui-dialog a.retour
    # Sortir de l'iframe
    Quitter la carte (SIG interne)  iframe


Actions sur le listing de tous les établissements, cartes et vue sommaire
    [Documentation]  Ce 'Test Case' vérifie l'intégration du SIG interne dans
    ...  le contexte du listing de tous les établissements.
    ...
    ...  - Action 'export SIG', carte et vue sommaire depuis le listing de tous les établissements
    ...  - Action 'géolocaliser' et carte depuis le listing de tous les établissements

    Depuis la page d'accueil  admin  admin

    # Action 'export SIG', carte et vue sommaire depuis le listing de tous les établissements
    Depuis le listing  etablissement_tous
    # L'action 'export SIG' est affichée au dessus du tableau
    Page Should Contain Element  css=div.tab-export a span.sig-25
    # Cliquer sur l'action 'export SIG'
    Click Element  css=div.tab-export a span.sig-25
    # Ouvrir la popup
    Se positionner dans la carte (SIG interne)  new_window
    # La carte s'est affichée sur le bon contexte
    WUX  Element Should Contain  css=#map-titre  ETABLISSEMENT_TOUS -1
    # Cliquer sur le marqueur
    # XXX Vérifier le comportement si il y a plusieurs marqueurs sur la carte
    WUX  Click Element  css=image[id^='OpenLayers_Geometry_Point_']
    # Cliquer sur l'onglet Infos
    # xpath à remplacer dès que possible par un identifiant
    WUX  Click Element  xpath=//html/body/div/div[5]/div[1]/div/ul/li[2]/a
    # Cliquer sur le lien du marqueur
    WUX  Click Element  css=#map-getfeatures-markers a
    # Cliquer sur le lien dans l'overlay de la vue sommaire
    WUX  Click Element  css=div.ui-dialog #link_etablissement
    # Le lien amène au bon contexte
    WUX  Page Title Should Be  Établissements > Tous Les Établissements > ${etab_g_01.code} - ${etab_g_01.libelle}
    # Fermet la popup
    Quitter la carte (SIG interne)  new_window

    # Action 'géolocaliser' et carte depuis le listing de tous les établissements
    Depuis le listing  etablissement_tous
    # Filtrer les résultats pour que l'établisssement référence apparaisse dans le listing
    Input Text  css=div#adv-search-adv-fields input#etablissement  ${etab_g_01.code}
    Input Text  css=div#adv-search-adv-fields input#libelle  ${etab_g_01.libelle}
    Click On Search Button
    # L'action 'géolocaliser' est affichée sur la ligne de l'établissement
    Page Should Contain Element  css=#action-tab-etablissement_tous-left-localiser-sig-interne-${etab_g_01.id}
    # Cliquer sur l'action 'géolocaliser'
    Click Element  css=#action-tab-etablissement_tous-left-localiser-sig-interne-${etab_g_01.id}
    La page ne doit pas contenir d'erreur
    # Le lien amène au bon contexte
    Page Title Should Be  Établissements > Tous Les Établissements > ${etab_g_01.code} - ${etab_g_01.libelle}
    # Se positionner dans l'iframe
    Se positionner dans la carte (SIG interne)  iframe
    # La carte s'est affichée sur le bon contexte
    WUX  Element Should Contain  css=#map-titre  ETABLISSEMENT ${etab_g_01.id}
    # Sortir de l'iframe
    Quitter la carte (SIG interne)  iframe


Actions sur le listing des ERP référentiels, cartes et vue sommaire
    [Documentation]  Ce 'Test Case' vérifie l'intégration du SIG interne dans
    ...  le contexte du listing des ERP référentiels.
    ...
    ...  - Action 'export SIG', carte et vue sommaire depuis le listing des ERP référentiels.
    ...  - Action 'géolocaliser' et carte depuis le listing des ERP référentiels.

    Depuis la page d'accueil  admin  admin

    # Action 'export SIG', carte et vue sommaire depuis le listing des ERP référentiels
    Depuis le listing  etablissement_referentiel_erp
    # L'action 'export SIG' est affichée au dessus du tableau
    Page Should Contain Element  css=div.tab-export a span.sig-25
    # Cliquer sur l'action 'export SIG'
    Click Element  css=div.tab-export a span.sig-25
    # Ouvrir la popup
    Se positionner dans la carte (SIG interne)  new_window
    WUX  Element Should Contain  css=#map-titre  ETABLISSEMENT_REFERENTIEL_ERP -1
    # Cliquer sur le marqueur
    # XXX Vérifier le comportement si il y a plusieurs marqueurs sur la carte
    WUX  Click Element  css=image[id^='OpenLayers_Geometry_Point_']
    # Cliquer sur l'onglet Infos
    # xpath à remplacer dès que possible par un identifiant
    WUX  Click Element  xpath=//html/body/div/div[5]/div[1]/div/ul/li[2]/a
    # Cliquer sur le lien du marqueur
    WUX  Click Element  css=#map-getfeatures-markers a
    # Cliquer sur le lien dans l'overlay de la vue sommaire
    WUX  Click Element  css=div.ui-dialog #link_etablissement
    # Le lien amène au bon contexte
    WUX  Page Title Should Be  Établissements > Référentiel ERP > ${etab_g_01.code} - ${etab_g_01.libelle}
    # Fermet la popup
    Quitter la carte (SIG interne)  new_window

    # Action 'géolocaliser' et carte depuis le listing des ERP référentiels
    Depuis le listing  etablissement_referentiel_erp
    # Filtrer les résultats pour que l'établisssement référence apparaisse dans le listing
    Input Text  css=div#adv-search-adv-fields input#etablissement  ${etab_g_01.code}
    Input Text  css=div#adv-search-adv-fields input#libelle  ${etab_g_01.libelle}
    Click On Search Button
    # L'action 'géolocaliser' est affichée sur la ligne de l'établissement
    Page Should Contain Element  css=#action-tab-etablissement_referentiel_erp-left-localiser-sig-interne-${etab_g_01.id}
    # Cliquer sur l'action 'géolocaliser'
    Click Element  css=#action-tab-etablissement_referentiel_erp-left-localiser-sig-interne-${etab_g_01.id}
    La page ne doit pas contenir d'erreur
    # Le lien amène au bon contexte
    Page Title Should Be  Établissements > Référentiel ERP > ${etab_g_01.code} - ${etab_g_01.libelle}
    # Se positionner dans l'iframe
    Se positionner dans la carte (SIG interne)  iframe
    # La carte s'est affichée sur le bon contexte
    WUX  Element Should Contain  css=#map-titre  ETABLISSEMENT ${etab_g_01.id}
    # Sortir de l'iframe
    Quitter la carte (SIG interne)  iframe


Vue 'géolocaliser' et carte sur l'établissement
    [Documentation]  Ce 'Test Case' vérifie l'intégration du SIG interne dans
    ...  le contexte des établissements (tous et ERP référentiel).
    ...
    ...  - Barre d'outils depuis la fiche 'consulter' de l'établissement
    ...  - Affichage de la carte sur la vue 'géolocaliser' de l'établissement

    Depuis la page d'accueil  admin  admin

    Depuis le contexte de l'établissement  ${etab_g_01.code}  ${etab_g_01.libelle}  tous
    # Barre d'outils depuis la fiche 'consulter' de l'établissement
    Page Title Should Be  Établissements > Tous Les Établissements > ${etab_g_01.code} - ${etab_g_01.libelle}
    Page Should Contain Element  css=#switch-geolocaliser-consulter
    Page Should Contain Element  css=#switch-geolocaliser-consulter a.localiser-sig-interne-16
    Page Should Contain Element  css=#switch-geolocaliser-consulter a.consult-16
    Cliquer sur l'action 'géolocaliser' dans le contexte de l'établissement
    La page ne doit pas contenir d'erreur
    Page Title Should Be  Établissements > Tous Les Établissements > ${etab_g_01.code} - ${etab_g_01.libelle}
    Page Should Contain Element  css=#switch-geolocaliser-consulter
    Page Should Contain Element  css=#switch-geolocaliser-consulter a.localiser-sig-interne-16
    Page Should Contain Element  css=#switch-geolocaliser-consulter a.consult-16
    # Affichage de la carte sur la vue 'géolocaliser' de l'établissement
    Se positionner dans la carte (SIG interne)  iframe
    WUX  Element Should Contain  css=#map-titre  ETABLISSEMENT ${etab_g_01.id}
    Quitter la carte (SIG interne)  iframe

    Depuis le contexte de l'établissement  ${etab_g_01.code}  ${etab_g_01.libelle}  referentiel_erp
    # Barre d'outils depuis la fiche 'consulter' de l'établissement
    Page Title Should Be  Établissements > Référentiel ERP > ${etab_g_01.code} - ${etab_g_01.libelle}
    Page Should Contain Element  css=#switch-geolocaliser-consulter
    Page Should Contain Element  css=#switch-geolocaliser-consulter a.localiser-sig-interne-16
    Page Should Contain Element  css=#switch-geolocaliser-consulter a.consult-16
    Cliquer sur l'action 'géolocaliser' dans le contexte de l'établissement
    La page ne doit pas contenir d'erreur
    Page Title Should Be  Établissements > Référentiel ERP > ${etab_g_01.code} - ${etab_g_01.libelle}
    Page Should Contain Element  css=#switch-geolocaliser-consulter
    Page Should Contain Element  css=#switch-geolocaliser-consulter a.localiser-sig-interne-16
    Page Should Contain Element  css=#switch-geolocaliser-consulter a.consult-16
    # Affichage de la carte sur la vue 'géolocaliser' de l'établissement
    Se positionner dans la carte (SIG interne)  iframe
    WUX  Element Should Contain  css=#map-titre  ETABLISSEMENT ${etab_g_01.id}
    Quitter la carte (SIG interne)  iframe


Option désactivée - Absence des actions et des liens
    [Documentation]  Le SIG interne est une option. Ce 'Test Case' a pour objet
    ...  de vérifier que les actions amenées par cette option sont bien
    ...  absentes lorsque l'option est désactivée.

    Depuis la page d'accueil  admin  admin

    # Désactivation de l'option
    &{values} =  Create Dictionary
    ...  libelle=option_localisation
    ...  valeur=false
    Ajouter ou modifier l'enregistrement de type 'paramètre' (om_parametre)  ${values}

    # L'action 'export SIG' et l'action 'géolocaliser' par établissement doivent être
    # absentes sur les listing des établissements ERP référentiels
    Depuis le listing des ERP référentiels
    Page Should Not Contain Element  css=div.tab-export a span.sig-25
    Page Should Not Contain Element  css=#action-tab-etablissement_referentiel_erp-left-localiser-sig-interne-2803

    # L'action 'export SIG' et l'action 'géolocaliser' par établissement doivent être
    # absentes sur les listing de tous les établissements
    Depuis le listing de tous les établissements
    Page Should Not Contain Element  css=div.tab-export a span.sig-25
    Page Should Not Contain Element  css=#action-tab-etablissement_tous-left-localiser-sig-interne-2803

    # La barre d'outils 'géolocaliser' sur la vue 'consulter' de l'établissement doit
    # être absente dans le contexte de tous les établissements
    Depuis le contexte de l'établissement  ${etab_g_01.code}  ${etab_g_01.libelle}  tous
    Page Should Not Contain Element  css=#switch-geolocaliser-consulter

    # La barre d'outils 'géolocaliser' sur la vue 'consulter' de l'établissement doit
    # être absente dans le contexte des ERP référentiels
    Depuis le contexte de l'établissement  ${etab_g_01.code}  ${etab_g_01.libelle}  referentiel_erp
    Page Should Not Contain Element  css=#switch-geolocaliser-consulter

