<?php
/**
 * Ce script contient la définition de la classe 'DigitalizedDocumentPHP70Test'.
 *
 * @package openaria
 * @version SVN : $Id$
 */

require_once "010_test_digitalizedDocument_common.php";
final class DigitalizedDocumentPHP70Test extends DigitalizedDocumentCommon {
    public function setUp() {
        $this->common_setUp();
    }
    public function tearDown() {
        $this->common_tearDown();
    }
    public function onNotSuccessfulTest(Throwable $e) {
        $this->common_onNotSuccessfulTest($e);
    }
    public static function setUpBeforeClass() {
        DigitalizedDocumentCommon::common_setUpBeforeClass();
    }
    public static function tearDownAfterClass() {
        DigitalizedDocumentCommon::common_tearDownAfterClass();
    }
}
