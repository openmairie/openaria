*** Settings ***
Documentation     Ressources (librairies, keywords et variables)

# Mots-clefs framework
Library  openmairie.robotframework.Library

# Mots-clefs métier
Resource          application.robot
Resource          app${/}acteur.robot
Resource          app${/}analyse.robot
Resource          app${/}autorite_police.robot
Resource          app${/}autorite_police_decision.robot
Resource          app${/}autorite_police_motif.robot
Resource          app${/}contact.robot
Resource          app${/}contrainte.robot
Resource          app${/}courrier_type.robot
Resource          app${/}derogation_scda.robot
Resource          app${/}document_entrant.robot
Resource          app${/}document_genere.robot
Resource          app${/}dossier_coordination.robot
Resource          app${/}dossier_coordination_message.robot
Resource          app${/}dossier_instruction.robot
Resource          app${/}dossier_instruction_reunion.robot
Resource          app${/}essai_realise.robot
Resource          app${/}etablissement.robot
Resource          app${/}etablissement_nature.robot
Resource          app${/}etablissement_unite.robot
Resource          app${/}periodicite_visites.robot
Resource          app${/}prescription.robot
Resource          app${/}proces_verbal.robot
Resource          app${/}programmation.robot
Resource          app${/}referentiel_ads.robot
Resource          app${/}reunion.robot
Resource          app${/}reunion_avis.robot
Resource          app${/}reunion_categorie.robot
Resource          app${/}reunion_instance.robot
Resource          app${/}reunion_instance_membre.robot
Resource          app${/}reunion_type.robot
Resource          app${/}sig.robot
Resource          app${/}technicien_arrondissement.robot
Resource          app${/}visite.robot
Resource          app${/}voie.robot
Resource          app${/}signataire.robot

*** Variables ***
${SERVER}           localhost
${PROJECT_NAME}     openaria
${BROWSER}          firefox
${DELAY}            0
${ADMIN_USER}       admin
${ADMIN_PASSWORD}   admin
${PROJECT_URL}      http://${SERVER}/${PROJECT_NAME}/
${PATH_BIN_FILES}   ${EXECDIR}${/}binary_files${/}
${TITLE}            :: openMairie :: openARIA - Analyse du Risque Incendie et de l'Accessibilité pour les ERP
${SESSION_COOKIE}   1bb484de79f96a6d0b00aaf4463c18f8bf

*** Keywords ***
For Suite Setup
    Reload Library  openmairie.robotframework.Library
    # Préparation des répertoire de stockage de screenshots dédiés à la
    # documentation
    Create Directory  results/screenshots
    Create Directory  results/screenshots/ergonomie
    Create Directory  results/screenshots/etablissements
    Create Directory  results/screenshots/dossiers
    Create Directory  results/screenshots/suivi
    # Les keywords définit dans le resources.robot sont prioritaires
    Set Library Search Order    resources
    Ouvrir le navigateur
    Tests Setup


For Suite Teardown
    Fermer le navigateur

