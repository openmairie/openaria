*** Settings ***
Documentation    Actions spécifiques aux contraintes

*** Keywords ***

Depuis le contexte de la contrainte
    [Documentation]  Accède au formulaire
    [Arguments]  ${contrainte}

    # On accède au tableau
    Depuis le listing  contrainte
    # On recherche l'enregistrement
    Use Simple Search  libellé  ${contrainte}
    # On clique sur le résultat
    Click On Link  ${contrainte}
    # On vérifie qu'il n'y a pas d'erreur
    La page ne doit pas contenir d'erreur

Depuis le contexte de la contrainte archivée
    [Documentation]  Accède au formulaire
    [Arguments]  ${contrainte}

    # On accède au tableau
    Depuis le listing  contrainte
    # On affiche les éléments expirés
    Click Element  css=span.om_validite_link > a
    WUX  Element Should Contain  tab-contrainte  ${contrainte}
    # On recherche l'enregistrement
    Use Simple Search  libellé  ${contrainte}
    # On clique sur le résultat
    Click On Link  ${contrainte}
    # On vérifie qu'il n'y a pas d'erreur
    La page ne doit pas contenir d'erreur

Ajouter la contrainte
    [Documentation]  Crée l'enregistrement
    [Arguments]  ${values}

    # On accède au tableau
    Depuis le listing  contrainte
    # On clique sur le bouton ajouter
    Click On Add Button
    # On saisit des valeurs
    Saisir la contrainte  ${values}
    # On valide le formulaire
    Click On Submit Button

Modifier la contrainte
    [Documentation]  Modifie l'enregistrement
    [Arguments]  ${contrainte}  ${values}

    # On accède à l'enregistrement
    Depuis le contexte de la contrainte  ${contrainte}
    # On clique sur le bouton modifier
    Click On Form Portlet Action  contrainte  modifier
    # On saisit des valeurs
    Saisir la contrainte  ${values}
    # On valide le formulaire
    Click On Submit Button

Archiver la contrainte
    [Documentation]  Modifie l'enregistrement
    [Arguments]  ${contrainte}  ${date}

    # On accède à l'enregistrement
    Depuis le contexte de la contrainte  ${contrainte}
    # On clique sur le bouton modifier
    Click On Form Portlet Action  contrainte  modifier
    # On saisit une date de fin de validité
    &{archivee} =  Create Dictionary
    ...  om_validite_fin=${date}
    Saisir la contrainte  ${archivee}
    # On valide le formulaire
    Click On Submit Button

Supprimer la contrainte
    [Documentation]  Supprime l'enregistrement
    [Arguments]  ${contrainte}

    # On accède à l'enregistrement
    Depuis le contexte de la contrainte  ${contrainte}
    # On clique sur le bouton supprimer
    Click On Form Portlet Action  contrainte  supprimer
    # On valide le formulaire
    Click On Submit Button

Supprimer la contrainte archivée
    [Documentation]  Supprime l'enregistrement
    [Arguments]  ${contrainte}

    # On accède à l'enregistrement
    Depuis le contexte de la contrainte archivée  ${contrainte}
    # On clique sur le bouton supprimer
    Click On Form Portlet Action  contrainte  supprimer
    # On valide le formulaire
    Click On Submit Button

Saisir la contrainte
    [Documentation]  Remplit le formulaire
    [Arguments]  ${values}

    Si "id_referentiel" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "nature" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "groupe" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "sousgroupe" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "libelle" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "texte" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "texte_surcharge" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "ordre_d_affichage" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "lie_a_un_referentiel" existe dans "${values}" on execute "Set Checkbox" dans le formulaire
    Si "om_validite_debut" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "om_validite_fin" existe dans "${values}" on execute "Input Datepicker" dans le formulaire

Appliquer la contrainte au DC
    [Documentation]  Ouvre le groupe et sous-groupe de la contrainte puis clique sur la checkbox
    ...  Ce mot-clef suppose qu'une seule contrainte fasse partie du sous-groupe.
    [Arguments]  ${groupe}  ${sousgroupe}

    WUX  Click Element  css=#fieldset-sousform-lien_contrainte_dossier_coordination-${groupe} legend
    WUX  Click Element  css=#fieldset-sousform-lien_contrainte_dossier_coordination-${sousgroupe} legend
    Sleep  1
    WUX  Select Checkbox  css=#fieldset-sousform-lien_contrainte_dossier_coordination-${sousgroupe} input[id^="contrainte_"]

Appliquer la contrainte à l'établissement
    [Documentation]  Ouvre le groupe et sous-groupe de la contrainte puis clique sur la checkbox
    ...  Ce mot-clef suppose qu'une seule contrainte fasse partie du sous-groupe.
    [Arguments]  ${groupe}  ${sousgroupe}

    WUX  Click Element  css=#fieldset-sousform-lien_contrainte_etablissement-${groupe} legend
    WUX  Click Element  css=#fieldset-sousform-lien_contrainte_etablissement-${sousgroupe} legend
    Sleep  1
    WUX  Select Checkbox  css=#fieldset-sousform-lien_contrainte_etablissement-${sousgroupe} input[id^="contrainte_"]

Synchroniser les contraintes
    [Documentation]  Ajoute/modifie/archive les contraintes paramétrées
    ...  selon celles du référentiel SIG

    Go To  ${PROJECT_URL}${OM_ROUTE_FORM}&obj=contrainte_synchronisation&action=4&idx=0
    Submit Form  css=#form-contrainte-synchroniser
