*** Settings ***
Documentation     Actions spécifiques aux dérogations SCDA

*** Keywords ***
Saisir la dérogation SCDA
    [Arguments]    ${code}=null    ${libelle}=null    ${date_debut}=null    ${date_fin}=null

    # Code
    Run Keyword If    '${code}' != 'null'    Input Text    css=#code    ${code}
    # Libellé
    Run Keyword If    '${libelle}' != 'null'    Input Text    css=#libelle    ${libelle}
    # Date de début
    Run Keyword If    '${date_debut}' != 'null'    Input Text    css=#om_validite_debut    ${date_debut}
    # Date de fin
    Run Keyword If    '${date_fin}' != 'null'    Input Text    css=#om_validite_fin    ${date_fin}

Ajouter une dérogation SCDA depuis le menu
    [Arguments]    ${code}=null    ${libelle}=null    ${date_debut}=null    ${date_fin}=null

    # On se positionne sur le tableau de bord
    Go To Dashboard
    # On ouvre le tableau
    Depuis le listing  derogation_scda
    # On clique sur l'icone ajouter
    Click On Add Button
    # On remplit les champs du formulaire
    Saisir la dérogation SCDA    ${code}    ${libelle}    ${date_debut}    ${date_fin}
    # On poste le formulaire
    Click On Submit Button
    # On vérifie qu'il n'y a pas d'erreur de base de données
    Valid Message Should Be    Vos modifications ont bien été enregistrées.
