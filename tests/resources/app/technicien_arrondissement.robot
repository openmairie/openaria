*** Settings ***
Documentation     Actions spécifiques aux lien acteur arrondissement.

*** Keywords ***
Depuis l'onglet arrondissement de l'acteur

    [Documentation]

    [Arguments]    ${acteur}

    #
    Depuis le contexte de l'acteur    ${acteur}
    # On clique sur l'onglet membre
    On clique sur l'onglet    technicien_arrondissement    Lien Technicien/arrondissement


Paramétrer l'arrondissement pour l'acteur

    [Documentation]

    [Arguments]    ${arrondissement}    ${acteur}

    Depuis l'onglet arrondissement de l'acteur    ${acteur}
    # On clique sur le bouton ajouter
    Click On Add Button JS
    #
    Select From List By Label    css=#arrondissement    ${arrondissement}
    #
    Click On Submit Button In Subform
    #
    Valid Message Should Be In Subform    Vos modifications ont bien été enregistrées.

