*** Settings ***
Documentation     Actions spécifiques aux dossiers d'instruction.

*** Keywords ***
Depuis le contexte du dossier d'instruction

   [Documentation]    Permet d'accéder à l'écran de visualisation d'un dossier d'instruction.

    [Arguments]    ${dossier_instruction}

    # On accède directement a l'écran de listing de tous les dossier_instruction
    # qui n'est pas interfacé dans le menu
    Depuis le listing  dossier_instruction
    # On fait une recherche sur le libellé du DI
    Input Text    css=div#adv-search-adv-fields input#dossier    ${dossier_instruction}
    # On valide le formulaire de recherche
    Click On Search Button
    # On accède à la visualisation du DI
    Click On Link    ${dossier_instruction}
    #
    Page Title Should Contain    ${dossier_instruction}


Depuis le formulaire de modification du dossier d'instruction

    [Documentation]

    [Arguments]    ${dossier_instruction}

    Depuis le contexte du dossier d'instruction    ${dossier_instruction}
    Click On Form Portlet Action    dossier_instruction    modifier


Depuis le contexte du dossier d'instruction de type visite

    [Documentation]    Permet d'accéder à l'écran de visualisation d'un
    ...    dossier d'instruction de type visite.

    [Arguments]    ${dossier_instruction}

    # On ouvre le tableau de bord
    Go To Dashboard
    # On ouvre le menu
    Go To Submenu In Menu    dossiers    dossier_instruction_tous_visites
    # On fait une recherche sur le libellé du DI
    Input Text    css=div#adv-search-adv-fields input#dossier    ${dossier_instruction}
    Click On Search Button
    # On accède à la visualisation du DI
    Click On Link    ${dossier_instruction}
    # On vérifie que le dossier d'instruction apparait dans le titre
    Page Title Should Contain    ${dossier_instruction}


Depuis l'onglet visites du dossier d'instruction

    [Documentation]    Permet d'accéder à l'onglet visites dans le contexte d'un
    ...    dossier d'instruction.

    [Arguments]    ${dossier_instruction}

    #
    Depuis le contexte du dossier d'instruction de type visite    ${dossier_instruction}
    #
    On clique sur l'onglet    visite    Visites


Qualifier le dossier d'instruction

    [Documentation]    Permet de qualifier le dossier d'instruction passé en
    ...    paramétre. Décoche la case à cocher "a_qualifier".

    [Arguments]    ${dossier_instruction_libelle}

    #
    Depuis le formulaire de modification du dossier d'instruction    ${dossier_instruction_libelle}
    # On qualifie le dossier d'instruction
    Unselect Checkbox   css=#a_qualifier
    # On valide le formulaire
    Click On Submit Button
    # On vérifie que le DC a été correctement créé
    Valid Message Should Contain    Vos modifications ont bien été enregistrées.


Clôturer le dossier d'instruction
    [Tags]  dossier_instruction
    [Documentation]  Clôture le DI passé en paramétre.
    [Arguments]  ${dossier_instruction_libelle}

    Depuis le contexte du dossier d'instruction  ${dossier_instruction_libelle}
    Click On Form Portlet Action  dossier_instruction  cloturer
    Valid Message Should Contain  Le dossier d'instruction a été correctement cloturé.


Depuis l'interface d'affectation par lot

    [Documentation]    Permet de qualifier le dossier d'instruction passé en
    ...    paramétre. Décoche la case à cocher "a_qualifier".

    # On ouvre le tableau de bord
    Go To Dashboard
    # On ouvre le menu
    Go To Submenu In Menu    dossiers    dossier_instruction_a_affecter
    # On clique sur l'action 'Affecter par lot'
    Click Element    css=#action-tab-dossier_instruction_a_affecter-corner-affecter-par-lot


Affecter le dossier d'instruction
    [Documentation]  Affecte le techinicien au dossier d'instruction.
    [Arguments]  ${dossier_instruction_libelle}  ${technicien}
    Depuis le formulaire de modification du dossier d'instruction  ${dossier_instruction_libelle}
    Select From List By Label  css=#technicien  ${technicien}
    Click On Submit Button
    Valid Message Should Contain  Vos modifications ont bien été enregistrées.

