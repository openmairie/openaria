*** Settings ***
Documentation     Actions spécifiques aux acteurs.

*** Keywords ***
Depuis le listing des acteurs

    [Documentation]

    #
    Depuis le listing  acteur


Depuis le contexte de l'acteur

    [Documentation]

    [Arguments]    ${acteur}

    #
    Depuis le listing des acteurs
    #
    Use Simple Search    nom et prénom    ${acteur}
    #
    Click On Link    ${acteur}


Ajouter l'acteur
    [Arguments]    ${nom_prenom}    ${utilisateur}=null    ${service}=null    ${role}=null    ${acronyme}=null    ${couleur}=null

    #
    Depuis le listing des acteurs
    Click On Add Button

    # On saisie le nom et prénom
    Input Text    css=#nom_prenom    ${nom_prenom}
    # On sélectionne un utilisateur
    Run Keyword If    '${utilisateur}' != 'null'    Select From List By Label    css=#om_utilisateur    ${utilisateur}
    # On sélectionne le service
    Run Keyword If    '${service}' != 'null'    Select From List By Label    css=#service    ${service}
    # On sélectionne le rôle
    Run Keyword If    '${role}' != 'null'    Select From List By Label    css=#role    ${role}
    #
    Run Keyword If    '${acronyme}' != 'null'    Input Text    css=#acronyme    ${acronyme}
    #
    Run Keyword If    '${couleur}' != 'null'    Input Text    css=#couleur    ${couleur}
    # On valide le formulaire
    Click On Submit Button
    # On vérifie le message affiché à l'utilisateur
    Valid Message Should Be    Vos modifications ont bien été enregistrées.

