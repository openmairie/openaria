*** Settings ***
Documentation     Actions spécifiques aux analyses

*** Keywords ***
Modifier données techniques depuis analyse SI
    [Arguments]    ${DI_SI}    ${alarme}=null    ${ssi}=null    ${conformite}=null    ${alimentation}=null    ${service_securite}=null    ${personnel_jour}=null    ${personnel_nuit}=null    ${effectif_public}=null    ${effectif_personnel}=null

    Accéder à l'analyse du dossier d'instruction    ${DI_SI}
    WUX  Éditer bloc analyse  donnees_techniques
    WUX  Saisir données techniques SI    ${alarme}    ${ssi}    ${conformite}    ${alimentation}    ${service_securite}    ${personnel_jour}    ${personnel_nuit}    ${effectif_public}    ${effectif_personnel}
    Submit Form    css=#form_analyses_modifier
    WUX  Valid Message Should Be    Vos modifications ont bien été enregistrées.
    WUX  Click Element    css=#sousform-analyses a.retour


Saisir données techniques SI
    [Arguments]    ${alarme}=null    ${ssi}=null    ${conformite}=null    ${alimentation}=null    ${service_securite}=null    ${personnel_jour}=null    ${personnel_nuit}=null    ${effectif_public}=null    ${effectif_personnel}=null

    # type d'alarme
    Run Keyword If    '${alarme}' != 'null'    Select From List By Label    css=#si_type_alarme    ${alarme}
    # type SSI
    Run Keyword If    '${ssi}' != 'null'    Select From List By Label    css=#si_type_ssi    ${ssi}
    # conformité L16
    Run Keyword If    '${conformite}' == 'true'    Select From List By Label    css=#si_conformite_l16    Oui
    Run Keyword If    '${conformite}' == 'false'    Select From List By Label    css=#si_conformite_l16    Non
    Run Keyword If    '${conformite}' == 'nc'    Select From List By Label    css=#si_conformite_l16    NC
    # alimentation de remplacement
    Run Keyword If    '${alimentation}' == 'true'    Select From List By Label    css=#si_alimentation_remplacement    Oui
    Run Keyword If    '${alimentation}' == 'false'    Select From List By Label    css=#si_alimentation_remplacement    Non
    Run Keyword If    '${alimentation}' == 'nc'    Select From List By Label    css=#si_alimentation_remplacement    NC
    # Service sécurité
    Run Keyword If    '${service_securite}' == 'true'    Select From List By Label    css=#si_service_securite    Oui
    Run Keyword If    '${service_securite}' == 'false'    Select From List By Label    css=#si_service_securite    Non
    Run Keyword If    '${service_securite}' == 'nc'    Select From List By Label    css=#si_service_securite    NC
    # effectif du personnel de jour
    Run Keyword If    '${personnel_jour}' != 'null'    Input Text    css=#si_personnel_jour    ${personnel_jour}
    # effectif du personnel de nuit
    Run Keyword If    '${personnel_nuit}' != 'null'    Input Text    css=#si_personnel_nuit    ${personnel_nuit}
    # effectif public
    Run Keyword If    '${effectif_public}' != 'null'    Input Text    css=#si_effectif_public    ${effectif_public}
    # effectif personnel
    Run Keyword If    '${effectif_personnel}' != 'null'    Input Text    css=#si_effectif_personnel    ${effectif_personnel}


Accéder à l'analyse du dossier d'instruction

    [Documentation]    Permet d'accèder à l'écran d'analyse d'un dossier d'instruction.
    ...
    ...    Attention : l'utilisateur doit déjà être authentifié pour réaliser cette action.

    [Arguments]    ${dossier_instruction}

    #
    Depuis le contexte du dossier d'instruction    ${dossier_instruction}
    # On accède à l'analyse du DI
    Click Element    css=#analyses
    #
    Page Title Should Contain    ${dossier_instruction}
    Selected Tab Title Should Be    analyses    Analyse
    Wait Until Element Is Visible    titre_etat_analyse
    #

Vérifier état en cours
    WUX  Element Text Should Be    css=#analyse_etat    en cours de rédaction
    Action analyse présente    finaliser
    Action analyse absente    valider
    Action analyse absente    reouvrir_terminee
    Action analyse absente    reouvrir_validee
    Action analyse absente    reouvrir_actee

Vérifier état terminé
    [Arguments]    ${reouvrir}    ${valider}
    WUX  Element Text Should Be    css=#analyse_etat    terminée
    Run Keyword If    '${reouvrir}' == 'true'    Action analyse présente    reouvrir_terminee
    Run Keyword If    '${reouvrir}' == 'false'    Action analyse absente    reouvrir_terminee
    Run Keyword If    '${valider}' == 'true'    Action analyse présente    valider
    Run Keyword If    '${valider}' == 'false'    Action analyse absente    valider
    Action analyse absente    finaliser
    Action analyse absente    reouvrir_validee
    Action analyse absente    reouvrir_actee

Vérifier état validé
    WUX  Element Text Should Be    css=#analyse_etat    validée
    Action analyse présente    reouvrir_validee
    Action analyse absente    valider
    Action analyse absente    finaliser
    Action analyse absente    reouvrir_terminee
    Action analyse absente    reouvrir_actee

Vérifier état acté
    WUX  Element Text Should Be    css=#analyse_etat    actée
    Action analyse présente    reouvrir_actee
    Action analyse absente    valider
    Action analyse absente    finaliser
    Action analyse absente    reouvrir_terminee
    Action analyse absente    reouvrir_validee

Action analyse présente
    [Arguments]    ${action}
    Page Should Contain Element    css=#sousform-container #portlet-actions #action-form-analyses-${action}

Action analyse absente
    [Arguments]    ${action}
    Page Should Not Contain Element    css=#sousform-container #portlet-actions #action-form-analyses-${action}

Cliquer action analyse
    [Arguments]    ${action}
    WUX  Click Element  css=#action-form-analyses-${action}
    La page ne doit pas contenir d'erreur


Édition analyse en cours est activée
    [Arguments]  ${service}
    Element Should Be Visible  action_edit_en_cours_analyses_type
    Element Should Be Visible  action_edit_en_cours_objet
    Element Should Be Visible  action_edit_en_cours_descriptif_etablissement
    Run Keyword If  '${service}' == 'si'  Element Should Be Visible  action_edit_en_cours_classification_etablissement
    Run Keyword If  '${service}' == 'acc'  Element Should Not Be Visible  action_edit_en_cours_classification_etablissement
    Element Should Be Visible  action_edit_en_cours_donnees_techniques
    Element Should Be Visible  action_edit_en_cours_reglementation_applicable
    Element Should Be Visible  action_edit_en_cours_prescriptions
    Run Keyword If  '${service}' == 'si'  Element Should Be Visible  action_edit_en_cours_documents_presentes
    Run Keyword If  '${service}' == 'acc'  Element Should Not Be Visible  action_edit_en_cours_documents_presentes
    Element Should Be Visible  action_edit_en_cours_essais_realises
    Element Should Be Visible  action_edit_en_cours_compte_rendu
    Element Should Be Visible  action_edit_en_cours_observation
    Element Should Be Visible  action_edit_en_cours_avis_propose
    Run Keyword If  '${service}' == 'si'  Element Should Be Visible  action_edit_en_cours_proposition_decision_ap
    Run Keyword If  '${service}' == 'acc'  Element Should Not Be Visible  action_edit_en_cours_proposition_decision_ap


Édition analyse en cours est désactivée
    Element Should Not Be Visible  action_edit_en_cours_analyses_type
    Element Should Not Be Visible  action_edit_en_cours_objet
    Element Should Not Be Visible  action_edit_en_cours_descriptif_etablissement
    Element Should Not Be Visible  action_edit_en_cours_classification_etablissement
    Element Should Not Be Visible  action_edit_en_cours_donnees_techniques
    Element Should Not Be Visible  action_edit_en_cours_reglementation_applicable
    Element Should Not Be Visible  action_edit_en_cours_prescriptions
    Element Should Not Be Visible  action_edit_en_cours_documents_presentes
    Element Should Not Be Visible  action_edit_en_cours_essais_realises
    Element Should Not Be Visible  action_edit_en_cours_compte_rendu
    Element Should Not Be Visible  action_edit_en_cours_observation
    Element Should Not Be Visible  action_edit_en_cours_avis_propose
    Element Should Not Be Visible  action_edit_en_cours_proposition_decision_ap

Édition analyse terminée est activée
    [Arguments]  ${service}
    Element Should Be Visible  action_edit_termine_analyses_type
    Element Should Be Visible  action_edit_termine_objet
    Element Should Be Visible  action_edit_termine_descriptif_etablissement
    Run Keyword If  '${service}' == 'si'  Element Should Be Visible  action_edit_termine_classification_etablissement
    Run Keyword If  '${service}' == 'acc'  Element Should Not Be Visible  action_edit_termine_classification_etablissement
    Element Should Be Visible  action_edit_termine_donnees_techniques
    Element Should Be Visible  action_edit_termine_reglementation_applicable
    Element Should Be Visible  action_edit_termine_prescriptions
    Run Keyword If  '${service}' == 'si'  Element Should Be Visible  action_edit_termine_documents_presentes
    Run Keyword If  '${service}' == 'acc'  Element Should Not Be Visible  action_edit_termine_documents_presentes
    Element Should Be Visible  action_edit_termine_essais_realises
    Element Should Be Visible  action_edit_termine_compte_rendu
    Element Should Be Visible  action_edit_termine_observation
    Element Should Be Visible  action_edit_termine_avis_propose
    Run Keyword If  '${service}' == 'si'  Element Should Be Visible  action_edit_termine_proposition_decision_ap
    Run Keyword If  '${service}' == 'acc'  Element Should Not Be Visible  action_edit_termine_proposition_decision_ap

Édition analyse terminée est désactivée
    Element Should Not Be Visible  action_edit_termine_analyses_type
    Element Should Not Be Visible  action_edit_termine_objet
    Element Should Not Be Visible  action_edit_termine_descriptif_etablissement
    Element Should Not Be Visible  action_edit_termine_classification_etablissement
    Element Should Not Be Visible  action_edit_termine_donnees_techniques
    Element Should Not Be Visible  action_edit_termine_reglementation_applicable
    Element Should Not Be Visible  action_edit_termine_prescriptions
    Element Should Not Be Visible  action_edit_termine_documents_presentes
    Element Should Not Be Visible  action_edit_termine_essais_realises
    Element Should Not Be Visible  action_edit_termine_compte_rendu
    Element Should Not Be Visible  action_edit_termine_observation
    Element Should Not Be Visible  action_edit_termine_avis_propose
    Element Should Not Be Visible  action_edit_termine_proposition_decision_ap


Éditer bloc analyse

    [Arguments]  ${bloc}  ${etat_analyse}=en_cours

    # On clique sur l'action modifier du bloc passé en paramètre sur la vue de
    # l'analyse
    Click Element  action_edit_${etat_analyse}_${bloc}
    # On attend que le formulaire soit visibile
    WUX  Element Should Be Visible  css=#form_analyses_modifier


Valider l'analyse du dossier d'instruction
    [Arguments]  ${dossier_instruction}
    Accéder à l'analyse du dossier d'instruction  ${dossier_instruction}
    WUX  Click Link    Valider


Valider l'analyse
    Cliquer action analyse  valider
    Valid Message Should Be In Subform  Validation de l'analyse.


Terminer l'analyse
    Cliquer action analyse  finaliser
    Valid Message Should Be In Subform  Rédaction terminée.


Réouvrir l'analyse
    Cliquer action analyse  reouvrir_actee
    Valid Message Should Be In Subform  Réouverture de l'analyse.
