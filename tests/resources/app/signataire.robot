*** Settings ***
Documentation  Objet 'signataire'.


*** Keywords ***
Depuis le listing des signataires
    [Documentation]  Accède directement via URL au listing des signataires.
    Depuis le listing  signataire


Depuis le formulaire d'ajout de signataire
    [Documentation]  Accède directement via URL au formulaire d'ajout d'un signataire.
    Go To  ${PROJECT_URL}${OM_ROUTE_FORM}&obj=signataire&action=0&retour=form


Ajouter le signataire
    [Documentation]  Crée l'enregistrement
    [Arguments]  ${values}
    Depuis le formulaire d'ajout de signataire
    Saisir les valeurs dans le formulaire du signataire  ${values}
    Click On Submit Button
    Valid Message Should Be  Vos modifications ont bien été enregistrées.


Saisir les valeurs dans le formulaire du signataire
    [Documentation]  Remplit le formulaire
    [Arguments]  ${values}
    Si "nom" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "prenom" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "civilite" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "signataire_qualite" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "signature" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "defaut" existe dans "${values}" on execute "Set Checkbox" dans le formulaire
    Si "om_validite_debut" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "om_validite_fin" existe dans "${values}" on execute "Input Datepicker" dans le formulaire


