*** Settings ***
Documentation     Actions spécifiques aux réunions.

*** Keywords ***
Depuis le listing des documents générés du menu suivi

    [Documentation]    Permet d'accéder au lsiting des documents générés depuis
    ...    le menu suivi -> documents générés -> gestion.

    # On clique sur le tableau de bord
    Go To Dashboard
    # On ouvre le menu
    Go To Submenu In Menu    suivi    courrier
    # On vérifie le fil d'Ariane
    Page Title Should Be    Suivi > Documents Générés > Gestion


Depuis le formulaire du suivi par code barres

    [Documentation]    Permet d'accéder au formulaire de suivi des documents
    ...    générés par codes barres.

    # On clique sur le tableau de bord
    Go To Dashboard
    # On ouvre le menu
    Go To Submenu In Menu    suivi    courrier_suivi
    # On vérifie le fil d'Ariane
    Page Title Should Be    Suivi > Documents Générés > Suivi Par Code Barres


Depuis le formulaire de l'impression des éditions RAR

    [Documentation]

    # On clique sur le tableau de bord
    Go To Dashboard
    # On ouvre le menu
    Go To Submenu In Menu    suivi    courrier_rar
    # On vérifie le fil d'Ariane
    Page Title Should Be    Suivi > Documents Générés > Édition RAR


Depuis le contexte du document généré

    [Documentation]    Permet d'accéder au formulaire du document généré.

    [Arguments]    ${courrier}

    Depuis le listing des documents générés du menu suivi
    # On fait une recherche sur le code barres
    Use Simple Search    code barres    ${courrier}
    # On accède à la visualisation du document généré
    Click On Link    ${courrier}
    #
    Page Title Should Contain    ${courrier}


Depuis le formulaire de modification du document généré

    [Documentation]

    [Arguments]    ${courrier}

    Depuis le contexte du document généré    ${courrier}
    # On clique sur l'action modifier
    Click On Form Portlet Action    courrier    modifier


Depuis le contexte du document généré par le menu suivi par code barres

    [Documentation]    Permet d'accéder au formulaire du document généré en
    ...    passant par le menu de suivi par code barres.

    [Arguments]    ${code_barres}

    Depuis le formulaire du suivi par code barres
    # On saisit le code barres
    Input Text  css=#code_barres  ${code_barres}
    # On clique sur le bouton valider
    WUX  Submit Form  courrier_suivi_par_code_barres_form
    # On vérifie que le bon document est ouvert
    WUX  Form Static Value Should Be  code_barres  ${code_barres}


Depuis l'onglet document généré du dossier d'instruction

    [Documentation]    Permet d'accéder à l'onglet document généré dans le contexte d'un dossier d'instruction.

    [Arguments]    ${dossier_instruction}

    #
    Depuis le contexte du dossier d'instruction    ${dossier_instruction}
    # On clique sur l'onglet réunion
    On clique sur l'onglet    courrier    Documents Générés


Depuis l'onglet document généré du dossier de coordination

    [Documentation]    Permet d'accéder à l'onglet document généré dans le
    ...    contexte d'un dossier de coordination.

    [Arguments]    ${dossier_coordination}

    #
    Depuis le contexte du dossier de coordination    ${dossier_coordination}
    # On clique sur l'onglet réunion
    On clique sur l'onglet    courrier    Documents Générés


Depuis l'onglet document généré de l'établissement

    [Documentation]    Permet d'accéder à l'onglet "Documents Générés" dans le
    ...    contexte de l'établissement.

    [Arguments]    ${etablissement_code}=null    ${etablissement_libelle}=null

    #
    Depuis le contexte de l'établissement    ${etablissement_code}    ${etablissement_libelle}
    #
    On clique sur l'onglet    courrier    Documents Générés


Ajouter le document généré depuis le contexte du dossier d'instruction

    [Documentation]    Ajoute un document généré depuis un dossier d'instruction.

    [Arguments]    ${dossier_instruction_libelle}    ${params}    ${courrier_type}    ${modele_edition}    ${courrier_joint}=null    ${complement1_om_html}=null    ${complement2_om_html}=null

    #
    Depuis l'onglet document généré du dossier d'instruction    ${dossier_instruction_libelle}
    # On clique sur l'action ajouter du tableau
    Click On Add Button JS
    #
    Saisir le document généré    ${params}    ${courrier_type}    ${modele_edition}    ${courrier_joint}    ${complement1_om_html}    ${complement2_om_html}
    # On valide le formulaire
    Click On Submit Button In Subform
    # On vérifie que le message de validation
    Valid Message Should Contain In Subform    Vos modifications ont bien été enregistrées.


Ajouter le document généré depuis le contexte du dossier de coordination

    [Documentation]    Ajoute un document généré depuis un dossier de
    ...    coordination.

    [Arguments]    ${dossier_coordination_libelle}    ${params}    ${courrier_type}    ${modele_edition}    ${courrier_joint}=null    ${complement1_om_html}=null    ${complement2_om_html}=null

    #
    Depuis l'onglet document généré du dossier de coordination    ${dossier_coordination_libelle}
    # On clique sur l'action ajouter du tableau
    Click On Add Button JS
    #
    Saisir le document généré    ${params}    ${courrier_type}    ${modele_edition}    ${courrier_joint}    ${complement1_om_html}    ${complement2_om_html}
    # On valide le formulaire
    Click On Submit Button In Subform
    # On vérifie que le message de validation
    Valid Message Should Contain In Subform    Vos modifications ont bien été enregistrées.


Ajouter le document généré depuis le contexte de l'établissement

    [Documentation]    Ajoute un document généré depuis l'établissement.

    [Arguments]    ${etablissement_code}    ${etablissement_libelle}    ${params}    ${courrier_type}    ${modele_edition}    ${courrier_joint}=null    ${complement1_om_html}=null    ${complement2_om_html}=null

    #
    Depuis l'onglet document généré de l'établissement    ${etablissement_code}    ${etablissement_libelle}
    # On clique sur l'action ajouter du tableau
    Click On Add Button JS
    #
    Saisir le document généré    ${params}    ${courrier_type}    ${modele_edition}    ${courrier_joint}    ${complement1_om_html}    ${complement2_om_html}
    # On valide le formulaire
    Click On Submit Button In Subform
    # On vérifie que le message de validation
    Valid Message Should Contain In Subform    Vos modifications ont bien été enregistrées.


Saisir le document généré

    [Documentation]    Remplie les champs du formulaire du document généré.

    [Arguments]    ${params}    ${courrier_type}    ${modele_edition}    ${courrier_joint}=null    ${complement1_om_html}=null    ${complement2_om_html}=null

    # Sélectionne le type du courrier
    Select From List By Label    css=#courrier_type    ${courrier_type}
    # Sélectionne le modèle d'édition
    WUX  Select From List By Label    css=#modele_edition    ${modele_edition}
    # Recherche le courrier joint
    Run keyword If    '${courrier_joint}' != 'null'    Input Text    css=#autocomplete-courrier-search    ${courrier_joint}
    # Sélectionne le courrier joint dans l'autocomplete
    Run keyword If    '${courrier_joint}' != 'null'    WUX  Click Link    ${courrier_joint}
    # Saisit le complément 1
    Run keyword If    '${complement1_om_html}' != 'null'    Input HTML    complement1_om_html    ${complement1_om_html}
    # Saisit le complément 2
    Run keyword If    '${complement2_om_html}' != 'null'    Input HTML    complement2_om_html    ${complement2_om_html}
    # Sélectionne les destinataires
    ${contacts_lies} =    Get From Dictionary    ${params}    contacts_lies
    @{contacts_lies} =    Convert To List    ${contacts_lies}
    Select From List By Label    css=#contacts_lies    @{contacts_lies}


Suivi des dates du documente généré

    [Documentation]    Permet de saisir le suivi des dates du document généré.

    [Arguments]    ${date_envoi_signature}=null    ${date_retour_signature}=null    ${date_envoi_controle_legalite}=null    ${date_retour_controle_legalite}=null    ${date_envoi_rar}=null    ${date_retour_rar}=null

    # Saisit la date d'envoi signature
    Run keyword If    '${date_envoi_signature}' != 'null'    Input Datepicker    date_envoi_signature    ${date_envoi_signature}
    # Saisit la date de retour signature
    Run keyword If    '${date_retour_signature}' != 'null'    Input Datepicker    date_retour_signature    ${date_retour_signature}
    # Saisit la date d'envoi au contrôle légalité
    Run keyword If    '${date_envoi_controle_legalite}' != 'null'    Input Datepicker    date_envoi_controle_legalite    ${date_envoi_controle_legalite}
    # Saisit la date de retour au contrôle légalité
    Run keyword If    '${date_retour_controle_legalite}' != 'null'    Input Datepicker    date_retour_controle_legalite    ${date_retour_controle_legalite}
    # Saisit la date d'envoi de l'AR
    Run keyword If    '${date_envoi_rar}' != 'null'    Input Datepicker    date_envoi_rar    ${date_envoi_rar}
    # Saisit la date de retour de l'AR
    Run keyword If    '${date_retour_rar}' != 'null'    Input Datepicker    date_retour_rar    ${date_retour_rar}


Modifier le document généré depuis le menu

    [Documentation]    Modifie le document généré. La valeur 'null' permet de ne
    ...    pas modifier le champ.

    [Arguments]    ${code_barres}    ${date_envoi_signature}=null    ${date_retour_signature}=null    ${date_envoi_controle_legalite}=null    ${date_retour_controle_legalite}=null    ${date_envoi_rar}=null    ${date_retour_rar}=null

    #
    Depuis le contexte du document généré    ${code_barres}
    # On clique sur l'action modifier
    Click On Form Portlet Action    courrier    modifier
    #
    Suivi des dates du documente généré    ${date_envoi_signature}    ${date_retour_signature}    ${date_envoi_controle_legalite}    ${date_retour_controle_legalite}    ${date_envoi_rar}    ${date_retour_rar}
    # On valide le formulaire
    Click On Submit Button
    # On vérifie le message de validation
    Valid Message Should Contain    Vos modifications ont bien été enregistrées.


Finaliser le document généré

    [Documentation]     Finalise le document généré.

    [Arguments]    ${code_barres}

    #
    Depuis le contexte du document généré    ${code_barres}
    # On finalise le courrier
    Click On Form Portlet Action    courrier    finalise
    # On vérifie le message de validation
    WUX  Valid Message Should Contain    Finalisation correctement effectuée.


Prévisualiser le document généré

    [Documentation]    Prévisualise le PDF du document généré.

    [Arguments]    ${code_barres}    ${expected_text}

    #
    Depuis le contexte du document généré    ${code_barres}
    # On finalise le courrier
    Click On Form Portlet Action    courrier    previsualiser  new_window
    # On ouvre le PDF
    Open PDF  ${OM_PDF_TITLE}
    # On vérifie le contenu
    WUX  Page Should Contain    ${expected_text}
    Page Should Contain    DOCUMENT DE TRAVAIL
    # On revient à la fenêtre principale
    Close PDF


Saisir le texte type

    [Documentation]    Permet d'ajouter un texte-type de document généré à
    ...    à utiliser lors de la rédaction des compléments du document.

    [Arguments]    ${libelle}=null    ${courrier_type}=null    ${contenu_om_html}=null    ${date_debut}=null    ${date_fin}=null

    # Saisit le libellé
    Run keyword If    '${libelle}' != 'null'    Input Text    css=#libelle    ${libelle}
    # Sélectionne le type du courrier
    Run keyword If    '${courrier_type}' != 'null'    Select From List By Label    css=#courrier_type    ${courrier_type}
    # Saisit le contenu
    Run keyword If    '${contenu_om_html}' != 'null'    Input HTML    contenu_om_html    ${contenu_om_html}
    # Saisit la date de début
    Run keyword If    '${date_debut}' != 'null'    Input Datepicker    date_debut    ${date_debut}
    # Saisit la date de fin
    Run keyword If    '${date_fin}' != 'null'    Input Datepicker    date_fin    ${date_fin}


Ajouter le texte type depuis le menu

    [Documentation]    Permet d'ajouter, depuis son menu, un texte-type de
    ...    document généré à utiliser lors de la rédaction des compléments.

    [Arguments]    ${libelle}=null    ${courrier_type}=null    ${contenu_om_html}=null    ${date_debut}=null    ${date_fin}=null

    # On clique sur le tableau de bord
    Go To Dashboard
    # On ouvre le menu
    Depuis le listing  courrier_texte_type
    # On vérifie le fil d'Ariane
    #XXX Page Title Should Be    Paramétrage > Vocabulaires > Textes Types Docs. Gen.
    # On clique sur le bouton ajouter
    Click On Add Button
    #
    Saisir le texte type    ${libelle}    ${courrier_type}    ${contenu_om_html}    ${date_debut}    ${date_fin}
    # On valide le formulaire
    Click On Submit Button
    # On vérifie le message de validation
    Valid Message Should Contain    Vos modifications ont bien été enregistrées.


Depuis la liste des textes types

    [Documentation]    Affiche la liste des textes types disponible pour ce
    ...    champ.

    [Arguments]    ${courrier_texte_type_id}

    # On clique sur le bouton pour insérer un texte-type
    Click Element Until New Element  ${courrier_texte_type_id}  css=.ui-widget-overlay


Insérer une liste de texte type

    [Documentation]    Permet d'insérer un texte-type dans un champ du
    ...    formulaire.

    [Arguments]    ${courrier_texte_type_id}    ${target_field_id}    ${params}

    Depuis la liste des textes types    ${courrier_texte_type_id}
    # Sélectionne une liste de checkbox
    ${liste_texte_type_id} =    Get From Dictionary    ${params}    liste_texte_type_id
    @{liste_texte_type_id} =    Convert To List    ${liste_texte_type_id}
    Select Checkbox From List    @{liste_texte_type_id}
    # On clique sur le bouton ajouter
    Click On Submit Button In Overlay    courrier_texte_type
    # On vérifie que les textes type sont ajoutés
    ${liste_texte_type_contenu} =    Get From Dictionary    ${params}    liste_texte_type_contenu
    @{liste_texte_type_contenu} =    Convert To List    ${liste_texte_type_contenu}
    WUX  Form HTML Should Contain From List    ${target_field_id}    @{liste_texte_type_contenu}


Depuis le contexte du document généré (accès par URL)
    [Documentation]
    [Arguments]  ${document_genere_id}
    Go To  ${PROJECT_URL}${OM_ROUTE_FORM}&obj=courrier&action=3&idx=${document_genere_id}


Récupérer le code barres du document généré à partir de son identifiant
    [Arguments]  ${document_genere_id}
    Depuis le contexte du document généré (accès par URL)  ${document_genere_id}
    ${document_genere_codebarres} =  Get Text  css=span#code_barres
    [Return]  ${document_genere_codebarres}


Récupérer l'uid du fichier finalisé du document généré

    [Arguments]  ${document_genere_codebarre}
    Depuis le formulaire de modification du document généré  ${document_genere_codebarre}
    ${document_genere_fichier_finalise_uid} =  Get Value  css=input#om_fichier_finalise_courrier
    [Return]  ${document_genere_fichier_finalise_uid}


Récupérer l'uid du fichier signé du document généré

    [Arguments]  ${document_genere_codebarre}
    Depuis le formulaire de modification du document généré  ${document_genere_codebarre}
    ${document_genere_fichier_signe_uid} =  Get Value  css=input#om_fichier_signe_courrier
    [Return]  ${document_genere_fichier_signe_uid}


Ajouter le fichier signé du document généré

    [Arguments]  ${document_genere_codebarre}  ${fichier}
    Depuis le formulaire de modification du document généré  ${document_genere_codebarre}
    Add File  om_fichier_signe_courrier  ${fichier}
    Click On Submit Button
    Valid Message Should Contain  Vos modifications ont bien été enregistrées.


Ajouter le modèle d'édition
    [Documentation]  Ajoute un modèle d'édition
    [Arguments]  ${values}

    # On clique sur le tableau de bord
    Go To Dashboard
    # On ouvre le menu
    Depuis le listing  modele_edition
    # On clique sur le bouton ajouter
    Click On Add Button
    # On saisit des valeurs
    Saisir le modèle d'édition  ${values}
    # On valide le formulaire
    Click On Submit Button
    # On vérifie le message de validation
    Valid Message Should Contain  Vos modifications ont bien été enregistrées.
    # On récupère l'ID du nouvel enregistrement
    ${id} =  Get Text  css=div.form-content span#modele_edition
    [Return]  ${id}

Depuis le contexte du modèle d'édition (accès par URL)
    [Documentation]
    [Arguments]  ${id}
    Go To  ${PROJECT_URL}${OM_ROUTE_FORM}&obj=modele_edition&action=3&idx=${id}


Saisir le modèle d'édition
    [Documentation]  Remplit le formulaire du modèle d'édition.
    [Arguments]  ${values}

    #
    Si "om_lettretype_id" existe dans "${values}" on execute "Select From Chosen List For 'Lettre Type' By Name" dans le formulaire
    Si "courrier_type" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    #
    Si "code" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "libelle" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "description" existe dans "${values}" on execute "Input Text" dans le formulaire
    #
    Si "om_validite_debut" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "om_validite_fin" existe dans "${values}" on execute "Input Datepicker" dans le formulaire

