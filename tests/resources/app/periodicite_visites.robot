*** Settings ***
Documentation   Périodicité des visites.

*** Keywords ***
Depuis le controlpanel 'Gestion de la périodicité'
    [Tags]  periodicite_visites
    [Documentation]  Accède au controlpanel 'Gestion de la périodicité'.

    Go To  ${PROJECT_URL}${OM_ROUTE_FORM}&obj=periodicite_visites&action=99&idx=0
    La page ne doit pas contenir d'erreur


Déclencher le traitement de périodicité
    [Tags]  periodicite_visites
    [Documentation]  Déclenche le traitement de périodicité.

    Depuis le controlpanel 'Gestion de la périodicité'
    Click Element  css=#action-controlpanel-periodicite_visites-update_all_dossier_coordination_periodique
    Wait Until Element Is Visible  css=.ui-dialog .ui-dialog-buttonset button
    Click Element  css=.ui-dialog .ui-dialog-buttonset button
    WUX  Valid Message Should Contain   Nombre d'établissement total :
    La page ne doit pas contenir d'erreur

