*** Settings ***
Documentation     Actions spécifiques aux procès verbaux.

*** Keywords ***
Depuis l'onglet PV du DI

    [Documentation]    Permet d'accéder à l'onglet PV dans le contexte d'un
    ...    dossier d'instruction.

    [Arguments]    ${dossier_instruction}

    #
    Depuis le contexte du dossier d'instruction  ${dossier_instruction}
    #
    On clique sur l'onglet  proces_verbal    PV


Depuis le formulaire de génération d'un nouveau PV sur le DI

    [Documentation]  ...

    [Arguments]  ${di}

    #
    Depuis l'onglet PV du DI  ${di}

    # On ouvre la vue de génération d'un nouveau PV
    WUX  Click Element    css=#generer_pv
    # On attend l'ouverture du formulaire
    WUX  Element Should Contain  css=#sousform-proces_verbal  odèle d'édition à utiliser lors de la génération
    # On vérifie que la page ne contient pas d'erreurs
    La page ne doit pas contenir d'erreur


Depuis le formulaire de regénération du dernier PV sur le DI

    [Documentation]  ...

    [Arguments]  ${di}

    #
    Depuis l'onglet PV du DI  ${di}

    # On ouvre la vue de regénération du dernier PV
    WUX  Click Element    css=#regenerer_pv
    # On attend l'ouverture du formulaire
    WUX  Element Should Contain  css=#sousform-proces_verbal  uméro du PV à regénérer
    # On vérifie que la page ne contient pas d'erreurs
    La page ne doit pas contenir d'erreur


Depuis le formulaire d'ajout de PV sur le DI

    [Documentation]  ...

    [Arguments]  ${di}

    #
    Depuis l'onglet PV du DI  ${di}

    # On ouvre la vue d'ajout de PV
    WUX  Click Element    css=#ajouter_pv
    # On attend l'ouverture du formulaire
    WUX  Element Should Contain  css=#sousform-proces_verbal  PV signé
    # On vérifie que la page ne contient pas d'erreurs
    La page ne doit pas contenir d'erreur




Récupérer l'identifiant du document généré lié depuis le procès verbal numéro
    [Documentation]  ...
    [Arguments]  ${dossier_instruction}  ${numero_du_pv}
    Depuis l'onglet PV du DI  ${dossier_instruction}
    Click On Link  ${numero_du_pv}
    WUX  Element Text Should Be  css=span#numero  ${numero_du_pv}
    ${document_genere_id} =  Get Selected List Value  css=#courrier_genere
    Run Keyword If  ${document_genere_id} == ${None}  Fail
    [Return]  ${document_genere_id}


L'action 'Générer un nouveau PV' doit être disponible
    WUX  Element Should Be Visible  css=#generer_pv


L'action 'Regénérer le dernier PV' doit être disponible
    WUX  Element Should Be Visible  css=#regenerer_pv


L'action 'Ajouter un PV' doit être disponible
    WUX  Element Should Be Visible  css=#ajouter_pv


L'action 'Générer un nouveau PV' ne doit pas être disponible
    WUX  Element Should Not Be Visible  css=#generer_pv


L'action 'Regénérer le dernier PV' ne doit pas être disponible
    WUX  Element Should Not Be Visible  css=#regenerer_pv


L'action 'Ajouter un PV' ne doit pas être disponible
    WUX  Element Should Not Be Visible  css=#ajouter_pv


Ajouter un PV sur le DI
    [Documentation]
    [Arguments]  ${dossier_instruction}  ${values}
    Depuis le formulaire d'ajout de PV sur le DI  ${dossier_instruction}
    Saisir les valeurs du formulaire de procès verbal  ${values}
    Click On Submit Button In Subform
    Valid Message Should Contain In Subform  Vos modifications ont bien été enregistrées.


Générer un nouveau PV sur le DI
    [Documentation]
    [Arguments]  ${values}
    Depuis le formulaire de génération d'un nouveau PV sur le DI  ${dossier_instruction}
    Saisir les valeurs du formulaire de procès verbal  ${values}
    Click On Submit Button In Subform
    Valid Message Should Contain In Subform  Vos modifications ont bien été enregistrées.


Regénérer le dernier PV sur le DI
    [Documentation]
    [Arguments]  ${values}
    Depuis le formulaire de regénération du dernier PV sur le DI  ${dossier_instruction}
    Saisir les valeurs du formulaire de procès verbal  ${values}
    Click On Submit Button In Subform
    Valid Message Should Contain In Subform  Vos modifications ont bien été enregistrées.


Récupérer l'uid du fichier signé du procès verbal

    [Arguments]  ${document_genere_codebarre}
    Depuis le formulaire de modification du document généré  ${document_genere_codebarre}
    ${document_genere_fichier_finalise_uid} =  Get Value  css=input#om_fichier_signe
    [Return]  ${document_genere_fichier_finalise_uid}


Saisir les valeurs du formulaire de procès verbal
    [Documentation]   ...
    ...
    ...  &{pv01} =  Create Dictionary
    ...  dossier_instruction_reunion=
    ...  modele_edition=
    ...  date_redaction=
    ...  signataire=
    ...  om_fichier_signe=
    [Arguments]  ${values}
    Si "dossier_instruction_reunion" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "modele_edition" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "date_redaction" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "signataire" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "om_fichier_signe" existe dans "${values}" on execute "Add File" dans le formulaire


Depuis le procès verbal généré dans le contexte du DI
    [Documentation]  Ce keyword permet de se positionner sur la vue d'un PV généré existant
    ...  dans le contexte de son dossier d'instruction
    [Arguments]  ${dossier_instruction}  ${numero_du_pv_genere}
    Depuis l'onglet PV du DI  ${dossier_instruction}
    WUX  Element Should Contain  css=#soustab-proces_verbal  ${numero_du_pv_genere}
    Click Link  ${numero_du_pv_genere}
    WUX  Element Should Contain  css=#numero  ${numero_du_pv_genere}
