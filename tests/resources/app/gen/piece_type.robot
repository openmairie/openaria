*** Settings ***
Documentation    CRUD de la table piece_type
...    @author  generated
...    @package openARIA
...    @version 04/12/2017 12:12

*** Keywords ***

Depuis le contexte type
    [Documentation]  Accède au formulaire
    [Arguments]  ${piece_type}

    # On accède au tableau
    Go To Tab  piece_type
    # On recherche l'enregistrement
    Use Simple Search  type  ${piece_type}
    # On clique sur le résultat
    Click On Link  ${piece_type}
    # On vérifie qu'il n'y a pas d'erreur
    Page Should Not Contain Errors

Ajouter type
    [Documentation]  Crée l'enregistrement
    [Arguments]  ${values}

    # On accède au tableau
    Go To Tab  piece_type
    # On clique sur le bouton ajouter
    Click On Add Button
    # On saisit des valeurs
    Saisir type  ${values}
    # On valide le formulaire
    Click On Submit Button
    # On récupère l'ID du nouvel enregistrement
    ${piece_type} =  Get Text  css=div.form-content span#piece_type
    # On le retourne
    [Return]  ${piece_type}

Modifier type
    [Documentation]  Modifie l'enregistrement
    [Arguments]  ${piece_type}  ${values}

    # On accède à l'enregistrement
    Depuis le contexte type  ${piece_type}
    # On clique sur le bouton modifier
    Click On Form Portlet Action  piece_type  modifier
    # On saisit des valeurs
    Saisir type  ${values}
    # On valide le formulaire
    Click On Submit Button

Supprimer type
    [Documentation]  Supprime l'enregistrement
    [Arguments]  ${piece_type}

    # On accède à l'enregistrement
    Depuis le contexte type  ${piece_type}
    # On clique sur le bouton supprimer
    Click On Form Portlet Action  piece_type  supprimer
    # On valide le formulaire
    Click On Submit Button

Saisir type
    [Documentation]  Remplit le formulaire
    [Arguments]  ${values}
    
    Si "libelle" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "code" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "description" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "om_validite_debut" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "om_validite_fin" existe dans "${values}" on execute "Input Datepicker" dans le formulaire