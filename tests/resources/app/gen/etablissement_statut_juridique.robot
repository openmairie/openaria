*** Settings ***
Documentation    CRUD de la table etablissement_statut_juridique
...    @author  generated
...    @package openARIA
...    @version 04/12/2017 12:12

*** Keywords ***

Depuis le contexte statut juridique
    [Documentation]  Accède au formulaire
    [Arguments]  ${etablissement_statut_juridique}

    # On accède au tableau
    Go To Tab  etablissement_statut_juridique
    # On recherche l'enregistrement
    Use Simple Search  statut juridique  ${etablissement_statut_juridique}
    # On clique sur le résultat
    Click On Link  ${etablissement_statut_juridique}
    # On vérifie qu'il n'y a pas d'erreur
    Page Should Not Contain Errors

Ajouter statut juridique
    [Documentation]  Crée l'enregistrement
    [Arguments]  ${values}

    # On accède au tableau
    Go To Tab  etablissement_statut_juridique
    # On clique sur le bouton ajouter
    Click On Add Button
    # On saisit des valeurs
    Saisir statut juridique  ${values}
    # On valide le formulaire
    Click On Submit Button
    # On récupère l'ID du nouvel enregistrement
    ${etablissement_statut_juridique} =  Get Text  css=div.form-content span#etablissement_statut_juridique
    # On le retourne
    [Return]  ${etablissement_statut_juridique}

Modifier statut juridique
    [Documentation]  Modifie l'enregistrement
    [Arguments]  ${etablissement_statut_juridique}  ${values}

    # On accède à l'enregistrement
    Depuis le contexte statut juridique  ${etablissement_statut_juridique}
    # On clique sur le bouton modifier
    Click On Form Portlet Action  etablissement_statut_juridique  modifier
    # On saisit des valeurs
    Saisir statut juridique  ${values}
    # On valide le formulaire
    Click On Submit Button

Supprimer statut juridique
    [Documentation]  Supprime l'enregistrement
    [Arguments]  ${etablissement_statut_juridique}

    # On accède à l'enregistrement
    Depuis le contexte statut juridique  ${etablissement_statut_juridique}
    # On clique sur le bouton supprimer
    Click On Form Portlet Action  etablissement_statut_juridique  supprimer
    # On valide le formulaire
    Click On Submit Button

Saisir statut juridique
    [Documentation]  Remplit le formulaire
    [Arguments]  ${values}
    
    Si "libelle" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "om_validite_debut" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "om_validite_fin" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "code" existe dans "${values}" on execute "Input Text" dans le formulaire