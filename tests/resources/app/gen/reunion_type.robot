*** Settings ***
Documentation    CRUD de la table reunion_type
...    @author  generated
...    @package openARIA
...    @version 04/12/2017 12:12

*** Keywords ***

Depuis le contexte type de réunion
    [Documentation]  Accède au formulaire
    [Arguments]  ${reunion_type}

    # On accède au tableau
    Go To Tab  reunion_type
    # On recherche l'enregistrement
    Use Simple Search  type de réunion  ${reunion_type}
    # On clique sur le résultat
    Click On Link  ${reunion_type}
    # On vérifie qu'il n'y a pas d'erreur
    Page Should Not Contain Errors

Ajouter type de réunion
    [Documentation]  Crée l'enregistrement
    [Arguments]  ${values}

    # On accède au tableau
    Go To Tab  reunion_type
    # On clique sur le bouton ajouter
    Click On Add Button
    # On saisit des valeurs
    Saisir type de réunion  ${values}
    # On valide le formulaire
    Click On Submit Button
    # On récupère l'ID du nouvel enregistrement
    ${reunion_type} =  Get Text  css=div.form-content span#reunion_type
    # On le retourne
    [Return]  ${reunion_type}

Modifier type de réunion
    [Documentation]  Modifie l'enregistrement
    [Arguments]  ${reunion_type}  ${values}

    # On accède à l'enregistrement
    Depuis le contexte type de réunion  ${reunion_type}
    # On clique sur le bouton modifier
    Click On Form Portlet Action  reunion_type  modifier
    # On saisit des valeurs
    Saisir type de réunion  ${values}
    # On valide le formulaire
    Click On Submit Button

Supprimer type de réunion
    [Documentation]  Supprime l'enregistrement
    [Arguments]  ${reunion_type}

    # On accède à l'enregistrement
    Depuis le contexte type de réunion  ${reunion_type}
    # On clique sur le bouton supprimer
    Click On Form Portlet Action  reunion_type  supprimer
    # On valide le formulaire
    Click On Submit Button

Saisir type de réunion
    [Documentation]  Remplit le formulaire
    [Arguments]  ${values}
    
    Si "code" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "libelle" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "lieu_adresse_ligne1" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "lieu_adresse_ligne2" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "lieu_salle" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "heure" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "listes_de_diffusion" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "modele_courriel_convoquer" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "modele_courriel_cloturer" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "commission" existe dans "${values}" on execute "Set Checkbox" dans le formulaire
    Si "president" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "modele_ordre_du_jour" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "modele_compte_rendu_global" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "modele_compte_rendu_specifique" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "modele_feuille_presence" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "service" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "om_validite_debut" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "om_validite_fin" existe dans "${values}" on execute "Input Datepicker" dans le formulaire