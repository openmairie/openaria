*** Settings ***
Documentation    CRUD de la table courrier_texte_type
...    @author  generated
...    @package openARIA
...    @version 04/12/2017 12:12

*** Keywords ***

Depuis le contexte texte-type
    [Documentation]  Accède au formulaire
    [Arguments]  ${courrier_texte_type}

    # On accède au tableau
    Go To Tab  courrier_texte_type
    # On recherche l'enregistrement
    Use Simple Search  texte-type  ${courrier_texte_type}
    # On clique sur le résultat
    Click On Link  ${courrier_texte_type}
    # On vérifie qu'il n'y a pas d'erreur
    Page Should Not Contain Errors

Ajouter texte-type
    [Documentation]  Crée l'enregistrement
    [Arguments]  ${values}

    # On accède au tableau
    Go To Tab  courrier_texte_type
    # On clique sur le bouton ajouter
    Click On Add Button
    # On saisit des valeurs
    Saisir texte-type  ${values}
    # On valide le formulaire
    Click On Submit Button
    # On récupère l'ID du nouvel enregistrement
    ${courrier_texte_type} =  Get Text  css=div.form-content span#courrier_texte_type
    # On le retourne
    [Return]  ${courrier_texte_type}

Modifier texte-type
    [Documentation]  Modifie l'enregistrement
    [Arguments]  ${courrier_texte_type}  ${values}

    # On accède à l'enregistrement
    Depuis le contexte texte-type  ${courrier_texte_type}
    # On clique sur le bouton modifier
    Click On Form Portlet Action  courrier_texte_type  modifier
    # On saisit des valeurs
    Saisir texte-type  ${values}
    # On valide le formulaire
    Click On Submit Button

Supprimer texte-type
    [Documentation]  Supprime l'enregistrement
    [Arguments]  ${courrier_texte_type}

    # On accède à l'enregistrement
    Depuis le contexte texte-type  ${courrier_texte_type}
    # On clique sur le bouton supprimer
    Click On Form Portlet Action  courrier_texte_type  supprimer
    # On valide le formulaire
    Click On Submit Button

Saisir texte-type
    [Documentation]  Remplit le formulaire
    [Arguments]  ${values}
    
    Si "libelle" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "courrier_type" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "contenu_om_html" existe dans "${values}" on execute "Input HTML" dans le formulaire
    Si "om_validite_debut" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "om_validite_fin" existe dans "${values}" on execute "Input Datepicker" dans le formulaire