*** Settings ***
Documentation    CRUD de la table etablissement_etat
...    @author  generated
...    @package openARIA
...    @version 04/12/2017 12:12

*** Keywords ***

Depuis le contexte état
    [Documentation]  Accède au formulaire
    [Arguments]  ${etablissement_etat}

    # On accède au tableau
    Go To Tab  etablissement_etat
    # On recherche l'enregistrement
    Use Simple Search  état  ${etablissement_etat}
    # On clique sur le résultat
    Click On Link  ${etablissement_etat}
    # On vérifie qu'il n'y a pas d'erreur
    Page Should Not Contain Errors

Ajouter état
    [Documentation]  Crée l'enregistrement
    [Arguments]  ${values}

    # On accède au tableau
    Go To Tab  etablissement_etat
    # On clique sur le bouton ajouter
    Click On Add Button
    # On saisit des valeurs
    Saisir état  ${values}
    # On valide le formulaire
    Click On Submit Button
    # On récupère l'ID du nouvel enregistrement
    ${etablissement_etat} =  Get Text  css=div.form-content span#etablissement_etat
    # On le retourne
    [Return]  ${etablissement_etat}

Modifier état
    [Documentation]  Modifie l'enregistrement
    [Arguments]  ${etablissement_etat}  ${values}

    # On accède à l'enregistrement
    Depuis le contexte état  ${etablissement_etat}
    # On clique sur le bouton modifier
    Click On Form Portlet Action  etablissement_etat  modifier
    # On saisit des valeurs
    Saisir état  ${values}
    # On valide le formulaire
    Click On Submit Button

Supprimer état
    [Documentation]  Supprime l'enregistrement
    [Arguments]  ${etablissement_etat}

    # On accède à l'enregistrement
    Depuis le contexte état  ${etablissement_etat}
    # On clique sur le bouton supprimer
    Click On Form Portlet Action  etablissement_etat  supprimer
    # On valide le formulaire
    Click On Submit Button

Saisir état
    [Documentation]  Remplit le formulaire
    [Arguments]  ${values}
    
    Si "statut_administratif" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "description" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "om_validite_debut" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "om_validite_fin" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "code" existe dans "${values}" on execute "Input Text" dans le formulaire