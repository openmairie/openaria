*** Settings ***
Documentation    CRUD de la table dossier_type
...    @author  generated
...    @package openARIA
...    @version 04/12/2017 12:12

*** Keywords ***

Depuis le contexte type de dossier
    [Documentation]  Accède au formulaire
    [Arguments]  ${dossier_type}

    # On accède au tableau
    Go To Tab  dossier_type
    # On recherche l'enregistrement
    Use Simple Search  type de dossier  ${dossier_type}
    # On clique sur le résultat
    Click On Link  ${dossier_type}
    # On vérifie qu'il n'y a pas d'erreur
    Page Should Not Contain Errors

Ajouter type de dossier
    [Documentation]  Crée l'enregistrement
    [Arguments]  ${values}

    # On accède au tableau
    Go To Tab  dossier_type
    # On clique sur le bouton ajouter
    Click On Add Button
    # On saisit des valeurs
    Saisir type de dossier  ${values}
    # On valide le formulaire
    Click On Submit Button
    # On récupère l'ID du nouvel enregistrement
    ${dossier_type} =  Get Text  css=div.form-content span#dossier_type
    # On le retourne
    [Return]  ${dossier_type}

Modifier type de dossier
    [Documentation]  Modifie l'enregistrement
    [Arguments]  ${dossier_type}  ${values}

    # On accède à l'enregistrement
    Depuis le contexte type de dossier  ${dossier_type}
    # On clique sur le bouton modifier
    Click On Form Portlet Action  dossier_type  modifier
    # On saisit des valeurs
    Saisir type de dossier  ${values}
    # On valide le formulaire
    Click On Submit Button

Supprimer type de dossier
    [Documentation]  Supprime l'enregistrement
    [Arguments]  ${dossier_type}

    # On accède à l'enregistrement
    Depuis le contexte type de dossier  ${dossier_type}
    # On clique sur le bouton supprimer
    Click On Form Portlet Action  dossier_type  supprimer
    # On valide le formulaire
    Click On Submit Button

Saisir type de dossier
    [Documentation]  Remplit le formulaire
    [Arguments]  ${values}
    
    Si "code" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "libelle" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "description" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "om_validite_debut" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "om_validite_fin" existe dans "${values}" on execute "Input Datepicker" dans le formulaire