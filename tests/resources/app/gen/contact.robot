*** Settings ***
Documentation    CRUD de la table contact
...    @author  generated
...    @package openARIA
...    @version 04/12/2017 12:12

*** Keywords ***

Depuis le contexte contact
    [Documentation]  Accède au formulaire
    [Arguments]  ${contact}

    # On accède au tableau
    Go To Tab  contact
    # On recherche l'enregistrement
    Use Simple Search  contact  ${contact}
    # On clique sur le résultat
    Click On Link  ${contact}
    # On vérifie qu'il n'y a pas d'erreur
    Page Should Not Contain Errors

Ajouter contact
    [Documentation]  Crée l'enregistrement
    [Arguments]  ${values}

    # On accède au tableau
    Go To Tab  contact
    # On clique sur le bouton ajouter
    Click On Add Button
    # On saisit des valeurs
    Saisir contact  ${values}
    # On valide le formulaire
    Click On Submit Button
    # On récupère l'ID du nouvel enregistrement
    ${contact} =  Get Text  css=div.form-content span#contact
    # On le retourne
    [Return]  ${contact}

Modifier contact
    [Documentation]  Modifie l'enregistrement
    [Arguments]  ${contact}  ${values}

    # On accède à l'enregistrement
    Depuis le contexte contact  ${contact}
    # On clique sur le bouton modifier
    Click On Form Portlet Action  contact  modifier
    # On saisit des valeurs
    Saisir contact  ${values}
    # On valide le formulaire
    Click On Submit Button

Supprimer contact
    [Documentation]  Supprime l'enregistrement
    [Arguments]  ${contact}

    # On accède à l'enregistrement
    Depuis le contexte contact  ${contact}
    # On clique sur le bouton supprimer
    Click On Form Portlet Action  contact  supprimer
    # On valide le formulaire
    Click On Submit Button

Saisir contact
    [Documentation]  Remplit le formulaire
    [Arguments]  ${values}
    
    Si "etablissement" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "contact_type" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "civilite" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "nom" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "prenom" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "titre" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "telephone" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "mobile" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "fax" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "courriel" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "om_validite_debut" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "om_validite_fin" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "adresse_numero" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "adresse_numero2" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "adresse_voie" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "adresse_complement" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "adresse_cp" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "adresse_ville" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "lieu_dit" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "boite_postale" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "cedex" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "pays" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "qualite" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "denomination" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "raison_sociale" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "siret" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "categorie_juridique" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "reception_convocation" existe dans "${values}" on execute "Set Checkbox" dans le formulaire
    Si "service" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "reception_programmation" existe dans "${values}" on execute "Set Checkbox" dans le formulaire
    Si "reception_commission" existe dans "${values}" on execute "Set Checkbox" dans le formulaire