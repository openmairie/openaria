*** Settings ***
Documentation    CRUD de la table visite_motif_annulation
...    @author  generated
...    @package openARIA
...    @version 04/12/2017 12:12

*** Keywords ***

Depuis le contexte motif de l'annulation de la visite
    [Documentation]  Accède au formulaire
    [Arguments]  ${visite_motif_annulation}

    # On accède au tableau
    Go To Tab  visite_motif_annulation
    # On recherche l'enregistrement
    Use Simple Search  motif de l'annulation de la visite  ${visite_motif_annulation}
    # On clique sur le résultat
    Click On Link  ${visite_motif_annulation}
    # On vérifie qu'il n'y a pas d'erreur
    Page Should Not Contain Errors

Ajouter motif de l'annulation de la visite
    [Documentation]  Crée l'enregistrement
    [Arguments]  ${values}

    # On accède au tableau
    Go To Tab  visite_motif_annulation
    # On clique sur le bouton ajouter
    Click On Add Button
    # On saisit des valeurs
    Saisir motif de l'annulation de la visite  ${values}
    # On valide le formulaire
    Click On Submit Button
    # On récupère l'ID du nouvel enregistrement
    ${visite_motif_annulation} =  Get Text  css=div.form-content span#visite_motif_annulation
    # On le retourne
    [Return]  ${visite_motif_annulation}

Modifier motif de l'annulation de la visite
    [Documentation]  Modifie l'enregistrement
    [Arguments]  ${visite_motif_annulation}  ${values}

    # On accède à l'enregistrement
    Depuis le contexte motif de l'annulation de la visite  ${visite_motif_annulation}
    # On clique sur le bouton modifier
    Click On Form Portlet Action  visite_motif_annulation  modifier
    # On saisit des valeurs
    Saisir motif de l'annulation de la visite  ${values}
    # On valide le formulaire
    Click On Submit Button

Supprimer motif de l'annulation de la visite
    [Documentation]  Supprime l'enregistrement
    [Arguments]  ${visite_motif_annulation}

    # On accède à l'enregistrement
    Depuis le contexte motif de l'annulation de la visite  ${visite_motif_annulation}
    # On clique sur le bouton supprimer
    Click On Form Portlet Action  visite_motif_annulation  supprimer
    # On valide le formulaire
    Click On Submit Button

Saisir motif de l'annulation de la visite
    [Documentation]  Remplit le formulaire
    [Arguments]  ${values}
    
    Si "code" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "libelle" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "description" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "om_validite_debut" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "om_validite_fin" existe dans "${values}" on execute "Input Datepicker" dans le formulaire