*** Settings ***
Documentation    CRUD de la table dossier_instruction
...    @author  generated
...    @package openARIA
...    @version 04/12/2017 12:12

*** Keywords ***

Depuis le contexte dossier d'instruction
    [Documentation]  Accède au formulaire
    [Arguments]  ${dossier_instruction}

    # On accède au tableau
    Go To Tab  dossier_instruction
    # On recherche l'enregistrement
    Use Simple Search  dossier d'instruction  ${dossier_instruction}
    # On clique sur le résultat
    Click On Link  ${dossier_instruction}
    # On vérifie qu'il n'y a pas d'erreur
    Page Should Not Contain Errors

Ajouter dossier d'instruction
    [Documentation]  Crée l'enregistrement
    [Arguments]  ${values}

    # On accède au tableau
    Go To Tab  dossier_instruction
    # On clique sur le bouton ajouter
    Click On Add Button
    # On saisit des valeurs
    Saisir dossier d'instruction  ${values}
    # On valide le formulaire
    Click On Submit Button
    # On récupère l'ID du nouvel enregistrement
    ${dossier_instruction} =  Get Text  css=div.form-content span#dossier_instruction
    # On le retourne
    [Return]  ${dossier_instruction}

Modifier dossier d'instruction
    [Documentation]  Modifie l'enregistrement
    [Arguments]  ${dossier_instruction}  ${values}

    # On accède à l'enregistrement
    Depuis le contexte dossier d'instruction  ${dossier_instruction}
    # On clique sur le bouton modifier
    Click On Form Portlet Action  dossier_instruction  modifier
    # On saisit des valeurs
    Saisir dossier d'instruction  ${values}
    # On valide le formulaire
    Click On Submit Button

Supprimer dossier d'instruction
    [Documentation]  Supprime l'enregistrement
    [Arguments]  ${dossier_instruction}

    # On accède à l'enregistrement
    Depuis le contexte dossier d'instruction  ${dossier_instruction}
    # On clique sur le bouton supprimer
    Click On Form Portlet Action  dossier_instruction  supprimer
    # On valide le formulaire
    Click On Submit Button

Saisir dossier d'instruction
    [Documentation]  Remplit le formulaire
    [Arguments]  ${values}
    
    Si "libelle" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "dossier_coordination" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "technicien" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "service" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "a_qualifier" existe dans "${values}" on execute "Set Checkbox" dans le formulaire
    Si "incompletude" existe dans "${values}" on execute "Set Checkbox" dans le formulaire
    Si "piece_attendue" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "description" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "notes" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "autorite_competente" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "dossier_cloture" existe dans "${values}" on execute "Set Checkbox" dans le formulaire
    Si "prioritaire" existe dans "${values}" on execute "Set Checkbox" dans le formulaire
    Si "statut" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "date_cloture" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "date_ouverture" existe dans "${values}" on execute "Input Datepicker" dans le formulaire