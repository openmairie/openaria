*** Settings ***
Documentation    CRUD de la table document_presente
...    @author  generated
...    @package openARIA
...    @version 04/12/2017 12:12

*** Keywords ***

Depuis le contexte document présenté
    [Documentation]  Accède au formulaire
    [Arguments]  ${document_presente}

    # On accède au tableau
    Go To Tab  document_presente
    # On recherche l'enregistrement
    Use Simple Search  document présenté  ${document_presente}
    # On clique sur le résultat
    Click On Link  ${document_presente}
    # On vérifie qu'il n'y a pas d'erreur
    Page Should Not Contain Errors

Ajouter document présenté
    [Documentation]  Crée l'enregistrement
    [Arguments]  ${values}

    # On accède au tableau
    Go To Tab  document_presente
    # On clique sur le bouton ajouter
    Click On Add Button
    # On saisit des valeurs
    Saisir document présenté  ${values}
    # On valide le formulaire
    Click On Submit Button
    # On récupère l'ID du nouvel enregistrement
    ${document_presente} =  Get Text  css=div.form-content span#document_presente
    # On le retourne
    [Return]  ${document_presente}

Modifier document présenté
    [Documentation]  Modifie l'enregistrement
    [Arguments]  ${document_presente}  ${values}

    # On accède à l'enregistrement
    Depuis le contexte document présenté  ${document_presente}
    # On clique sur le bouton modifier
    Click On Form Portlet Action  document_presente  modifier
    # On saisit des valeurs
    Saisir document présenté  ${values}
    # On valide le formulaire
    Click On Submit Button

Supprimer document présenté
    [Documentation]  Supprime l'enregistrement
    [Arguments]  ${document_presente}

    # On accède à l'enregistrement
    Depuis le contexte document présenté  ${document_presente}
    # On clique sur le bouton supprimer
    Click On Form Portlet Action  document_presente  supprimer
    # On valide le formulaire
    Click On Submit Button

Saisir document présenté
    [Documentation]  Remplit le formulaire
    [Arguments]  ${values}
    
    Si "service" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "libelle" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "description_om_html" existe dans "${values}" on execute "Input HTML" dans le formulaire
    Si "om_validite_debut" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "om_validite_fin" existe dans "${values}" on execute "Input Datepicker" dans le formulaire