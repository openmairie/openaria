*** Settings ***
Documentation    CRUD de la table acteur_plage_visite
...    @author  generated
...    @package openARIA
...    @version 04/12/2017 12:12

*** Keywords ***

Depuis le contexte plages privilégiées de visites
    [Documentation]  Accède au formulaire
    [Arguments]  ${acteur_plage_visite}

    # On accède au tableau
    Go To Tab  acteur_plage_visite
    # On recherche l'enregistrement
    Use Simple Search  plages privilégiées de visites  ${acteur_plage_visite}
    # On clique sur le résultat
    Click On Link  ${acteur_plage_visite}
    # On vérifie qu'il n'y a pas d'erreur
    Page Should Not Contain Errors

Ajouter plages privilégiées de visites
    [Documentation]  Crée l'enregistrement
    [Arguments]  ${values}

    # On accède au tableau
    Go To Tab  acteur_plage_visite
    # On clique sur le bouton ajouter
    Click On Add Button
    # On saisit des valeurs
    Saisir plages privilégiées de visites  ${values}
    # On valide le formulaire
    Click On Submit Button
    # On récupère l'ID du nouvel enregistrement
    ${acteur_plage_visite} =  Get Text  css=div.form-content span#acteur_plage_visite
    # On le retourne
    [Return]  ${acteur_plage_visite}

Modifier plages privilégiées de visites
    [Documentation]  Modifie l'enregistrement
    [Arguments]  ${acteur_plage_visite}  ${values}

    # On accède à l'enregistrement
    Depuis le contexte plages privilégiées de visites  ${acteur_plage_visite}
    # On clique sur le bouton modifier
    Click On Form Portlet Action  acteur_plage_visite  modifier
    # On saisit des valeurs
    Saisir plages privilégiées de visites  ${values}
    # On valide le formulaire
    Click On Submit Button

Supprimer plages privilégiées de visites
    [Documentation]  Supprime l'enregistrement
    [Arguments]  ${acteur_plage_visite}

    # On accède à l'enregistrement
    Depuis le contexte plages privilégiées de visites  ${acteur_plage_visite}
    # On clique sur le bouton supprimer
    Click On Form Portlet Action  acteur_plage_visite  supprimer
    # On valide le formulaire
    Click On Submit Button

Saisir plages privilégiées de visites
    [Documentation]  Remplit le formulaire
    [Arguments]  ${values}
    
    Si "acteur" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "lundi_matin" existe dans "${values}" on execute "Set Checkbox" dans le formulaire
    Si "lundi_apresmidi" existe dans "${values}" on execute "Set Checkbox" dans le formulaire
    Si "mardi_matin" existe dans "${values}" on execute "Set Checkbox" dans le formulaire
    Si "mardi_apresmidi" existe dans "${values}" on execute "Set Checkbox" dans le formulaire
    Si "mercredi_matin" existe dans "${values}" on execute "Set Checkbox" dans le formulaire
    Si "mercredi_apresmidi" existe dans "${values}" on execute "Set Checkbox" dans le formulaire
    Si "jeudi_matin" existe dans "${values}" on execute "Set Checkbox" dans le formulaire
    Si "jeudi_apresmidi" existe dans "${values}" on execute "Set Checkbox" dans le formulaire
    Si "vendredi_matin" existe dans "${values}" on execute "Set Checkbox" dans le formulaire
    Si "vendredi_apresmidi" existe dans "${values}" on execute "Set Checkbox" dans le formulaire