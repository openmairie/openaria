*** Settings ***
Documentation    CRUD de la table signataire_qualite
...    @author  generated
...    @package openARIA
...    @version 04/12/2017 12:12

*** Keywords ***

Depuis le contexte qualité du signataire
    [Documentation]  Accède au formulaire
    [Arguments]  ${signataire_qualite}

    # On accède au tableau
    Go To Tab  signataire_qualite
    # On recherche l'enregistrement
    Use Simple Search  qualité du signataire  ${signataire_qualite}
    # On clique sur le résultat
    Click On Link  ${signataire_qualite}
    # On vérifie qu'il n'y a pas d'erreur
    Page Should Not Contain Errors

Ajouter qualité du signataire
    [Documentation]  Crée l'enregistrement
    [Arguments]  ${values}

    # On accède au tableau
    Go To Tab  signataire_qualite
    # On clique sur le bouton ajouter
    Click On Add Button
    # On saisit des valeurs
    Saisir qualité du signataire  ${values}
    # On valide le formulaire
    Click On Submit Button
    # On récupère l'ID du nouvel enregistrement
    ${signataire_qualite} =  Get Text  css=div.form-content span#signataire_qualite
    # On le retourne
    [Return]  ${signataire_qualite}

Modifier qualité du signataire
    [Documentation]  Modifie l'enregistrement
    [Arguments]  ${signataire_qualite}  ${values}

    # On accède à l'enregistrement
    Depuis le contexte qualité du signataire  ${signataire_qualite}
    # On clique sur le bouton modifier
    Click On Form Portlet Action  signataire_qualite  modifier
    # On saisit des valeurs
    Saisir qualité du signataire  ${values}
    # On valide le formulaire
    Click On Submit Button

Supprimer qualité du signataire
    [Documentation]  Supprime l'enregistrement
    [Arguments]  ${signataire_qualite}

    # On accède à l'enregistrement
    Depuis le contexte qualité du signataire  ${signataire_qualite}
    # On clique sur le bouton supprimer
    Click On Form Portlet Action  signataire_qualite  supprimer
    # On valide le formulaire
    Click On Submit Button

Saisir qualité du signataire
    [Documentation]  Remplit le formulaire
    [Arguments]  ${values}
    
    Si "code" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "libelle" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "description" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "om_validite_debut" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "om_validite_fin" existe dans "${values}" on execute "Input Datepicker" dans le formulaire