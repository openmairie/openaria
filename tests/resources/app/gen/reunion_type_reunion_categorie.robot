*** Settings ***
Documentation    CRUD de la table reunion_type_reunion_categorie
...    @author  generated
...    @package openARIA
...    @version 04/12/2017 12:12

*** Keywords ***

Depuis le contexte lien entre le type de la réunion et la catégorie de la réunion
    [Documentation]  Accède au formulaire
    [Arguments]  ${reunion_type_reunion_categorie}

    # On accède au tableau
    Go To Tab  reunion_type_reunion_categorie
    # On recherche l'enregistrement
    Use Simple Search  lien entre le type de la réunion et la catégorie de la réunion  ${reunion_type_reunion_categorie}
    # On clique sur le résultat
    Click On Link  ${reunion_type_reunion_categorie}
    # On vérifie qu'il n'y a pas d'erreur
    Page Should Not Contain Errors

Ajouter lien entre le type de la réunion et la catégorie de la réunion
    [Documentation]  Crée l'enregistrement
    [Arguments]  ${values}

    # On accède au tableau
    Go To Tab  reunion_type_reunion_categorie
    # On clique sur le bouton ajouter
    Click On Add Button
    # On saisit des valeurs
    Saisir lien entre le type de la réunion et la catégorie de la réunion  ${values}
    # On valide le formulaire
    Click On Submit Button
    # On récupère l'ID du nouvel enregistrement
    ${reunion_type_reunion_categorie} =  Get Text  css=div.form-content span#reunion_type_reunion_categorie
    # On le retourne
    [Return]  ${reunion_type_reunion_categorie}

Modifier lien entre le type de la réunion et la catégorie de la réunion
    [Documentation]  Modifie l'enregistrement
    [Arguments]  ${reunion_type_reunion_categorie}  ${values}

    # On accède à l'enregistrement
    Depuis le contexte lien entre le type de la réunion et la catégorie de la réunion  ${reunion_type_reunion_categorie}
    # On clique sur le bouton modifier
    Click On Form Portlet Action  reunion_type_reunion_categorie  modifier
    # On saisit des valeurs
    Saisir lien entre le type de la réunion et la catégorie de la réunion  ${values}
    # On valide le formulaire
    Click On Submit Button

Supprimer lien entre le type de la réunion et la catégorie de la réunion
    [Documentation]  Supprime l'enregistrement
    [Arguments]  ${reunion_type_reunion_categorie}

    # On accède à l'enregistrement
    Depuis le contexte lien entre le type de la réunion et la catégorie de la réunion  ${reunion_type_reunion_categorie}
    # On clique sur le bouton supprimer
    Click On Form Portlet Action  reunion_type_reunion_categorie  supprimer
    # On valide le formulaire
    Click On Submit Button

Saisir lien entre le type de la réunion et la catégorie de la réunion
    [Documentation]  Remplit le formulaire
    [Arguments]  ${values}
    
    Si "reunion_type" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "reunion_categorie" existe dans "${values}" on execute "Select From List By Label" dans le formulaire