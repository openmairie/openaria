*** Settings ***
Documentation    CRUD de la table dossier_coordination
...    @author  generated
...    @package openARIA
...    @version 04/12/2017 12:12

*** Keywords ***

Depuis le contexte dossier de coordination
    [Documentation]  Accède au formulaire
    [Arguments]  ${dossier_coordination}

    # On accède au tableau
    Go To Tab  dossier_coordination
    # On recherche l'enregistrement
    Use Simple Search  dossier de coordination  ${dossier_coordination}
    # On clique sur le résultat
    Click On Link  ${dossier_coordination}
    # On vérifie qu'il n'y a pas d'erreur
    Page Should Not Contain Errors

Ajouter dossier de coordination
    [Documentation]  Crée l'enregistrement
    [Arguments]  ${values}

    # On accède au tableau
    Go To Tab  dossier_coordination
    # On clique sur le bouton ajouter
    Click On Add Button
    # On saisit des valeurs
    Saisir dossier de coordination  ${values}
    # On valide le formulaire
    Click On Submit Button
    # On récupère l'ID du nouvel enregistrement
    ${dossier_coordination} =  Get Text  css=div.form-content span#dossier_coordination
    # On le retourne
    [Return]  ${dossier_coordination}

Modifier dossier de coordination
    [Documentation]  Modifie l'enregistrement
    [Arguments]  ${dossier_coordination}  ${values}

    # On accède à l'enregistrement
    Depuis le contexte dossier de coordination  ${dossier_coordination}
    # On clique sur le bouton modifier
    Click On Form Portlet Action  dossier_coordination  modifier
    # On saisit des valeurs
    Saisir dossier de coordination  ${values}
    # On valide le formulaire
    Click On Submit Button

Supprimer dossier de coordination
    [Documentation]  Supprime l'enregistrement
    [Arguments]  ${dossier_coordination}

    # On accède à l'enregistrement
    Depuis le contexte dossier de coordination  ${dossier_coordination}
    # On clique sur le bouton supprimer
    Click On Form Portlet Action  dossier_coordination  supprimer
    # On valide le formulaire
    Click On Submit Button

Saisir dossier de coordination
    [Documentation]  Remplit le formulaire
    [Arguments]  ${values}
    
    Si "etablissement" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "libelle" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "dossier_coordination_type" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "date_demande" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "date_butoir" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "dossier_autorisation_ads" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "dossier_instruction_ads" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "a_qualifier" existe dans "${values}" on execute "Set Checkbox" dans le formulaire
    Si "etablissement_type" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "etablissement_categorie" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "erp" existe dans "${values}" on execute "Set Checkbox" dans le formulaire
    Si "dossier_cloture" existe dans "${values}" on execute "Set Checkbox" dans le formulaire
    Si "contraintes_urba_om_html" existe dans "${values}" on execute "Input HTML" dans le formulaire
    Si "etablissement_locaux_sommeil" existe dans "${values}" on execute "Set Checkbox" dans le formulaire
    Si "references_cadastrales" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "dossier_coordination_parent" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "description" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "dossier_instruction_secu" existe dans "${values}" on execute "Set Checkbox" dans le formulaire
    Si "dossier_instruction_acc" existe dans "${values}" on execute "Set Checkbox" dans le formulaire
    Si "autorite_police_encours" existe dans "${values}" on execute "Set Checkbox" dans le formulaire
    Si "date_cloture" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "geolocalise" existe dans "${values}" on execute "Set Checkbox" dans le formulaire
    Si "interface_referentiel_ads" existe dans "${values}" on execute "Set Checkbox" dans le formulaire
    Si "enjeu_erp" existe dans "${values}" on execute "Set Checkbox" dans le formulaire
    Si "depot_de_piece" existe dans "${values}" on execute "Set Checkbox" dans le formulaire
    Si "enjeu_ads" existe dans "${values}" on execute "Set Checkbox" dans le formulaire
    Si "geom_point" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "geom_emprise" existe dans "${values}" on execute "Input Text" dans le formulaire