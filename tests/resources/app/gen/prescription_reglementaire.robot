*** Settings ***
Documentation    CRUD de la table prescription_reglementaire
...    @author  generated
...    @package openARIA
...    @version 04/12/2017 12:12

*** Keywords ***

Depuis le contexte prescription réglementaire
    [Documentation]  Accède au formulaire
    [Arguments]  ${prescription_reglementaire}

    # On accède au tableau
    Go To Tab  prescription_reglementaire
    # On recherche l'enregistrement
    Use Simple Search  prescription réglementaire  ${prescription_reglementaire}
    # On clique sur le résultat
    Click On Link  ${prescription_reglementaire}
    # On vérifie qu'il n'y a pas d'erreur
    Page Should Not Contain Errors

Ajouter prescription réglementaire
    [Documentation]  Crée l'enregistrement
    [Arguments]  ${values}

    # On accède au tableau
    Go To Tab  prescription_reglementaire
    # On clique sur le bouton ajouter
    Click On Add Button
    # On saisit des valeurs
    Saisir prescription réglementaire  ${values}
    # On valide le formulaire
    Click On Submit Button
    # On récupère l'ID du nouvel enregistrement
    ${prescription_reglementaire} =  Get Text  css=div.form-content span#prescription_reglementaire
    # On le retourne
    [Return]  ${prescription_reglementaire}

Modifier prescription réglementaire
    [Documentation]  Modifie l'enregistrement
    [Arguments]  ${prescription_reglementaire}  ${values}

    # On accède à l'enregistrement
    Depuis le contexte prescription réglementaire  ${prescription_reglementaire}
    # On clique sur le bouton modifier
    Click On Form Portlet Action  prescription_reglementaire  modifier
    # On saisit des valeurs
    Saisir prescription réglementaire  ${values}
    # On valide le formulaire
    Click On Submit Button

Supprimer prescription réglementaire
    [Documentation]  Supprime l'enregistrement
    [Arguments]  ${prescription_reglementaire}

    # On accède à l'enregistrement
    Depuis le contexte prescription réglementaire  ${prescription_reglementaire}
    # On clique sur le bouton supprimer
    Click On Form Portlet Action  prescription_reglementaire  supprimer
    # On valide le formulaire
    Click On Submit Button

Saisir prescription réglementaire
    [Documentation]  Remplit le formulaire
    [Arguments]  ${values}
    
    Si "service" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "tete_de_chapitre1" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "tete_de_chapitre2" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "libelle" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "description_pr_om_html" existe dans "${values}" on execute "Input HTML" dans le formulaire
    Si "defavorable" existe dans "${values}" on execute "Set Checkbox" dans le formulaire
    Si "om_validite_debut" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "om_validite_fin" existe dans "${values}" on execute "Input Datepicker" dans le formulaire