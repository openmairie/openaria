*** Settings ***
Documentation    CRUD de la table piece
...    @author  generated
...    @package openARIA
...    @version 04/12/2017 12:12

*** Keywords ***

Depuis le contexte pièce
    [Documentation]  Accède au formulaire
    [Arguments]  ${piece}

    # On accède au tableau
    Go To Tab  piece
    # On recherche l'enregistrement
    Use Simple Search  pièce  ${piece}
    # On clique sur le résultat
    Click On Link  ${piece}
    # On vérifie qu'il n'y a pas d'erreur
    Page Should Not Contain Errors

Ajouter pièce
    [Documentation]  Crée l'enregistrement
    [Arguments]  ${values}

    # On accède au tableau
    Go To Tab  piece
    # On clique sur le bouton ajouter
    Click On Add Button
    # On saisit des valeurs
    Saisir pièce  ${values}
    # On valide le formulaire
    Click On Submit Button
    # On récupère l'ID du nouvel enregistrement
    ${piece} =  Get Text  css=div.form-content span#piece
    # On le retourne
    [Return]  ${piece}

Modifier pièce
    [Documentation]  Modifie l'enregistrement
    [Arguments]  ${piece}  ${values}

    # On accède à l'enregistrement
    Depuis le contexte pièce  ${piece}
    # On clique sur le bouton modifier
    Click On Form Portlet Action  piece  modifier
    # On saisit des valeurs
    Saisir pièce  ${values}
    # On valide le formulaire
    Click On Submit Button

Supprimer pièce
    [Documentation]  Supprime l'enregistrement
    [Arguments]  ${piece}

    # On accède à l'enregistrement
    Depuis le contexte pièce  ${piece}
    # On clique sur le bouton supprimer
    Click On Form Portlet Action  piece  supprimer
    # On valide le formulaire
    Click On Submit Button

Saisir pièce
    [Documentation]  Remplit le formulaire
    [Arguments]  ${values}
    
    Si "etablissement" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "dossier_coordination" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "nom" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "piece_type" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "uid" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "om_date_creation" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "dossier_instruction" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "date_reception" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "date_emission" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "piece_statut" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "date_butoir" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "suivi" existe dans "${values}" on execute "Set Checkbox" dans le formulaire
    Si "commentaire_suivi" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "lu" existe dans "${values}" on execute "Set Checkbox" dans le formulaire
    Si "choix_lien" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "service" existe dans "${values}" on execute "Select From List By Label" dans le formulaire