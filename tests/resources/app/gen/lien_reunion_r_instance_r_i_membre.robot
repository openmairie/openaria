*** Settings ***
Documentation    CRUD de la table lien_reunion_r_instance_r_i_membre
...    @author  generated
...    @package openARIA
...    @version 04/12/2017 12:12

*** Keywords ***

Depuis le contexte lien_reunion_r_instance_r_i_membre
    [Documentation]  Accède au formulaire
    [Arguments]  ${lien_reunion_r_instance_r_i_membre}

    # On accède au tableau
    Go To Tab  lien_reunion_r_instance_r_i_membre
    # On recherche l'enregistrement
    Use Simple Search  lien_reunion_r_instance_r_i_membre  ${lien_reunion_r_instance_r_i_membre}
    # On clique sur le résultat
    Click On Link  ${lien_reunion_r_instance_r_i_membre}
    # On vérifie qu'il n'y a pas d'erreur
    Page Should Not Contain Errors

Ajouter lien_reunion_r_instance_r_i_membre
    [Documentation]  Crée l'enregistrement
    [Arguments]  ${values}

    # On accède au tableau
    Go To Tab  lien_reunion_r_instance_r_i_membre
    # On clique sur le bouton ajouter
    Click On Add Button
    # On saisit des valeurs
    Saisir lien_reunion_r_instance_r_i_membre  ${values}
    # On valide le formulaire
    Click On Submit Button
    # On récupère l'ID du nouvel enregistrement
    ${lien_reunion_r_instance_r_i_membre} =  Get Text  css=div.form-content span#lien_reunion_r_instance_r_i_membre
    # On le retourne
    [Return]  ${lien_reunion_r_instance_r_i_membre}

Modifier lien_reunion_r_instance_r_i_membre
    [Documentation]  Modifie l'enregistrement
    [Arguments]  ${lien_reunion_r_instance_r_i_membre}  ${values}

    # On accède à l'enregistrement
    Depuis le contexte lien_reunion_r_instance_r_i_membre  ${lien_reunion_r_instance_r_i_membre}
    # On clique sur le bouton modifier
    Click On Form Portlet Action  lien_reunion_r_instance_r_i_membre  modifier
    # On saisit des valeurs
    Saisir lien_reunion_r_instance_r_i_membre  ${values}
    # On valide le formulaire
    Click On Submit Button

Supprimer lien_reunion_r_instance_r_i_membre
    [Documentation]  Supprime l'enregistrement
    [Arguments]  ${lien_reunion_r_instance_r_i_membre}

    # On accède à l'enregistrement
    Depuis le contexte lien_reunion_r_instance_r_i_membre  ${lien_reunion_r_instance_r_i_membre}
    # On clique sur le bouton supprimer
    Click On Form Portlet Action  lien_reunion_r_instance_r_i_membre  supprimer
    # On valide le formulaire
    Click On Submit Button

Saisir lien_reunion_r_instance_r_i_membre
    [Documentation]  Remplit le formulaire
    [Arguments]  ${values}
    
    Si "reunion" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "reunion_instance" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "reunion_instance_membre" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "observation" existe dans "${values}" on execute "Input Text" dans le formulaire