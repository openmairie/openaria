*** Settings ***
Documentation    CRUD de la table prescription
...    @author  generated
...    @package openARIA
...    @version 04/12/2017 12:12

*** Keywords ***

Depuis le contexte prescription
    [Documentation]  Accède au formulaire
    [Arguments]  ${prescription}

    # On accède au tableau
    Go To Tab  prescription
    # On recherche l'enregistrement
    Use Simple Search  prescription  ${prescription}
    # On clique sur le résultat
    Click On Link  ${prescription}
    # On vérifie qu'il n'y a pas d'erreur
    Page Should Not Contain Errors

Ajouter prescription
    [Documentation]  Crée l'enregistrement
    [Arguments]  ${values}

    # On accède au tableau
    Go To Tab  prescription
    # On clique sur le bouton ajouter
    Click On Add Button
    # On saisit des valeurs
    Saisir prescription  ${values}
    # On valide le formulaire
    Click On Submit Button
    # On récupère l'ID du nouvel enregistrement
    ${prescription} =  Get Text  css=div.form-content span#prescription
    # On le retourne
    [Return]  ${prescription}

Modifier prescription
    [Documentation]  Modifie l'enregistrement
    [Arguments]  ${prescription}  ${values}

    # On accède à l'enregistrement
    Depuis le contexte prescription  ${prescription}
    # On clique sur le bouton modifier
    Click On Form Portlet Action  prescription  modifier
    # On saisit des valeurs
    Saisir prescription  ${values}
    # On valide le formulaire
    Click On Submit Button

Supprimer prescription
    [Documentation]  Supprime l'enregistrement
    [Arguments]  ${prescription}

    # On accède à l'enregistrement
    Depuis le contexte prescription  ${prescription}
    # On clique sur le bouton supprimer
    Click On Form Portlet Action  prescription  supprimer
    # On valide le formulaire
    Click On Submit Button

Saisir prescription
    [Documentation]  Remplit le formulaire
    [Arguments]  ${values}
    
    Si "analyses" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "ordre" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "prescription_reglementaire" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "pr_description_om_html" existe dans "${values}" on execute "Input HTML" dans le formulaire
    Si "pr_defavorable" existe dans "${values}" on execute "Set Checkbox" dans le formulaire
    Si "ps_description_om_html" existe dans "${values}" on execute "Input HTML" dans le formulaire