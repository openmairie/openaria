*** Settings ***
Documentation    CRUD de la table acteur_conge
...    @author  generated
...    @package openARIA
...    @version 04/12/2017 12:12

*** Keywords ***

Depuis le contexte congés
    [Documentation]  Accède au formulaire
    [Arguments]  ${acteur_conge}

    # On accède au tableau
    Go To Tab  acteur_conge
    # On recherche l'enregistrement
    Use Simple Search  congés  ${acteur_conge}
    # On clique sur le résultat
    Click On Link  ${acteur_conge}
    # On vérifie qu'il n'y a pas d'erreur
    Page Should Not Contain Errors

Ajouter congés
    [Documentation]  Crée l'enregistrement
    [Arguments]  ${values}

    # On accède au tableau
    Go To Tab  acteur_conge
    # On clique sur le bouton ajouter
    Click On Add Button
    # On saisit des valeurs
    Saisir congés  ${values}
    # On valide le formulaire
    Click On Submit Button
    # On récupère l'ID du nouvel enregistrement
    ${acteur_conge} =  Get Text  css=div.form-content span#acteur_conge
    # On le retourne
    [Return]  ${acteur_conge}

Modifier congés
    [Documentation]  Modifie l'enregistrement
    [Arguments]  ${acteur_conge}  ${values}

    # On accède à l'enregistrement
    Depuis le contexte congés  ${acteur_conge}
    # On clique sur le bouton modifier
    Click On Form Portlet Action  acteur_conge  modifier
    # On saisit des valeurs
    Saisir congés  ${values}
    # On valide le formulaire
    Click On Submit Button

Supprimer congés
    [Documentation]  Supprime l'enregistrement
    [Arguments]  ${acteur_conge}

    # On accède à l'enregistrement
    Depuis le contexte congés  ${acteur_conge}
    # On clique sur le bouton supprimer
    Click On Form Portlet Action  acteur_conge  supprimer
    # On valide le formulaire
    Click On Submit Button

Saisir congés
    [Documentation]  Remplit le formulaire
    [Arguments]  ${values}
    
    Si "acteur" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "date_debut" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "heure_debut" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "date_fin" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "heure_fin" existe dans "${values}" on execute "Input Text" dans le formulaire