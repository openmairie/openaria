*** Settings ***
Documentation    CRUD de la table derogation_scda
...    @author  generated
...    @package openARIA
...    @version 04/12/2017 12:12

*** Keywords ***

Depuis le contexte Dérogation SCDA
    [Documentation]  Accède au formulaire
    [Arguments]  ${derogation_scda}

    # On accède au tableau
    Go To Tab  derogation_scda
    # On recherche l'enregistrement
    Use Simple Search  Dérogation SCDA  ${derogation_scda}
    # On clique sur le résultat
    Click On Link  ${derogation_scda}
    # On vérifie qu'il n'y a pas d'erreur
    Page Should Not Contain Errors

Ajouter Dérogation SCDA
    [Documentation]  Crée l'enregistrement
    [Arguments]  ${values}

    # On accède au tableau
    Go To Tab  derogation_scda
    # On clique sur le bouton ajouter
    Click On Add Button
    # On saisit des valeurs
    Saisir Dérogation SCDA  ${values}
    # On valide le formulaire
    Click On Submit Button
    # On récupère l'ID du nouvel enregistrement
    ${derogation_scda} =  Get Text  css=div.form-content span#derogation_scda
    # On le retourne
    [Return]  ${derogation_scda}

Modifier Dérogation SCDA
    [Documentation]  Modifie l'enregistrement
    [Arguments]  ${derogation_scda}  ${values}

    # On accède à l'enregistrement
    Depuis le contexte Dérogation SCDA  ${derogation_scda}
    # On clique sur le bouton modifier
    Click On Form Portlet Action  derogation_scda  modifier
    # On saisit des valeurs
    Saisir Dérogation SCDA  ${values}
    # On valide le formulaire
    Click On Submit Button

Supprimer Dérogation SCDA
    [Documentation]  Supprime l'enregistrement
    [Arguments]  ${derogation_scda}

    # On accède à l'enregistrement
    Depuis le contexte Dérogation SCDA  ${derogation_scda}
    # On clique sur le bouton supprimer
    Click On Form Portlet Action  derogation_scda  supprimer
    # On valide le formulaire
    Click On Submit Button

Saisir Dérogation SCDA
    [Documentation]  Remplit le formulaire
    [Arguments]  ${values}
    
    Si "code" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "libelle" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "om_validite_debut" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "om_validite_fin" existe dans "${values}" on execute "Input Datepicker" dans le formulaire