*** Settings ***
Documentation    CRUD de la table courrier
...    @author  generated
...    @package openARIA
...    @version 04/12/2017 12:12

*** Keywords ***

Depuis le contexte courrier
    [Documentation]  Accède au formulaire
    [Arguments]  ${courrier}

    # On accède au tableau
    Go To Tab  courrier
    # On recherche l'enregistrement
    Use Simple Search  courrier  ${courrier}
    # On clique sur le résultat
    Click On Link  ${courrier}
    # On vérifie qu'il n'y a pas d'erreur
    Page Should Not Contain Errors

Ajouter courrier
    [Documentation]  Crée l'enregistrement
    [Arguments]  ${values}

    # On accède au tableau
    Go To Tab  courrier
    # On clique sur le bouton ajouter
    Click On Add Button
    # On saisit des valeurs
    Saisir courrier  ${values}
    # On valide le formulaire
    Click On Submit Button
    # On récupère l'ID du nouvel enregistrement
    ${courrier} =  Get Text  css=div.form-content span#courrier
    # On le retourne
    [Return]  ${courrier}

Modifier courrier
    [Documentation]  Modifie l'enregistrement
    [Arguments]  ${courrier}  ${values}

    # On accède à l'enregistrement
    Depuis le contexte courrier  ${courrier}
    # On clique sur le bouton modifier
    Click On Form Portlet Action  courrier  modifier
    # On saisit des valeurs
    Saisir courrier  ${values}
    # On valide le formulaire
    Click On Submit Button

Supprimer courrier
    [Documentation]  Supprime l'enregistrement
    [Arguments]  ${courrier}

    # On accède à l'enregistrement
    Depuis le contexte courrier  ${courrier}
    # On clique sur le bouton supprimer
    Click On Form Portlet Action  courrier  supprimer
    # On valide le formulaire
    Click On Submit Button

Saisir courrier
    [Documentation]  Remplit le formulaire
    [Arguments]  ${values}
    
    Si "etablissement" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "dossier_coordination" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "dossier_instruction" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "complement1_om_html" existe dans "${values}" on execute "Input HTML" dans le formulaire
    Si "complement2_om_html" existe dans "${values}" on execute "Input HTML" dans le formulaire
    Si "finalise" existe dans "${values}" on execute "Set Checkbox" dans le formulaire
    Si "om_fichier_finalise_courrier" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "om_fichier_signe_courrier" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "om_date_creation" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "date_finalisation" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "date_envoi_signature" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "date_retour_signature" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "date_envoi_controle_legalite" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "date_retour_controle_legalite" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "date_envoi_rar" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "date_retour_rar" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "code_barres" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "date_envoi_mail_om_fichier_finalise_courrier" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "date_envoi_mail_om_fichier_signe_courrier" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "mailing" existe dans "${values}" on execute "Set Checkbox" dans le formulaire
    Si "courrier_parent" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "courrier_joint" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "modele_edition" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "signataire" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "courrier_type" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "proces_verbal" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "visite" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "arrete_numero" existe dans "${values}" on execute "Input Text" dans le formulaire