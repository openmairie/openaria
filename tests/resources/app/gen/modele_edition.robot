*** Settings ***
Documentation    CRUD de la table modele_edition
...    @author  generated
...    @package openARIA
...    @version 18/11/2018 21:11

*** Keywords ***

Depuis le contexte modèle d'édition
    [Documentation]  Accède au formulaire
    [Arguments]  ${modele_edition}

    # On accède au tableau
    Go To Tab  modele_edition
    # On recherche l'enregistrement
    Use Simple Search  modèle d'édition  ${modele_edition}
    # On clique sur le résultat
    Click On Link  ${modele_edition}
    # On vérifie qu'il n'y a pas d'erreur
    Page Should Not Contain Errors

Ajouter modèle d'édition
    [Documentation]  Crée l'enregistrement
    [Arguments]  ${values}

    # On accède au tableau
    Go To Tab  modele_edition
    # On clique sur le bouton ajouter
    Click On Add Button
    # On saisit des valeurs
    Saisir modèle d'édition  ${values}
    # On valide le formulaire
    Click On Submit Button
    # On récupère l'ID du nouvel enregistrement
    ${modele_edition} =  Get Text  css=div.form-content span#modele_edition
    # On le retourne
    [Return]  ${modele_edition}

Modifier modèle d'édition
    [Documentation]  Modifie l'enregistrement
    [Arguments]  ${modele_edition}  ${values}

    # On accède à l'enregistrement
    Depuis le contexte modèle d'édition  ${modele_edition}
    # On clique sur le bouton modifier
    Click On Form Portlet Action  modele_edition  modifier
    # On saisit des valeurs
    Saisir modèle d'édition  ${values}
    # On valide le formulaire
    Click On Submit Button

Supprimer modèle d'édition
    [Documentation]  Supprime l'enregistrement
    [Arguments]  ${modele_edition}

    # On accède à l'enregistrement
    Depuis le contexte modèle d'édition  ${modele_edition}
    # On clique sur le bouton supprimer
    Click On Form Portlet Action  modele_edition  supprimer
    # On valide le formulaire
    Click On Submit Button

Saisir modèle d'édition
    [Documentation]  Remplit le formulaire
    [Arguments]  ${values}
    
    Si "libelle" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "description" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "om_validite_debut" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "om_validite_fin" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "code" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "courrier_type" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "om_lettretype_id" existe dans "${values}" on execute "Input Text" dans le formulaire