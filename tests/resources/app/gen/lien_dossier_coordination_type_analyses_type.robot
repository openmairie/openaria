*** Settings ***
Documentation    CRUD de la table lien_dossier_coordination_type_analyses_type
...    @author  generated
...    @package openARIA
...    @version 04/12/2017 12:12

*** Keywords ***

Depuis le contexte lien type dossier de coordination / type analyses
    [Documentation]  Accède au formulaire
    [Arguments]  ${lien_dossier_coordination_type_analyses_type}

    # On accède au tableau
    Go To Tab  lien_dossier_coordination_type_analyses_type
    # On recherche l'enregistrement
    Use Simple Search  lien type dossier de coordination / type analyses  ${lien_dossier_coordination_type_analyses_type}
    # On clique sur le résultat
    Click On Link  ${lien_dossier_coordination_type_analyses_type}
    # On vérifie qu'il n'y a pas d'erreur
    Page Should Not Contain Errors

Ajouter lien type dossier de coordination / type analyses
    [Documentation]  Crée l'enregistrement
    [Arguments]  ${values}

    # On accède au tableau
    Go To Tab  lien_dossier_coordination_type_analyses_type
    # On clique sur le bouton ajouter
    Click On Add Button
    # On saisit des valeurs
    Saisir lien type dossier de coordination / type analyses  ${values}
    # On valide le formulaire
    Click On Submit Button
    # On récupère l'ID du nouvel enregistrement
    ${lien_dossier_coordination_type_analyses_type} =  Get Text  css=div.form-content span#lien_dossier_coordination_type_analyses_type
    # On le retourne
    [Return]  ${lien_dossier_coordination_type_analyses_type}

Modifier lien type dossier de coordination / type analyses
    [Documentation]  Modifie l'enregistrement
    [Arguments]  ${lien_dossier_coordination_type_analyses_type}  ${values}

    # On accède à l'enregistrement
    Depuis le contexte lien type dossier de coordination / type analyses  ${lien_dossier_coordination_type_analyses_type}
    # On clique sur le bouton modifier
    Click On Form Portlet Action  lien_dossier_coordination_type_analyses_type  modifier
    # On saisit des valeurs
    Saisir lien type dossier de coordination / type analyses  ${values}
    # On valide le formulaire
    Click On Submit Button

Supprimer lien type dossier de coordination / type analyses
    [Documentation]  Supprime l'enregistrement
    [Arguments]  ${lien_dossier_coordination_type_analyses_type}

    # On accède à l'enregistrement
    Depuis le contexte lien type dossier de coordination / type analyses  ${lien_dossier_coordination_type_analyses_type}
    # On clique sur le bouton supprimer
    Click On Form Portlet Action  lien_dossier_coordination_type_analyses_type  supprimer
    # On valide le formulaire
    Click On Submit Button

Saisir lien type dossier de coordination / type analyses
    [Documentation]  Remplit le formulaire
    [Arguments]  ${values}
    
    Si "dossier_coordination_type" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "analyses_type" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "service" existe dans "${values}" on execute "Input Text" dans le formulaire