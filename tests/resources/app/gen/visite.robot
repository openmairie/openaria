*** Settings ***
Documentation    CRUD de la table visite
...    @author  generated
...    @package openARIA
...    @version 04/12/2017 12:12

*** Keywords ***

Depuis le contexte visite
    [Documentation]  Accède au formulaire
    [Arguments]  ${visite}

    # On accède au tableau
    Go To Tab  visite
    # On recherche l'enregistrement
    Use Simple Search  visite  ${visite}
    # On clique sur le résultat
    Click On Link  ${visite}
    # On vérifie qu'il n'y a pas d'erreur
    Page Should Not Contain Errors

Ajouter visite
    [Documentation]  Crée l'enregistrement
    [Arguments]  ${values}

    # On accède au tableau
    Go To Tab  visite
    # On clique sur le bouton ajouter
    Click On Add Button
    # On saisit des valeurs
    Saisir visite  ${values}
    # On valide le formulaire
    Click On Submit Button
    # On récupère l'ID du nouvel enregistrement
    ${visite} =  Get Text  css=div.form-content span#visite
    # On le retourne
    [Return]  ${visite}

Modifier visite
    [Documentation]  Modifie l'enregistrement
    [Arguments]  ${visite}  ${values}

    # On accède à l'enregistrement
    Depuis le contexte visite  ${visite}
    # On clique sur le bouton modifier
    Click On Form Portlet Action  visite  modifier
    # On saisit des valeurs
    Saisir visite  ${values}
    # On valide le formulaire
    Click On Submit Button

Supprimer visite
    [Documentation]  Supprime l'enregistrement
    [Arguments]  ${visite}

    # On accède à l'enregistrement
    Depuis le contexte visite  ${visite}
    # On clique sur le bouton supprimer
    Click On Form Portlet Action  visite  supprimer
    # On valide le formulaire
    Click On Submit Button

Saisir visite
    [Documentation]  Remplit le formulaire
    [Arguments]  ${values}
    
    Si "visite_motif_annulation" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "visite_etat" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "dossier_instruction" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "acteur" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "programmation" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "date_creation" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "date_annulation" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "observation" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "programmation_version_creation" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "programmation_version_annulation" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "heure_debut" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "heure_fin" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "convocation_exploitants" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "date_visite" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "a_poursuivre" existe dans "${values}" on execute "Set Checkbox" dans le formulaire
    Si "courrier_convocation_exploitants" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "courrier_annulation" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "programmation_version_modification" existe dans "${values}" on execute "Input Text" dans le formulaire