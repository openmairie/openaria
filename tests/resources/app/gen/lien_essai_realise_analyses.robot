*** Settings ***
Documentation    CRUD de la table lien_essai_realise_analyses
...    @author  generated
...    @package openARIA
...    @version 04/12/2017 12:12

*** Keywords ***

Depuis le contexte lien essai réalisé / analyses
    [Documentation]  Accède au formulaire
    [Arguments]  ${lien_essai_realise_analyses}

    # On accède au tableau
    Go To Tab  lien_essai_realise_analyses
    # On recherche l'enregistrement
    Use Simple Search  lien essai réalisé / analyses  ${lien_essai_realise_analyses}
    # On clique sur le résultat
    Click On Link  ${lien_essai_realise_analyses}
    # On vérifie qu'il n'y a pas d'erreur
    Page Should Not Contain Errors

Ajouter lien essai réalisé / analyses
    [Documentation]  Crée l'enregistrement
    [Arguments]  ${values}

    # On accède au tableau
    Go To Tab  lien_essai_realise_analyses
    # On clique sur le bouton ajouter
    Click On Add Button
    # On saisit des valeurs
    Saisir lien essai réalisé / analyses  ${values}
    # On valide le formulaire
    Click On Submit Button
    # On récupère l'ID du nouvel enregistrement
    ${lien_essai_realise_analyses} =  Get Text  css=div.form-content span#lien_essai_realise_analyses
    # On le retourne
    [Return]  ${lien_essai_realise_analyses}

Modifier lien essai réalisé / analyses
    [Documentation]  Modifie l'enregistrement
    [Arguments]  ${lien_essai_realise_analyses}  ${values}

    # On accède à l'enregistrement
    Depuis le contexte lien essai réalisé / analyses  ${lien_essai_realise_analyses}
    # On clique sur le bouton modifier
    Click On Form Portlet Action  lien_essai_realise_analyses  modifier
    # On saisit des valeurs
    Saisir lien essai réalisé / analyses  ${values}
    # On valide le formulaire
    Click On Submit Button

Supprimer lien essai réalisé / analyses
    [Documentation]  Supprime l'enregistrement
    [Arguments]  ${lien_essai_realise_analyses}

    # On accède à l'enregistrement
    Depuis le contexte lien essai réalisé / analyses  ${lien_essai_realise_analyses}
    # On clique sur le bouton supprimer
    Click On Form Portlet Action  lien_essai_realise_analyses  supprimer
    # On valide le formulaire
    Click On Submit Button

Saisir lien essai réalisé / analyses
    [Documentation]  Remplit le formulaire
    [Arguments]  ${values}
    
    Si "essai_realise" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "analyses" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "concluant" existe dans "${values}" on execute "Set Checkbox" dans le formulaire
    Si "complement" existe dans "${values}" on execute "Input Text" dans le formulaire