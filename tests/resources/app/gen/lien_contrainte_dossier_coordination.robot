*** Settings ***
Documentation    CRUD de la table lien_contrainte_dossier_coordination
...    @author  generated
...    @package openARIA
...    @version 04/12/2017 12:12

*** Keywords ***

Depuis le contexte lien contrainte / DC
    [Documentation]  Accède au formulaire
    [Arguments]  ${lien_contrainte_dossier_coordination}

    # On accède au tableau
    Go To Tab  lien_contrainte_dossier_coordination
    # On recherche l'enregistrement
    Use Simple Search  lien contrainte / DC  ${lien_contrainte_dossier_coordination}
    # On clique sur le résultat
    Click On Link  ${lien_contrainte_dossier_coordination}
    # On vérifie qu'il n'y a pas d'erreur
    Page Should Not Contain Errors

Ajouter lien contrainte / DC
    [Documentation]  Crée l'enregistrement
    [Arguments]  ${values}

    # On accède au tableau
    Go To Tab  lien_contrainte_dossier_coordination
    # On clique sur le bouton ajouter
    Click On Add Button
    # On saisit des valeurs
    Saisir lien contrainte / DC  ${values}
    # On valide le formulaire
    Click On Submit Button
    # On récupère l'ID du nouvel enregistrement
    ${lien_contrainte_dossier_coordination} =  Get Text  css=div.form-content span#lien_contrainte_dossier_coordination
    # On le retourne
    [Return]  ${lien_contrainte_dossier_coordination}

Modifier lien contrainte / DC
    [Documentation]  Modifie l'enregistrement
    [Arguments]  ${lien_contrainte_dossier_coordination}  ${values}

    # On accède à l'enregistrement
    Depuis le contexte lien contrainte / DC  ${lien_contrainte_dossier_coordination}
    # On clique sur le bouton modifier
    Click On Form Portlet Action  lien_contrainte_dossier_coordination  modifier
    # On saisit des valeurs
    Saisir lien contrainte / DC  ${values}
    # On valide le formulaire
    Click On Submit Button

Supprimer lien contrainte / DC
    [Documentation]  Supprime l'enregistrement
    [Arguments]  ${lien_contrainte_dossier_coordination}

    # On accède à l'enregistrement
    Depuis le contexte lien contrainte / DC  ${lien_contrainte_dossier_coordination}
    # On clique sur le bouton supprimer
    Click On Form Portlet Action  lien_contrainte_dossier_coordination  supprimer
    # On valide le formulaire
    Click On Submit Button

Saisir lien contrainte / DC
    [Documentation]  Remplit le formulaire
    [Arguments]  ${values}
    
    Si "dossier_coordination" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "contrainte" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "texte_complete" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "recuperee" existe dans "${values}" on execute "Set Checkbox" dans le formulaire