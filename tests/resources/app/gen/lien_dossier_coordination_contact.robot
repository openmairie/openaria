*** Settings ***
Documentation    CRUD de la table lien_dossier_coordination_contact
...    @author  generated
...    @package openARIA
...    @version 04/12/2017 12:12

*** Keywords ***

Depuis le contexte lien DC / contacts
    [Documentation]  Accède au formulaire
    [Arguments]  ${lien_dossier_coordination_contact}

    # On accède au tableau
    Go To Tab  lien_dossier_coordination_contact
    # On recherche l'enregistrement
    Use Simple Search  lien DC / contacts  ${lien_dossier_coordination_contact}
    # On clique sur le résultat
    Click On Link  ${lien_dossier_coordination_contact}
    # On vérifie qu'il n'y a pas d'erreur
    Page Should Not Contain Errors

Ajouter lien DC / contacts
    [Documentation]  Crée l'enregistrement
    [Arguments]  ${values}

    # On accède au tableau
    Go To Tab  lien_dossier_coordination_contact
    # On clique sur le bouton ajouter
    Click On Add Button
    # On saisit des valeurs
    Saisir lien DC / contacts  ${values}
    # On valide le formulaire
    Click On Submit Button
    # On récupère l'ID du nouvel enregistrement
    ${lien_dossier_coordination_contact} =  Get Text  css=div.form-content span#lien_dossier_coordination_contact
    # On le retourne
    [Return]  ${lien_dossier_coordination_contact}

Modifier lien DC / contacts
    [Documentation]  Modifie l'enregistrement
    [Arguments]  ${lien_dossier_coordination_contact}  ${values}

    # On accède à l'enregistrement
    Depuis le contexte lien DC / contacts  ${lien_dossier_coordination_contact}
    # On clique sur le bouton modifier
    Click On Form Portlet Action  lien_dossier_coordination_contact  modifier
    # On saisit des valeurs
    Saisir lien DC / contacts  ${values}
    # On valide le formulaire
    Click On Submit Button

Supprimer lien DC / contacts
    [Documentation]  Supprime l'enregistrement
    [Arguments]  ${lien_dossier_coordination_contact}

    # On accède à l'enregistrement
    Depuis le contexte lien DC / contacts  ${lien_dossier_coordination_contact}
    # On clique sur le bouton supprimer
    Click On Form Portlet Action  lien_dossier_coordination_contact  supprimer
    # On valide le formulaire
    Click On Submit Button

Saisir lien DC / contacts
    [Documentation]  Remplit le formulaire
    [Arguments]  ${values}
    
    Si "dossier_coordination" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "contact" existe dans "${values}" on execute "Select From List By Label" dans le formulaire