*** Settings ***
Documentation    CRUD de la table reunion_type_reunion_instance
...    @author  generated
...    @package openARIA
...    @version 04/12/2017 12:12

*** Keywords ***

Depuis le contexte reunion_type_reunion_instance
    [Documentation]  Accède au formulaire
    [Arguments]  ${reunion_type_reunion_instance}

    # On accède au tableau
    Go To Tab  reunion_type_reunion_instance
    # On recherche l'enregistrement
    Use Simple Search  reunion_type_reunion_instance  ${reunion_type_reunion_instance}
    # On clique sur le résultat
    Click On Link  ${reunion_type_reunion_instance}
    # On vérifie qu'il n'y a pas d'erreur
    Page Should Not Contain Errors

Ajouter reunion_type_reunion_instance
    [Documentation]  Crée l'enregistrement
    [Arguments]  ${values}

    # On accède au tableau
    Go To Tab  reunion_type_reunion_instance
    # On clique sur le bouton ajouter
    Click On Add Button
    # On saisit des valeurs
    Saisir reunion_type_reunion_instance  ${values}
    # On valide le formulaire
    Click On Submit Button
    # On récupère l'ID du nouvel enregistrement
    ${reunion_type_reunion_instance} =  Get Text  css=div.form-content span#reunion_type_reunion_instance
    # On le retourne
    [Return]  ${reunion_type_reunion_instance}

Modifier reunion_type_reunion_instance
    [Documentation]  Modifie l'enregistrement
    [Arguments]  ${reunion_type_reunion_instance}  ${values}

    # On accède à l'enregistrement
    Depuis le contexte reunion_type_reunion_instance  ${reunion_type_reunion_instance}
    # On clique sur le bouton modifier
    Click On Form Portlet Action  reunion_type_reunion_instance  modifier
    # On saisit des valeurs
    Saisir reunion_type_reunion_instance  ${values}
    # On valide le formulaire
    Click On Submit Button

Supprimer reunion_type_reunion_instance
    [Documentation]  Supprime l'enregistrement
    [Arguments]  ${reunion_type_reunion_instance}

    # On accède à l'enregistrement
    Depuis le contexte reunion_type_reunion_instance  ${reunion_type_reunion_instance}
    # On clique sur le bouton supprimer
    Click On Form Portlet Action  reunion_type_reunion_instance  supprimer
    # On valide le formulaire
    Click On Submit Button

Saisir reunion_type_reunion_instance
    [Documentation]  Remplit le formulaire
    [Arguments]  ${values}
    
    Si "reunion_type" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "reunion_instance" existe dans "${values}" on execute "Select From List By Label" dans le formulaire