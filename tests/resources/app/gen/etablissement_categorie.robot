*** Settings ***
Documentation    CRUD de la table etablissement_categorie
...    @author  generated
...    @package openARIA
...    @version 04/12/2017 12:12

*** Keywords ***

Depuis le contexte catégorie
    [Documentation]  Accède au formulaire
    [Arguments]  ${etablissement_categorie}

    # On accède au tableau
    Go To Tab  etablissement_categorie
    # On recherche l'enregistrement
    Use Simple Search  catégorie  ${etablissement_categorie}
    # On clique sur le résultat
    Click On Link  ${etablissement_categorie}
    # On vérifie qu'il n'y a pas d'erreur
    Page Should Not Contain Errors

Ajouter catégorie
    [Documentation]  Crée l'enregistrement
    [Arguments]  ${values}

    # On accède au tableau
    Go To Tab  etablissement_categorie
    # On clique sur le bouton ajouter
    Click On Add Button
    # On saisit des valeurs
    Saisir catégorie  ${values}
    # On valide le formulaire
    Click On Submit Button
    # On récupère l'ID du nouvel enregistrement
    ${etablissement_categorie} =  Get Text  css=div.form-content span#etablissement_categorie
    # On le retourne
    [Return]  ${etablissement_categorie}

Modifier catégorie
    [Documentation]  Modifie l'enregistrement
    [Arguments]  ${etablissement_categorie}  ${values}

    # On accède à l'enregistrement
    Depuis le contexte catégorie  ${etablissement_categorie}
    # On clique sur le bouton modifier
    Click On Form Portlet Action  etablissement_categorie  modifier
    # On saisit des valeurs
    Saisir catégorie  ${values}
    # On valide le formulaire
    Click On Submit Button

Supprimer catégorie
    [Documentation]  Supprime l'enregistrement
    [Arguments]  ${etablissement_categorie}

    # On accède à l'enregistrement
    Depuis le contexte catégorie  ${etablissement_categorie}
    # On clique sur le bouton supprimer
    Click On Form Portlet Action  etablissement_categorie  supprimer
    # On valide le formulaire
    Click On Submit Button

Saisir catégorie
    [Documentation]  Remplit le formulaire
    [Arguments]  ${values}
    
    Si "libelle" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "description" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "om_validite_debut" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "om_validite_fin" existe dans "${values}" on execute "Input Datepicker" dans le formulaire