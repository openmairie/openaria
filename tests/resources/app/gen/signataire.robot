*** Settings ***
Documentation    CRUD de la table signataire
...    @author  generated
...    @package openARIA
...    @version 04/12/2017 12:12

*** Keywords ***

Depuis le contexte signataire
    [Documentation]  Accède au formulaire
    [Arguments]  ${signataire}

    # On accède au tableau
    Go To Tab  signataire
    # On recherche l'enregistrement
    Use Simple Search  signataire  ${signataire}
    # On clique sur le résultat
    Click On Link  ${signataire}
    # On vérifie qu'il n'y a pas d'erreur
    Page Should Not Contain Errors

Ajouter signataire
    [Documentation]  Crée l'enregistrement
    [Arguments]  ${values}

    # On accède au tableau
    Go To Tab  signataire
    # On clique sur le bouton ajouter
    Click On Add Button
    # On saisit des valeurs
    Saisir signataire  ${values}
    # On valide le formulaire
    Click On Submit Button
    # On récupère l'ID du nouvel enregistrement
    ${signataire} =  Get Text  css=div.form-content span#signataire
    # On le retourne
    [Return]  ${signataire}

Modifier signataire
    [Documentation]  Modifie l'enregistrement
    [Arguments]  ${signataire}  ${values}

    # On accède à l'enregistrement
    Depuis le contexte signataire  ${signataire}
    # On clique sur le bouton modifier
    Click On Form Portlet Action  signataire  modifier
    # On saisit des valeurs
    Saisir signataire  ${values}
    # On valide le formulaire
    Click On Submit Button

Supprimer signataire
    [Documentation]  Supprime l'enregistrement
    [Arguments]  ${signataire}

    # On accède à l'enregistrement
    Depuis le contexte signataire  ${signataire}
    # On clique sur le bouton supprimer
    Click On Form Portlet Action  signataire  supprimer
    # On valide le formulaire
    Click On Submit Button

Saisir signataire
    [Documentation]  Remplit le formulaire
    [Arguments]  ${values}
    
    Si "nom" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "prenom" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "civilite" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "signataire_qualite" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "signature" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "defaut" existe dans "${values}" on execute "Set Checkbox" dans le formulaire
    Si "om_validite_debut" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "om_validite_fin" existe dans "${values}" on execute "Input Datepicker" dans le formulaire