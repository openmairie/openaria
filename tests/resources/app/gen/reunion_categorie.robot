*** Settings ***
Documentation    CRUD de la table reunion_categorie
...    @author  generated
...    @package openARIA
...    @version 04/12/2017 12:12

*** Keywords ***

Depuis le contexte catégorie de la réunion
    [Documentation]  Accède au formulaire
    [Arguments]  ${reunion_categorie}

    # On accède au tableau
    Go To Tab  reunion_categorie
    # On recherche l'enregistrement
    Use Simple Search  catégorie de la réunion  ${reunion_categorie}
    # On clique sur le résultat
    Click On Link  ${reunion_categorie}
    # On vérifie qu'il n'y a pas d'erreur
    Page Should Not Contain Errors

Ajouter catégorie de la réunion
    [Documentation]  Crée l'enregistrement
    [Arguments]  ${values}

    # On accède au tableau
    Go To Tab  reunion_categorie
    # On clique sur le bouton ajouter
    Click On Add Button
    # On saisit des valeurs
    Saisir catégorie de la réunion  ${values}
    # On valide le formulaire
    Click On Submit Button
    # On récupère l'ID du nouvel enregistrement
    ${reunion_categorie} =  Get Text  css=div.form-content span#reunion_categorie
    # On le retourne
    [Return]  ${reunion_categorie}

Modifier catégorie de la réunion
    [Documentation]  Modifie l'enregistrement
    [Arguments]  ${reunion_categorie}  ${values}

    # On accède à l'enregistrement
    Depuis le contexte catégorie de la réunion  ${reunion_categorie}
    # On clique sur le bouton modifier
    Click On Form Portlet Action  reunion_categorie  modifier
    # On saisit des valeurs
    Saisir catégorie de la réunion  ${values}
    # On valide le formulaire
    Click On Submit Button

Supprimer catégorie de la réunion
    [Documentation]  Supprime l'enregistrement
    [Arguments]  ${reunion_categorie}

    # On accède à l'enregistrement
    Depuis le contexte catégorie de la réunion  ${reunion_categorie}
    # On clique sur le bouton supprimer
    Click On Form Portlet Action  reunion_categorie  supprimer
    # On valide le formulaire
    Click On Submit Button

Saisir catégorie de la réunion
    [Documentation]  Remplit le formulaire
    [Arguments]  ${values}
    
    Si "code" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "libelle" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "description" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "service" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "om_validite_debut" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "om_validite_fin" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "ordre" existe dans "${values}" on execute "Input Text" dans le formulaire