*** Settings ***
Documentation    CRUD de la table autorite_police
...    @author  generated
...    @package openARIA
...    @version 04/12/2017 12:12

*** Keywords ***

Depuis le contexte autorité de police
    [Documentation]  Accède au formulaire
    [Arguments]  ${autorite_police}

    # On accède au tableau
    Go To Tab  autorite_police
    # On recherche l'enregistrement
    Use Simple Search  autorité de police  ${autorite_police}
    # On clique sur le résultat
    Click On Link  ${autorite_police}
    # On vérifie qu'il n'y a pas d'erreur
    Page Should Not Contain Errors

Ajouter autorité de police
    [Documentation]  Crée l'enregistrement
    [Arguments]  ${values}

    # On accède au tableau
    Go To Tab  autorite_police
    # On clique sur le bouton ajouter
    Click On Add Button
    # On saisit des valeurs
    Saisir autorité de police  ${values}
    # On valide le formulaire
    Click On Submit Button
    # On récupère l'ID du nouvel enregistrement
    ${autorite_police} =  Get Text  css=div.form-content span#autorite_police
    # On le retourne
    [Return]  ${autorite_police}

Modifier autorité de police
    [Documentation]  Modifie l'enregistrement
    [Arguments]  ${autorite_police}  ${values}

    # On accède à l'enregistrement
    Depuis le contexte autorité de police  ${autorite_police}
    # On clique sur le bouton modifier
    Click On Form Portlet Action  autorite_police  modifier
    # On saisit des valeurs
    Saisir autorité de police  ${values}
    # On valide le formulaire
    Click On Submit Button

Supprimer autorité de police
    [Documentation]  Supprime l'enregistrement
    [Arguments]  ${autorite_police}

    # On accède à l'enregistrement
    Depuis le contexte autorité de police  ${autorite_police}
    # On clique sur le bouton supprimer
    Click On Form Portlet Action  autorite_police  supprimer
    # On valide le formulaire
    Click On Submit Button

Saisir autorité de police
    [Documentation]  Remplit le formulaire
    [Arguments]  ${values}
    
    Si "autorite_police_decision" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "date_decision" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "delai" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "autorite_police_motif" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "cloture" existe dans "${values}" on execute "Set Checkbox" dans le formulaire
    Si "date_notification" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "date_butoir" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "service" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "dossier_instruction_reunion" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "dossier_coordination" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "etablissement" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "dossier_instruction_reunion_prochain" existe dans "${values}" on execute "Select From List By Label" dans le formulaire