*** Settings ***
Documentation    CRUD de la table technicien_arrondissement
...    @author  generated
...    @package openARIA
...    @version 04/12/2017 12:12

*** Keywords ***

Depuis le contexte lien technicien/arrondissement
    [Documentation]  Accède au formulaire
    [Arguments]  ${technicien_arrondissement}

    # On accède au tableau
    Go To Tab  technicien_arrondissement
    # On recherche l'enregistrement
    Use Simple Search  lien technicien/arrondissement  ${technicien_arrondissement}
    # On clique sur le résultat
    Click On Link  ${technicien_arrondissement}
    # On vérifie qu'il n'y a pas d'erreur
    Page Should Not Contain Errors

Ajouter lien technicien/arrondissement
    [Documentation]  Crée l'enregistrement
    [Arguments]  ${values}

    # On accède au tableau
    Go To Tab  technicien_arrondissement
    # On clique sur le bouton ajouter
    Click On Add Button
    # On saisit des valeurs
    Saisir lien technicien/arrondissement  ${values}
    # On valide le formulaire
    Click On Submit Button
    # On récupère l'ID du nouvel enregistrement
    ${technicien_arrondissement} =  Get Text  css=div.form-content span#technicien_arrondissement
    # On le retourne
    [Return]  ${technicien_arrondissement}

Modifier lien technicien/arrondissement
    [Documentation]  Modifie l'enregistrement
    [Arguments]  ${technicien_arrondissement}  ${values}

    # On accède à l'enregistrement
    Depuis le contexte lien technicien/arrondissement  ${technicien_arrondissement}
    # On clique sur le bouton modifier
    Click On Form Portlet Action  technicien_arrondissement  modifier
    # On saisit des valeurs
    Saisir lien technicien/arrondissement  ${values}
    # On valide le formulaire
    Click On Submit Button

Supprimer lien technicien/arrondissement
    [Documentation]  Supprime l'enregistrement
    [Arguments]  ${technicien_arrondissement}

    # On accède à l'enregistrement
    Depuis le contexte lien technicien/arrondissement  ${technicien_arrondissement}
    # On clique sur le bouton supprimer
    Click On Form Portlet Action  technicien_arrondissement  supprimer
    # On valide le formulaire
    Click On Submit Button

Saisir lien technicien/arrondissement
    [Documentation]  Remplit le formulaire
    [Arguments]  ${values}
    
    Si "service" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "technicien" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "arrondissement" existe dans "${values}" on execute "Select From List By Label" dans le formulaire