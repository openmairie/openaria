*** Settings ***
Documentation    CRUD de la table etablissement_tutelle_adm
...    @author  generated
...    @package openARIA
...    @version 04/12/2017 12:12

*** Keywords ***

Depuis le contexte tutelle administrative
    [Documentation]  Accède au formulaire
    [Arguments]  ${etablissement_tutelle_adm}

    # On accède au tableau
    Go To Tab  etablissement_tutelle_adm
    # On recherche l'enregistrement
    Use Simple Search  tutelle administrative  ${etablissement_tutelle_adm}
    # On clique sur le résultat
    Click On Link  ${etablissement_tutelle_adm}
    # On vérifie qu'il n'y a pas d'erreur
    Page Should Not Contain Errors

Ajouter tutelle administrative
    [Documentation]  Crée l'enregistrement
    [Arguments]  ${values}

    # On accède au tableau
    Go To Tab  etablissement_tutelle_adm
    # On clique sur le bouton ajouter
    Click On Add Button
    # On saisit des valeurs
    Saisir tutelle administrative  ${values}
    # On valide le formulaire
    Click On Submit Button
    # On récupère l'ID du nouvel enregistrement
    ${etablissement_tutelle_adm} =  Get Text  css=div.form-content span#etablissement_tutelle_adm
    # On le retourne
    [Return]  ${etablissement_tutelle_adm}

Modifier tutelle administrative
    [Documentation]  Modifie l'enregistrement
    [Arguments]  ${etablissement_tutelle_adm}  ${values}

    # On accède à l'enregistrement
    Depuis le contexte tutelle administrative  ${etablissement_tutelle_adm}
    # On clique sur le bouton modifier
    Click On Form Portlet Action  etablissement_tutelle_adm  modifier
    # On saisit des valeurs
    Saisir tutelle administrative  ${values}
    # On valide le formulaire
    Click On Submit Button

Supprimer tutelle administrative
    [Documentation]  Supprime l'enregistrement
    [Arguments]  ${etablissement_tutelle_adm}

    # On accède à l'enregistrement
    Depuis le contexte tutelle administrative  ${etablissement_tutelle_adm}
    # On clique sur le bouton supprimer
    Click On Form Portlet Action  etablissement_tutelle_adm  supprimer
    # On valide le formulaire
    Click On Submit Button

Saisir tutelle administrative
    [Documentation]  Remplit le formulaire
    [Arguments]  ${values}
    
    Si "libelle" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "om_validite_debut" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "om_validite_fin" existe dans "${values}" on execute "Input Datepicker" dans le formulaire