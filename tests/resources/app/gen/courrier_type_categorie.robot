*** Settings ***
Documentation    CRUD de la table courrier_type_categorie
...    @author  generated
...    @package openARIA
...    @version 04/12/2017 12:12

*** Keywords ***

Depuis le contexte type de catégorie de courrier
    [Documentation]  Accède au formulaire
    [Arguments]  ${courrier_type_categorie}

    # On accède au tableau
    Go To Tab  courrier_type_categorie
    # On recherche l'enregistrement
    Use Simple Search  type de catégorie de courrier  ${courrier_type_categorie}
    # On clique sur le résultat
    Click On Link  ${courrier_type_categorie}
    # On vérifie qu'il n'y a pas d'erreur
    Page Should Not Contain Errors

Ajouter type de catégorie de courrier
    [Documentation]  Crée l'enregistrement
    [Arguments]  ${values}

    # On accède au tableau
    Go To Tab  courrier_type_categorie
    # On clique sur le bouton ajouter
    Click On Add Button
    # On saisit des valeurs
    Saisir type de catégorie de courrier  ${values}
    # On valide le formulaire
    Click On Submit Button
    # On récupère l'ID du nouvel enregistrement
    ${courrier_type_categorie} =  Get Text  css=div.form-content span#courrier_type_categorie
    # On le retourne
    [Return]  ${courrier_type_categorie}

Modifier type de catégorie de courrier
    [Documentation]  Modifie l'enregistrement
    [Arguments]  ${courrier_type_categorie}  ${values}

    # On accède à l'enregistrement
    Depuis le contexte type de catégorie de courrier  ${courrier_type_categorie}
    # On clique sur le bouton modifier
    Click On Form Portlet Action  courrier_type_categorie  modifier
    # On saisit des valeurs
    Saisir type de catégorie de courrier  ${values}
    # On valide le formulaire
    Click On Submit Button

Supprimer type de catégorie de courrier
    [Documentation]  Supprime l'enregistrement
    [Arguments]  ${courrier_type_categorie}

    # On accède à l'enregistrement
    Depuis le contexte type de catégorie de courrier  ${courrier_type_categorie}
    # On clique sur le bouton supprimer
    Click On Form Portlet Action  courrier_type_categorie  supprimer
    # On valide le formulaire
    Click On Submit Button

Saisir type de catégorie de courrier
    [Documentation]  Remplit le formulaire
    [Arguments]  ${values}
    
    Si "code" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "libelle" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "description" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "om_validite_debut" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "om_validite_fin" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "objet" existe dans "${values}" on execute "Input Text" dans le formulaire