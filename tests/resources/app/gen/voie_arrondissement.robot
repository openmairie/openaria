*** Settings ***
Documentation    CRUD de la table voie_arrondissement
...    @author  generated
...    @package openARIA
...    @version 04/12/2017 12:12

*** Keywords ***

Depuis le contexte arrondissement / voie
    [Documentation]  Accède au formulaire
    [Arguments]  ${voie_arrondissement}

    # On accède au tableau
    Go To Tab  voie_arrondissement
    # On recherche l'enregistrement
    Use Simple Search  arrondissement / voie  ${voie_arrondissement}
    # On clique sur le résultat
    Click On Link  ${voie_arrondissement}
    # On vérifie qu'il n'y a pas d'erreur
    Page Should Not Contain Errors

Ajouter arrondissement / voie
    [Documentation]  Crée l'enregistrement
    [Arguments]  ${values}

    # On accède au tableau
    Go To Tab  voie_arrondissement
    # On clique sur le bouton ajouter
    Click On Add Button
    # On saisit des valeurs
    Saisir arrondissement / voie  ${values}
    # On valide le formulaire
    Click On Submit Button
    # On récupère l'ID du nouvel enregistrement
    ${voie_arrondissement} =  Get Text  css=div.form-content span#voie_arrondissement
    # On le retourne
    [Return]  ${voie_arrondissement}

Modifier arrondissement / voie
    [Documentation]  Modifie l'enregistrement
    [Arguments]  ${voie_arrondissement}  ${values}

    # On accède à l'enregistrement
    Depuis le contexte arrondissement / voie  ${voie_arrondissement}
    # On clique sur le bouton modifier
    Click On Form Portlet Action  voie_arrondissement  modifier
    # On saisit des valeurs
    Saisir arrondissement / voie  ${values}
    # On valide le formulaire
    Click On Submit Button

Supprimer arrondissement / voie
    [Documentation]  Supprime l'enregistrement
    [Arguments]  ${voie_arrondissement}

    # On accède à l'enregistrement
    Depuis le contexte arrondissement / voie  ${voie_arrondissement}
    # On clique sur le bouton supprimer
    Click On Form Portlet Action  voie_arrondissement  supprimer
    # On valide le formulaire
    Click On Submit Button

Saisir arrondissement / voie
    [Documentation]  Remplit le formulaire
    [Arguments]  ${values}
    
    Si "voie" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "arrondissement" existe dans "${values}" on execute "Select From List By Label" dans le formulaire