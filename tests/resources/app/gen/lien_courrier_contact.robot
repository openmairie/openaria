*** Settings ***
Documentation    CRUD de la table lien_courrier_contact
...    @author  generated
...    @package openARIA
...    @version 04/12/2017 12:12

*** Keywords ***

Depuis le contexte lien courrier / contact
    [Documentation]  Accède au formulaire
    [Arguments]  ${lien_courrier_contact}

    # On accède au tableau
    Go To Tab  lien_courrier_contact
    # On recherche l'enregistrement
    Use Simple Search  lien courrier / contact  ${lien_courrier_contact}
    # On clique sur le résultat
    Click On Link  ${lien_courrier_contact}
    # On vérifie qu'il n'y a pas d'erreur
    Page Should Not Contain Errors

Ajouter lien courrier / contact
    [Documentation]  Crée l'enregistrement
    [Arguments]  ${values}

    # On accède au tableau
    Go To Tab  lien_courrier_contact
    # On clique sur le bouton ajouter
    Click On Add Button
    # On saisit des valeurs
    Saisir lien courrier / contact  ${values}
    # On valide le formulaire
    Click On Submit Button
    # On récupère l'ID du nouvel enregistrement
    ${lien_courrier_contact} =  Get Text  css=div.form-content span#lien_courrier_contact
    # On le retourne
    [Return]  ${lien_courrier_contact}

Modifier lien courrier / contact
    [Documentation]  Modifie l'enregistrement
    [Arguments]  ${lien_courrier_contact}  ${values}

    # On accède à l'enregistrement
    Depuis le contexte lien courrier / contact  ${lien_courrier_contact}
    # On clique sur le bouton modifier
    Click On Form Portlet Action  lien_courrier_contact  modifier
    # On saisit des valeurs
    Saisir lien courrier / contact  ${values}
    # On valide le formulaire
    Click On Submit Button

Supprimer lien courrier / contact
    [Documentation]  Supprime l'enregistrement
    [Arguments]  ${lien_courrier_contact}

    # On accède à l'enregistrement
    Depuis le contexte lien courrier / contact  ${lien_courrier_contact}
    # On clique sur le bouton supprimer
    Click On Form Portlet Action  lien_courrier_contact  supprimer
    # On valide le formulaire
    Click On Submit Button

Saisir lien courrier / contact
    [Documentation]  Remplit le formulaire
    [Arguments]  ${values}
    
    Si "courrier" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "contact" existe dans "${values}" on execute "Select From List By Label" dans le formulaire