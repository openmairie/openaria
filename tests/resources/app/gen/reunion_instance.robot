*** Settings ***
Documentation    CRUD de la table reunion_instance
...    @author  generated
...    @package openARIA
...    @version 04/12/2017 12:12

*** Keywords ***

Depuis le contexte instance de réunion
    [Documentation]  Accède au formulaire
    [Arguments]  ${reunion_instance}

    # On accède au tableau
    Go To Tab  reunion_instance
    # On recherche l'enregistrement
    Use Simple Search  instance de réunion  ${reunion_instance}
    # On clique sur le résultat
    Click On Link  ${reunion_instance}
    # On vérifie qu'il n'y a pas d'erreur
    Page Should Not Contain Errors

Ajouter instance de réunion
    [Documentation]  Crée l'enregistrement
    [Arguments]  ${values}

    # On accède au tableau
    Go To Tab  reunion_instance
    # On clique sur le bouton ajouter
    Click On Add Button
    # On saisit des valeurs
    Saisir instance de réunion  ${values}
    # On valide le formulaire
    Click On Submit Button
    # On récupère l'ID du nouvel enregistrement
    ${reunion_instance} =  Get Text  css=div.form-content span#reunion_instance
    # On le retourne
    [Return]  ${reunion_instance}

Modifier instance de réunion
    [Documentation]  Modifie l'enregistrement
    [Arguments]  ${reunion_instance}  ${values}

    # On accède à l'enregistrement
    Depuis le contexte instance de réunion  ${reunion_instance}
    # On clique sur le bouton modifier
    Click On Form Portlet Action  reunion_instance  modifier
    # On saisit des valeurs
    Saisir instance de réunion  ${values}
    # On valide le formulaire
    Click On Submit Button

Supprimer instance de réunion
    [Documentation]  Supprime l'enregistrement
    [Arguments]  ${reunion_instance}

    # On accède à l'enregistrement
    Depuis le contexte instance de réunion  ${reunion_instance}
    # On clique sur le bouton supprimer
    Click On Form Portlet Action  reunion_instance  supprimer
    # On valide le formulaire
    Click On Submit Button

Saisir instance de réunion
    [Documentation]  Remplit le formulaire
    [Arguments]  ${values}
    
    Si "code" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "libelle" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "description" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "courriels" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "service" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "om_validite_debut" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "om_validite_fin" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "ordre" existe dans "${values}" on execute "Input Text" dans le formulaire