*** Settings ***
Documentation    CRUD de la table visite_duree
...    @author  generated
...    @package openARIA
...    @version 04/12/2017 12:12

*** Keywords ***

Depuis le contexte durée de visite
    [Documentation]  Accède au formulaire
    [Arguments]  ${visite_duree}

    # On accède au tableau
    Go To Tab  visite_duree
    # On recherche l'enregistrement
    Use Simple Search  durée de visite  ${visite_duree}
    # On clique sur le résultat
    Click On Link  ${visite_duree}
    # On vérifie qu'il n'y a pas d'erreur
    Page Should Not Contain Errors

Ajouter durée de visite
    [Documentation]  Crée l'enregistrement
    [Arguments]  ${values}

    # On accède au tableau
    Go To Tab  visite_duree
    # On clique sur le bouton ajouter
    Click On Add Button
    # On saisit des valeurs
    Saisir durée de visite  ${values}
    # On valide le formulaire
    Click On Submit Button
    # On récupère l'ID du nouvel enregistrement
    ${visite_duree} =  Get Text  css=div.form-content span#visite_duree
    # On le retourne
    [Return]  ${visite_duree}

Modifier durée de visite
    [Documentation]  Modifie l'enregistrement
    [Arguments]  ${visite_duree}  ${values}

    # On accède à l'enregistrement
    Depuis le contexte durée de visite  ${visite_duree}
    # On clique sur le bouton modifier
    Click On Form Portlet Action  visite_duree  modifier
    # On saisit des valeurs
    Saisir durée de visite  ${values}
    # On valide le formulaire
    Click On Submit Button

Supprimer durée de visite
    [Documentation]  Supprime l'enregistrement
    [Arguments]  ${visite_duree}

    # On accède à l'enregistrement
    Depuis le contexte durée de visite  ${visite_duree}
    # On clique sur le bouton supprimer
    Click On Form Portlet Action  visite_duree  supprimer
    # On valide le formulaire
    Click On Submit Button

Saisir durée de visite
    [Documentation]  Remplit le formulaire
    [Arguments]  ${values}
    
    Si "libelle" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "duree" existe dans "${values}" on execute "Input Text" dans le formulaire