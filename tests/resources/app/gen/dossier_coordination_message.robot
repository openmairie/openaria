*** Settings ***
Documentation    CRUD de la table dossier_coordination_message
...    @author  generated
...    @package openARIA
...    @version 06/11/2018 22:11

*** Keywords ***

Depuis le contexte message
    [Documentation]  Accède au formulaire
    [Arguments]  ${dossier_coordination_message}

    # On accède au tableau
    Go To Tab  dossier_coordination_message
    # On recherche l'enregistrement
    Use Simple Search  message  ${dossier_coordination_message}
    # On clique sur le résultat
    Click On Link  ${dossier_coordination_message}
    # On vérifie qu'il n'y a pas d'erreur
    Page Should Not Contain Errors

Ajouter message
    [Documentation]  Crée l'enregistrement
    [Arguments]  ${values}

    # On accède au tableau
    Go To Tab  dossier_coordination_message
    # On clique sur le bouton ajouter
    Click On Add Button
    # On saisit des valeurs
    Saisir message  ${values}
    # On valide le formulaire
    Click On Submit Button
    # On récupère l'ID du nouvel enregistrement
    ${dossier_coordination_message} =  Get Text  css=div.form-content span#dossier_coordination_message
    # On le retourne
    [Return]  ${dossier_coordination_message}

Modifier message
    [Documentation]  Modifie l'enregistrement
    [Arguments]  ${dossier_coordination_message}  ${values}

    # On accède à l'enregistrement
    Depuis le contexte message  ${dossier_coordination_message}
    # On clique sur le bouton modifier
    Click On Form Portlet Action  dossier_coordination_message  modifier
    # On saisit des valeurs
    Saisir message  ${values}
    # On valide le formulaire
    Click On Submit Button

Supprimer message
    [Documentation]  Supprime l'enregistrement
    [Arguments]  ${dossier_coordination_message}

    # On accède à l'enregistrement
    Depuis le contexte message  ${dossier_coordination_message}
    # On clique sur le bouton supprimer
    Click On Form Portlet Action  dossier_coordination_message  supprimer
    # On valide le formulaire
    Click On Submit Button

Saisir message
    [Documentation]  Remplit le formulaire
    [Arguments]  ${values}
    
    Si "categorie" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "dossier_coordination" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "type" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "emetteur" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "date_emission" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "contenu" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "contenu_json" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "si_cadre_lu" existe dans "${values}" on execute "Set Checkbox" dans le formulaire
    Si "si_technicien_lu" existe dans "${values}" on execute "Set Checkbox" dans le formulaire
    Si "si_mode_lecture" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "acc_cadre_lu" existe dans "${values}" on execute "Set Checkbox" dans le formulaire
    Si "acc_technicien_lu" existe dans "${values}" on execute "Set Checkbox" dans le formulaire
    Si "acc_mode_lecture" existe dans "${values}" on execute "Input Text" dans le formulaire