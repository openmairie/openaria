*** Settings ***
Documentation    CRUD de la table programmation
...    @author  generated
...    @package openARIA
...    @version 04/12/2017 12:12

*** Keywords ***

Depuis le contexte programmation
    [Documentation]  Accède au formulaire
    [Arguments]  ${programmation}

    # On accède au tableau
    Go To Tab  programmation
    # On recherche l'enregistrement
    Use Simple Search  programmation  ${programmation}
    # On clique sur le résultat
    Click On Link  ${programmation}
    # On vérifie qu'il n'y a pas d'erreur
    Page Should Not Contain Errors

Ajouter programmation
    [Documentation]  Crée l'enregistrement
    [Arguments]  ${values}

    # On accède au tableau
    Go To Tab  programmation
    # On clique sur le bouton ajouter
    Click On Add Button
    # On saisit des valeurs
    Saisir programmation  ${values}
    # On valide le formulaire
    Click On Submit Button
    # On récupère l'ID du nouvel enregistrement
    ${programmation} =  Get Text  css=div.form-content span#programmation
    # On le retourne
    [Return]  ${programmation}

Modifier programmation
    [Documentation]  Modifie l'enregistrement
    [Arguments]  ${programmation}  ${values}

    # On accède à l'enregistrement
    Depuis le contexte programmation  ${programmation}
    # On clique sur le bouton modifier
    Click On Form Portlet Action  programmation  modifier
    # On saisit des valeurs
    Saisir programmation  ${values}
    # On valide le formulaire
    Click On Submit Button

Supprimer programmation
    [Documentation]  Supprime l'enregistrement
    [Arguments]  ${programmation}

    # On accède à l'enregistrement
    Depuis le contexte programmation  ${programmation}
    # On clique sur le bouton supprimer
    Click On Form Portlet Action  programmation  supprimer
    # On valide le formulaire
    Click On Submit Button

Saisir programmation
    [Documentation]  Remplit le formulaire
    [Arguments]  ${values}
    
    Si "annee" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "numero_semaine" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "version" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "programmation_etat" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "date_modification" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "convocation_exploitants" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "date_envoi_ce" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "convocation_membres" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "date_envoi_cm" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "semaine_annee" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "service" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "planifier_nouveau" existe dans "${values}" on execute "Set Checkbox" dans le formulaire