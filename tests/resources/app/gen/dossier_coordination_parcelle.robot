*** Settings ***
Documentation    CRUD de la table dossier_coordination_parcelle
...    @author  generated
...    @package openARIA
...    @version 04/12/2017 12:12

*** Keywords ***

Depuis le contexte parcelle du dossier de coordination
    [Documentation]  Accède au formulaire
    [Arguments]  ${dossier_coordination_parcelle}

    # On accède au tableau
    Go To Tab  dossier_coordination_parcelle
    # On recherche l'enregistrement
    Use Simple Search  parcelle du dossier de coordination  ${dossier_coordination_parcelle}
    # On clique sur le résultat
    Click On Link  ${dossier_coordination_parcelle}
    # On vérifie qu'il n'y a pas d'erreur
    Page Should Not Contain Errors

Ajouter parcelle du dossier de coordination
    [Documentation]  Crée l'enregistrement
    [Arguments]  ${values}

    # On accède au tableau
    Go To Tab  dossier_coordination_parcelle
    # On clique sur le bouton ajouter
    Click On Add Button
    # On saisit des valeurs
    Saisir parcelle du dossier de coordination  ${values}
    # On valide le formulaire
    Click On Submit Button
    # On récupère l'ID du nouvel enregistrement
    ${dossier_coordination_parcelle} =  Get Text  css=div.form-content span#dossier_coordination_parcelle
    # On le retourne
    [Return]  ${dossier_coordination_parcelle}

Modifier parcelle du dossier de coordination
    [Documentation]  Modifie l'enregistrement
    [Arguments]  ${dossier_coordination_parcelle}  ${values}

    # On accède à l'enregistrement
    Depuis le contexte parcelle du dossier de coordination  ${dossier_coordination_parcelle}
    # On clique sur le bouton modifier
    Click On Form Portlet Action  dossier_coordination_parcelle  modifier
    # On saisit des valeurs
    Saisir parcelle du dossier de coordination  ${values}
    # On valide le formulaire
    Click On Submit Button

Supprimer parcelle du dossier de coordination
    [Documentation]  Supprime l'enregistrement
    [Arguments]  ${dossier_coordination_parcelle}

    # On accède à l'enregistrement
    Depuis le contexte parcelle du dossier de coordination  ${dossier_coordination_parcelle}
    # On clique sur le bouton supprimer
    Click On Form Portlet Action  dossier_coordination_parcelle  supprimer
    # On valide le formulaire
    Click On Submit Button

Saisir parcelle du dossier de coordination
    [Documentation]  Remplit le formulaire
    [Arguments]  ${values}
    
    Si "dossier_coordination" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "ref_cadastre" existe dans "${values}" on execute "Input Text" dans le formulaire