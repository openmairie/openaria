*** Settings ***
Documentation     Actions spécifiques à l'établissement

*** Keywords ***
Saisir la visite

    [Documentation]    Permet de saisir le formulaire d'une visite.

    [Arguments]    ${heure_debut_heure}=null    ${heure_debut_minute}=null    ${heure_fin_heure}=null    ${heure_fin_minute}=null    ${programmation_version_modification}=null    ${observation}=null    ${a_poursuivre}=null    ${courrier_annulation}=null

    # On sélectionne l'heure de début
    Input Hour Minute   heure_debut    ${heure_debut_heure}    ${heure_debut_minute}
    # On sélectionne la minute de début
    Input Hour Minute   heure_fin    ${heure_fin_heure}    ${heure_fin_minute}
    # On saisit la version de modification de la programmation
    Run Keyword If    '${programmation_version_modification}' != 'null'    Input Text    css=#programmation_version_modification    ${programmation_version_modification}
    # On saisit une observation
    Run Keyword If    '${observation}' != 'null'    Input Text    css=#observation    ${observation}
    # On coche ou décoche le champ a_poursuivre
    Run Keyword If    '${a_poursuivre}' != 'null'    Run Keyword If    '${a_poursuivre}' != 'true'    Unselect Checkbox    css=#a_poursuivre
    Run Keyword If    '${a_poursuivre}' != 'null'    Run Keyword If    '${a_poursuivre}' != 'false'    Select Checkbox    css=#a_poursuivre


Ajouter une visite depuis la programmation

    [Documentation]    Permet d'ajouter une visite sur une semaine de
    ...    programmation.

    [Arguments]    ${semaine_programmation}    ${technicien}    ${dossier_instruction_libelle}    ${jour_semaine}=1    ${a_poursuivre}=null    ${heure_debut_heure}=null    ${heure_debut_minute}=null    ${heure_fin_heure}=null    ${heure_fin_minute}=null    ${programmation_version_modification}=null    ${observation}=null    ${courrier_annulation}=null

    # On clique sur le tableau de bord
    Go To Dashboard
    # On ouvre le menu
    Go To Submenu In Menu    suivi    programmation
    # On ouvre la semaine
    Click On Link    ${semaine_programmation}
    # On ouvre l'interface spécifique de programmation
    Click On Link    Programmer les visites
    # On vérifie l'ouverture
    WUX  Element Text Should Be    css=#bloc_entete    Semaine ${semaine_programmation}
    # On sélectionne un technicien
    Select From List By Label    css=#acteur    ${technicien}
    # On fait une recherche sur le libelle du DI
    Input Text    css=table#tableau_propositions tr.search_tab_row th.dossier_instruction_libelle input.search_tab_input   ${dossier_instruction_libelle}
    # On crée une visite pour le DI le jeudi de 08:45 à 10:45
    ${jour_semaine} =    Convert to Integer    ${jour_semaine}
    ${jour_semaine} =    Evaluate    ${jour_semaine}+1
    # XXX Le Drag And Drop ne fonctionne pas, on exécute donc du javascript pour le simuler
    #Drag And Drop    css=table#tableau_propositions tbody tr    css=tr.fc-minor:nth-child(4) td.fc-widget-content:nth-child(2)
    Execute JavaScript  ${EXECDIR}${/}binary_files${/}jquery.simulate.js
    Execute Javascript  var element = $("table#tableau_propositions tbody tr"); element.simulate( "drag", { dx: 0, dy: 450 } );
    # On vérifie que l'overlay est correctement ouvert
    WUX  Element Should Be Visible  css=#sousform-visite.overlay div.subtitle h3
    # On saisit les informations de la visite
    Saisir la visite    ${heure_debut_heure}    ${heure_debut_minute}    ${heure_fin_heure}    ${heure_fin_minute}    ${programmation_version_modification}    ${observation}    ${a_poursuivre}    ${courrier_annulation}
    # On valide le formulaire
    Click On Submit Button In Subform
    # On vérifie le message de validation
    WUX  Valid Message Should Contain    Vos modifications ont bien été enregistrées.
    ${visite_id} =  Get Element Attribute  css=#sousform-visite.overlay #visite  value
    # On ferme l'overlay
    Click On Back Button In Subform
    # On vérifie que le di est toujours présent dans le tableau
    WUX  Page Should Contain    ${dossier_instruction_libelle}
    [return]  ${visite_id}


Ajouter une semaine de programmation

    [Documentation]    Permet d'ajouter une semaine de programmation et de
    ...    retourner son identifiant. Même si la semaine existe déjà, aucune
    ...    erreur n'est retorunée afin de continuer le traitement normal.

    [Arguments]    ${annee}    ${numero_semaine}    ${service_libelle}=null

    # On ouvre le menu
    Go To Submenu In Menu    suivi    programmation
    # On passe en mode ajout
    Click On Add Button
    # On saisit les valeurs
    Input Value With JS    annee    ${annee}
    Input Value With JS    numero_semaine    ${numero_semaine}
    Run Keyword If    '${service_libelle}' != 'null'    Select From List By Label    css=#service    ${service_libelle}
    # On valide le formulaire
    Click On Submit Button
    # Quelque soit l'issue de la validation du formulaire, on retourne
    # l'identifiant de la semaine
    ${numero_semaine} =    STR_PAD_LEFT    ${numero_semaine}    2    0
    [Return]    ${annee}/${numero_semaine}


Depuis le listing des visites dans le contexte de la programmation
    [Tags]  visite  programmation
    [Documentation]  Accède au listing des enregistrements de type 'visite'
    ...  dans le contexte d'une programmation.
    ...
    ...  `programmation` est attendu au format 'AAAA/SS'.
    [Arguments]  ${programmation}

    Depuis le contexte de la programmation  ${programmation}
    On clique sur l'onglet  visite  Visite


Depuis la visite dans le contexte de la programmation
    [Tags]  visite  programmation
    [Documentation]  Accède à la fiche de la visite dans le contexte d'une
    ...  programmation.
    ...
    ...  `programmation` est attendu au format 'AAAA/SS'. `visite_id` est un
    ...  entier repréentant l'identifiant numérique d'une visite.
    [Arguments]  ${programmation}  ${visite_id}

    Depuis le listing des visites dans le contexte de la programmation  ${programmation}
    Click Element  css=#action-soustab-visite-left-consulter-${visite_id}
    Page Subtitle Should Be  > visites > ${visite_id}
    La page ne doit pas contenir d'erreur


Depuis le formulaire de modification d'une visite dans le contexte de la programmation
    [Tags]  visite  programmation
    [Documentation]  Accède au formulaire de modification de la visite dans le
    ...  contexte d'une programmation.
    ...
    ...  `programmation` est attendu au format 'AAAA/SS'. `visite_id` est un
    ...  entier repréentant l'identifiant numérique d'une visite.
    [Arguments]  ${programmation}  ${visite_id}

    Depuis la visite dans le contexte de la programmation  ${programmation}  ${visite_id}
    Click On Subform Portlet Action  visite  modifier


Supprimer la visite dans le contexte de la programmation
    [Tags]  visite  programmation
    [Documentation]  Supprime la visite depuis le contexte d'une programmation.
    ...
    ...  `programmation` est attendu au format 'AAAA/SS'. `visite_id` est un
    ...  entier repréentant l'identifiant numérique d'une visite.
    [Arguments]  ${programmation}  ${visite_id}

    Depuis la visite dans le contexte de la programmation  ${programmation}  ${visite_id}
    Click On Subform Portlet Action  visite  supprimer
    Click On Submit Button In Subform
    Valid Message Should Be In Subtab  La suppression a été correctement effectuée.

