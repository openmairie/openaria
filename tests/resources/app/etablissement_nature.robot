*** Settings ***
Documentation     Actions spécifiques à la nature d'établissement

*** Keywords ***
Depuis le listing des natures d'établissement
    [Documentation]  Accède au listing des natures d'établissement
    Depuis le listing  etablissement_nature


Depuis le contexte de la nature d'établisement
    [Documentation]  Accède à la fiche d'une nature d'établissement
    [Arguments]  ${code}
    Depuis le listing des natures d'établissement
    Use Simple Search  code  ${code}
    Click On Link  ${code}
    La page ne doit pas contenir d'erreur


Modifier la nature d'établissement
    [Documentation]  Modifie la nature d'établissement
    [Arguments]  ${code}  ${values}
    Depuis le contexte de la nature d'établisement  ${code}
    Click On Form Portlet Action  etablissement_nature  modifier
    Saisir les valeurs dans le formulaire de la nature d'établissement  ${values}
    Click On Submit Button
    Valid Message Should Be  Vos modifications ont bien été enregistrées.


Saisir les valeurs dans le formulaire de la nature d'établissement
    [Documentation]  Remplit le formulaire
    [Arguments]  ${values}
    Si "nature" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "description" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "om_validite_debut" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "om_validite_fin" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "code" existe dans "${values}" on execute "Input Text" dans le formulaire

