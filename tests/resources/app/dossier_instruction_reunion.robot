*** Settings ***
Documentation     Actions spécifiques aux dossier_instruction_reunion.

*** Keywords ***
Depuis l'onglet réunion du dossier d'instruction

    [Documentation]    Permet d'accéder à l'onglet réunion dans le contexte d'un dossier d'instruction.

    [Arguments]    ${dossier_instruction}

    #
    Depuis le contexte du dossier d'instruction    ${dossier_instruction}
    # On clique sur l'onglet réunion
    On clique sur l'onglet    dossier_instruction_reunion_contexte_di    Réunions


Ajouter la demande de passage en réunion depuis le contexte du dossier d'instruction

    [Documentation]    Ce Keyword permet de créer une demande de passage en réunion sur un dossier d'instruction existant. L'utilisateur connecté lors de l'appel doit avoir les permissions d'accéder au DI et de créer une demande de passage.

    [Arguments]    ${dossier_coordination_libelle}    ${dossier_instruction_service}    ${date_souhaitee}    ${reunion_type}    ${reunion_type_categorie}    ${motivation}=null

    #
    Depuis l'onglet réunion du dossier d'instruction    ${dossier_coordination_libelle}-${dossier_instruction_service}
    # On clique sur l'action ajouter du tableau
    Click On Add Button JS
    # On sélectionne le type de réunion
    Select From List By Label    css=#reunion_type    ${reunion_type}
    # On saisit la catégorie de type de réunion
    WUX  Select From List By Label    css=#reunion_type_categorie    ${reunion_type_categorie}
    # On saisit la date souhaitée
    Input Datepicker    date_souhaitee    ${date_souhaitee}
    # Saisie de la motivation si elle est passée en paramètre
    Run Keyword If    '${motivation}' != 'null'    Input Text    css=#motivation    ${motivation}
    # On valide le formulaire
    Click On Submit Button In Subform
    # On vérifie le message affiché à l'utilisateur
    WUX  Valid Message Should Be    Vos modifications ont bien été enregistrées.
    # On retourne au listing
    Click On Back Button In Subform
    # On clique sur l'élément qui vient d'être créé
    # @todo Le click sur cette valeur n'est pas très probant
    Click On Link    ${date_souhaitee}
    # On attend que la page soit chargée
    WUX  Element Should Be Visible    css=#dossier_instruction_reunion
    # On récupère la valeur de l'id
    ${dossier_instruction_reunion_id} =    Get Text    css=#dossier_instruction_reunion

    [Return]    ${dossier_instruction_reunion_id}


Depuis la demande de passage (retour d'avis) dans le contexte de la réunion

    [Documentation]    Ce Keyword permet d'accéder à l'écran de visualisation d'une demande de passage en réunion dans le contexte de la réunion à laquelle elle est planifiée.

    [Arguments]    ${reunion}    ${dossier_instruction_reunion_id}

    #
    Depuis l'ordre du jour de la réunion    ${reunion}
    #
    WUX  Click Element    css=#action-view_meeting-reunion-left-consulter-${dossier_instruction_reunion_id}
    #
    La page ne doit pas contenir d'erreur

