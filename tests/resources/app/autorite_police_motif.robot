*** Settings ***
Documentation     Actions spécifiques à l'autorité de police

*** Keywords ***
Ajouter un motif d'autorité de police
    [Arguments]    ${code}    ${libelle}    ${description}    ${service}    ${date_debut}    ${date_fin}
    # On clique sur le tableau de bord
    Go To Dashboard
    # On clique sur l'item correspondant du menu
    Depuis le listing  autorite_police_motif
    # On vérifie le fil d'Ariane
    #XXX Page Title Should Be    Paramétrage > Autorité de police > Motifs
    # On clique sur l'action ajouter du tableau
    Click On Add Button
    # On saisit le code
    Input Text    css=#code    ${code}
    # On saisit le libellé
    Input Text    css=#libelle    ${libelle}
    # On saisit la description
    Input Text    css=#description    ${description}
    # On sélectionne le service
    Select From List By Label    css=#service    ${service}
    # On saisit la date de début de validité
    Input Datepicker    date_debut    ${date_debut}
    # On saisit la date de fin de validité
    Input Datepicker    date_fin    ${date_fin}
    # On valide le formulaire
    Click On Submit Button
    # On vérifie le message affiché à l'utilisateur
    Valid Message Should Be    Vos modifications ont bien été enregistrées.
