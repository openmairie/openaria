*** Settings ***
Documentation    Actions spécifiques aux types de courrier.

*** Keywords ***
Ajouter le type de courrier
    [Tags]  courrier_type
    [Documentation]  Ajoute un enregistrement de type 'type de courrier' (courrier_type).
    ...
    ...  Exemple :
    ...
    ...  | &{courrier_type} =  Create Dictionary
    ...  | ...  code=DG-SPEC
    ...  | ...  libelle=Documents Générés Spécifiques
    ...  | ...  description=Description des documents générés spécifiques
    ...  | ...  om_validite_debut=
    ...  | ...  om_validite_fin=
    ...  | ...  courrier_type_categorie=Documents générés liés aux établissements, aux dossiers de coordination et aux dossiers d'instruction
    ...  | ...  service=
    ...  | ${courrier_type_id} =  Ajouter le type de courrier  ${courrier_type}
    ...
    [Arguments]  ${values}
    Depuis le listing  courrier_type
    Click On Add Button
    Saisir le type de courrier  ${values}
    Click On Submit Button
    Valid Message Should Be  Vos modifications ont bien été enregistrées.
    ${courrier_type} =  Get Text  css=div.form-content span#courrier_type
    [Return]  ${courrier_type}


Saisir le type de courrier
    [Documentation]  Remplit le formulaire
    [Arguments]  ${values}
    Si "code" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "libelle" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "description" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "om_validite_debut" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "om_validite_fin" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "courrier_type_categorie" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "service" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
