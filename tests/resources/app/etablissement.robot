*** Settings ***
Documentation     Actions spécifiques à l'établissement

*** Keywords ***
Depuis le listing de tous les établissements

    [Documentation]    Permet d'accéder au listing de tous les établissements.

    #
    Go To Dashboard
    #
    Go To Submenu In Menu    etablissements    etablissement_tous


Depuis le listing de tous les établissements avec les archivés

    [Documentation]    Permet d'accéder au listing de tous les établissements,
    ...    même des archivés.

    #
    Go To Dashboard
    #
    Go To Submenu In Menu    etablissements    etablissement_tous
    #
    Click On Link    Afficher les éléments archivés


Depuis le listing des ERP référentiels

    [Documentation]    Permet d'acéder au listing des ERP référentiels.

    #
    Go To Dashboard
    #
    Go To Submenu In Menu    etablissements    etablissement_referentiel_erp


Depuis le contexte de l'établissement

    [Documentation]  Accède à la vue consulter de l'enregistrement de type
    ...  'établissement' (etablissement).
    ...
    ...  ``contexte`` permet de préciser depuis quel contexte on souhaite
    ...  accéder à l'élément (tous ou referentiel_erp).
    [Arguments]    ${code}=null    ${libelle}=null    ${contexte}=tous

    Run Keyword If  '${contexte}' == 'tous'  Depuis le listing de tous les établissements
    Run Keyword If  '${contexte}' == 'referentiel_erp'  Depuis le listing des ERP référentiels
    #
    Run Keyword If    '${code}' != 'null'    Input Text    css=#etablissement    ${code}
    Run Keyword If    '${libelle}' != 'null'    Input Text    css=#libelle    ${libelle}
    Click On Search Button
    #
    Run Keyword If    '${code}' != 'null'    Click On Link    ${code}    ELSE IF    '${libelle}' != 'null'    Click On Link    ${libelle}    ELSE    Fail

    # Vérification que le formulaire est ouvert et qu'on se trouve sur le bon
    # établissement en vérifiant le contenu des champs code et/ou libellé
    Run Keyword If  '${code}' != 'null'  WUX  Element Should Contain  css=#code  ${code}
    Run Keyword If  '${libelle}' != 'null'  WUX  Element Should Contain  css=#libelle  ${libelle}


Depuis le contexte de l'établissement archivé

    [Documentation]    Affiche l'établissement même si celui-ci est archivé.

    [Arguments]    ${code}=null    ${libelle}=null

    Depuis le listing de tous les établissements avec les archivés
    #
    Run Keyword If    '${code}' != 'null'    Input Text    css=#etablissement    ${code}
    Run Keyword If    '${libelle}' != 'null'    Input Text    css=#libelle    ${libelle}
    Click On Search Button
    #
    Run Keyword If    '${code}' != 'null'    Click On Link    ${code}    ELSE IF    '${libelle}' != 'null'    Click On Link    ${libelle}    ELSE    Fail


Depuis le formulaire d'ajout d'établissement
    [Tags]  etablissement
    [Documentation]  Accède directement via URL au formulaire d'ajout d'un enregistrement de type 'etablissement' dans le contexte 'etablissement_tous'.
    Go To  ${PROJECT_URL}${OM_ROUTE_FORM}&obj=etablissement_tous&action=0&retour=form
    La page ne doit pas contenir d'erreur


Depuis le formulaire de modification de l'établissement

    [Documentation]    Permet d'accéder au formulaire de modification de
    ...    l'établissement.

    [Arguments]    ${code}=null    ${libelle}=null

    Depuis le contexte de l'établissement    ${code}    ${libelle}
    # On clique sur l'action modifier
    Click On Form Portlet Action    etablissement_tous    modifier


Depuis l'onglet contact de l'établissement

    [Documentation]    Permet d'accéder à l'onglet contact dans le contexte d'un établissement.

    [Arguments]    ${code}=null    ${libelle}=null

    #
    Depuis le contexte de l'établissement    ${code}    ${libelle}
    # On clique sur l'onglet
    On clique sur l'onglet    contact    Contacts


Vérifier les champs périodiques de l'établissement

    [Documentation]    Vérifie les champs concernant les visites périodiques
    ...    d'un établissement.

    [Arguments]    ${dossier_coordination_periodique}=null    ${si_derniere_visite_periodique_date}=null    ${si_prochaine_visite_periodique_date_previsionnelle}=null    ${si_periodicite_visites}=null

    # On vérifie le dossier de coordination périodique
    Run keyword If    '${dossier_coordination_periodique}' != 'null'    Link Value Should Be    dossier_coordination_periodique    ${dossier_coordination_periodique}
    # On vérifie la date de dernière visite périodique
    Run keyword If    '${si_derniere_visite_periodique_date}' != 'null'    Form Static Value Should Be    css=#si_derniere_visite_periodique_date    ${si_derniere_visite_periodique_date}
    # On vérifie la date de prochaine visite périodique
    Run keyword If    '${si_prochaine_visite_periodique_date_previsionnelle}' != 'null'    Form Static Value Should Be    css=#si_prochaine_visite_periodique_date_previsionnelle    ${si_prochaine_visite_periodique_date_previsionnelle}
    # On vérifie la périodicité des visites (en année)
    Run keyword If    '${si_periodicite_visites}' != 'null'    Form Static Value Should Be    css=#si_periodicite_visites    ${si_periodicite_visites}


Saisir l'exploitant depuis le formulaire de l'établissement

    [Documentation]    Permet de saisir l'exploitant depuis le formulaire de
    ...    l'établissement.

    [Arguments]    ${etablissement_code}=null    ${etablissement_libelle}=null    ${exp_civilite}=null    ${exp_nom}=null    ${exp_prenom}=null    ${meme_adresse}=null    ${exp_adresse_numero}=null    ${exp_adresse_numero2}=null    ${exp_adresse_voie}=null    ${exp_adresse_complement}=null    ${exp_lieu_dit}=null    ${exp_boite_postale}=null    ${exp_adresse_cp}=null    ${exp_adresse_ville}=null    ${exp_cedex}=null    ${exp_pays}=null

    Depuis le contexte de l'établissement    ${etablissement_code}    ${etablissement_libelle}
    # On clique sur le bouton modifier
    Click On Form Portlet Action    etablissement_tous    modifier
    # On saisit les valeurs
    Run Keyword If    '${exp_civilite}' != 'null'    Select From List By Label    css=#exp_civilite    ${exp_civilite}
    Run Keyword If    '${exp_nom}' != 'null'    Input Text    css=#exp_nom    ${exp_nom}
    Run Keyword If    '${exp_prenom}' != 'null'    Input Text    css=#exp_prenom    ${exp_prenom}
    Run Keyword If    '${meme_adresse}' != 'null'    Run Keyword If    '${meme_adresse}' == 'true'    Select Checkbox    css=#meme_adresse
    Run Keyword If    '${meme_adresse}' != 'null'    Run Keyword If    '${meme_adresse}' == 'false'    Unselect Checkbox    css=#meme_adresse
    Run Keyword If    '${exp_adresse_numero}' != 'null'    Input Text    css=#exp_adresse_numero    ${exp_adresse_numero}
    Run Keyword If    '${exp_adresse_numero2}' != 'null'    Input Text    css=#exp_adresse_numero2    ${exp_adresse_numero2}
    Run Keyword If    '${exp_adresse_voie}' != 'null'    Input Text    css=#exp_adresse_voie    ${exp_adresse_voie}
    Run Keyword If    '${exp_adresse_complement}' != 'null'    Input Text    css=#exp_adresse_complement    ${exp_adresse_complement}
    Run Keyword If    '${exp_lieu_dit}' != 'null'    Input Text    css=#exp_lieu_dit    ${exp_lieu_dit}
    Run Keyword If    '${exp_boite_postale}' != 'null'    Input Text    css=#exp_boite_postale    ${exp_boite_postale}
    Run Keyword If    '${exp_adresse_cp}' != 'null'    Input Text    css=#exp_adresse_cp    ${exp_adresse_cp}
    Run Keyword If    '${exp_adresse_ville}' != 'null'    Input Text    css=#exp_adresse_ville    ${exp_adresse_ville}
    Run Keyword If    '${exp_cedex}' != 'null'    Input Text    css=#exp_cedex    ${exp_cedex}
    Run Keyword If    '${exp_pays}' != 'null'    Input Text    css=#exp_pays    ${exp_pays}
    # On valide le formulaire
    Click On Submit Button
    # On vérifie le message de validation
    Valid Message Should Be    Vos modifications ont bien été enregistrées.


Archiver l'établissement

    [Documentation]    Archive l'établissement.

    [Arguments]    ${code}=null    ${libelle}=null

    Depuis le contexte de l'établissement    ${code}    ${libelle}
    # On clique sur l'action archiver
    Click On Form Portlet Action    etablissement_tous    archiver
    # On vérifie le message de validation
    WUX  Valid Message Should Contain    Archivage correctement effectué.


Désarchiver l'établissement

    [Documentation]    Désarchive l'établissement.

    [Arguments]    ${code}=null    ${libelle}=null

    Depuis le contexte de l'établissement archivé    ${code}    ${libelle}
    # On clique sur l'action archiver
    Click On Form Portlet Action    etablissement_tous    desarchiver
    # On vérifie le message de validation
    WUX  Valid Message Should Contain    Désarchivage correctement effectué.


Saisir les valeurs dans le formulaire de l'établissement

    [Arguments]  ${values}

    #
    Si "libelle" existe dans "${values}" on execute "Input Text" dans le formulaire
    #
    Si "etablissement_nature" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    #
    Si "etablissement_type" existe dans "${values}" on execute "Select From Chosen List For 'Type' By Name" dans le formulaire
    Si "etablissement_categorie" existe dans "${values}" on execute "Select From Chosen List For 'Catégorie' By Name" dans le formulaire
    Si "etablissement_etat" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    #
    Si "adresse_numero" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "adresse_numero2" existe dans "${values}" on execute "Input Text" dans le formulaire
    #
    Si "adresse_voie" existe dans "${values}" on sélectionne la valeur sur l'autocomplete "voie" dans le formulaire
    #
    Si "adresse_complement" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "lieu_dit" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "boite_postale" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "adresse_cp" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "adresse_ville" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "adresse_arrondissement" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "cedex" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "npai" existe dans "${values}" on execute "Set Checkbox" dans le formulaire
    Si "telephone" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "fax" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "siret" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "annee_de_construction" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "etablissement_statut_juridique" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "etablissement_tutelle_adm" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "ref_patrimoine" existe dans "${values}" on execute "Input Text" dans le formulaire
    #
    Si "exp_civilite" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "exp_nom" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "exp_prenom" existe dans "${values}" on execute "Input Text" dans le formulaire
    #
    Si "meme_adresse" existe dans "${values}" on execute "Set Checkbox" dans le formulaire
    #
    Si "exp_adresse_numero" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "exp_adresse_numero2" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "exp_adresse_voie" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "exp_adresse_complement" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "exp_lieu_dit" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "exp_boite_postale" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "exp_adresse_cp" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "exp_adresse_ville" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "exp_cedex" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "exp_pays" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "terrain_references_cadastrales" existe dans "${values}" on execute "Saisir les références cadastrales"
    Si "si_derniere_visite_periodique_date" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "si_derniere_visite_date" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "si_derniere_visite_avis" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "si_derniere_visite_technicien" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "acc_derniere_visite_date" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "acc_derniere_visite_avis" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "acc_derniere_visite_technicien" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "si_autorite_competente_visite" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "si_autorite_competente_plan" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "si_dernier_plan_avis" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    #
    Click Element  css=div.bloc-details-etablissement legend.collapsed
    Si "acc_consignes_om_html" existe dans "${values}" on execute "Input HTML" dans le formulaire
    Si "acc_descriptif_om_html" existe dans "${values}" on execute "Input HTML" dans le formulaire
    Si "si_consignes_om_html" existe dans "${values}" on execute "Input HTML" dans le formulaire
    Si "si_descriptif_om_html" existe dans "${values}" on execute "Input HTML" dans le formulaire
    Si "si_type_alarme" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "si_type_ssi" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "si_conformite_l16" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "si_alimentation_remplacement" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "si_service_securite" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Click Element  css=div.bloc-details-etablissement legend.collapsible


Ajouter l'établissement

    [Arguments]  ${values}

    Depuis le formulaire d'ajout d'établissement
    Saisir les valeurs dans le formulaire de l'établissement  ${values}
    Click On Submit Button
    Valid Message Should Contain  Vos modifications ont bien été enregistrées.
    ${etablissement_code} =  Get Text  css=#code
    [return]    ${etablissement_code}


Modifier l'établissement
    [Arguments]  ${values}  ${code}=null  ${libelle}=null

    Depuis le formulaire de modification de l'établissement  ${code}  ${libelle}
    Saisir les valeurs dans le formulaire de l'établissement  ${values}
    Click On Submit Button
    Valid Message Should Contain  Vos modifications ont bien été enregistrées.


Supprimer l'établissement
    [Arguments]    ${libelle}
    Go To Dashboard
    Go To Submenu In Menu    etablissements    etablissement_tous

    Input Text    css=#libelle    ${libelle}
    Click On Search Button
    Click On Link    ${libelle}

    Click On Form Portlet Action    etablissement_tous    supprimer
    Click On Submit Button
    Valid Message Should Be    La suppression a été correctement effectuée.


Depuis la vue 'géolocaliser' (SIG interne) de l'établissement
    [Tags]  etablissement
    [Documentation]  Accède à la vue géolocaliser de l'enregistrement de type
    ...  'établissement' (etablissement).
    [Arguments]  ${code}=null  ${libelle}=null  ${contexte}=tous

    Depuis le contexte de l'établissement  ${code}  ${libelle}
    Cliquer sur l'action 'géolocaliser' dans le contexte de l'établissement
    La page ne doit pas contenir d'erreur


Cliquer sur l'action 'géolocaliser' dans le contexte de l'établissement
    [Tags]  etablissement  sig_interne
    [Documentation]  Clique sur l'action géolocaliser depuis les vues
    ...  consulter et géolocaliser d'un enregistrement de type 'établissement'
    ...  (etablissement).
    ...
    ...  Si l'option SIG interne est activée, alors un portlet d'actions
    ...  contextuelles est disponible depuis les vues consulter et
    ...  géolocaliser d'un enregistrement de type 'établissement'
    ...  (etablissement). Ce keyword permet de cliquer sur cette action.

    Click Element  css=#switch-geolocaliser-consulter a.localiser-sig-interne-16


Cliquer sur l'action 'consulter' dans le contexte de l'établissement
    [Tags]  etablissement  sig_interne
    [Documentation]  Clique sur l'action consulter depuis les vues
    ...  consulter et géolocaliser d'un enregistrement de type 'établissement'
    ...  (etablissement).
    ...
    ...  Si l'option SIG interne est activée, alors un portlet d'actions
    ...  contextuelles est disponible depuis les vues consulter et
    ...  géolocaliser d'un enregistrement de type 'établissement'
    ...  (etablissement). Ce keyword permet de cliquer sur cette action.

    Click Element  css=#switch-geolocaliser-consulter a.consult-16


Se positionner dans la carte (SIG interne)
    [Tags]  sig_interne
    [Documentation]  La carte du module SIG interne peut être chargée
    ...  dans le corps de la page, dans une iframe ou dans une nouvelle fenêtre.
    ...  Ce mot clé nous permet de nous positionner dans la bonne frame, ou
    ...  fenêtre si c'est nécessaire.
    ...
    ...  ``mode`` permet d'indiquer dans quel cas nous sommes : *iframe*,
    ...  *new_window*, ...
    [Arguments]  ${mode}

    Run Keyword If  '${mode}' == 'iframe'  Run Keywords
    ...  Select Frame
    ...  //iframe
    ...  AND
    ...  Log
    ...  XXX On ne peut pas utiliser le keyword 'La page ne doit pas contenir d'erreur' suivant à cause d'une incompatibilité entre 'Select Frame' et 'Page Should Not Contain'
    # ...  AND
    # ...  La page ne doit pas contenir d'erreur
    Run Keyword If  '${mode}' == 'new_window'  Run Keywords
    ...  Select Window  NEW
    ...  AND
    ...  La page ne doit pas contenir d'erreur


Quitter la carte (SIG interne)
    [Tags]  sig_interne
    [Documentation]  La carte du module SIG interne peut être chargée
    ...  dans le corps de la page, dans une iframe ou dans une nouvelle fenêtre.
    ...  Ce mot clé permet de nous remettre dans le contexte standard en
    ...  déselectionnant la frame ou la fenêtre.
    ...
    ...  ``mode`` permet d'indiquer dans quel cas nous sommes : *iframe*,
    ...  *new_window*, ...
    [Arguments]  ${mode}

    Run Keyword If  '${mode}' == 'iframe'  Unselect Frame
    Run Keyword If  '${mode}' == 'new_window'  Run Keywords
    ...  Close Window
    ...  AND
    ...  Select Window

