*** Settings ***
Documentation     Actions spécifiques aux dossiers de coordination.

*** Keywords ***
Depuis le contexte du dossier de coordination

   [Documentation]    Permet d'accéder à l'écran de visualisation d'un dossier de coordination.

    [Arguments]    ${dossier_coordination}

    # On se positionne sur le tableau de bord
    Go To Dashboard
    # On ouvre le menu
    Go To Submenu In Menu    dossiers    dossier_coordination
    # On fait une recherche sur le libellé du DC
    Input Text    css=div#adv-search-adv-fields input#libelle    ${dossier_coordination}
    Click On Search Button
    # On accède à la visualisation du DC
    Click On Link    ${dossier_coordination}
    #
    Page Title Should Contain    ${dossier_coordination}

Depuis le formulaire d'ajout du dossier de coordination
    [Documentation]
    Go To  ${PROJECT_URL}${OM_ROUTE_FORM}&obj=dossier_coordination_nouveau&action=0&retour=form


Depuis le formulaire de modification du dossier de coordination

    [Documentation]

    [Arguments]    ${dossier_coordination}

    Depuis le contexte du dossier de coordination    ${dossier_coordination}
    Click On Form Portlet Action    dossier_coordination    modifier


Clôturer le dossier de coordination

    [Documentation]    Permet de clôturer une dossier de coordination.

    [Arguments]    ${dossier_coordination_libelle}

    #
    Depuis le contexte du dossier de coordination    ${dossier_coordination_libelle}
    # On vérifie que l'action de clôturer soit affichée
    Portlet Action Should Be In Form    dossier_coordination    cloturer
    # On clique sur l'action de clôturer
    Click On Form Portlet Action    dossier_coordination    cloturer
    # On vérifie que le DC a été correctement clôturé
    WUX  Valid Message Should Contain    Le dossier de coordination a été correctement cloturé.


Saisir les valeurs dans le formulaire du dossier de coordination

    [Documentation]
    ...  @{ref_cad} =  Create List  806  AB  25
    ...  &{dc00} =  Create Dictionary
    ...  dossier_coordination_type=
    ...  description=
    ...  etablissement=
    ...  date_demande=
    ...  a_qualifier=
    ...  dossier_instruction_secu=
    ...  dossier_instruction_acc=
    ...  dossier_coordination_parent=
    ...  erp=
    ...  etablissement_type=
    ...  date_butoir=
    ...  dossier_autorisation_ads=
    ...  dossier_instruction_ads=
    ...  terrain_references_cadastrales=${ref_cad}
    [Arguments]  ${values}

    #
    Si "dossier_coordination_type" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    #
    Si "description" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "date_demande" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "date_butoir" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    #
    Si "a_qualifier" existe dans "${values}" on execute "Set Checkbox" dans le formulaire
    #
    Si "dossier_instruction_secu" existe dans "${values}" on execute "Set Checkbox" dans le formulaire
    Si "dossier_instruction_acc" existe dans "${values}" on execute "Set Checkbox" dans le formulaire
    #
    Si "etablissement" existe dans "${values}" on sélectionne la valeur sur l'autocomplete "etablissement_tous" dans le formulaire
    #
    Si "dossier_coordination_parent" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "erp" existe dans "${values}" on execute "Set Checkbox" dans le formulaire
    Si "etablissement_type" existe dans "${values}" on execute "Select From Chosen List For 'Type' By Name" dans le formulaire
    #
    Si "dossier_autorisation_ads" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "dossier_instruction_ads" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "terrain_references_cadastrales" existe dans "${values}" on execute "Saisir les références cadastrales"


Ajouter le dossier de coordination

    [Documentation]

    [Arguments]  ${values}

    Depuis le formulaire d'ajout du dossier de coordination
    Saisir les valeurs dans le formulaire du dossier de coordination  ${values}
    # On vérifie que les DI sont correctement créés si la case à cocher 'à qualifier' est décochée
    # et que la case à cocher du DI est cochée.
    ${a_qualifier_checked} =    Run Keyword And Return Status    Checkbox Should Be Selected    css=#a_qualifier
    ${di_si_checked} =    Run Keyword And Return Status    Checkbox Should Be Selected    css=#dossier_instruction_secu
    ${di_acc_checked} =    Run Keyword And Return Status    Checkbox Should Be Selected    css=#dossier_instruction_acc
    Click On Submit Button
    Valid Message Should Contain  Vos modifications ont bien été enregistrées.
    Run Keyword If    '${a_qualifier_checked}' == 'False' and '${di_si_checked}' == 'True'    Valid Message Should Contain    -SI a été créé.
    Run Keyword If    '${a_qualifier_checked}' == 'False' and '${di_acc_checked}' == 'True'    Valid Message Should Contain    -ACC a été créé.
    # On récupère le libelle du dossier de coordination
    ${dc_libelle} =  Get Text  css=div.form-content span#libelle

    [Return]    ${dc_libelle}


Depuis le listing des dossiers de coordination

    Go To  ${PROJECT_URL}${OM_ROUTE_TAB}&obj=dossier_coordination


Depuis le listing des dossiers de coordination à qualifier

    Go To  ${PROJECT_URL}${OM_ROUTE_TAB}&obj=dossier_coordination_a_qualifier


Saisir les références cadastrales
    [Arguments]    ${references_cadastrales}
    [Documentation]  Permet de saisir un nombre "infini" de références cadastrales sur une
    ...  seule ligne. Ce mot clé recoit une liste de références cadastrales avec un élément
    ...  par ligne, ex: @{ref_cad} =  Create List  806  AB  0001  A  0050
    ...  Ce mot-clé clique sur le bouton "ajouter d'autres champs" autant de fois que
    ...  nécessaire.

    # Initialisation du compteur à 1
    ${i} =  Set Variable  1
    :FOR  ${values}  IN  @{references_cadastrales}
    \    # S'il y a plus de 3 champs à saisir et qu'il manque des champs de saisie dans le formulaire
    \    Run Keyword If  ${i} > 3 and (${i}-3)%2 == 1  Click Element  moreFieldReferenceCadastrale0
    \    # Insertion de la valeur dans le champ
    \    WUX  Input Text  css=.reference_cadastrale_custom_fields .reference_cadastrale_custom_field:nth-child(${i})  ${values}
    \    # Incrémentation du compteur
    \    ${i}  Evaluate  ${i}+1


Modifier le dossier de coordination
    [Arguments]  ${values}  ${dossier_coordination}

    Depuis le formulaire de modification du dossier de coordination  ${dossier_coordination}
    Saisir les valeurs dans le formulaire du dossier de coordination  ${values}
    Click On Submit Button
    Valid Message Should Contain  Vos modifications ont bien été enregistrées.


Depuis l'onglet DC Fils du dossier de coordination
    [Documentation]    Permet d'accéder à l'onglet "DC Fils" dans le contexte d'un dossier de coordination.
    [Arguments]    ${dossier_coordination}
    Depuis le contexte du dossier de coordination  ${dossier_coordination}
    On clique sur l'onglet  dossier_coordination  DC Fils


Depuis l'onglet "Contacts" du DC
    [Documentation]  Permet d'accéder à l'onglet "Contacts" dans le contexte d'un dossier de coordination.
    [Arguments]    ${dossier_coordination}
    Depuis le contexte du dossier de coordination  ${dossier_coordination}
    On clique sur l'onglet  contact_contexte_dossier_coordination  Contacts

