*** Settings ***
Documentation    Actions spécifiques aux voies

*** Keywords ***

Depuis le contexte voie
    [Documentation]  Accède au formulaire
    [Arguments]  ${voie}

    # On accède au tableau
    Depuis le listing  voie
    # On recherche l'enregistrement
    Use Simple Search  libellé  ${voie}
    # On clique sur le résultat
    Click On Link  ${voie}
    # On vérifie qu'il n'y a pas d'erreur
    La page ne doit pas contenir d'erreur

Ajouter voie
    [Documentation]  Crée l'enregistrement
    [Arguments]  ${values}

    # On accède au tableau
    Depuis le listing  voie
    # On clique sur le bouton ajouter
    Click On Add Button
    # On saisit des valeurs
    Saisir voie  ${values}
    # On valide le formulaire
    Click On Submit Button
    # On récupère l'ID du nouvel enregistrement
    ${voie} =  Get Text  css=div.form-content span#voie
    # On le retourne
    [Return]  ${voie}

Modifier voie
    [Documentation]  Modifie l'enregistrement
    [Arguments]  ${voie}  ${values}

    # On accède à l'enregistrement
    Depuis le contexte voie  ${voie}
    # On clique sur le bouton modifier
    Click On Form Portlet Action  voie  modifier
    # On saisit des valeurs
    Saisir voie  ${values}
    # On valide le formulaire
    Click On Submit Button
    WUX  Valid Message Should Contain    Vos modifications ont bien été enregistrées.

Supprimer voie
    [Documentation]  Supprime l'enregistrement
    [Arguments]  ${voie}

    # On accède à l'enregistrement
    Depuis le contexte voie  ${voie}
    # On clique sur le bouton supprimer
    Click On Form Portlet Action  voie  supprimer
    # On valide le formulaire
    Click On Submit Button

Saisir voie
    [Documentation]  Remplit le formulaire
    [Arguments]  ${values}

    Si "libelle" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "rivoli" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "om_validite_debut" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "om_validite_fin" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "om_collectivite" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "id_voie_ref" existe dans "${values}" on execute "Input Text" dans le formulaire
