*** Settings ***
Documentation     Actions spécifiques à l'application

*** Keywords ***
WUX
    [Documentation]  "WUX -> WUKS -> Wait Until Keyword Succeds" Alias pour
    ...  gagner en lisibilité.
    [Arguments]  @{args}
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  @{args}


Select From Chosen List For 'Type' By Name
    [Arguments]  ${field_id}  ${value}
    Select From Chosen List  css=#${field_id}  [${value}]

Select From Chosen List For 'Catégorie' By Name
    [Arguments]  ${field_id}  ${value}
    Select From Chosen List  css=#${field_id}  [${value}]

Select From Chosen List For 'Lettre Type' By Name
    [Arguments]  ${field_id}  ${value}
    Select From Chosen List  css=#${field_id}  [${value}]

Selected Label From Chosen List Should Be
    [Arguments]  ${field_id}  ${value}
    ${label_selected} =  Get Text  ${field_id}_chosen span
    Should Be Equal As Strings  ${label_selected}  ${value}

Select From Chosen List
    [Arguments]  ${field_id}  ${value}
    Click Element  ${field_id}_chosen
    Input Text  ${field_id}_chosen input.chosen-search-input  ${value}
    Press Key  ${field_id}_chosen input.chosen-search-input  \\09

Select From Multiple Chosen List
    [Arguments]  ${field_id}  ${values}
    Click Element  ${field_id}_chosen
    :FOR  ${ELEMENT}  IN  @{values}
    \  Input Text  ${field_id}_chosen input.chosen-search-input  ${ELEMENT}
    \  Press Key  ${field_id}_chosen input.chosen-search-input  \\13
    Press Key  ${field_id}_chosen input.chosen-search-input  \\09

Unselect From Chosen List
    [Arguments]  ${field_id}
    Click Element  ${field_id}_chosen .search-choice-close

Select value in autocomplete
    [Documentation]
    [Arguments]  ${autocomplete_name}=null  ${search_value}=null  ${value}=null  ${target_id}=null
    Execute Javascript  window.document.getElementById("autocomplete-${autocomplete_name}-search").scrollIntoView(true);
    Input Text  css=#autocomplete-${autocomplete_name}-search  ${search_value}
    WUX  Page Should Contain Element  xpath=//ul[contains(@class, 'ui-autocomplete')]/li[contains(@class, 'ui-menu-item')]/a[text()="${value}"]
    # On lie cet établissement
    WUX  Click Element Until No More Element  css=a.autocomplete-${autocomplete_name}-result-${target_id}
    WUX  Element Should Be Visible  css=#autocomplete-${autocomplete_name}-check
    Element Should Be Visible  css=#autocomplete-${autocomplete_name}-empty
    Page Should Contain Element  xpath=//*[@id='autocomplete-${autocomplete_name}-search' and @readonly='']

Modifier exploitant depuis la fiche d'un établissement
    [Documentation]    Permet de modifier les données de l'exploitant
    ...    d'un établissement.
    [Arguments]    ${etablissement_code}    ${exp_civilite}    ${exp_nom}    ${exp_prenom}    ${meme_adresse}=true
    # On clique sur le tableau de bord
    Go To Dashboard
    # On accède au menu
    Go To Submenu In Menu    etablissements    etablissement_tous
    # On recherche l'établissement
    Input Text    css=div#adv-search-adv-fields input#etablissement    ${etablissement_code}
    Click On Search Button
    # On clique sur l'établissement
    Click Link    ${etablissement_code}
    # On clique sur le bouton modifier
    Click On Form Portlet Action    etablissement_tous    modifier
    # On sélectionne la civilité
    Select From List By Label    css=#exp_civilite    ${exp_civilite}
    # On saisit le nom
    Input Text    css=#exp_nom    ${exp_nom}
    # On saisit le prénom
    Input Text    css=#exp_prenom    ${exp_prenom}
    # On coche/décoche la case même adresse
    Run keyword If    '${meme_adresse}' == 'true'    Select Checkbox    css=#meme_adresse
    Run keyword If    '${meme_adresse}' == 'false'    Unselect Checkbox    css=#meme_adresse
    # On valide le formulaire
    Click On Submit Button
    # On vérifie le message de validation
    Valid Message Should Be    Vos modifications ont bien été enregistrées.

Ajouter un particulier depuis un établissement
    [Documentation]    Permet d'ajouter un contact à un établissement.
    [Arguments]    ${etablissement_code}    ${contact_type}    ${civilite}    ${nom}    ${prenom}    ${reception_convocation}=false
    #
    Depuis le contexte de l'établissement    ${etablissement_code}
    # On clique sur l'onglet "Contacts"
    On clique sur l'onglet    contact    Contacts
    # On clique sur le bouton d'ajout
    Click On Add Button JS
    # On saisit le contact
    Saisir un particulier    ${contact_type}    ${civilite}    ${nom}    ${prenom}    ${reception_convocation}
    # On valide le formulaire
    Click On Submit Button In Subform
    # On vérifie le message de validation
    Valid Message Should Be    Vos modifications ont bien été enregistrées.

Saisir un particulier
    [Documentation]    Permet de remplir le formulaire d'un contact.
    [Arguments]    ${contact_type}    ${civilite}    ${nom}    ${prenom}    ${reception_convocation}=false
    # On sélectionne le type
    Select From List By Label    css=#contact_type    ${contact_type}
    # On sélectionne la civilité
    Select From List By Label    css=#civilite    ${civilite}
    # On saisit le nom
    Input Text    css=#nom    ${nom}
    # On saisit le prénom
    Input Text    css=#prenom    ${prenom}
    # On coche/décoche la case reception_convocation
    Run keyword If    '${reception_convocation}' == 'true'    Select Checkbox    css=#reception_convocation
    Run keyword If    '${reception_convocation}' == 'false'    Unselect Checkbox    css=#reception_convocation


Ajouter ou modifier l'enregistrement de type 'paramètre' (om_parametre)
    [Tags]
    [Documentation]  Ajoute ou modifie l'enregsitrement de type 'paramètre' (om_parametre).
    ...
    ...  Vérife si l'enregistrement existe, si c'est le cas on le modifie sinon on l'ajoute.
    ...
    ...  Exemple :
    ...
    ...  | &{values} =  Create Dictionary
    ...  | ...  libelle=option_localisation
    ...  | ...  valeur=sig_interne
    ...  | Ajouter ou modifier l'enregistrement de type 'paramètre' (om_parametre)  ${values}
    [Arguments]  ${values}

    Depuis le listing des paramètres
    Use Simple Search  libellé  ${values.libelle}
    ${notexists} =  Run Keyword And Return Status  Element Should Contain  css=.tab-data  Aucun enregistrement.
    Run Keyword If  ${notexists} == False  Modifier le paramètre  ${values.libelle}  ${values.valeur}  null
    Run Keyword If  ${notexists} == True  Ajouter le paramètre depuis le menu  ${values.libelle}  ${values.valeur}  null


La page du fichier PDF doit contenir les chaînes de caractères
    [Tags]
    [Documentation]  Change de fenêtre, fait les verifs, et ferme la fenêtre.
    ...  Il faut donc avoir déclenché l'ouverture de la fenêtre avant.
    ...  L'argument window est {name, title, url} de la fenêtre sans le suffixe '.php'
    [Arguments]  ${window}  ${strings_to_find}  ${page_number}=1  ${from_system}=False

    Run Keyword If  ${from_system}==False  Open PDF  ${window}
    PDF Move Page To  ${page_number}
    La page ne doit pas contenir d'erreur
    ${result} =  Get Text  css=#pageContainer${page_number}
    ${result} =  Set Variable  ${result.replace('\n', ' ')}
    :FOR  ${string}  IN  @{strings_to_find}
    \    Should Contain  ${result}  ${string}
    Run Keyword If  ${from_system}==False  Close PDF


Ajouter les permissions au profil
    [Tags]  om_profil  om_droit
    [Documentation]
    [Arguments]  ${profil_libelle}  ${permissions_to_add}
    Depuis le contexte du profil  null  ${profil_libelle}
    Click On Form Portlet Action  om_profil  modifier
    :FOR  ${permission}  IN  @{permissions_to_add}
    \  Select Checkbox  css=input[name="permissions[]"][value="${permission}"]
    Click On Submit Button
    Valid Message Should Contain  Vos modifications ont bien été enregistrées.


Supprimer les permissions au profil
    [Tags]  om_profil  om_droit
    [Documentation]
    [Arguments]  ${profil_libelle}  ${permissions_to_remove}
    Depuis le contexte du profil  null  ${profil_libelle}
    Click On Form Portlet Action  om_profil  modifier
    :FOR  ${permission}  IN  @{permissions_to_remove}
    \  Unselect Checkbox  css=input[name="permissions[]"][value="${permission}"]
    Click On Submit Button
    Valid Message Should Contain  Vos modifications ont bien été enregistrées.


Depuis le listing des enregistrements de type 'widget' (om_widget)
    [Tags]  om_widget
    [Documentation]  Accède directement via URL au listing des widgets.
    Depuis le listing  om_widget


Depuis le formulaire d'ajout d'un enregistrement de type 'widget' (om_widget)
    [Tags]  om_widget
    [Documentation]  Accède directement via URL au formulaire d'ajout d'une
    ...  catégorie de réunion.
    Go To  ${PROJECT_URL}${OM_ROUTE_FORM}&obj=om_widget&action=0&retour=form


Depuis le contexte de l'enregistrement de type 'widget' (om_widget)
    [Documentation]
    [Arguments]  ${libelle}
    Depuis le listing des enregistrements de type 'widget' (om_widget)
    Use Simple Search  libellé  ${libelle}
    Click On Link  ${libelle}


Depuis le formulaire de modification de l'enregistrement de type 'widget' (om_widget)
    [Tags]  om_widget
    [Arguments]  ${libelle}
    Depuis le contexte de l'enregistrement de type 'widget' (om_widget)  ${libelle}
    Click On Form Portlet Action  om_widget  modifier


Modifier l'enregistrement de type 'widget' (om_widget)
    [Tags]  om_widget
    [Documentation]  Modifie l'enregistrement
    [Arguments]  ${libelle}  ${values}
    Depuis le formulaire de modification de l'enregistrement de type 'widget' (om_widget)  ${libelle}
    Saisir les valeurs dans le formulaire de type 'widget' (om_widget)  ${values}
    Click On Submit Button
    Valid Message Should Be  Vos modifications ont bien été enregistrées.


Ajouter l'enregistrement de type 'widget' (om_widget)
    [Tags]  om_widget
    [Documentation]  Crée l'enregistrement
    [Arguments]  ${values}
    Depuis le formulaire d'ajout d'un enregistrement de type 'widget' (om_widget)
    Saisir les valeurs dans le formulaire de type 'widget' (om_widget)  ${values}
    Click On Submit Button
    Valid Message Should Be  Vos modifications ont bien été enregistrées.


Saisir les valeurs dans le formulaire de type 'widget' (om_widget)
    [Tags]  om_widget
    [Documentation]  Remplit le formulaire
    [Arguments]  ${values}
    Si "libelle" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "type" existe dans "${values}" on execute "Select From List By Value" dans le formulaire
    Si "arguments" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "script" existe dans "${values}" on execute "Select From List By Value" dans le formulaire
    Si "lien" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "texte" existe dans "${values}" on execute "Input Text" dans le formulaire


Ajouter ou modifier l'enregistrement de type 'widget' (om_widget)
    [Tags]  om_widget
    [Documentation]  Ajoute ou modifie l'enregsitrement de type 'widget' (om_widget).
    ...
    ...  Vérife si l'enregistrement existe, si c'est le cas on le modifie sinon on l'ajoute.
    ...
    ...  Exemple :
    ...
    ...  | &{values} =  Create Dictionary
    ...  | ...  libelle=Analyses à valider
    ...  | ...  type=file
    ...  | ...  script=analyse_a_valider
    ...  | ...  arguments=
    ...  | Ajouter ou modifier l'enregistrement de type 'widget' (om_widget)  ${values}
    [Arguments]  ${values}

    Depuis le listing des enregistrements de type 'widget' (om_widget)
    Use Simple Search  libellé  ${values.libelle}
    ${notexists} =  Run Keyword And Return Status  Element Should Contain  css=.tab-data  Aucun enregistrement.
    Run Keyword If  ${notexists} == False  Modifier l'enregistrement de type 'widget' (om_widget)  ${values.libelle}  ${values}
    Run Keyword If  ${notexists} == True  Ajouter l'enregistrement de type 'widget' (om_widget)  ${values}

