*** Settings ***
Resource  resources/resources.robot
Suite Setup  For Suite Setup
Suite Teardown  For Suite Teardown
Documentation  Test suite Procès-verbal :
...  génération par la(le) secrétaire,
...  regénération,
...  ajout.


*** Test Cases ***
Constitution d'un jeu de données

    [Documentation]  L'objet de ce 'Test Case' est de constituer un jeu de
    ...  données cohérent pour les scénarios fonctionnels qui suivent.

    # En tant que profil ADMINISTRATEUR
    Depuis la page d'accueil  admin  admin

    # On définit une liste de clés pour lesquelles on souhaite vérifier
    # l'absence dans le fichier de métadonnées. Ce sont les métadonnées
    # spécifiques aux arrêtés.
    @{md_no} =  Create List
    ...  arrete_numero
    ...  arrete_reglementaire
    ...  arrete_notification
    ...  arrete_date_notification
    ...  arrete_publication
    ...  arrete_date_publication
    ...  arrete_temporaire
    ...  arrete_expiration
    ...  arrete_date_controle_legalite
    ...  arrete_nature_acte
    ...  arrete_nature_acte_niv1
    ...  arrete_nature_acte_niv2
    Set Suite Variable  ${md_no}

    ##
    ## SIGNATAIRE 01
    ##
    &{signataire01} =  Create Dictionary
    ...  nom=Dubois
    ...  prenom=Julien
    ...  signature=Monsieur le Maire, Julien Dubois
    ...  civilite=M.
    ...  signataire_qualite=Maire
    Ajouter le signataire  ${signataire01}
    Set Suite Variable  ${signataire01}

    ##
    ## CATEGORIE DE REUNION 01
    ##
    &{reucategorie01} =  Create Dictionary
    ...  code=DIV
    ...  libelle=Divers
    ...  description=Tout ce ne qui ne rentre pas dans les autres catégories
    ...  service=Sécurité Incendie
    ...  ordre=40
    Ajouter la catégorie de réunion  ${reucategorie01}
    Set Suite Variable  ${reucategorie01}

    ##
    ## INSTANCE DE REUNION 01
    ##
    &{reuinstance01} =  Create Dictionary
    ...  code=MP
    ...  libelle=Marins Pompier
    ...  service=Sécurité Incendie
    Ajouter l'instance de réunion  ${reuinstance01}
    Set Suite Variable  ${reuinstance01}

    ##
    ## TYPE DE REUNION 01
    ##
    @{categories_autorisees}  Create List  ${reucategorie01.libelle}
    @{avis_autorises}  Create List  DIFFERE
    @{instances_autorisees}  Create List  ${reuinstance01.libelle}
    &{reutype01} =  Create Dictionary
    ...  code=NC
    ...  libelle=Ordre du jour inconnu
    ...  service=Sécurité Incendie
    ...  categories_autorisees=@{categories_autorisees}
    ...  avis_autorises=@{avis_autorises}
    ...  instances_autorisees=@{instances_autorisees}
    Ajouter le type de réunion  ${reutype01}
    Set Suite Variable  ${reutype01}

    ##
    ## REUNION 01
    ##
    &{reunion01} =  Create Dictionary
    ...  reunion_type=${reutype01.libelle}
    ...  date_reunion=${DATE_FORMAT_DD/MM/YYYY}
    ...  date_reunion_yyyy_mm_dd=${DATE_FORMAT_YYYY-MM-DD}
    Ajouter la réunion  ${reunion01}
    ${reunion01_code} =  Set Variable  ${reutype01.code}-${reunion01.date_reunion_yyyy_mm_dd}
    Set Suite Variable  ${reunion01}
    Set Suite Variable  ${reunion01_code}

    ##
    ## ETABLISSEMENT 01
    ##
    &{etab01} =  Create Dictionary
    ...  libelle=LEVALET ETS
    ...  etablissement_nature=ERP Référentiel
    ...  etablissement_type=R
    ...  etablissement_categorie=1
    ...  si_autorite_competente_visite=Commission communale de sécurité
    ...  siret=73282932000074
    ...  adresse_numero=12
    ...  adresse_voie=RUE DE ROME
    ...  adresse_cp=13006
    ...  adresse_ville=MARSEILLE
    ...  adresse_arrondissement=6ème
    ...  exp_civilite=M.
    ...  exp_nom=DEUXRE
    ...  exp_prenom=Javier
    ...  etablissement_etat=Ouvert
    ${etab01_code} =  Ajouter l'établissement  ${etab01}
    ${etab01_titre} =  Set Variable  ${etab01_code} - ${etab01.libelle}
    Set Suite Variable  ${etab01}
    Set Suite Variable  ${etab01_code}
    Set Suite Variable  ${etab01_titre}

    ##
    ## DOSSIER DE COORDINATION 01
    ##
    &{dc01} =  Create Dictionary
    ...  dossier_coordination_type=Visite de réception
    ...  description=Aménagement du sous-sol
    ...  date_demande=08/01/2015
    ...  etablissement=${etab01_titre}
    ...  a_qualifier=false
    ...  dossier_autorisation_ads=AT013055160001
    ${dc01_libelle} =  Ajouter le dossier de coordination  ${dc01}
    Set Suite Variable  ${dc01}
    Set Suite Variable  ${dc01_libelle}
    Set Suite Variable  ${dc01_di_si}  ${dc01_libelle}-SI
    Set Suite Variable  ${dc01_di_acc}  ${dc01_libelle}-ACC

    ##
    ## DEMANDE DE PASSAGE EN REUNION 01
    ##
    # Par défaut c'est la date du jour qui est positionnée
    &{dpr01} =  Create Dictionary
    ...  dossier_instruction=${dc01_di_si}
    ...  date_souhaitee=${DATE_FORMAT_DD/MM/YYYY}
    ...  reunion_type=${reutype01.libelle}
    ...  reunion_type_categorie=${reucategorie01.libelle}
    ...  reunion_code=${reunion01_code}
    ...  reunion_date_format_yyyy_mm_dd=${reunion01.date_reunion_yyyy_mm_dd}
    Planifier directement le DC ou DI pour la réunion dans la catégorie
    ...  ${dpr01.dossier_instruction}
    ...  ${dpr01.reunion_code}
    ...  ${dpr01.reunion_type_categorie}
    Set Suite Variable  ${dpr01}

    #
    #
    #
    # On rédige la proposition d'avis de l'analyse sécurité incendie
    Accéder à l'analyse du dossier d'instruction  ${dc01_di_si}
    #
    Éditer bloc analyse  avis_propose
    WUX  Select From List By Label    css=#reunion_avis    DIFFERE
    Input Text    css=#avis_complement    Nécessite une nouvelle visite
    Submit Form    css=#form_analyses_modifier
    WUX  Valid Message Should Be    Vos modifications ont bien été enregistrées.
    Click Element    css=#sousform-analyses a.retour

    #
    Accéder à l'analyse du dossier d'instruction  ${dc01_di_si}
    Terminer l'analyse
    Valider l'analyse


Prévisualisation du PV

    [Documentation]  L'objet de ce 'Test Case' est de vérifier les
    ...  particularités de l'édition PDF permettant de prévisualiser le PV
    ...  depuis l'analyse. Les particularités sont : la mention DOCUMENT DE
    ...  TRAVAIL doit apparaître, les champs de fusion concernant le procès
    ...  verbal ne doivent pas être remplacés mais apparaître tel quel entre
    ...  crochets.

    # En tant que profil SECRETAIRE SI
    Depuis la page d'accueil  secretaire-si  secretaire-si

    ## VERIFICATION DU CONTENU DU FICHIER PDF
    # On ouvre le PDF en cliquant sur l'action PV (prévisualisation) du
    # portlet d'actions de la vue analyse
    Accéder à l'analyse du dossier d'instruction  ${dc01_di_si}
    Cliquer action analyse  proces_verbal
    # On se positionne dans le contexte du fichier PDF
    WUX  Open PDF  ${OM_PDF_TITLE}
    # On vérifie les particularités du PDF
    WUX  Page Should Contain  DOCUMENT DE TRAVAIL
    WUX  Page Should Contain  [proces_verbal.numero]
    PDF Pages Number Should Be    3
    # On ferme le PDF et on revient à la fenêtre principale
    Close PDF


Génération d'un PV

    [Documentation]  L'objet de ce 'Test Case' est de vérifier la fonctionnalité
    ...  de génération de PV sur une analyse. Les points importants sont :
    ...  - vérification de l'intégration du widget 'analyses à acter' pour le
    ...    profil SECRETAIRE SI
    ...  - vérification de l'intégration de la fonction générer un nouveau PV
    ...    pour le profil SECRETAIRE SI
    ...  - la création du numéro de PV
    ...  - la finalisation de l'édition (stockage + document de travail
    ...    + métadonnées)
    ...  - le changement d'état de l'analyse en actée
    ...  - la mise à jour de la proposition d'avis ???

    # En tant que profil SECRETAIRE SI
    Depuis la page d'accueil  secretaire-si  secretaire-si

    ##
    ## Intégration du widget 'analyses à acter' et de la fonction générer un
    ## nouveau PV et de la création du numéro de PV
    ##
    ## La génération du PV est le tâche d'un utilisateur avec le profil
    ## 'SECRETAIRE SI'. L'analyse doit être validée don apparaître dans
    ## le widget du tableau de bord 'Analyses à acter'.

    # On vérifie que le dossier apparaît bien dans le widget 'analyse à acter'
    #XXX
    # On clique sur le lien pour accéder au dossier d'instruction
    Click On Link  ${dc01.description}
    # On vérifie que nous sommes bien sur le dossier d'instruction en question
    #XXX
    # On vérifie que la page ne contient pas d'erreurs
    La page ne doit pas contenir d'erreur
    # On clique sur l'onglet PV du dossier d'instruction
    On clique sur l'onglet  proces_verbal  PV
    L'action 'Générer un nouveau PV' doit être disponible
    # On clique sur le bouton 'Générer un nouveau PV'
    Click Element  css=#generer_pv
    # On vérifie que le formulaire de génération est correctement ouvert
    # en vérifiant qu'un des champs du formulaire est présent à l'écran
    WUX  Element Should Contain  css=#sousform-proces_verbal  odèle d'édition à utiliser lors de la génération
    # On vérifie que la page ne contient pas d'erreurs
    La page ne doit pas contenir d'erreur
    # On sélectionne un passage en réunion
    Select From List By Label  css=#dossier_instruction_reunion  ${reunion01.date_reunion} - ${reutype01.libelle}
    # On sélectionne un signataire
    Select From List By Label  css=#signataire  ${signataire01.civilite} ${signataire01.signataire_qualite} ${signataire01.nom} ${signataire01.prenom}
    # On valide le formulaire
    Click On Submit Button In Subform
    # On vérifie que le message de validation est correct
    Valid Message Should Contain In Subform  Vos modifications ont bien été enregistrées.
    # On vérifie que la page ne contient pas d'erreurs
    La page ne doit pas contenir d'erreur
    # On récupère le numéro de PV présent dans le message de validation
    ${dc01_di_si_pv01_numero} =  Get Text  css=#new_pv_number
    # On la valeur pour une utilisation dans la suite du test
    Set Suite Variable  ${dc01_di_si_pv01_numero}
    # On retourne à la liste des PV
    Click On Back Button In Subform
    # On clique sur le PV nouvellement généré
    Click On Link  ${dc01_di_si_pv01_numero}

    ##
    ## Finalisation de l'édition (stockage + document de travail)
    ## La mention document de travail dans l'édition ne doit plus apparaître
    ##
    # On ouvre le PDF en cliquant sur le lien 'Télécharger' du champ fichier
    Click Element  css=#om_fichier_finalise span.reqmo-16 a
    WUX  Open PDF  ${OM_PDF_TITLE}
    # On vérifie les particularités du PDF
    WUX  Page Should Contain  ${dc01_di_si_pv01_numero}
    Page Should Not Contain    DOCUMENT DE TRAVAIL
    PDF Pages Number Should Be    3
    # On ferme le PDF et on revient à la fenêtre principale
    Close PDF

    # En tant que profil ADMINISTRATEUR
    Depuis la page d'accueil  admin  admin

    ${docgen01_id} =  Récupérer l'identifiant du document généré lié depuis le procès verbal numéro  ${dc01_di_si}  ${dc01_di_si_pv01_numero}
    ${docgen01_code_barres} =  Récupérer le code barres du document généré à partir de son identifiant  ${docgen01_id}
    Set Suite Variable  ${docgen01_id}
    Set Suite Variable  ${docgen01_code_barres}


    ##
    ## Gestion des métadonnées
    ##
    ## Vérifie les métadonnées pour le champ fichier :
    ## - [courrier.om_fichier_finalise_courrier] / Cas PV.
    ##

    ## => [courrier.om_fichier_finalise_courrier] (METADATA FILESTORAGE)
    ${docgen01_fichier_finalise_uid} =  Récupérer l'uid du fichier finalisé du document généré  ${docgen01_code_barres}
    ${docgen01_fichier_finalise_path} =  Récupérer le chemin vers le fichier correspondant à l'uid  ${docgen01_fichier_finalise_uid}
    ${docgen01_fichier_finalise_md_path} =  Récupérer le chemin vers le fichier de métadonnées correspondant à l'uid  ${docgen01_fichier_finalise_uid}
    Le fichier doit exister  ${docgen01_fichier_finalise_path}
    Le fichier doit exister  ${docgen01_fichier_finalise_md_path}
    # Composition du dictionnaire représentant les métadonnées qui doivent
    # apparaître dans le fichier de métadonnées.
    ${size}=  Get File Size  ${docgen01_fichier_finalise_path}
    ${md} =  Create Dictionary
    ...  filename=document_genere_${docgen01_id}.pdf
    ...  mimetype=application/pdf
    ...  size=${size}
    ...  titre=Établissement ${etab01_code} - Dossier ${dc01_di_si} - procès verbal
    ...  description=document généré finalisé
    ...  application=openARIA
    ...  origine=généré
    ...  etablissement_code=${etab01_code}
    ...  etablissement_libelle=${etab01.libelle}
    ...  etablissement_siret=${etab01.siret}
    ...  etablissement_referentiel=true
    ...  etablissement_exploitant=${etab01.exp_prenom} ${etab01.exp_nom}
    ...  etablissement_adresse_numero=${etab01.adresse_numero}
    ...  etablissement_adresse_mention=
    ...  etablissement_adresse_voie=${etab01.adresse_voie}
    ...  etablissement_adresse_cp=${etab01.adresse_cp}
    ...  etablissement_adresse_ville=${etab01.adresse_ville}
    ...  etablissement_adresse_arrondissement=${etab01.adresse_arrondissement}
    ...  etablissement_ref_patrimoine=
    ...  dossier_coordination=${dc01_libelle}
    ...  dossier_instruction=${dc01_di_si}
    ...  signataire=${signataire01.prenom} ${signataire01.nom}
    ...  signataire_qualite=${signataire01.signataire_qualite}
    ...  date_signature=
    ...  pv_erp_numero=SI-${dc01_di_si_pv01_numero}
    ...  pv_erp_nature_analyse=Visite de réception sécurité
    ...  pv_erp_reference_urbanisme=${dc01.dossier_autorisation_ads}
    ...  pv_erp_avis_rendu=
    ...  code_reunion=${reunion01_code}
    ...  date_reunion=${reunion01.date_reunion_yyyy_mm_dd}
    ...  type_reunion=${reutype01.libelle}
    ...  commission=false
    Les métadonnées (clé/valeur) doivent être présentes dans le fichier  ${md}  ${docgen01_fichier_finalise_md_path}
    Les métadonnées (clé) ne doivent pas être présentes dans le fichier  ${md_no}  ${docgen01_fichier_finalise_md_path}


    ##
    ##
    ##
    # On vérifie que l'analyse est actée
    Accéder à l'analyse du dossier d'instruction  ${dc01_di_si}
    Vérifier état acté

    ##
    ##
    ##
    # On vérifie que la proposition d'avis de la demande de réunion a été mise à jour
    Click Element  css=#dossier_instruction_reunion_contexte_di
    Click On Link  ${reunion01.date_reunion}
    Element Text Should Be  proposition_avis  DIFFERE
    Element Text Should Be  proposition_avis_complement  Nécessite une nouvelle visite


Regénération du PV

    [Documentation]  ...

    # En tant que profil CADRE SI
    Depuis la page d'accueil  cadre-si  cadre-si

    ## POSITIONNEMENT DE L'ANALYSE DANS UN ETAT PERMETTANT LA REGENERATION DU PV
    # On accède à l'analyse SI du DI
    # On ré-ouvre l'analyse pour la revalider, cela permet de pouvoir regénérer
    Accéder à l'analyse du dossier d'instruction    ${dc01_di_si}
    Réouvrir l'analyse
    Terminer l'analyse
    Valider l'analyse


    ## REGENERATION DU PV
    Depuis l'onglet PV du DI  ${dc01_di_si}
    L'action 'Regénérer le dernier PV' doit être disponible
    # On clique sur le bouton 'Regénérer le dernier PV'
    Click Element  css=#regenerer_pv
    # On vérifie que le formulaire de regénération est correctement ouvert
    # en vérifiant qu'un des champs du formulaire est présent à l'écran
    WUX  Element Should Contain  css=#sousform-proces_verbal  uméro du PV à regénérer
    # On vérifie que la page ne contient pas d'erreurs
    La page ne doit pas contenir d'erreur
    # On saisi une date de rédaction
    Input Datepicker    date_redaction    01/01/2015
    # On valide le formulaire
    Click On Submit Button In Subform
    #
    Valid Message Should Contain In Subform    Vos modifications ont bien été enregistrées.

    ## VERIFICATIONS SPECIFIQUES A LA REGENERATION
    # On revient au tableau des PV
    Click On Back Button In Subform
    # On vérifie que le numéro est toujours le même
    Page Should Contain    ${dc01_di_si_pv01_numero}
    # On accède au PV regénéré
    Click On Link    ${dc01_di_si_pv01_numero}

    ## VERIFICATION DU CONTENU DU FICHIER PDF
    # On ouvre le PDF en cliquant sur le lien 'Télécharger' du champ fichier
    Click Element  css=#om_fichier_finalise span.reqmo-16 a
    WUX  Open PDF  ${OM_PDF_TITLE}
    # On vérifie les particularités du PDF
    WUX  Page Should Contain  ${dc01_di_si_pv01_numero}
    Page Should Not Contain    DOCUMENT DE TRAVAIL
    PDF Pages Number Should Be    3
    # On ferme le PDF et on revient à la fenêtre principale
    Close PDF

    ## VERIFICATION DU CHANGEMENT D'ETAT DE L'ANALYSE
    # On vérifie que l'analyse est actée
    Accéder à l'analyse du dossier d'instruction  ${dc01_di_si}
    Vérifier état acté


Ajout d'un PV signé sur un PV généré

    [Documentation]  ...

    # En tant que profil SECRETAIRE SI
    Depuis la page d'accueil  secretaire-si  secretaire-si

    #
    Depuis l'onglet PV du DI  ${dc01_di_si}
    # On vérifie qu'il y a le PV
    WUX  Page Should Contain    ${dc01_di_si_pv01_numero}
    # On accède au PV
    Click On Link    ${dc01_di_si_pv01_numero}
    WUX  Page Should Contain    ${dc01_di_si_pv01_numero}
    # On vérifie qu'il n'y a pas l'action modifier
    Portlet Action Should Not Be In SubForm    proces_verbal    modifier
    # On ajoute le PV signé
    Click On SubForm Portlet Action    proces_verbal    add_signed_file
    Add File    om_fichier_signe_courrier    pv_signe.pdf
    # On valide le formulaire
    Click On Submit Button In Subform
    # On revient à sa consulation
    Click On Back Button In Subform
    # On vérifie que le PV signé a été ajouté
    ${dc01_di_si_pv01_id} =  Get Text  css=#sousform-proces_verbal #proces_verbal
    Page Should Contain  document_genere_${docgen01_id}_signe.pdf

    ## VERIFICATION DU CONTENU DU FICHIER PDF
    # On ouvre le PDF en cliquant sur le lien 'Télécharger' du champ fichier
    Click Element  css=#om_fichier_signe span.reqmo-16 a
    WUX  Open PDF  ${OM_PDF_TITLE}
    # On vérifie les particularités du PDF
    WUX  Page Should Contain  Demande de passage en réunion rattachée au PV
    PDF Pages Number Should Be    3
    # On ferme le PDF et on revient à la fenêtre principale
    Close PDF

    ##
    ## Gestion des métadonnées
    ##
    ## Vérifie les métadonnées pour le champ fichier :
    ## - [courrier.om_fichier_signe_courrier] / Cas PV.
    ##

    ## => [courrier.om_fichier_signe_courrier] (METADATA FILESTORAGE)
    ${docgen01_fichier_signe_uid} =  Récupérer l'uid du fichier signé du document généré  ${docgen01_code_barres}
    ${docgen01_fichier_signe_path} =  Récupérer le chemin vers le fichier correspondant à l'uid  ${docgen01_fichier_signe_uid}
    ${docgen01_fichier_signe_md_path} =  Récupérer le chemin vers le fichier de métadonnées correspondant à l'uid  ${docgen01_fichier_signe_uid}
    Le fichier doit exister  ${docgen01_fichier_signe_path}
    Le fichier doit exister  ${docgen01_fichier_signe_md_path}
    # Composition du dictionnaire représentant les métadonnées qui doivent
    # apparaître dans le fichier de métadonnées.
    ${size}=  Get File Size  ${docgen01_fichier_signe_path}
    ${md} =  Create Dictionary
    ...  filename=document_genere_${docgen01_id}_signe.pdf
    ...  mimetype=application/pdf
    ...  size=${size}
    ...  titre=Établissement ${etab01_code} - Dossier ${dc01_di_si} - procès verbal (signé)
    ...  description=document généré numérisé signé
    ...  application=openARIA
    ...  origine=téléversé
    ...  etablissement_code=${etab01_code}
    ...  etablissement_libelle=${etab01.libelle}
    ...  etablissement_siret=${etab01.siret}
    ...  etablissement_referentiel=true
    ...  etablissement_exploitant=${etab01.exp_prenom} ${etab01.exp_nom}
    ...  etablissement_adresse_numero=${etab01.adresse_numero}
    ...  etablissement_adresse_mention=
    ...  etablissement_adresse_voie=${etab01.adresse_voie}
    ...  etablissement_adresse_cp=${etab01.adresse_cp}
    ...  etablissement_adresse_ville=${etab01.adresse_ville}
    ...  etablissement_adresse_arrondissement=${etab01.adresse_arrondissement}
    ...  etablissement_ref_patrimoine=
    ...  dossier_coordination=${dc01_libelle}
    ...  dossier_instruction=${dc01_di_si}
    ...  signataire=${signataire01.prenom} ${signataire01.nom}
    ...  signataire_qualite=${signataire01.signataire_qualite}
    ...  date_signature=
    ...  pv_erp_numero=SI-${dc01_di_si_pv01_numero}
    ...  pv_erp_nature_analyse=Visite de réception sécurité
    ...  pv_erp_reference_urbanisme=${dc01.dossier_autorisation_ads}
    ...  pv_erp_avis_rendu=
    ...  code_reunion=${reunion01_code}
    ...  date_reunion=${reunion01.date_reunion_yyyy_mm_dd}
    ...  type_reunion=${reutype01.libelle}
    ...  commission=false
    Les métadonnées (clé/valeur) doivent être présentes dans le fichier  ${md}  ${docgen01_fichier_signe_md_path}
    Les métadonnées (clé) ne doivent pas être présentes dans le fichier  ${md_no}  ${docgen01_fichier_signe_md_path}


Ajout d'un PV tiers

    [Documentation]  ...

    # En tant que profil SECRETAIRE SI
    Depuis la page d'accueil  secretaire-si  secretaire-si

    #
    Depuis l'onglet PV du DI  ${dc01_di_si}
    L'action 'Ajouter un PV' doit être disponible
    # On clique sur le bouton 'Ajouter un PV'
    Click Element    css=#ajouter_pv
    # On vérifie que le formulaire d'ajout est correctement ouvert
    # en vérifiant qu'un des champs du formulaire est présent à l'écran
    WUX  Element Should Contain  css=#sousform-proces_verbal  PV signé
    # On vérifie que la page ne contient pas d'erreurs
    La page ne doit pas contenir d'erreur
    # On valide le formulaire sans sélectionner de demande de passage en réunion pour vérifier que la
    # saisie est refusée car le champ est obligatoire
    Click On Submit Button In Subform
    Error Message Should Contain  Le champ demande de passage en réunion est obligatoire
    # On sélectionne un passage en réunion
    Select From List By Label  css=#dossier_instruction_reunion  ${dpr01.date_souhaitee} - ${reutype01.libelle}
    # On saisi une date de rédaction
    Input Datepicker  date_redaction  02/01/2015
    # On valide le formulaire sans poster de fichier pour vérifier que la
    # saisie est refusée car le champ fichier est obligatoire
    Click On Submit Button In Subform
    Error Message Should Contain  Le champ PV signé est obligatoire
    # On poste un fichier
    Add File  om_fichier_signe  pv_ajoute.pdf
    # On valide le formulaire
    Click On Submit Button In Subform
    Valid Message Should Contain In Subform  Vos modifications ont bien été enregistrées.
    # On retourne à la liste des PV
    Click On Back Button In Subform
    # On clique sur le numéro de pv regénéré
    Click On Link  ajouté
    ${dc01_di_si_pv02_id} =  Get Text  css=#sousform-proces_verbal #proces_verbal
    Page Should Contain  proces_verbal_${dc01_di_si_pv02_id}_signe.pdf

    ##
    ## stockage
    ##

    # On ouvre le PDF en cliquant sur le lien visualiser du champ fichier du
    # formulaire puis en cliquant sur le lien de l'overlay
    Click Element  css=#om_fichier_signe span.reqmo-16 a
    WUX  Open PDF  ${OM_PDF_TITLE}
    # On vérifie les particularités du PDF
    WUX  Page Should Contain  20150101/2/0000001
    PDF Pages Number Should Be    3
    # On ferme le PDF et on revient à la fenêtre principale
    Close PDF


    # Récupération de l'uid du fichier ajouté
    Click On SubForm Portlet Action    proces_verbal    modifier
    WUX  Element Should Be Visible  css=input#om_fichier_signe_upload
    ${pv_fichier_signe_uid} =  Get Value  css=input#om_fichier_signe

    ##
    ## Gestion des métadonnées
    ##
    ## Vérifie les métadonnées pour le champ fichier :
    ##
    ## - Procès verbal numérisé ajouté [proces_verbal.om_fichier_signe]
    ##   > Téléversé
    ##   > Stockage à l'ajout du fichier
    ##   > Mise à jour à chaque mise à jour du champ fichier
    ##

    ## => [proces_verbal.om_fichier_signe] (METADATA FILESTORAGE)
    ${pv_fichier_signe_path} =  Récupérer le chemin vers le fichier correspondant à l'uid  ${pv_fichier_signe_uid}
    ${pv_fichier_signe_md_path} =  Récupérer le chemin vers le fichier de métadonnées correspondant à l'uid  ${pv_fichier_signe_uid}
    Le fichier doit exister  ${pv_fichier_signe_path}
    Le fichier doit exister  ${pv_fichier_signe_md_path}
    # Composition du dictionnaire représentant les métadonnées qui doivent
    # apparaître dans le fichier de métadonnées.
    ${size}=  Get File Size  ${pv_fichier_signe_path}
    ${md} =  Create Dictionary
    ...  filename=proces_verbal_${dc01_di_si_pv02_id}_signe.pdf
    ...  mimetype=application/pdf
    ...  size=${size}
    ...  titre=Établissement ${etab01_code} - Dossier ${dc01_di_si} - procès verbal
    ...  description=procès verbal numérisé ajouté
    ...  application=openARIA
    ...  origine=téléversé
    ...  etablissement_code=${etab01_code}
    ...  etablissement_libelle=${etab01.libelle}
    ...  etablissement_siret=${etab01.siret}
    ...  etablissement_referentiel=true
    ...  etablissement_exploitant=${etab01.exp_prenom} ${etab01.exp_nom}
    ...  etablissement_adresse_numero=${etab01.adresse_numero}
    ...  etablissement_adresse_mention=
    ...  etablissement_adresse_voie=${etab01.adresse_voie}
    ...  etablissement_adresse_cp=${etab01.adresse_cp}
    ...  etablissement_adresse_ville=${etab01.adresse_ville}
    ...  etablissement_adresse_arrondissement=${etab01.adresse_arrondissement}
    ...  etablissement_ref_patrimoine=
    ...  dossier_coordination=${dc01_libelle}
    ...  dossier_instruction=${dc01_di_si}
    ...  signataire=
    ...  signataire_qualite=
    ...  date_signature=
    ...  pv_erp_numero=
    ...  pv_erp_nature_analyse=Visite de réception sécurité
    ...  pv_erp_reference_urbanisme=${dc01.dossier_autorisation_ads}
    ...  pv_erp_avis_rendu=
    ...  code_reunion=${dpr01.reunion_code}
    ...  date_reunion=${dpr01.reunion_date_format_yyyy_mm_dd}
    ...  type_reunion=${dpr01.reunion_type}
    ...  commission=false
    Les métadonnées (clé/valeur) doivent être présentes dans le fichier  ${md}  ${pv_fichier_signe_md_path}
    Les métadonnées (clé) ne doivent pas être présentes dans le fichier  ${md_no}  ${pv_fichier_signe_md_path}


Mise à jour des données de l'établissement

    [Documentation]  Le fait d'ajouter un PV signé (soit directement dans le
    ...  cas d'un PV externe, soit après une génération) met à jour les données
    ...  techniques sécurité incendie et la classification de l'établissement.
    ...  L'objet de ce 'Test Case' est de tester l'écrasement des données.

    # En tant que profil CADRE SI
    Depuis la page d'accueil  cadre-si  cadre-si

    ## CONSTITUTION D'UN JEU DE DONNEES SPECIFIQUE

    ##
    ## ETABLISSEMENT 02
    ##
    # On crée un établissement ouvert de type R et de catégorie 1.
    # On ne renseigne aucune donnée technique, ce sera au PV de récupérer
    # celles de l'analyse.
    &{etab02} =  Create Dictionary
    ...  libelle=UNIVERSITE DU SUD
    ...  etablissement_nature=ERP Référentiel
    ...  etablissement_type=R
    ...  etablissement_categorie=1
    ...  si_autorite_competente_visite=Commission communale de sécurité
    ...  adresse_numero=29
    ...  adresse_voie=RUE DE ROME
    ...  adresse_cp=13006
    ...  adresse_ville=MARSEILLE
    ...  adresse_arrondissement=6ème
    ...  exp_civilite=M.
    ...  exp_nom=Pinto
    ...  exp_prenom=Jérôme
    ...  etablissement_etat=Ouvert
    ${etab02_code} =  Ajouter l'établissement  ${etab02}
    ${etab02_titre} =  Set Variable  ${etab02_code} - ${etab02.libelle}
    Set Suite Variable  ${etab02}
    Set Suite Variable  ${etab02_code}
    Set Suite Variable  ${etab02_titre}

    ##
    ## DOSSIER DE COORDINATION 02
    ##
    # On crée un dossier de coordination de type visite de réception, on génère
    # un DI SI et un DI ACC et on lie le DC à cet établissement
    &{dc02} =  Create Dictionary
    ...  dossier_coordination_type=Visite de réception
    ...  description=Agrandissement
    ...  etablissement=${etab02_titre}
    ...  date_demande=12/12/2014
    ...  a_qualifier=false
    ...  dossier_instruction_secu=true
    ...  dossier_instruction_acc=true
    ...  dossier_autorisation_ads=AT013055160002
    ${dc02_libelle} =  Ajouter le dossier de coordination  ${dc02}
    Set Suite Variable  ${dc02}
    Set Suite Variable  ${dc02_libelle}
    Set Suite Variable  ${dc02_di_si}  ${dc02_libelle}-SI
    Set Suite Variable  ${dc02_di_acc}  ${dc02_libelle}-ACC

    ##
    ## DEMANDE DE PASSAGE EN REUNION 02
    ##
    &{dpr02} =  Create Dictionary
    ...  date_souhaitee=10/10/2015
    Ajouter la demande de passage en réunion depuis le contexte du dossier d'instruction
    ...  ${dc02_libelle}  SI
    ...  ${dpr02.date_souhaitee}
    ...  ${reutype01.libelle}
    ...  ${reucategorie01.libelle}

    ##
    ##
    ##
    # On édite l'analyse sécurité incendie
    Accéder à l'analyse du dossier d'instruction    ${dc02_di_si}
    # 1/2 - classification de l'établissement
    # On accède au formulaire de modification de la classification établissement
    Éditer bloc analyse  classification_etablissement
    # On modifie les valeurs qui doivent être plus tard remontées à l'établissement
    Select From List By Label  css=#etablissement_type  [GA] Gares Accessibles au public (chemins de fer, téléphériques, remonte-pentes...)
    Select From List By Label  css=#etablissement_categorie  [3] de 301 à 700 personnes
    # On valide le formulaire
    Submit Form    css=#form_analyses_modifier
    WUX  Valid Message Should Be  Vos modifications ont bien été enregistrées.
    # On revient sur la vue de l'analyse
    Click Element  css=#sousform-analyses a.retour
    WUX  Element Should Be Visible  css=#analyse_etat
    La page ne doit pas contenir d'erreur
    # On vérifie que les valeurs saisies sont bien prises en compte
    Page Should Contain  Type : [GA] Gares Accessibles au public (chemins de fer, téléphériques, remonte-pentes...)
    Page Should Contain  Catégorie : [3] de 301 à 700 personnes
    # 2/2 - données techniques
    Modifier données techniques depuis analyse SI    ${dc02_di_si}    1

    ## AJOUT D'UN PV EXTERNE
    &{pv} =  Create Dictionary
    ...  dossier_instruction_reunion=${dpr02.date_souhaitee} - ${reutype01.libelle}
    ...  date_redaction=02/01/2015
    ...  om_fichier_signe=pv_ajoute.pdf
    Ajouter un PV sur le DI  ${dc02_di_si}  ${pv}

    ## VERIFICATION DE LA MISE A JOUR DES DONNEES
    # On accède à la fiche de visualisation de l'établissement
    Depuis le contexte de l'établissement  ${etab02_code}  ${etab02.libelle}
    # On vérifie l'écrasement des données de l'établissement
    Element Text Should Be  css=#etablissement_type  GA
    Element Text Should Be  css=#etablissement_categorie  3
    Open Fieldset  etablissement_tous  details
    Element Should Contain  css=#si_type_alarme  1


