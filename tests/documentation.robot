*** Settings ***
Resource  resources/resources.robot
Suite Setup  For Suite Setup
Suite Teardown  For Suite Teardown
Documentation  TestSuite "Documentation" : cette suite permet d'extraire
...  automatiquement les captures à destination de la documentation.


*** Keywords ***
Highlight heading
    [Arguments]  ${locator}
    Update element style  ${locator}  margin-top  0.75em
    Highlight  ${locator}


StrPadLeft0
    [Arguments]  ${element}
    ${element_catenate} =    Catenate    SEPARATOR=    0    ${element}
    ${element} =    Set Variable If
    ...  ${element} < 10  ${element_catenate}
    ...  ${element} >= 10  ${element}
    [Return]  ${element}


*** Test Cases ***
Constitution d'un jeu de données

    [Documentation]  L'objet de ce 'Test Case' est de constituer un jeu de de
    ...    données cohérent pour les scénarios fonctionnels qui suivent.

    [Tags]  doc

    #
    Depuis la page d'accueil  admin  admin

    # On importe les établissements
    Depuis l'import    import_etablissement
    Add File    fic1    import_etablissements_doc.csv
    Click On Submit Button In Import CSV
    WUX  Valid Message Should Contain    50 ligne(s) importée(s)

    # On calcule la périodicité
    Go To  ${PROJECT_URL}${OM_ROUTE_FORM}&obj=periodicite_visites&action=99&idx=0
    Click Element  css=#action-controlpanel-periodicite_visites-update_all_dossier_coordination_periodique
    WUX  Click Element  css=.ui-dialog .ui-dialog-buttonset button
    WUX  Valid Message Should Contain  Nombre d'établissement total : 50

    # Active l'option de récupération automatique du référentiel patrimoine
    Modifier le paramètre    option_referentiel_patrimoine    true

    # Ajoute un lien patrimoine
    Depuis le formulaire de modification de l'établissement    T2803    ASSOCIATION RABELAIS LYCEE COLLEGE ECOLE
    Select From List By Label    css=#etablissement_statut_juridique    Ville
    Input Text    css=input.refquart    812
    Input Text    css=input.refsect    A
    Input Text    css=input.refparc    0077
    Click Button    css=#lien_referentiel_patrimoine
    WUX  Page Should Contain    Liste des références patrimoine trouvées :
    Select Checkbox    css=input[value='91']
    Click Button    css=input[value='Valider']
    Sleep    2
    Element Text Should Be    css=#ref_patrimoine    91
    Click On Submit Button
    Valid Message Should Be    Vos modifications ont bien été enregistrées.
    La page ne doit pas contenir d'erreur

    # Contacts pour la documentation des établissements
    Saisir l'exploitant depuis le formulaire de l'établissement    T2803    ASSOCIATION RABELAIS LYCEE COLLEGE ECOLE    M.    Durand    Jacques    true
    Ajouter le contact depuis l'établissement    T2803    ASSOCIATION RABELAIS LYCEE COLLEGE ECOLE    Mandataire    null    null    M.    Dupont    Yves
    Ajouter le contact depuis l'établissement    T2803    ASSOCIATION RABELAIS LYCEE COLLEGE ECOLE    (*) Demandeur Principal    null    null    M.    Bernard    Charles

    # Création d'un établissement qui sera considéré comme déjà géolocalisé
    &{etab01} =  Create Dictionary
    ...  libelle=CC La Pinède
    ...  etablissement_nature=ERP Référentiel
    ...  etablissement_type=M
    ...  etablissement_categorie=1
    ...  si_autorite_competente_visite=Commission communale de sécurité
    ...  adresse_numero=9797
    ...  adresse_voie=RUE DE ROME
    ...  adresse_cp=13006
    ...  adresse_ville=MARSEILLE
    ...  adresse_arrondissement=6ème
    ...  siret=73282932000074
    ...  exp_civilite=Mme
    ...  exp_nom=Leclerc
    ...  exp_prenom=Tatiana
    ${etab01_code} =  Ajouter l'établissement  ${etab01}
    ${etab01_titre} =  Set Variable  ${etab01_code} - ${etab01.libelle}
    Set Suite Variable  ${etab01}
    Set Suite Variable  ${etab01_code}
    Set Suite Variable  ${etab01_titre}

    # Création d'un établissement qui sera considéré comme géolocalisable
    @{ref_cad} =  Create List  806  AB  0025
    &{etab02} =  Create Dictionary
    ...  libelle=MONDIAL EVASION
    ...  etablissement_nature=ERP Référentiel
    ...  etablissement_type=R
    ...  etablissement_categorie=1
    ...  si_autorite_competente_visite=Commission communale de sécurité
    ...  adresse_numero=150
    ...  adresse_voie=RUE DE ROME
    ...  adresse_cp=13006
    ...  adresse_ville=MARSEILLE
    ...  adresse_arrondissement=6ème
    ...  siret=73282932000074
    ...  exp_civilite=M.
    ...  exp_nom=Norman
    ...  exp_prenom=Paul
    ...  etablissement_etat=Ouvert
    ...  terrain_references_cadastrales=${ref_cad}

    ${etab02_code} =  Ajouter l'établissement  ${etab02}
    ${etab02_titre} =  Set Variable  ${etab02_code} - ${etab02.libelle}
    Set Suite Variable  ${etab02}
    Set Suite Variable  ${etab02_code}
    Set Suite Variable  ${etab02_titre}

    # Création d'un établissement qui sera considéré comme non géolocalisable
    &{etab03} =  Create Dictionary
    ...  libelle=MONDIAL NATURE
    ...  etablissement_nature=ERP Référentiel
    ...  si_autorite_competente_visite=Commission communale de sécurité
    ...  adresse_numero=350
    ...  adresse_voie=Rue de Rome
    ...  etablissement_type=R
    ...  etablissement_categorie=1

    ${etab03_code} =  Ajouter l'établissement  ${etab03}
    ${etab03_titre} =  Set Variable  ${etab03_code} - ${etab03.libelle}
    Set Suite Variable  ${etab03}
    Set Suite Variable  ${etab03_code}
    Set Suite Variable  ${etab03_titre}

    # Création d'un établissement dont les références cadastrales seront
    # modifiées afin de prendre le CE de l'action de récupération des
    # propriétaires par parcelles
    @{etab04_ref_cad} =  Create List  806  AB  0026
    &{etab04} =  Create Dictionary
    ...  libelle=MONDIAL TOUR
    ...  etablissement_nature=ERP Référentiel
    ...  etablissement_type=R
    ...  etablissement_categorie=1
    ...  si_autorite_competente_visite=Commission communale de sécurité
    ...  adresse_numero=25
    ...  adresse_voie=RUE DE ROME
    ...  adresse_cp=13006
    ...  adresse_ville=MARSEILLE
    ...  adresse_arrondissement=6ème
    ...  siret=73282932000074
    ...  exp_civilite=M.
    ...  exp_nom=Lacroix
    ...  exp_prenom=Jean
    ...  etablissement_etat=Ouvert
    ...  terrain_references_cadastrales=${etab04_ref_cad}
    ${etab04_code} =  Ajouter l'établissement  ${etab04}
    ${etab04_titre} =  Set Variable  ${etab04_code} - ${etab04.libelle}
    Set Suite Variable  ${etab04}
    Set Suite Variable  ${etab04_code}
    Set Suite Variable  ${etab04_titre}

    # On cré une UA validée sur l'établissement
    ${ua_validee1_libelle} =    Set Variable    Bâtiment A1
    ${ua_validee1_description} =  Set Variable  Description
    Ajouter l'UA depuis le contexte d'un établissement    ${etab01_code}    ${etab01.libelle}    ${ua_validee1_libelle}  aucun  aucun  aucun  aucun    null    aucun    aucun    aucun    aucun    null    null    aucun    null    null    ${ua_validee1_description}
    # On cré une UA validée sur l'établissement
    ${ua_validee2_libelle} =    Set Variable    Bâtiment A2
    ${ua_validee2_description} =  Set Variable  Description
    Ajouter l'UA depuis le contexte d'un établissement    ${etab01_code}    ${etab01.libelle}    ${ua_validee2_libelle}  aucun  aucun  aucun  aucun    null    aucun    aucun    aucun    aucun    null    null    aucun    null    null    ${ua_validee2_description}
    # On cré une UA archivée sur l'établissement
    ${ua_archivee_libelle} =    Set Variable    Bâtiment A3
    ${ua_archivee_description} =  Set Variable  Description
    Ajouter l'UA depuis le contexte d'un établissement    ${etab01_code}    ${etab01.libelle}    ${ua_archivee_libelle}  aucun  aucun  aucun  aucun    null    aucun    aucun    aucun    aucun    null    null    aucun    null    null    ${ua_archivee_description}
    Archiver l'UA validée    ${etab01_code}    ${etab01.libelle}    ${ua_archivee_libelle}
    #
    Set Suite Variable    ${etab01_code}
    Set Suite Variable    ${etab01.libelle}
    Set Suite Variable    ${ua_validee1_libelle}
    Set Suite Variable    ${ua_validee1_description}
    Set Suite Variable    ${ua_validee2_libelle}
    Set Suite Variable    ${ua_validee2_description}
    Set Suite Variable    ${ua_archivee_libelle}
    Set Suite Variable    ${ua_archivee_description}

    #
    Accéder à l'analyse du dossier d'instruction  VPS-VISIT-000001-SI
    Cliquer action analyse  finaliser
    Accéder à l'analyse du dossier d'instruction  VPS-VISIT-000015-SI
    Cliquer action analyse  finaliser
    Cliquer action analyse  valider
    Accéder à l'analyse du dossier d'instruction  VPS-VISIT-000016-SI
    Cliquer action analyse  finaliser
    Cliquer action analyse  valider

    #
    ${year} =  Get Time  year
    ${year1} =  Evaluate    ${year} + 1
    ${month} =  Get Time  month

    ${weeknum_0} =    Set Variable If
    ...  '${month}' == '01'  1
    ...  '${month}' == '02'  5
    ...  '${month}' == '03'  9
    ...  '${month}' == '04'  13
    ...  '${month}' == '05'  18
    ...  '${month}' == '06'  22
    ...  '${month}' == '07'  26
    ...  '${month}' == '08'  30
    ...  '${month}' == '09'  34
    ...  '${month}' == '10'  38
    ...  '${month}' == '11'  42
    ...  '${month}' == '12'  48

    ${weeknum_1} =    Evaluate    ${weeknum_0} + 1
    ${weeknum_2} =    Evaluate    ${weeknum_1} + 1
    ${weeknum_3} =    Evaluate    ${weeknum_2} + 1
    ${weeknum_4} =    Evaluate    ${weeknum_3} + 1
    ${weeknum_4} =    Set Variable If
    ...  '${month}' == '12'  1  ${weeknum_4}
    ${year} =    Set Variable If
    ...  '${month}' == '12'  ${year1}  ${year}
    ${weeknum_5} =    Evaluate    ${weeknum_4} + 1
    ${weeknum_6} =    Evaluate    ${weeknum_5} + 1
    ${weeknum_7} =    Evaluate    ${weeknum_6} + 1
    ${weeknum_8} =    Evaluate    ${weeknum_7} + 1
    ${weeknum_9} =    Evaluate    ${weeknum_8} + 1

    ${weeknum_0} =    StrPadLeft0    ${weeknum_0}
    ${weeknum_1} =    StrPadLeft0    ${weeknum_1}
    ${weeknum_2} =    StrPadLeft0    ${weeknum_2}
    ${weeknum_3} =    StrPadLeft0    ${weeknum_3}
    ${weeknum_4} =    StrPadLeft0    ${weeknum_4}
    ${weeknum_5} =    StrPadLeft0    ${weeknum_5}
    ${weeknum_6} =    StrPadLeft0    ${weeknum_6}
    ${weeknum_7} =    StrPadLeft0    ${weeknum_7}
    ${weeknum_8} =    StrPadLeft0    ${weeknum_8}
    ${weeknum_9} =    StrPadLeft0    ${weeknum_9}

    ${programmation_0_code} =    Catenate    SEPARATOR=/    ${year}    ${weeknum_0}
    ${programmation_1_code} =    Catenate    SEPARATOR=/    ${year}    ${weeknum_1}
    ${programmation_2_code} =    Catenate    SEPARATOR=/    ${year}    ${weeknum_2}
    ${programmation_3_code} =    Catenate    SEPARATOR=/    ${year}    ${weeknum_3}
    ${programmation_4_code} =    Catenate    SEPARATOR=/    ${year}    ${weeknum_4}
    ${programmation_5_code} =    Catenate    SEPARATOR=/    ${year}    ${weeknum_5}
    ${programmation_6_code} =    Catenate    SEPARATOR=/    ${year}    ${weeknum_6}
    ${programmation_7_code} =    Catenate    SEPARATOR=/    ${year}    ${weeknum_7}
    ${programmation_8_code} =    Catenate    SEPARATOR=/    ${year}    ${weeknum_8}
    ${programmation_9_code} =    Catenate    SEPARATOR=/    ${year}    ${weeknum_9}

    Ajouter la programmation depuis le menu  Sécurité Incendie  ${year}  ${weeknum_0}
    Ajouter la programmation depuis le menu  Sécurité Incendie  ${year}  ${weeknum_1}
    Ajouter la programmation depuis le menu  Sécurité Incendie  ${year}  ${weeknum_2}
    Ajouter la programmation depuis le menu  Sécurité Incendie  ${year}  ${weeknum_3}
    Ajouter la programmation depuis le menu  Sécurité Incendie  ${year}  ${weeknum_4}
    Ajouter la programmation depuis le menu  Sécurité Incendie  ${year}  ${weeknum_5}
    Ajouter la programmation depuis le menu  Sécurité Incendie  ${year}  ${weeknum_6}
    Ajouter la programmation depuis le menu  Sécurité Incendie  ${year}  ${weeknum_7}
    Ajouter la programmation depuis le menu  Sécurité Incendie  ${year}  ${weeknum_8}
    Ajouter la programmation depuis le menu  Sécurité Incendie  ${year}  ${weeknum_9}

    Set Suite Variable    ${programmation_0_code}
    Set Suite Variable    ${programmation_1_code}
    Set Suite Variable    ${programmation_2_code}
    Set Suite Variable    ${programmation_3_code}
    Set Suite Variable    ${programmation_4_code}
    Set Suite Variable    ${programmation_5_code}
    Set Suite Variable    ${programmation_6_code}
    Set Suite Variable    ${programmation_7_code}
    Set Suite Variable    ${programmation_8_code}
    Set Suite Variable    ${programmation_9_code}

    Finaliser puis valider la programmation  ${programmation_0_code}
    Finaliser la programmation  ${programmation_1_code}
    Finaliser puis valider la programmation  ${programmation_2_code}
    Finaliser la programmation  ${programmation_3_code}
    Finaliser puis valider la programmation  ${programmation_4_code}
    Finaliser la programmation  ${programmation_5_code}
    Finaliser puis valider la programmation  ${programmation_6_code}
    Finaliser la programmation  ${programmation_7_code}
    Finaliser puis valider la programmation  ${programmation_8_code}

    ##
    ## REUNION 01
    ##
    &{reunion01} =  Create Dictionary
    ...  reunion_type=Commission Communale d'Accessibilité
    ...  date_reunion=03/12/2014
    Ajouter la réunion  ${reunion01}
    ${reunion01_code} =  Set Variable  CCA-2014-12-03
    Set Suite Variable  ${reunion01_code}
    ##
    ## REUNION 02
    ##
    &{reunion02} =  Create Dictionary
    ...  reunion_type=Groupe Technique d'Etude de Plans
    ...  date_reunion=01/12/2014
    ...  listes_de_diffusion=nospam@openmairie.org
    Ajouter la réunion  ${reunion02}
    ${reunion02_code} =  Set Variable  GTEP-2014-12-01
    Set Suite Variable  ${reunion02_code}
    #
    Planifier directement le DC ou DI pour la réunion dans la catégorie    VPS-VISIT-000014    ${reunion02_code}    Plans
    Planifier directement le DC ou DI pour la réunion dans la catégorie    VPS-VISIT-000015    ${reunion02_code}    Plans
    Planifier directement le DC ou DI pour la réunion dans la catégorie    VPS-VISIT-000016    ${reunion02_code}    Plans
    Numéroter l'ordre du jour de la réunion    ${reunion02_code}
    Depuis la demande de passage (retour d'avis) dans le contexte de la réunion    ${reunion02_code}    1
    Depuis la demande de passage (retour d'avis) dans le contexte de la réunion    ${reunion02_code}    2
    Depuis la demande de passage (retour d'avis) dans le contexte de la réunion    ${reunion02_code}    3
    Rendre l'avis sur tous les dossiers de la réunion    ${reunion02_code}
    ##
    ## REUNION 03
    ##
    &{reunion03} =  Create Dictionary
    ...  reunion_type=Commisson Communale de Sécurité
    ...  date_reunion=28/11/2014
    Ajouter la réunion  ${reunion03}
    ${reunion03_code} =  Set Variable  CCS-2014-11-28
    Set Suite Variable  ${reunion03_code}
    #
    Ajouter la demande de passage en réunion depuis le contexte du dossier d'instruction    VPS-VISIT-000001    SI    02/12/2014    Commisson Communale de Sécurité     Visites
    Ajouter la demande de passage en réunion depuis le contexte du dossier d'instruction    VPS-VISIT-000002    SI    30/11/2014    Commisson Communale de Sécurité     Visites
    Ajouter la demande de passage en réunion depuis le contexte du dossier d'instruction    VPS-VISIT-000003    SI    12/12/2014    Commisson Communale de Sécurité     Visites
    Ajouter la demande de passage en réunion depuis le contexte du dossier d'instruction    VPS-VISIT-000004    SI    15/01/2015    Commisson Communale de Sécurité     Visites
    Ajouter la demande de passage en réunion depuis le contexte du dossier d'instruction    VPS-VISIT-000005    SI    30/04/2014    Commisson Communale de Sécurité     Visites
    Ajouter la demande de passage en réunion depuis le contexte du dossier d'instruction    VPS-VISIT-000006    SI    08/11/2014    Commisson Communale de Sécurité     Visites
    Ajouter la demande de passage en réunion depuis le contexte du dossier d'instruction    VPS-VISIT-000007    SI    06/10/2014    Commisson Communale de Sécurité     Visites
    Ajouter la demande de passage en réunion depuis le contexte du dossier d'instruction    VPS-VISIT-000008    SI    25/12/2014    Commisson Communale de Sécurité     Visites
    Ajouter la demande de passage en réunion depuis le contexte du dossier d'instruction    VPS-VISIT-000009    SI    14/11/2014    Commisson Communale de Sécurité     Visites
    Ajouter la demande de passage en réunion depuis le contexte du dossier d'instruction    VPS-VISIT-000010    SI    29/11/2014    Commisson Communale de Sécurité     Visites
    Planifier directement le DC ou DI pour la réunion dans la catégorie    VPS-VISIT-000011    ${reunion03_code}    Visites
    Planifier directement le DC ou DI pour la réunion dans la catégorie    VPS-VISIT-000012    ${reunion03_code}    Visites
    Planifier directement le DC ou DI pour la réunion dans la catégorie    VPS-VISIT-000013    ${reunion03_code}    Visites

    # Les documents entrants
    &{document_entrant_1} =  Create Dictionary
    ...  nom=Document entrant 1
    ...  piece_type=Acte
    ...  uid=lettre_test.pdf
    ...  date_reception=01/01/2015
    ...  service=Sécurité Incendie
    ...  suivi=true
    ...  date_butoir=15/04/2016
    &{document_entrant_2} =  Create Dictionary
    ...  nom=Document entrant 2
    ...  piece_type=Vérification technique
    ...  uid=lettre_test.pdf
    ...  date_reception=10/01/2015
    ...  service=Sécurité Incendie
    &{document_entrant_3} =  Create Dictionary
    ...  nom=Document entrant 3
    ...  piece_type=Signalement
    ...  uid=lettre_test.pdf
    ...  date_reception=13/01/2015
    ...  service=Sécurité Incendie
    ...  suivi=true
    &{document_entrant_4} =  Create Dictionary
    ...  nom=Document entrant 4
    ...  piece_type=Acte
    ...  uid=lettre_test.pdf
    ...  date_reception=22/01/2015
    ...  service=Sécurité Incendie
    &{document_entrant_5} =  Create Dictionary
    ...  nom=Document entrant 5
    ...  piece_type=Acte
    ...  uid=lettre_test.pdf
    ...  date_reception=23/02/2015
    ...  service=Sécurité Incendie
    ...  choix_lien=établissement
    ...  etablissement=T3074 - COLLEGE AMPERE
    ...  suivi=true
    ...  date_butoir=30/03/2015
    &{document_entrant_6} =  Create Dictionary
    ...  nom=Document entrant 6
    ...  piece_type=Signalement
    ...  uid=lettre_test.pdf
    ...  date_reception=05/03/2015
    ...  service=Sécurité Incendie
    ...  choix_lien=établissement
    ...  etablissement=T3074 - COLLEGE AMPERE
    ...  suivi=true
    &{document_entrant_7} =  Create Dictionary
    ...  nom=Document entrant 7
    ...  piece_type=Vérification technique
    ...  uid=lettre_test.pdf
    ...  date_reception=01/03/2015
    ...  service=Sécurité Incendie
    ...  choix_lien=dossier d'instruction
    ...  dossier_instruction=VPS-VISIT-000003-SI
    ...  suivi=true
    ...  date_butoir=20/04/2016
    &{document_entrant_8} =  Create Dictionary
    ...  nom=Document entrant 8
    ...  piece_type=Signalement
    ...  uid=lettre_test.pdf
    ...  date_reception=01/03/2015
    ...  service=Sécurité Incendie
    ...  choix_lien=dossier de coordination
    ...  dossier_coordination=VPS-VISIT-000003
    ...  suivi=true
    Ajouter un document entrant depuis la bannette  ${document_entrant_1}
    Ajouter un document entrant depuis la bannette  ${document_entrant_2}
    Ajouter un document entrant depuis la bannette  ${document_entrant_3}
    Ajouter un document entrant depuis la bannette  ${document_entrant_4}
    Ajouter un document entrant depuis la bannette  ${document_entrant_5}
    Ajouter un document entrant depuis la bannette  ${document_entrant_6}
    Ajouter un document entrant depuis la bannette  ${document_entrant_7}
    Ajouter un document entrant depuis la bannette  ${document_entrant_8}

    # Les documents générés
    Saisir l'exploitant depuis le formulaire de l'établissement    T1302    LYCEE ALEXANDRE DUMAS    M.    Martin    Paul    true
    Ajouter le contact institutionnel depuis le menu    null    Sécurité Incendie    null    M.    Doe    John    null    null    null    null    null    null    null    null    null    null    null    null    null    null    null    true    true
    @{contacts_lies}    Create List    (Exploitant) M. Martin Paul    (Institutionnel) M. Doe John
    ${params}    Create Dictionary    contacts_lies=@{contacts_lies}
    Ajouter le document généré depuis le contexte du dossier d'instruction    VPS-VISIT-000019-SI    ${params}    Courrier simple    Courrier simple
    @{contacts_lies}    Create List    (Exploitant) M. Martin Paul
    ${params}    Create Dictionary    contacts_lies=@{contacts_lies}
    Ajouter le document généré depuis le contexte du dossier de coordination    VPS-VISIT-000019    ${params}    Courrier simple    Courrier simple
    Ajouter le document généré depuis le contexte de l'établissement    T1302    LYCEE ALEXANDRE DUMAS    ${params}    Courrier simple    Courrier simple
    Ajouter le texte type depuis le menu    Article 1    Courrier simple    Article 1 : ce courrier est ...
    Ajouter le texte type depuis le menu    Article 2    Courrier simple    Article 2 : ce courrier est ...
    Ajouter le texte type depuis le menu    Article 3    Courrier simple    Article 3 : ce courrier est ...
    # Documents générés à éditer
    Ajouter un particulier depuis un établissement    ${etab01_code}    Mandataire    M.    Huard    Raymond    true
    @{contacts_lies}    Create List    (Mandataire) M. Huard Raymond
    ${params}    Create Dictionary    contacts_lies=@{contacts_lies}
    Ajouter le document généré depuis le contexte de l'établissement    ${etab01_code}  ${etab01.libelle}     ${params}    Courrier simple    Courrier simple
    Click On Back Button In Subform
    Click On Link  Huard Raymond
    ${courrier} =  Get Text  css=#code_barres
    Finaliser le document généré  ${courrier}
    Ajouter un particulier depuis un établissement    ${etab01_code}    Mandataire    M.    Gour    Pascal    true
    @{contacts_lies}    Create List    (Mandataire) M. Gour Pascal
    ${params}    Create Dictionary    contacts_lies=@{contacts_lies}
    Ajouter le document généré depuis le contexte de l'établissement    ${etab01_code}  ${etab01.libelle}     ${params}    Courrier simple    Courrier simple
    Click On Back Button In Subform
    Click On Link  Gour Pascal
    ${courrier} =  Get Text  css=#code_barres
    Finaliser le document généré  ${courrier}
    # Documents générés en attente de signature
    Ajouter un particulier depuis un établissement    ${etab01_code}    Mandataire    M.    Croquetaigne    Patrick    true
    @{contacts_lies}    Create List    (Mandataire) M. Croquetaigne Patrick
    ${params}    Create Dictionary    contacts_lies=@{contacts_lies}
    Ajouter le document généré depuis le contexte de l'établissement    ${etab01_code}  ${etab01.libelle}     ${params}    Courrier simple    Courrier simple
    Click On Back Button In Subform
    Click On Link  Croquetaigne Patrick
    ${courrier} =  Get Text  css=#code_barres
    Finaliser le document généré  ${courrier}
    Modifier le document généré depuis le menu  ${courrier}  ${DATE_FORMAT_DD/MM/YYYY}
    Ajouter un particulier depuis un établissement    ${etab01_code}    Mandataire    M.    Émond    Soren    true
    @{contacts_lies}    Create List    (Mandataire) M. Émond Soren
    ${params}    Create Dictionary    contacts_lies=@{contacts_lies}
    Ajouter le document généré depuis le contexte de l'établissement    ${etab01_code}  ${etab01.libelle}     ${params}    Courrier simple    Courrier simple
    Click On Back Button In Subform
    Click On Link  Émond Soren
    ${courrier} =  Get Text  css=#code_barres
    Finaliser le document généré  ${courrier}
    Modifier le document généré depuis le menu  ${courrier}  ${DATE_FORMAT_DD/MM/YYYY}
    # Documents générés en attente de retour AR
    Ajouter un particulier depuis un établissement    ${etab01_code}    Mandataire    M.    Desnoyer    Marc    true
    @{contacts_lies}    Create List    (Mandataire) M. Desnoyer Marc
    ${params}    Create Dictionary    contacts_lies=@{contacts_lies}
    Ajouter le document généré depuis le contexte de l'établissement    ${etab01_code}  ${etab01.libelle}     ${params}    Courrier simple    Courrier simple
    Click On Back Button In Subform
    Click On Link  Desnoyer Marc
    ${courrier} =  Get Text  css=#code_barres
    Finaliser le document généré  ${courrier}
    Modifier le document généré depuis le menu
    ...  code_barres=${courrier}
    ...  date_envoi_rar=${DATE_FORMAT_DD/MM/YYYY}
    Ajouter un particulier depuis un établissement    ${etab01_code}    Mandataire    M.    Morneau    Denis    true
    @{contacts_lies}    Create List    (Mandataire) M. Morneau Denis
    ${params}    Create Dictionary    contacts_lies=@{contacts_lies}
    Ajouter le document généré depuis le contexte de l'établissement    ${etab01_code}  ${etab01.libelle}     ${params}    Courrier simple    Courrier simple
    Click On Back Button In Subform
    Click On Link  Morneau Denis
    ${courrier} =  Get Text  css=#code_barres
    Finaliser le document généré  ${courrier}
    Modifier le document généré depuis le menu
    ...  code_barres=${courrier}
    ...  date_envoi_rar=${DATE_FORMAT_DD/MM/YYYY}

    @{ref_cad} =  Create List  806  AB  0025
    &{dc01} =  Create Dictionary
    ...  dossier_coordination_type=Permis de construire
    ...  description=Dossier de coordination concernant un permis de construire
    ...  etablissement=${etab01_titre}
    ...  date_butoir=${DATE_FORMAT_DD/MM/YYYY}
    ...  terrain_references_cadastrales=${ref_cad}
    ...  dossier_instruction_ads=PC0130551600001P0

    ${dc01_libelle} =  Ajouter le dossier de coordination  ${dc01}
    Set Suite Variable  ${dc01}
    Set Suite Variable  ${dc01_libelle}

    &{dc02} =  Create Dictionary
    ...  dossier_coordination_type=Autorisation de Travaux
    ...  description=Dossier de coordination concernant un permis de construire
    ...  etablissement=${etab01_titre}
    ...  date_butoir=${DATE_FORMAT_DD/MM/YYYY}
    ...  dossier_instruction_ads=PC0130551609998P0

    ${dc02_libelle} =  Ajouter le dossier de coordination  ${dc02}
    Set Suite Variable  ${dc02}
    Set Suite Variable  ${dc02_libelle}

    # Création d'un dossier de coordination dont les références cadastrales
    # seront modifiées afin de prendre le CE de l'action de récupération des
    # propriétaires par parcelles
    &{dc03} =  Create Dictionary
    ...  dossier_coordination_type=Autorisation de Travaux
    ...  description=Description
    ...  etablissement=${etab04_code} - ${etab04.libelle}
    ...  date_demande=${DATE_FORMAT_DD/MM/YYYY}
    ...  terrain_references_cadastrales=${etab04_ref_cad}
    ${dc03_libelle} =  Ajouter le dossier de coordination  ${dc03}
    Set Suite Variable  ${dc03_libelle}

    &{dc_visit_1} =  Create Dictionary
    ...  dossier_coordination_type=Visite de contrôle SI
    ...  description=Visite de contrôle du service sécurité incendie
    ...  etablissement=${etab01_titre}
    ...  dossier_instruction_ads=PC0130551609999P0
    ${dc_visit_1_libelle} =  Ajouter le dossier de coordination  ${dc_visit_1}
    Click Link  ${dc_visit_1_libelle}-SI
    ${dc_visit_1_id} =  Get Mandatory Value  css=#dossier_instruction
    ${di_visit_1_libelle} =  Get Text  css=#libelle
    Set Suite Variable  ${dc_visit_1_libelle}
    Set Suite Variable  ${di_visit_1_libelle}

    ##
    ## DOSSIER DE COORDINATION 04
    ##
    &{dc04} =  Create Dictionary
    ...  dossier_coordination_type=Autorisation de Travaux
    ...  description=Aménagement du sous-sol
    ...  date_demande=08/01/2015
    ...  etablissement=${etab01_titre}
    ...  a_qualifier=false
    ${dc04_libelle} =  Ajouter le dossier de coordination  ${dc04}
    Set Suite Variable  ${dc04}
    Set Suite Variable  ${dc04_libelle}
    Set Suite Variable  ${dc04_di_si}  ${dc04_libelle}-SI
    Set Suite Variable  ${dc04_di_acc}  ${dc04_libelle}-ACC

    ##
    ## DOSSIER DE COORDINATION 05
    ## Ce dossier n'est pas qualifié et possède une date de demande très
    ## ancienne
    ##
    &{dc05} =  Create Dictionary
    ...  dossier_coordination_type=Autorisation de Travaux
    ...  description=Aménagement du sous-sol
    ...  date_demande=08/01/2015
    ...  etablissement=${etab03_titre}
    ...  a_qualifier=true
    ${dc05_libelle} =  Ajouter le dossier de coordination  ${dc05}
    Set Suite Variable  ${dc05}
    Set Suite Variable  ${dc05_libelle}
    Set Suite Variable  ${dc05_di_si}  ${dc05_libelle}-SI
    Set Suite Variable  ${dc05_di_acc}  ${dc05_libelle}-ACC

    ##
    ## DOSSIER DE COORDINATION 06
    ## destiné à la gestion du marqueur connexion au référentiel ADS dans la
    ## section dossiers
    ##
    ${dossier_instruction_ads} =  Set Variable  AT0990991000001P0
    Depuis la page d'accueil  admin  admin
    Activer l'option référentiel ADS
    ${type} =  Set Variable  ADS_ERP__AT__DEPOT_INITIAL
    ${emetteur} =  Set Variable  108_guichetunique_at_test_170
    ${json} =  Set Variable  { "type" : "${type}", "date" : "01/01/2014 12:00", "emetteur" : "${emetteur}", "dossier_instruction" : "${dossier_instruction_ads}" }
    Vérifier le code retour du web service et vérifier que son message est  Post  messages  ${json}  200  Insertion du message '${type}' OK.
    ${json} =  Set Variable  { "module": "messagesasync" }
    Vérifier le code retour du web service et vérifier que son message contient  Post  maintenance  ${json}  200  Traitement terminé.
    Depuis le listing de tous les messages
    Input Text  css=div#adv-search-adv-fields input#emetteur  ${emetteur}
    Click On Search Button
    WUX  Element Should Contain  css=#tab-dossier_coordination_message_tous  ${emetteur}
    Click Link  ${emetteur}
    WUX  Page Title Should Be  Dossiers > Messages > Tous Les Messages > 01/01/2014 12:00:00
    WUX  Element Should Be Visible  css=span#dossier_coordination
    ${dc06_libelle} =  Get Text  css=span#dossier_coordination
    Set Suite Variable  ${dc06_libelle}
    Désactiver l'option référentiel ADS
    Depuis le formulaire de modification du dossier de coordination  ${dc06_libelle}
    Unselect Checkbox  css=#a_qualifier
    Click On Submit Button
    Valid Message Should Contain  Vos modifications ont bien été enregistrées.
    ${type} =  Set Variable  ADS_ERP__AT__DEPOT_DE_PIECE_PAR_LE_PETITIONNAIRE
    ${emetteur} =  Set Variable  guichetunique
    ${json} =  Set Variable  { "type" : "${type}", "date" : "31/12/2015 14:42", "emetteur" : "${emetteur}", "dossier_instruction" : "${dossier_instruction_ads}", "contenu": { "type_piece" : "complémentaire" } }
    Vérifier le code retour du web service et vérifier que son message est  Post  messages  ${json}  200  Insertion du message '${type}' OK.
    #

Initialisation du répertoire accueillant les captures
    [Documentation]
    [Tags]  doc
    Depuis la page d'accueil  admin  admin
    Activer le plugin geoaria_tests et l'option sig
    #
    Create Directory    results/screenshots
    Create Directory    results/screenshots/ergonomie
    Create Directory    results/screenshots/etablissements
    Create Directory    results/screenshots/dossiers
    Create Directory    results/screenshots/suivi
    Create Directory    results/screenshots/administration_parametrage


Section 'Ergonomie'
    [Documentation]  L'objet de ce 'Test Case' est de réaliser les captures d'écran
    ...    à destination de la documentation.
    [Tags]  doc
    #
    Depuis la page d'accueil  admin  admin
    Activer l'option référentiel ADS
    Go To Dashboard
    Capture and crop page screenshot  screenshots/ergonomie/widget_dossiers_instruction_a_qualifier_affecter.png
    ...    css=div.widget_dossier_instruction_a_qualifier_affecter
    #
    Depuis la page d'accueil    technicien-si    technicien-si
    Go To Dashboard
    #
    Capture and crop page screenshot  screenshots/ergonomie/tableau-de-bord-exemple.png
    ...    css=#content
    #
    Capture and crop page screenshot  screenshots/ergonomie/widget_mon_activite.png
    ...    css=div.widget_mon_activite
    Capture and crop page screenshot  screenshots/ergonomie/widget_mes_di_en_reunions.png
    ...    css=div.widget_mes_di_en_reunions
    Capture and crop page screenshot  screenshots/ergonomie/widget_mes_visites_a_realiser.png
    ...    css=div.widget_mes_visites_a_realiser
    Capture and crop page screenshot  screenshots/ergonomie/widget_mes_documents_entrants_non_lus.png
    ...    css=div.widget_mes_documents_entrants_non_lus
    Capture and crop page screenshot  screenshots/ergonomie/widget_dossier_instruction_mes_visites.png
    ...    css=div.widget_dossier_instruction_mes_visites
    Capture and crop page screenshot  screenshots/ergonomie/widget_dossier_instruction_mes_plans.png
    ...    css=div.widget_dossier_instruction_mes_plans
    Capture and crop page screenshot  screenshots/ergonomie/widget_mes_infos.png
    ...    css=div.widget_mes_infos
    #
    Depuis la page d'accueil    cadre-si    cadre-si
    Go To Dashboard
    #
    Capture and crop page screenshot  screenshots/ergonomie/widget_activite_service.png
    ...    css=div.widget_activite_service
    Capture and crop page screenshot  screenshots/ergonomie/widget_dossier_coordination_a_qualifier.png
    ...    css=div.widget_dossier_coordination_a_qualifier
    Capture and crop page screenshot  screenshots/ergonomie/widget_dossier_coordination_a_cloturer.png
    ...    css=div.widget_dossier_coordination_a_cloturer
    Capture and crop page screenshot  screenshots/ergonomie/widget_documents_entrants_a_valider.png
    ...    css=div.widget_documents_entrants_a_valider
    Capture and crop page screenshot  screenshots/ergonomie/widget_documents_entrants_suivis.png
    ...    css=div.widget_documents_entrants_suivis
    Capture and crop page screenshot  screenshots/ergonomie/widget_programmation_a_valider.png
    ...    css=div.widget_programmation_a_valider
    Capture and crop page screenshot  screenshots/ergonomie/widget_programmation_urgentes.png
    ...    css=div.widget_programmation_urgentes
    Capture and crop page screenshot  screenshots/ergonomie/widget_analyse_a_valider.png
    ...    css=div.widget_analyse_a_valider
    Capture and crop page screenshot  screenshots/ergonomie/widget_mes_messages_non_lus.png
    ...    css=div.widget_message_mes_non_lu
    Capture and crop page screenshot  screenshots/ergonomie/widget_documents_generes_a_editer.png
    ...    css=div.widget_documents_generes_a_editer
    Capture and crop page screenshot  screenshots/ergonomie/widget_documents_generes_attente_signature.png
    ...    css=div.widget_documents_generes_attente_signature
    Capture and crop page screenshot  screenshots/ergonomie/widget_documents_generes_attente_retour_ar.png
    ...    css=div.widget_documents_generes_attente_retour_ar
    #
    Depuis la page d'accueil    secretaire-si    secretaire-si
    Go To Dashboard
    #
    Capture and crop page screenshot  screenshots/ergonomie/widget_convocations_exploitants_a_envoyer.png
    ...    css=div.widget_convocations_exploitants_a_envoyer
    Capture and crop page screenshot  screenshots/ergonomie/widget_convocations_membres_a_envoyer.png
    ...    css=div.widget_convocations_membres_a_envoyer
    Capture and crop page screenshot  screenshots/ergonomie/widget_etablissements_npai.png
    ...    css=div.widget_etablissements_npai
    Capture and crop page screenshot  screenshots/ergonomie/widget_analyse_a_acter.png
    ...    css=div.widget_analyse_a_acter
    #
    Depuis la page d'accueil    nonconfig    nonconfig
    Go To Dashboard
    #
    Capture and crop page screenshot  screenshots/ergonomie/widget_profil_non_configure.png
    ...    css=div.widget

    #
    Depuis la page d'accueil    admin    admin
    Désactiver l'option référentiel ADS

Section 'Établissements'
    [Documentation]  L'objet de ce 'Test Case' est de réaliser les captures d'écran
    ...    à destination de la documentation.
    [Tags]  doc
    #
    Depuis la page d'accueil    admin    admin

    # MENU
    Click On Menu Rubrik    etablissements
    Capture and crop page screenshot  screenshots/etablissements/menu-rubrik-etablissements.png
    ...    css=#menu-list

    #
    Depuis le listing de tous les établissements
    Capture and crop page screenshot  screenshots/etablissements/etablissement_tous-listing.png
    ...    css=#content
    Capture and crop page screenshot  screenshots/etablissements/etablissement-listing-search.png
    ...    css=#advanced-form

    #
    Depuis le listing des ERP référentiels
    Capture and crop page screenshot  screenshots/etablissements/etablissement_referentiel_erp-listing.png
    ...    css=#content

    #
    Depuis le listing de tous les établissements
    Click On Add Button
    Capture and crop page screenshot  screenshots/etablissements/etablissement-form-ajouter.png
    ...    css=#content
    #
    Input Text    css=#autocomplete-voie-search    rue
    Sleep    1
    Capture and crop page screenshot  screenshots/etablissements/etablissement-form-autocomplete.png
    ...    css=#fieldset-form-etablissement_tous-etablissement

    #
    Depuis le formulaire de modification de l'établissement    T2803    ASSOCIATION RABELAIS LYCEE COLLEGE ECOLE
    #
    Capture and crop page screenshot  screenshots/etablissements/etablissement_exploitant-form-ajouter.png
    ...    css=#fieldset-form-etablissement_tous-exploitant
    #
    Capture and crop page screenshot  screenshots/etablissements/etablissement-form-patrimoine.png
    ...    css=#fieldset-form-etablissement_tous-localisation

    #
    Depuis l'import    import_etablissement
    Capture and crop page screenshot  screenshots/etablissements/etablissement-form-import.png
    ...    css=#content

    #
    Depuis le contexte de l'établissement    T2803    ASSOCIATION RABELAIS LYCEE COLLEGE ECOLE

    #
    Highlight heading  css=#action-form-etablissement_tous-geocoder
    Capture and crop page screenshot  screenshots/etablissements/etablissement-action-geolocaliser-link.png
    ...    css=#action-form-etablissement_tous-geocoder
    Clear highlight  css=#action-form-etablissement_tous-geocoder

    Click Element  action-form-etablissement_tous-geocoder
    WUX  Valid Message Should Contain  L'établissement a été géolocalisé avec une précision de
    Depuis le contexte de l'établissement    T2803    ASSOCIATION RABELAIS LYCEE COLLEGE ECOLE
    Capture and crop page screenshot  screenshots/etablissements/etablissement-fiche.png
    ...    css=#content

    #
    Highlight heading  css=#action-form-etablissement_tous-archiver
    Capture and crop page screenshot  screenshots/etablissements/etablissement-action-archiver-link.png
    ...    css=#action-form-etablissement_tous-archiver
    Clear highlight  css=#action-form-etablissement_tous-archiver
    #
    Archiver l'établissement    T2803    ASSOCIATION RABELAIS LYCEE COLLEGE ECOLE
    #
    Highlight heading  css=#action-form-etablissement_tous-desarchiver
    Capture and crop page screenshot  screenshots/etablissements/etablissement-action-desarchiver-link.png
    ...    css=#action-form-etablissement_tous-desarchiver
    Clear highlight  css=#action-form-etablissement_tous-desarchiver
    #
    Désarchiver l'établissement    T2803    ASSOCIATION RABELAIS LYCEE COLLEGE ECOLE

    #
    Depuis l'onglet contact de l'établissement    T2803    ASSOCIATION RABELAIS LYCEE COLLEGE ECOLE
    Capture and crop page screenshot  screenshots/etablissements/etablissement_contact-listing.png
    ...    css=#content

    #
    Depuis l'onglet document entrant de l'établissement    T3074
    Capture and crop page screenshot  screenshots/etablissements/etablissement-onglet-documents-entrants-listing.png
    ...    css=#content


    #
    Depuis le listing des 'UA validées' sur l'établissement    ${etab01_code}    ${etab01.libelle}
    Capture and crop page screenshot  screenshots/etablissements/etablissement_onglet-ua.png
    ...    css=#content
    Capture and crop page screenshot  screenshots/etablissements/etablissement-onglet-ua-listing-ua-validees.png
    ...    css=#ua-tabs
    #
    Depuis le listing des 'UA en projet' sur l'établissement    ${etab01_code}    ${etab01.libelle}
    Capture and crop page screenshot  screenshots/etablissements/etablissement-onglet-ua-listing-ua-en-projet.png
    ...    css=#ua-tabs
    #
    Depuis le listing des 'UA archivées' sur l'établissement    ${etab01_code}    ${etab01.libelle}
    Capture and crop page screenshot  screenshots/etablissements/etablissement-onglet-ua-listing-ua-archives.png
    ...    css=#ua-tabs
    #
    Depuis le listing des UA
    Capture and crop page screenshot  screenshots/etablissements/etablissement-ua-listing.png
    ...    css=#content
    Capture and crop page screenshot  screenshots/etablissements/etablissement-ua-search.png
    ...    css=#advanced-form
    #
    Depuis la fiche de l'UA    ${etab01_code}    ${etab01.libelle}    ${ua_validee1_libelle}
    Capture and crop page screenshot  screenshots/etablissements/etablissement-ua-view.png
    ...    css=#content

    # On modifie le champ id_voie_ref de la rue de Rome qui est envoyé par la
    # suite au SIG
    &{voie} =  Create Dictionary
    ...  libelle=Rue de Rome
    ...  id_voie_ref=100

    Modifier voie  Rue de Rome  ${voie}

    # Création d'un établissement qui ne pourra pas être géolocalisé automatiquement
    &{etab} =  Create Dictionary
    ...  libelle=Métro la Fourragère
    ...  etablissement_nature=ERP potentiel
    ...  adresse_numero=350
    ...  adresse_voie=Rue de Rome
    ...  acc_consignes_om_html=Consigne
    ...  acc_descriptif_om_html=Description
    ...  si_consignes_om_html=Consigne
    ...  si_descriptif_om_html=Description
    ...  si_autorite_competente_visite=Commission communale de sécurité
    ...  si_autorite_competente_plan=Commission communale de sécurité
    ...  si_type_alarme=2a
    ...  si_type_ssi=A
    Ajouter l'établissement  ${etab}
    #
    WUX  Valid Message Should Contain  L'établissement n'a pas pu être créé automatiquement sur le SIG.
    WUX  Valid Message Should Contain  Cliquez ici pour le dessiner.
    Capture and crop page screenshot  screenshots/etablissements/etablissement-ajouter-geolocaliser-fail.png
    ...    css=.message.ui-widget.ui-corner-all.ui-state-highlight.ui-state-valid

    # Création d'un établissement qui pourra être géolocalisé automatiquement
    @{ref_cad} =  Create List  806  AB  25
    &{etab} =  Create Dictionary
    ...  libelle=Métro la Fourragère
    ...  etablissement_nature=ERP potentiel
    ...  adresse_numero=150
    ...  terrain_references_cadastrales=${ref_cad}
    ...  acc_consignes_om_html=Consigne
    ...  acc_descriptif_om_html=Description
    ...  si_consignes_om_html=Consigne
    ...  si_descriptif_om_html=Description
    ...  si_autorite_competente_visite=Commission communale de sécurité
    ...  si_autorite_competente_plan=Commission communale de sécurité
    ...  si_type_alarme=2a
    ...  si_type_ssi=A
    Ajouter l'établissement  ${etab}
    #
    WUX  Valid Message Should Contain  L'établissement a été géolocalisé avec une précision de 0m.
    Capture and crop page screenshot  screenshots/etablissements/etablissement-ajouter-geolocaliser-success.png
    ...    css=.message.ui-widget.ui-corner-all.ui-state-highlight.ui-state-valid

    # Création d'un établissement qui est déjà géolocalisé
    &{etab} =  Create Dictionary
    ...  libelle=Métro la Fourragère
    ...  etablissement_nature=ERP potentiel
    ...  adresse_numero=9797
    ...  acc_consignes_om_html=Consigne
    ...  acc_descriptif_om_html=Description
    ...  si_consignes_om_html=Consigne
    ...  si_descriptif_om_html=Description
    ...  si_autorite_competente_visite=Commission communale de sécurité
    ...  si_autorite_competente_plan=Commission communale de sécurité
    ...  si_type_alarme=2a
    ...  si_type_ssi=A
    Ajouter l'établissement  ${etab}
    #
    WUX  Valid Message Should Contain  L'établissement est déjà géolocalisé.
    Capture and crop page screenshot  screenshots/etablissements/etablissement-ajouter-geolocaliser-deja-geolocalise.png
    ...    css=.message.ui-widget.ui-corner-all.ui-state-highlight.ui-state-valid

    #
    # Captures d'écran des actions de géolocalisation sur la fiche des établissements
    #

    Depuis le contexte de l'établissement  ${etab01_code}
    Click Element  action-form-etablissement_tous-geocoder
    Wait Until Element Is Visible  css=.message.ui-widget.ui-corner-all.ui-state-highlight.ui-state-valid
    Capture and crop page screenshot  screenshots/etablissements/etablissement-geolocaliser-deja-geolocalise.png
    ...    css=.message.ui-widget.ui-corner-all.ui-state-highlight.ui-state-valid

    Depuis le contexte de l'établissement  ${etab02_code}
    Click Element  action-form-etablissement_tous-geocoder
    Wait Until Element Is Visible  css=.message.ui-widget.ui-corner-all.ui-state-highlight.ui-state-valid
    Capture and crop page screenshot  screenshots/etablissements/etablissement-geolocaliser-success.png
    ...    css=.message.ui-widget.ui-corner-all.ui-state-highlight.ui-state-valid

    Depuis le contexte de l'établissement  ${etab03_code}
    Click Element  action-form-etablissement_tous-geocoder
    Wait Until Element Is Visible  css=.message.ui-widget.ui-corner-all.ui-state-highlight.ui-state-valid
    Capture and crop page screenshot  screenshots/etablissements/etablissement-geolocaliser-fail.png
    ...    css=.message.ui-widget.ui-corner-all.ui-state-highlight.ui-state-valid


    # Captures d'écran des de récupération des propriétaires par parcelles sur
    # la fiche des établissements


    Depuis le contexte de l'établissement  ${etab04_code}

    Capture and crop page screenshot  screenshots/etablissements/etablissement-action-recup-proprietaire-link.png
    ...    css=span.contact-16

    Click Element  css=#references_cadastrales_get_plot_owner
    Sleep  1

    WUX  Capture and crop page screenshot  screenshots/etablissements/etablissement-recup-proprietaire-liste.png
    ...    css=div.ui-dialog

    Capture and crop page screenshot  screenshots/etablissements/etablissement-recup-proprietaire-action-ajout-contact-link.png
    ...    css=a#plot_owner_add_btn_contact

    # Bouton ajouter en sousform
    WUX  Click Element  css=span.add-16
    Sleep  1
    La page ne doit pas contenir d'erreur

    WUX  Capture and crop page screenshot  screenshots/etablissements/etablissement-recup-proprietaire-form-ajout-contact.png
    ...    css=div.ui-dialog

    Capture and crop page screenshot  screenshots/etablissements/etablissement-recup-proprietaire-action-retour-contact-link.png
    ...    css=div#sformulaire a.retour

    Capture and crop page screenshot  screenshots/etablissements/etablissement-recup-proprietaire-action-fermer-overlay.png
    ...    css=input#plot_owner_close_btn

    Click Element  css=span.ui-icon-closethick
    Wait Until Element Is Not Visible  css=div#overlay-container

    @{ref_cad} =  Create List  111  AA  1111
    &{values} =  Create Dictionary
    ...  terrain_references_cadastrales=${ref_cad}
    Modifier l'établissement  ${values}  ${etab04_code}
    Click Element  css=#references_cadastrales_get_plot_owner
    Sleep  1

    WUX  Capture and crop page screenshot  screenshots/etablissements/etablissement-recup-proprietaire-vide.png
    ...    css=div.ui-dialog

    Click Element  css=span.ui-icon-closethick
    Wait Until Element Is Not Visible  css=div#overlay-container

    @{ref_cad} =  Create List  777  ZZ  7777
    &{values} =  Create Dictionary
    ...  terrain_references_cadastrales=${ref_cad}
    Modifier l'établissement  ${values}  ${etab04_code}
    Click Element  css=#references_cadastrales_get_plot_owner
    Sleep  1

    WUX  Capture and crop page screenshot  screenshots/etablissements/etablissement-recup-proprietaire-erreur-sig.png
    ...    css=div.ui-dialog

    Click Element  css=span.ui-icon-closethick
    Wait Until Element Is Not Visible  css=div#overlay-container

    # END / Les établissements



Section 'Dossiers'
    [Documentation]  L'objet de ce 'Test Case' est de réaliser les captures d'écran
    ...    à destination de la documentation.
    [Tags]  doc

    # BEGIN / Messages
    Depuis la page d'accueil    admin    admin
    Activer l'option référentiel ADS
    Depuis la page d'accueil  cadre-si  cadre-si
    #
    Depuis le listing de tous les messages
    Capture and crop page screenshot  screenshots/dossiers/messages-listing-tous-les-messages.png
    ...    css=#content
    #
    Click Link      guichetunique
    Capture and crop page screenshot  screenshots/dossiers/messages-fiche-visualisation.png
    ...    css=#content
    #
    Depuis le listing de mes messages non lus
    Capture and crop page screenshot  screenshots/dossiers/messages-listing-mes-non-lus.png
    ...    css=#content
    #
    Depuis le listing des messages dans le contexte du dossier d'instruction  ${dc06_libelle}-SI
    On clique sur l'onglet  dossier_coordination_message_contexte_di  Messages
    Capture and crop page screenshot  screenshots/dossiers/di-onglet-messages-listing.png
    ...    css=#content
    #
    Click Link      guichetunique
    WUX  Portlet Action Should Be In SubForm      dossier_coordination_message_contexte_di    marquer_si_cadre_comme_lu
    Capture and crop page screenshot  screenshots/dossiers/di-onglet-messages-fiche-visualisation-action-marquer-comme-lu-link.png
    ...    css=#action-sousform-dossier_coordination_message_contexte_di-marquer_si_cadre_comme_lu
    Click On SubForm Portlet Action    dossier_coordination_message_contexte_di    marquer_si_cadre_comme_lu
    WUX  Valid Message Should Contain  Le message a été marqué comme lu.
    #
    WUX  Portlet Action Should Be In SubForm      dossier_coordination_message_contexte_di    marquer_si_cadre_comme_non_lu
    Capture and crop page screenshot  screenshots/dossiers/di-onglet-messages-fiche-visualisation.png
    ...    css=#content
    #
    Capture and crop page screenshot  screenshots/dossiers/di-onglet-messages-fiche-visualisation-action-marquer-comme-non-lu-link.png
    ...    css=#action-sousform-dossier_coordination_message_contexte_di-marquer_si_cadre_comme_non_lu
    Depuis la page d'accueil    admin    admin
    Désactiver l'option référentiel ADS
    # END / Messages

    Depuis la page d'accueil    admin    admin

    # BEGIN / Connexion Référentiel ADS + DA DI ADS
    Activer l'option référentiel ADS
    Depuis le contexte du dossier de coordination  ${dc06_libelle}
    Capture and crop page screenshot  screenshots/dossiers/dossier_coordination-connecte-referentiel-ads-flag.png
    ...    css=#fieldset-form-dossier_coordination-urbanisme
    Highlight heading  css=div.field-type-link_dossier_autorisation_ads
    Capture and crop page screenshot  screenshots/dossiers/dossier_coordination-da-ads-link.png
    ...    css=#fieldset-form-dossier_coordination-urbanisme
    Depuis le contexte du dossier d'instruction  ${dc06_libelle}-SI
    Capture and crop page screenshot  screenshots/dossiers/dossier_instruction-avec-dc-connecte-referentiel-ads-flag.png
    ...    css=#fieldset-form-dossier_instruction-urbanisme
    Highlight heading  css=div.field-type-link_dossier_autorisation_ads
    Capture and crop page screenshot  screenshots/dossiers/dossier_instruction-da-ads-link.png
    ...    css=#fieldset-form-dossier_instruction-urbanisme
    Désactiver l'option référentiel ADS
    # END / Connexion Référentiel ADS + DA DI ADS

    # BEGIN / A enjeu ERP
    Depuis le contexte du dossier de coordination  ${dc04_libelle}
    Capture and crop page screenshot  screenshots/dossiers/dossier_coordination-action-marquer_a_enjeu-link.png
    ...    css=#action-form-dossier_coordination-marquer_a_enjeu
    Click On Form Portlet Action    dossier_coordination    marquer_a_enjeu  modale
    Wait Until Element Is Visible    css=.ui-dialog .ui-dialog-buttonset button
    Click Element    css=.ui-dialog .ui-dialog-buttonset button
    WUX  Valid Message Should Contain  Marqueur 'enjeu ERP' activé.
    #
    Capture and crop page screenshot  screenshots/dossiers/dossier_coordination-action-demarquer_a_enjeu-link.png
    ...    css=#action-form-dossier_coordination-demarquer_a_enjeu
    Capture and crop page screenshot  screenshots/dossiers/dossier_coordination-a-enjeu-flag.png
    ...    css=#fieldset-form-dossier_coordination-informations-generales
    Depuis le contexte du dossier d'instruction  ${dc04_di_si}
    Capture and crop page screenshot  screenshots/dossiers/dossier_instruction-avec-dc-a-enjeu-flag.png
    ...    css=#fieldset-form-dossier_instruction-dossier-de-coordination
    # END / A enjeu ERP

    # START / Les dossiers de coordination
    Depuis le listing des dossiers de coordination à qualifier
    Capture and crop page screenshot  screenshots/dossiers/dossier_coordination_a_qualifier-listing.png
    ...    css=#content
    Depuis le listing des dossiers de coordination
    Capture and crop page screenshot  screenshots/dossiers/dossier_coordination-listing.png
    ...    css=#content
    # On se rend sur le dossier de coordination
    Depuis le contexte du dossier de coordination  ${dc01_libelle}
    Capture and crop page screenshot  screenshots/dossiers/dossier_coordination-fiche.png
    ...    css=#content
    Click Element  action-form-dossier_coordination-geocoder
    WUX  Valid Message Should Contain  Le dossier de coordination a été géolocalisé avec une précision de 0m.
    Capture and crop page screenshot  screenshots/dossiers/dossier_coordination-geolocaliser-success.png
    ...    css=.message.ui-widget.ui-corner-all.ui-state-highlight.ui-state-valid

    Depuis le contexte du dossier de coordination  ${dc02_libelle}
    Click Element  action-form-dossier_coordination-geocoder
    WUX  Valid Message Should Contain  Le dossier de coordination n'a pas pu être créé automatiquement sur le SIG.
    Capture and crop page screenshot  screenshots/dossiers/dossier_coordination-geolocaliser-fail.png
    ...    css=.message.ui-widget.ui-corner-all.ui-state-highlight.ui-state-valid

    Depuis le contexte du dossier de coordination  ${dc_visit_1_libelle}
    Click Element  action-form-dossier_coordination-geocoder
    WUX  Valid Message Should Contain  Le dossier de coordination est déjà géolocalisé.
    Capture and crop page screenshot  screenshots/dossiers/dossier_coordination-geolocaliser-deja-geolocalise.png
    ...    css=.message.ui-widget.ui-corner-all.ui-state-highlight.ui-state-valid

    Click Element  action-form-dossier_coordination-modifier
    @{ref_cad} =  Create List  806  AB  25
    Saisir les références cadastrales  ${ref_cad}
    Capture and crop page screenshot  screenshots/dossiers/dossier_coordination-overlay-etablissements-proches-action-link.png
    ...    css=#fieldset-form-dossier_coordination-qualification
    Click Element  autocomplete-etablissement_tous-link-selection
    Execute JavaScript  window.jQuery("#tab_etablissements_proches table tbody tr td.col-1").mousemove();
    Sleep  2
    Capture and crop page screenshot  screenshots/dossiers/dossier_coordination-overlay-etablissements-proches-success.png
    ...    css=#overlay-etablissements-proches

    Click Element  close-button
    @{ref_cad} =  Create List  806  AB  24
    Saisir les références cadastrales  ${ref_cad}
    Click Element  autocomplete-etablissement_tous-link-selection
    Capture and crop page screenshot  screenshots/dossiers/dossier_coordination-overlay-etablissements-proches-tableau-vide.png
    ...    css=#overlay-etablissements-proches

    &{dc} =  Create Dictionary
    ...  dossier_coordination_type=Autorisation de Travaux
    ...  description=Dossier de coordination concernant une visite de réception
    ...  etablissement=${etab01_titre}

    Go To  ${PROJECT_URL}${OM_ROUTE_FORM}&obj=dossier_coordination_nouveau&action=0&retour=form
    Saisir les valeurs dans le formulaire du dossier de coordination  ${dc}
    Click On Submit Button
    WUX  Valid Message Should Contain  Vos modifications ont bien été enregistrées.
    WUX  Valid Message Should Contain  Le dossier de coordination a été géolocalisé avec une précision de 15m.
    Capture and crop page screenshot  screenshots/dossiers/dossier_coordination-ajouter-geolocaliser-success.png
    ...    css=.message.ui-widget.ui-corner-all.ui-state-highlight.ui-state-valid

    &{dc} =  Create Dictionary
    ...  dossier_coordination_type=Autorisation de Travaux
    ...  description=Dossier de coordination concernant une visite de réception
    ...  etablissement=${etab01_titre}
    ...  dossier_instruction_ads=PC0130551609998P0

    Go To  ${PROJECT_URL}${OM_ROUTE_FORM}&obj=dossier_coordination_nouveau&action=0&retour=form
    Saisir les valeurs dans le formulaire du dossier de coordination  ${dc}
    Click On Submit Button
    WUX  Valid Message Should Contain  Vos modifications ont bien été enregistrées.
    WUX  Valid Message Should Contain  Le dossier de coordination n'a pas pu être créé automatiquement sur le SIG.
    Capture and crop page screenshot  screenshots/dossiers/dossier_coordination-ajouter-geolocaliser-fail.png
    ...    css=.message.ui-widget.ui-corner-all.ui-state-highlight.ui-state-valid

    &{dc01} =  Create Dictionary
    ...  dossier_coordination_type=Autorisation de Travaux
    ...  description=Dossier de coordination concernant une visite de réception
    ...  etablissement=${etab01_titre}
    ...  dossier_instruction_ads=PC0130551609999P0

    Go To  ${PROJECT_URL}${OM_ROUTE_FORM}&obj=dossier_coordination_nouveau&action=0&retour=form
    Saisir les valeurs dans le formulaire du dossier de coordination  ${dc01}
    Click On Submit Button
    WUX  Valid Message Should Contain  Vos modifications ont bien été enregistrées.
    WUX  Valid Message Should Contain  Le dossier de coordination est déjà géolocalisé.
    Capture and crop page screenshot  screenshots/dossiers/dossier_coordination-ajouter-geolocaliser-deja-geolocalise.png
    ...    css=.message.ui-widget.ui-corner-all.ui-state-highlight.ui-state-valid

    &{dc} =  Create Dictionary
    ...  dossier_coordination_type=Visite de réception
    ...  description=Dossier de coordination concernant une visite de réception
    ...  etablissement=${etab01_titre}

    Go To  ${PROJECT_URL}${OM_ROUTE_FORM}&obj=dossier_coordination_nouveau&action=0&retour=form
    Saisir les valeurs dans le formulaire du dossier de coordination  ${dc}
    Click On Submit Button
    WUX  Valid Message Should Contain  Vos modifications ont bien été enregistrées.
    WUX  Valid Message Should Contain  Le dossier n'est pas géolocalisable
    Capture and crop page screenshot  screenshots/dossiers/dossier_coordination-ajouter-geolocaliser-type-non-geolocalisable.png
    ...    css=.message.ui-widget.ui-corner-all.ui-state-highlight.ui-state-valid
        # On vérifie que les DI sont correctement créés si la case à cocher 'à qualifier' est décochée
    # et que la case à cocher du DI est cochée.
    ${a_qualifier_checked} =    Run Keyword And Return Status    Checkbox Should Be Selected    css=#a_qualifier
    ${di_si_checked} =    Run Keyword And Return Status    Checkbox Should Be Selected    css=#dossier_instruction_secu
    ${di_acc_checked} =    Run Keyword And Return Status    Checkbox Should Be Selected    css=#dossier_instruction_acc
    Run Keyword If    '${a_qualifier_checked}' == 'False' and '${di_si_checked}' == 'True'    Valid Message Should Contain    -SI a été créé.
    Run Keyword If    '${a_qualifier_checked}' == 'False' and '${di_acc_checked}' == 'True'    Valid Message Should Contain    -ACC a été créé.

    #
    # Captures d'écran des de récupération des propriétaires par parcelles sur
    # la fiche des dossiers de coordination
    #

    Depuis le contexte du dossier de coordination  ${dc03_libelle}

    Capture and crop page screenshot  screenshots/dossiers/dossier_coordination-action-recup-proprietaire-link.png
    ...    css=span.contact-16

    Click Element  css=#references_cadastrales_get_plot_owner
    Sleep  1

    WUX  Capture and crop page screenshot  screenshots/dossiers/dossier_coordination-recup-proprietaire-liste.png
    ...    css=div.ui-dialog

    Capture and crop page screenshot  screenshots/dossiers/dossier_coordination-recup-proprietaire-action-ajout-contact-link.png
    ...    css=a#plot_owner_add_btn_contact

    # Bouton ajouter en sousform
    WUX  Click Element  css=span.add-16
    Sleep  1
    La page ne doit pas contenir d'erreur

    WUX  Capture and crop page screenshot  screenshots/dossiers/dossier_coordination-recup-proprietaire-form-ajout-contact.png
    ...    css=div.ui-dialog

    Capture and crop page screenshot  screenshots/dossiers/dossier_coordination-recup-proprietaire-action-retour-contact-link.png
    ...    css=div#overlay-container div#sformulaire a.retour

    Capture and crop page screenshot  screenshots/dossiers/dossier_coordination-recup-proprietaire-action-fermer-overlay.png
    ...    css=input#plot_owner_close_btn

    Click Element  css=span.ui-icon-closethick
    Wait Until Element Is Not Visible  css=div#overlay-container

    @{ref_cad} =  Create List  111  AA  1111
    &{values} =  Create Dictionary
    ...  terrain_references_cadastrales=${ref_cad}
    Modifier le dossier de coordination  ${values}  ${dc03_libelle}
    Click Element  css=#references_cadastrales_get_plot_owner
    Sleep  1

    WUX  Capture and crop page screenshot  screenshots/dossiers/dossier_coordination-recup-proprietaire-vide.png
    ...    css=div.ui-dialog

    Click Element  css=span.ui-icon-closethick
    Wait Until Element Is Not Visible  css=div#overlay-container

    @{ref_cad} =  Create List  777  ZZ  7777
    &{values} =  Create Dictionary
    ...  terrain_references_cadastrales=${ref_cad}
    Modifier le dossier de coordination  ${values}  ${dc03_libelle}
    Click Element  css=#references_cadastrales_get_plot_owner
    Sleep  1

    WUX  Capture and crop page screenshot  screenshots/dossiers/dossier_coordination-recup-proprietaire-erreur-sig.png
    ...    css=div.ui-dialog

    Click Element  css=span.ui-icon-closethick
    Wait Until Element Is Not Visible  css=div#overlay-container

    #
    Depuis l'onglet DC Fils du dossier de coordination    VPS-VISIT-000003
    Capture and crop page screenshot  screenshots/dossiers/dc-onglet-dc-fils-listing.png
    ...    css=#content

    # END / Les dossiers de coordination

    # START / Les dossiers d'instruction

    Depuis le listing  dossier_instruction
    Capture and crop page screenshot  screenshots/dossiers/dossier_instruction-listing.png
    ...    css=#content
    # On se rend sur un dossier d'instruction
    Input Text  css=#dossier  ${di_visit_1_libelle}
    Click On Search Button
    Click On Link  ${di_visit_1_libelle}
    Capture and crop page screenshot  screenshots/dossiers/dossier_instruction-fiche.png
    ...    css=#content


    # END / Les dossiers d'instruction

    #
    Click On Menu Rubrik    dossiers
    Click Element    css=#title
    Capture and crop page screenshot  screenshots/dossiers/menu-rubrik-dossiers.png
    ...    css=#menu-list

    #
    Depuis le formulaire d'ajout du dossier de coordination
    Capture and crop page screenshot  screenshots/dossiers/dossier_coordination-form-ajouter.png
    ...    css=#content

    #
    Depuis l'onglet document entrant du dossier de coordination    VPS-VISIT-000003
    Capture and crop page screenshot  screenshots/dossiers/dc-onglet-documents-entrants-listing.png
    ...    css=#content

    #
    Depuis l'onglet document entrant du dossier d'instruction    VPS-VISIT-000003-SI
    Capture and crop page screenshot  screenshots/dossiers/di-onglet-documents-entrants-listing.png
    ...    css=#content

    # BEGIN - DI - ONGLET PV

    # Analyse validée -> 'Générer un nouveau PV' disponible
    Depuis l'onglet PV du DI  VPS-VISIT-000016-SI
    Capture and crop page screenshot  screenshots/dossiers/di-onglet-pv-listing-action-generer.png
    ...    css=#generer_pv
    Click Element  css=#generer_pv
    WUX  Element Should Contain  css=#sousform-proces_verbal  odèle d'édition à utiliser lors de la génération
    Capture and crop page screenshot  screenshots/dossiers/di-onglet-pv-form-action-generer.png
    ...    css=#content

    # On valide la génération pour accéder à la fiche de visualisation d'un PV généré
    Select From List By Label  css=#dossier_instruction_reunion  01/12/2014 - Groupe Technique d'Etude de Plans
    Click On Submit Button In Subform
    Valid Message Should Contain In Subform  Vos modifications ont bien été enregistrées.
    ${pv_numero} =  Get Text  css=#new_pv_number
    Click On Back Button In Subform
    Click On Link  ${pv_numero}
    Capture and crop page screenshot  screenshots/dossiers/di-onglet-pv-form-action-consulte-pv-genere.png
    ...    css=#content

    # Analyse re-validée -> 'Regénérer le dernier PV' disponible
    Accéder à l'analyse du dossier d'instruction  VPS-VISIT-000016-SI
    Réouvrir l'analyse
    Terminer l'analyse
    Valider l'analyse
    Depuis l'onglet PV du DI  VPS-VISIT-000016-SI
    Capture and crop page screenshot  screenshots/dossiers/di-onglet-pv-listing.png
    ...    css=#content
    Capture and crop page screenshot  screenshots/dossiers/di-onglet-pv-listing-action-regenerer.png
    ...    css=#regenerer_pv
    Click Element  css=#regenerer_pv
    WUX  Element Should Contain  css=#sousform-proces_verbal  uméro du PV à regénérer
    Capture and crop page screenshot  screenshots/dossiers/di-onglet-pv-form-action-regenerer.png
    ...    css=#content

    # 'Ajouter un PV' disponible
    Depuis l'onglet PV du DI  VPS-VISIT-000012-SI
    Capture and crop page screenshot  screenshots/dossiers/di-onglet-pv-listing-action-ajouter.png
    ...    css=#ajouter_pv
    Click Element    css=#ajouter_pv
    WUX  Element Should Contain  css=#sousform-proces_verbal  PV signé
    Capture and crop page screenshot  screenshots/dossiers/di-onglet-pv-form-action-ajouter.png
    ...    css=#content

    # On valide l'ajout pour accéder à la fiche de visualisation d'un PV ajouté
    Select From List By Label  css=#dossier_instruction_reunion  28/11/2014 - Commisson Communale de Sécurité
    Input Datepicker  date_redaction  02/01/2015
    Add File  om_fichier_signe  pv_ajoute.pdf
    Click On Submit Button In Subform
    Valid Message Should Contain In Subform  Vos modifications ont bien été enregistrées.
    Click On Back Button In Subform
    Click On Link  ajouté
    Capture and crop page screenshot  screenshots/dossiers/di-onglet-pv-form-action-consulte-pv-ajoute.png
    ...    css=#content

    # END - DI - ONGLET PV

    # BEGIN - MODULE SWROD
    Activer le plugin swrodaria_tests et l'option
    Depuis le formulaire de modification du dossier de coordination  ${dc04_libelle}
    Input Text  css=#dossier_autorisation_ads  PC0130551200001
    Input Text  css=#dossier_instruction_ads  PC0130551200001M01
    Click On Submit Button
    Valid Message Should Contain    Vos modifications ont bien été enregistrées.
    #
    Depuis l'onglet document entrant du dossier de coordination    ${dc04_libelle}
    Click Element  css=#onglet-document_entrant_guichet_unique__contexte_dc
    WUX  Element Should Be Visible  css=#swrod table.document_numerise
    Capture and crop page screenshot  screenshots/dossiers/dc-onglet-documents-entrants-swrod-onglet-gu-view.png
    ...    css=#content
    #
    Depuis l'onglet document entrant du dossier d'instruction    ${dc04_di_si}
    Click Element  css=#onglet-document_entrant_guichet_unique__contexte_di
    WUX  Element Should Be Visible  css=#swrod table.document_numerise
    Capture and crop page screenshot  screenshots/dossiers/di-onglet-documents-entrants-swrod-onglet-gu-view.png
    ...    css=#content
    #
    Désactiver le plugin swrodaria_tests et l'option
    # END - MODULE SWROD

    # On remet le jeu de test dans l'état initial
    &{voie} =  Create Dictionary
    ...  libelle=Rue de Rome
    ...  id_voie_ref=${EMPTY}

    Modifier voie  Rue de Rome  ${voie}

Section 'Suivi'
    [Documentation]  L'objet de ce 'Test Case' est de réaliser les captures d'écran
    ...    à destination de la documentation.
    [Tags]  doc
    #
    Depuis la page d'accueil    admin    admin
    #
    Click On Menu Rubrik    suivi
    Capture and crop page screenshot  screenshots/suivi/menu-rubrik-suivi.png
    ...    css=#menu-list
    #
    Depuis le listing des réunions
    Capture and crop page screenshot  screenshots/suivi/reunions-listing.png
    ...    css=#content
    #
    Click On Add Button
    Select From List By Label    css=#reunion_type    Commisson Communale de Sécurité
    Capture and crop page screenshot  screenshots/suivi/reunions-form-ajouter.png
    ...    css=#content
    #
    Depuis le contexte de la réunion    ${reunion03_code}
    Capture and crop page screenshot  screenshots/suivi/reunions-fiche.png
    ...    css=#content

    Depuis le contexte de la réunion    ${reunion03_code}
    Highlight heading  css=#action-form-reunion-meeting
    Capture and crop page screenshot  screenshots/suivi/reunions-action-meeting-link.png
    ...    css=#action-form-reunion-meeting
    Click On Form Portlet Action    reunion    meeting
    Capture and crop page screenshot  screenshots/suivi/reunions-action-meeting-view.png
    ...    css=#content
    #
    Depuis le contexte de la réunion    ${reunion03_code}
    Highlight heading  css=#action-form-reunion-planifier
    Capture and crop page screenshot  screenshots/suivi/reunions-action-planifier-link.png
    ...    css=#action-form-reunion-planifier
    Click On Form Portlet Action    reunion    planifier
    Capture and crop page screenshot  screenshots/suivi/reunions-action-planifier-view.png
    ...    css=#content

    Depuis le contexte de la réunion    ${reunion03_code}
    Highlight heading  css=#action-form-reunion-deplanifier
    Capture and crop page screenshot  screenshots/suivi/reunions-action-deplanifier-link.png
    ...    css=#action-form-reunion-deplanifier
    Click On Form Portlet Action    reunion    deplanifier
    Capture and crop page screenshot  screenshots/suivi/reunions-action-deplanifier-view.png
    ...    css=#content
    #
    Depuis le contexte de la réunion    ${reunion03_code}
    Highlight heading  css=#action-form-reunion-planifier-nouveau
    Capture and crop page screenshot  screenshots/suivi/reunions-action-planifier-nouveau-link.png
    ...    action-form-reunion-planifier-nouveau
    Click On Form Portlet Action    reunion    planifier-nouveau
    Capture and crop page screenshot  screenshots/suivi/reunions-action-planifier-nouveau-view.png
    ...    css=#content
    Depuis le formulaire de planification directe en mode pour la réunion    programmation    ${reunion03_code}
    Capture and crop page screenshot  screenshots/suivi/reunions-action-planifier-nouveau-view-programmation.png
    ...    css=#content
    Depuis le formulaire de planification directe en mode pour la réunion    réunion    ${reunion03_code}
    Capture and crop page screenshot  screenshots/suivi/reunions-action-planifier-nouveau-view-reunion.png
    ...    css=#content
    Depuis le formulaire de planification directe en mode pour la réunion    dossier    ${reunion03_code}
    Capture and crop page screenshot  screenshots/suivi/reunions-action-planifier-nouveau-view-dossier.png
    ...    css=#content
    #
    Depuis le contexte de la réunion    ${reunion03_code}
    Highlight heading  css=#action-form-reunion-numeroter
    Capture and crop page screenshot  screenshots/suivi/reunions-action-numeroter-link.png
    ...    css=#action-form-reunion-numeroter
    Click On Form Portlet Action    reunion    numeroter    modale
    Capture and crop page screenshot  screenshots/suivi/reunions-action-numeroter-view.png
    ...    css=#content
    Click Element    css=.ui-dialog-titlebar-close
    #
    Depuis le contexte de la réunion    ${reunion03_code}
    Highlight heading  css=#action-form-reunion-convoquer
    Capture and crop page screenshot  screenshots/suivi/reunions-action-convoquer-link.png
    ...    css=#action-form-reunion-convoquer
    Click On Form Portlet Action    reunion    convoquer
    Capture and crop page screenshot  screenshots/suivi/reunions-action-convoquer-view.png
    ...    css=#content
    #
    Depuis le contexte de la réunion    ${reunion03_code}
    Highlight heading  css=#action-form-reunion-edition-feuille_presence
    Capture and crop page screenshot  screenshots/suivi/reunions-action-edition-feuille_presence-link.png
    ...    css=#action-form-reunion-edition-feuille_presence
    # Click On Form Portlet Action    reunion    edition-feuille_presence  new_window
    # Open PDF  ${OM_PDF_TITLE}
    # Bootstrap jQuery
    # Capture and crop page screenshot  screenshots/suivi/reunions-action-edition-feuille_presence-view.png
    # ...    css=body
    # Close Current Window
    #
    Depuis le contexte de la réunion    ${reunion03_code}
    Highlight heading  css=#action-form-reunion-edition-ordre_du_jour
    Capture and crop page screenshot  screenshots/suivi/reunions-action-edition-ordre_du_jour-link.png
    ...    css=#action-form-reunion-edition-ordre_du_jour
    # Click On Form Portlet Action    reunion    edition-ordre_du_jour  new_window
    # Open PDF  ${OM_PDF_TITLE}
    # Bootstrap jQuery
    # Capture and crop page screenshot  screenshots/suivi/reunions-action-edition-ordre_du_jour-view.png
    # ...    css=body
    # Close Current Window
    #
    Depuis le contexte de la réunion    ${reunion03_code}
    Highlight heading  css=#action-form-reunion-edition-compte_rendu_global
    Capture and crop page screenshot  screenshots/suivi/reunions-action-edition-compte_rendu_global-link.png
    ...    css=#action-form-reunion-edition-compte_rendu_global
    # Click On Form Portlet Action    reunion    edition-compte_rendu_global  new_window
    # Open PDF  ${OM_PDF_TITLE}
    # Bootstrap jQuery
    # Capture and crop page screenshot  screenshots/suivi/reunions-action-edition-compte_rendu_global-view.png
    # ...    css=body
    # Close Current Window
    #
    Depuis le contexte de la réunion    ${reunion03_code}
    Highlight heading  css=#action-form-reunion-edition-compte_rendu_specifique
    Capture and crop page screenshot  screenshots/suivi/reunions-action-edition-compte_rendu_specifique-link.png
    ...    css=#action-form-reunion-edition-compte_rendu_specifique
    # Click On Form Portlet Action    reunion    edition-compte_rendu_specifique  new_window
    # Open PDF  ${OM_PDF_TITLE}
    # Bootstrap jQuery
    # Capture and crop page screenshot  screenshots/suivi/reunions-action-edition-compte_rendu_specifique-view.png
    # ...    css=body
    # Close Current Window
    #
    Depuis le contexte de la réunion    ${reunion03_code}
    Highlight heading  css=#action-form-reunion-supprimer
    Capture and crop page screenshot  screenshots/suivi/reunions-action-supprimer-link.png
    ...    css=#action-form-reunion-supprimer

    #
    Depuis le contexte de la réunion    ${reunion02_code}
    Highlight heading  css=#action-form-reunion-cloturer
    Capture and crop page screenshot  screenshots/suivi/reunions-action-cloturer-link.png
    ...    css=#action-form-reunion-cloturer
    Click On Form Portlet Action    reunion    cloturer
    Capture and crop page screenshot  screenshots/suivi/reunions-action-cloturer-view.png
    ...    css=#content
    #
    Clôturer la réunion    ${reunion02_code}
    #
    Depuis le contexte de la réunion    ${reunion02_code}
    Highlight heading  css=#action-form-reunion-integrer-documents-numerises
    Capture and crop page screenshot  screenshots/suivi/reunions-action-integrer-documents-numerises-link.png
    ...    css=#action-form-reunion-integrer-documents-numerises
    Click On Form Portlet Action    reunion    integrer-documents-numerises
    Capture and crop page screenshot  screenshots/suivi/reunions-action-integrer-documents-numerises-view.png
    ...    css=#content

    #
    Depuis le contexte de la programmation    ${programmation_8_code}
    Highlight heading  css=#action-form-programmation-envoyer_convoc_exploit
    Capture and crop page screenshot  screenshots/suivi/programmations-action-envoyer_convoc_exploit-link.png
    ...    css=#action-form-programmation-envoyer_convoc_exploit

    #
    Depuis le contexte de la programmation    ${programmation_8_code}
    Highlight heading  css=#action-form-programmation-envoyer_part
    Capture and crop page screenshot  screenshots/suivi/programmations-action-envoyer_part-link.png
    ...    css=#action-form-programmation-envoyer_part
    #
    Depuis le contexte de la programmation    ${programmation_8_code}
    Highlight heading  css=#action-form-programmation-edition-programmation_planning
    Capture and crop page screenshot  screenshots/suivi/programmations-action-view_convoc_membres-link.png
    ...    css=#action-form-programmation-edition-programmation_planning

    #
    Depuis le contexte de la programmation    ${programmation_9_code}
    Capture and crop page screenshot  screenshots/suivi/programmations-fiche.png
    ...    css=#content
    Highlight heading  css=#action-form-programmation-programmer
    Capture and crop page screenshot  screenshots/suivi/programmations-action-programmer-link.png
    ...    css=#action-form-programmation-programmer
    Click On Form Portlet Action    programmation    programmer
    Capture and crop page screenshot  screenshots/suivi/programmations-action-programmer-view.png
    ...    css=#content

    Activer le plugin geoaria_tests et l'option sig
    Depuis le contexte de la programmation    ${programmation_9_code}
    Click On Form Portlet Action    programmation    programmer
    Click Button    id=btn_proches
    Wait Until Element Is Visible  css=div.bloc_filtre_visite_etablissement_proches_fields
    Highlight heading  css=#form-content:first-child
    Capture and crop page screenshot  screenshots/suivi/programmations-action-programmer-view-etab_proche.png
    ...    css=#content
    Désactiver le plugin geoaria_tests et l'option sig

    #
    Depuis le listing des programmations
    Capture and crop page screenshot  screenshots/suivi/programmations-listing.png
    ...    css=#content
    Click On Add Button
    Capture and crop page screenshot  screenshots/suivi/programmations-form-ajouter.png
    ...    css=#content

    # START / Les documents entrants

    #
    Depuis la page d'accueil    admin    admin

    #
    Depuis le listing des documents entrants bannette
    Capture and crop page screenshot  screenshots/suivi/piece_bannette-listing.png
    ...    css=#content
    Click On Add Button
    Capture and crop page screenshot  screenshots/suivi/piece-form-ajouter.png
    ...    css=#content

    #
    Depuis le listing des documents entrants suivi
    Capture and crop page screenshot  screenshots/suivi/piece_suivi-listing.png
    ...    css=#content

    #
    Depuis le listing des documents entrants à valider
    Capture and crop page screenshot  screenshots/suivi/piece_a_valider-listing.png
    ...    css=#content

    #
    Depuis le contexte d'une document entrant à valider    Document entrant 7
    Capture and crop page screenshot  screenshots/suivi/piece-fiche.png
    ...    css=#content
    Highlight heading  css=#action-form-piece_a_valider-lu
    Capture and crop page screenshot  screenshots/suivi/piece-action-lu-link.png
    ...    css=#action-form-piece_a_valider-lu
    Marquer comme lu un dossier entrant    piece_a_valider
    Highlight heading  css=#action-form-piece_a_valider-non_lu
    Capture and crop page screenshot  screenshots/suivi/piece-action-non_lu-link.png
    ...    css=#action-form-piece_a_valider-non_lu
    Marquer comme non lu un dossier entrant    piece_a_valider
    Marquer comme non suivi un dossier entrant    piece_a_valider
    Highlight heading  css=#action-form-piece_a_valider-suivi
    Capture and crop page screenshot  screenshots/suivi/piece-action-suivi-link.png
    ...    css=#action-form-piece_a_valider-suivi
    Marquer comme suivi un dossier entrant    piece_a_valider
    Highlight heading  css=#action-form-piece_a_valider-non_suivi
    Capture and crop page screenshot  screenshots/suivi/piece-action-non_suivi-link.png
    ...    css=#action-form-piece_a_valider-non_suivi
    Highlight heading  css=#action-form-piece_a_valider-valide
    Capture and crop page screenshot  screenshots/suivi/piece-action-valide-link.png
    ...    css=#action-form-piece_a_valider-valide

    #
    Depuis le formulaire de modification du dossier d'instruction    VPS-VISIT-000003-SI
    Select From List By Label    css=#technicien    Paul DURAND
    # On valide le formulaire
    Click On Submit Button
    # On vérifie le message affiché à l'utilisateur
    Valid Message Should Be    Vos modifications ont bien été enregistrées.

    #
    Depuis la page d'accueil    technicien-si    technicien-si

    #
    Depuis le listing des documents entrants non lu
    Capture and crop page screenshot  screenshots/suivi/piece_non_lu-listing.png
    ...    css=#content

    # END / Les documents entrants

    # START / Les documents générés

    #
    Depuis la page d'accueil    admin    admin

    #
    Depuis le contexte du document généré    100000000001
    Capture and crop page screenshot  screenshots/suivi/courrier-fiche.png
    ...    css=#content

    #
    Highlight heading  css=#action-form-courrier-previsualiser
    Capture and crop page screenshot  screenshots/suivi/courrier-action-previsualiser-link.png
    ...    css=#action-form-courrier-previsualiser
    Clear highlight  css=#action-form-courrier-previsualiser

    #
    Highlight heading  css=#action-form-courrier-finalise
    Capture and crop page screenshot  screenshots/suivi/courrier-action-finalise-link.png
    ...    css=#action-form-courrier-finalise

    #
    Finaliser le document généré    100000000001
    Highlight heading  css=#action-form-courrier-definalise
    Capture and crop page screenshot  screenshots/suivi/courrier-action-definalise-link.png
    ...    css=#action-form-courrier-definalise

    #
    Depuis le contexte du document généré    100000000004
    Capture and crop page screenshot  screenshots/suivi/courrier_enfant-fiche.png
    ...    css=#content

    #
    Depuis le contexte du document généré    100000000002
    Capture and crop page screenshot  screenshots/suivi/courrier-fiche.png
    ...    css=#content

    #
    Depuis le formulaire de modification du document généré    100000000004
    Capture and crop page screenshot  screenshots/suivi/courrier_valide-form-modifier.png
    ...    css=#content

    #
    Depuis le formulaire de modification du document généré    100000000002
    Capture and crop page screenshot  screenshots/suivi/courrier_devalide-form-modifier.png
    ...    css=#content
    WUX  Depuis la liste des textes types    courrier_texte_type_complement1
    Capture and crop page screenshot  screenshots/suivi/piece_texte_type-listing.png
    ...    css=#form-courrier_texte_type-overlay
    Click Element    css=.ui-dialog-titlebar-close

    #
    Depuis le listing des documents générés du menu suivi
    Capture and crop page screenshot  screenshots/suivi/courrier-listing.png
    ...    css=#content

    #
    Depuis le listing  courrier_a_editer
    Capture and crop page screenshot  screenshots/suivi/courrier_a_editer-listing.png
    ...    css=#content

    #
    Depuis le listing  courrier_attente_signature
    Capture and crop page screenshot  screenshots/suivi/courrier_attente_signature-listing.png
    ...    css=#content

    #
    Depuis le listing  courrier_attente_retour_ar
    Capture and crop page screenshot  screenshots/suivi/courrier_attente_retour_ar-listing.png
    ...    css=#content

    #
    Depuis l'onglet document généré de l'établissement    T1302    LYCEE ALEXANDRE DUMAS
    Capture and crop page screenshot  screenshots/suivi/courrier_dossier_instruction-listing.png
    ...    css=#content

    #
    Depuis l'onglet document généré du dossier de coordination    VPS-VISIT-000019
    Capture and crop page screenshot  screenshots/suivi/courrier_dossier_coordination-listing.png
    ...    css=#content

    #
    Depuis l'onglet document généré du dossier d'instruction    VPS-VISIT-000019-SI
    Capture and crop page screenshot  screenshots/suivi/courrier_etablissement-listing.png
    ...    css=#content
    Click On Add Button
    Capture and crop page screenshot  screenshots/suivi/courrier-form-ajouter.png
    ...    css=#content

    #
    Depuis le formulaire du suivi par code barres
    Capture and crop page screenshot  screenshots/suivi/courrier_suivi-form.png
    ...    css=#content

    #
    Depuis le formulaire de l'impression des éditions RAR
    Capture and crop page screenshot  screenshots/suivi/courrier_rar-form.png
    ...    css=#content

    #
    Depuis le listing des contacts institutionnels
    Capture and crop page screenshot  screenshots/suivi/contact_institutionnel-listing.png
    ...    css=#content
    Click On Add Button
    Capture and crop page screenshot  screenshots/suivi/contact_institutionnel-form-ajouter.png
    ...    css=#content
    Capture and crop page screenshot  screenshots/suivi/contact_institutionnel-form-bloc-informations-particulier.png
    ...    css=#fieldset-form-contact_institutionnel-informations
    Select From List By Label  css=#qualite  personne morale
    Capture and crop page screenshot  screenshots/suivi/contact_institutionnel-form-bloc-informations-personne_morale.png
    ...    css=#fieldset-form-contact_institutionnel-informations

    #
    Depuis le contexte d'un contact institutionnel     Doe    M. Doe John
    Capture and crop page screenshot  screenshots/suivi/contact_institutionnel-fiche.png
    ...    css=#content

    # END / Les documents générés

    ####
    # BEGIN / Pilotage
    ####

    # Statisitiques #

    Depuis la page d'accueil    cadre-si    cadre-si
    # On ouvre la vue statistiques
    Go To Submenu In Menu    suivi    statistiques
    # Screen : État des lieux des éléments réalisés sur l'année courante (Visites)
    Capture and crop page screenshot  screenshots/suivi/etat_des_lieux_visites.png
    ...    css=div#dashboard > div.container-fluid > div.row:nth-child(2) > div:nth-child(1)
    # Screen : État des lieux des éléments réalisés sur l'année courante (Plans)
    Capture and crop page screenshot  screenshots/suivi/etat_des_lieux_plans.png
    ...    css=div#dashboard > div.container-fluid > div.row:nth-child(2) > div:nth-child(2)
    # Screen : État des lieux des éléments en cours (dossiers)
    Capture and crop page screenshot  screenshots/suivi/etat_des_lieux_dossiers.png
    ...    css=div#dashboard > div.container-fluid > div.col2 > div:nth-child(1)
    # Screen : État des lieux des éléments en cours (délais)
    Capture and crop page screenshot  screenshots/suivi/etat_des_lieux_delais.png
    ...    css=div#dashboard > div.container-fluid > div.col2 > div:nth-child(2)
    # Screen : État des lieux des éléments en cours (éditions)
    Capture and crop page screenshot  screenshots/suivi/etat_des_lieux_editions.png
    ...    css=div#dashboard > div.container-fluid > div.row:nth-child(8) > div

    # Requêtes mémorisées #

    Depuis la page d'accueil    cadre-si    cadre-si
    # On ouvre la vue statistiques
    Go To Submenu In Menu    suivi    reqmo
    # Screen : choix de la requête mémorisée
    Capture and crop page screenshot  screenshots/suivi/choix_requete_memorisee.png
    ...    css=div#reqmo-list fieldset

    ####
    # END / Pilotage
    ####

Section 'Administration'
    [Documentation]  L'objet de ce 'Test Case' est de réaliser les captures d'écran
    ...    à destination de la documentation.
    [Tags]  doc

    ####
    # START / Administration et paramétrage
    ####

    Depuis la page d'accueil  admin  admin
    Activer le plugin geoaria_tests et l'option sig
    Go To  ${PROJECT_URL}app/index.php?module=settings
    Page Should Contain  géolocalisation
    Go To  ${PROJECT_URL}${OM_ROUTE_FORM}&obj=dossier_coordination_geocoder_tous&action=260&idx=0
        Capture and Crop Page Screenshot  screenshots/administration_parametrage/administration_geocoder-tous.png
    ...    css=#content
    Click On Submit Button
    Capture and Crop Page Screenshot  screenshots/administration_parametrage/administration_geocoder-tous-success.png
    ...    css=.message.ui-widget.ui-corner-all.ui-state-highlight.ui-state-valid
    # Remise dans l'état initial
    Désactiver le plugin geoaria_tests et l'option sig

    ## PERIODICITE DES VISITES
    Go To  ${PROJECT_URL}${OM_ROUTE_TAB}&obj=periodicite_visites
    Capture and Crop Page Screenshot  screenshots/administration_parametrage/administration-periodicite_visites-listing.png
    ...  css=#content
    Go To  ${PROJECT_URL}${OM_ROUTE_FORM}&obj=periodicite_visites&action=0&retour=form
    Capture and Crop Page Screenshot  screenshots/administration_parametrage/administration-periodicite_visites-form-ajout.png
    ...  css=#content
    Go To  ${PROJECT_URL}${OM_ROUTE_FORM}&obj=periodicite_visites&action=99&idx=0
    Capture and Crop Page Screenshot  screenshots/administration_parametrage/administration-periodicite_visites-controlpanel.png
    ...  css=#content

    ####
    # END / Administration et paramétrage
    ####
