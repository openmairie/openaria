*** Settings ***
Resource  resources/resources.robot
Suite Setup  For Suite Setup
Suite Teardown  For Suite Teardown
Documentation  Teste la fonctionnalité SWROD : interface de consultation et de
...  téléchargement des documents du guichet unique en lecture seule.


*** Test Cases ***
Constitution d'un jeu de données

    [Documentation]  L'objet de ce 'Test Case' est de constituer un jeu de de données
    ...  cohérent pour les scénarios fonctionnels qui suivent.

    #
    Depuis la page d'accueil  admin  admin

    #
    &{etab01} =  Create Dictionary
    ...  libelle=ETABLISSEMENT SWROD
    ...  etablissement_nature=ERP Référentiel
    ...  etablissement_type=M
    ...  etablissement_categorie=1
    ...  si_autorite_competente_visite=Commission communale de sécurité
    ...  adresse_numero=42
    ...  adresse_voie=RUE DE ROME
    ...  adresse_cp=13006
    ...  adresse_ville=MARSEILLE
    ...  adresse_arrondissement=6ème
    ...  siret=73282932000074
    ...  exp_civilite=Mme
    ...  exp_nom=Létourneau
    ...  exp_prenom=Melisande
    ${etab01_code} =  Ajouter l'établissement  ${etab01}
    ${etab01_titre} =  Set Variable  ${etab01_code} - ${etab01.libelle}
    Set Suite Variable  ${etab01}
    Set Suite Variable  ${etab01_code}
    Set Suite Variable  ${etab01_titre}


Intégration de l'onglet 'Guichet Unique' dans l'onglet 'Documents Entrants' du DC et du DI

    [Documentation]  Le double onglet 'Interne' / 'Guichet Unique' apparaît
    ...  dans les cas suivants sinon c'est le listing 'Documents Entrants'
    ...  standard qui apparaît.
    ...  les conditions sont identiques sur le DC et sur le DI
    ...  - l'option SWROD est activée
    ...  - ET :
    ...     * le DC contient un dossier d'autorisation ADS
    ...     * OU le DC contient un dossier d'instruction ADS

    #
    # Constitution du jeu de données spécifique au TestCase
    #
    # On ajoute le dossier déjà qualifié
    #
    &{dc01} =  Create Dictionary
    ...  dossier_coordination_type=Permis de construire
    ...  description=Dossier de coordination concernant un permis de construire
    ...  etablissement=${etab01_titre}
    ...  date_butoir=${DATE_FORMAT_DD/MM/YYYY}
    ...  a_qualifier=false
    ...  dossier_instruction_secu=true
    ...  dossier_instruction_acc=true
    ${dc01_libelle} =  Ajouter le dossier de coordination  ${dc01}
    Set Suite Variable    ${dc01}
    Set Suite Variable    ${dc01_libelle}
    ${di_1_si_libelle} =  Catenate  SEPARATOR=  ${dc01_libelle}  -SI
    ${di_1_acc_libelle} =  Catenate  SEPARATOR=  ${dc01_libelle}  -ACC

    #
    # L'option n'est pas activée ET aucune info n'est saisie dans le dc
    #
    #
    Depuis l'onglet document entrant du dossier de coordination  ${dc01_libelle}
    Element Should Not Contain  css=#formulaire  Guichet Unique
    #
    Depuis l'onglet document entrant du dossier d'instruction  ${di_1_si_libelle}
    Element Should Not Contain  css=#formulaire  Guichet Unique

    #
    # Activation du plugin
    #
    Activer le plugin swrodaria_tests et l'option

    #
    # L'option est activée ET aucune info n'est saisie dans le dc
    #
    #
    Depuis l'onglet document entrant du dossier de coordination  ${dc01_libelle}
    Element Should Not Contain  css=#formulaire  Guichet Unique
    #
    Depuis l'onglet document entrant du dossier d'instruction  ${di_1_si_libelle}
    Element Should Not Contain  css=#formulaire  Guichet Unique

    #
    # Saisie d'une référence ADS sans documents
    #
    Depuis le formulaire de modification du dossier de coordination  ${dc01_libelle}
    Input Text  css=#dossier_autorisation_ads  AZERTY
    Input Text  css=#dossier_instruction_ads  ${EMPTY}
    Click On Submit Button
    Valid Message Should Contain    Vos modifications ont bien été enregistrées.

    #
    # L'option est activée ET une info est saisie dans le dc
    #
    #
    Depuis l'onglet document entrant du dossier de coordination  ${dc01_libelle}
    Element Should Contain  css=#onglet-document_entrant_guichet_unique__contexte_dc  Guichet Unique
    Click Element  css=#onglet-document_entrant_guichet_unique__contexte_dc
    WUX  Element Should Be Visible  css=#swrod p.noData
    WUX  Element Text Should Be  css=#swrod p.noData  Aucun enregistrement
    #
    Depuis l'onglet document entrant du dossier d'instruction  ${di_1_si_libelle}
    Element Should Contain  css=#onglet-document_entrant_guichet_unique__contexte_di  Guichet Unique
    Click Element  css=#onglet-document_entrant_guichet_unique__contexte_di
    WUX  Element Should Be Visible  css=#swrod p.noData
    WUX  Element Text Should Be  css=#swrod p.noData  Aucun enregistrement


    #
    # Saisie d'une référence DI ADS
    #
    Depuis le formulaire de modification du dossier de coordination  ${dc01_libelle}
    Input Text  css=#dossier_autorisation_ads  ${EMPTY}
    Input Text  css=#dossier_instruction_ads  PC0130551200001M01
    Click On Submit Button
    Valid Message Should Contain    Vos modifications ont bien été enregistrées.

    #
    # L'option est activée ET une info est saisie dans le dc (di ads)
    #
    #
    Depuis l'onglet document entrant du dossier de coordination  ${dc01_libelle}
    Element Should Contain  css=#onglet-document_entrant_guichet_unique__contexte_dc  Guichet Unique
    Click Element  css=#onglet-document_entrant_guichet_unique__contexte_dc
    WUX  Element Should Be Visible  css=#swrod table.document_numerise
    WUX  Element Should Contain  css=#swrod  PC0130551200001M01
    WUX  Element Should Contain  css=#swrod  20140912ARR-02.pdf
    WUX  Element Should Not Contain  css=#swrod  PC0130551200001P0
    WUX  Element Should Not Contain  css=#swrod  20140312ARR.pdf
    WUX  Element Should Contain  css=#swrod  arrêté de conformité

    #
    Depuis l'onglet document entrant du dossier d'instruction  ${di_1_si_libelle}
    Element Should Contain  css=#onglet-document_entrant_guichet_unique__contexte_di  Guichet Unique
    Click Element  css=#onglet-document_entrant_guichet_unique__contexte_di
    WUX  Element Should Be Visible  css=#swrod table.document_numerise
    WUX  Element Should Contain  css=#swrod  PC0130551200001M01
    WUX  Element Should Contain  css=#swrod  20140912ARR-02.pdf
    WUX  Element Should Not Contain  css=#swrod  PC0130551200001P0
    WUX  Element Should Not Contain  css=#swrod  20140312ARR.pdf
    WUX  Element Should Contain  css=#swrod  arrêté de conformité
    #
    # Saisie d'une référence DC ADS
    #
    Depuis le formulaire de modification du dossier de coordination  ${dc01_libelle}
    Input Text  css=#dossier_autorisation_ads  PC0130551200001
    Input Text  css=#dossier_instruction_ads  PC0130551200001M01
    Click On Submit Button
    Valid Message Should Contain    Vos modifications ont bien été enregistrées.

    #
    # L'option est activée ET une info est saisie dans le dc (dc ads)
    #
    #
    Depuis l'onglet document entrant du dossier de coordination  ${dc01_libelle}
    Element Should Contain  css=#onglet-document_entrant_guichet_unique__contexte_dc  Guichet Unique
    Click Element  css=#onglet-document_entrant_guichet_unique__contexte_dc
    WUX  Element Should Be Visible  css=#swrod table.document_numerise
    WUX  Element Should Contain  css=#swrod  PC0130551200001M01
    WUX  Element Should Contain  css=#swrod  20140912ARR-02.pdf
    WUX  Element Should Contain  css=#swrod  PC0130551200001P0
    WUX  Element Should Contain  css=#swrod  20140312ARR.pdf
    WUX  Element Should Contain  css=#swrod  arrêté de conformité

    #
    Depuis l'onglet document entrant du dossier d'instruction  ${di_1_si_libelle}
    Element Should Contain  css=#onglet-document_entrant_guichet_unique__contexte_di  Guichet Unique
    Click Element  css=#onglet-document_entrant_guichet_unique__contexte_di
    WUX  Element Should Be Visible  css=#swrod table.document_numerise
    WUX  Element Should Contain  css=#swrod  PC0130551200001M01
    WUX  Element Should Contain  css=#swrod  20140912ARR-02.pdf
    WUX  Element Should Contain  css=#swrod  PC0130551200001P0
    WUX  Element Should Contain  css=#swrod  20140312ARR.pdf
    WUX  Element Should Contain  css=#swrod  arrêté de conformité


    #
    # Désactivation du plugin
    #
    Désactiver le plugin swrodaria_tests et l'option

    #
    # L'option est désactivée ET une info est saisie dans le dc
    #
    #
    Depuis l'onglet document entrant du dossier de coordination  ${dc01_libelle}
    Element Should Not Contain  css=#formulaire  Guichet Unique
    #
    Depuis l'onglet document entrant du dossier d'instruction  ${di_1_si_libelle}
    Element Should Not Contain  css=#formulaire  Guichet Unique


