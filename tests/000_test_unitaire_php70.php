<?php
/**
 * Ce script contient la définition de la classe 'GeneralPHP70Test'.
 *
 * @package openaria
 * @version SVN : $Id$
 */

require_once "000_test_unitaire_common.php";
final class GeneralPHP70Test extends GeneralCommon {
    public function setUp() {
        $this->common_setUp();
    }
    public function tearDown() {
        $this->common_tearDown();
    }
    public function onNotSuccessfulTest(Throwable $e) {
        $this->common_onNotSuccessfulTest($e);
    }
}
