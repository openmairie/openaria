--
-- PostgreSQL database dump
--

-- Dumped from database version 9.5.10
-- Dumped by pg_dump version 9.5.10

-- SET statement_timeout = 0;
-- SET lock_timeout = 0;
-- SET client_encoding = 'UTF8';
-- SET standard_conforming_strings = on;
-- SET check_function_bodies = false;
-- SET client_min_messages = warning;
-- SET row_security = off;

-- SET search_path = openaria, pg_catalog;

--
-- Data for Name: om_requete; Type: TABLE DATA; Schema: openaria; Owner: -
--

INSERT INTO om_requete (om_requete, code, libelle, description, requete, merge_fields, type, classe, methode) VALUES (4, 'reunion', 'Contexte ''réunion''', 'Tous les champs de fusion spcifiques aux réunions', '', '', 'objet', 'reunion', NULL);
INSERT INTO om_requete (om_requete, code, libelle, description, requete, merge_fields, type, classe, methode) VALUES (1, 'aucune', 'Aucune REQUÊTE', 'A sélectionner lorsqu''aucune requête n''est nécessaire pour la composition de l''édition', 'SELECT '''' as aucune_valeur FROM &DB_PREFIXEom_collectivite', '[aucune_valeur]', 'sql', NULL, NULL);
INSERT INTO om_requete (om_requete, code, libelle, description, requete, merge_fields, type, classe, methode) VALUES (7, 'programmation', 'Contexte ''programmation''', 'Tous les champs de fusion spécifiques aux programmations de visites', '', '', 'objet', 'programmation', NULL);
INSERT INTO om_requete (om_requete, code, libelle, description, requete, merge_fields, type, classe, methode) VALUES (12, 'document_genere', 'Contexte ''document genere''', 'Tous les champs de fusion spécifiques à une analyse, son DI, son DC et son établissement s''il existe.', '', '', 'objet', 'courrier', 'get_all_merge_fields');
INSERT INTO om_requete (om_requete, code, libelle, description, requete, merge_fields, type, classe, methode) VALUES (2, 'analyses', 'Contexte ''analyse / DI''', 'Tous les champs de fusion spécifiques à une analyse, son DI, son DC et son établissement s''il existe.', '', '', 'objet', 'analyses', 'get_all_merge_fields');
INSERT INTO om_requete (om_requete, code, libelle, description, requete, merge_fields, type, classe, methode) VALUES (5, 'dossier_instruction_reunion', 'Contexte ''passage en réunion / DI''', 'Tous les champs de fusion spécifiques aux demandes de passage en réunion', '', '', 'objet', 'dossier_instruction_reunion', 'get_all_merge_fields');
INSERT INTO om_requete (om_requete, code, libelle, description, requete, merge_fields, type, classe, methode) VALUES (6, 'proces_verbal', 'Contexte ''procès-verbal / DI''', 'Tous les champs de fusion de la requête Analyse, ainsi que ceux du PV', '', '', 'objet', 'courrier', 'get_all_merge_fields_for_proces_verbal');
INSERT INTO om_requete (om_requete, code, libelle, description, requete, merge_fields, type, classe, methode) VALUES (11, 'visite', 'Contexte ''visite''', 'Tous les champs de fusion spécifiques aux visites', '', '', 'objet', 'courrier', 'get_all_merge_fields_for_visite');


--
-- Data for Name: om_etat; Type: TABLE DATA; Schema: openaria; Owner: -
--



--
-- Name: om_etat_seq; Type: SEQUENCE SET; Schema: openaria; Owner: -
--

SELECT pg_catalog.setval('om_etat_seq', 1, false);


--
-- Data for Name: om_lettretype; Type: TABLE DATA; Schema: openaria; Owner: -
--

INSERT INTO om_lettretype (om_lettretype, om_collectivite, id, libelle, actif, orientation, format, logo, logoleft, logotop, titre_om_htmletat, titreleft, titretop, titrelargeur, titrehauteur, titrebordure, corps_om_htmletatex, om_sql, margeleft, margetop, margeright, margebottom, se_font, se_couleurtexte, header_om_htmletat, header_offset, footer_om_htmletat, footer_offset) VALUES (101, 1, 'lt_courrier_standard_etab', 'LT - Courrier standard - Établissement', true, 'P', 'A4', NULL, 10, 10, '
<p><span style=''font-weight: bold;''>Établissement</span></p>
<p>Établissement : [etablissement.code] - [etablissement.libelle]</p>
', 10, 16, 0, 10, '0', '<p>...</p>', 12, 10, 10, 10, 10, 'helvetica', '0-0-0', NULL, 0, '<p style="text-align:center;font-size:8pt;"><em>Page &numpage/&nbpages</em></p>', 12);
INSERT INTO om_lettretype (om_lettretype, om_collectivite, id, libelle, actif, orientation, format, logo, logoleft, logotop, titre_om_htmletat, titreleft, titretop, titrelargeur, titrehauteur, titrebordure, corps_om_htmletatex, om_sql, margeleft, margetop, margeright, margebottom, se_font, se_couleurtexte, header_om_htmletat, header_offset, footer_om_htmletat, footer_offset) VALUES (102, 1, 'lt_courrier_standard_dc', 'LT - Courrier standard - Dossier de coordination', true, 'P', 'A4', NULL, 10, 10, '
<p><span style=''font-weight: bold;''>Dossier de Coordination</span></p>
<p>Établissement : [etablissement.code] - [etablissement.libelle]</p>
<p>DC : [dossier_coordination.libelle]</p>
', 10, 16, 0, 10, '0', '<p>...</p>', 12, 10, 10, 10, 10, 'helvetica', '0-0-0', NULL, 0, '<p style="text-align:center;font-size:8pt;"><em>Page &numpage/&nbpages</em></p>', 12);
INSERT INTO om_lettretype (om_lettretype, om_collectivite, id, libelle, actif, orientation, format, logo, logoleft, logotop, titre_om_htmletat, titreleft, titretop, titrelargeur, titrehauteur, titrebordure, corps_om_htmletatex, om_sql, margeleft, margetop, margeright, margebottom, se_font, se_couleurtexte, header_om_htmletat, header_offset, footer_om_htmletat, footer_offset) VALUES (103, 1, 'lt_courrier_standard_di', 'LT - Courrier standard - Dossier d''instruction', true, 'P', 'A4', NULL, 10, 10, '
<p><span style=''font-weight: bold;''>Dossier d''Instruction</span></p>
<p>Établissement : [etablissement.code] - [etablissement.libelle]</p>
<p>DC : [dossier_coordination.libelle]</p>
<p>DI : [dossier_instruction.libelle]</p>
', 10, 16, 0, 10, '0', '<p>...</p>', 12, 10, 10, 10, 10, 'helvetica', '0-0-0', NULL, 0, '<p style="text-align:center;font-size:8pt;"><em>Page &numpage/&nbpages</em></p>', 12);
INSERT INTO om_lettretype (om_lettretype, om_collectivite, id, libelle, actif, orientation, format, logo, logoleft, logotop, titre_om_htmletat, titreleft, titretop, titrelargeur, titrehauteur, titrebordure, corps_om_htmletatex, om_sql, margeleft, margetop, margeright, margebottom, se_font, se_couleurtexte, header_om_htmletat, header_offset, footer_om_htmletat, footer_offset) VALUES (10, 1, 'reunion_ordre_du_jour', 'Réunion - Ordre du jour', true, 'L', 'A4', NULL, 10, 10, '<table>
<tbody>
<tr>
<td>
<p>[reunion.lieu_salle]</p>
<p>[reunion.lieu_adresse_ligne1]</p>
<p>[reunion.lieu_adresse_ligne2]</p>
</td>
<td>
<p style=''text-align: right;''><span class=''mce_maj''>[reunion.libelle]</span></p>
<p style=''text-align: right;''> </p>
<p style=''text-align: right;''><span style=''font-weight: bold;''>ORDRE DU JOUR DE LA RÉUNION DU [reunion.date_reunion] À [reunion.heure_reunion]</span></p>
</td>
</tr>
</tbody>
</table>', 10, 16, 0, 10, '0', '<p><span id=''reu_odj'' class=''mce_sousetat''>Réunion - Ordre du jour</span></p>
<p> </p>', 4, 10, 10, 10, 10, 'helvetica', '0-0-0', NULL, 0, '<p style="text-align:center;font-size:8pt;"><em>Page &numpage/&nbpages</em></p>', 12);
INSERT INTO om_lettretype (om_lettretype, om_collectivite, id, libelle, actif, orientation, format, logo, logoleft, logotop, titre_om_htmletat, titreleft, titretop, titrelargeur, titrehauteur, titrebordure, corps_om_htmletatex, om_sql, margeleft, margetop, margeright, margebottom, se_font, se_couleurtexte, header_om_htmletat, header_offset, footer_om_htmletat, footer_offset) VALUES (14, 1, 'reunion_ordre_du_jour_cca', 'Réunion - Ordre du jour CCA', true, 'L', 'A4', NULL, 10, 10, '<table>
<tbody>
<tr>
<td>
<p>[reunion.lieu_salle]</p>
<p>[reunion.lieu_adresse_ligne1]</p>
<p>[reunion.lieu_adresse_ligne2]</p>
</td>
<td>
<p style=''text-align: right;''><span class=''mce_maj''>[reunion.libelle]</span></p>
<p style=''text-align: right;''> </p>
<p style=''text-align: right;''><span style=''font-weight: bold;''>ORDRE DU JOUR DE LA RÉUNION DU [reunion.date_reunion] À [reunion.heure_reunion]</span></p>
</td>
</tr>
</tbody>
</table>', 10, 16, 0, 10, '0', '<p><span id=''reu_odj_cca'' class=''mce_sousetat''>Réunion - Ordre du jour CCA</span></p>
<p> </p>', 4, 10, 10, 10, 10, 'helvetica', '0-0-0', NULL, 0, '<p style="text-align:center;font-size:8pt;"><em>Page &numpage/&nbpages</em></p>', 12);
INSERT INTO om_lettretype (om_lettretype, om_collectivite, id, libelle, actif, orientation, format, logo, logoleft, logotop, titre_om_htmletat, titreleft, titretop, titrelargeur, titrehauteur, titrebordure, corps_om_htmletatex, om_sql, margeleft, margetop, margeright, margebottom, se_font, se_couleurtexte, header_om_htmletat, header_offset, footer_om_htmletat, footer_offset) VALUES (15, 1, 'reunion_ordre_du_jour_ccs', 'Réunion - Ordre du jour CCS', true, 'L', 'A4', NULL, 10, 10, '<table>
<tbody>
<tr>
<td>
<p>[reunion.lieu_salle]</p>
<p>[reunion.lieu_adresse_ligne1]</p>
<p>[reunion.lieu_adresse_ligne2]</p>
</td>
<td>
<p style=''text-align: right;''><span class=''mce_maj''>[reunion.libelle]</span></p>
<p style=''text-align: right;''> </p>
<p style=''text-align: right;''><span style=''font-weight: bold;''>ORDRE DU JOUR DE LA RÉUNION DU [reunion.date_reunion] À [reunion.heure_reunion]</span></p>
</td>
</tr>
</tbody>
</table>', 10, 16, 0, 10, '0', '<p><span id=''reu_odj_ccs_plen_plans'' class=''mce_sousetat''>Réunion - Ordre du jour CCS - Plans</span></p>
<p> </p>
<p><span id=''reu_odj_ccs_plen_visites'' class=''mce_sousetat''>Réunion - Ordre du jour CCS - Visites</span></p>
<p> </p>
<p><span id=''reu_odj_ccs_plen_apdif'' class=''mce_sousetat''>Réunion - Ordre du jour CCS - Avis différés</span></p>
<p> </p>
<p><span id=''reu_odj_ccs_plen_apmed'' class=''mce_sousetat''>Réunion - Ordre du jour CCS - Mises en demeure</span></p>
<p> </p>
<p><span id=''reu_odj_ccs_plen_apscds'' class=''mce_sousetat''>Réunion - Ordre du jour CCS - Sous Commission Départementale</span></p>
<p> </p>
<p><span id=''reu_odj_ccs_plen_enjeux'' class=''mce_sousetat''>Réunion - Ordre du jour CCS - Enjeux</span></p>
<p> </p>', 4, 10, 10, 10, 10, 'helvetica', '0-0-0', NULL, 0, '<p style="text-align:center;font-size:8pt;"><em>Page &numpage/&nbpages</em></p>', 12);
INSERT INTO om_lettretype (om_lettretype, om_collectivite, id, libelle, actif, orientation, format, logo, logoleft, logotop, titre_om_htmletat, titreleft, titretop, titrelargeur, titrehauteur, titrebordure, corps_om_htmletatex, om_sql, margeleft, margetop, margeright, margebottom, se_font, se_couleurtexte, header_om_htmletat, header_offset, footer_om_htmletat, footer_offset) VALUES (1, 1, 'programmation_annulation_exploitant', 'Programmation - Lettre d''annulation de visite', true, 'P', 'A4', NULL, 10, 10, '<p>Lettre d''annulation de visite</p>', 109, 16, 0, 10, '0', '<p>Lettre d''annulation de visite</p>', 11, 0, 0, 0, 0, 'helvetica', '0-0-0', NULL, 0, '<p style="text-align:center;font-size:8pt;"><em>Page &numpage/&nbpages</em></p>', 12);
INSERT INTO om_lettretype (om_lettretype, om_collectivite, id, libelle, actif, orientation, format, logo, logoleft, logotop, titre_om_htmletat, titreleft, titretop, titrelargeur, titrehauteur, titrebordure, corps_om_htmletatex, om_sql, margeleft, margetop, margeright, margebottom, se_font, se_couleurtexte, header_om_htmletat, header_offset, footer_om_htmletat, footer_offset) VALUES (2, 1, 'programmation_planning', '(*) Programmation - Planning des visites', true, 'L', 'A4', NULL, 10, 10, 
	'
	<p style=''text-align: center;''><span style=''font-weight: bold; font-size: 12pt;''>PROGRAMMATION DES VISITES DE LA COMMISSION COMMUNALE <span class=''mce_maj''>[programmation.service]</span></span></p>
	<p style=''text-align: center;''>SEMAINE [programmation.semaine_annee] du [programmation.premier_jour_lettre] au [programmation.dernier_jour_lettre]</p>
	<p style=''text-align: center;''>(version : [programmation.version] / date de modification : [programmation.date_modification] / [programmation.nb_visites] visites)</p>
	',
10, 10, 0, 0, '1', '<p><span id=''prg_vis_pro'' class=''mce_sousetat''>Programmation - Liste des visites programmées</span></p>
<p> </p>
<p><br pagebreak=''true'' /><br /><span id=''prg_vis_his'' class=''mce_sousetat''>Programmation - Historique des visites programmées</span></p>
<p> </p>', 7, 10, 10, 10, 10, 'helvetica', '0-0-0', NULL, 0, '<p style="text-align:center;font-size:8pt;"><em>Page &numpage/&nbpages</em></p>', 12);
INSERT INTO om_lettretype (om_lettretype, om_collectivite, id, libelle, actif, orientation, format, logo, logoleft, logotop, titre_om_htmletat, titreleft, titretop, titrelargeur, titrehauteur, titrebordure, corps_om_htmletatex, om_sql, margeleft, margetop, margeright, margebottom, se_font, se_couleurtexte, header_om_htmletat, header_offset, footer_om_htmletat, footer_offset) VALUES (9, 1, 'proces_verbal_acc', 'PV ACC', true, 'P', 'A4', NULL, 10, 10, '<p style=''text-align: center;''><span class=''mce_maj'' style=''font-size: 14pt;''>Procès verbal - accessibilité<br /></span></p>', 7, 10, 0, 10, '0', '<p style=''text-align: center;''><span style=''font-weight: bold;''>Références</span></p>
<p> </p>
<p><span style=''font-weight: bold;''>* Procès-verbal</span></p>
<p>Numéro : [pv_numero]</p>
<p>Date de rédaction : [pv_date_redaction]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Signataire du PV</span></p>
<p>Civilité : [signataire_civilite]</p>
<p>Nom : [signataire_nom]</p>
<p>Prénom : [signataire_prenom]</p>
<p>Qualité : [signataire_qualite]</p>
<p>Signature : [signataire_signature]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Demande de passage en réunion rattachée au PV</span></p>
<p>Date de passage souhaitée : [reunion_date]</p>
<p>Type de réunion : [reunion_type]</p>
<p>Catégorie du type de réunion : [reunion_categorie]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Dosser d''instruction</span></p>
<p>Libellé : [di_lib]</p>
<p>Technicien en charge : [di_tech]</p>
<p>Autorité compétente : [di_ac]</p>
<p>À qualifier : [di_qualif]</p>
<p>Incomplétude : [di_incomp]</p>
<p>Clôturé : [di_clos]</p>
<p>Prioritaire : [di_prio]</p>
<p>Pièces attendues : [di_piece]</p>
<p>Description détaillée : [di_desc]</p>
<p>Commentaires d''instruction : [di_notes]</p>
<p>Statut : [di_statut]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Dossier de coordination</span></p>
<p>Libellé : [dc_lib]</p>
<p>Type : [dc_type]</p>
<p>Date de la demande : [dc_date_dmnd]</p>
<p>Date butoir : [dc_date_butr]</p>
<p>Dossier d''autorisation ADS : [dc_da_ads]</p>
<p>Dossier d''instruction ADS : [dc_di_ads]</p>
<p>À qualifier : [dc_qualif]</p>
<p>Clôturé : [dc_clos]</p>
<p>Contraintes urbaines : [dc_ctr_urb]</p>
<p>Dossier de coordination parent : [dc_parent]</p>
<p>Description : [dc_desc]</p>
<p>Autorité de police en cours : [dc_ap]</p>
<p>Références cadastrales : [dc_ref_cad]</p>
<p> </p>
<p><br /><span style=''font-weight: bold;''>* Établissement</span></p>
<p>Numéro T : [etab_code]</p>
<p>Libellé : [etab_lib]</p>
<p>Type : [etab_type]</p>
<p>Catégorie : [etab_cat]</p>
<p>Siret : [etab_siret]</p>
<p>Téléphone : [etab_tel]</p>
<p>Fax : [etab_fax]</p>
<p>Année de construction : [etab_annee_const]</p>
<p>Nature : [etab_nat]</p>
<p>Statut juridique : [etab_stat_jur]</p>
<p>Tutelle administrative : [etab_tut_adm]</p>
<p>État : [etab_etat]</p>
<p>Adresse : [etab_adresse]</p>
<p>Date d''arrêté d''ouverture : [etab_date_ar_ouv]</p>
<p>Exploitant : [etab_exp]</p>
<p>Références cadastrales : [etab_ref_cad]</p>
<p> </p>
<p style=''text-align: center;''><span style=''font-weight: bold;''>Analyse<br /></span></p>
<p><span style=''font-weight: bold;''>* Type d''analyse</span><br />[anls_type]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Objet</span></p>
<p>[anls_obj]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Descriptif de l''établissement</span></p>
<p>[anls_desc]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Classification de l''établissement</span></p>
<p>Type d''établissement : [dc_etab_type]</p>
<p>Type(s) secondaire(s) : [dc_etab_type_sec]</p>
<p>Catégorie d''établissement : [dc_etab_cat]</p>
<p>Établissement référentiel : [dc_etab_erp]</p>
<p>Locaux à sommeil : [dc_etab_loc_som]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Données techniques</span></p>
<p>Handicap mental : [dt_h_ment]</p>
<p>Handicap auditif : [dt_h_audtf]</p>
<p>Handicap physique : [dt_h_phsq]</p>
<p>Handicap visuel : [dt_h_visu]</p>
<p>Élévateur : [dt_elvtr]</p>
<p>Ascenseur : [dt_ascsr]</p>
<p>Douche : [dt_douche]</p>
<p>Boucle magnétique : [dt_bcl_mgnt]</p>
<p>Sanitaires : [dt_sanitaire]</p>
<p>Nb de places de stationnement aménagées : [dt_plc_stmnt_amng]</p>
<p>Nb de chambres aménagées : [dt_chmbr_amng]</p>
<p>Nb de places assises public : [dt_plc_ass_pub]</p>
<p>Dérogation SCDA : [dt_scda]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Réglementation applicable</span></p>
<p>[anls_reg_app]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Prescriptions<br /></span></p>
<p>[anls_presc]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Documents présentés lors des visites<br /></span></p>
<p>[anls_doc_pdnt]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Documents fournis après les visites</span></p>
<p>[anls_doc_apres]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Essais réalisés</span></p>
<p>[anls_essais]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Compte-rendu</span></p>
<p>[anls_cr]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Observation</span></p>
<p>[anls_obs]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Avis proposé</span></p>
<p>[anls_avis_prop]</p>
<p>[anls_avis_comp]</p>', 6, 5, 10, 5, 15, 'helvetica', '0-0-0', NULL, 0, '<p style="text-align:center;font-size:8pt;"><em>Page &numpage/&nbpages</em></p>', 12);
INSERT INTO om_lettretype (om_lettretype, om_collectivite, id, libelle, actif, orientation, format, logo, logoleft, logotop, titre_om_htmletat, titreleft, titretop, titrelargeur, titrehauteur, titrebordure, corps_om_htmletatex, om_sql, margeleft, margetop, margeright, margebottom, se_font, se_couleurtexte, header_om_htmletat, header_offset, footer_om_htmletat, footer_offset) VALUES (12, 1, 'reunion_feuille_de_presence', 'Réunion - Feuille de présence', true, 'P', 'A4', NULL, 10, 10, '<p style=''text-align: center;''><span style=''text-decoration: underline;''><em><span style=''font-weight: bold;''>FEUILLE DE PRÉSENCE</span></em></span></p>
<p style=''text-align: center;''> </p>
<p style=''text-align: center;''>[reunion.libelle]</p>
<p style=''text-align: center;''> </p>
<p style=''text-align: center;''>[reunion.date_reunion]</p>', 10, 16, 0, 10, '0', '<p><span id=''reu_fp'' class=''mce_sousetat''>Réunion - Feuille de présence</span></p>
<p> </p>', 4, 10, 10, 10, 10, 'helvetica', '0-0-0', NULL, 0, '<p style="text-align:center;font-size:8pt;"><em>Page &numpage/&nbpages</em></p>', 12);
INSERT INTO om_lettretype (om_lettretype, om_collectivite, id, libelle, actif, orientation, format, logo, logoleft, logotop, titre_om_htmletat, titreleft, titretop, titrelargeur, titrehauteur, titrebordure, corps_om_htmletatex, om_sql, margeleft, margetop, margeright, margebottom, se_font, se_couleurtexte, header_om_htmletat, header_offset, footer_om_htmletat, footer_offset) VALUES (11, 1, 'reunion_compte_rendu_general', 'Réunion - Compte-rendu général', true, 'L', 'A4', NULL, 10, 10, '<table>
<tbody>
<tr>
<td>
<p>[reunion.lieu_salle]</p>
<p>[reunion.lieu_adresse_ligne1]</p>
<p>[reunion.lieu_adresse_ligne2]</p>
</td>
<td>
<p style=''text-align: right;''><span class=''mce_maj''>[reunion.libelle]</span></p>
<p style=''text-align: right;''> </p>
<p style=''text-align: right;''><span style=''font-weight: bold;''>COMPTE-RENDU GÉNÉRAL DE LA RÉUNION DU [reunion.date_reunion] À [reunion.heure_reunion]</span></p>
</td>
</tr>
</tbody>
</table>', 10, 16, 0, 10, '0', '<p><span id=''reu_crg'' class=''mce_sousetat''>Réunion - Compte-rendu général</span></p>
<p> </p>', 4, 10, 10, 10, 10, 'helvetica', '0-0-0', NULL, 0, '<p style="text-align:center;font-size:8pt;"><em>Page &numpage/&nbpages</em></p>', 12);
INSERT INTO om_lettretype (om_lettretype, om_collectivite, id, libelle, actif, orientation, format, logo, logoleft, logotop, titre_om_htmletat, titreleft, titretop, titrelargeur, titrehauteur, titrebordure, corps_om_htmletatex, om_sql, margeleft, margetop, margeright, margebottom, se_font, se_couleurtexte, header_om_htmletat, header_offset, footer_om_htmletat, footer_offset) VALUES (115, 1, 'lt_arrete_ouverture', 'LT - Arrêté d''ouverture', true, 'P', 'A4', NULL, 10, 10, '<p><span style=''font-weight: bold;''>Arrêté d''ouverture</span></p><p>Établissement : [etablissement.code] - [etablissement.libelle]</p><p>DC : [dossier_coordination.libelle]</p>', 10, 16, 0, 10, '0', '<p>...</p>', 12, 10, 10, 10, 10, 'helvetica', '0-0-0', NULL, 0, '<p style="text-align:center;font-size:8pt;"><em>Page &numpage/&nbpages</em></p>', 12);
INSERT INTO om_lettretype (om_lettretype, om_collectivite, id, libelle, actif, orientation, format, logo, logoleft, logotop, titre_om_htmletat, titreleft, titretop, titrelargeur, titrehauteur, titrebordure, corps_om_htmletatex, om_sql, margeleft, margetop, margeright, margebottom, se_font, se_couleurtexte, header_om_htmletat, header_offset, footer_om_htmletat, footer_offset) VALUES (16, 1, 'reunion_compte_rendu_general_cca', 'Réunion - Compte-rendu général CCA', true, 'L', 'A4', NULL, 10, 10, '<table>
<tbody>
<tr>
<td>
<p>[reunion.lieu_salle]</p>
<p>[reunion.lieu_adresse_ligne1]</p>
<p>[reunion.lieu_adresse_ligne2]</p>
</td>
<td>
<p style=''text-align: right;''><span class=''mce_maj''>[reunion.libelle]</span></p>
<p style=''text-align: right;''> </p>
<p style=''text-align: right;''><span style=''font-weight: bold;''>COMPTE-RENDU GÉNÉRAL DE LA RÉUNION DU [reunion.date_reunion] À [reunion.heure_reunion]</span></p>
</td>
</tr>
</tbody>
</table>', 10, 16, 0, 10, '0', '<p><span id=''reu_crg_cca'' class=''mce_sousetat''>Réunion - Compte-rendu général CCA</span></p>
<p> </p>', 4, 10, 10, 10, 10, 'helvetica', '0-0-0', NULL, 0, '<p style="text-align:center;font-size:8pt;"><em>Page &numpage/&nbpages</em></p>', 12);
INSERT INTO om_lettretype (om_lettretype, om_collectivite, id, libelle, actif, orientation, format, logo, logoleft, logotop, titre_om_htmletat, titreleft, titretop, titrelargeur, titrehauteur, titrebordure, corps_om_htmletatex, om_sql, margeleft, margetop, margeright, margebottom, se_font, se_couleurtexte, header_om_htmletat, header_offset, footer_om_htmletat, footer_offset) VALUES (17, 1, 'reunion_compte_rendu_general_ccs', 'Réunion - Compte-rendu général CCS', true, 'L', 'A4', NULL, 10, 10, '<table>
<tbody>
<tr>
<td>
<p>[reunion.lieu_salle]</p>
<p>[reunion.lieu_adresse_ligne1]</p>
<p>[reunion.lieu_adresse_ligne2]</p>
</td>
<td>
<p style=''text-align: right;''><span class=''mce_maj''>[reunion.libelle]</span></p>
<p style=''text-align: right;''> </p>
<p style=''text-align: right;''><span style=''font-weight: bold;''>COMPTE-RENDU GÉNÉRAL DE LA RÉUNION DU [reunion.date_reunion] À [reunion.heure_reunion]</span></p>
</td>
</tr>
</tbody>
</table>', 10, 16, 0, 10, '0', '<p><span id=''reu_crg_ccs_plen_plans'' class=''mce_sousetat''>Réunion - Compte-rendu général CCS - Plans</span></p>
<p> </p>
<p><span id=''reu_crg_ccs_plen_visites'' class=''mce_sousetat''>Réunion - Compte-rendu général CCS - Visites</span></p>
<p> </p>
<p><span id=''reu_crg_ccs_plen_apdif'' class=''mce_sousetat''>Réunion - Compte-rendu général CCS - Avis différés</span></p>
<p> </p>
<p><span id=''reu_crg_ccs_plen_apmed'' class=''mce_sousetat''>Réunion - Compte-rendu général CCS - Mises en demeure</span></p>
<p> </p>
<p><span id=''reu_crg_ccs_plen_apscds'' class=''mce_sousetat''>Réunion - Compte-rendu général CCS - Sous Commission Départementale</span></p>
<p> </p>
<p><span id=''reu_crg_ccs_plen_enjeux'' class=''mce_sousetat''>Réunion - Compte-rendu général CCS - Enjeux</span></p>
<p> </p>', 4, 10, 10, 10, 10, 'helvetica', '0-0-0', NULL, 0, '<p style="text-align:center;font-size:8pt;"><em>Page &numpage/&nbpages</em></p>', 12);
INSERT INTO om_lettretype (om_lettretype, om_collectivite, id, libelle, actif, orientation, format, logo, logoleft, logotop, titre_om_htmletat, titreleft, titretop, titrelargeur, titrehauteur, titrebordure, corps_om_htmletatex, om_sql, margeleft, margetop, margeright, margebottom, se_font, se_couleurtexte, header_om_htmletat, header_offset, footer_om_htmletat, footer_offset) VALUES (13, 1, 'reunion_compte_rendu_avis', 'Réunion - Compte rendu d''avis', true, 'P', 'A4', NULL, 10, 10, '<table>
<tbody>
<tr>
<td>
<p>[rn_salle]</p>
<p>[rn_adresse]</p>
<p>[rn_adresse_comp]</p>
</td>
<td style=''text-align: right;''><span class=''mce_maj'' style=''font-weight: bold;''>[rn_type]</span></td>
</tr>
</tbody>
</table>
<p style=''text-align: center;''> </p>
<p style=''text-align: center;''><span style=''font-weight: bold;''>COMPTE RENDU SPÉCIFIQUE</span></p>
<p style=''text-align: center;''><span style=''font-weight: bold;''>RÉUNION DU [rn_date] À [rn_heure]<br /></span></p>', 10, 16, 0, 10, '0', '<p>Dossier : [di_libelle]</p>
<p>Établissement : [et_code] - [et_libelle]</p>
<p> </p>
<p><span style=''font-weight: bold;''>Demande de passage en réunion</span></p>
<p>proposition d''avis : [dpr.proposition_avis]<br />complément de la proposition d''avis : [dpr.proposition_avis_comp]<br />ordre de passage : [dpr.ordre]<br />date souhaitée : [dpr.date_souhaitee]<br />avis rendu : [dpr.avis_rendu]<br />complément de l''avis rendu : [dpr.avis_rendu_comp]<br />motivation de l''avis rendu : [dpr.avis_rendu_motiv]<br />catégorie de passage : [dpr.reunion_type_categorie]</p>
<p> </p>
<p><span style="font-size: 10pt; font-family: times new roman,times; font-weight: bold;">Avis de la CCS :</span></p>
<p>[dpr.liste_ap]</p>
<p> </p>
<p><span id=''reu_cra'' class=''mce_sousetat''>Réunion - Compte-rendu d''avis</span></p>
<p> </p>
<p> </p>
<table border=''1''>
<tbody>
<tr>
<td>
<p><span class=''mce_maj'' style=''font-weight: bold;''>AVIS DE [rn_type]</span></p>
<p> </p>
<p style=''text-align: center;''><span class=''mce_maj'' style=''font-weight: bold;''>[avis_libelle]</span></p>
<p style=''text-align: center;''><span style=''font-weight: bold;''>[avis_complement]</span></p>
<p style=''text-align: center;''><span style=''font-weight: bold;''>[avis_motivation]</span></p>
<p style=''text-align: center;''> </p>
<p style=''text-align: center;''><span style=''font-weight: bold;''>Ne change pas l''avis de visite</span></p>
</td>
</tr>
</tbody>
</table>
<p> </p>
<p style=''text-align: right;''>Le Président</p>
<p style=''text-align: right;''>[rn_president]</p>', 5, 10, 10, 10, 10, 'helvetica', '0-0-0', NULL, 0, '<p style="text-align:center;font-size:8pt;"><em>Page &numpage/&nbpages</em></p>', 12);
INSERT INTO om_lettretype (om_lettretype, om_collectivite, id, libelle, actif, orientation, format, logo, logoleft, logotop, titre_om_htmletat, titreleft, titretop, titrelargeur, titrehauteur, titrebordure, corps_om_htmletatex, om_sql, margeleft, margetop, margeright, margebottom, se_font, se_couleurtexte, header_om_htmletat, header_offset, footer_om_htmletat, footer_offset) VALUES (3, 1, 'programmation_convocation_exploitant', 'Programmation - Convocation exploitant', true, 'P', 'A4', NULL, 10, 10, '<p>Lettre de convocation</p>', 109, 16, 0, 10, '0', '<p class=''western'' style=''margin-right: 0.5cm; text-indent: 1cm; margin-bottom: 0cm; text-align: right;''><span style=''font-family: helvetica; font-size: 10pt;''>Le [date_evenement_instruction]</span></p>
<p class=''western'' style=''margin-left: 1cm; margin-right: 0.5cm; margin-bottom: 0cm;'' align=''JUSTIFY''><span style=''font-weight: bold; font-size: 10pt; font-family: helvetica;''><em><span style=''text-decoration: underline;''>Objet</span> :<span class=''mce_maj''> CONVOCATION.</span></em></span></p>
<p class=''western'' style=''margin-left: 1cm; margin-right: 0.5cm; margin-bottom: 0cm;'' align=''JUSTIFY''> </p>
<p class=''western'' style=''margin-left: 1cm; margin-right: 0.5cm; margin-bottom: 0cm;'' align=''JUSTIFY''><span style=''font-family: helvetica; font-size: 10pt;''>Madame, Monsieur,</span></p>
<p class=''western'' style=''margin-left: 1cm; margin-right: 0.5cm; margin-bottom: 0cm;'' align=''JUSTIFY''> </p>
<p class=''western'' style=''margin-left: 1cm; margin-right: 0.5cm; margin-bottom: 0cm;'' align=''JUSTIFY''><span style=''font-family: helvetica; font-size: 10pt;''>Vous avez déposé le <span class=''field_value pre''>[date_depot_dossier]</span> une demande de <span class=''mce_maj''>[libelle_datd] [libelle_dit]</span>, enregistrée sous les références portées dans le cadre ci-dessus.</span></p>
<p class=''western'' style=''margin-left: 1cm; margin-right: 0.5cm; margin-bottom: 0cm;'' align=''JUSTIFY''> </p>
<p class=''western'' style=''margin-left: 1cm; margin-right: 0.5cm; margin-bottom: 0cm;'' align=''JUSTIFY''><span style=''font-family: helvetica; font-size: 10pt;''>Je vous informe que des pièces manquent dans le dossier que vous avez déposé.</span></p>
<p class=''western'' style=''margin-left: 1cm; margin-right: 0.5cm; margin-bottom: 0cm;'' align=''JUSTIFY''><span style=''font-family: helvetica; font-size: 10pt;''>Pour permettre l’instruction, il est nécessaire que vous fassiez parvenir à la mairie les pièces ou indications suivantes : </span></p>
<p class=''western'' style=''margin-left: 1cm; margin-right: 0.5cm; margin-bottom: 0cm;'' align=''JUSTIFY''><span style=''font-family: helvetica; font-size: 10pt;''><span class=''field_value pre''>[complement2_instruction]</span></span></p>
<p class=''western'' style=''margin-left: 1cm; margin-right: 0.5cm; margin-bottom: 0cm;'' align=''JUSTIFY''><span style=''font-family: helvetica; font-size: 10pt;''>Ces pièces seront déposées contre récépissé, ou adressées par courrier avec accusé de réception. </span></p>
<p class=''western'' style=''margin-left: 1cm; margin-right: 0.5cm; margin-bottom: 0cm;'' align=''JUSTIFY''> </p>
<p class=''western'' style=''margin-left: 1cm; margin-right: 0.5cm; margin-bottom: 0cm;'' align=''JUSTIFY''><span style=''font-family: helvetica; font-size: 10pt;''>Le délai d’instruction de <span class=''mce_maj'' style=''font-weight: bold;''>[archive_delai_instruction] mois</span> qui vous a été notifié commencera à courir à partir de la date de réception en mairie de la totalité des informations et pièces manquantes.</span></p>
<p class=''western'' style=''margin-left: 1cm; margin-right: 0.5cm; margin-bottom: 0cm;'' align=''JUSTIFY''> </p>
<p class=''western'' style=''margin-left: 1cm; margin-right: 0.5cm; margin-bottom: 0cm;'' align=''JUSTIFY''><span style=''font-family: helvetica; font-size: 10pt;''>Vous disposez de <strong>3 mois</strong> à compter de la date de réception de cette lettre pour faire parvenir à la mairie l’intégralité des pièces et informations manquantes. Dans le cas contraire, vous serez réputé avoir renoncé à votre projet, et votre demande fera l’objet d’une <span style=''font-weight: bold;''>décision tacite de rejet / d''opposition</span> (article R. 423-39 du code de l''urbanisme)</span></p>
<p class=''western'' style=''margin-left: 1cm; margin-right: 0.5cm; margin-bottom: 0cm;'' align=''JUSTIFY''> </p>
<p class=''western'' style=''margin-left: 1cm; margin-right: 0.5cm; margin-bottom: 0cm;'' align=''JUSTIFY''><span style=''font-family: helvetica; font-size: 10pt;''>Veuillez agréer, Madame, Monsieur, l''assurance de mes salutations distinguées.</span></p>
<p class=''western'' style=''margin-left: 1cm; margin-right: 0.5cm; margin-bottom: 0cm;'' align=''JUSTIFY''> </p>
<table style=''width: 100%; margin-left: auto; margin-right: auto;''>
<tbody>
<tr>
<td style=''width: 60%;''> </td>
<td><span style=''font-family: helvetica; font-size: 10pt;''>Pour le Maire,</span></td>
<td> </td>
</tr>
<tr>
<td> </td>
<td style=''text-align: left;''>
<p><span style=''font-family: helvetica; font-size: 10pt;''>Le responsable de la Division,</span></p>
<p> </p>
<p> </p>
<p> </p>
<p><span style=''font-size: 10pt; font-family: helvetica; font-weight: bold;''>[chef_division]</span></p>
</td>
<td style=''text-align: center;''> </td>
</tr>
</tbody>
</table>
<p class=''western'' style=''margin-left: 0.64cm; margin-bottom: 0cm;'' align=''JUSTIFY''> </p>
<p class=''western'' style=''margin-left: 0.64cm; margin-bottom: 0cm;'' align=''JUSTIFY''><span class=''mce_maj'' style=''font-size: 8pt; font-family: helvetica;''><span style=''text-decoration: underline;''><strong>DéLAIS ET VOIES DE RECOURS contre la présente lettre</strong></span> : </span></p>
<p class=''western'' style=''margin-left: 0.64cm; margin-bottom: 0cm;'' align=''JUSTIFY''><span style=''font-size: 8pt; font-family: helvetica;''><strong>Le (ou les) demandeur(s) peut contester la légalité de la présente lettre dans les deux mois qui suivent la date de notification. A cet effet, il peut saisir le tribunal administratif territorialement compétent d''un recours contentieux.</strong></span></p>', 11, 0, 0, 0, 0, 'helvetica', '0-0-0', NULL, 0, '<p style="text-align:center;font-size:8pt;"><em>Page &numpage/&nbpages</em></p>', 12);
INSERT INTO om_lettretype (om_lettretype, om_collectivite, id, libelle, actif, orientation, format, logo, logoleft, logotop, titre_om_htmletat, titreleft, titretop, titrelargeur, titrehauteur, titrebordure, corps_om_htmletatex, om_sql, margeleft, margetop, margeright, margebottom, se_font, se_couleurtexte, header_om_htmletat, header_offset, footer_om_htmletat, footer_offset) VALUES (4, 1, 'analyse_rapport_si', 'Analyse - Rapport SI', true, 'P', 'A4', NULL, 10, 10, '<p><span class=''mce_maj'' style=''font-size: 14pt;''>Rapport d''analyse - sécurité incendie<br /></span></p>', 48, 10, 0, 10, '0', '<p style=''text-align: center;''><span style=''font-weight: bold;''>Références</span></p>
<p> </p>
<p><span style=''font-weight: bold;''>* Dosser d''instruction</span></p>
<p>Libellé : [di_lib]</p>
<p>Technicien en charge : [di_tech]</p>
<p>Autorité compétente : [di_ac]</p>
<p>À qualifier : [di_qualif]</p>
<p>Incomplétude : [di_incomp]</p>
<p>Clôturé : [di_clos]</p>
<p>Prioritaire : [di_prio]</p>
<p>Pièces attendues : [di_piece]</p>
<p>Description détaillée : [di_desc]</p>
<p>Commentaires d''instruction : [di_notes]</p>
<p>Statut : [di_statut]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Dossier de coordination</span></p>
<p>Libellé : [dc_lib]</p>
<p>Type : [dc_type]</p>
<p>Date de la demande : [dc_date_dmnd]</p>
<p>Date butoir : [dc_date_butr]</p>
<p>Dossier d''autorisation ADS : [dc_da_ads]</p>
<p>Dossier d''instruction ADS : [dc_di_ads]</p>
<p>À qualifier : [dc_qualif]</p>
<p>Clôturé : [dc_clos]</p>
<p>Contraintes urbaines : [dc_ctr_urb]</p>
<p>Dossier de coordination parent : [dc_parent]</p>
<p>Description : [dc_desc]</p>
<p>Autorité de police en cours : [dc_ap]</p>
<p>Références cadastrales : [dc_ref_cad]</p>
<p> </p>
<p><br /><span style=''font-weight: bold;''>* Établissement</span></p>
<p>Numéro T : [etab_code]</p>
<p>Libellé : [etab_lib]</p>
<p>Type : [etab_type]</p>
<p>Catégorie : [etab_cat]</p>
<p>Siret : [etab_siret]</p>
<p>Téléphone : [etab_tel]</p>
<p>Fax : [etab_fax]</p>
<p>Année de construction : [etab_annee_const]</p>
<p>Nature : [etab_nat]</p>
<p>Statut juridique : [etab_stat_jur]</p>
<p>Tutelle administrative : [etab_tut_adm]</p>
<p>État : [etab_etat]</p>
<p>Adresse : [etab_adresse]</p>
<p>Date d''arrêté d''ouverture : [etab_date_ar_ouv]</p>
<p>Exploitant : [etab_exp]</p>
<p>Références cadastrales : [etab_ref_cad]</p>
<p> </p>
<p style=''text-align: center;''><span style=''font-weight: bold;''>Analyse<br /></span></p>
<p><span style=''font-weight: bold;''>* Type d''analyse</span><br />[anls_type]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Objet</span></p>
<p>[anls_obj]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Descriptif de l''établissement</span></p>
<p>[anls_desc]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Classification de l''établissement</span></p>
<p>Type d''établissement : [dc_etab_type]</p>
<p>Type(s) secondaire(s) : [dc_etab_type_sec]</p>
<p>Catégorie d''établissement : [dc_etab_cat]</p>
<p>Établissement référentiel : [dc_etab_erp]</p>
<p>Locaux à sommeil : [dc_etab_loc_som]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Données techniques</span></p>
<p>Effectif public : [dt_eff_pub]</p>
<p>Effectif personnel : [dt_eff_pers]</p>
<p>Type SSI : [dt_ssi]</p>
<p>Type alarme : [dt_alarme]</p>
<p>Conformité L16 : [dt_l16]</p>
<p>Alimentation de remplacement : [dt_alim]</p>
<p>Service sécurité : [dt_svc_sec]</p>
<p>Personnel de jour : [dt_pers_jour]</p>
<p>Personnel de nuit : [dt_pers_nuit]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Réglementation applicable</span></p>
<p>[anls_reg_app]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Prescriptions</span></p>
<p>[anls_presc]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Documents présentés lors des visites</span></p>
<p>[anls_doc_pdnt]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Documents fournis après les visites</span></p>
<p>[anls_doc_apres]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Essais réalisés</span></p>
<p>[anls_essais]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Compte-rendu</span></p>
<p>[anls_cr]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Observation</span></p>
<p>[anls_obs]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Avis proposé</span></p>
<p>[anls_avis_prop]</p>
<p>[anls_avis_comp]</p>', 2, 5, 10, 5, 15, 'helvetica', '0-0-0', NULL, 0, '<p style="text-align:center;font-size:8pt;"><em>Page &numpage/&nbpages</em></p>', 12);
INSERT INTO om_lettretype (om_lettretype, om_collectivite, id, libelle, actif, orientation, format, logo, logoleft, logotop, titre_om_htmletat, titreleft, titretop, titrelargeur, titrehauteur, titrebordure, corps_om_htmletatex, om_sql, margeleft, margetop, margeright, margebottom, se_font, se_couleurtexte, header_om_htmletat, header_offset, footer_om_htmletat, footer_offset) VALUES (6, 1, 'analyse_compte_rendu_si', 'Analyse - Compte-rendu SI', true, 'P', 'A4', NULL, 10, 10, '<p><span class=''mce_maj'' style=''font-size: 14pt;''>Compte-rendu d''analyse - sécurité incendie<br /></span></p>', 42, 10, 0, 10, '0', '<p style=''text-align: center;''><span style=''font-weight: bold;''>Références</span></p>
<p> </p>
<p><span style=''font-weight: bold;''>* Dosser d''instruction</span></p>
<p>Libellé : [di_lib]</p>
<p>Technicien en charge : [di_tech]</p>
<p>Autorité compétente : [di_ac]</p>
<p>À qualifier : [di_qualif]</p>
<p>Incomplétude : [di_incomp]</p>
<p>Clôturé : [di_clos]</p>
<p>Prioritaire : [di_prio]</p>
<p>Pièces attendues : [di_piece]</p>
<p>Description détaillée : [di_desc]</p>
<p>Commentaires d''instruction : [di_notes]</p>
<p>Statut : [di_statut]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Dossier de coordination</span></p>
<p>Libellé : [dc_lib]</p>
<p>Type : [dc_type]</p>
<p>Date de la demande : [dc_date_dmnd]</p>
<p>Date butoir : [dc_date_butr]</p>
<p>Dossier d''autorisation ADS : [dc_da_ads]</p>
<p>Dossier d''instruction ADS : [dc_di_ads]</p>
<p>À qualifier : [dc_qualif]</p>
<p>Clôturé : [dc_clos]</p>
<p>Contraintes urbaines : [dc_ctr_urb]</p>
<p>Dossier de coordination parent : [dc_parent]</p>
<p>Description : [dc_desc]</p>
<p>Autorité de police en cours : [dc_ap]</p>
<p>Références cadastrales : [dc_ref_cad]</p>
<p> </p>
<p><br /><span style=''font-weight: bold;''>* Établissement</span></p>
<p>Numéro T : [etab_code]</p>
<p>Libellé : [etab_lib]</p>
<p>Type : [etab_type]</p>
<p>Catégorie : [etab_cat]</p>
<p>Siret : [etab_siret]</p>
<p>Téléphone : [etab_tel]</p>
<p>Fax : [etab_fax]</p>
<p>Année de construction : [etab_annee_const]</p>
<p>Nature : [etab_nat]</p>
<p>Statut juridique : [etab_stat_jur]</p>
<p>Tutelle administrative : [etab_tut_adm]</p>
<p>État : [etab_etat]</p>
<p>Adresse : [etab_adresse]</p>
<p>Date d''arrêté d''ouverture : [etab_date_ar_ouv]</p>
<p>Exploitant : [etab_exp]</p>
<p>Références cadastrales : [etab_ref_cad]</p>
<p> </p>
<p style=''text-align: center;''><span style=''font-weight: bold;''>Analyse<br /></span></p>
<p><span style=''font-weight: bold;''>* Type d''analyse</span><br />[anls_type]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Objet</span></p>
<p>[anls_obj]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Descriptif de l''établissement</span></p>
<p>[anls_desc]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Classification de l''établissement</span></p>
<p>Type d''établissement : [dc_etab_type]</p>
<p>Type(s) secondaire(s) : [dc_etab_type_sec]</p>
<p>Catégorie d''établissement : [dc_etab_cat]</p>
<p>Établissement référentiel : [dc_etab_erp]</p>
<p>Locaux à sommeil : [dc_etab_loc_som]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Données techniques</span></p>
<p>Effectif public : [dt_eff_pub]</p>
<p>Effectif personnel : [dt_eff_pers]</p>
<p>Type SSI : [dt_ssi]</p>
<p>Type alarme : [dt_alarme]</p>
<p>Conformité L16 : [dt_l16]</p>
<p>Alimentation de remplacement : [dt_alim]</p>
<p>Service sécurité : [dt_svc_sec]</p>
<p>Personnel de jour : [dt_pers_jour]</p>
<p>Personnel de nuit : [dt_pers_nuit]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Réglementation applicable</span></p>
<p>[anls_reg_app]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Prescriptions</span></p>
<p>[anls_presc]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Documents présentés lors des visites</span></p>
<p>[anls_doc_pdnt]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Documents fournis après les visites</span></p>
<p>[anls_doc_apres]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Essais réalisés</span></p>
<p>[anls_essais]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Compte-rendu</span></p>
<p>[anls_cr]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Observation</span></p>
<p>[anls_obs]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Avis proposé</span></p>
<p>[anls_avis_prop]</p>
<p>[anls_avis_comp]</p>', 2, 5, 10, 5, 15, 'helvetica', '0-0-0', NULL, 0, '<p style="text-align:center;font-size:8pt;"><em>Page &numpage/&nbpages</em></p>', 12);
INSERT INTO om_lettretype (om_lettretype, om_collectivite, id, libelle, actif, orientation, format, logo, logoleft, logotop, titre_om_htmletat, titreleft, titretop, titrelargeur, titrehauteur, titrebordure, corps_om_htmletatex, om_sql, margeleft, margetop, margeright, margebottom, se_font, se_couleurtexte, header_om_htmletat, header_offset, footer_om_htmletat, footer_offset) VALUES (5, 1, 'analyse_rapport_acc', 'Analyse - Rapport ACC', true, 'P', 'A4', NULL, 10, 10, '<p><span class=''mce_maj'' style=''font-size: 14pt;''>Rapport d''analyse - accessibilité<br /></span></p>', 57, 10, 0, 10, '0', '<p style=''text-align: center;''><span style=''font-weight: bold;''>Références</span></p>
<p> </p>
<p><span style=''font-weight: bold;''>* Dosser d''instruction</span></p>
<p>Libellé : [di_lib]</p>
<p>Technicien en charge : [di_tech]</p>
<p>Autorité compétente : [di_ac]</p>
<p>À qualifier : [di_qualif]</p>
<p>Incomplétude : [di_incomp]</p>
<p>Clôturé : [di_clos]</p>
<p>Prioritaire : [di_prio]</p>
<p>Pièces attendues : [di_piece]</p>
<p>Description détaillée : [di_desc]</p>
<p>Commentaires d''instruction : [di_notes]</p>
<p>Statut : [di_statut]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Dossier de coordination</span></p>
<p>Libellé : [dc_lib]</p>
<p>Type : [dc_type]</p>
<p>Date de la demande : [dc_date_dmnd]</p>
<p>Date butoir : [dc_date_butr]</p>
<p>Dossier d''autorisation ADS : [dc_da_ads]</p>
<p>Dossier d''instruction ADS : [dc_di_ads]</p>
<p>À qualifier : [dc_qualif]</p>
<p>Clôturé : [dc_clos]</p>
<p>Contraintes urbaines : [dc_ctr_urb]</p>
<p>Dossier de coordination parent : [dc_parent]</p>
<p>Description : [dc_desc]</p>
<p>Autorité de police en cours : [dc_ap]</p>
<p>Références cadastrales : [dc_ref_cad]</p>
<p> </p>
<p><br /><span style=''font-weight: bold;''>* Établissement</span></p>
<p>Numéro T : [etab_code]</p>
<p>Libellé : [etab_lib]</p>
<p>Type : [etab_type]</p>
<p>Catégorie : [etab_cat]</p>
<p>Siret : [etab_siret]</p>
<p>Téléphone : [etab_tel]</p>
<p>Fax : [etab_fax]</p>
<p>Année de construction : [etab_annee_const]</p>
<p>Nature : [etab_nat]</p>
<p>Statut juridique : [etab_stat_jur]</p>
<p>Tutelle administrative : [etab_tut_adm]</p>
<p>État : [etab_etat]</p>
<p>Adresse : [etab_adresse]</p>
<p>Date d''arrêté d''ouverture : [etab_date_ar_ouv]</p>
<p>Exploitant : [etab_exp]</p>
<p>Références cadastrales : [etab_ref_cad]</p>
<p> </p>
<p style=''text-align: center;''><span style=''font-weight: bold;''>Analyse<br /></span></p>
<p><span style=''font-weight: bold;''>* Type d''analyse</span><br />[anls_type]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Objet</span></p>
<p>[anls_obj]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Descriptif de l''établissement</span></p>
<p>[anls_desc]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Classification de l''établissement</span></p>
<p>Type d''établissement : [dc_etab_type]</p>
<p>Type(s) secondaire(s) : [dc_etab_type_sec]</p>
<p>Catégorie d''établissement : [dc_etab_cat]</p>
<p>Établissement référentiel : [dc_etab_erp]</p>
<p>Locaux à sommeil : [dc_etab_loc_som]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Données techniques</span></p>
<p>Handicap mental : [dt_h_ment]</p>
<p>Handicap auditif : [dt_h_audtf]</p>
<p>Handicap physique : [dt_h_phsq]</p>
<p>Handicap visuel : [dt_h_visu]</p>
<p>Élévateur : [dt_elvtr]</p>
<p>Ascenseur : [dt_ascsr]</p>
<p>Douche : [dt_douche]</p>
<p>Boucle magnétique : [dt_bcl_mgnt]</p>
<p>Sanitaires : [dt_sanitaire]</p>
<p>Nb de places de stationnement aménagées : [dt_plc_stmnt_amng]</p>
<p>Nb de chambres aménagées : [dt_chmbr_amng]</p>
<p>Nb de places assises public : [dt_plc_ass_pub]</p>
<p>Dérogation SCDA : [dt_scda]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Réglementation applicable</span></p>
<p>[anls_reg_app]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Prescriptions<br /></span></p>
<p>[anls_presc]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Documents présentés lors des visites<br /></span></p>
<p>[anls_doc_pdnt]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Documents fournis après les visites</span></p>
<p>[anls_doc_apres]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Essais réalisés</span></p>
<p>[anls_essais]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Compte-rendu</span></p>
<p>[anls_cr]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Observation</span></p>
<p>[anls_obs]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Avis proposé</span></p>
<p>[anls_avis_prop]</p>
<p>[anls_avis_comp]</p>', 2, 5, 10, 5, 15, 'helvetica', '0-0-0', NULL, 0, '<p style="text-align:center;font-size:8pt;"><em>Page &numpage/&nbpages</em></p>', 12);
INSERT INTO om_lettretype (om_lettretype, om_collectivite, id, libelle, actif, orientation, format, logo, logoleft, logotop, titre_om_htmletat, titreleft, titretop, titrelargeur, titrehauteur, titrebordure, corps_om_htmletatex, om_sql, margeleft, margetop, margeright, margebottom, se_font, se_couleurtexte, header_om_htmletat, header_offset, footer_om_htmletat, footer_offset) VALUES (7, 1, 'analyse_compte_rendu_acc', 'Analyse - Compte-rendu ACC', true, 'P', 'A4', NULL, 10, 10, '<p><span class=''mce_maj'' style=''font-size: 14pt;''>Compte-rendu d''analyse - accessibilité<br /></span></p>', 50, 10, 0, 10, '0', '<p style=''text-align: center;''><span style=''font-weight: bold;''>Références</span></p>
<p> </p>
<p><span style=''font-weight: bold;''>* Dosser d''instruction</span></p>
<p>Libellé : [di_lib]</p>
<p>Technicien en charge : [di_tech]</p>
<p>Autorité compétente : [di_ac]</p>
<p>À qualifier : [di_qualif]</p>
<p>Incomplétude : [di_incomp]</p>
<p>Clôturé : [di_clos]</p>
<p>Prioritaire : [di_prio]</p>
<p>Pièces attendues : [di_piece]</p>
<p>Description détaillée : [di_desc]</p>
<p>Commentaires d''instruction : [di_notes]</p>
<p>Statut : [di_statut]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Dossier de coordination</span></p>
<p>Libellé : [dc_lib]</p>
<p>Type : [dc_type]</p>
<p>Date de la demande : [dc_date_dmnd]</p>
<p>Date butoir : [dc_date_butr]</p>
<p>Dossier d''autorisation ADS : [dc_da_ads]</p>
<p>Dossier d''instruction ADS : [dc_di_ads]</p>
<p>À qualifier : [dc_qualif]</p>
<p>Clôturé : [dc_clos]</p>
<p>Contraintes urbaines : [dc_ctr_urb]</p>
<p>Dossier de coordination parent : [dc_parent]</p>
<p>Description : [dc_desc]</p>
<p>Autorité de police en cours : [dc_ap]</p>
<p>Références cadastrales : [dc_ref_cad]</p>
<p> </p>
<p><br /><span style=''font-weight: bold;''>* Établissement</span></p>
<p>Numéro T : [etab_code]</p>
<p>Libellé : [etab_lib]</p>
<p>Type : [etab_type]</p>
<p>Catégorie : [etab_cat]</p>
<p>Siret : [etab_siret]</p>
<p>Téléphone : [etab_tel]</p>
<p>Fax : [etab_fax]</p>
<p>Année de construction : [etab_annee_const]</p>
<p>Nature : [etab_nat]</p>
<p>Statut juridique : [etab_stat_jur]</p>
<p>Tutelle administrative : [etab_tut_adm]</p>
<p>État : [etab_etat]</p>
<p>Adresse : [etab_adresse]</p>
<p>Date d''arrêté d''ouverture : [etab_date_ar_ouv]</p>
<p>Exploitant : [etab_exp]</p>
<p>Références cadastrales : [etab_ref_cad]</p>
<p>UA Validées :</p>
<p>[etablissement.ua_validees]</p>
<p> </p>
<p style=''text-align: center;''><span style=''font-weight: bold;''>Analyse<br /></span></p>
<p><span style=''font-weight: bold;''>* Type d''analyse</span><br />[anls_type]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Objet</span></p>
<p>[anls_obj]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Descriptif de l''établissement</span></p>
<p>[anls_desc]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Classification de l''établissement</span></p>
<p>Type d''établissement : [dc_etab_type]</p>
<p>Type(s) secondaire(s) : [dc_etab_type_sec]</p>
<p>Catégorie d''établissement : [dc_etab_cat]</p>
<p>Établissement référentiel : [dc_etab_erp]</p>
<p>Locaux à sommeil : [dc_etab_loc_som]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Données techniques / UA</span></p>
<p>UA analysées :</p>
<p>[analyse.ua_analysees]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Réglementation applicable</span></p>
<p>[anls_reg_app]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Prescriptions<br /></span></p>
<p>[anls_presc]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Documents présentés lors des visites<br /></span></p>
<p>[anls_doc_pdnt]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Documents fournis après les visites</span></p>
<p>[anls_doc_apres]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Essais réalisés</span></p>
<p>[anls_essais]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Compte-rendu</span></p>
<p>[anls_cr]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Observation</span></p>
<p>[anls_obs]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Avis proposé</span></p>
<p>[anls_avis_prop]</p>
<p>[anls_avis_comp]</p>', 2, 5, 10, 5, 15, 'helvetica', '0-0-0', NULL, 0, '<p style="text-align:center;font-size:8pt;"><em>Page &numpage/&nbpages</em></p>', 12);
INSERT INTO om_lettretype (om_lettretype, om_collectivite, id, libelle, actif, orientation, format, logo, logoleft, logotop, titre_om_htmletat, titreleft, titretop, titrelargeur, titrehauteur, titrebordure, corps_om_htmletatex, om_sql, margeleft, margetop, margeright, margebottom, se_font, se_couleurtexte, header_om_htmletat, header_offset, footer_om_htmletat, footer_offset) VALUES (18, 1, 'courrier_standard_modele0', 'courrier_standard_modele0', true, 'P', 'A4', NULL, 10, 5, '<p style=''text-align: center;''><em><span class=''mce_maj'' style=''text-align: center; font-weight: bold; font-family: helvetica; font-size: 14pt;''><span class=''field_value pre''><span class=''field_value pre''>Ville de marseille</span></span></span></em></p>
<p style=''text-align: center;''><span style=''font-size: 12pt;''><em><span class=''mce_maj'' style=''text-align: center; font-family: helvetica;''><span class=''field_value pre''><span class=''field_value pre''>&amp;libelle_direction</span></span></span></em></span></p>
<p style=''text-align: center;''><br /><span style=''font-weight: bold; font-size: 14pt;''><em><span class=''mce_maj'' style=''font-family: helvetica;''>[libelle_om_lettretype] de [libelle_datd]</span></em></span></p>
<div style=''text-align: center;''><span style=''font-family: helvetica; font-size: 8pt;''>si vous souhaitez obtenir des informations sur votre dossier, adressez-vous :<br /></span><span class=''mce_maj'' style=''font-family: helvetica; font-size: 8pt;''>&amp;adresse_SAU </span></div>
<table style=''height: 20px; width: 92%;'' border=''1'' cellpadding=''5''>
<tbody>
<tr>
<td style=''width: 60%;''>
<p><span style=''font-size: 10pt; font-family: helvetica;''><span style=''font-size: 10pt; font-family: helvetica;''><span style=''font-size: 12pt;''><span style=''font-size: 10pt;''>Dossier : </span> <span style=''font-weight: bold;''><span class=''field_value pre''><span class=''field_value pre''>[libelle_dossier]</span></span></span></span></span></span></p>
<p><span style=''font-size: 10pt; font-family: helvetica;''>Déposé le : <span class=''field_value pre''><span class=''field_value pre''><span class=''field_value pre''><span class=''field_value pre''>[date_depot_dossier]</span></span></span></span></span></p>
<p><span style=''font-size: 10pt; font-family: helvetica;''><span class=''field_value pre''><span class=''field_value pre''><span class=''field_value pre''><span class=''field_value pre''><span style=''text-decoration: underline;''>Nature des travaux</span> :<span class=''mce_maj'' style=''font-weight: bold; font-size: 10pt;''> [co_projet_desc_donnees_techniques] [am_projet_desc_donnees_techniques] [dm_projet_desc_donnees_techniques]</span></span></span></span></span></span></p>
<p><span style=''font-size: 10pt; font-family: helvetica;''><span style=''text-decoration: underline;''>Adresse des travaux</span> :</span></p>
<p><span class=''field_value pre'' style=''font-size: 10pt; font-family: helvetica; font-weight: bold;''><span class=''field_value pre mce_maj''>[etablissement.code] - [etablissement.libelle] [terrain_adresse_voie_numero_dossier] [terrain_adresse_voie_dossier] </span></span></p>
<p><span class=''field_value pre'' style=''font-size: 10pt; font-family: helvetica; font-weight: bold;''><span class=''field_value pre mce_maj''>[terrain_adresse_lieu_dit_dossier] </span><span class=''field_value pre mce_maj''>[terrain_adresse_bp_dossier]</span></span></p>
<p><span class=''field_value pre'' style=''font-size: 10pt; font-family: helvetica; font-weight: bold;''><span class=''field_value pre mce_maj''>[terrain_adresse_code_postal_dossier] [terrain_adresse_localite_dossier] [terrain_adresse_cedex_dossier]</span></span></p>
</td>
<td>
<p><span style=''font-family: helvetica; font-size: 10pt;''><span style=''text-decoration: underline;''>Demandeur</span> :</span> <span class=''mce_codebarre'' style=''font-size: 10pt; font-family: helvetica;''>[courrier.code_barres]<br /></span><span class=''mce_maj'' style=''font-family: helvetica; font-size: 10pt; font-weight: bold;''> [nom_petitionnaire_principal] </span></p>
<p><span class=''mce_maj'' style=''font-family: helvetica; font-size: 10pt; font-weight: bold;''>[numero_petitionnaire_principal] [voie_petitionnaire_principal]</span></p>
<p><span class=''mce_maj'' style=''font-family: helvetica; font-size: 10pt; font-weight: bold;''>[complement_petitionnaire_principal] - [lieu_dit_petitionnaire_principal]</span></p>
<p><span class=''mce_maj'' style=''font-family: helvetica; font-size: 10pt; font-weight: bold;''>[bp_petitionnaire_principal]</span></p>
<p><span class=''mce_maj'' style=''font-family: helvetica; font-size: 10pt; font-weight: bold;''>[code_postal_petitionnaire_principal] [localite_petitionnaire_principal] [cedex_petitionnaire_principal] </span></p>
<p><span class=''mce_maj'' style=''font-family: helvetica; font-size: 10pt; font-weight: bold;''>[pays_petitionnaire_principal]</span></p>
<p><span style=''font-family: helvetica; font-size: 10pt;''><span style=''font-size: 8pt;''><span style=''text-decoration: underline;''>Demandeur(s) co-titulaire(s)</span> : </span><span class=''mce_maj'' style=''font-size: 8pt;''>[nom_petitionnaire_1] - [nom_petitionnaire_2] - [nom_petitionnaire_3] - [nom_petitionnaire_4] - [nom_petitionnaire_5]</span></span></p>
</td>
</tr>
<tr>
<td colspan=''2''>
<p><span style=''font-size: 8pt; font-family: helvetica;''>&amp;contraintes(liste_groupe=Zones Du Plu)<br />Destination - surface de plancher créée : <span class=''field_value pre''>[tab_surface_donnees_techniques]</span></span></p>
</td>
</tr>
</tbody>
</table>', 10, 5, 0, 0, '0', '<p class=''western'' style=''margin-right: 0.5cm; text-indent: 1cm; margin-bottom: 0cm; text-align: right;''><span style=''font-family: helvetica; font-size: 10pt;''>Le &amp;aujourdhui</span></p>
<p class=''western'' style=''margin-left: 1cm; margin-right: 0.5cm; margin-bottom: 0cm;'' align=''JUSTIFY''> </p>
<p class=''western'' style=''margin-left: 1cm; margin-right: 0.5cm; margin-bottom: 0cm;'' align=''JUSTIFY''><span style=''font-family: helvetica; font-size: 10pt;''>Madame, Monsieur,</span></p>
<p class=''western'' style=''margin-left: 1cm; margin-right: 0.5cm; margin-bottom: 0cm;'' align=''JUSTIFY''> </p>
<p class=''western'' style=''margin-left: 1cm; margin-right: 0.5cm; margin-bottom: 0cm;'' align=''JUSTIFY''><span style=''font-family: helvetica; font-size: 10pt;''>[courrier.complement1_om_html]</span></p>
<p class=''western'' style=''margin-left: 1cm; margin-right: 0.5cm; margin-bottom: 0cm;'' align=''JUSTIFY''> </p>
<p class=''western'' style=''margin-left: 1cm; margin-right: 0.5cm; margin-bottom: 0cm;'' align=''JUSTIFY''><span style=''font-family: helvetica; font-size: 10pt;''>Veuillez agréer, Madame, Monsieur, l''assurance de mes salutations distinguées.</span></p>
<p class=''western'' style=''margin-left: 1cm; margin-right: 0.5cm; margin-bottom: 0cm;'' align=''JUSTIFY''> </p>
<table style=''width: 100%; margin-left: auto; margin-right: auto;''>
<tbody>
<tr>
<td style=''width: 60%;''> </td>
<td><span style=''font-family: helvetica; font-size: 10pt;''>[courrier.complement2_om_html]</span></td>
<td> </td>
</tr>
</tbody>
</table>
<p class=''western'' style=''margin-left: 0.64cm; margin-bottom: 0cm;'' align=''JUSTIFY''> </p>
<p> </p>', 12, 10, 5, 10, 12, 'helvetica', '0-0-0', NULL, 0, '<p style="text-align:center;font-size:8pt;"><em>Page &numpage/&nbpages</em></p>', 12);
INSERT INTO om_lettretype (om_lettretype, om_collectivite, id, libelle, actif, orientation, format, logo, logoleft, logotop, titre_om_htmletat, titreleft, titretop, titrelargeur, titrehauteur, titrebordure, corps_om_htmletatex, om_sql, margeleft, margetop, margeright, margebottom, se_font, se_couleurtexte, header_om_htmletat, header_offset, footer_om_htmletat, footer_offset) VALUES (
	8, 1, 'proces_verbal_si', 'PV SI', true, 'P', 'A4', NULL, 10, 10, 
	'<p style=''text-align: center;''><span class=''mce_maj'' style=''font-size: 14pt;''>Procès verbal - sécurité incendie<br /></span></p>',
	7, 10, 0, 10, '0', 
	'<p>* Procès-verbal</p><p>Numéro : [proces_verbal.numero]<br />Date de rédaction : [proces_verbal.date_redaction]</p><p><br />* PV / Signataire</p><p>Civilité : [signataire_civilite]<br />Nom : [signataire_nom]<br />Prénom : [signataire_prenom]<br />Qualité : [signataire_qualite]<br />Signature : [signataire_signature]</p><p><br />* PV / Demande de passage en réunion</p><p>Date de passage souhaitée : [reunion_date]<br />Type de réunion : [reunion_type]<br />Catégorie du type de réunion : [reunion_categorie]<br />Proposition d''avis : [dpr.proposition_avis]<br />Complément de la proposition d''avis : [dpr.proposition_avis_comp]<br />Ordre de passage : [dpr.ordre]<br />Date souhaitée : [dpr.date_souhaitee]<br />Avis rendu : [dpr.avis_rendu]<br />Complément de l''avis rendu : [dpr.avis_rendu_comp]<br />Motivation de l''avis rendu : [dpr.avis_rendu_motiv]<br />Date : [reunion.date]<br />Heure : [reunion.heure]<br />Salle : [reunion.salle]<br />Adresse : [reunion.adresse]<br />Complément d''adresse : [reunion.adresse_comp]<br />Président : [reunion.president]</p><p><br />* Dosser d''instruction</p><p>Libellé : [dossier_instruction.libelle]<br />Autorité compétente : [dossier_instruction.autorite_competente]<br />À qualifier : [dossier_instruction..a_qualifier]<br />Incomplétude : [dossier_instruction.incompletude]<br />Clôturé : [dossier_instruction.dossier_cloture]<br />Prioritaire : [dossier_instruction.prioritaire]<br />Pièces attendues : [dossier_instruction.piece_attendue]<br />Description détaillée : [dossier_instruction.description]<br />Commentaires d''instruction : [dossier_instruction.notes]<br />Statut : [dossier_instruction.statut]</p><p>Liste des visites avec heure de début : |[dossier_instruction.liste_visites]|</p><p>Liste des dates de visites : |[dossier_instruction.liste_visites_dates]|</p><p>* DI / Technicien</p><p>Nom et prénom : [acteur.nom_prenom]<br />Service : [acteur.service]<br />Rôle : [acteur.role]<br />Acronyme : [acteur.acronyme]<br />Références : [acteur.reference]<br />Login : [acteur.login]<br />Email : [acteur.email]</p><p><br />* Dossier de coordination</p><p>Libellé : [dossier_coordination.libelle]<br />Type : [dossier_coordination.dossier_coordination_type]<br />Date de la demande : [dossier_coordination.date_demande]<br />Date butoir : [dossier_coordination.date_butoir]<br />Dossier d''autorisation ADS : [dossier_coordination.dossier_autorisation_ads]<br />Dossier d''instruction ADS : [dossier_coordination.dossier_instruction_ads]<br />À qualifier : [dossier_coordination.a_qualifier]<br />Clôturé : [dossier_coordination.dossier_cloture]<br />Contraintes urbaines : [dossier_coordination.contraintes_urba_om_html]<br />Dossier de coordination parent : [dossier_coordination.dossier_coordination_parent]<br />Description : [dossier_coordination.description]<br />Autorité de police en cours : [dossier_coordination.autorite_police_encours]<br />Références cadastrales : [dossier_coordination.references_cadastrales]</p><p> </p><p>* Établissement</p><p>Numéro T : [etablissement.code]<br />Libellé : [etablissement.libelle]<br />Type : [etablissement.etablissement_type]<br />Catégorie : [etablissement.etablissement_categorie]<br />Siret : [etablissement.siret]<br />Téléphone : [etablissement.telephone]<br />Fax : [etablissement.fax]<br />Année de construction : [etablissement.annee_de_construction]<br />Nature : [etablissement.etablissement_nature]<br />Statut juridique : [etablissement.etablissement_statut_juridique]<br />Tutelle administrative : [etablissement.etablissement_tutelle_adm]<br />État : [etablissement.etablissement_etat]<br />Adresse : [etablissement.adresse]<br />Date d''arrêté d''ouverture : [etablissement.date_arrete_ouverture]<br />Références cadastrales : [etablissement.references_cadastrales]</p><p><br />* Analyse</p><p>- Type d''analyse<br />[analyse.type]<br />- Objet<br />[analyse.objet]<br />- Descriptif de l''établissement<br />[analyse.descriptif_etablissement]<br />- Classification de l''établissement<br />Type d''établissement : [dossier_coordination.etablissement_type]<br />Type(s) secondaire(s) : [dossier_coordination.etablissement_type_secondaire]<br />Catégorie d''établissement : [dossier_coordination.etablissement_categorie]<br />Établissement référentiel : [dossier_coordination.erp]<br />Locaux à sommeil : [dossier_coordination.etablissement_locaux_sommeil]<br />- Données techniques<br />Effectif public : [analyse.dt_si_effectif_public]<br />Effectif personnel : [analyse.dt_si_effectif_personnel]<br />Type SSI : [analyse.dt_si_type_ssi]<br />Type alarme : [analyse.dt_si_type_alarme]<br />Conformité L16 : [analyse.dt_si_conformite_l16]<br />Alimentation de remplacement : [analyse.dt_si_alimentation_remplacement]<br />Service sécurité : [analyse.dt_si_service_securite]<br />Personnel de jour : [analyse.dt_si_personnel_jour]<br />Personnel de nuit : [analyse.dt_si_personnel_nuit]<br />- Réglementation applicable<br />[analyse.reglementation_applicable]<br />- Prescriptions<br />[analyse.prescriptions]<br />[analyse.prescriptions_defavorables]<br />- Documents présentés lors des visites<br />[analyse.documents_presentes_pendant]<br />- Documents fournis après les visites<br />[analyse.documents_presentes_apres]<br />- Essais réalisés<br />[analyse.essais_realises]<br />- Compte-rendu<br />[analyse.compte_rendu]<br />- Observation<br />[analyse.observations]<br />- Avis proposé<br />[analyse.proposition_avis]<br />[analyse.proposition_avis_complement]</p>',
	6, 5, 10, 5, 15, 'helvetica', '0-0-0', NULL, 0,
	'<p style="text-align:center;font-size:8pt;"><em>Page &numpage/&nbpages</em></p>',
	12
);


--
-- Name: om_lettretype_seq; Type: SEQUENCE SET; Schema: openaria; Owner: -
--

SELECT pg_catalog.setval('om_lettretype_seq', 116, false);


--
-- Data for Name: om_logo; Type: TABLE DATA; Schema: openaria; Owner: -
--



--
-- Name: om_logo_seq; Type: SEQUENCE SET; Schema: openaria; Owner: -
--

SELECT pg_catalog.setval('om_logo_seq', 1, false);


--
-- Name: om_requete_seq; Type: SEQUENCE SET; Schema: openaria; Owner: -
--

SELECT pg_catalog.setval('om_requete_seq', 13, false);


--
-- Data for Name: om_sousetat; Type: TABLE DATA; Schema: openaria; Owner: -
--

INSERT INTO om_sousetat (om_sousetat, om_collectivite, id, libelle, actif, titre, titrehauteur, titrefont, titreattribut, titretaille, titrebordure, titrealign, titrefond, titrefondcouleur, titretextecouleur, intervalle_debut, intervalle_fin, entete_flag, entete_fond, entete_orientation, entete_hauteur, entetecolone_bordure, entetecolone_align, entete_fondcouleur, entete_textecouleur, tableau_largeur, tableau_bordure, tableau_fontaille, bordure_couleur, se_fond1, se_fond2, cellule_fond, cellule_hauteur, cellule_largeur, cellule_bordure_un, cellule_bordure, cellule_align, cellule_fond_total, cellule_fontaille_total, cellule_hauteur_total, cellule_fondcouleur_total, cellule_bordure_total, cellule_align_total, cellule_fond_moyenne, cellule_fontaille_moyenne, cellule_hauteur_moyenne, cellule_fondcouleur_moyenne, cellule_bordure_moyenne, cellule_align_moyenne, cellule_fond_nbr, cellule_fontaille_nbr, cellule_hauteur_nbr, cellule_fondcouleur_nbr, cellule_bordure_nbr, cellule_align_nbr, cellule_numerique, cellule_total, cellule_moyenne, cellule_compteur, om_sql) VALUES (18, 1, 'reu_fp', 'Réunion - Feuille de présence', true, '...', 10, 'helvetica', 'B', 12, '0', 'L', '0', '255-255-255', '0-0-0', 10, 15, '1', '1', '0|0|0', 15, 'LTBR|LTBR|LTBR', 'C|C|C', '188-188-188', '0-0-0', 195, '1', 10, '0-0-0', '255-255-255', '239-239-239', '1', 10, '80|70|40', 'LTBR|LTBR|LTBR', 'LTBR|LTBR|LTBR', 'L|L|L', '1', 10, 15, '196-213-213', 'LTBR|LTBR|LTBR', 'L|L|L', '1', 10, 15, '196-213-213', 'LTBR|LTBR|LTBR', 'L|L|L', '1', 10, 15, '196-213-213', 'LTBR|LTBR|LTBR', 'L|L|L', '999|999|999', '0|0|0', '0|0|0', '0|0|0', 'SELECT
    ---- Organisme
    instance.libelle as organisme,
    ---- Fonction et nom
    CONCAT_WS(''
'',
        -- Ligne 1 : fonction
        membre.description,
        -- Ligne 2 : nom
        membre.membre
    ) as nom,
    ---- Signature
    ''

'' as signature
FROM
    &DB_PREFIXEreunion
LEFT JOIN &DB_PREFIXElien_reunion_r_instance_r_i_membre as lien
    ON lien.reunion = reunion.reunion
LEFT JOIN &DB_PREFIXEreunion_instance as instance
    ON lien.reunion_instance = instance.reunion_instance
LEFT JOIN &DB_PREFIXEreunion_instance_membre as membre
    ON lien.reunion_instance_membre = membre.reunion_instance_membre
WHERE
    reunion.reunion = &idx
ORDER BY
    instance.libelle, membre.membre');
INSERT INTO om_sousetat (om_sousetat, om_collectivite, id, libelle, actif, titre, titrehauteur, titrefont, titreattribut, titretaille, titrebordure, titrealign, titrefond, titrefondcouleur, titretextecouleur, intervalle_debut, intervalle_fin, entete_flag, entete_fond, entete_orientation, entete_hauteur, entetecolone_bordure, entetecolone_align, entete_fondcouleur, entete_textecouleur, tableau_largeur, tableau_bordure, tableau_fontaille, bordure_couleur, se_fond1, se_fond2, cellule_fond, cellule_hauteur, cellule_largeur, cellule_bordure_un, cellule_bordure, cellule_align, cellule_fond_total, cellule_fontaille_total, cellule_hauteur_total, cellule_fondcouleur_total, cellule_bordure_total, cellule_align_total, cellule_fond_moyenne, cellule_fontaille_moyenne, cellule_hauteur_moyenne, cellule_fondcouleur_moyenne, cellule_bordure_moyenne, cellule_align_moyenne, cellule_fond_nbr, cellule_fontaille_nbr, cellule_hauteur_nbr, cellule_fondcouleur_nbr, cellule_bordure_nbr, cellule_align_nbr, cellule_numerique, cellule_total, cellule_moyenne, cellule_compteur, om_sql) VALUES (19, 1, 'reu_cra', 'Réunion - Compte-rendu d''avis', true, '...', 10, 'helvetica', 'B', 12, '0', 'L', '0', '255-255-255', '0-0-0', 10, 15, '1', '1', '0|0|0', 15, 'LTBR|LTBR|LTBR', 'C|C|C', '188-188-188', '0-0-0', 195, '1', 10, '0-0-0', '255-255-255', '239-239-239', '1', 10, '80|70|40', 'LTBR|LTBR|LTBR', 'LTBR|LTBR|LTBR', 'L|L|L', '1', 10, 15, '196-213-213', 'LTBR|LTBR|LTBR', 'L|L|L', '1', 10, 15, '196-213-213', 'LTBR|LTBR|LTBR', 'L|L|L', '1', 10, 15, '196-213-213', 'LTBR|LTBR|LTBR', 'L|L|L', '999|999|999', '0|0|0', '0|0|0', '0|0|0', 'SELECT
    ---- Membres
    CONCAT_WS(''
'',
        -- Ligne 1 : Instance
        CONCAT_WS('' '', ''Représentant'', instance.libelle),
        -- Ligne 2 : Membre
        membre.membre
    ) as membres,
    ---- Avis et motivation 
    ''



'' as avis,
    ---- Visas
    ''



'' as visas
FROM
    &DB_PREFIXElien_reunion_r_instance_r_i_membre as lien
LEFT JOIN &DB_PREFIXEreunion
    ON lien.reunion = reunion.reunion
LEFT JOIN &DB_PREFIXEdossier_instruction_reunion as dossier
    ON dossier.reunion = reunion.reunion
LEFT JOIN &DB_PREFIXEreunion_type
    ON reunion.reunion_type = reunion_type.reunion_type
LEFT JOIN &DB_PREFIXEreunion_instance as instance
    ON lien.reunion_instance = instance.reunion_instance
LEFT JOIN &DB_PREFIXEreunion_instance_membre as membre
    ON lien.reunion_instance_membre = membre.reunion_instance_membre
WHERE
    dossier.dossier_instruction_reunion = &idx
    AND lien.reunion_instance != reunion_type.president
ORDER BY
    instance.libelle');
INSERT INTO om_sousetat (om_sousetat, om_collectivite, id, libelle, actif, titre, titrehauteur, titrefont, titreattribut, titretaille, titrebordure, titrealign, titrefond, titrefondcouleur, titretextecouleur, intervalle_debut, intervalle_fin, entete_flag, entete_fond, entete_orientation, entete_hauteur, entetecolone_bordure, entetecolone_align, entete_fondcouleur, entete_textecouleur, tableau_largeur, tableau_bordure, tableau_fontaille, bordure_couleur, se_fond1, se_fond2, cellule_fond, cellule_hauteur, cellule_largeur, cellule_bordure_un, cellule_bordure, cellule_align, cellule_fond_total, cellule_fontaille_total, cellule_hauteur_total, cellule_fondcouleur_total, cellule_bordure_total, cellule_align_total, cellule_fond_moyenne, cellule_fontaille_moyenne, cellule_hauteur_moyenne, cellule_fondcouleur_moyenne, cellule_bordure_moyenne, cellule_align_moyenne, cellule_fond_nbr, cellule_fontaille_nbr, cellule_hauteur_nbr, cellule_fondcouleur_nbr, cellule_bordure_nbr, cellule_align_nbr, cellule_numerique, cellule_total, cellule_moyenne, cellule_compteur, om_sql) VALUES (21, 1, 'prg_vis_pro', 'Programmation - Liste des visites programmées', true, '...', 0, 'helvetica', '', 0, '0', 'L', '0', '255-255-255', '0-0-0', 0, 0, '1', '1', '0|0|0|0|0', 10, 'LTBR|LTBR|LTBR|LTBR|LTBR', 'C|C|C|C|C', '227-223-223', '0-0-0', 277, '1', 10, '0-0-0', '255-255-255', '239-239-239', '1', 11, '40|60|20|60|97', 'LTBR|LTBR|LTBR|LTBR|LTBR', 'LTBR|LTBR|LTBR|LTBR|LTBR', 'C|C|C|C|C', '1', 10, 15, '196-213-213', 'LTBR|LTBR|LTBR|LTBR|LTBR', 'C|L|L|L|L', '1', 10, 15, '196-213-213', 'LTBR|LTBR|LTBR|LTBR|LTBR', 'C|L|L|L||L', '1', 10, 15, '196-213-213', 'LTBR|LTBR|LTBR|LTBR|LTBR', 'C|L|L|L|L', '999|999|999|999|999', '0|0|00|0|0', '0|0|0|0|0', '0|0|0|0|0', 'SELECT
    CONCAT(
        to_char(visite.date_visite, ''DD/MM/YYYY''), ''
'', visite.heure_debut, '' - '', visite.heure_fin
    ) as visite,
    dossier_instruction.libelle as dossier_instruction,
    acteur.nom_prenom as technicien,

    concat(contact_civilite.libelle, '' '', contact.nom, '' '', contact.prenom) as exploitant,
    CONCAT(
        etablissement.code, 
        '' - '', 
        etablissement.libelle, ''
'', etablissement.adresse_numero, 
        '' '', 
        etablissement.adresse_numero2, 
        '' '', 
        voie.libelle,
        '' '', 
        etablissement.adresse_cp, 
        '' ('', 
        arrondissement.libelle,
        '')''
    ) as etablissement
FROM
    &DB_PREFIXEvisite
LEFT JOIN 
    &DB_PREFIXEprogrammation
    ON visite.programmation = programmation.programmation
LEFT JOIN 
    &DB_PREFIXEdossier_instruction
    ON visite.dossier_instruction = dossier_instruction.dossier_instruction
LEFT JOIN 
    &DB_PREFIXEacteur
    ON visite.acteur = acteur.acteur
LEFT JOIN 
    &DB_PREFIXEdossier_coordination
    ON dossier_instruction.dossier_coordination = dossier_coordination.dossier_coordination
LEFT JOIN 
    &DB_PREFIXEetablissement
    ON dossier_coordination.etablissement = etablissement.etablissement
LEFT JOIN 
    &DB_PREFIXEvoie
    ON etablissement.adresse_voie = voie.voie
LEFT JOIN 
    &DB_PREFIXEarrondissement
    ON etablissement.adresse_arrondissement = arrondissement.arrondissement
LEFT JOIN 
    &DB_PREFIXEcontact
    ON etablissement.etablissement = contact.etablissement
LEFT JOIN
    &DB_PREFIXEcontact_type
    ON contact.contact_type = contact_type.contact_type
LEFT JOIN
    &DB_PREFIXEcontact_civilite
    ON contact.civilite = contact_civilite.contact_civilite
LEFT JOIN
    &DB_PREFIXEprogrammation_etat
    ON programmation.programmation_etat = programmation_etat.programmation_etat
LEFT JOIN
    &DB_PREFIXEvisite_etat
    ON visite.visite_etat = visite_etat.visite_etat
WHERE
    programmation.programmation = &idx
    AND (
        LOWER(contact_type.code) = LOWER(''EXPL'') 
        OR contact_type.code IS NULL )
    AND LOWER(visite_etat.code) != LOWER(''ANN'')
    AND (
         (LOWER(programmation_etat.code) = LOWER(''VAL'') AND 
            visite.programmation_version_creation <= programmation.version) 
         OR
         (LOWER(programmation_etat.code) != LOWER(''VAL'') AND 
            visite.programmation_version_creation < programmation.version)
        )
        AND (visite.programmation_version_annulation != 1 OR         
             visite.programmation_version_annulation IS NULL)
ORDER BY 
    visite.date_visite ASC');
INSERT INTO om_sousetat (om_sousetat, om_collectivite, id, libelle, actif, titre, titrehauteur, titrefont, titreattribut, titretaille, titrebordure, titrealign, titrefond, titrefondcouleur, titretextecouleur, intervalle_debut, intervalle_fin, entete_flag, entete_fond, entete_orientation, entete_hauteur, entetecolone_bordure, entetecolone_align, entete_fondcouleur, entete_textecouleur, tableau_largeur, tableau_bordure, tableau_fontaille, bordure_couleur, se_fond1, se_fond2, cellule_fond, cellule_hauteur, cellule_largeur, cellule_bordure_un, cellule_bordure, cellule_align, cellule_fond_total, cellule_fontaille_total, cellule_hauteur_total, cellule_fondcouleur_total, cellule_bordure_total, cellule_align_total, cellule_fond_moyenne, cellule_fontaille_moyenne, cellule_hauteur_moyenne, cellule_fondcouleur_moyenne, cellule_bordure_moyenne, cellule_align_moyenne, cellule_fond_nbr, cellule_fontaille_nbr, cellule_hauteur_nbr, cellule_fondcouleur_nbr, cellule_bordure_nbr, cellule_align_nbr, cellule_numerique, cellule_total, cellule_moyenne, cellule_compteur, om_sql) VALUES (20, 1, 'prg_vis_his', 'Programmation - Historique des visites programmées', true, 'Historique', 10, 'helvetica', 'B', 12, '0', 'L', '0', '255-255-255', '0-0-0', 10, 15, '0', '1', '0|0|0|0|0|0', 15, 'LTBR|LTBR|LTBR|LTBR|LTBR|LTBR', 'C|C|C|C|C|C', '188-188-188', '0-0-0', 277, '0', 8, '186-179-179', '227-224-224', '206-211-243', '1', 8, '30|30|30|30|60|97', 'TR|TRL|TRL|TRL|TRL|TL', 'TR|TRL|TRL|TRL|TRL|TL', 'C|C|C|C|C|C', '1', 10, 15, '196-213-213', 'LTBR|LTBR|LTBR|LTBR|LTBR|LTBR', 'C|L|L|L|L|L', '1', 10, 15, '196-213-213', 'LTBR|LTBR|LTBR|LTBR|LTBR|LTBR', 'C|L|L|L|L|L', '1', 10, 15, '196-213-213', 'LTBR|LTBR|LTBR|LTBR|LTBR|LTBR', 'C|L|L|L|L|L', '999|999|999|999|999|999', '0|0|0|0|0|0', '0|0|0|0|0|0', '0|0|0|0|0|0', 'SELECT

CONCAT(
''Version '',
visite.programmation_version_creation
) as version,

    visite_etat.libelle as visite_etat,
    dossier_instruction.libelle dossier_instruction,
    acteur.nom_prenom as technicien,

CONCAT(
    to_char(visite.date_visite, ''DD/MM/YYYY''), ''
'',
    visite.heure_debut,
'' - '',
    visite.heure_fin
) as visite,

    CONCAT(
        etablissement.code, 
        '' - '', 
        etablissement.libelle, ''
'', etablissement.adresse_numero, 
        '' '', 
        etablissement.adresse_numero2, 
        '' '', 
        voie.libelle,
        '' '', 
        etablissement.adresse_cp, 
        '' ('', 
        arrondissement.libelle,
        '')''
    ) as etablissement

FROM
    &DB_PREFIXEvisite
LEFT JOIN 
    &DB_PREFIXEdossier_instruction
    ON visite.dossier_instruction = dossier_instruction.dossier_instruction
LEFT JOIN 
    &DB_PREFIXEacteur
    ON visite.acteur = acteur.acteur
LEFT JOIN 
    &DB_PREFIXEdossier_coordination
    ON dossier_instruction.dossier_coordination = dossier_coordination.dossier_coordination
LEFT JOIN 
    &DB_PREFIXEetablissement
    ON dossier_coordination.etablissement = etablissement.etablissement
LEFT JOIN 
    &DB_PREFIXEvoie
    ON etablissement.adresse_voie = voie.voie
LEFT JOIN 
    &DB_PREFIXEarrondissement
    ON etablissement.adresse_arrondissement = arrondissement.arrondissement
LEFT JOIN
    &DB_PREFIXEvisite_etat
    ON visite.visite_etat = visite_etat.visite_etat
LEFT JOIN 
    &DB_PREFIXEcontact
    ON etablissement.etablissement = contact.etablissement
LEFT JOIN
    &DB_PREFIXEcontact_type
    ON contact.contact_type = contact_type.contact_type
LEFT JOIN
    &DB_PREFIXEcontact_civilite
    ON contact.civilite = contact_civilite.contact_civilite
WHERE
    visite.programmation = &idx
    AND visite.programmation_version_annulation IS NULL
    AND (
LOWER(contact_type.code) = LOWER(''EXPL'')
OR 
contact_type.code is null
)
UNION
SELECT

CONCAT(
''Version '',
visite.programmation_version_annulation
) as version,

    visite_etat.libelle as visite_etat,
    dossier_instruction.libelle dossier_instruction,
    acteur.nom_prenom as technicien,

CONCAT(
    to_char(visite.date_visite, ''DD/MM/YYYY''), ''
'',
    visite.heure_debut,
'' - '',
    visite.heure_fin
) as visite,

    CONCAT(
        etablissement.code, 
        '' - '', 
        etablissement.libelle, ''
'', etablissement.adresse_numero, 
        '' '', 
        etablissement.adresse_numero2, 
        '' '', 
        voie.libelle,
        '' '', 
        etablissement.adresse_cp, 
        '' ('', 
        arrondissement.libelle,
        '')''
    ) as etablissement
FROM
    &DB_PREFIXEvisite
LEFT JOIN 
    &DB_PREFIXEdossier_instruction
    ON visite.dossier_instruction = dossier_instruction.dossier_instruction
LEFT JOIN 
    &DB_PREFIXEacteur
    ON visite.acteur = acteur.acteur
LEFT JOIN 
    &DB_PREFIXEdossier_coordination
    ON dossier_instruction.dossier_coordination = dossier_coordination.dossier_coordination
LEFT JOIN 
    &DB_PREFIXEetablissement
    ON dossier_coordination.etablissement = etablissement.etablissement
LEFT JOIN 
    &DB_PREFIXEvoie
    ON etablissement.adresse_voie = voie.voie
LEFT JOIN 
    &DB_PREFIXEarrondissement
    ON etablissement.adresse_arrondissement = arrondissement.arrondissement
LEFT JOIN
    &DB_PREFIXEvisite_etat
    ON visite.visite_etat = visite_etat.visite_etat
LEFT JOIN 
    &DB_PREFIXEcontact
    ON etablissement.etablissement = contact.etablissement
LEFT JOIN
    &DB_PREFIXEcontact_type
    ON contact.contact_type = contact_type.contact_type
LEFT JOIN
    &DB_PREFIXEcontact_civilite
    ON contact.civilite = contact_civilite.contact_civilite
WHERE
    visite.programmation = &idx
    AND visite.programmation_version_annulation IS NOT NULL
    AND (
LOWER(contact_type.code) = LOWER(''EXPL'')
OR 
contact_type.code is null
)');
INSERT INTO om_sousetat (om_sousetat, om_collectivite, id, libelle, actif, titre, titrehauteur, titrefont, titreattribut, titretaille, titrebordure, titrealign, titrefond, titrefondcouleur, titretextecouleur, intervalle_debut, intervalle_fin, entete_flag, entete_fond, entete_orientation, entete_hauteur, entetecolone_bordure, entetecolone_align, entete_fondcouleur, entete_textecouleur, tableau_largeur, tableau_bordure, tableau_fontaille, bordure_couleur, se_fond1, se_fond2, cellule_fond, cellule_hauteur, cellule_largeur, cellule_bordure_un, cellule_bordure, cellule_align, cellule_fond_total, cellule_fontaille_total, cellule_hauteur_total, cellule_fondcouleur_total, cellule_bordure_total, cellule_align_total, cellule_fond_moyenne, cellule_fontaille_moyenne, cellule_hauteur_moyenne, cellule_fondcouleur_moyenne, cellule_bordure_moyenne, cellule_align_moyenne, cellule_fond_nbr, cellule_fontaille_nbr, cellule_hauteur_nbr, cellule_fondcouleur_nbr, cellule_bordure_nbr, cellule_align_nbr, cellule_numerique, cellule_total, cellule_moyenne, cellule_compteur, om_sql) VALUES (5, 1, 'reu_crg_cca', 'Réunion - Compte-rendu général CCA', true, '...', 10, 'helvetica', 'B', 12, '0', 'L', '0', '255-255-255', '0-0-0', 10, 15, '1', '1', '0|0|0|0', 15, 'LTBR|LTBR|LTBR|LTBR', 'C|C|C|C', '188-188-188', '0-0-0', 195, '1', 10, '0-0-0', '255-255-255', '239-239-239', '1', 10, '15|70|70|120', 'LTBR|LTBR|LTBR|LTBR', 'LTBR|LTBR|LTBR|LTBR', 'C|L|L|L', '1', 10, 15, '196-213-213', 'LTBR|LTBR|LTBR|LTBR', 'C|L|L|L', '1', 10, 15, '196-213-213', 'LTBR|LTBR|LTBR|LTBR', 'C|L|L|L', '1', 10, 15, '196-213-213', 'LTBR|LTBR|LTBR|LTBR', 'C|L|L|L', '999|999|999|999', '0|0|0|0', '0|0|0|0', '0|0|0|0', 'SELECT
    ---- Numéro
    dossier_instruction_reunion.ordre as n°,
    ---- Établissement
    CONCAT_WS(''
'',
        -- Ligne 1
        CONCAT(etablissement.code, '' - '', etablissement.libelle, '' [ '', etablissement_categorie.libelle, '' / '', etablissement_type.libelle ,'' ]''),
        -- Ligne 2
        CONCAT_WS('' '', etablissement.adresse_numero, etablissement.adresse_numero2, voie.libelle),
        -- Ligne 3
        CONCAT_WS('' '', etablissement.adresse_cp, etablissement.adresse_ville)
    ) as établissement,
    ---- Dossier
    CONCAT_WS(''
'',
        -- Ligne 1
        dossier_coordination_type.libelle,
        -- Ligne 2
        CONCAT(dossier_instruction.libelle, '' / '', dossier_coordination.dossier_instruction_ads),
        -- Ligne 3
        CONCAT(''Demande du  : '', to_char(dossier_coordination.date_demande, ''DD/MM/YYYY'')),
        -- Ligne 4
        CONCAT(''=> '', acteur.acronyme)
    ) as dossier,
    ---- Avis (colonne vide)
    CONCAT_WS(''
'',
        -- Ligne 1
        reunion_avis0.libelle,
        -- Ligne 2
        dossier_instruction_reunion.avis_complement
    ) as avis
FROM
    &DB_PREFIXEdossier_instruction_reunion
        LEFT JOIN &DB_PREFIXEdossier_instruction
            ON dossier_instruction_reunion.dossier_instruction = dossier_instruction.dossier_instruction
        LEFT JOIN &DB_PREFIXEdossier_coordination
            ON dossier_instruction.dossier_coordination = dossier_coordination.dossier_coordination
        LEFT JOIN &DB_PREFIXEdossier_coordination_type
            ON dossier_coordination.dossier_coordination_type = dossier_coordination_type.dossier_coordination_type
        LEFT JOIN &DB_PREFIXEetablissement
            ON dossier_coordination.etablissement = etablissement.etablissement
        LEFT JOIN &DB_PREFIXEvoie
            ON etablissement.adresse_voie = voie.voie
        LEFT JOIN &DB_PREFIXEetablissement_type
            ON etablissement.etablissement_type = etablissement_type.etablissement_type
        LEFT JOIN &DB_PREFIXEetablissement_categorie
            ON etablissement.etablissement_categorie = etablissement_categorie.etablissement_categorie
        LEFT JOIN &DB_PREFIXEacteur
            ON dossier_instruction.technicien = acteur.acteur
        LEFT JOIN &DB_PREFIXEreunion_categorie
            ON dossier_instruction_reunion.reunion_type_categorie=reunion_categorie.reunion_categorie
        LEFT JOIN &DB_PREFIXEreunion_avis as reunion_avis0 
            ON dossier_instruction_reunion.avis=reunion_avis0.reunion_avis 
        LEFT JOIN &DB_PREFIXEreunion_avis as reunion_avis2 
            ON dossier_instruction_reunion.proposition_avis=reunion_avis2.reunion_avis
WHERE
    dossier_instruction_reunion.reunion = &idx
ORDER BY
    reunion_categorie.ordre, dossier_instruction_reunion.ordre');
INSERT INTO om_sousetat (om_sousetat, om_collectivite, id, libelle, actif, titre, titrehauteur, titrefont, titreattribut, titretaille, titrebordure, titrealign, titrefond, titrefondcouleur, titretextecouleur, intervalle_debut, intervalle_fin, entete_flag, entete_fond, entete_orientation, entete_hauteur, entetecolone_bordure, entetecolone_align, entete_fondcouleur, entete_textecouleur, tableau_largeur, tableau_bordure, tableau_fontaille, bordure_couleur, se_fond1, se_fond2, cellule_fond, cellule_hauteur, cellule_largeur, cellule_bordure_un, cellule_bordure, cellule_align, cellule_fond_total, cellule_fontaille_total, cellule_hauteur_total, cellule_fondcouleur_total, cellule_bordure_total, cellule_align_total, cellule_fond_moyenne, cellule_fontaille_moyenne, cellule_hauteur_moyenne, cellule_fondcouleur_moyenne, cellule_bordure_moyenne, cellule_align_moyenne, cellule_fond_nbr, cellule_fontaille_nbr, cellule_hauteur_nbr, cellule_fondcouleur_nbr, cellule_bordure_nbr, cellule_align_nbr, cellule_numerique, cellule_total, cellule_moyenne, cellule_compteur, om_sql) VALUES (13, 1, 'reu_crg_ccs_plen_enjeux', 'Réunion - Compte-rendu général CCS - Enjeux', true, 'Enjeux', 10, 'helvetica', 'B', 12, '0', 'L', '0', '255-255-255', '0-0-0', 10, 15, '1', '1', '0|0|0|0', 15, 'LTBR|LTBR|LTBR|LTBR', 'C|C|C|C', '188-188-188', '0-0-0', 195, '1', 10, '0-0-0', '255-255-255', '239-239-239', '1', 10, '15|70|70|120', 'LTBR|LTBR|LTBR|LTBR', 'LTBR|LTBR|LTBR|LTBR', 'C|L|L|L', '1', 10, 15, '196-213-213', 'LTBR|LTBR|LTBR|LTBR', 'C|L|L|L', '1', 10, 15, '196-213-213', 'LTBR|LTBR|LTBR|LTBR', 'C|L|L|L', '1', 10, 15, '196-213-213', 'LTBR|LTBR|LTBR|LTBR', 'C|L|L|L', '999|999|999|999', '0|0|0|0', '0|0|0|0', '0|0|0|0', 'SELECT
    ---- Numéro
    dossier_instruction_reunion.ordre as n°,
    ---- Établissement
    CONCAT_WS(''
'',
        -- Ligne 1
        CONCAT(etablissement.code, '' - '', etablissement.libelle, '' [ '', etablissement_categorie.libelle, '' / '', etablissement_type.libelle ,'' ]''),
        -- Ligne 2
        CONCAT_WS('' '', etablissement.adresse_numero, etablissement.adresse_numero2, voie.libelle),
        -- Ligne 3
        CONCAT_WS('' '', etablissement.adresse_cp, etablissement.adresse_ville)
    ) as établissement,
    ---- Dossier
    CONCAT_WS(''
'',
        -- Ligne 1
        dossier_coordination_type.libelle,
        -- Ligne 2
        CONCAT(dossier_instruction.libelle, '' / '', dossier_coordination.dossier_instruction_ads),
        -- Ligne 3
        CONCAT(''Demande du  : '', to_char(dossier_coordination.date_demande, ''DD/MM/YYYY'')),
        -- Ligne 4
        CONCAT(''=> '', acteur.acronyme)
    ) as dossier,
    ---- Avis (colonne vide)
    CONCAT_WS(''
'',
        -- Ligne 1
        reunion_avis0.libelle,
        -- Ligne 2
        dossier_instruction_reunion.avis_complement
    ) as avis
FROM
    &DB_PREFIXEdossier_instruction_reunion
        LEFT JOIN &DB_PREFIXEdossier_instruction
            ON dossier_instruction_reunion.dossier_instruction = dossier_instruction.dossier_instruction
        LEFT JOIN &DB_PREFIXEdossier_coordination
            ON dossier_instruction.dossier_coordination = dossier_coordination.dossier_coordination
        LEFT JOIN &DB_PREFIXEdossier_coordination_type
            ON dossier_coordination.dossier_coordination_type = dossier_coordination_type.dossier_coordination_type
        LEFT JOIN &DB_PREFIXEetablissement
            ON dossier_coordination.etablissement = etablissement.etablissement
        LEFT JOIN &DB_PREFIXEvoie
            ON etablissement.adresse_voie = voie.voie
        LEFT JOIN &DB_PREFIXEetablissement_type
            ON etablissement.etablissement_type = etablissement_type.etablissement_type
        LEFT JOIN &DB_PREFIXEetablissement_categorie
            ON etablissement.etablissement_categorie = etablissement_categorie.etablissement_categorie
        LEFT JOIN &DB_PREFIXEacteur
            ON dossier_instruction.technicien = acteur.acteur
        LEFT JOIN &DB_PREFIXEreunion_categorie
            ON dossier_instruction_reunion.reunion_type_categorie=reunion_categorie.reunion_categorie
        LEFT JOIN &DB_PREFIXEreunion_avis as reunion_avis0 
            ON dossier_instruction_reunion.avis=reunion_avis0.reunion_avis 
        LEFT JOIN &DB_PREFIXEreunion_avis as reunion_avis2 
            ON dossier_instruction_reunion.proposition_avis=reunion_avis2.reunion_avis
WHERE
    dossier_instruction_reunion.reunion = &idx
    AND lower(reunion_categorie.code) = ''enjeux''
ORDER BY
    reunion_categorie.ordre, dossier_instruction_reunion.ordre');
INSERT INTO om_sousetat (om_sousetat, om_collectivite, id, libelle, actif, titre, titrehauteur, titrefont, titreattribut, titretaille, titrebordure, titrealign, titrefond, titrefondcouleur, titretextecouleur, intervalle_debut, intervalle_fin, entete_flag, entete_fond, entete_orientation, entete_hauteur, entetecolone_bordure, entetecolone_align, entete_fondcouleur, entete_textecouleur, tableau_largeur, tableau_bordure, tableau_fontaille, bordure_couleur, se_fond1, se_fond2, cellule_fond, cellule_hauteur, cellule_largeur, cellule_bordure_un, cellule_bordure, cellule_align, cellule_fond_total, cellule_fontaille_total, cellule_hauteur_total, cellule_fondcouleur_total, cellule_bordure_total, cellule_align_total, cellule_fond_moyenne, cellule_fontaille_moyenne, cellule_hauteur_moyenne, cellule_fondcouleur_moyenne, cellule_bordure_moyenne, cellule_align_moyenne, cellule_fond_nbr, cellule_fontaille_nbr, cellule_hauteur_nbr, cellule_fondcouleur_nbr, cellule_bordure_nbr, cellule_align_nbr, cellule_numerique, cellule_total, cellule_moyenne, cellule_compteur, om_sql) VALUES (12, 1, 'reu_crg_ccs_plen_apmed', 'Réunion - Compte-rendu général CCS - Mises en demeure', true, 'Mises en demeure', 10, 'helvetica', 'B', 12, '0', 'L', '0', '255-255-255', '0-0-0', 10, 15, '1', '1', '0|0|0|0', 15, 'LTBR|LTBR|LTBR|LTBR', 'C|C|C|C', '188-188-188', '0-0-0', 195, '1', 10, '0-0-0', '255-255-255', '239-239-239', '1', 10, '15|70|70|120', 'LTBR|LTBR|LTBR|LTBR', 'LTBR|LTBR|LTBR|LTBR', 'C|L|L|L', '1', 10, 15, '196-213-213', 'LTBR|LTBR|LTBR|LTBR', 'C|L|L|L', '1', 10, 15, '196-213-213', 'LTBR|LTBR|LTBR|LTBR', 'C|L|L|L', '1', 10, 15, '196-213-213', 'LTBR|LTBR|LTBR|LTBR', 'C|L|L|L', '999|999|999|999', '0|0|0|0', '0|0|0|0', '0|0|0|0', 'SELECT
    ---- Numéro
    dossier_instruction_reunion.ordre as n°,
    ---- Établissement
    CONCAT_WS(''
'',
        -- Ligne 1
        CONCAT(etablissement.code, '' - '', etablissement.libelle, '' [ '', etablissement_categorie.libelle, '' / '', etablissement_type.libelle ,'' ]''),
        -- Ligne 2
        CONCAT_WS('' '', etablissement.adresse_numero, etablissement.adresse_numero2, voie.libelle),
        -- Ligne 3
        CONCAT_WS('' '', etablissement.adresse_cp, etablissement.adresse_ville)
    ) as établissement,
    ---- Dossier
    CONCAT_WS(''
'',
        -- Ligne 1
        dossier_coordination_type.libelle,
        -- Ligne 2
        CONCAT(dossier_instruction.libelle, '' / '', dossier_coordination.dossier_instruction_ads),
        -- Ligne 3
        CONCAT(''Demande du  : '', to_char(dossier_coordination.date_demande, ''DD/MM/YYYY'')),
        -- Ligne 4
        CONCAT(''=> '', acteur.acronyme)
    ) as dossier,
    ---- Avis (colonne vide)
    CONCAT_WS(''
'',
        -- Ligne 1
        reunion_avis0.libelle,
        -- Ligne 2
        dossier_instruction_reunion.avis_complement
    ) as avis
FROM
    &DB_PREFIXEdossier_instruction_reunion
        LEFT JOIN &DB_PREFIXEdossier_instruction
            ON dossier_instruction_reunion.dossier_instruction = dossier_instruction.dossier_instruction
        LEFT JOIN &DB_PREFIXEdossier_coordination
            ON dossier_instruction.dossier_coordination = dossier_coordination.dossier_coordination
        LEFT JOIN &DB_PREFIXEdossier_coordination_type
            ON dossier_coordination.dossier_coordination_type = dossier_coordination_type.dossier_coordination_type
        LEFT JOIN &DB_PREFIXEetablissement
            ON dossier_coordination.etablissement = etablissement.etablissement
        LEFT JOIN &DB_PREFIXEvoie
            ON etablissement.adresse_voie = voie.voie
        LEFT JOIN &DB_PREFIXEetablissement_type
            ON etablissement.etablissement_type = etablissement_type.etablissement_type
        LEFT JOIN &DB_PREFIXEetablissement_categorie
            ON etablissement.etablissement_categorie = etablissement_categorie.etablissement_categorie
        LEFT JOIN &DB_PREFIXEacteur
            ON dossier_instruction.technicien = acteur.acteur
        LEFT JOIN &DB_PREFIXEreunion_categorie
            ON dossier_instruction_reunion.reunion_type_categorie=reunion_categorie.reunion_categorie
        LEFT JOIN &DB_PREFIXEreunion_avis as reunion_avis0 
            ON dossier_instruction_reunion.avis=reunion_avis0.reunion_avis 
        LEFT JOIN &DB_PREFIXEreunion_avis as reunion_avis2 
            ON dossier_instruction_reunion.proposition_avis=reunion_avis2.reunion_avis
WHERE
    dossier_instruction_reunion.reunion = &idx
    AND lower(reunion_categorie.code) = ''ap_med''
ORDER BY
    reunion_categorie.ordre, dossier_instruction_reunion.ordre');
INSERT INTO om_sousetat (om_sousetat, om_collectivite, id, libelle, actif, titre, titrehauteur, titrefont, titreattribut, titretaille, titrebordure, titrealign, titrefond, titrefondcouleur, titretextecouleur, intervalle_debut, intervalle_fin, entete_flag, entete_fond, entete_orientation, entete_hauteur, entetecolone_bordure, entetecolone_align, entete_fondcouleur, entete_textecouleur, tableau_largeur, tableau_bordure, tableau_fontaille, bordure_couleur, se_fond1, se_fond2, cellule_fond, cellule_hauteur, cellule_largeur, cellule_bordure_un, cellule_bordure, cellule_align, cellule_fond_total, cellule_fontaille_total, cellule_hauteur_total, cellule_fondcouleur_total, cellule_bordure_total, cellule_align_total, cellule_fond_moyenne, cellule_fontaille_moyenne, cellule_hauteur_moyenne, cellule_fondcouleur_moyenne, cellule_bordure_moyenne, cellule_align_moyenne, cellule_fond_nbr, cellule_fontaille_nbr, cellule_hauteur_nbr, cellule_fondcouleur_nbr, cellule_bordure_nbr, cellule_align_nbr, cellule_numerique, cellule_total, cellule_moyenne, cellule_compteur, om_sql) VALUES (15, 1, 'reu_crg_ccs_plen_plans', 'Réunion - Compte-rendu général CCS - Plans', true, 'Plans', 10, 'helvetica', 'B', 12, '0', 'L', '0', '255-255-255', '0-0-0', 10, 15, '1', '1', '0|0|0|0', 15, 'LTBR|LTBR|LTBR|LTBR', 'C|C|C|C', '188-188-188', '0-0-0', 195, '1', 10, '0-0-0', '255-255-255', '239-239-239', '1', 10, '15|70|70|120', 'LTBR|LTBR|LTBR|LTBR', 'LTBR|LTBR|LTBR|LTBR', 'C|L|L|L', '1', 10, 15, '196-213-213', 'LTBR|LTBR|LTBR|LTBR', 'C|L|L|L', '1', 10, 15, '196-213-213', 'LTBR|LTBR|LTBR|LTBR', 'C|L|L|L', '1', 10, 15, '196-213-213', 'LTBR|LTBR|LTBR|LTBR', 'C|L|L|L', '999|999|999|999', '0|0|0|0', '0|0|0|0', '0|0|0|0', 'SELECT
    ---- Numéro
    dossier_instruction_reunion.ordre as n°,
    ---- Établissement
    CONCAT_WS(''
'',
        -- Ligne 1
        CONCAT(etablissement.code, '' - '', etablissement.libelle, '' [ '', etablissement_categorie.libelle, '' / '', etablissement_type.libelle ,'' ]''),
        -- Ligne 2
        CONCAT_WS('' '', etablissement.adresse_numero, etablissement.adresse_numero2, voie.libelle),
        -- Ligne 3
        CONCAT_WS('' '', etablissement.adresse_cp, etablissement.adresse_ville)
    ) as établissement,
    ---- Dossier
    CONCAT_WS(''
'',
        -- Ligne 1
        dossier_coordination_type.libelle,
        -- Ligne 2
        CONCAT(dossier_instruction.libelle, '' / '', dossier_coordination.dossier_instruction_ads),
        -- Ligne 3
        CONCAT(''Demande du  : '', to_char(dossier_coordination.date_demande, ''DD/MM/YYYY'')),
        -- Ligne 4
        CONCAT(''=> '', acteur.acronyme)
    ) as dossier,
    ---- Avis (colonne vide)
    CONCAT_WS(''
'',
        -- Ligne 1
        reunion_avis0.libelle,
        -- Ligne 2
        dossier_instruction_reunion.avis_complement
    ) as avis
FROM
    &DB_PREFIXEdossier_instruction_reunion
        LEFT JOIN &DB_PREFIXEdossier_instruction
            ON dossier_instruction_reunion.dossier_instruction = dossier_instruction.dossier_instruction
        LEFT JOIN &DB_PREFIXEdossier_coordination
            ON dossier_instruction.dossier_coordination = dossier_coordination.dossier_coordination
        LEFT JOIN &DB_PREFIXEdossier_coordination_type
            ON dossier_coordination.dossier_coordination_type = dossier_coordination_type.dossier_coordination_type
        LEFT JOIN &DB_PREFIXEetablissement
            ON dossier_coordination.etablissement = etablissement.etablissement
        LEFT JOIN &DB_PREFIXEvoie
            ON etablissement.adresse_voie = voie.voie
        LEFT JOIN &DB_PREFIXEetablissement_type
            ON etablissement.etablissement_type = etablissement_type.etablissement_type
        LEFT JOIN &DB_PREFIXEetablissement_categorie
            ON etablissement.etablissement_categorie = etablissement_categorie.etablissement_categorie
        LEFT JOIN &DB_PREFIXEacteur
            ON dossier_instruction.technicien = acteur.acteur
        LEFT JOIN &DB_PREFIXEreunion_categorie
            ON dossier_instruction_reunion.reunion_type_categorie=reunion_categorie.reunion_categorie
        LEFT JOIN &DB_PREFIXEreunion_avis as reunion_avis0 
            ON dossier_instruction_reunion.avis=reunion_avis0.reunion_avis 
        LEFT JOIN &DB_PREFIXEreunion_avis as reunion_avis2 
            ON dossier_instruction_reunion.proposition_avis=reunion_avis2.reunion_avis
WHERE
    dossier_instruction_reunion.reunion = &idx
    AND lower(reunion_categorie.code) = ''plans''
ORDER BY
    reunion_categorie.ordre, dossier_instruction_reunion.ordre');
INSERT INTO om_sousetat (om_sousetat, om_collectivite, id, libelle, actif, titre, titrehauteur, titrefont, titreattribut, titretaille, titrebordure, titrealign, titrefond, titrefondcouleur, titretextecouleur, intervalle_debut, intervalle_fin, entete_flag, entete_fond, entete_orientation, entete_hauteur, entetecolone_bordure, entetecolone_align, entete_fondcouleur, entete_textecouleur, tableau_largeur, tableau_bordure, tableau_fontaille, bordure_couleur, se_fond1, se_fond2, cellule_fond, cellule_hauteur, cellule_largeur, cellule_bordure_un, cellule_bordure, cellule_align, cellule_fond_total, cellule_fontaille_total, cellule_hauteur_total, cellule_fondcouleur_total, cellule_bordure_total, cellule_align_total, cellule_fond_moyenne, cellule_fontaille_moyenne, cellule_hauteur_moyenne, cellule_fondcouleur_moyenne, cellule_bordure_moyenne, cellule_align_moyenne, cellule_fond_nbr, cellule_fontaille_nbr, cellule_hauteur_nbr, cellule_fondcouleur_nbr, cellule_bordure_nbr, cellule_align_nbr, cellule_numerique, cellule_total, cellule_moyenne, cellule_compteur, om_sql) VALUES (17, 1, 'reu_crg_ccs_plen_apscds', 'Réunion - Compte-rendu général CCS - Sous Commission Départementale', true, 'Sous Commission Départementale', 10, 'helvetica', 'B', 12, '0', 'L', '0', '255-255-255', '0-0-0', 10, 15, '1', '1', '0|0|0|0', 15, 'LTBR|LTBR|LTBR|LTBR', 'C|C|C|C', '188-188-188', '0-0-0', 195, '1', 10, '0-0-0', '255-255-255', '239-239-239', '1', 10, '15|70|70|120', 'LTBR|LTBR|LTBR|LTBR', 'LTBR|LTBR|LTBR|LTBR', 'C|L|L|L', '1', 10, 15, '196-213-213', 'LTBR|LTBR|LTBR|LTBR', 'C|L|L|L', '1', 10, 15, '196-213-213', 'LTBR|LTBR|LTBR|LTBR', 'C|L|L|L', '1', 10, 15, '196-213-213', 'LTBR|LTBR|LTBR|LTBR', 'C|L|L|L', '999|999|999|999', '0|0|0|0', '0|0|0|0', '0|0|0|0', 'SELECT
    ---- Numéro
    dossier_instruction_reunion.ordre as n°,
    ---- Établissement
    CONCAT_WS(''
'',
        -- Ligne 1
        CONCAT(etablissement.code, '' - '', etablissement.libelle, '' [ '', etablissement_categorie.libelle, '' / '', etablissement_type.libelle ,'' ]''),
        -- Ligne 2
        CONCAT_WS('' '', etablissement.adresse_numero, etablissement.adresse_numero2, voie.libelle),
        -- Ligne 3
        CONCAT_WS('' '', etablissement.adresse_cp, etablissement.adresse_ville)
    ) as établissement,
    ---- Dossier
    CONCAT_WS(''
'',
        -- Ligne 1
        dossier_coordination_type.libelle,
        -- Ligne 2
        CONCAT(dossier_instruction.libelle, '' / '', dossier_coordination.dossier_instruction_ads),
        -- Ligne 3
        CONCAT(''Demande du  : '', to_char(dossier_coordination.date_demande, ''DD/MM/YYYY'')),
        -- Ligne 4
        CONCAT(''=> '', acteur.acronyme)
    ) as dossier,
    ---- Avis (colonne vide)
    CONCAT_WS(''
'',
        -- Ligne 1
        reunion_avis0.libelle,
        -- Ligne 2
        dossier_instruction_reunion.avis_complement
    ) as avis
FROM
    &DB_PREFIXEdossier_instruction_reunion
        LEFT JOIN &DB_PREFIXEdossier_instruction
            ON dossier_instruction_reunion.dossier_instruction = dossier_instruction.dossier_instruction
        LEFT JOIN &DB_PREFIXEdossier_coordination
            ON dossier_instruction.dossier_coordination = dossier_coordination.dossier_coordination
        LEFT JOIN &DB_PREFIXEdossier_coordination_type
            ON dossier_coordination.dossier_coordination_type = dossier_coordination_type.dossier_coordination_type
        LEFT JOIN &DB_PREFIXEetablissement
            ON dossier_coordination.etablissement = etablissement.etablissement
        LEFT JOIN &DB_PREFIXEvoie
            ON etablissement.adresse_voie = voie.voie
        LEFT JOIN &DB_PREFIXEetablissement_type
            ON etablissement.etablissement_type = etablissement_type.etablissement_type
        LEFT JOIN &DB_PREFIXEetablissement_categorie
            ON etablissement.etablissement_categorie = etablissement_categorie.etablissement_categorie
        LEFT JOIN &DB_PREFIXEacteur
            ON dossier_instruction.technicien = acteur.acteur
        LEFT JOIN &DB_PREFIXEreunion_categorie
            ON dossier_instruction_reunion.reunion_type_categorie=reunion_categorie.reunion_categorie
        LEFT JOIN &DB_PREFIXEreunion_avis as reunion_avis0 
            ON dossier_instruction_reunion.avis=reunion_avis0.reunion_avis 
        LEFT JOIN &DB_PREFIXEreunion_avis as reunion_avis2 
            ON dossier_instruction_reunion.proposition_avis=reunion_avis2.reunion_avis
WHERE
    dossier_instruction_reunion.reunion = &idx
    AND lower(reunion_categorie.code) = ''ap_scds''
ORDER BY
    reunion_categorie.ordre, dossier_instruction_reunion.ordre');
INSERT INTO om_sousetat (om_sousetat, om_collectivite, id, libelle, actif, titre, titrehauteur, titrefont, titreattribut, titretaille, titrebordure, titrealign, titrefond, titrefondcouleur, titretextecouleur, intervalle_debut, intervalle_fin, entete_flag, entete_fond, entete_orientation, entete_hauteur, entetecolone_bordure, entetecolone_align, entete_fondcouleur, entete_textecouleur, tableau_largeur, tableau_bordure, tableau_fontaille, bordure_couleur, se_fond1, se_fond2, cellule_fond, cellule_hauteur, cellule_largeur, cellule_bordure_un, cellule_bordure, cellule_align, cellule_fond_total, cellule_fontaille_total, cellule_hauteur_total, cellule_fondcouleur_total, cellule_bordure_total, cellule_align_total, cellule_fond_moyenne, cellule_fontaille_moyenne, cellule_hauteur_moyenne, cellule_fondcouleur_moyenne, cellule_bordure_moyenne, cellule_align_moyenne, cellule_fond_nbr, cellule_fontaille_nbr, cellule_hauteur_nbr, cellule_fondcouleur_nbr, cellule_bordure_nbr, cellule_align_nbr, cellule_numerique, cellule_total, cellule_moyenne, cellule_compteur, om_sql) VALUES (16, 1, 'reu_crg_ccs_plen_visites', 'Réunion - Compte-rendu général CCS - Visites', true, 'Visites', 10, 'helvetica', 'B', 12, '0', 'L', '0', '255-255-255', '0-0-0', 10, 15, '1', '1', '0|0|0|0', 15, 'LTBR|LTBR|LTBR|LTBR', 'C|C|C|C', '188-188-188', '0-0-0', 195, '1', 10, '0-0-0', '255-255-255', '239-239-239', '1', 10, '15|70|70|120', 'LTBR|LTBR|LTBR|LTBR', 'LTBR|LTBR|LTBR|LTBR', 'C|L|L|L', '1', 10, 15, '196-213-213', 'LTBR|LTBR|LTBR|LTBR', 'C|L|L|L', '1', 10, 15, '196-213-213', 'LTBR|LTBR|LTBR|LTBR', 'C|L|L|L', '1', 10, 15, '196-213-213', 'LTBR|LTBR|LTBR|LTBR', 'C|L|L|L', '999|999|999|999', '0|0|0|0', '0|0|0|0', '0|0|0|0', 'SELECT
    ---- Numéro
    dossier_instruction_reunion.ordre as n°,
    ---- Établissement
    CONCAT_WS(''
'',
        -- Ligne 1
        CONCAT(etablissement.code, '' - '', etablissement.libelle, '' [ '', etablissement_categorie.libelle, '' / '', etablissement_type.libelle ,'' ]''),
        -- Ligne 2
        CONCAT_WS('' '', etablissement.adresse_numero, etablissement.adresse_numero2, voie.libelle),
        -- Ligne 3
        CONCAT_WS('' '', etablissement.adresse_cp, etablissement.adresse_ville)
    ) as établissement,
    ---- Dossier
    CONCAT_WS(''
'',
        -- Ligne 1
        dossier_coordination_type.libelle,
        -- Ligne 2
        CONCAT(dossier_instruction.libelle, '' / '', dossier_coordination.dossier_instruction_ads),
        -- Ligne 3
        CONCAT(''Demande du  : '', to_char(dossier_coordination.date_demande, ''DD/MM/YYYY'')),
        -- Ligne 4
        CONCAT(''=> '', acteur.acronyme)
    ) as dossier,
    ---- Avis (colonne vide)
    CONCAT_WS(''
'',
        -- Ligne 1
        reunion_avis0.libelle,
        -- Ligne 2
        dossier_instruction_reunion.avis_complement
    ) as avis
FROM
    &DB_PREFIXEdossier_instruction_reunion
        LEFT JOIN &DB_PREFIXEdossier_instruction
            ON dossier_instruction_reunion.dossier_instruction = dossier_instruction.dossier_instruction
        LEFT JOIN &DB_PREFIXEdossier_coordination
            ON dossier_instruction.dossier_coordination = dossier_coordination.dossier_coordination
        LEFT JOIN &DB_PREFIXEdossier_coordination_type
            ON dossier_coordination.dossier_coordination_type = dossier_coordination_type.dossier_coordination_type
        LEFT JOIN &DB_PREFIXEetablissement
            ON dossier_coordination.etablissement = etablissement.etablissement
        LEFT JOIN &DB_PREFIXEvoie
            ON etablissement.adresse_voie = voie.voie
        LEFT JOIN &DB_PREFIXEetablissement_type
            ON etablissement.etablissement_type = etablissement_type.etablissement_type
        LEFT JOIN &DB_PREFIXEetablissement_categorie
            ON etablissement.etablissement_categorie = etablissement_categorie.etablissement_categorie
        LEFT JOIN &DB_PREFIXEacteur
            ON dossier_instruction.technicien = acteur.acteur
        LEFT JOIN &DB_PREFIXEreunion_categorie
            ON dossier_instruction_reunion.reunion_type_categorie=reunion_categorie.reunion_categorie
        LEFT JOIN &DB_PREFIXEreunion_avis as reunion_avis0 
            ON dossier_instruction_reunion.avis=reunion_avis0.reunion_avis 
        LEFT JOIN &DB_PREFIXEreunion_avis as reunion_avis2 
            ON dossier_instruction_reunion.proposition_avis=reunion_avis2.reunion_avis
WHERE
    dossier_instruction_reunion.reunion = &idx
    AND lower(reunion_categorie.code) = ''visites''
ORDER BY
    reunion_categorie.ordre, dossier_instruction_reunion.ordre');
INSERT INTO om_sousetat (om_sousetat, om_collectivite, id, libelle, actif, titre, titrehauteur, titrefont, titreattribut, titretaille, titrebordure, titrealign, titrefond, titrefondcouleur, titretextecouleur, intervalle_debut, intervalle_fin, entete_flag, entete_fond, entete_orientation, entete_hauteur, entetecolone_bordure, entetecolone_align, entete_fondcouleur, entete_textecouleur, tableau_largeur, tableau_bordure, tableau_fontaille, bordure_couleur, se_fond1, se_fond2, cellule_fond, cellule_hauteur, cellule_largeur, cellule_bordure_un, cellule_bordure, cellule_align, cellule_fond_total, cellule_fontaille_total, cellule_hauteur_total, cellule_fondcouleur_total, cellule_bordure_total, cellule_align_total, cellule_fond_moyenne, cellule_fontaille_moyenne, cellule_hauteur_moyenne, cellule_fondcouleur_moyenne, cellule_bordure_moyenne, cellule_align_moyenne, cellule_fond_nbr, cellule_fontaille_nbr, cellule_hauteur_nbr, cellule_fondcouleur_nbr, cellule_bordure_nbr, cellule_align_nbr, cellule_numerique, cellule_total, cellule_moyenne, cellule_compteur, om_sql) VALUES (6, 1, 'reu_crg', 'Réunion - Compte-rendu général', true, '...', 10, 'helvetica', 'B', 12, '0', 'L', '0', '255-255-255', '0-0-0', 10, 15, '1', '1', '0|0|0|0', 15, 'LTBR|LTBR|LTBR|LTBR', 'C|C|C|C', '188-188-188', '0-0-0', 195, '1', 10, '0-0-0', '255-255-255', '239-239-239', '1', 10, '15|70|70|120', 'LTBR|LTBR|LTBR|LTBR', 'LTBR|LTBR|LTBR|LTBR', 'C|L|L|L', '1', 10, 15, '196-213-213', 'LTBR|LTBR|LTBR|LTBR', 'C|L|L|L', '1', 10, 15, '196-213-213', 'LTBR|LTBR|LTBR|LTBR', 'C|L|L|L', '1', 10, 15, '196-213-213', 'LTBR|LTBR|LTBR|LTBR', 'C|L|L|L', '999|999|999|999', '0|0|0|0', '0|0|0|0', '0|0|0|0', 'SELECT
    ---- Numéro
    dossier_instruction_reunion.ordre as n°,
    ---- Établissement
    CONCAT_WS(''
'',
        -- Ligne 1
        CONCAT(etablissement.code, '' - '', etablissement.libelle, '' [ '', etablissement_categorie.libelle, '' / '', etablissement_type.libelle ,'' ]''),
        -- Ligne 2
        CONCAT_WS('' '', etablissement.adresse_numero, etablissement.adresse_numero2, voie.libelle),
        -- Ligne 3
        CONCAT_WS('' '', etablissement.adresse_cp, etablissement.adresse_ville)
    ) as établissement,
    ---- Dossier
    CONCAT_WS(''
'',
        -- Ligne 1
        dossier_coordination_type.libelle,
        -- Ligne 2
        CONCAT(dossier_instruction.libelle, '' / '', dossier_coordination.dossier_instruction_ads),
        -- Ligne 3
        CONCAT(''Demande du  : '', to_char(dossier_coordination.date_demande, ''DD/MM/YYYY'')),
        -- Ligne 4
        CONCAT(''=> '', acteur.acronyme)
    ) as dossier,
    ---- Avis (colonne vide)
    CONCAT_WS(''
'',
        -- Ligne 1
        reunion_avis0.libelle,
        -- Ligne 2
        dossier_instruction_reunion.avis_complement
    ) as avis
FROM
    &DB_PREFIXEdossier_instruction_reunion
        LEFT JOIN &DB_PREFIXEdossier_instruction
            ON dossier_instruction_reunion.dossier_instruction = dossier_instruction.dossier_instruction
        LEFT JOIN &DB_PREFIXEdossier_coordination
            ON dossier_instruction.dossier_coordination = dossier_coordination.dossier_coordination
        LEFT JOIN &DB_PREFIXEdossier_coordination_type
            ON dossier_coordination.dossier_coordination_type = dossier_coordination_type.dossier_coordination_type
        LEFT JOIN &DB_PREFIXEetablissement
            ON dossier_coordination.etablissement = etablissement.etablissement
        LEFT JOIN &DB_PREFIXEvoie
            ON etablissement.adresse_voie = voie.voie
        LEFT JOIN &DB_PREFIXEetablissement_type
            ON etablissement.etablissement_type = etablissement_type.etablissement_type
        LEFT JOIN &DB_PREFIXEetablissement_categorie
            ON etablissement.etablissement_categorie = etablissement_categorie.etablissement_categorie
        LEFT JOIN &DB_PREFIXEacteur
            ON dossier_instruction.technicien = acteur.acteur
        LEFT JOIN &DB_PREFIXEreunion_categorie
            ON dossier_instruction_reunion.reunion_type_categorie=reunion_categorie.reunion_categorie
        LEFT JOIN &DB_PREFIXEreunion_avis as reunion_avis0 
            ON dossier_instruction_reunion.avis=reunion_avis0.reunion_avis 
        LEFT JOIN &DB_PREFIXEreunion_avis as reunion_avis2 
            ON dossier_instruction_reunion.proposition_avis=reunion_avis2.reunion_avis
WHERE
    dossier_instruction_reunion.reunion = &idx
ORDER BY
    reunion_categorie.ordre, dossier_instruction_reunion.ordre');
INSERT INTO om_sousetat (om_sousetat, om_collectivite, id, libelle, actif, titre, titrehauteur, titrefont, titreattribut, titretaille, titrebordure, titrealign, titrefond, titrefondcouleur, titretextecouleur, intervalle_debut, intervalle_fin, entete_flag, entete_fond, entete_orientation, entete_hauteur, entetecolone_bordure, entetecolone_align, entete_fondcouleur, entete_textecouleur, tableau_largeur, tableau_bordure, tableau_fontaille, bordure_couleur, se_fond1, se_fond2, cellule_fond, cellule_hauteur, cellule_largeur, cellule_bordure_un, cellule_bordure, cellule_align, cellule_fond_total, cellule_fontaille_total, cellule_hauteur_total, cellule_fondcouleur_total, cellule_bordure_total, cellule_align_total, cellule_fond_moyenne, cellule_fontaille_moyenne, cellule_hauteur_moyenne, cellule_fondcouleur_moyenne, cellule_bordure_moyenne, cellule_align_moyenne, cellule_fond_nbr, cellule_fontaille_nbr, cellule_hauteur_nbr, cellule_fondcouleur_nbr, cellule_bordure_nbr, cellule_align_nbr, cellule_numerique, cellule_total, cellule_moyenne, cellule_compteur, om_sql) VALUES (14, 1, 'reu_crg_ccs_plen_apdif', 'Réunion - Compte-rendu général CCS - Avis différés', true, 'Avis différés', 10, 'helvetica', 'B', 12, '0', 'L', '0', '255-255-255', '0-0-0', 10, 15, '1', '1', '0|0|0|0', 15, 'LTBR|LTBR|LTBR|LTBR', 'C|C|C|C', '188-188-188', '0-0-0', 195, '1', 10, '0-0-0', '255-255-255', '239-239-239', '1', 10, '15|70|70|120', 'LTBR|LTBR|LTBR|LTBR', 'LTBR|LTBR|LTBR|LTBR', 'C|L|L|L', '1', 10, 15, '196-213-213', 'LTBR|LTBR|LTBR|LTBR', 'C|L|L|L', '1', 10, 15, '196-213-213', 'LTBR|LTBR|LTBR|LTBR', 'C|L|L|L', '1', 10, 15, '196-213-213', 'LTBR|LTBR|LTBR|LTBR', 'C|L|L|L', '999|999|999|999', '0|0|0|0', '0|0|0|0', '0|0|0|0', 'SELECT
    ---- Numéro
    dossier_instruction_reunion.ordre as n°,
    ---- Établissement
    CONCAT_WS(''
'',
        -- Ligne 1
        CONCAT(etablissement.code, '' - '', etablissement.libelle, '' [ '', etablissement_categorie.libelle, '' / '', etablissement_type.libelle ,'' ]''),
        -- Ligne 2
        CONCAT_WS('' '', etablissement.adresse_numero, etablissement.adresse_numero2, voie.libelle),
        -- Ligne 3
        CONCAT_WS('' '', etablissement.adresse_cp, etablissement.adresse_ville)
    ) as établissement,
    ---- Dossier
    CONCAT_WS(''
'',
        -- Ligne 1
        dossier_coordination_type.libelle,
        -- Ligne 2
        CONCAT(dossier_instruction.libelle, '' / '', dossier_coordination.dossier_instruction_ads),
        -- Ligne 3
        CONCAT(''Demande du  : '', to_char(dossier_coordination.date_demande, ''DD/MM/YYYY'')),
        -- Ligne 4
        CONCAT(''=> '', acteur.acronyme)
    ) as dossier,
    ---- Avis (colonne vide)
    CONCAT_WS(''
'',
        -- Ligne 1
        reunion_avis0.libelle,
        -- Ligne 2
        dossier_instruction_reunion.avis_complement
    ) as avis
FROM
    &DB_PREFIXEdossier_instruction_reunion
        LEFT JOIN &DB_PREFIXEdossier_instruction
            ON dossier_instruction_reunion.dossier_instruction = dossier_instruction.dossier_instruction
        LEFT JOIN &DB_PREFIXEdossier_coordination
            ON dossier_instruction.dossier_coordination = dossier_coordination.dossier_coordination
        LEFT JOIN &DB_PREFIXEdossier_coordination_type
            ON dossier_coordination.dossier_coordination_type = dossier_coordination_type.dossier_coordination_type
        LEFT JOIN &DB_PREFIXEetablissement
            ON dossier_coordination.etablissement = etablissement.etablissement
        LEFT JOIN &DB_PREFIXEvoie
            ON etablissement.adresse_voie = voie.voie
        LEFT JOIN &DB_PREFIXEetablissement_type
            ON etablissement.etablissement_type = etablissement_type.etablissement_type
        LEFT JOIN &DB_PREFIXEetablissement_categorie
            ON etablissement.etablissement_categorie = etablissement_categorie.etablissement_categorie
        LEFT JOIN &DB_PREFIXEacteur
            ON dossier_instruction.technicien = acteur.acteur
        LEFT JOIN &DB_PREFIXEreunion_categorie
            ON dossier_instruction_reunion.reunion_type_categorie=reunion_categorie.reunion_categorie
        LEFT JOIN &DB_PREFIXEreunion_avis as reunion_avis0 
            ON dossier_instruction_reunion.avis=reunion_avis0.reunion_avis 
        LEFT JOIN &DB_PREFIXEreunion_avis as reunion_avis2 
            ON dossier_instruction_reunion.proposition_avis=reunion_avis2.reunion_avis
WHERE
    dossier_instruction_reunion.reunion = &idx
    AND lower(reunion_categorie.code) = ''ap_dif''
ORDER BY
    reunion_categorie.ordre, dossier_instruction_reunion.ordre');
INSERT INTO om_sousetat (om_sousetat, om_collectivite, id, libelle, actif, titre, titrehauteur, titrefont, titreattribut, titretaille, titrebordure, titrealign, titrefond, titrefondcouleur, titretextecouleur, intervalle_debut, intervalle_fin, entete_flag, entete_fond, entete_orientation, entete_hauteur, entetecolone_bordure, entetecolone_align, entete_fondcouleur, entete_textecouleur, tableau_largeur, tableau_bordure, tableau_fontaille, bordure_couleur, se_fond1, se_fond2, cellule_fond, cellule_hauteur, cellule_largeur, cellule_bordure_un, cellule_bordure, cellule_align, cellule_fond_total, cellule_fontaille_total, cellule_hauteur_total, cellule_fondcouleur_total, cellule_bordure_total, cellule_align_total, cellule_fond_moyenne, cellule_fontaille_moyenne, cellule_hauteur_moyenne, cellule_fondcouleur_moyenne, cellule_bordure_moyenne, cellule_align_moyenne, cellule_fond_nbr, cellule_fontaille_nbr, cellule_hauteur_nbr, cellule_fondcouleur_nbr, cellule_bordure_nbr, cellule_align_nbr, cellule_numerique, cellule_total, cellule_moyenne, cellule_compteur, om_sql) VALUES (3, 1, 'reu_odj_ccs_plen_visites', 'Réunion - Ordre du jour CCS - Visites', true, 'Visites', 10, 'helvetica', 'B', 12, '0', 'L', '0', '255-255-255', '0-0-0', 10, 15, '1', '1', '0|0|0|0|0', 15, 'LTBR|LTBR|LTBR|LTBR|LTBR', 'C|C|C|C|C', '188-188-188', '0-0-0', 195, '1', 10, '0-0-0', '255-255-255', '239-239-239', '1', 10, '15|70|70|60|60', 'LTBR|LTBR|LTBR|LTBR|LTBR', 'LTBR|LTBR|LTBR|LTBR|LTBR', 'C|L|L|L|L', '1', 10, 15, '196-213-213', 'LTBR|LTBR|LTBR|LTBR|LTBR', 'C|L|L|L|L', '1', 10, 15, '196-213-213', 'LTBR|LTBR|LTBR|LTBR|LTBR', 'C|L|L|L|L', '1', 10, 15, '196-213-213', 'LTBR|LTBR|LTBR|LTBR|LTBR', 'C|L|L|L|L', '999|999|999|999|999', '0|0|0|0|0', '0|0|0|0|0', '0|0|0|0|0', 'SELECT
    ---- Numéro
    dossier_instruction_reunion.ordre as n°,
    ---- Établissement
    CONCAT_WS(''
'',
        -- Ligne 1
        CONCAT(etablissement.code, '' - '', etablissement.libelle, '' [ '', etablissement_categorie.libelle, '' / '', etablissement_type.libelle ,'' ]''),
        -- Ligne 2
        CONCAT_WS('' '', etablissement.adresse_numero, etablissement.adresse_numero2, voie.libelle),
        -- Ligne 3
        CONCAT_WS('' '', etablissement.adresse_cp, etablissement.adresse_ville)
    ) as établissement,
    ---- Dossier
    CONCAT_WS(''
'',
        -- Ligne 1
        dossier_coordination_type.libelle,
        -- Ligne 2
        CONCAT(dossier_instruction.libelle, '' / '', dossier_coordination.dossier_instruction_ads),
        -- Ligne 3
        CONCAT(''Demande du  : '', to_char(dossier_coordination.date_demande, ''DD/MM/YYYY'')),
        -- Ligne 4
        CONCAT(''=> '', acteur.acronyme)
    ) as dossier,
    ---- Proposition
    CONCAT_WS(''
'',
        -- Ligne 1
        reunion_avis2.libelle,
        -- Ligne 2
        dossier_instruction_reunion.proposition_avis_complement
    ) as proposition,
    ---- Avis (colonne vide)
    '' '' as avis
FROM
    &DB_PREFIXEdossier_instruction_reunion
        LEFT JOIN &DB_PREFIXEdossier_instruction
            ON dossier_instruction_reunion.dossier_instruction = dossier_instruction.dossier_instruction
        LEFT JOIN &DB_PREFIXEdossier_coordination
            ON dossier_instruction.dossier_coordination = dossier_coordination.dossier_coordination
        LEFT JOIN &DB_PREFIXEdossier_coordination_type
            ON dossier_coordination.dossier_coordination_type = dossier_coordination_type.dossier_coordination_type
        LEFT JOIN &DB_PREFIXEetablissement
            ON dossier_coordination.etablissement = etablissement.etablissement
        LEFT JOIN &DB_PREFIXEvoie
            ON etablissement.adresse_voie = voie.voie
        LEFT JOIN &DB_PREFIXEetablissement_type
            ON etablissement.etablissement_type = etablissement_type.etablissement_type
        LEFT JOIN &DB_PREFIXEetablissement_categorie
            ON etablissement.etablissement_categorie = etablissement_categorie.etablissement_categorie
        LEFT JOIN &DB_PREFIXEacteur
            ON dossier_instruction.technicien = acteur.acteur
        LEFT JOIN &DB_PREFIXEreunion_categorie
            ON dossier_instruction_reunion.reunion_type_categorie=reunion_categorie.reunion_categorie
        LEFT JOIN &DB_PREFIXEreunion_avis as reunion_avis0 
            ON dossier_instruction_reunion.avis=reunion_avis0.reunion_avis 
        LEFT JOIN &DB_PREFIXEreunion_avis as reunion_avis2 
            ON dossier_instruction_reunion.proposition_avis=reunion_avis2.reunion_avis
WHERE
    dossier_instruction_reunion.reunion = &idx
    AND lower(reunion_categorie.code) = ''visites''
ORDER BY
    reunion_categorie.ordre, dossier_instruction_reunion.ordre');
INSERT INTO om_sousetat (om_sousetat, om_collectivite, id, libelle, actif, titre, titrehauteur, titrefont, titreattribut, titretaille, titrebordure, titrealign, titrefond, titrefondcouleur, titretextecouleur, intervalle_debut, intervalle_fin, entete_flag, entete_fond, entete_orientation, entete_hauteur, entetecolone_bordure, entetecolone_align, entete_fondcouleur, entete_textecouleur, tableau_largeur, tableau_bordure, tableau_fontaille, bordure_couleur, se_fond1, se_fond2, cellule_fond, cellule_hauteur, cellule_largeur, cellule_bordure_un, cellule_bordure, cellule_align, cellule_fond_total, cellule_fontaille_total, cellule_hauteur_total, cellule_fondcouleur_total, cellule_bordure_total, cellule_align_total, cellule_fond_moyenne, cellule_fontaille_moyenne, cellule_hauteur_moyenne, cellule_fondcouleur_moyenne, cellule_bordure_moyenne, cellule_align_moyenne, cellule_fond_nbr, cellule_fontaille_nbr, cellule_hauteur_nbr, cellule_fondcouleur_nbr, cellule_bordure_nbr, cellule_align_nbr, cellule_numerique, cellule_total, cellule_moyenne, cellule_compteur, om_sql) VALUES (2, 1, 'reu_odj_cca', 'Réunion - Ordre du jour CCA', true, '...', 10, 'helvetica', 'B', 12, '0', 'L', '0', '255-255-255', '0-0-0', 10, 15, '1', '1', '0|0|0|0|0', 15, 'LTBR|LTBR|LTBR|LTBR|LTBR', 'C|C|C|C|C', '188-188-188', '0-0-0', 195, '1', 10, '0-0-0', '255-255-255', '239-239-239', '1', 10, '15|70|70|60|60', 'LTBR|LTBR|LTBR|LTBR|LTBR', 'LTBR|LTBR|LTBR|LTBR|LTBR', 'C|L|L|L|L', '1', 10, 15, '196-213-213', 'LTBR|LTBR|LTBR|LTBR|LTBR', 'C|L|L|L|L', '1', 10, 15, '196-213-213', 'LTBR|LTBR|LTBR|LTBR|LTBR', 'C|L|L|L|L', '1', 10, 15, '196-213-213', 'LTBR|LTBR|LTBR|LTBR|LTBR', 'C|L|L|L|L', '999|999|999|999|999', '0|0|0|0|0', '0|0|0|0|0', '0|0|0|0|0', 'SELECT
    ---- Numéro
    dossier_instruction_reunion.ordre as n°,
    ---- Établissement
    CONCAT_WS(''
'',
        -- Ligne 1
        CONCAT(etablissement.code, '' - '', etablissement.libelle, '' [ '', etablissement_categorie.libelle, '' / '', etablissement_type.libelle ,'' ]''),
        -- Ligne 2
        CONCAT_WS('' '', etablissement.adresse_numero, etablissement.adresse_numero2, voie.libelle),
        -- Ligne 3
        CONCAT_WS('' '', etablissement.adresse_cp, etablissement.adresse_ville)
    ) as établissement,
    ---- Dossier
    CONCAT_WS(''
'',
        -- Ligne 1
        dossier_coordination_type.libelle,
        -- Ligne 2
        CONCAT(dossier_instruction.libelle, '' / '', dossier_coordination.dossier_instruction_ads),
        -- Ligne 3
        CONCAT(''Demande du  : '', to_char(dossier_coordination.date_demande, ''DD/MM/YYYY'')),
        -- Ligne 4
        CONCAT(''=> '', acteur.acronyme)
    ) as dossier,
    ---- Proposition
    CONCAT_WS(''
'',
        -- Ligne 1
        reunion_avis2.libelle,
        -- Ligne 2
        dossier_instruction_reunion.proposition_avis_complement
    ) as proposition,
    ---- Avis (colonne vide)
    '' '' as avis
FROM
    &DB_PREFIXEdossier_instruction_reunion
        LEFT JOIN &DB_PREFIXEdossier_instruction
            ON dossier_instruction_reunion.dossier_instruction = dossier_instruction.dossier_instruction
        LEFT JOIN &DB_PREFIXEdossier_coordination
            ON dossier_instruction.dossier_coordination = dossier_coordination.dossier_coordination
        LEFT JOIN &DB_PREFIXEdossier_coordination_type
            ON dossier_coordination.dossier_coordination_type = dossier_coordination_type.dossier_coordination_type
        LEFT JOIN &DB_PREFIXEetablissement
            ON dossier_coordination.etablissement = etablissement.etablissement
        LEFT JOIN &DB_PREFIXEvoie
            ON etablissement.adresse_voie = voie.voie
        LEFT JOIN &DB_PREFIXEetablissement_type
            ON etablissement.etablissement_type = etablissement_type.etablissement_type
        LEFT JOIN &DB_PREFIXEetablissement_categorie
            ON etablissement.etablissement_categorie = etablissement_categorie.etablissement_categorie
        LEFT JOIN &DB_PREFIXEacteur
            ON dossier_instruction.technicien = acteur.acteur
        LEFT JOIN &DB_PREFIXEreunion_categorie
            ON dossier_instruction_reunion.reunion_type_categorie=reunion_categorie.reunion_categorie
        LEFT JOIN &DB_PREFIXEreunion_avis as reunion_avis0 
            ON dossier_instruction_reunion.avis=reunion_avis0.reunion_avis 
        LEFT JOIN &DB_PREFIXEreunion_avis as reunion_avis2 
            ON dossier_instruction_reunion.proposition_avis=reunion_avis2.reunion_avis
WHERE
    dossier_instruction_reunion.reunion = &idx
ORDER BY
    reunion_categorie.ordre, dossier_instruction_reunion.ordre');
INSERT INTO om_sousetat (om_sousetat, om_collectivite, id, libelle, actif, titre, titrehauteur, titrefont, titreattribut, titretaille, titrebordure, titrealign, titrefond, titrefondcouleur, titretextecouleur, intervalle_debut, intervalle_fin, entete_flag, entete_fond, entete_orientation, entete_hauteur, entetecolone_bordure, entetecolone_align, entete_fondcouleur, entete_textecouleur, tableau_largeur, tableau_bordure, tableau_fontaille, bordure_couleur, se_fond1, se_fond2, cellule_fond, cellule_hauteur, cellule_largeur, cellule_bordure_un, cellule_bordure, cellule_align, cellule_fond_total, cellule_fontaille_total, cellule_hauteur_total, cellule_fondcouleur_total, cellule_bordure_total, cellule_align_total, cellule_fond_moyenne, cellule_fontaille_moyenne, cellule_hauteur_moyenne, cellule_fondcouleur_moyenne, cellule_bordure_moyenne, cellule_align_moyenne, cellule_fond_nbr, cellule_fontaille_nbr, cellule_hauteur_nbr, cellule_fondcouleur_nbr, cellule_bordure_nbr, cellule_align_nbr, cellule_numerique, cellule_total, cellule_moyenne, cellule_compteur, om_sql) VALUES (4, 1, 'reu_odj', 'Réunion - Ordre du jour', true, '...', 10, 'helvetica', 'B', 12, '0', 'L', '0', '255-255-255', '0-0-0', 10, 15, '1', '1', '0|0|0|0|0', 15, 'LTBR|LTBR|LTBR|LTBR|LTBR', 'C|C|C|C|C', '188-188-188', '0-0-0', 195, '1', 10, '0-0-0', '255-255-255', '239-239-239', '1', 10, '15|70|70|60|60', 'LTBR|LTBR|LTBR|LTBR|LTBR', 'LTBR|LTBR|LTBR|LTBR|LTBR', 'C|L|L|L|L', '1', 10, 15, '196-213-213', 'LTBR|LTBR|LTBR|LTBR|LTBR', 'C|L|L|L|L', '1', 10, 15, '196-213-213', 'LTBR|LTBR|LTBR|LTBR|LTBR', 'C|L|L|L|L', '1', 10, 15, '196-213-213', 'LTBR|LTBR|LTBR|LTBR|LTBR', 'C|L|L|L|L', '999|999|999|999|999', '0|0|0|0|0', '0|0|0|0|0', '0|0|0|0|0', 'SELECT
    ---- Numéro
    dossier_instruction_reunion.ordre as n°,
    ---- Établissement
    CONCAT_WS(''
'',
        -- Ligne 1
        CONCAT(etablissement.code, '' - '', etablissement.libelle, '' [ '', etablissement_categorie.libelle, '' / '', etablissement_type.libelle ,'' ]''),
        -- Ligne 2
        CONCAT_WS('' '', etablissement.adresse_numero, etablissement.adresse_numero2, voie.libelle),
        -- Ligne 3
        CONCAT_WS('' '', etablissement.adresse_cp, etablissement.adresse_ville)
    ) as établissement,
    ---- Dossier
    CONCAT_WS(''
'',
        -- Ligne 1
        dossier_coordination_type.libelle,
        -- Ligne 2
        CONCAT(dossier_instruction.libelle, '' / '', dossier_coordination.dossier_instruction_ads),
        -- Ligne 3
        CONCAT(''Demande du  : '', to_char(dossier_coordination.date_demande, ''DD/MM/YYYY'')),
        -- Ligne 4
        CONCAT(''=> '', acteur.acronyme)
    ) as dossier,
    ---- Proposition
    CONCAT_WS(''
'',
        -- Ligne 1
        reunion_avis2.libelle,
        -- Ligne 2
        dossier_instruction_reunion.proposition_avis_complement
    ) as proposition,
    ---- Avis (colonne vide)
    '' '' as avis
FROM
    &DB_PREFIXEdossier_instruction_reunion
        LEFT JOIN &DB_PREFIXEdossier_instruction
            ON dossier_instruction_reunion.dossier_instruction = dossier_instruction.dossier_instruction
        LEFT JOIN &DB_PREFIXEdossier_coordination
            ON dossier_instruction.dossier_coordination = dossier_coordination.dossier_coordination
        LEFT JOIN &DB_PREFIXEdossier_coordination_type
            ON dossier_coordination.dossier_coordination_type = dossier_coordination_type.dossier_coordination_type
        LEFT JOIN &DB_PREFIXEetablissement
            ON dossier_coordination.etablissement = etablissement.etablissement
        LEFT JOIN &DB_PREFIXEvoie
            ON etablissement.adresse_voie = voie.voie
        LEFT JOIN &DB_PREFIXEetablissement_type
            ON etablissement.etablissement_type = etablissement_type.etablissement_type
        LEFT JOIN &DB_PREFIXEetablissement_categorie
            ON etablissement.etablissement_categorie = etablissement_categorie.etablissement_categorie
        LEFT JOIN &DB_PREFIXEacteur
            ON dossier_instruction.technicien = acteur.acteur
        LEFT JOIN &DB_PREFIXEreunion_categorie
            ON dossier_instruction_reunion.reunion_type_categorie=reunion_categorie.reunion_categorie
        LEFT JOIN &DB_PREFIXEreunion_avis as reunion_avis0 
            ON dossier_instruction_reunion.avis=reunion_avis0.reunion_avis 
        LEFT JOIN &DB_PREFIXEreunion_avis as reunion_avis2 
            ON dossier_instruction_reunion.proposition_avis=reunion_avis2.reunion_avis
WHERE
    dossier_instruction_reunion.reunion = &idx
ORDER BY
    reunion_categorie.ordre, dossier_instruction_reunion.ordre');
INSERT INTO om_sousetat (om_sousetat, om_collectivite, id, libelle, actif, titre, titrehauteur, titrefont, titreattribut, titretaille, titrebordure, titrealign, titrefond, titrefondcouleur, titretextecouleur, intervalle_debut, intervalle_fin, entete_flag, entete_fond, entete_orientation, entete_hauteur, entetecolone_bordure, entetecolone_align, entete_fondcouleur, entete_textecouleur, tableau_largeur, tableau_bordure, tableau_fontaille, bordure_couleur, se_fond1, se_fond2, cellule_fond, cellule_hauteur, cellule_largeur, cellule_bordure_un, cellule_bordure, cellule_align, cellule_fond_total, cellule_fontaille_total, cellule_hauteur_total, cellule_fondcouleur_total, cellule_bordure_total, cellule_align_total, cellule_fond_moyenne, cellule_fontaille_moyenne, cellule_hauteur_moyenne, cellule_fondcouleur_moyenne, cellule_bordure_moyenne, cellule_align_moyenne, cellule_fond_nbr, cellule_fontaille_nbr, cellule_hauteur_nbr, cellule_fondcouleur_nbr, cellule_bordure_nbr, cellule_align_nbr, cellule_numerique, cellule_total, cellule_moyenne, cellule_compteur, om_sql) VALUES (11, 1, 'reu_odj_ccs_plen_apscds', 'Réunion - Ordre du jour CCS - Sous Commission Départementale', true, 'Sous Commission Départementale', 10, 'helvetica', 'B', 12, '0', 'L', '0', '255-255-255', '0-0-0', 10, 15, '1', '1', '0|0|0|0|0', 15, 'LTBR|LTBR|LTBR|LTBR|LTBR', 'C|C|C|C|C', '188-188-188', '0-0-0', 195, '1', 10, '0-0-0', '255-255-255', '239-239-239', '1', 10, '15|70|70|60|60', 'LTBR|LTBR|LTBR|LTBR|LTBR', 'LTBR|LTBR|LTBR|LTBR|LTBR', 'C|L|L|L|L', '1', 10, 15, '196-213-213', 'LTBR|LTBR|LTBR|LTBR|LTBR', 'C|L|L|L|L', '1', 10, 15, '196-213-213', 'LTBR|LTBR|LTBR|LTBR|LTBR', 'C|L|L|L|L', '1', 10, 15, '196-213-213', 'LTBR|LTBR|LTBR|LTBR|LTBR', 'C|L|L|L|L', '999|999|999|999|999', '0|0|0|0|0', '0|0|0|0|0', '0|0|0|0|0', 'SELECT
    ---- Numéro
    dossier_instruction_reunion.ordre as n°,
    ---- Établissement
    CONCAT_WS(''
'',
        -- Ligne 1
        CONCAT(etablissement.code, '' - '', etablissement.libelle, '' [ '', etablissement_categorie.libelle, '' / '', etablissement_type.libelle ,'' ]''),
        -- Ligne 2
        CONCAT_WS('' '', etablissement.adresse_numero, etablissement.adresse_numero2, voie.libelle),
        -- Ligne 3
        CONCAT_WS('' '', etablissement.adresse_cp, etablissement.adresse_ville)
    ) as établissement,
    ---- Dossier
    CONCAT_WS(''
'',
        -- Ligne 1
        dossier_coordination_type.libelle,
        -- Ligne 2
        CONCAT(dossier_instruction.libelle, '' / '', dossier_coordination.dossier_instruction_ads),
        -- Ligne 3
        CONCAT(''Demande du  : '', to_char(dossier_coordination.date_demande, ''DD/MM/YYYY'')),
        -- Ligne 4
        CONCAT(''=> '', acteur.acronyme)
    ) as dossier,
    ---- Proposition
    CONCAT_WS(''
'',
        -- Ligne 1
        reunion_avis2.libelle,
        -- Ligne 2
        dossier_instruction_reunion.proposition_avis_complement
    ) as proposition,
    ---- Avis (colonne vide)
    '' '' as avis
FROM
    &DB_PREFIXEdossier_instruction_reunion
        LEFT JOIN &DB_PREFIXEdossier_instruction
            ON dossier_instruction_reunion.dossier_instruction = dossier_instruction.dossier_instruction
        LEFT JOIN &DB_PREFIXEdossier_coordination
            ON dossier_instruction.dossier_coordination = dossier_coordination.dossier_coordination
        LEFT JOIN &DB_PREFIXEdossier_coordination_type
            ON dossier_coordination.dossier_coordination_type = dossier_coordination_type.dossier_coordination_type
        LEFT JOIN &DB_PREFIXEetablissement
            ON dossier_coordination.etablissement = etablissement.etablissement
        LEFT JOIN &DB_PREFIXEvoie
            ON etablissement.adresse_voie = voie.voie
        LEFT JOIN &DB_PREFIXEetablissement_type
            ON etablissement.etablissement_type = etablissement_type.etablissement_type
        LEFT JOIN &DB_PREFIXEetablissement_categorie
            ON etablissement.etablissement_categorie = etablissement_categorie.etablissement_categorie
        LEFT JOIN &DB_PREFIXEacteur
            ON dossier_instruction.technicien = acteur.acteur
        LEFT JOIN &DB_PREFIXEreunion_categorie
            ON dossier_instruction_reunion.reunion_type_categorie=reunion_categorie.reunion_categorie
        LEFT JOIN &DB_PREFIXEreunion_avis as reunion_avis0 
            ON dossier_instruction_reunion.avis=reunion_avis0.reunion_avis 
        LEFT JOIN &DB_PREFIXEreunion_avis as reunion_avis2 
            ON dossier_instruction_reunion.proposition_avis=reunion_avis2.reunion_avis
WHERE
    dossier_instruction_reunion.reunion = &idx
    AND lower(reunion_categorie.code) = ''ap_scds''
ORDER BY
    reunion_categorie.ordre, dossier_instruction_reunion.ordre');
INSERT INTO om_sousetat (om_sousetat, om_collectivite, id, libelle, actif, titre, titrehauteur, titrefont, titreattribut, titretaille, titrebordure, titrealign, titrefond, titrefondcouleur, titretextecouleur, intervalle_debut, intervalle_fin, entete_flag, entete_fond, entete_orientation, entete_hauteur, entetecolone_bordure, entetecolone_align, entete_fondcouleur, entete_textecouleur, tableau_largeur, tableau_bordure, tableau_fontaille, bordure_couleur, se_fond1, se_fond2, cellule_fond, cellule_hauteur, cellule_largeur, cellule_bordure_un, cellule_bordure, cellule_align, cellule_fond_total, cellule_fontaille_total, cellule_hauteur_total, cellule_fondcouleur_total, cellule_bordure_total, cellule_align_total, cellule_fond_moyenne, cellule_fontaille_moyenne, cellule_hauteur_moyenne, cellule_fondcouleur_moyenne, cellule_bordure_moyenne, cellule_align_moyenne, cellule_fond_nbr, cellule_fontaille_nbr, cellule_hauteur_nbr, cellule_fondcouleur_nbr, cellule_bordure_nbr, cellule_align_nbr, cellule_numerique, cellule_total, cellule_moyenne, cellule_compteur, om_sql) VALUES (8, 1, 'reu_odj_ccs_plen_enjeux', 'Réunion - Ordre du jour CCS - Enjeux', true, 'Enjeux', 10, 'helvetica', 'B', 12, '0', 'L', '0', '255-255-255', '0-0-0', 10, 15, '1', '1', '0|0|0|0|0', 15, 'LTBR|LTBR|LTBR|LTBR|LTBR', 'C|C|C|C|C', '188-188-188', '0-0-0', 195, '1', 10, '0-0-0', '255-255-255', '239-239-239', '1', 10, '15|70|70|60|60', 'LTBR|LTBR|LTBR|LTBR|LTBR', 'LTBR|LTBR|LTBR|LTBR|LTBR', 'C|L|L|L|L', '1', 10, 15, '196-213-213', 'LTBR|LTBR|LTBR|LTBR|LTBR', 'C|L|L|L|L', '1', 10, 15, '196-213-213', 'LTBR|LTBR|LTBR|LTBR|LTBR', 'C|L|L|L|L', '1', 10, 15, '196-213-213', 'LTBR|LTBR|LTBR|LTBR|LTBR', 'C|L|L|L|L', '999|999|999|999|999', '0|0|0|0|0', '0|0|0|0|0', '0|0|0|0|0', 'SELECT
    ---- Numéro
    dossier_instruction_reunion.ordre as n°,
    ---- Établissement
    CONCAT_WS(''
'',
        -- Ligne 1
        CONCAT(etablissement.code, '' - '', etablissement.libelle, '' [ '', etablissement_categorie.libelle, '' / '', etablissement_type.libelle ,'' ]''),
        -- Ligne 2
        CONCAT_WS('' '', etablissement.adresse_numero, etablissement.adresse_numero2, voie.libelle),
        -- Ligne 3
        CONCAT_WS('' '', etablissement.adresse_cp, etablissement.adresse_ville)
    ) as établissement,
    ---- Dossier
    CONCAT_WS(''
'',
        -- Ligne 1
        dossier_coordination_type.libelle,
        -- Ligne 2
        CONCAT(dossier_instruction.libelle, '' / '', dossier_coordination.dossier_instruction_ads),
        -- Ligne 3
        CONCAT(''Demande du  : '', to_char(dossier_coordination.date_demande, ''DD/MM/YYYY'')),
        -- Ligne 4
        CONCAT(''=> '', acteur.acronyme)
    ) as dossier,
    ---- Proposition
    CONCAT_WS(''
'',
        -- Ligne 1
        reunion_avis2.libelle,
        -- Ligne 2
        dossier_instruction_reunion.proposition_avis_complement
    ) as proposition,
    ---- Avis (colonne vide)
    '' '' as avis
FROM
    &DB_PREFIXEdossier_instruction_reunion
        LEFT JOIN &DB_PREFIXEdossier_instruction
            ON dossier_instruction_reunion.dossier_instruction = dossier_instruction.dossier_instruction
        LEFT JOIN &DB_PREFIXEdossier_coordination
            ON dossier_instruction.dossier_coordination = dossier_coordination.dossier_coordination
        LEFT JOIN &DB_PREFIXEdossier_coordination_type
            ON dossier_coordination.dossier_coordination_type = dossier_coordination_type.dossier_coordination_type
        LEFT JOIN &DB_PREFIXEetablissement
            ON dossier_coordination.etablissement = etablissement.etablissement
        LEFT JOIN &DB_PREFIXEvoie
            ON etablissement.adresse_voie = voie.voie
        LEFT JOIN &DB_PREFIXEetablissement_type
            ON etablissement.etablissement_type = etablissement_type.etablissement_type
        LEFT JOIN &DB_PREFIXEetablissement_categorie
            ON etablissement.etablissement_categorie = etablissement_categorie.etablissement_categorie
        LEFT JOIN &DB_PREFIXEacteur
            ON dossier_instruction.technicien = acteur.acteur
        LEFT JOIN &DB_PREFIXEreunion_categorie
            ON dossier_instruction_reunion.reunion_type_categorie=reunion_categorie.reunion_categorie
        LEFT JOIN &DB_PREFIXEreunion_avis as reunion_avis0 
            ON dossier_instruction_reunion.avis=reunion_avis0.reunion_avis 
        LEFT JOIN &DB_PREFIXEreunion_avis as reunion_avis2 
            ON dossier_instruction_reunion.proposition_avis=reunion_avis2.reunion_avis
WHERE
    dossier_instruction_reunion.reunion = &idx
    AND lower(reunion_categorie.code) = ''enjeux''
ORDER BY
    reunion_categorie.ordre, dossier_instruction_reunion.ordre');
INSERT INTO om_sousetat (om_sousetat, om_collectivite, id, libelle, actif, titre, titrehauteur, titrefont, titreattribut, titretaille, titrebordure, titrealign, titrefond, titrefondcouleur, titretextecouleur, intervalle_debut, intervalle_fin, entete_flag, entete_fond, entete_orientation, entete_hauteur, entetecolone_bordure, entetecolone_align, entete_fondcouleur, entete_textecouleur, tableau_largeur, tableau_bordure, tableau_fontaille, bordure_couleur, se_fond1, se_fond2, cellule_fond, cellule_hauteur, cellule_largeur, cellule_bordure_un, cellule_bordure, cellule_align, cellule_fond_total, cellule_fontaille_total, cellule_hauteur_total, cellule_fondcouleur_total, cellule_bordure_total, cellule_align_total, cellule_fond_moyenne, cellule_fontaille_moyenne, cellule_hauteur_moyenne, cellule_fondcouleur_moyenne, cellule_bordure_moyenne, cellule_align_moyenne, cellule_fond_nbr, cellule_fontaille_nbr, cellule_hauteur_nbr, cellule_fondcouleur_nbr, cellule_bordure_nbr, cellule_align_nbr, cellule_numerique, cellule_total, cellule_moyenne, cellule_compteur, om_sql) VALUES (10, 1, 'reu_odj_ccs_plen_apmed', 'Réunion - Ordre du jour CCS - Mises en demeure', true, 'Mises en demeure', 10, 'helvetica', 'B', 12, '0', 'L', '0', '255-255-255', '0-0-0', 10, 15, '1', '1', '0|0|0|0|0', 15, 'LTBR|LTBR|LTBR|LTBR|LTBR', 'C|C|C|C|C', '188-188-188', '0-0-0', 195, '1', 10, '0-0-0', '255-255-255', '239-239-239', '1', 10, '15|70|70|60|60', 'LTBR|LTBR|LTBR|LTBR|LTBR', 'LTBR|LTBR|LTBR|LTBR|LTBR', 'C|L|L|L|L', '1', 10, 15, '196-213-213', 'LTBR|LTBR|LTBR|LTBR|LTBR', 'C|L|L|L|L', '1', 10, 15, '196-213-213', 'LTBR|LTBR|LTBR|LTBR|LTBR', 'C|L|L|L|L', '1', 10, 15, '196-213-213', 'LTBR|LTBR|LTBR|LTBR|LTBR', 'C|L|L|L|L', '999|999|999|999|999', '0|0|0|0|0', '0|0|0|0|0', '0|0|0|0|0', 'SELECT
    ---- Numéro
    dossier_instruction_reunion.ordre as n°,
    ---- Établissement
    CONCAT_WS(''
'',
        -- Ligne 1
        CONCAT(etablissement.code, '' - '', etablissement.libelle, '' [ '', etablissement_categorie.libelle, '' / '', etablissement_type.libelle ,'' ]''),
        -- Ligne 2
        CONCAT_WS('' '', etablissement.adresse_numero, etablissement.adresse_numero2, voie.libelle),
        -- Ligne 3
        CONCAT_WS('' '', etablissement.adresse_cp, etablissement.adresse_ville)
    ) as établissement,
    ---- Dossier
    CONCAT_WS(''
'',
        -- Ligne 1
        dossier_coordination_type.libelle,
        -- Ligne 2
        CONCAT(dossier_instruction.libelle, '' / '', dossier_coordination.dossier_instruction_ads),
        -- Ligne 3
        CONCAT(''Demande du  : '', to_char(dossier_coordination.date_demande, ''DD/MM/YYYY'')),
        -- Ligne 4
        CONCAT(''=> '', acteur.acronyme)
    ) as dossier,
    ---- Proposition
    CONCAT_WS(''
'',
        -- Ligne 1
        reunion_avis2.libelle,
        -- Ligne 2
        dossier_instruction_reunion.proposition_avis_complement
    ) as proposition,
    ---- Avis (colonne vide)
    '' '' as avis
FROM
    &DB_PREFIXEdossier_instruction_reunion
        LEFT JOIN &DB_PREFIXEdossier_instruction
            ON dossier_instruction_reunion.dossier_instruction = dossier_instruction.dossier_instruction
        LEFT JOIN &DB_PREFIXEdossier_coordination
            ON dossier_instruction.dossier_coordination = dossier_coordination.dossier_coordination
        LEFT JOIN &DB_PREFIXEdossier_coordination_type
            ON dossier_coordination.dossier_coordination_type = dossier_coordination_type.dossier_coordination_type
        LEFT JOIN &DB_PREFIXEetablissement
            ON dossier_coordination.etablissement = etablissement.etablissement
        LEFT JOIN &DB_PREFIXEvoie
            ON etablissement.adresse_voie = voie.voie
        LEFT JOIN &DB_PREFIXEetablissement_type
            ON etablissement.etablissement_type = etablissement_type.etablissement_type
        LEFT JOIN &DB_PREFIXEetablissement_categorie
            ON etablissement.etablissement_categorie = etablissement_categorie.etablissement_categorie
        LEFT JOIN &DB_PREFIXEacteur
            ON dossier_instruction.technicien = acteur.acteur
        LEFT JOIN &DB_PREFIXEreunion_categorie
            ON dossier_instruction_reunion.reunion_type_categorie=reunion_categorie.reunion_categorie
        LEFT JOIN &DB_PREFIXEreunion_avis as reunion_avis0 
            ON dossier_instruction_reunion.avis=reunion_avis0.reunion_avis 
        LEFT JOIN &DB_PREFIXEreunion_avis as reunion_avis2 
            ON dossier_instruction_reunion.proposition_avis=reunion_avis2.reunion_avis
WHERE
    dossier_instruction_reunion.reunion = &idx
    AND lower(reunion_categorie.code) = ''ap_med''
ORDER BY
    reunion_categorie.ordre, dossier_instruction_reunion.ordre');
INSERT INTO om_sousetat (om_sousetat, om_collectivite, id, libelle, actif, titre, titrehauteur, titrefont, titreattribut, titretaille, titrebordure, titrealign, titrefond, titrefondcouleur, titretextecouleur, intervalle_debut, intervalle_fin, entete_flag, entete_fond, entete_orientation, entete_hauteur, entetecolone_bordure, entetecolone_align, entete_fondcouleur, entete_textecouleur, tableau_largeur, tableau_bordure, tableau_fontaille, bordure_couleur, se_fond1, se_fond2, cellule_fond, cellule_hauteur, cellule_largeur, cellule_bordure_un, cellule_bordure, cellule_align, cellule_fond_total, cellule_fontaille_total, cellule_hauteur_total, cellule_fondcouleur_total, cellule_bordure_total, cellule_align_total, cellule_fond_moyenne, cellule_fontaille_moyenne, cellule_hauteur_moyenne, cellule_fondcouleur_moyenne, cellule_bordure_moyenne, cellule_align_moyenne, cellule_fond_nbr, cellule_fontaille_nbr, cellule_hauteur_nbr, cellule_fondcouleur_nbr, cellule_bordure_nbr, cellule_align_nbr, cellule_numerique, cellule_total, cellule_moyenne, cellule_compteur, om_sql) VALUES (9, 1, 'reu_odj_ccs_plen_apdif', 'Réunion - Ordre du jour CCS - Avis différés', true, 'Avis différés', 10, 'helvetica', 'B', 12, '0', 'L', '0', '255-255-255', '0-0-0', 10, 15, '1', '1', '0|0|0|0|0', 15, 'LTBR|LTBR|LTBR|LTBR|LTBR', 'C|C|C|C|C', '188-188-188', '0-0-0', 195, '1', 10, '0-0-0', '255-255-255', '239-239-239', '1', 10, '15|70|70|60|60', 'LTBR|LTBR|LTBR|LTBR|LTBR', 'LTBR|LTBR|LTBR|LTBR|LTBR', 'C|L|L|L|L', '1', 10, 15, '196-213-213', 'LTBR|LTBR|LTBR|LTBR|LTBR', 'C|L|L|L|L', '1', 10, 15, '196-213-213', 'LTBR|LTBR|LTBR|LTBR|LTBR', 'C|L|L|L|L', '1', 10, 15, '196-213-213', 'LTBR|LTBR|LTBR|LTBR|LTBR', 'C|L|L|L|L', '999|999|999|999|999', '0|0|0|0|0', '0|0|0|0|0', '0|0|0|0|0', 'SELECT
    ---- Numéro
    dossier_instruction_reunion.ordre as n°,
    ---- Établissement
    CONCAT_WS(''
'',
        -- Ligne 1
        CONCAT(etablissement.code, '' - '', etablissement.libelle, '' [ '', etablissement_categorie.libelle, '' / '', etablissement_type.libelle ,'' ]''),
        -- Ligne 2
        CONCAT_WS('' '', etablissement.adresse_numero, etablissement.adresse_numero2, voie.libelle),
        -- Ligne 3
        CONCAT_WS('' '', etablissement.adresse_cp, etablissement.adresse_ville)
    ) as établissement,
    ---- Dossier
    CONCAT_WS(''
'',
        -- Ligne 1
        dossier_coordination_type.libelle,
        -- Ligne 2
        CONCAT(dossier_instruction.libelle, '' / '', dossier_coordination.dossier_instruction_ads),
        -- Ligne 3
        CONCAT(''Demande du  : '', to_char(dossier_coordination.date_demande, ''DD/MM/YYYY'')),
        -- Ligne 4
        CONCAT(''=> '', acteur.acronyme)
    ) as dossier,
    ---- Proposition
    CONCAT_WS(''
'',
        -- Ligne 1
        reunion_avis2.libelle,
        -- Ligne 2
        dossier_instruction_reunion.proposition_avis_complement
    ) as proposition,
    ---- Avis (colonne vide)
    '' '' as avis
FROM
    &DB_PREFIXEdossier_instruction_reunion
        LEFT JOIN &DB_PREFIXEdossier_instruction
            ON dossier_instruction_reunion.dossier_instruction = dossier_instruction.dossier_instruction
        LEFT JOIN &DB_PREFIXEdossier_coordination
            ON dossier_instruction.dossier_coordination = dossier_coordination.dossier_coordination
        LEFT JOIN &DB_PREFIXEdossier_coordination_type
            ON dossier_coordination.dossier_coordination_type = dossier_coordination_type.dossier_coordination_type
        LEFT JOIN &DB_PREFIXEetablissement
            ON dossier_coordination.etablissement = etablissement.etablissement
        LEFT JOIN &DB_PREFIXEvoie
            ON etablissement.adresse_voie = voie.voie
        LEFT JOIN &DB_PREFIXEetablissement_type
            ON etablissement.etablissement_type = etablissement_type.etablissement_type
        LEFT JOIN &DB_PREFIXEetablissement_categorie
            ON etablissement.etablissement_categorie = etablissement_categorie.etablissement_categorie
        LEFT JOIN &DB_PREFIXEacteur
            ON dossier_instruction.technicien = acteur.acteur
        LEFT JOIN &DB_PREFIXEreunion_categorie
            ON dossier_instruction_reunion.reunion_type_categorie=reunion_categorie.reunion_categorie
        LEFT JOIN &DB_PREFIXEreunion_avis as reunion_avis0 
            ON dossier_instruction_reunion.avis=reunion_avis0.reunion_avis 
        LEFT JOIN &DB_PREFIXEreunion_avis as reunion_avis2 
            ON dossier_instruction_reunion.proposition_avis=reunion_avis2.reunion_avis
WHERE
    dossier_instruction_reunion.reunion = &idx
    AND lower(reunion_categorie.code) = ''ap_dif''
ORDER BY
    reunion_categorie.ordre, dossier_instruction_reunion.ordre');
INSERT INTO om_sousetat (om_sousetat, om_collectivite, id, libelle, actif, titre, titrehauteur, titrefont, titreattribut, titretaille, titrebordure, titrealign, titrefond, titrefondcouleur, titretextecouleur, intervalle_debut, intervalle_fin, entete_flag, entete_fond, entete_orientation, entete_hauteur, entetecolone_bordure, entetecolone_align, entete_fondcouleur, entete_textecouleur, tableau_largeur, tableau_bordure, tableau_fontaille, bordure_couleur, se_fond1, se_fond2, cellule_fond, cellule_hauteur, cellule_largeur, cellule_bordure_un, cellule_bordure, cellule_align, cellule_fond_total, cellule_fontaille_total, cellule_hauteur_total, cellule_fondcouleur_total, cellule_bordure_total, cellule_align_total, cellule_fond_moyenne, cellule_fontaille_moyenne, cellule_hauteur_moyenne, cellule_fondcouleur_moyenne, cellule_bordure_moyenne, cellule_align_moyenne, cellule_fond_nbr, cellule_fontaille_nbr, cellule_hauteur_nbr, cellule_fondcouleur_nbr, cellule_bordure_nbr, cellule_align_nbr, cellule_numerique, cellule_total, cellule_moyenne, cellule_compteur, om_sql) VALUES (7, 1, 'reu_odj_ccs_plen_plans', 'Réunion - Ordre du jour CCS - Plans', true, 'Plans', 10, 'helvetica', 'B', 12, '0', 'L', '0', '255-255-255', '0-0-0', 10, 15, '1', '1', '0|0|0|0|0', 15, 'LTBR|LTBR|LTBR|LTBR|LTBR', 'C|C|C|C|C', '188-188-188', '0-0-0', 195, '1', 10, '0-0-0', '255-255-255', '239-239-239', '1', 10, '15|70|70|60|60', 'LTBR|LTBR|LTBR|LTBR|LTBR', 'LTBR|LTBR|LTBR|LTBR|LTBR', 'C|L|L|L|L', '1', 10, 15, '196-213-213', 'LTBR|LTBR|LTBR|LTBR|LTBR', 'C|L|L|L|L', '1', 10, 15, '196-213-213', 'LTBR|LTBR|LTBR|LTBR|LTBR', 'C|L|L|L|L', '1', 10, 15, '196-213-213', 'LTBR|LTBR|LTBR|LTBR|LTBR', 'C|L|L|L|L', '999|999|999|999|999', '0|0|0|0|0', '0|0|0|0|0', '0|0|0|0|0', 'SELECT
    ---- Numéro
    dossier_instruction_reunion.ordre as n°,
    ---- Établissement
    CONCAT_WS(''
'',
        -- Ligne 1
        CONCAT(etablissement.code, '' - '', etablissement.libelle, '' [ '', etablissement_categorie.libelle, '' / '', etablissement_type.libelle ,'' ]''),
        -- Ligne 2
        CONCAT_WS('' '', etablissement.adresse_numero, etablissement.adresse_numero2, voie.libelle),
        -- Ligne 3
        CONCAT_WS('' '', etablissement.adresse_cp, etablissement.adresse_ville)
    ) as établissement,
    ---- Dossier
    CONCAT_WS(''
'',
        -- Ligne 1
        dossier_coordination_type.libelle,
        -- Ligne 2
        CONCAT(dossier_instruction.libelle, '' / '', dossier_coordination.dossier_instruction_ads),
        -- Ligne 3
        CONCAT(''Demande du  : '', to_char(dossier_coordination.date_demande, ''DD/MM/YYYY'')),
        -- Ligne 4
        CONCAT(''=> '', acteur.acronyme)
    ) as dossier,
    ---- Proposition
    CONCAT_WS(''
'',
        -- Ligne 1
        reunion_avis2.libelle,
        -- Ligne 2
        dossier_instruction_reunion.proposition_avis_complement
    ) as proposition,
    ---- Avis (colonne vide)
    '' '' as avis
FROM
    &DB_PREFIXEdossier_instruction_reunion
        LEFT JOIN &DB_PREFIXEdossier_instruction
            ON dossier_instruction_reunion.dossier_instruction = dossier_instruction.dossier_instruction
        LEFT JOIN &DB_PREFIXEdossier_coordination
            ON dossier_instruction.dossier_coordination = dossier_coordination.dossier_coordination
        LEFT JOIN &DB_PREFIXEdossier_coordination_type
            ON dossier_coordination.dossier_coordination_type = dossier_coordination_type.dossier_coordination_type
        LEFT JOIN &DB_PREFIXEetablissement
            ON dossier_coordination.etablissement = etablissement.etablissement
        LEFT JOIN &DB_PREFIXEvoie
            ON etablissement.adresse_voie = voie.voie
        LEFT JOIN &DB_PREFIXEetablissement_type
            ON etablissement.etablissement_type = etablissement_type.etablissement_type
        LEFT JOIN &DB_PREFIXEetablissement_categorie
            ON etablissement.etablissement_categorie = etablissement_categorie.etablissement_categorie
        LEFT JOIN &DB_PREFIXEacteur
            ON dossier_instruction.technicien = acteur.acteur
        LEFT JOIN &DB_PREFIXEreunion_categorie
            ON dossier_instruction_reunion.reunion_type_categorie=reunion_categorie.reunion_categorie
        LEFT JOIN &DB_PREFIXEreunion_avis as reunion_avis0 
            ON dossier_instruction_reunion.avis=reunion_avis0.reunion_avis 
        LEFT JOIN &DB_PREFIXEreunion_avis as reunion_avis2 
            ON dossier_instruction_reunion.proposition_avis=reunion_avis2.reunion_avis
WHERE
    dossier_instruction_reunion.reunion = &idx
    AND lower(reunion_categorie.code) = ''plans''
ORDER BY
    reunion_categorie.ordre, dossier_instruction_reunion.ordre');


--
-- Name: om_sousetat_seq; Type: SEQUENCE SET; Schema: openaria; Owner: -
--

SELECT pg_catalog.setval('om_sousetat_seq', 22, false);


--
-- PostgreSQL database dump complete
--

