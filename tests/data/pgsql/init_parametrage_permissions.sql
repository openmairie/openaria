-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
--
-- Création des profils, utilisateurs et tableaux de bord
--
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------

--
--
--
INSERT INTO om_profil (om_profil, libelle, hierarchie) VALUES (nextval('om_profil_seq'), 'PROFIL NON CONFIGURÉ', 0);
INSERT INTO om_utilisateur (om_utilisateur, nom, email, login, pwd, om_collectivite, om_type, om_profil) VALUES (nextval('om_utilisateur_seq'), 'nonconfig', 'nospam@openmairie.org', 'nonconfig', '135201940a2bfaf3b878d6e3887c1343', 1, 'db', (SELECT om_profil FROM om_profil WHERE libelle = 'PROFIL NON CONFIGURÉ'));
INSERT INTO om_dashboard (om_dashboard, om_profil, bloc, position, om_widget) VALUES (nextval('om_dashboard_seq'), (SELECT om_profil FROM om_profil WHERE libelle = 'PROFIL NON CONFIGURÉ'), 'C1', 1, (SELECT om_widget FROM om_widget WHERE libelle = 'Profil non configuré'));

--
-- Profil 'CADRE SI'
--
INSERT INTO om_profil (om_profil, libelle, hierarchie) VALUES (nextval('om_profil_seq'), 'CADRE SI', 0);
INSERT INTO om_utilisateur (om_utilisateur, nom, email, login, pwd, om_collectivite, om_type, om_profil) VALUES (nextval('om_utilisateur_seq'), 'cadre-si', 'nospam@openmairie.org', 'cadre-si', '28f8247f9970bdfcf101911d76a1d19e', 1, 'db', (SELECT om_profil FROM om_profil WHERE libelle = 'CADRE SI'));
INSERT INTO om_dashboard (om_dashboard, om_profil, bloc, position, om_widget) VALUES (nextval('om_dashboard_seq'), (SELECT om_profil FROM om_profil WHERE libelle = 'CADRE SI'), 'C2', 1, (SELECT om_widget FROM om_widget WHERE script = 'mes_infos'));
INSERT INTO om_dashboard (om_dashboard, om_profil, bloc, position, om_widget) VALUES (nextval('om_dashboard_seq'), (SELECT om_profil FROM om_profil WHERE libelle = 'CADRE SI'), 'C1', 1, (SELECT om_widget FROM om_widget WHERE script = 'activite_service'));
INSERT INTO om_dashboard (om_dashboard, om_profil, bloc, position, om_widget) VALUES (nextval('om_dashboard_seq'), (SELECT om_profil FROM om_profil WHERE libelle = 'CADRE SI'), 'C2', 2, (SELECT om_widget FROM om_widget WHERE script = 'dossier_coordination_a_qualifier'));
INSERT INTO om_dashboard (om_dashboard, om_profil, bloc, position, om_widget) VALUES (nextval('om_dashboard_seq'), (SELECT om_profil FROM om_profil WHERE libelle = 'CADRE SI'), 'C1', 2, (SELECT om_widget FROM om_widget WHERE script = 'programmation_urgentes'));
INSERT INTO om_dashboard (om_dashboard, om_profil, bloc, position, om_widget) VALUES (nextval('om_dashboard_seq'), (SELECT om_profil FROM om_profil WHERE libelle = 'CADRE SI'), 'C2', 3, (SELECT om_widget FROM om_widget WHERE script = 'programmation_a_valider'));
INSERT INTO om_dashboard (om_dashboard, om_profil, bloc, position, om_widget) VALUES (nextval('om_dashboard_seq'), (SELECT om_profil FROM om_profil WHERE libelle = 'CADRE SI'), 'C2', 4, (SELECT om_widget FROM om_widget WHERE script = 'analyse_a_valider'));
INSERT INTO om_dashboard (om_dashboard, om_profil, bloc, position, om_widget) VALUES (nextval('om_dashboard_seq'), (SELECT om_profil FROM om_profil WHERE libelle = 'CADRE SI'), 'C2', 5, (SELECT om_widget FROM om_widget WHERE script = 'documents_entrants_a_valider'));
INSERT INTO om_dashboard (om_dashboard, om_profil, bloc, position, om_widget) VALUES (nextval('om_dashboard_seq'), (SELECT om_profil FROM om_profil WHERE libelle = 'CADRE SI'), 'C2', 6, (SELECT om_widget FROM om_widget WHERE script = 'documents_entrants_suivis'));
INSERT INTO om_dashboard (om_dashboard, om_profil, bloc, position, om_widget) VALUES (nextval('om_dashboard_seq'), (SELECT om_profil FROM om_profil WHERE libelle = 'CADRE SI'), 'C2', 7, (SELECT om_widget FROM om_widget WHERE script = 'dossier_coordination_a_cloturer'));
INSERT INTO om_dashboard (om_dashboard, om_profil, bloc, position, om_widget) VALUES (nextval('om_dashboard_seq'), (SELECT om_profil FROM om_profil WHERE libelle = 'CADRE SI'), 'C1', 1, (SELECT om_widget FROM om_widget WHERE script = 'message_mes_non_lu'));
INSERT INTO om_dashboard (om_dashboard, om_profil, bloc, position, om_widget) VALUES (nextval('om_dashboard_seq'), (SELECT om_profil FROM om_profil WHERE libelle = 'CADRE SI'), 'C1', 1, (SELECT om_widget FROM om_widget WHERE script = 'documents_generes_a_editer'));
INSERT INTO om_dashboard (om_dashboard, om_profil, bloc, position, om_widget) VALUES (nextval('om_dashboard_seq'), (SELECT om_profil FROM om_profil WHERE libelle = 'CADRE SI'), 'C1', 1, (SELECT om_widget FROM om_widget WHERE script = 'documents_generes_attente_signature'));
INSERT INTO om_dashboard (om_dashboard, om_profil, bloc, position, om_widget) VALUES (nextval('om_dashboard_seq'), (SELECT om_profil FROM om_profil WHERE libelle = 'CADRE SI'), 'C1', 1, (SELECT om_widget FROM om_widget WHERE script = 'documents_generes_attente_retour_ar'));


--
-- Profil 'CADRE ACC'
--
INSERT INTO om_profil (om_profil, libelle, hierarchie) VALUES (nextval('om_profil_seq'), 'CADRE ACC', 0);
INSERT INTO om_utilisateur (om_utilisateur, nom, email, login, pwd, om_collectivite, om_type, om_profil) VALUES (nextval('om_utilisateur_seq'), 'cadre-acc', 'nospam@openmairie.org', 'cadre-acc', '06ff6e28cfb3cce326fa7bfc2f17b9f9', 1, 'db', (SELECT om_profil FROM om_profil WHERE libelle = 'CADRE ACC'));
INSERT INTO om_dashboard (om_dashboard, om_profil, bloc, position, om_widget) VALUES (nextval('om_dashboard_seq'), (SELECT om_profil FROM om_profil WHERE libelle = 'CADRE ACC'), 'C2', 1, (SELECT om_widget FROM om_widget WHERE script = 'mes_infos'));
INSERT INTO om_dashboard (om_dashboard, om_profil, bloc, position, om_widget) VALUES (nextval('om_dashboard_seq'), (SELECT om_profil FROM om_profil WHERE libelle = 'CADRE ACC'), 'C1', 1, (SELECT om_widget FROM om_widget WHERE script = 'activite_service'));
INSERT INTO om_dashboard (om_dashboard, om_profil, bloc, position, om_widget) VALUES (nextval('om_dashboard_seq'), (SELECT om_profil FROM om_profil WHERE libelle = 'CADRE ACC'), 'C2', 2, (SELECT om_widget FROM om_widget WHERE script = 'dossier_coordination_a_qualifier'));
INSERT INTO om_dashboard (om_dashboard, om_profil, bloc, position, om_widget) VALUES (nextval('om_dashboard_seq'), (SELECT om_profil FROM om_profil WHERE libelle = 'CADRE ACC'), 'C1', 2, (SELECT om_widget FROM om_widget WHERE script = 'programmation_urgentes'));
INSERT INTO om_dashboard (om_dashboard, om_profil, bloc, position, om_widget) VALUES (nextval('om_dashboard_seq'), (SELECT om_profil FROM om_profil WHERE libelle = 'CADRE ACC'), 'C2', 3, (SELECT om_widget FROM om_widget WHERE script = 'programmation_a_valider'));
INSERT INTO om_dashboard (om_dashboard, om_profil, bloc, position, om_widget) VALUES (nextval('om_dashboard_seq'), (SELECT om_profil FROM om_profil WHERE libelle = 'CADRE ACC'), 'C2', 4, (SELECT om_widget FROM om_widget WHERE script = 'analyse_a_valider'));
INSERT INTO om_dashboard (om_dashboard, om_profil, bloc, position, om_widget) VALUES (nextval('om_dashboard_seq'), (SELECT om_profil FROM om_profil WHERE libelle = 'CADRE ACC'), 'C2', 5, (SELECT om_widget FROM om_widget WHERE script = 'documents_entrants_a_valider'));
INSERT INTO om_dashboard (om_dashboard, om_profil, bloc, position, om_widget) VALUES (nextval('om_dashboard_seq'), (SELECT om_profil FROM om_profil WHERE libelle = 'CADRE ACC'), 'C2', 6, (SELECT om_widget FROM om_widget WHERE script = 'documents_entrants_suivis'));
INSERT INTO om_dashboard (om_dashboard, om_profil, bloc, position, om_widget) VALUES (nextval('om_dashboard_seq'), (SELECT om_profil FROM om_profil WHERE libelle = 'CADRE ACC'), 'C2', 7, (SELECT om_widget FROM om_widget WHERE script = 'dossier_coordination_a_cloturer'));
INSERT INTO om_dashboard (om_dashboard, om_profil, bloc, position, om_widget) VALUES (nextval('om_dashboard_seq'), (SELECT om_profil FROM om_profil WHERE libelle = 'CADRE ACC'), 'C1', 1, (SELECT om_widget FROM om_widget WHERE script = 'message_mes_non_lu'));
INSERT INTO om_dashboard (om_dashboard, om_profil, bloc, position, om_widget) VALUES (nextval('om_dashboard_seq'), (SELECT om_profil FROM om_profil WHERE libelle = 'CADRE ACC'), 'C1', 1, (SELECT om_widget FROM om_widget WHERE script = 'documents_generes_a_editer'));
INSERT INTO om_dashboard (om_dashboard, om_profil, bloc, position, om_widget) VALUES (nextval('om_dashboard_seq'), (SELECT om_profil FROM om_profil WHERE libelle = 'CADRE ACC'), 'C1', 1, (SELECT om_widget FROM om_widget WHERE script = 'documents_generes_attente_signature'));
INSERT INTO om_dashboard (om_dashboard, om_profil, bloc, position, om_widget) VALUES (nextval('om_dashboard_seq'), (SELECT om_profil FROM om_profil WHERE libelle = 'CADRE ACC'), 'C1', 1, (SELECT om_widget FROM om_widget WHERE script = 'documents_generes_attente_retour_ar'));


--
-- Profil 'TECHNICIEN SI'
--
INSERT INTO om_profil (om_profil, libelle, hierarchie) VALUES (nextval('om_profil_seq'), 'TECHNICIEN SI', 0);
INSERT INTO om_utilisateur (om_utilisateur, nom, email, login, pwd, om_collectivite, om_type, om_profil) VALUES (nextval('om_utilisateur_seq'), 'technicien-si', 'nospam@openmairie.org', 'technicien-si', '27ca8279fed45a2f0259d3390743a782', 1, 'db', (SELECT om_profil FROM om_profil WHERE libelle = 'TECHNICIEN SI'));
INSERT INTO om_dashboard (om_dashboard, om_profil, bloc, position, om_widget) VALUES (nextval('om_dashboard_seq'), (SELECT om_profil FROM om_profil WHERE libelle = 'TECHNICIEN SI'), 'C2', 1, (SELECT om_widget FROM om_widget WHERE script = 'mes_infos'));
INSERT INTO om_dashboard (om_dashboard, om_profil, bloc, position, om_widget) VALUES (nextval('om_dashboard_seq'), (SELECT om_profil FROM om_profil WHERE libelle = 'TECHNICIEN SI'), 'C1', 1, (SELECT om_widget FROM om_widget WHERE script = 'mon_activite'));
INSERT INTO om_dashboard (om_dashboard, om_profil, bloc, position, om_widget) VALUES (nextval('om_dashboard_seq'), (SELECT om_profil FROM om_profil WHERE libelle = 'TECHNICIEN SI'), 'C1', 2, (SELECT om_widget FROM om_widget WHERE script = 'mes_di_en_reunions'));
INSERT INTO om_dashboard (om_dashboard, om_profil, bloc, position, om_widget) VALUES (nextval('om_dashboard_seq'), (SELECT om_profil FROM om_profil WHERE libelle = 'TECHNICIEN SI'), 'C2', 2, (SELECT om_widget FROM om_widget WHERE script = 'dossier_instruction_mes_plans'));
INSERT INTO om_dashboard (om_dashboard, om_profil, bloc, position, om_widget) VALUES (nextval('om_dashboard_seq'), (SELECT om_profil FROM om_profil WHERE libelle = 'TECHNICIEN SI'), 'C2', 3, (SELECT om_widget FROM om_widget WHERE script = 'dossier_instruction_mes_visites'));
INSERT INTO om_dashboard (om_dashboard, om_profil, bloc, position, om_widget) VALUES (nextval('om_dashboard_seq'), (SELECT om_profil FROM om_profil WHERE libelle = 'TECHNICIEN SI'), 'C1', 3, (SELECT om_widget FROM om_widget WHERE script = 'mes_visites_a_realiser'));
INSERT INTO om_dashboard (om_dashboard, om_profil, bloc, position, om_widget) VALUES (nextval('om_dashboard_seq'), (SELECT om_profil FROM om_profil WHERE libelle = 'TECHNICIEN SI'), 'C1', 3, (SELECT om_widget FROM om_widget WHERE script = 'mes_documents_entrants_non_lus'));
INSERT INTO om_dashboard (om_dashboard, om_profil, bloc, position, om_widget) VALUES (nextval('om_dashboard_seq'), (SELECT om_profil FROM om_profil WHERE libelle = 'TECHNICIEN SI'), 'C1', 1, (SELECT om_widget FROM om_widget WHERE script = 'message_mes_non_lu'));


--
-- Profil 'TECHNICIEN ACC'
--
INSERT INTO om_profil (om_profil, libelle, hierarchie) VALUES (nextval('om_profil_seq'), 'TECHNICIEN ACC', 0);
INSERT INTO om_utilisateur (om_utilisateur, nom, email, login, pwd, om_collectivite, om_type, om_profil) VALUES (nextval('om_utilisateur_seq'), 'technicien-acc', 'nospam@openmairie.org', 'technicien-acc', '0ae54d3a09a236243800fb29dca264a3', 1, 'db', (SELECT om_profil FROM om_profil WHERE libelle = 'TECHNICIEN ACC'));
INSERT INTO om_dashboard (om_dashboard, om_profil, bloc, position, om_widget) VALUES (nextval('om_dashboard_seq'), (SELECT om_profil FROM om_profil WHERE libelle = 'TECHNICIEN ACC'), 'C2', 1, (SELECT om_widget FROM om_widget WHERE script = 'mes_infos'));
INSERT INTO om_dashboard (om_dashboard, om_profil, bloc, position, om_widget) VALUES (nextval('om_dashboard_seq'), (SELECT om_profil FROM om_profil WHERE libelle = 'TECHNICIEN ACC'), 'C1', 1, (SELECT om_widget FROM om_widget WHERE script = 'mon_activite'));
INSERT INTO om_dashboard (om_dashboard, om_profil, bloc, position, om_widget) VALUES (nextval('om_dashboard_seq'), (SELECT om_profil FROM om_profil WHERE libelle = 'TECHNICIEN ACC'), 'C1', 2, (SELECT om_widget FROM om_widget WHERE script = 'mes_di_en_reunions'));
INSERT INTO om_dashboard (om_dashboard, om_profil, bloc, position, om_widget) VALUES (nextval('om_dashboard_seq'), (SELECT om_profil FROM om_profil WHERE libelle = 'TECHNICIEN ACC'), 'C2', 2, (SELECT om_widget FROM om_widget WHERE script = 'dossier_instruction_mes_plans'));
INSERT INTO om_dashboard (om_dashboard, om_profil, bloc, position, om_widget) VALUES (nextval('om_dashboard_seq'), (SELECT om_profil FROM om_profil WHERE libelle = 'TECHNICIEN ACC'), 'C2', 2, (SELECT om_widget FROM om_widget WHERE script = 'dossier_instruction_mes_visites'));
INSERT INTO om_dashboard (om_dashboard, om_profil, bloc, position, om_widget) VALUES (nextval('om_dashboard_seq'), (SELECT om_profil FROM om_profil WHERE libelle = 'TECHNICIEN ACC'), 'C1', 3, (SELECT om_widget FROM om_widget WHERE script = 'mes_visites_a_realiser'));
INSERT INTO om_dashboard (om_dashboard, om_profil, bloc, position, om_widget) VALUES (nextval('om_dashboard_seq'), (SELECT om_profil FROM om_profil WHERE libelle = 'TECHNICIEN ACC'), 'C1', 3, (SELECT om_widget FROM om_widget WHERE script = 'mes_documents_entrants_non_lus'));
INSERT INTO om_dashboard (om_dashboard, om_profil, bloc, position, om_widget) VALUES (nextval('om_dashboard_seq'), (SELECT om_profil FROM om_profil WHERE libelle = 'TECHNICIEN ACC'), 'C1', 1, (SELECT om_widget FROM om_widget WHERE script = 'message_mes_non_lu'));


--
-- Profil 'SECRETAIRE SI'
--
INSERT INTO om_profil (om_profil, libelle, hierarchie) VALUES (nextval('om_profil_seq'), 'SECRETAIRE SI', 0);
INSERT INTO om_utilisateur (om_utilisateur, nom, email, login, pwd, om_collectivite, om_type, om_profil) VALUES (nextval('om_utilisateur_seq'), 'secretaire-si', 'nospam@openmairie.org', 'secretaire-si', '6b8a509cde10f837639417c2ea101106', 1, 'db', (SELECT om_profil FROM om_profil WHERE libelle = 'SECRETAIRE SI'));
INSERT INTO om_dashboard (om_dashboard, om_profil, bloc, position, om_widget) VALUES (nextval('om_dashboard_seq'), (SELECT om_profil FROM om_profil WHERE libelle = 'SECRETAIRE SI'), 'C2', 1, (SELECT om_widget FROM om_widget WHERE script = 'mes_infos'));
INSERT INTO om_dashboard (om_dashboard, om_profil, bloc, position, om_widget) VALUES (nextval('om_dashboard_seq'), (SELECT om_profil FROM om_profil WHERE libelle = 'SECRETAIRE SI'), 'C1', 2, (SELECT om_widget FROM om_widget WHERE script = 'activite_service'));
INSERT INTO om_dashboard (om_dashboard, om_profil, bloc, position, om_widget) VALUES (nextval('om_dashboard_seq'), (SELECT om_profil FROM om_profil WHERE libelle = 'SECRETAIRE SI'), 'C2', 2, (SELECT om_widget FROM om_widget WHERE script = 'convocations_exploitants_a_envoyer'));
INSERT INTO om_dashboard (om_dashboard, om_profil, bloc, position, om_widget) VALUES (nextval('om_dashboard_seq'), (SELECT om_profil FROM om_profil WHERE libelle = 'SECRETAIRE SI'), 'C2', 2, (SELECT om_widget FROM om_widget WHERE script = 'convocations_membres_a_envoyer'));
INSERT INTO om_dashboard (om_dashboard, om_profil, bloc, position, om_widget) VALUES (nextval('om_dashboard_seq'), (SELECT om_profil FROM om_profil WHERE libelle = 'SECRETAIRE SI'), 'C2', 2, (SELECT om_widget FROM om_widget WHERE script = 'etablissements_npai'));
INSERT INTO om_dashboard (om_dashboard, om_profil, bloc, position, om_widget) VALUES (nextval('om_dashboard_seq'), (SELECT om_profil FROM om_profil WHERE libelle = 'SECRETAIRE SI'), 'C2', 2, (SELECT om_widget FROM om_widget WHERE script = 'analyse_a_acter'));
INSERT INTO om_dashboard (om_dashboard, om_profil, bloc, position, om_widget) VALUES (nextval('om_dashboard_seq'), (SELECT om_profil FROM om_profil WHERE libelle = 'SECRETAIRE SI'), 'C1', 1, (SELECT om_widget FROM om_widget WHERE script = 'documents_generes_a_editer'));
INSERT INTO om_dashboard (om_dashboard, om_profil, bloc, position, om_widget) VALUES (nextval('om_dashboard_seq'), (SELECT om_profil FROM om_profil WHERE libelle = 'SECRETAIRE SI'), 'C1', 1, (SELECT om_widget FROM om_widget WHERE script = 'documents_generes_attente_signature'));
INSERT INTO om_dashboard (om_dashboard, om_profil, bloc, position, om_widget) VALUES (nextval('om_dashboard_seq'), (SELECT om_profil FROM om_profil WHERE libelle = 'SECRETAIRE SI'), 'C1', 1, (SELECT om_widget FROM om_widget WHERE script = 'documents_generes_attente_retour_ar'));


--
-- Profil 'SECRETAIRE ACC'
--
INSERT INTO om_profil (om_profil, libelle, hierarchie) VALUES (nextval('om_profil_seq'), 'SECRETAIRE ACC', 0);
INSERT INTO om_utilisateur (om_utilisateur, nom, email, login, pwd, om_collectivite, om_type, om_profil) VALUES (nextval('om_utilisateur_seq'), 'secretaire-acc', 'nospam@openmairie.org', 'secretaire-acc', 'b9f374fbe7696fb2b07ce4e05d459b35', 1, 'db', (SELECT om_profil FROM om_profil WHERE libelle = 'SECRETAIRE ACC'));
INSERT INTO om_dashboard (om_dashboard, om_profil, bloc, position, om_widget) VALUES (nextval('om_dashboard_seq'), (SELECT om_profil FROM om_profil WHERE libelle = 'SECRETAIRE ACC'), 'C2', 1, (SELECT om_widget FROM om_widget WHERE script = 'mes_infos'));
INSERT INTO om_dashboard (om_dashboard, om_profil, bloc, position, om_widget) VALUES (nextval('om_dashboard_seq'), (SELECT om_profil FROM om_profil WHERE libelle = 'SECRETAIRE ACC'), 'C1', 2, (SELECT om_widget FROM om_widget WHERE script = 'activite_service'));
INSERT INTO om_dashboard (om_dashboard, om_profil, bloc, position, om_widget) VALUES (nextval('om_dashboard_seq'), (SELECT om_profil FROM om_profil WHERE libelle = 'SECRETAIRE ACC'), 'C2', 2, (SELECT om_widget FROM om_widget WHERE script = 'convocations_exploitants_a_envoyer'));
INSERT INTO om_dashboard (om_dashboard, om_profil, bloc, position, om_widget) VALUES (nextval('om_dashboard_seq'), (SELECT om_profil FROM om_profil WHERE libelle = 'SECRETAIRE ACC'), 'C2', 2, (SELECT om_widget FROM om_widget WHERE script = 'convocations_membres_a_envoyer'));
INSERT INTO om_dashboard (om_dashboard, om_profil, bloc, position, om_widget) VALUES (nextval('om_dashboard_seq'), (SELECT om_profil FROM om_profil WHERE libelle = 'SECRETAIRE ACC'), 'C2', 2, (SELECT om_widget FROM om_widget WHERE script = 'etablissements_npai'));
INSERT INTO om_dashboard (om_dashboard, om_profil, bloc, position, om_widget) VALUES (nextval('om_dashboard_seq'), (SELECT om_profil FROM om_profil WHERE libelle = 'SECRETAIRE ACC'), 'C2', 2, (SELECT om_widget FROM om_widget WHERE script = 'analyse_a_acter'));
INSERT INTO om_dashboard (om_dashboard, om_profil, bloc, position, om_widget) VALUES (nextval('om_dashboard_seq'), (SELECT om_profil FROM om_profil WHERE libelle = 'SECRETAIRE ACC'), 'C1', 1, (SELECT om_widget FROM om_widget WHERE script = 'documents_generes_a_editer'));
INSERT INTO om_dashboard (om_dashboard, om_profil, bloc, position, om_widget) VALUES (nextval('om_dashboard_seq'), (SELECT om_profil FROM om_profil WHERE libelle = 'SECRETAIRE ACC'), 'C1', 1, (SELECT om_widget FROM om_widget WHERE script = 'documents_generes_attente_signature'));
INSERT INTO om_dashboard (om_dashboard, om_profil, bloc, position, om_widget) VALUES (nextval('om_dashboard_seq'), (SELECT om_profil FROM om_profil WHERE libelle = 'SECRETAIRE ACC'), 'C1', 1, (SELECT om_widget FROM om_widget WHERE script = 'documents_generes_attente_retour_ar'));


--
-- Data for Name: acteur; Type: TABLE DATA; Schema: openaria; Owner: -
--

INSERT INTO acteur (acteur, nom_prenom, om_utilisateur, service, role, acronyme, couleur, om_validite_debut, om_validite_fin, reference) VALUES (nextval('acteur_seq'), 'Luce DAVISO', (SELECT om_utilisateur FROM om_utilisateur WHERE login = 'secretaire-acc'), (SELECT service FROM service WHERE code = 'ACC'), 'secretaire', 'PAD', NULL, NULL, NULL, NULL);
INSERT INTO acteur (acteur, nom_prenom, om_utilisateur, service, role, acronyme, couleur, om_validite_debut, om_validite_fin, reference) VALUES (nextval('acteur_seq'), 'Jeanne MARTIN', (SELECT om_utilisateur FROM om_utilisateur WHERE login = 'secretaire-si'), (SELECT service FROM service WHERE code = 'SI'), 'secretaire', 'JED', NULL, NULL, NULL, NULL);
INSERT INTO acteur (acteur, nom_prenom, om_utilisateur, service, role, acronyme, couleur, om_validite_debut, om_validite_fin, reference) VALUES (nextval('acteur_seq'), 'Martine DUBOIS', (SELECT om_utilisateur FROM om_utilisateur WHERE login = 'cadre-acc'), (SELECT service FROM service WHERE code = 'ACC'), 'cadre', 'MAD', NULL, NULL, NULL, NULL);
INSERT INTO acteur (acteur, nom_prenom, om_utilisateur, service, role, acronyme, couleur, om_validite_debut, om_validite_fin, reference) VALUES (nextval('acteur_seq'), 'Pierre BERNARD', (SELECT om_utilisateur FROM om_utilisateur WHERE login = 'technicien-acc'), (SELECT service FROM service WHERE code = 'ACC'), 'technicien', 'PIB', NULL, NULL, NULL, NULL);
INSERT INTO acteur (acteur, nom_prenom, om_utilisateur, service, role, acronyme, couleur, om_validite_debut, om_validite_fin, reference) VALUES (nextval('acteur_seq'), 'Paul DURAND', (SELECT om_utilisateur FROM om_utilisateur WHERE login = 'technicien-si'), (SELECT service FROM service WHERE code = 'SI'), 'technicien', 'PAD', NULL, NULL, NULL, NULL);
INSERT INTO acteur (acteur, nom_prenom, om_utilisateur, service, role, acronyme, couleur, om_validite_debut, om_validite_fin, reference) VALUES (nextval('acteur_seq'), 'Jean DUPONT', (SELECT om_utilisateur FROM om_utilisateur WHERE login = 'cadre-si'), (SELECT service FROM service WHERE code = 'SI'), 'cadre', 'JED', NULL, NULL, NULL, NULL);

