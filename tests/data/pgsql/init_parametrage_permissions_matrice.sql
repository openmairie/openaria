-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
--
-- Création de la matrice des permissions
--
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------

-- On vide la table om_droit
DELETE FROM om_droit;

--
-- Profil 'ADMINISTRATEUR'
--

--
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'), libelle, (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR') FROM om_permission;

--
\i 'tests/data/pgsql/init_parametrage_permissions_profil_cadre_si.sql'
\i 'tests/data/pgsql/init_parametrage_permissions_profil_cadre_acc.sql'
\i 'tests/data/pgsql/init_parametrage_permissions_profil_technicien_si.sql'
\i 'tests/data/pgsql/init_parametrage_permissions_profil_technicien_acc.sql'
\i 'tests/data/pgsql/init_parametrage_permissions_profil_secretaire_si.sql'
\i 'tests/data/pgsql/init_parametrage_permissions_profil_secretaire_acc.sql'

