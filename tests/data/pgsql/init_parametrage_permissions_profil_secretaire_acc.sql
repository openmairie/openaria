--
--
--

--
\set profil '\'SECRETAIRE ACC\''

--
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES 

--
-- Rubriques de menu disponibles
--
(nextval('om_droit_seq'), 'menu_etablissement', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'menu_dossier_instruction', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'menu_suivi', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),

--
-- Rubrique ÉTABLISSEMENTS
--

-- Listings de la rubrique "Établissements"
(nextval('om_droit_seq'), 'etablissement_referentiel_erp_tab', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'etablissement_referentiel_erp_consulter', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'etablissement_tous_tab', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'etablissement_tous_consulter', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'etablissement_unite_tab', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'etablissement_unite_consulter', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),

-- Actions sur la fiche établissement
(nextval('om_droit_seq'), 'etablissement_tous_geoaria', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'etablissement_referentiel_erp_geoaria', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),

-- Onglets sur la fiche établissement
(nextval('om_droit_seq'), 'contact_tab', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'contact_consulter', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'dossier_coordination_tab', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'dossier_coordination_consulter', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'piece_tab', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'piece_consulter', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'piece_uid_telecharger', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'courrier_tab', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'courrier_consulter', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'courrier_om_fichier_finalise_courrier_telecharger', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'courrier_om_fichier_signe_courrier_telecharger', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'courrier_send_pv_to_referentiel_ads', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'autorite_police_tab', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'autorite_police_consulter', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
-- (nextval('om_droit_seq'), 'etablissement_unite_tab', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
-- (nextval('om_droit_seq'), 'etablissement_unite_consulter', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'etablissement_unite__contexte_etab__ua_valide_tab', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'etablissement_unite__contexte_etab__ua_valide_consulter', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'etablissement_unite__contexte_etab__ua_enprojet_tab', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'etablissement_unite__contexte_etab__ua_enprojet_consulter', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'etablissement_unite__contexte_etab__ua_archive_tab', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'etablissement_unite__contexte_etab__ua_archive_consulter', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),

-- Actions sur l'onglet contact / établissement
(nextval('om_droit_seq'), 'contact_ajouter', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'contact_modifier', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),

-- Actions sur l'onglet DC / établissement

-- Actions sur l'onglet Documents Entrants / établissement
(nextval('om_droit_seq'), 'piece_ajouter', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),

-- Actions sur l'onglet Documents Générés / établissement
(nextval('om_droit_seq'), 'courrier_ajouter', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'courrier_modifier', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'courrier_finalise', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'courrier_previsualiser', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),


-- Actions sur l'onglet Autorité de Police / établissement

-- Actions sur l'onglet UA / établissement

-- Actions sur la fiche UA

-- Onglets sur la fiche UA

--
-- Rubrique DOSSIERS
--

-- Listings de la rubrique "Dossiers"
--(nextval('om_droit_seq'), 'dossier_coordination_tab', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
--(nextval('om_droit_seq'), 'dossier_coordination_consulter', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'dossier_instruction_tous_plans_tab', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'dossier_instruction_tous_plans_consulter', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'dossier_instruction_tous_visites_tab', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'dossier_instruction_tous_visites_consulter', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'dossier_instruction_tab', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'dossier_instruction_consulter', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),

-- Actions sur la fiche DC
(nextval('om_droit_seq'), 'dossier_coordination_geoaria', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),

-- Onglets sur la fiche DC
(nextval('om_droit_seq'), 'contact_contexte_dossier_coordination_tab', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'contact_contexte_dossier_coordination_consulter', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),

-- Actions sur l'onglet Contacts / DC
(nextval('om_droit_seq'), 'contact_contexte_dossier_coordination_ajouter', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'contact_contexte_dossier_coordination_modifier', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'contact_contexte_dossier_coordination_supprimer', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),

-- Actions sur l'onglet DC Fils / DC
-- Actions sur l'onglet Documents Entrants / DC
-- Actions sur l'onglet Documents Générés / DC
-- Actions sur l'onglet AP / DC

-- Actions sur la fiche DI

-- Onglets sur la fiche DI
(nextval('om_droit_seq'), 'analyses_tab', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'analyses_consulter', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'dossier_instruction_analyses', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'proces_verbal_tab', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'proces_verbal_consulter', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'dossier_instruction_proces_verbal', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'dossier_instruction_reunion_contexte_di_tab', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'dossier_instruction_reunion_contexte_di_consulter', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'visite_tab', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'visite_consulter', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),

-- Actions sur l'onglet Analyse / DI
(nextval('om_droit_seq'), 'analyses_rapport_analyse', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'analyses_proces_verbal', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
--
(nextval('om_droit_seq'), 'analyses_finaliser', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'analyses_terminee_reouvrir', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
-- consultation
(nextval('om_droit_seq'), 'analyses_consulter_analyses_type', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'analyses_consulter_objet', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'analyses_consulter_descriptif_etablissement', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'analyses_consulter_classification_etablissement', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'analyses_consulter_donnees_techniques', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'analyses_consulter_reglementation_applicable', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'analyses_consulter_prescriptions', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'analyses_consulter_essais_realises', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'analyses_consulter_compte_rendu', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'analyses_consulter_observation', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'analyses_consulter_avis_propose', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
-- modification analyse en cours
(nextval('om_droit_seq'), 'analyses_en_cours_modifier_analyses_type', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'analyses_en_cours_modifier_objet', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'analyses_en_cours_modifier_descriptif_etablissement', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'analyses_en_cours_modifier_classification_etablissement', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'analyses_en_cours_modifier_donnees_techniques', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'analyses_en_cours_modifier_reglementation_applicable', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'analyses_en_cours_modifier_prescriptions', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'analyses_en_cours_modifier_essais_realises', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'analyses_en_cours_modifier_compte_rendu', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'analyses_en_cours_modifier_observation', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'analyses_en_cours_modifier_avis_propose', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'prescription_reglementaire_consulter', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
--
(nextval('om_droit_seq'), 'etablissement_unite__contexte_di_analyse__ua_en_analyse_tab', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'etablissement_unite__contexte_di_analyse__ua_en_analyse_consulter', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'etablissement_unite__contexte_di_analyse__ua_valide_sur_etab_tab', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'etablissement_unite__contexte_di_analyse__ua_valide_sur_etab_consulter', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
--
(nextval('om_droit_seq'), 'etablissement_unite__contexte_di_analyse__ua_en_analyse_modifier', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'etablissement_unite__contexte_di_analyse__ua_valide_sur_etab_analyser', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),

-- Actions sur l'onglet PV / DI
(nextval('om_droit_seq'), 'proces_verbal_ajouter', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'proces_verbal_modifier', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'proces_verbal_om_fichier_finalise_telecharger', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'proces_verbal_om_fichier_signe_telecharger', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'proces_verbal_send_pv_to_referentiel_ads', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),

-- Actions sur l'onglet Documents Entrants / DI

-- Actions sur l'onglet Documents Générés / DI

-- Actions sur l'onglet Réunions / DI

-- Actions sur l'onglet Visites / DI

--
-- Rubrique SUIVI
--

-- Listings de la rubrique "Suivi"
(nextval('om_droit_seq'), 'piece_bannette_tab', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'piece_bannette_consulter', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'piece_bannette_uid_telecharger', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'courrier_a_editer_tab', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'courrier_attente_signature_tab', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'courrier_attente_retour_ar_tab', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'courrier_suivi_form_suivi', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'courrier_rar_form_rar', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'programmation_tab', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'programmation_consulter', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'reunion_tab', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'reunion_consulter', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),

-- Actions sur la fiche bannette
(nextval('om_droit_seq'), 'piece_bannette_ajouter', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'piece_bannette_modifier', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),

-- Actions sur la fiche Document Généré
(nextval('om_droit_seq'), 'courrier_a_editer_consulter', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'courrier_a_editer_om_fichier_finalise_courrier_telecharger', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'courrier_a_editer_om_fichier_signe_courrier_telecharger', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'courrier_a_editer_ajouter', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'courrier_a_editer_modifier', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'courrier_a_editer_supprimer', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'courrier_a_editer_finalise', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'courrier_a_editer_definalise', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'courrier_a_editer_previsualiser', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'courrier_a_editer_send_pv_to_referentiel_ads', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'courrier_attente_signature_consulter', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'courrier_attente_signature_om_fichier_finalise_courrier_telecharger', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'courrier_attente_signature_om_fichier_signe_courrier_telecharger', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'courrier_attente_signature_ajouter', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'courrier_attente_signature_modifier', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'courrier_attente_signature_supprimer', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'courrier_attente_signature_finalise', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'courrier_attente_signature_definalise', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'courrier_attente_signature_previsualiser', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'courrier_attente_signature_send_pv_to_referentiel_ads', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'courrier_attente_retour_ar_consulter', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'courrier_attente_retour_ar_om_fichier_finalise_courrier_telecharger', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'courrier_attente_retour_ar_om_fichier_signe_courrier_telecharger', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'courrier_attente_retour_ar_ajouter', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'courrier_attente_retour_ar_modifier', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'courrier_attente_retour_ar_supprimer', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'courrier_attente_retour_ar_finalise', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'courrier_attente_retour_ar_definalise', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'courrier_attente_retour_ar_previsualiser', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'courrier_attente_retour_ar_send_pv_to_referentiel_ads', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),

-- Actions sur la fiche Programmation
(nextval('om_droit_seq'), 'programmation_envoyer_convoc_exploit', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'programmation_envoyer_part', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),

-- Actions sur la fiche Réunion
(nextval('om_droit_seq'), 'reunion_ajouter', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'reunion_type_json', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'reunion_modifier', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'reunion_convoquer', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'reunion_numeroter', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'reunion_planifier', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'reunion_deplanifier', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'reunion_planifier_nouveau', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'reunion_gerer', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'reunion_edition', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'reunion_cloturer', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'reunion_integrer_documents_numerises', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'reunion_om_fichier_reunion_odj_telecharger', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'reunion_om_fichier_reunion_cr_global_telecharger', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'reunion_om_fichier_reunion_cr_par_dossier_signe_telecharger', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'reunion_om_fichier_reunion_cr_global_signe_telecharger', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),

-- Onglets sur la fiche réunion
(nextval('om_droit_seq'), 'lien_reunion_r_instance_r_i_membre_tab', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'lien_reunion_r_instance_r_i_membre_consulter', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),

-- Action sur l'onglet Signataires / Fiche Réunion
(nextval('om_droit_seq'), 'lien_reunion_r_instance_r_i_membre_modifier', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),

-- Action sur la vue demande de passage / Fiche Réunion
(nextval('om_droit_seq'), 'dossier_instruction_reunion_contexte_reunion_consulter', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'dossier_instruction_reunion_contexte_reunion_rendre_l_avis', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'dossier_instruction_reunion_contexte_reunion_edition', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'dossier_instruction_reunion_consulter_resume', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'dossier_instruction_reunion_ajouter', (SELECT om_profil FROM om_profil WHERE libelle = :profil))



--
-- PARAMÉTRAGE ET ADMINISTRATION
--

;
