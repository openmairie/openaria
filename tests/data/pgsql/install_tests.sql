--------------------------------------------------------------------------------
-- Script d'installation des jeux de données pour les tests
--
-- ATTENTION ce script est prévu pour être appliqué après le install.sql
--
-- @package openaria
-- @version SVN : $Id$
--------------------------------------------------------------------------------
--
START TRANSACTION;
--
\set schema 'openaria'
--
SET search_path = :schema, public, pg_catalog;
--
\i 'tests/data/pgsql/init_parametrage.sql'
\i 'tests/data/pgsql/init_parametrage_widget_dashboard.sql'
\i 'tests/data/pgsql/init_parametrage_permissions.sql'
\i 'tests/data/pgsql/init_parametrage_permissions_matrice.sql'
\i 'tests/data/pgsql/init_parametrage_editions_1.sql'
\i 'tests/data/pgsql/init_parametrage_editions_2.sql'
\i 'tests/data/pgsql/init_data.sql'
\i 'tests/data/pgsql/init_geolocalisation.sql'
--
\i 'tests/data/pgsql/update_sequences.sql'
--
COMMIT;

