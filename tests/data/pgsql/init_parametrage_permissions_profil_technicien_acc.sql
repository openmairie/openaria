--
--
--

--
\set profil '\'TECHNICIEN ACC\''

--
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES 

--
-- Rubriques de menu disponibles
--
(nextval('om_droit_seq'), 'menu_etablissement', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'menu_dossier_instruction', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),

--
-- Rubrique ÉTABLISSEMENTS
--

-- Listings de la rubrique "Établissements"
(nextval('om_droit_seq'), 'etablissement_referentiel_erp_tab', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'etablissement_referentiel_erp_consulter', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'etablissement_tous_tab', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'etablissement_tous_consulter', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'etablissement_unite_tab', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'etablissement_unite_consulter', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),

-- Actions sur la fiche établissement
(nextval('om_droit_seq'), 'etablissement_tous_geoaria', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'etablissement_referentiel_erp_geoaria', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'lien_contrainte_etablissement', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'etablissement_contrainte', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),

-- Onglets sur la fiche établissement
(nextval('om_droit_seq'), 'contact_tab', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'contact_consulter', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'dossier_coordination_tab', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'dossier_coordination_consulter', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'piece_tab', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'piece_consulter', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'piece_uid_telecharger', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'courrier_tab', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'courrier_consulter', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'courrier_om_fichier_finalise_courrier_telecharger', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'courrier_om_fichier_signe_courrier_telecharger', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'courrier_send_pv_to_referentiel_ads', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'autorite_police_tab', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'autorite_police_consulter', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
-- (nextval('om_droit_seq'), 'etablissement_unite_tab', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
-- (nextval('om_droit_seq'), 'etablissement_unite_consulter', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'etablissement_unite__contexte_etab__ua_valide_tab', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'etablissement_unite__contexte_etab__ua_valide_consulter', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'etablissement_unite__contexte_etab__ua_enprojet_tab', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'etablissement_unite__contexte_etab__ua_enprojet_consulter', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'etablissement_unite__contexte_etab__ua_archive_tab', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'etablissement_unite__contexte_etab__ua_archive_consulter', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),

-- Actions sur l'onglet contact / établissement
(nextval('om_droit_seq'), 'contact_ajouter', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'contact_modifier', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),

-- Actions sur l'onglet DC / établissement

-- Actions sur l'onglet Documents Entrants / établissement
(nextval('om_droit_seq'), 'piece_ajouter', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'piece_modifier', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'piece_marquer_comme_lu', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),

-- Actions sur l'onglet Documents Générés / établissement
(nextval('om_droit_seq'), 'courrier_ajouter', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'courrier_modifier', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'courrier_supprimer', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'courrier_finalise', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'courrier_previsualiser', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),

-- Actions sur l'onglet Autorité de Police / établissement

-- Actions sur l'onglet UA / établissement
(nextval('om_droit_seq'), 'etablissement_unite__contexte_etab__ua_valide_ajouter', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),

-- Actions sur la fiche UA
(nextval('om_droit_seq'), 'etablissement_unite__contexte_etab__ua_valide_archiver', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'etablissement_unite__contexte_etab__ua_enprojet_archiver', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'etablissement_unite__contexte_etab__ua_enprojet_valider', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),


--
-- Rubrique DOSSIERS
--

-- Messages
(nextval('om_droit_seq'), 'dossier_coordination_message_contexte_dc_tab', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'dossier_coordination_message_contexte_di_tab', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'dossier_coordination_message_contexte_dc_consulter', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'dossier_coordination_message_contexte_dc_marquer_acc_technicien_comme_lu', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'dossier_coordination_message_contexte_dc_marquer_acc_technicien_comme_non_lu', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'dossier_coordination_message_contexte_di_consulter', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'dossier_coordination_message_contexte_di_marquer_acc_technicien_comme_lu', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'dossier_coordination_message_contexte_di_marquer_acc_technicien_comme_non_lu', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'dossier_coordination_message_mes_non_lu_tab', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),

-- Listings de la rubrique "Dossiers"
--(nextval('om_droit_seq'), 'dossier_coordination_tab', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
--(nextval('om_droit_seq'), 'dossier_coordination_consulter', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'dossier_instruction_mes_plans_tab', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'dossier_instruction_mes_plans_consulter', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'dossier_instruction_tous_plans_tab', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'dossier_instruction_tous_plans_consulter', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'dossier_instruction_mes_visites_tab', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'dossier_instruction_mes_visites_consulter', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'dossier_instruction_tous_visites_tab', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'dossier_instruction_tous_visites_consulter', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'dossier_instruction_tab', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'dossier_instruction_consulter', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'visite_mes_visites_a_realiser_tab', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'visite_mes_visites_a_realiser_consulter', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'piece_non_lu', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),

-- Actions sur la fiche DC
(nextval('om_droit_seq'), 'dossier_coordination_geoaria', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'lien_contrainte_dossier_coordination', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'dossier_coordination_contrainte', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),

-- Onglets sur la fiche DC
(nextval('om_droit_seq'), 'contact_contexte_dossier_coordination_tab', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'contact_contexte_dossier_coordination_consulter', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),

-- Actions sur l'onglet Contacts / DC
(nextval('om_droit_seq'), 'contact_contexte_dossier_coordination_ajouter', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'contact_contexte_dossier_coordination_modifier', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'contact_contexte_dossier_coordination_supprimer', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),

-- Actions sur l'onglet DC Fils / DC
-- Actions sur l'onglet Documents Entrants / DC
-- Actions sur l'onglet Documents Générés / DC
-- Actions sur l'onglet AP / DC

-- Actions sur la fiche DI
(nextval('om_droit_seq'), 'dossier_instruction_mes_plans_gerer_completude', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'dossier_instruction_tous_plans_gerer_completude', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'dossier_instruction_mes_visites_gerer_completude', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'dossier_instruction_tous_visites_gerer_completude', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'dossier_instruction_gerer_completude', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),

-- Onglets sur la fiche DI
(nextval('om_droit_seq'), 'analyses_tab', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'analyses_consulter', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'dossier_instruction_analyses', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'proces_verbal_tab', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'proces_verbal_consulter', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'dossier_instruction_proces_verbal', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'dossier_instruction_reunion_contexte_di_tab', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'dossier_instruction_reunion_contexte_di_consulter', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'visite_tab', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'visite_consulter', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),

-- Actions sur l'onglet Analyse / DI
(nextval('om_droit_seq'), 'analyses_rapport_analyse', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'analyses_proces_verbal', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
--
(nextval('om_droit_seq'), 'analyses_finaliser', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'analyses_terminee_reouvrir', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
-- consultation
(nextval('om_droit_seq'), 'analyses_consulter_analyses_type', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'analyses_consulter_objet', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'analyses_consulter_descriptif_etablissement', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'analyses_consulter_classification_etablissement', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'analyses_consulter_donnees_techniques', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'analyses_consulter_reglementation_applicable', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'analyses_consulter_prescriptions', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'analyses_consulter_essais_realises', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'analyses_consulter_compte_rendu', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'analyses_consulter_observation', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'analyses_consulter_avis_propose', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
-- modification analyse en cours
(nextval('om_droit_seq'), 'analyses_en_cours_modifier_analyses_type', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'analyses_en_cours_modifier_objet', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'analyses_en_cours_modifier_descriptif_etablissement', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'analyses_en_cours_modifier_classification_etablissement', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'analyses_en_cours_modifier_donnees_techniques', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'analyses_en_cours_modifier_reglementation_applicable', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'analyses_en_cours_modifier_prescriptions', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'analyses_en_cours_modifier_essais_realises', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'analyses_en_cours_modifier_compte_rendu', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'analyses_en_cours_modifier_observation', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'analyses_en_cours_modifier_avis_propose', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'prescription_reglementaire_consulter', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
--
(nextval('om_droit_seq'), 'etablissement_unite__contexte_di_analyse__ua_en_analyse_tab', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'etablissement_unite__contexte_di_analyse__ua_en_analyse_consulter', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'etablissement_unite__contexte_di_analyse__ua_valide_sur_etab_tab', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'etablissement_unite__contexte_di_analyse__ua_valide_sur_etab_consulter', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
--
(nextval('om_droit_seq'), 'etablissement_unite__contexte_di_analyse__ua_en_analyse_ajouter', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'etablissement_unite__contexte_di_analyse__ua_en_analyse_modifier', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'etablissement_unite__contexte_di_analyse__ua_valide_sur_etab_analyser', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),

-- Actions sur l'onglet PV / DI
(nextval('om_droit_seq'), 'proces_verbal_send_pv_to_referentiel_ads', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),

-- Actions sur l'onglet Documents Entrants / DI

-- Actions sur l'onglet Documents Générés / DI

-- Actions sur l'onglet Réunions / DI
(nextval('om_droit_seq'), 'dossier_instruction_reunion_contexte_di_ajouter', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'dossier_instruction_reunion_contexte_di_modifier_demande_de_passage', (SELECT om_profil FROM om_profil WHERE libelle = :profil))

-- Actions sur l'onglet Visites / DI

--
-- Rubrique SUIVI
--

--
-- PARAMÉTRAGE ET ADMINISTRATION
--

;
