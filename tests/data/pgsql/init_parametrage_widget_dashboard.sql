--------------------------------------------------------------------------------
-- INIT PARAMETRAGE WIDGET DASHBOARD
--
-- Initialisation de tous les widgets de tableau de bord.
--
-- @package openaria
-- @version SVN : $Id$
--------------------------------------------------------------------------------

--
INSERT INTO om_widget (om_widget, libelle, lien, texte, type, script, arguments) VALUES 
(nextval('om_widget_seq'), 'Mes infos', '', '', 'file', 'mes_infos', '')
,
(nextval('om_widget_seq'), 'Recherche directe d''établissement', '', '', 'file', 'direct_search', '')
,
(nextval('om_widget_seq'), 'Activité du service', '', '', 'file', 'activite_service', '')
,
(nextval('om_widget_seq'), 'Dossiers de coordination à qualifier', '', '', 'file', 'dossier_coordination_a_qualifier', '')
,
(nextval('om_widget_seq'), 'Mon activité', '', '', 'file', 'mon_activite', '')
,
(nextval('om_widget_seq'), 'Mes dossiers plans', '', '', 'file', 'dossier_instruction_mes_plans', '')
,
(nextval('om_widget_seq'), 'Mes dossiers visites', '', '', 'file', 'dossier_instruction_mes_visites', '')
,
(nextval('om_widget_seq'), 'Mes dossiers en réunion', '', '', 'file', 'mes_di_en_reunions', '')
,
(nextval('om_widget_seq'), 'Mes visites à réaliser', '', '', 'file', 'mes_visites_a_realiser', '')
,
(nextval('om_widget_seq'), 'Programmations urgentes', '', '', 'file', 'programmation_urgentes', '')
,
(nextval('om_widget_seq'), 'Programmations à valider', '', '', 'file', 'programmation_a_valider', '')
,
(nextval('om_widget_seq'), 'Convocations exploitants à envoyer', '', '', 'file', 'convocations_exploitants_a_envoyer', '')
,
(nextval('om_widget_seq'), 'Convocations membres à envoyer', '', '', 'file', 'convocations_membres_a_envoyer', '')
,
(nextval('om_widget_seq'), 'Établissements NPAI', '', '', 'file', 'etablissements_npai', '')
,
(nextval('om_widget_seq'), 'Analyses à valider', '', '', 'file', 'analyse_a_valider', '')
,
(nextval('om_widget_seq'), 'Analyses à acter', '', '', 'file', 'analyse_a_acter', '')
,
(nextval('om_widget_seq'), 'Autorités de police qui n''ont pas été notifiées ou exécutées', '', '', 'file', 'autorites_police_non_notifiees_executees', '')
,
(nextval('om_widget_seq'), 'Mes documents entrants non lus', '', '', 'file', 'mes_documents_entrants_non_lus', '')
,
(nextval('om_widget_seq'), 'Documents entrants à valider', '', '', 'file', 'documents_entrants_a_valider', '')
,
(nextval('om_widget_seq'), 'Documents entrants suivis', '', '', 'file', 'documents_entrants_suivis', '')
,
(nextval('om_widget_seq'), 'Profil non configuré', '', 'Votre profil ne peut accéder à aucune fonction du logiciel. Veuillez contacter l''administrateur pour que vos permissions soient configurées.', 'web', '', '')
,
(nextval('om_widget_seq'), 'Dossiers de coordination à clôturer', '', '', 'file', 'dossier_coordination_a_cloturer', '')
,
(nextval('om_widget_seq'), 'Mes messages', '', '', 'file', 'message_mes_non_lu', '')
,
(nextval('om_widget_seq'), 'Documents générés à éditer', '', '', 'file', 'documents_generes_a_editer', '')
,
(nextval('om_widget_seq'), 'Documents générés en attente de signature', '', '', 'file', 'documents_generes_attente_signature', '')
,
(nextval('om_widget_seq'), 'Documents générés en attente de retour AR', '', '', 'file', 'documents_generes_attente_retour_ar', '')
,
(nextval('om_widget_seq'), 'Dossiers d''instruction à affecter et / ou à qualifier', '', '', 'file', 'dossier_instruction_a_qualifier_affecter', '')
;

--
INSERT INTO om_dashboard (om_dashboard, om_profil, bloc, position, om_widget) 
SELECT 
	nextval('om_dashboard_seq'),
	(SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR'),
	'C1',
	om_widget,
	om_widget
FROM om_widget
WHERE 
    om_widget%2 = 0
	AND libelle <> 'Profil non configuré';
--
INSERT INTO om_dashboard (om_dashboard, om_profil, bloc, position, om_widget) 
SELECT 
	nextval('om_dashboard_seq'),
	(SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR'),
	'C2',
	om_widget,
	om_widget
FROM om_widget
WHERE 
    om_widget%2 <> 0
	AND libelle <> 'Profil non configuré';
