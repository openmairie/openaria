<?php
/**
 * Ce script contient la définition de la classe 'GeneralCommon'.
 *
 * @package framework_openmairie
 * @version SVN : $Id$
 */

require_once "../tests/resources/omtestcase.class.php";

/**
 * Cette classe permet de tester unitairement les fonctions de l'application.
 */
abstract class GeneralCommon extends OMTestCase {
    /**
     * Méthode lancée en fin de traitement
     */
    public function common_tearDown() {
        parent::common_tearDown();
        //
        $this->clean_session();
    }

    /**
     * Test la fonction define_next_week() de la classe Programmation.
     */
    public function test_next_week() {
        // Instanciation de la classe *om_application*
        $f = $this->get_inst_om_application();
        $f->disableLog();
        // Année en cours
        $year = date("Y");
        // Nombre de semaine de l'année en cours
        $weeks_per_year =  date("W", mktime(0,0,0,12,28,$year));
        // Initialisation des variables
        $i = 1;
        // Test sur toute les semaines de l'année en cours
        while ($i < $weeks_per_year){
            $date = $f->define_next_week($year, $i);
            $this->assertEquals($year, $date["annee"]);
            $this->assertEquals(str_pad(++$i, 2, "0",STR_PAD_LEFT), $date["semaine"]);
        }
        //Test sur la première semaine de l'année suivante
        $date = $f->define_next_week($year, $i);
        $this->assertEquals($year+1, $date["annee"]);
        $this->assertEquals("01", $date["semaine"]);
        // Destruction de la classe *om_application*
        $f->__destruct();
    }
}
