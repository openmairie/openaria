*** Settings ***
Resource  resources/resources.robot
Suite Setup  For Suite Setup
Suite Teardown  For Suite Teardown
Documentation  Les documents entrants...


*** Test Cases ***
Constitution d'un jeu de données

    [Documentation]  L'objet de ce 'Test Case' est de constituer un jeu de
    ...  données cohérent pour les scénarios fonctionnels qui suivent.

    # En tant que profil ADMINISTRATEUR
    Depuis la page d'accueil  admin  admin

    # On définit une liste de clés pour lesquelles on souhaite vérifier
    # l'absence dans le fichier de métadonnées. Ce sont les métadonnées
    # spécifiques aux arrêtés, aux PV, aux réunions et aux signataires.
    @{md_no} =  Create List
    ...  arrete_numero
    ...  arrete_reglementaire
    ...  arrete_notification
    ...  arrete_date_notification
    ...  arrete_publication
    ...  arrete_date_publication
    ...  arrete_temporaire
    ...  arrete_expiration
    ...  arrete_date_controle_legalite
    ...  arrete_nature_acte
    ...  arrete_nature_acte_niv1
    ...  arrete_nature_acte_niv2
    ...  signataire
    ...  signataire_qualite
    ...  date_signature
    ...  pv_erp_numero
    ...  pv_erp_nature_analyse
    ...  pv_erp_reference_urbanisme
    ...  pv_erp_avis_rendu
    ...  code_reunion
    ...  date_reunion
    ...  type_reunion
    ...  commission
    Set Suite Variable  ${md_no}

    ##
    ## ETABLISSEMENT 01
    ##
    &{etab01} =  Create Dictionary
    ...  libelle=Magasin EIGHT
    ...  etablissement_nature=ERP potentiel
    ...  adresse_numero=85
    ...  adresse_voie=RUE GASTON BERGER
    ...  adresse_cp=13010
    ...  adresse_ville=MARSEILLE
    ...  adresse_arrondissement=10ème
    ${etab01_code} =  Ajouter l'établissement  ${etab01}
    ${etab01_titre} =  Set Variable  ${etab01_code} - ${etab01.libelle}
    Set Suite Variable  ${etab01}
    Set Suite Variable  ${etab01_code}
    Set Suite Variable  ${etab01_titre}

    ##
    ## ETABLISSEMENT 02
    ##
    &{etab02} =  Create Dictionary
    ...  libelle=MONDIAL EV TEST130
    ...  etablissement_nature=ERP potentiel
    ...  adresse_numero=4
    ...  adresse_numero2=bis
    ...  adresse_voie=AVE ROGER SALENGRO
    ...  adresse_complement=CHEZ M. DURAND
    ...  adresse_cp=13002
    ...  adresse_ville=MARSEILLE
    ...  adresse_arrondissement=2ème
    ...  siret=73282932000074
    ...  exp_nom=DURAND
    ...  exp_prenom=Jean
    ...  etablissement_statut_juridique=Ville
    ...  ref_patrimoine=120\n90
    ${etab02_code} =  Ajouter l'établissement  ${etab02}
    ${etab02_titre} =  Set Variable  ${etab02_code} - ${etab02.libelle}
    Set Suite Variable  ${etab02}
    Set Suite Variable  ${etab02_code}
    Set Suite Variable  ${etab02_titre}

    ##
    ## DOSSIER DE COORDINATION 01
    ##
    &{dc01} =  Create Dictionary
    ...  dossier_coordination_type=Autorisation de Travaux
    ...  description=Description
    ...  etablissement=${etab02_titre}
    ...  date_demande=09/11/2014
    ...  a_qualifier=false
    ...  dossier_instruction_secu=true
    ...  dossier_instruction_acc=true
    ${dc01_libelle} =  Ajouter le dossier de coordination  ${dc01}
    Set Suite Variable  ${dc01}
    Set Suite Variable  ${dc01_libelle}
    ${dc01_di_acc} =  Catenate  SEPARATOR=-  ${dc01_libelle}  ACC
    ${dc01_di_si} =  Catenate  SEPARATOR=-  ${dc01_libelle}  SI
    Set Suite Variable  ${dc01_di_acc}
    Set Suite Variable  ${dc01_di_si}


Création et qualification d'un document entrant

    [Documentation]    Un document entrant est ajouté depuis le menu "Bannette",
    ...     il suit alors un workflow strict jusqu'à sa validation par un cadre.
    ...     L'objet de ce 'Test Case' est de tester que ce workflow est respecté.

    #
    Depuis la page d'accueil    secretaire-acc    secretaire-acc
    # On clique sur le tableau de bord
    Go To Dashboard
    # On ouvre le menu
    Go To Submenu In Menu    suivi    piece_bannette
    # On vérifie le fil d'Ariane
    Page Title Should Be    Suivi > Documents Entrants > Bannette
    # On clique sur l'action ajouter du tableau
    Click On Add Button
    # On saisit les informations
    Input Text    css=#nom    Lettre de test
    Input Datepicker    date_reception    01/01/2036
    Input Datepicker    date_emission    01/01/2036
    Add File    uid    lettre_test.pdf
    # On valide le formulaire
    Click On Submit Button
    # On vérifie le message affiché à l'utilisateur
    Valid Message Should Be    Vos modifications ont bien été enregistrées.
    # On clique sur le bouton retour
    Click On Back Button


    # On clique sur le tableau de bord
    Go To Dashboard
    # On ouvre le menu
    Go To Submenu In Menu    suivi    piece_bannette
    # On vérifie le fil d'Ariane
    Page Title Should Be    Suivi > Documents Entrants > Bannette
    # On recherche l'enregistrement
    Use Simple Search    nom    Lettre de test
    # On clique sur le résultat
    Click On Link    Lettre de test
    # On vérifie le statut du document
    Form Static Value Should Be    css=#piece_statut    En cours
    # On vérifie les actions
    Portlet Action Should Not Be In Form    piece_bannette    lu
    Portlet Action Should Not Be In Form    piece_bannette    suivi
    Portlet Action Should Not Be In Form    piece_bannette    non lu
    Portlet Action Should Not Be In Form    piece_bannette    non_suivi
    Portlet Action Should Not Be In Form    piece_bannette    valider
    Portlet Action Should Not Be In Form    piece_bannette    annuler

    # On modifie le document
    Click On Form Portlet Action     piece_bannette    modifier
    # On sélectionne un lien
    Select From List By Label    css=#choix_lien    établissement
    # On recherche un établissement grâce au champ "autocomplete"
    WUX  Input Text    css=#autocomplete-etablissement_tous-search    ${etab01_titre}
    # On lie cet établissement
    WUX  Click Link    ${etab01_titre}
    # On sélectionne le service
    #Select From List By Label    css=#service    Accessibilité
    # On valide le formulaire
    Click On Submit Button
    # On vérifie le message affiché à l'utilisateur
    Valid Message Should Be    Vos modifications ont bien été enregistrées.
    # On clique sur le bouton retourj
    Click On Back Button

    #
    Depuis la page d'accueil    cadre-acc    cadre-acc
    # On ouvre le menu
    Go To Submenu In Menu    suivi    piece_a_valider
    # On vérifie le fil d'Ariane
    Page Title Should Be    Suivi > Documents Entrants > À Valider
    # On recherche l'enregistrement
    Use Simple Search    nom    Lettre de test
    # On clique sur le résultat
    Click On Link    Lettre de test
    # On vérifie les nouvelles données
    Form Static Value Should Be    css=#link_etablissement    ${etab01_titre}
    Form Static Value Should Be    css=#piece_statut    Qualifié
    # On vérifie les actions
    Portlet Action Should Not Be In Form    piece_a_valider    lu
    Portlet Action Should Not Be In Form    piece_a_valider    suivi
    Portlet Action Should Be In Form    piece_a_valider    valide
    Portlet Action Should Not Be In Form    piece_a_valider    non lu
    Portlet Action Should Not Be In Form    piece_a_valider    non_suivi
    Portlet Action Should Not Be In Form    piece_a_valider    annule

    # On clique sur l'action valider
    Click On Form Portlet Action     piece_a_valider    valide
    # On vérifie le message affiché à l'utilisateur
    WUX  Valid Message Should Be     Le document entrant a été validé.
    # On vérifie les actions
    Portlet Action Should Not Be In Form    piece_a_valider    valide
    # On vérifie les données
    Form Static Value Should Be    css=#piece_statut    Validé
    # On vérifie que le lien est vérouillé
    Click On Form Portlet Action     piece_a_valider    modifier
    Page Should Not Contain Element    css#choix_lien


Visualisation d'un document entrant par un technicien
    #
    Depuis la page d'accueil    technicien-acc    technicien-acc
    #
    Depuis l'onglet document entrant de l'établissement    ${etab01_code}
    #
    Click On Link    Lettre de test
    Form Static Value Should Be    css=#nom    Lettre de test


Actions 'Marquer comme lu' / 'Marquer comme non lu'
    [Documentation]  Un document entrant possède un marqueur Lu / Non lu
    ...  permettant de le faire apparaître / de la masquer dans le widget
    ...  'Mes documents entrants non lus'.
    ...
    ...  Le marqueur de lecture n'a pas de sens si le document entrant n'est
    ...  pas rattaché à un dossier d'instruction et donc à un instructeur qui
    ...  peut agir sur ce marqueur de lecture.
    ...
    ...  Donc si le document n'est pas lié à un DI, le marqueur de lecture est
    ...  positionné à true (donc lu) et personne ne peut agir dessus. Si ce
    ...  document se retrouve lié à un DI alors le marqueur de lecture passe à
    ...  false (non lu) et l'admin, le cadrte et le technicien du DI peuvent
    ...  agir dessus.

    Depuis la page d'accueil  admin  admin
    # Document entrant docent_l_01 rattaché au DC dc01
    &{docent_l_01} =  Create Dictionary
    ...  nom=testouner
    ...  service=Sécurité Incendie
    ...  choix_lien=dossier de coordination
    ...  dossier_coordination=${dc01_libelle}
    ${docent_l_01.id} =  Ajouter un document entrant depuis la bannette  ${docent_l_01}
    # Puisque pas lié à un DI, le marqueur lu / non lu est caché sur le document entrant
    # sa valeur est à true
    Depuis le contexte d'une document entrant à valider  ${docent_l_01.nom}
    Form Field Attribute Should Be  lu  type  hidden
    ${field_lu_value} =  Get Value  css=#lu
    Should Be Equal As Strings  ${field_lu_value}  t
    Portlet Action Should Not Be In Form  piece_a_valider  lu
    Portlet Action Should Not Be In Form  piece_a_valider  non_lu
    # On accède au formulaire de modification du document entrant pour
    # vérifier que le champ lu n'est pas modifiable.
    Click On Form Portlet Action  piece_a_valider  modifier
    Form Field Attribute Should Be  lu  type  hidden
    ${field_lu_value} =  Get Value  css=#lu
    Should Be Equal As Strings  ${field_lu_value}  t
    # On valide le formulaire de modification du document entrant pour
    # vérifier que la valeur du marqueur lu est conservée à l'identique.
    Click On Submit Button
    Valid Message Should Be  Vos modifications ont bien été enregistrées.
    Form Field Attribute Should Be  lu  type  hidden
    ${field_lu_value} =  Get Value  css=#lu
    Should Be Equal As Strings  ${field_lu_value}  t
    Portlet Action Should Not Be In Form  piece_a_valider  lu
    Portlet Action Should Not Be In Form  piece_a_valider  non_lu

    # Le document est maintenant rattaché au DI SI du DC01
    &{docent_l_01_modif} =  Create Dictionary
    ...  choix_lien=dossier d'instruction
    ...  dossier_instruction=${dc01_di_si}
    Modifier le document entrant (accès par URL)  ${docent_l_01.id}  ${docent_l_01_modif}
    #
    Depuis le contexte d'une document entrant à valider  ${docent_l_01.nom}
    Element Text Should Be  css=span#lu  Non
    Portlet Action Should Be In Form  piece_a_valider  lu
    Portlet Action Should Not Be In Form  piece_a_valider  non_lu
    # On valide le formulaire de modification du document entrant pour
    # vérifier que la valeur du marqueur lu est conservée à l'identique.
    Click On Form Portlet Action  piece_a_valider  modifier
    Element Text Should Be  css=.field-type-checkboxhiddenstatic span#lu  Non
    Click On Submit Button
    Valid Message Should Be  Vos modifications ont bien été enregistrées.
    Element Text Should Be  css=span#lu  Non
    Portlet Action Should Be In Form  piece_a_valider  lu
    Portlet Action Should Not Be In Form  piece_a_valider  non_lu
    # On utilise l'action directe pour marquer le document entrant comme lu
    Click On Form Portlet Action  piece_a_valider  lu
    Valid Message Should Be  Le document entrant a été marqué comme lu.
    Element Text Should Be  css=span#lu  Oui
    Portlet Action Should Not Be In Form  piece_a_valider  lu
    Portlet Action Should Be In Form  piece_a_valider  non_lu
    # On accède au formulaire de modification du document entrant pour
    # vérifier que le champ lu n'est pas modifiable.
    Click On Form Portlet Action  piece_a_valider  modifier
    Element Text Should Be  css=.field-type-checkboxhiddenstatic span#lu  Oui
    # On valide le formulaire de modification du document entrant pour
    # vérifier que la valeur du marqueur lu est conservée à l'identique
    Click On Submit Button
    Valid Message Should Be  Vos modifications ont bien été enregistrées.
    Element Text Should Be  css=span#lu  Oui
    Portlet Action Should Not Be In Form  piece_a_valider  lu
    Portlet Action Should Be In Form  piece_a_valider  non_lu
    # On utilise l'action directe pour marquer le document entrant comme non lu
    Click On Form Portlet Action  piece_a_valider  non_lu
    Valid Message Should Be  Le document entrant a été marqué comme non lu.
    Element Text Should Be  css=span#lu  Non


Gestion des métadonnées

    [Documentation]  Vérifie les métadonnées pour le champ fichier :
    ...
    ...  - Document entrant numérisé [piece.uid]
    ...    > Téléversé
    ...    > Stockage à l'ajout du fichier
    ...    > Mise à jour à chaque mise à jour du fichier
    ...    > Mise à jour XXX

    # En tant que profil ADMINISTRATEUR
    Depuis la page d'accueil  admin  admin

    ##
    ## DOCUMENT ENTRANT 01
    ##
    ## Ce premier document entrant n'est lié à rien : ni établissement, ni DC, ni DI.
    ##
    &{docent01} =  Create Dictionary
    ...  nom=Document entrant MD 1
    ...  piece_type=Signalement
    ...  uid=lettre_test.pdf
    ...  date_reception=01/03/2015
    ...  date_emission=02/03/2015
    ...  service=Sécurité Incendie
    ...  suivi=true
    ...  date_butoir=05/03/2015
    ...  commentaire_suivi=blablabla
    ${docent01_id} =  Ajouter un document entrant depuis la bannette  ${docent01}

    ## => [piece.uid] (METADATA FILESTORAGE)
    # Récupération des éléments concernant les fichiers (uid + chemins) pour
    # effectuer les vérifications d'existence des fichiers et des métadonnées.
    ${docent01_fichier_uid} =  Récupérer l'uid du fichier du document entrant  ${docent01_id}
    ${docent01_fichier_path} =  Récupérer le chemin vers le fichier correspondant à l'uid  ${docent01_fichier_uid}
    Le fichier doit exister  ${docent01_fichier_path}
    ${docent01_fichier_md_path} =  Récupérer le chemin vers le fichier de métadonnées correspondant à l'uid  ${docent01_fichier_uid}
    Le fichier doit exister  ${docent01_fichier_md_path}
    # Composition du dictionnaire représentant les métadonnées qui doivent
    # apparaître dans le fichier de métadonnées.
    ${size} =  Get File Size  ${docent01_fichier_path}
    ${md} =  Create Dictionary
    ...  filename=${docent01.uid}
    ...  mimetype=application/pdf
    ...  size=${size}
    ...  titre=${docent01.piece_type}
    ...  description=document entrant numérisé
    ...  application=openARIA
    ...  origine=téléversé
    ...  etablissement_code=
    ...  etablissement_libelle=
    ...  etablissement_siret=
    ...  etablissement_referentiel=
    ...  etablissement_exploitant=
    ...  etablissement_adresse_numero=
    ...  etablissement_adresse_mention=
    ...  etablissement_adresse_voie=
    ...  etablissement_adresse_cp=
    ...  etablissement_adresse_ville=
    ...  etablissement_adresse_arrondissement=
    ...  etablissement_ref_patrimoine=
    ...  dossier_coordination=
    ...  dossier_instruction=
    Les métadonnées (clé/valeur) doivent être présentes dans le fichier  ${md}  ${docent01_fichier_md_path}
    Les métadonnées (clé) ne doivent pas être présentes dans le fichier  ${md_no}  ${docent01_fichier_md_path}
    # Modidication du document entrant pour vérifier la mise à jour des métadonnées
    &{docent01a} =  Create Dictionary
    ...  choix_lien=établissement
    ...  etablissement=${etab02_code} - ${etab02.libelle}
    Modifier le document entrant (accès par URL)  ${docent01_id}  ${docent01a}
    ${md} =  Create Dictionary
    ...  filename=${docent01.uid}
    ...  mimetype=application/pdf
    ...  size=${size}
    ...  titre=Établissement ${etab02_code} - ${docent01.piece_type}
    ...  description=document entrant numérisé
    ...  application=openARIA
    ...  origine=téléversé
    ...  etablissement_code=${etab02_code}
    ...  etablissement_libelle=${etab02.libelle}
    ...  etablissement_siret=${etab02.siret}
    ...  etablissement_referentiel=false
    ...  etablissement_exploitant=${etab02.exp_prenom} ${etab02.exp_nom}
    ...  etablissement_adresse_numero=${etab02.adresse_numero}
    ...  etablissement_adresse_mention=${etab02.adresse_numero2}
    ...  etablissement_adresse_voie=${etab02.adresse_voie}
    ...  etablissement_adresse_cp=${etab02.adresse_cp}
    ...  etablissement_adresse_ville=${etab02.adresse_ville}
    ...  etablissement_adresse_arrondissement=${etab02.adresse_arrondissement}
    ...  etablissement_ref_patrimoine=120;90
    ...  dossier_coordination=
    ...  dossier_instruction=
    Le fichier doit exister  ${docent01_fichier_path}
    Le fichier doit exister  ${docent01_fichier_md_path}
    Les métadonnées (clé/valeur) doivent être présentes dans le fichier  ${md}  ${docent01_fichier_md_path}
    Les métadonnées (clé) ne doivent pas être présentes dans le fichier  ${md_no}  ${docent01_fichier_md_path}

    ##
    ## DOCUMENT ENTRANT 02
    ##
    ## Ce document entrant est lié à un établissement donc à aucun DC ni DI.
    ##
    &{docent02} =  Create Dictionary
    ...  nom=Document entrant MD 2
    ...  piece_type=Signalement
    ...  uid=lettre_test.pdf
    ...  date_reception=01/03/2015
    ...  date_emission=02/03/2015
    ...  service=Sécurité Incendie
    ...  choix_lien=établissement
    ...  etablissement=${etab02_code} - ${etab02.libelle}
    ...  suivi=true
    ...  date_butoir=05/03/2015
    ...  commentaire_suivi=blablabla
    ${docent02_id} =  Ajouter un document entrant depuis la bannette  ${docent02}

    ## => [piece.uid] (METADATA FILESTORAGE)
    # Récupération des éléments concernant les fichiers (uid + chemins) pour
    # effectuer les vérifications d'existence des fichiers et des métadonnées.
    ${docent02_fichier_uid} =  Récupérer l'uid du fichier du document entrant  ${docent02_id}
    ${docent02_fichier_path} =  Récupérer le chemin vers le fichier correspondant à l'uid  ${docent02_fichier_uid}
    Le fichier doit exister  ${docent02_fichier_path}
    ${docent02_fichier_md_path} =  Récupérer le chemin vers le fichier de métadonnées correspondant à l'uid  ${docent02_fichier_uid}
    Le fichier doit exister  ${docent02_fichier_md_path}
    # Composition du dictionnaire représentant les métadonnées qui doivent
    # apparaître dans le fichier de métadonnées.
    ${size}=  Get File Size  ${docent02_fichier_path}
    ${md} =  Create Dictionary
    ...  filename=${docent02.uid}
    ...  mimetype=application/pdf
    ...  size=${size}
    ...  titre=Établissement ${etab02_code} - ${docent01.piece_type}
    ...  description=document entrant numérisé
    ...  application=openARIA
    ...  origine=téléversé
    ...  etablissement_code=${etab02_code}
    ...  etablissement_libelle=${etab02.libelle}
    ...  etablissement_siret=${etab02.siret}
    ...  etablissement_referentiel=false
    ...  etablissement_exploitant=${etab02.exp_prenom} ${etab02.exp_nom}
    ...  etablissement_adresse_numero=${etab02.adresse_numero}
    ...  etablissement_adresse_mention=${etab02.adresse_numero2}
    ...  etablissement_adresse_voie=${etab02.adresse_voie}
    ...  etablissement_adresse_cp=${etab02.adresse_cp}
    ...  etablissement_adresse_ville=${etab02.adresse_ville}
    ...  etablissement_adresse_arrondissement=${etab02.adresse_arrondissement}
    ...  etablissement_ref_patrimoine=120;90
    ...  dossier_coordination=
    ...  dossier_instruction=
    Les métadonnées (clé/valeur) doivent être présentes dans le fichier  ${md}  ${docent02_fichier_md_path}
    Les métadonnées (clé) ne doivent pas être présentes dans le fichier  ${md_no}  ${docent02_fichier_md_path}

    ##
    ## DOCUMENT ENTRANT 03
    ##
    ## Ce document entrant est lié à un DC donc à un établissement.
    ##
    &{docent03} =  Create Dictionary
    ...  nom=Document entrant MD 3
    ...  piece_type=Signalement
    ...  uid=lettre_test.pdf
    ...  date_reception=01/03/2015
    ...  date_emission=02/03/2015
    ...  service=Sécurité Incendie
    ...  choix_lien=dossier de coordination
    ...  dossier_coordination=${dc01_libelle}
    ...  suivi=true
    ...  date_butoir=05/03/2015
    ...  commentaire_suivi=blablabla
    ${docent03_id} =  Ajouter un document entrant depuis la bannette  ${docent03}

    ## => [piece.uid] (METADATA FILESTORAGE)
    # Récupération des éléments concernant les fichiers (uid + chemins) pour
    # effectuer les vérifications d'existence des fichiers et des métadonnées.
    ${docent03_fichier_uid} =  Récupérer l'uid du fichier du document entrant  ${docent03_id}
    ${docent03_fichier_path} =  Récupérer le chemin vers le fichier correspondant à l'uid  ${docent03_fichier_uid}
    Le fichier doit exister  ${docent03_fichier_path}
    ${docent03_fichier_md_path} =  Récupérer le chemin vers le fichier de métadonnées correspondant à l'uid  ${docent03_fichier_uid}
    Le fichier doit exister  ${docent03_fichier_md_path}
    # Composition du dictionnaire représentant les métadonnées qui doivent
    # apparaître dans le fichier de métadonnées.
    ${size}=  Get File Size  ${docent03_fichier_path}
    ${md} =  Create Dictionary
    ...  filename=${docent03.uid}
    ...  mimetype=application/pdf
    ...  size=${size}
    ...  titre=Établissement ${etab02_code} - Dossier ${dc01_libelle} - ${docent01.piece_type}
    ...  description=document entrant numérisé
    ...  application=openARIA
    ...  origine=téléversé
    ...  etablissement_code=${etab02_code}
    ...  etablissement_libelle=${etab02.libelle}
    ...  etablissement_siret=${etab02.siret}
    ...  etablissement_referentiel=false
    ...  etablissement_exploitant=${etab02.exp_prenom} ${etab02.exp_nom}
    ...  etablissement_adresse_numero=${etab02.adresse_numero}
    ...  etablissement_adresse_mention=${etab02.adresse_numero2}
    ...  etablissement_adresse_voie=${etab02.adresse_voie}
    ...  etablissement_adresse_cp=${etab02.adresse_cp}
    ...  etablissement_adresse_ville=${etab02.adresse_ville}
    ...  etablissement_adresse_arrondissement=${etab02.adresse_arrondissement}
    ...  etablissement_ref_patrimoine=120;90
    ...  dossier_coordination=${dc01_libelle}
    ...  dossier_instruction=
    Les métadonnées (clé/valeur) doivent être présentes dans le fichier  ${md}  ${docent03_fichier_md_path}
    Les métadonnées (clé) ne doivent pas être présentes dans le fichier  ${md_no}  ${docent03_fichier_md_path}

    ##
    ## DOCUMENT ENTRANT 04
    ##
    ## Ce document entrant est lié à un DI donc à un DC et à un établissement.
    ##
    &{docent04} =  Create Dictionary
    ...  nom=Document entrant MD 4
    ...  piece_type=Signalement
    ...  uid=lettre_test.pdf
    ...  date_reception=01/03/2015
    ...  date_emission=02/03/2015
    ...  choix_lien=dossier d'instruction
    ...  dossier_instruction=${dc01_di_si}
    ...  suivi=true
    ...  date_butoir=05/03/2015
    ...  commentaire_suivi=blablabla
    ${docent04_id} =  Ajouter un document entrant depuis la bannette  ${docent04}

    ## => [piece.uid] (METADATA FILESTORAGE)
    # Récupération des éléments concernant les fichiers (uid + chemins) pour
    # effectuer les vérifications d'existence des fichiers et des métadonnées.
    ${docent04_fichier_uid} =  Récupérer l'uid du fichier du document entrant  ${docent04_id}
    ${docent04_fichier_path} =  Récupérer le chemin vers le fichier correspondant à l'uid  ${docent04_fichier_uid}
    Le fichier doit exister  ${docent04_fichier_path}
    ${docent04_fichier_md_path} =  Récupérer le chemin vers le fichier de métadonnées correspondant à l'uid  ${docent04_fichier_uid}
    Le fichier doit exister  ${docent04_fichier_md_path}
    # Composition du dictionnaire représentant les métadonnées qui doivent
    # apparaître dans le fichier de métadonnées.
    ${size}=  Get File Size  ${docent04_fichier_path}
    ${md} =  Create Dictionary
    ...  filename=${docent04.uid}
    ...  mimetype=application/pdf
    ...  size=${size}
    ...  titre=Établissement ${etab02_code} - Dossier ${dc01_di_si} - ${docent01.piece_type}
    ...  description=document entrant numérisé
    ...  application=openARIA
    ...  origine=téléversé
    ...  etablissement_code=${etab02_code}
    ...  etablissement_libelle=${etab02.libelle}
    ...  etablissement_siret=${etab02.siret}
    ...  etablissement_referentiel=false
    ...  etablissement_exploitant=${etab02.exp_prenom} ${etab02.exp_nom}
    ...  etablissement_adresse_numero=${etab02.adresse_numero}
    ...  etablissement_adresse_mention=${etab02.adresse_numero2}
    ...  etablissement_adresse_voie=${etab02.adresse_voie}
    ...  etablissement_adresse_cp=${etab02.adresse_cp}
    ...  etablissement_adresse_ville=${etab02.adresse_ville}
    ...  etablissement_adresse_arrondissement=${etab02.adresse_arrondissement}
    ...  etablissement_ref_patrimoine=120;90
    ...  dossier_coordination=${dc01_libelle}
    ...  dossier_instruction=${dc01_di_si}
    Les métadonnées (clé/valeur) doivent être présentes dans le fichier  ${md}  ${docent04_fichier_md_path}
    Les métadonnées (clé) ne doivent pas être présentes dans le fichier  ${md_no}  ${docent04_fichier_md_path}


