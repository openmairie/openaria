*** Settings ***
Resource  resources/resources.robot
Suite Setup  For Suite Setup
Suite Teardown  For Suite Teardown
Documentation  Les réunions...


*** Test Cases ***
Constitution d'un jeu de données

    [Documentation]  L'objet de ce 'Test Case' est de constituer un jeu de
    ...  données cohérent pour les scénarios fonctionnels qui suivent.

    # En tant que profil ADMINISTRATEUR
    Depuis la page d'accueil  admin  admin

    # On définit une liste de clés pour lesquelles on souhaite vérifier
    # l'absence dans le fichier de métadonnées. Ce sont les métadonnées
    # spécifiques aux arrêtés, aux PV, aux signataires, aux établissements,
    # aux DC et aux DI.
    @{md_no} =  Create List
    ...  pv_erp_numero
    ...  pv_erp_nature_analyse
    ...  pv_erp_reference_urbanisme
    ...  pv_erp_avis_rendu
    ...  signataire
    ...  signataire_qualite
    ...  date_signature
    ...  arrete_numero
    ...  arrete_reglementaire
    ...  arrete_notification
    ...  arrete_date_notification
    ...  arrete_publication
    ...  arrete_date_publication
    ...  arrete_temporaire
    ...  arrete_expiration
    ...  arrete_date_controle_legalite
    ...  arrete_nature_acte
    ...  arrete_nature_acte_niv1
    ...  arrete_nature_acte_niv2
    ...  etablissement_code
    ...  etablissement_libelle
    ...  etablissement_siret
    ...  etablissement_referentiel
    ...  etablissement_exploitant
    ...  etablissement_adresse_numero
    ...  etablissement_adresse_mention
    ...  etablissement_adresse_voie
    ...  etablissement_adresse_cp
    ...  etablissement_adresse_ville
    ...  etablissement_adresse_arrondissement
    ...  etablissement_ref_patrimoine
    ...  dossier_coordination
    ...  dossier_instruction
    Set Suite Variable  ${md_no}

    ##
    ## INSTANCE DE REUNION 01
    ##
    &{reuinstance01} =  Create Dictionary
    ...  code=RETINA France
    ...  libelle=Association RETINA France
    ...  service=Accessibilité
    Ajouter l'instance de réunion  ${reuinstance01}

    ##
    ## INSTANCE DE REUNION 02
    ##
    &{reuinstance02} =  Create Dictionary
    ...  code=D.D.C.S. 13
    ...  libelle=Direction départementale de la cohésion sociale (DDCS) des Bouches-du-Rhône
    ...  service=Accessibilité
    Ajouter l'instance de réunion  ${reuinstance02}

    ##
    ## INSTANCE DE REUNION 03
    ##
    &{reuinstance03} =  Create Dictionary
    ...  code=A.P.F.
    ...  libelle=Association des Paralysés de France
    ...  service=Accessibilité
    Ajouter l'instance de réunion  ${reuinstance03}

    ##
    ## INSTANCE DE REUNION 04
    ##
    &{reuinstance04} =  Create Dictionary
    ...  code=D.D.T.M. 13
    ...  libelle=Direction départementale des territoires et de la mer (DDTM) des Bouches-du-Rhône
    ...  service=Accessibilité
    Ajouter l'instance de réunion  ${reuinstance04}

    ##
    ## INSTANCE DE REUNION 05
    ##
    &{reuinstance05} =  Create Dictionary
    ...  code=BMP
    ...  libelle=Bataillon des Marins Pompier
    ...  service=Sécurité Incendie
    Ajouter l'instance de réunion  ${reuinstance05}

    ##
    ## INSTANCE DE REUNION 06
    ##
    &{reuinstance06} =  Create Dictionary
    ...  code=PN
    ...  libelle=Police Nationale
    ...  service=Sécurité Incendie
    Ajouter l'instance de réunion  ${reuinstance06}

    #
    Ajouter un membre depuis le contexte de l'instance  ${reuinstance01.code}  M. DURAND
    Ajouter un membre depuis le contexte de l'instance  ${reuinstance01.code}  M. DUPONT
    Ajouter un membre depuis le contexte de l'instance  ${reuinstance04.code}  Mme MICHEL
    Ajouter un membre depuis le contexte de l'instance  ${reuinstance05.code}  M. GERARD
    Ajouter un membre depuis le contexte de l'instance  ${reuinstance05.code}  M. JACQUES

    ##
    ## CATEGORIE DE REUNION 01
    ##
    &{reucategorie01} =  Create Dictionary
    ...  code=plans
    ...  libelle=Plans
    ...  description=Plans
    ...  service=Accessibilité
    ...  ordre=10
    Ajouter la catégorie de réunion  ${reucategorie01}

    ##
    ## CATEGORIE DE REUNION 02
    ##
    &{reucategorie02} =  Create Dictionary
    ...  code=plans
    ...  libelle=Plans
    ...  description=Plans
    ...  service=Sécurité Incendie
    ...  ordre=10
    Ajouter la catégorie de réunion  ${reucategorie02}

    ##
    ## CATEGORIE DE REUNION 03
    ##
    &{reucategorie03} =  Create Dictionary
    ...  code=visites
    ...  libelle=Visites
    ...  description=Visites
    ...  service=Sécurité Incendie
    ...  ordre=20
    Ajouter la catégorie de réunion  ${reucategorie03}

    ##
    ## CATEGORIE DE REUNION 04
    ##
    &{reucategorie04} =  Create Dictionary
    ...  code=enjeux
    ...  libelle=Dossiers à enjeux
    ...  description=Dossiers à enjeux
    ...  service=Sécurité Incendie
    ...  ordre=30
    Ajouter la catégorie de réunion  ${reucategorie04}

    ##
    ## AVIS DE REUNION 01
    ##
    &{reuavis01} =  Create Dictionary
    ...  code=TEST
    ...  libelle=AVIS TEST 1
    ...  description=Description
    ...  service=Sécurité Incendie
    Ajouter l'avis de réunion  ${reuavis01}
    Set Suite Variable  ${reuavis01}

    ##
    ## AVIS DE REUNION 02
    ##
    &{reuavis02} =  Create Dictionary
    ...  code=TEST
    ...  libelle=AVIS TEST 2
    ...  description=Description
    ...  service=Accessibilité
    Ajouter l'avis de réunion  ${reuavis02}
    Set Suite Variable  ${reuavis02}

    ##
    ## TYPE DE REUNION 01
    ##
    @{categories_autorisees}  Create List  ${reucategorie02.libelle}  ${reucategorie03.libelle}  ${reucategorie04.libelle}
    @{avis_autorises}  Create List  FAVORABLE  DEFAVORABLE  DIFFERE  SANS AVIS  A REVOIR
    @{instances_autorisees}  Create List  ${reuinstance05.libelle}  ${reuinstance06.libelle}
    &{reutype01} =  Create Dictionary
    ...  code=CCS-PLEN
    ...  libelle=CCS Plénière
    ...  service=Sécurité Incendie
    ...  categories_autorisees=@{categories_autorisees}
    ...  avis_autorises=@{avis_autorises}
    ...  instances_autorisees=@{instances_autorisees}
    ...  lieu_salle=DGUP
    ...  lieu_adresse_ligne1=30 AV SALENGRO
    ...  lieu_adresse_ligne2=13003 MARSEILLE
    ...  heure=12:00
    ...  listes_de_diffusion=nospam@openmairie.org
    ...  modele_courriel_convoquer=Bonjour,\n\nVeuillez trouver ci joint les informations concernant la réunion citée en objet :\n - Date : [date]\n - Heure : [heure]\n\nCordialement.
    ...  modele_ordre_du_jour=Réunion - Ordre du jour CCS
    ...  modele_compte_rendu_global=Réunion - Compte-rendu général CCS
    ...  modele_compte_rendu_specifique=Réunion - Compte rendu d'avis
    ...  modele_feuille_presence=Réunion - Feuille de présence
    Ajouter le type de réunion  ${reutype01}
    Set Suite Variable  ${reutype01}

    ##
    ## TYPE DE REUNION 02
    ##
    @{categories_autorisees}  Create List  ${reucategorie02.libelle}
    @{avis_autorises}  Create List  FAVORABLE  DEFAVORABLE  DIFFERE  SANS AVIS  A REVOIR
    @{instances_autorisees}  Create List  ${reuinstance05.libelle}  ${reuinstance06.libelle}
    &{reutype02} =  Create Dictionary
    ...  code=CCS-GTEP-A
    ...  libelle=CCS Groupe technique d'étude de plans
    ...  service=Sécurité Incendie
    ...  categories_autorisees=@{categories_autorisees}
    ...  avis_autorises=@{avis_autorises}
    ...  instances_autorisees=@{instances_autorisees}
    Ajouter le type de réunion  ${reutype02}
    Set Suite Variable  ${reutype02}

    ##
    ## TYPE DE REUNION 03
    ##
    @{categories_autorisees}  Create List  ${reucategorie01.libelle}
    @{avis_autorises}  Create List  FAVORABLE  DEFAVORABLE  AJOURNE  SANS AVIS
    @{instances_autorisees}  Create List  ${reuinstance01.libelle}  ${reuinstance02.libelle}  ${reuinstance04.libelle}
    &{reutype03} =  Create Dictionary
    ...  code=CCA-PLEN
    ...  libelle=CCA Plénière
    ...  service=Accessibilité
    ...  categories_autorisees=@{categories_autorisees}
    ...  avis_autorises=@{avis_autorises}
    ...  instances_autorisees=@{instances_autorisees}
    Ajouter le type de réunion  ${reutype03}
    Set Suite Variable  ${reutype03}

    ##
    ## REUNION 01
    ##
    &{reunion01} =  Create Dictionary
    ...  reunion_type=${reutype01.libelle}
    ...  date_reunion=28/11/2014
    ...  date_reunion_yyyy_mm_dd=2014-11-28
    Ajouter la réunion  ${reunion01}
    ${reunion01_code} =  Set Variable  ${reutype01.code}-${reunion01.date_reunion_yyyy_mm_dd}
    Set Suite Variable  ${reunion01}
    Set Suite Variable  ${reunion01_code}

    ##
    ## REUNION 02
    ##
    &{reunion02} =  Create Dictionary
    ...  reunion_type=${reutype02.libelle}
    ...  date_reunion=01/12/2014
    ...  date_reunion_yyyy_mm_dd=2014-12-01
    Ajouter la réunion  ${reunion02}
    ${reunion02_code} =  Set Variable  ${reutype02.code}-${reunion02.date_reunion_yyyy_mm_dd}
    Set Suite Variable  ${reunion02}
    Set Suite Variable  ${reunion02_code}

    ##
    ## REUNION 03
    ##
    &{reunion03} =  Create Dictionary
    ...  reunion_type=${reutype03.libelle}
    ...  date_reunion=03/12/2014
    ...  date_reunion_yyyy_mm_dd=2014-12-03
    Ajouter la réunion  ${reunion03}
    ${reunion03_code} =  Set Variable  ${reutype03.code}-${reunion03.date_reunion_yyyy_mm_dd}
    Set Suite Variable  ${reunion03}
    Set Suite Variable  ${reunion03_code}

    ##
    ## ETABLISSEMENT 01
    ##
    &{etab01} =  Create Dictionary
    ...  libelle=BRASSERIE DES ARTS
    ...  etablissement_nature=ERP Référentiel
    ...  etablissement_type=N
    ...  etablissement_categorie=1
    ...  si_autorite_competente_visite=Commission communale de sécurité
    ...  adresse_numero=23
    ...  adresse_numero2=bis
    ...  adresse_complement=Bâtiment E
    ...  lieu_dit=Préf
    ...  boite_postale=12
    ...  adresse_voie=RUE DE ROME
    ...  adresse_cp=13006
    ...  adresse_ville=MARSEILLE
    ...  adresse_arrondissement=6ème
    ...  cedex=322
    ...  siret=73282932000074
    ...  exp_civilite=Mme
    ...  exp_nom=Maillard
    ...  exp_prenom=Eva
    ...  exp_adresse_numero=3
    ...  exp_adresse_numero2=A
    ...  exp_adresse_complement=Bâtiment B
    ...  exp_lieu_dit=Square
    ...  exp_boite_postale=430
    ...  exp_adresse_voie=RUE BELLE
    ...  exp_adresse_cp=13009
    ...  exp_adresse_ville=MARSEILLE
    ...  exp_adresse_arrondissement=9ème
    ...  exp_cedex=108
    ${etab01_code} =  Ajouter l'établissement  ${etab01}
    ${etab01_titre} =  Set Variable  ${etab01_code} - ${etab01.libelle}
    Set Suite Variable  ${etab01}
    Set Suite Variable  ${etab01_code}
    Set Suite Variable  ${etab01_titre}

    ##
    ## ETABLISSEMENT 02
    ##
    &{etab02} =  Create Dictionary
    ...  libelle=MONOPRIX
    ...  etablissement_nature=ERP Référentiel
    ...  etablissement_type=M
    ...  etablissement_categorie=1
    ...  si_autorite_competente_visite=Commission communale de sécurité
    ...  adresse_numero=25
    ...  adresse_voie=RUE DE ROME
    ...  adresse_cp=13006
    ...  adresse_ville=MARSEILLE
    ...  adresse_arrondissement=6ème
    ...  siret=37822443000017
    ...  exp_civilite=M.
    ...  exp_nom=Fontaine
    ...  exp_prenom=Aaron
    ${etab02_code} =  Ajouter l'établissement  ${etab02}
    ${etab02_titre} =  Set Variable  ${etab02_code} - ${etab02.libelle}
    Set Suite Variable  ${etab02}
    Set Suite Variable  ${etab02_code}
    Set Suite Variable  ${etab02_titre}

    ##
    ## ETABLISSEMENT 03
    ##
    &{etab03} =  Create Dictionary
    ...  libelle=MAGASIN SPAR
    ...  etablissement_nature=ERP Référentiel
    ...  etablissement_type=M
    ...  etablissement_categorie=1
    ...  si_autorite_competente_visite=Commission communale de sécurité
    ...  adresse_numero=27
    ...  adresse_voie=RUE DE ROME
    ...  adresse_cp=13006
    ...  adresse_ville=MARSEILLE
    ...  adresse_arrondissement=6ème
    ...  exp_civilite=M.
    ...  exp_nom=Noel
    ...  exp_prenom=Adrien
    ${etab03_code} =  Ajouter l'établissement  ${etab03}
    ${etab03_titre} =  Set Variable  ${etab03_code} - ${etab03.libelle}
    Set Suite Variable  ${etab03}
    Set Suite Variable  ${etab03_code}
    Set Suite Variable  ${etab03_titre}

    ##
    ## ETABLISSEMENT 04
    ##
    &{etab04} =  Create Dictionary
    ...  libelle=CARREFOUR CITY
    ...  etablissement_nature=ERP Référentiel
    ...  etablissement_type=R
    ...  etablissement_categorie=1
    ...  si_autorite_competente_visite=Commission communale de sécurité
    ...  adresse_numero=29
    ...  adresse_voie=RUE DE ROME
    ...  adresse_cp=13006
    ...  adresse_ville=MARSEILLE
    ...  adresse_arrondissement=6ème
    ...  siret=null
    ...  exp_civilite=M.
    ...  exp_nom=Brun
    ...  exp_prenom=Kyllian
    ...  etablissement_etat=Ouvert
    ${etab04_code} =  Ajouter l'établissement  ${etab04}
    ${etab04_titre} =  Set Variable  ${etab04_code} - ${etab04.libelle}
    Set Suite Variable  ${etab04}
    Set Suite Variable  ${etab04_code}
    Set Suite Variable  ${etab04_titre}

    ##
    ## DOSSIER DE COORDINATION 01
    ##
    &{dc01} =  Create Dictionary
    ...  dossier_coordination_type=Autorisation de Travaux
    ...  description=Description
    ...  etablissement=${etab01_titre}
    ...  date_demande=09/11/2014
    ...  a_qualifier=false
    ...  dossier_instruction_secu=true
    ...  dossier_instruction_acc=true
    ${dc01_libelle} =  Ajouter le dossier de coordination  ${dc01}
    Set Suite Variable    ${dc01}
    Set Suite Variable    ${dc01_libelle}

    ##
    ## DOSSIER DE COORDINATION 02
    ##
    &{dc02} =  Create Dictionary
    ...  dossier_coordination_type=Permis de construire
    ...  description=Nouveau
    ...  etablissement=${etab02_titre}
    ...  date_demande=10/11/2014
    ...  a_qualifier=false
    ...  dossier_instruction_secu=true
    ...  dossier_instruction_acc=true
    ${dc02_libelle} =  Ajouter le dossier de coordination  ${dc02}
    Set Suite Variable  ${dc02}
    Set Suite Variable  ${dc02_libelle}

    ##
    ## DOSSIER DE COORDINATION 03
    ##
    &{dc03} =  Create Dictionary
    ...  dossier_coordination_type=Visite de réception
    ...  description=Agrandissement
    ...  etablissement=${etab03_titre}
    ...  date_demande=12/11/2014
    ...  a_qualifier=false
    ...  dossier_instruction_secu=true
    ...  dossier_instruction_acc=true
    ${dc03_libelle} =  Ajouter le dossier de coordination  ${dc03}
    Set Suite Variable  ${dc03}
    Set Suite Variable  ${dc03_libelle}

    ##
    ## DOSSIER DE COORDINATION 04
    ##
    &{dc04} =  Create Dictionary
    ...  dossier_coordination_type=Visite périodique SI
    ...  description=Nouveau
    ...  etablissement=${etab04_titre}
    ...  date_demande=14/11/2014
    ...  a_qualifier=false
    ...  dossier_instruction_secu=true
    ...  dossier_instruction_acc=false
    ${dc04_libelle} =  Ajouter le dossier de coordination  ${dc04}
    Set Suite Variable  ${dc04}
    Set Suite Variable  ${dc04_libelle}

    ##
    ## DEMANDE DE PASSAGE EN REUNION 01
    ##
    &{dpr01} =  Create Dictionary
    ...  service=SI
    ...  dossier_coordination=${dc01_libelle}
    ...  dossier_instruction=${dc01_libelle}-SI
    ...  date_souhaitee=19/11/2014
    ...  reunion_type=${reutype02.libelle}
    ...  reunion_categorie=Plans
    ...  motivation=Motivation1
    #
    ${dpr01_id} =  Ajouter la demande de passage en réunion depuis le contexte du dossier d'instruction
    ...  ${dpr01.dossier_coordination}  ${dpr01.service}  ${dpr01.date_souhaitee}  ${dpr01.reunion_type}  ${dpr01.reunion_categorie}  ${dpr01.motivation}
    Set Suite Variable  ${dpr01}
    Set Suite Variable  ${dpr01_id}

    ##
    ## DEMANDE DE PASSAGE EN REUNION 02
    ##
    &{dpr02} =  Create Dictionary
    ...  service=SI
    ...  dossier_coordination=${dc01_libelle}
    ...  dossier_instruction=${dc01_libelle}-SI
    ...  date_souhaitee=02/12/2014
    ...  reunion_type=${reutype01.libelle}
    ...  reunion_categorie=Plans
    ...  motivation=Motivation2
    #
    ${dpr02_id} =  Ajouter la demande de passage en réunion depuis le contexte du dossier d'instruction
    ...  ${dpr02.dossier_coordination}  ${dpr02.service}  ${dpr02.date_souhaitee}  ${dpr02.reunion_type}  ${dpr02.reunion_categorie}  ${dpr02.motivation}
    Set Suite Variable  ${dpr02}
    Set Suite Variable  ${dpr02_id}

    ##
    ## DEMANDE DE PASSAGE EN REUNION 03
    ##
    &{dpr03} =  Create Dictionary
    ...  service=ACC
    ...  dossier_coordination=${dc01_libelle}
    ...  dossier_instruction=${dc01_libelle}-ACC
    ...  date_souhaitee=30/11/2014
    ...  reunion_type=${reutype03.libelle}
    ...  reunion_categorie=Plans
    ...  motivation=
    #
    ${dpr03_id} =  Ajouter la demande de passage en réunion depuis le contexte du dossier d'instruction
    ...  ${dpr03.dossier_coordination}  ${dpr03.service}  ${dpr03.date_souhaitee}  ${dpr03.reunion_type}  ${dpr03.reunion_categorie}  ${dpr03.motivation}
    Set Suite Variable  ${dpr03}
    Set Suite Variable  ${dpr03_id}

    ##
    ## DEMANDE DE PASSAGE EN REUNION 04
    ##
    &{dpr04} =  Create Dictionary
    ...  service=SI
    ...  dossier_coordination=${dc02_libelle}
    ...  dossier_instruction=${dc02_libelle}-SI
    ...  date_souhaitee=08/12/2014
    ...  reunion_type=${reutype01.libelle}
    ...  reunion_categorie=Plans
    ...  motivation=
    #
    ${dpr04_id} =  Ajouter la demande de passage en réunion depuis le contexte du dossier d'instruction
     ...  ${dpr04.dossier_coordination}  ${dpr04.service}  ${dpr04.date_souhaitee}  ${dpr04.reunion_type}  ${dpr04.reunion_categorie}  ${dpr04.motivation}
    Set Suite Variable  ${dpr04}
    Set Suite Variable  ${dpr04_id}

    #
    Ajouter la demande de passage en réunion depuis le contexte du dossier d'instruction
    ...  ${dc02_libelle}  SI    23/11/2014    CCS Groupe technique d'étude de plans     Plans
    Ajouter la demande de passage en réunion depuis le contexte du dossier d'instruction
    ...  ${dc02_libelle}  ACC    02/12/2014    CCA Plénière     Plans
    #
    Ajouter la demande de passage en réunion depuis le contexte du dossier d'instruction
    ...  ${dc03_libelle}  SI    02/12/2014    CCS Plénière     Visites
    #
    Ajouter la demande de passage en réunion depuis le contexte du dossier d'instruction
    ...  ${dc04_libelle}  SI    30/11/2014    CCS Plénière     Visites

    #
    Planifier toutes les demandes de passages pressenties pour la réunion  ${reunion01_code}
    Planifier toutes les demandes de passages pressenties pour la réunion  ${reunion02_code}
    Planifier toutes les demandes de passages pressenties pour la réunion  ${reunion03_code}
    #
    Numéroter l'ordre du jour de la réunion  ${reunion01_code}
    Numéroter l'ordre du jour de la réunion  ${reunion02_code}
    Numéroter l'ordre du jour de la réunion  ${reunion03_code}


Intégration du champ réunion dans la demande de passage en réunion (contexte DI)
    [Documentation]  Le champ réunion dans ce contexte a le comportement suivant :
    ...  - vide en consultation lorsque la dpr est non planifiée
    ...  - rempli en consutation lorque la dpr est planifiée avec un lien si
    ...    l'utilisateur a la permission sinon simplement avec un libellé

    # En tant que profil ADMINISTRATEUR
    Depuis la page d'accueil  admin  admin
    # Création d'une demande de passage non liée
    &{dpr} =  Create Dictionary
    ...  service=SI
    ...  dossier_coordination=${dc01_libelle}
    ...  dossier_instruction=${dc01_libelle}-SI
    ...  date_souhaitee=19/11/2019
    ...  reunion_type=${reutype02.libelle}
    ...  reunion_categorie=Plans
    ...  motivation=Motivation Intégration
    ${dpr_id} =  Ajouter la demande de passage en réunion depuis le contexte du dossier d'instruction
    ...  ${dpr.dossier_coordination}  ${dpr.service}  ${dpr.date_souhaitee}  ${dpr.reunion_type}  ${dpr.reunion_categorie}  ${dpr.motivation}
    # Vérification de l'absence du lien vers une réunion
    Depuis l'onglet réunion du dossier d'instruction  ${dpr.dossier_instruction}
    Click On Link  ${dpr.date_souhaitee}
    WUX  Element Should Be Visible  css=#dossier_instruction_reunion
    Page Should Not Contain Element  css=#link_reunion
    # Planification du dossier en réunion
    Planifier toutes les demandes de passages pressenties pour la réunion  ${reunion02_code}
    # Vérification de la présence du lien vers une réunion et de son formatage
    Depuis l'onglet réunion du dossier d'instruction  ${dpr.dossier_instruction}
    Click On Link  ${dpr.date_souhaitee}
    WUX  Element Should Be Visible  css=#dossier_instruction_reunion
    Page Should Contain Element  css=#link_reunion
    ${link_reunio_text}=   Set Variable  ${reunion02_code} - ${reutype02.libelle}
    Element Should Contain  css=#link_reunion  ${link_reunio_text}
    Page Should Contain Element  xpath=//a[@id='link_reunion' and text()[contains(.,"${link_reunio_text}")]]
    Click Element  css=#link_reunion
    WUX  Page Title Should Contain  Suivi > Réunions > Gestion > (${reunion02_code})
    # En tant que profil TECHNICIEN SI (pas accès aux réunions)
    Depuis la page d'accueil  technicien-si  technicien-si
    Depuis l'onglet réunion du dossier d'instruction  ${dpr.dossier_instruction}
    Click On Link  ${dpr.date_souhaitee}
    WUX  Element Should Be Visible  css=#dossier_instruction_reunion
    Page Should Not Contain Element  css=#link_reunion
    Element Should Contain  css=#fieldset-sousform-dossier_instruction_reunion_contexte_di-reunion  ${reunion02_code} - ${reutype02.libelle}


Paramétrage d'un type de réunion

    [Documentation]  Paramétrage d'un type de réunion :
    ...  - ...
    ...  - Gestion des catactères de validatité des instances, avis, catégories
    ...  - ...

    # En tant que profil ADMINISTRATEUR
    Depuis la page d'accueil  admin  admin

    ###
    ### Gestion des catactères de validatité des instances, avis, catégories
    ###
    ## - on cré une catégorie de réunion, une instance de réunion, un avis de
    ##   réunion dont les dates de validité indique que l'enregistrement est
    ##   valide
    ## - on cré un type de réunion en sélectionnant un élément de chaque
    ##   précédemment créé
    ## - on modifie les dates de validité de la catégorie de réunion,
    ##   l'instance de réunion, l'avis de réunion pour que les enregistrements
    ##   deviennent non valides
    ## - on accède au formulaire de modification du type de réunion
    ##   précédemment créé pour vérifier que les trois valeurs sont toujours
    ##   sélectionnées
    ## - on désélectionne les éléments en question et on valide le formulaire
    ##   de modification du type de réunion
    ## - on accède au formulaire de modification du type de réunion pour
    ##   vérifier que les valeurs non valides ne sont plus proposées dans les
    ##   listes à choix

    ##
    ## CATEGORIE DE REUNION 05
    ##
    &{reucategorie05} =  Create Dictionary
    ...  code=T100REUC5
    ...  libelle=t100 REUNION CATEGORIE 05
    ...  description=Description
    ...  service=Sécurité Incendie
    ...  ordre=30
    Ajouter la catégorie de réunion  ${reucategorie05}
    Set Suite Variable  ${reucategorie05}

    ##
    ## AVIS DE REUNION 03
    ##
    &{reuavis03} =  Create Dictionary
    ...  code=T100REUA3
    ...  libelle=T100 REUNION AVIS 03
    ...  description=Description
    ...  service=Sécurité Incendie
    Ajouter l'avis de réunion  ${reuavis03}
    Set Suite Variable  ${reuavis03}

    ##
    ## INSTANCE DE REUNION 07
    ##
    &{reuinstance07} =  Create Dictionary
    ...  code=T100REUI7
    ...  libelle=T100 REUNION INSTANCE 07
    ...  description=Description
    ...  service=Sécurité Incendie
    Ajouter l'instance de réunion  ${reuinstance07}
    Set Suite Variable  ${reuinstance07}

    ##
    ## TYPE DE REUNION 04
    ##
    @{categories_autorisees}  Create List  ${reucategorie05.libelle}
    @{avis_autorises}  Create List  ${reuavis03.libelle}
    @{instances_autorisees}  Create List  ${reuinstance07.libelle}
    &{reutype04} =  Create Dictionary
    ...  code=T100REUT4
    ...  libelle=T100 REUNION TYPE 04
    ...  service=Sécurité Incendie
    ...  categories_autorisees=@{categories_autorisees}
    ...  avis_autorises=@{avis_autorises}
    ...  instances_autorisees=@{instances_autorisees}
    Ajouter le type de réunion  ${reutype04}
    Set Suite Variable  ${reutype04}

    ## On rend les trois paramètres non valides
    &{reuavis03_validite} =  Create Dictionary
    ...  om_validite_fin=01/01/2015
    Modifier l'avis de réunion  ${reuavis03.code}  ${reuavis03_validite}
    &{reuinstance07_validite} =  Create Dictionary
    ...  om_validite_fin=01/01/2015
    Modifier l'instance de réunion  ${reuinstance07.code}  ${reuinstance07_validite}
    &{reucategorie05_validite} =  Create Dictionary
    ...  om_validite_fin=01/01/2015
    Modifier la catégorie de réunion  ${reucategorie05.code}  ${reucategorie05_validite}

    ## On vérifie que les valeurs sont biens séléectionnées et on les désélectionne
    Depuis le formulaire de modification du type de réunion  ${reutype04.code}
    ${categories_autorisees} =  Get Selected List Labels  css=#categories_autorisees
    ${avis_autorises} =  Get Selected List Labels  css=#avis_autorises
    ${instances_autorisees} =  Get Selected List Labels  css=#instances_autorisees
    List Should Contain Value  ${categories_autorisees}  ${reucategorie05.libelle}
    List Should Contain Value  ${avis_autorises}  ${reuavis03.libelle}
    List Should Contain Value  ${instances_autorisees}  ${reuinstance07.libelle}
    Unselect From List By Label  css=#categories_autorisees  ${reucategorie05.libelle}
    Unselect From List By Label  css=#avis_autorises  ${reuavis03.libelle}
    Unselect From List By Label  css=#instances_autorisees  ${reuinstance07.libelle}
    Click On Submit Button
    Valid Message Should Contain  Vos modifications ont bien été enregistrées.

    ## On vérifie que les valeurs ne sont plus proposées dans les listes à choix
    Depuis le formulaire de modification du type de réunion  ${reutype04.code}
    @{categories_autorisees}  Create List  ${reucategorie05.libelle}
    @{avis_autorises}  Create List  ${reuavis03.libelle}
    @{instances_autorisees}  Create List  ${reuinstance07.libelle}
    Select List Should Not Contain List  css=#categories_autorisees  ${categories_autorisees}
    Select List Should Not Contain List  css=#avis_autorises  ${avis_autorises}
    Select List Should Not Contain List  css=#instances_autorisees  ${instances_autorisees}


Création d'une réunion

    [Documentation]

    Comment    @todo Écrire le Test Case'


Création manuelle d'une demande de passage en réunion

    [Documentation]

    Comment    @todo Écrire le 'Test Case'


Création automatique d'une demande de passage en réunion - mode programmation

    [Documentation]  La création automatique d'une demande de passage en
    ...  réunion se fait par l'action 'planifier nouveau' dans le contexte
    ...  d'une réunion. Les trois modes disponibles sont : 'programmation',
    ...  'réunion' et 'dossier'.
    ...
    ...  Le mode 'programmation' permet de récupérer l'ensemble des DI
    ...  qui constituent une semaine de programmation de visites. Les
    ...  carcatéristiques de ce mode sont les suivantes : ne sont proposées
    ...  que les programmations passées (semaine précédente) qui n'ont pas
    ...  été déjà planifiées, même si le DI possède plusieurs visites on ne
    ...  le planifie qu'une seule fois, on ne peut pas planifier une
    ...  programmation sans DI, la sélection de programmation est obligatoire.

    ${tcvid} =  Set Variable  T100 CADUDDPERMP
    ${current_year} =  Get Time  year
    ${past_year} =  Evaluate  ${current_year}-1
    ${next_year} =  Evaluate  ${current_year}+1

    Depuis la page d'accueil  admin  admin

    &{etab_l_01} =  Create Dictionary
    ...  libelle=ETABL01 ${tcvid}
    ...  etablissement_nature=ERP potentiel
    ${etab_l_01.code} =  Ajouter l'établissement  ${etab_l_01}
    ${etab_l_01.titre} =  Set Variable  ${etab_l_01.code} - ${etab_l_01.libelle}
    &{dc_l_01} =  Create Dictionary
    ...  dossier_coordination_type=Visite de contrôle SI
    ...  description=DCL01 ${tcvid}
    ...  etablissement=${etab_l_01.titre}
    ...  a_qualifier=false
    ${dc_l_01.libelle} =  Ajouter le dossier de coordination  ${dc_l_01}

    # Création d'une réunion
    ${reu_l_01} =  Create Dictionary
    ...  reunion_type=Commisson Communale de Sécurité
    ${reu_l_01.code} =  Ajouter la réunion  ${reu_l_01}

    # Création d'une semaine de programmation ancienne vide
    &{prog_l_01} =  Create Dictionary
    ...  service=Sécurité Incendie
    ...  annee=2017
    ...  semaine=52
    ${prog_l_01.code} =  Ajouter une semaine de programmation
    ...  service_libelle=${prog_l_01.service}
    ...  annee=${prog_l_01.annee}
    ...  numero_semaine=${prog_l_01.semaine}
    # Création d'une semaine de programmation ancienne dans l'année précédente non vide
    &{prog_l_02} =  Create Dictionary
    ...  service=Sécurité Incendie
    ...  annee=${past_year}
    ...  semaine=01
    ${prog_l_02.code} =  Ajouter une semaine de programmation
    ...  service_libelle=${prog_l_02.service}
    ...  annee=${prog_l_02.annee}
    ...  numero_semaine=${prog_l_02.semaine}
    ${visite_l_01} =  Create Dictionary
    ...  programmation=${prog_l_02.code}
    ...  technicien=Paul DURAND
    ...  dossier_instruction_libelle=${dc_l_01.libelle}-SI
    ${visite_l_01.id} =  Ajouter une visite depuis la programmation
    ...  semaine_programmation=${visite_l_01.programmation}
    ...  technicien=${visite_l_01.technicien}
    ...  dossier_instruction_libelle=${visite_l_01.dossier_instruction_libelle}
    ...  jour_semaine=1
    ...  a_poursuivre=true
    ${visite_l_02} =  Create Dictionary
    ...  programmation=${visite_l_01.programmation}
    ...  technicien=${visite_l_01.technicien}
    ...  dossier_instruction_libelle=${visite_l_01.dossier_instruction_libelle}
    ${visite_l_02.id} =  Ajouter une visite depuis la programmation
    ...  semaine_programmation=${visite_l_02.programmation}
    ...  technicien=${visite_l_02.technicien}
    ...  dossier_instruction_libelle=${visite_l_02.dossier_instruction_libelle}
    ...  jour_semaine=3
    ...  a_poursuivre=true
    # Création d'une semaine de programmation future dans l'année suivante
    &{prog_l_03} =  Create Dictionary
    ...  service=Sécurité Incendie
    ...  annee=${next_year}
    ...  semaine=52
    ${prog_l_03.code} =  Ajouter une semaine de programmation
    ...  service_libelle=${prog_l_03.service}
    ...  annee=${prog_l_03.annee}
    ...  numero_semaine=${prog_l_03.semaine}

    # On se positionne dans le formulaire de planification directe
    Depuis le formulaire de planification directe en mode pour la réunion
    ...  programmation
    ...  ${reu_l_01.code}
    Page Subtitle Should Be  > Planifier un ou plusieurs dossiers sans demande de passage

    # La programmation est obligatoire
    Click On Submit Button
    Page Subtitle Should Be  > Planifier un ou plusieurs dossiers sans demande de passage
    Error Message Should Be  La sélection de la programmation est obligatoire.

    # Les programmations proposées sont soit les années précédentes, soit dans
    # l'année courante et dans le passé
    ${programmations_prop} =  Get List Items  css=#programmation
    List Should Contain Value  ${programmations_prop}  Programmation n°${prog_l_01.code}
    List Should Contain Value  ${programmations_prop}  Programmation n°${prog_l_02.code}
    List Should Not Contain Value  ${programmations_prop}  Programmation n°${prog_l_03.code}

    # Programmation vide
    Select From List By Label  css=#programmation  Programmation n°${prog_l_01.code}
    Select From List By Label  css=#categorie  Visites
    Click On Submit Button
    Page Subtitle Should Be  > Planifier un ou plusieurs dossiers sans demande de passage
    Error Message Should Be  Aucun dossier sur cette programmation.

    # Programmation ok
    Select From List By Label  css=#programmation  Programmation n°${prog_l_02.code}
    Select From List By Label  css=#categorie  Visites
    Click On Submit Button
    Page Subtitle Should Be  > Planifier un ou plusieurs dossiers sans demande de passage
    Valid Message Should Be  Dossier(s) planifié(s) avec succès.

    # La validation du formulaire réinitialise le mode de planificiation
    Select From List By Label  css=#reunion_choix_mode_planif  programmation
    Click On Submit Button
    Page Subtitle Should Be  > Planifier un ou plusieurs dossiers sans demande de passage

    # La programmation importée précédemment ne doit plus apparaître dans les
    # propositions
    ${programmations_prop} =  Get List Items  css=#programmation
    List Should Contain Value  ${programmations_prop}  Programmation n°${prog_l_01.code}
    List Should Not Contain Value  ${programmations_prop}  Programmation n°${prog_l_02.code}
    List Should Not Contain Value  ${programmations_prop}  Programmation n°${prog_l_03.code}

    # On numérote la réunion pour vérifier qu'un seul DI a été ajouté à l'ordre du jour même
    # si il porte sur deux visites
    Numéroter l'ordre du jour de la réunion  ${reu_l_01.code}
    Page Should Contain  numérotation confirmée - dernier numéro : 1


Création automatique d'une demande de passage en réunion - mode réunion

    [Documentation]  La création automatique d'une demande de passage en
    ...  réunion se fait par l'action 'planifier nouveau' dans le contexte
    ...  d'une réunion. Les trois modes disponibles sont : 'programmation',
    ...  'réunion' et 'dossier'.
    ...
    ...  Le mode 'réunion' permet de récupérer l'ensemble des DI
    ...  qui constituent une réunion précédente clôturée.

    Comment    @todo Écrire le 'Test Case'


Création automatique d'une demande de passage en réunion - mode dossier

    [Documentation]  La création automatique d'une demande de passage en
    ...  réunion se fait par l'action 'planifier nouveau' dans le contexte
    ...  d'une réunion. Les trois modes disponibles sont : 'programmation',
    ...  'réunion' et 'dossier'.
    ...
    ...  Le mode 'dossier' permet de planifier un DI à la réunion.

    Comment    @todo Écrire le 'Test Case'


Modification d'une demande de passage en réunion

    [Documentation]

    Comment    @todo Écrire le 'Test Case'


Composition de l'ordre du jour d'une réunion

    [Documentation]    La composition de l'ordre du jour contient les actions suivantes :
    ...    - Visualisation des dossiers planifiés à l'ordre du jour
    ...    - Planification des dossiers pressentis
    ...    - Déplanification des dossiers planifiés
    ...    - Planification des dossiers non pressentis

    Comment    @todo Écrire le 'Test Case'


Numérotation de l'ordre du jour d'une réunion

    [Documentation]    Une action permet de déclencher la numérotation de l'ordre du jour. Cette action numérote la liste des demandes de passage planifiées à partir de 1. Une fois que la numérotation a été déclenchée, tout nouveau dossier prendra le numéro suivant. Un dossier retiré de l'ordre du jour laissera un vide dans la numérotation. La numérotation se fait par catégorie selon l'ordre défini dans le paramétrage du type de réunion.

    Comment    @todo Écrire le 'Test Case'


Suivi des passages en réunion par le technicien

    [Documentation]    Un widget sur le tableau de bord d'un utilisateur 'technicien' permet de visualiser les dossiers dont il est l'instructeur et qui vont passer en réunion. Ce widget liste toutes les demandes de passage planifiées dans le mois suivant par rapport à la date du jour dont l'utilisateur connecté est l'instructeur du dossier d'instruction lié à ces demandes. Le listing contient une rupture par réunion (la date et le libellé du type de réunion sont affichés), puis par catégorie de passage de la réunion en question (le libellé de la catégorie est affiché). C'est l'identifiant du dossier d'instruction qui est présenté avec un lien vers l'écran de visualisation de ce dossier d'instruction.

    Comment    @todo Écrire le 'Test Case'


Champs de fusion [reunion.*]
    [Documentation]  Champs de fusion de la requête 'reunion'.
    ...
    ...  Quatre champs de fusion concernant le président de la réunion (
    ...  [reunion.president],
    ...  [reunion.president_signataire_membre],
    ...  [reunion.president_signataire_membre_description],
    ...  [reunion.president_signataire_observation]).

    Depuis la page d'accueil  admin  admin
    ## Constitution du jeu de données
    # Création de deux instances de réunion, d'une catégorie de réunion, d'un
    # type de réunion (avec comme président l'instance 01) et une réunion?
    &{reuinst_l_01} =  Create Dictionary
    ...  code=TEST100GODJ1
    ...  libelle=TEST100 Génération Ordre Du Jour 1
    ...  service=Accessibilité
    Ajouter l'instance de réunion  ${reuinst_l_01}
    Ajouter un membre depuis le contexte de l'instance  ${reuinst_l_01.code}  president_membre_nom  president_membre_description
    &{reuinst_l_02} =  Create Dictionary
    ...  code=TEST100GODJ2
    ...  libelle=TEST100 Génération Ordre Du Jour 2
    ...  service=Accessibilité
    Ajouter l'instance de réunion  ${reuinst_l_02}
    &{reucateg_l_01} =  Create Dictionary
    ...  code=TEST100GODJ
    ...  libelle=TEST100 Génération Ordre Du Jour
    ...  description=Plans
    ...  service=Accessibilité
    ...  ordre=10
    Ajouter la catégorie de réunion  ${reucateg_l_01}
    @{categories_autorisees}  Create List  ${reucateg_l_01.libelle}
    @{avis_autorises}  Create List  FAVORABLE  DEFAVORABLE
    @{instances_autorisees}  Create List  ${reuinst_l_01.libelle}  ${reuinst_l_02.libelle}
    &{reutype_l_01} =  Create Dictionary
    ...  code=TODJ
    ...  libelle=TEST100 Génération Ordre Du Jour
    ...  service=Accessibilité
    ...  categories_autorisees=@{categories_autorisees}
    ...  avis_autorises=@{avis_autorises}
    ...  instances_autorisees=@{instances_autorisees}
    ...  lieu_salle=DGUP
    ...  lieu_adresse_ligne1=30 AV SALENGRO
    ...  lieu_adresse_ligne2=13003 MARSEILLE
    ...  heure=12:00
    ...  listes_de_diffusion=nospam@openmairie.org
    ...  president=${reuinst_l_01.libelle}
    ...  modele_courriel_convoquer=Bonjour,\n\nVeuillez trouver ci joint les informations concernant la réunion citée en objet :\n - Date : [date]\n - Heure : [heure]\n\nCordialement.
    ...  modele_ordre_du_jour=Réunion - Ordre du jour
    Ajouter le type de réunion  ${reutype_l_01}
    &{reu_l_01} =  Create Dictionary
    ...  reunion_type=${reutype_l_01.libelle}
    ...  date_reunion=20/07/2016
    ...  date_reunion_yyyy_mm_dd=2016-07-20
    Ajouter la réunion  ${reu_l_01}
    ${reu_l_01.code} =  Set Variable  ${reutype_l_01.code}-${reu_l_01.date_reunion_yyyy_mm_dd}
    ${reu_l_01.libelle} =  Set Variable  ${reu_l_01.reunion_type} du ${reu_l_01.date_reunion}
    # On ajoute les champs de fusion à tester dans la lettre type
    ${titre} =  Set Variable  "<p>ORDRE DU JOUR</p><p>
    ...  REU_PRE_BEGIN_[reunion.president]_REU_PRE_END<br/>
    ...  REU_PRE_SIGN_MEMBRE_BEGIN_[reunion.president_signataire_membre]_REU_PRE_SIGN_MEMBRE_END<br/>
    ...  REU_PRE_SIGN_MEMBRE_DESC_BEGIN_[reunion.president_signataire_membre_description]_REU_PRE_SIGN_MEMBRE_DESC_END<br/>
    ...  REU_PRE_SIGN_OBS_BEGIN_[reunion.president_signataire_observation]_REU_PRE_SIGN_OBS_END
    ...  </p>"
    Modifier la lettre-type
    ...  reunion_ordre_du_jour
    ...  null
    ...  ${titre}
    ...  null
    Modifier la lettre-type
    ...  reunion_ordre_du_jour

    ## Cas n°1 : pas de signataire => champs de fusion vides
    # Les champs de fusion dans l'édition PDF doivent donc retourner une chaîne vide.
    Depuis le contexte de la réunion  ${reu_l_01.code}
    ${contenu_pdf} =  Create List
    ...  REU_PRE_BEGIN_${reuinst_l_01.libelle}_REU_PRE_END
    ...  REU_PRE_SIGN_MEMBRE_BEGIN__REU_PRE_SIGN_MEMBRE_END
    ...  REU_PRE_SIGN_MEMBRE_DESC_BEGIN__REU_PRE_SIGN_MEMBRE_DESC_END
    ...  REU_PRE_SIGN_OBS_BEGIN__REU_PRE_SIGN_OBS_END
    Click On Form Portlet Action  reunion  edition-ordre_du_jour  new_window
    La page du fichier PDF doit contenir les chaînes de caractères
    ...  ${OM_PDF_TITLE}
    ...  ${contenu_pdf}
    ...  page_number=1

    ## Cas n°2 : membre signataire sélectioné => champs de fusion correctement rendus
    # On renseigne des valeurs dans les champs du signataire président de la réunion.
    # Les champs de fusion dans l'édition PDF doivent retourner les bonnes valeurs.
    Depuis le listing des signataires dans le contexte de la réunion  ${reu_l_01.code}
    Click Link  ${reuinst_l_01.libelle}
    Click On SubForm Portlet Action  lien_reunion_r_instance_r_i_membre  modifier
    Select From List ByL abel  css=#reunion_instance_membre  president_membre_nom
    Input Text  css=#observation  obsobs
    Click On Submit Button In SubForm
    Valid Message Should Be In SubForm  Vos modifications ont bien été enregistrées.
    Depuis le contexte de la réunion  ${reu_l_01.code}
    ${contenu_pdf} =  Create List
    ...  REU_PRE_BEGIN_${reuinst_l_01.libelle}_REU_PRE_END
    ...  REU_PRE_SIGN_MEMBRE_BEGIN_president_membre_nom_REU_PRE_SIGN_MEMBRE_END
    ...  REU_PRE_SIGN_MEMBRE_DESC_BEGIN_president_membre_description_REU_PRE_SIGN_MEMBRE_DESC_END
    ...  REU_PRE_SIGN_OBS_BEGIN_obsobs_REU_PRE_SIGN_OBS_END
    Click On Form Portlet Action  reunion  edition-ordre_du_jour  new_window
    La page du fichier PDF doit contenir les chaînes de caractères
    ...  ${OM_PDF_TITLE}
    ...  ${contenu_pdf}
    ...  page_number=1


Génération d'un ordre du jour

    [Documentation]    Édition PDF

    Comment    @todo Écrire le 'Test Case'


Convocation des membres

    [Documentation]    À tout moment une action permet de convoquer les instances de la réunion en cliquant sur l'action « Convoquer les membres » dans l'écran de gestion de la réunion. Cette action permet d'envoyer un mail aux différentes adresses paramétrées dans les instances, ainsi qu'aux adresses présentes dans le champ « liste de diffusion » de la réunion. Un écran permet de confirmer l'envoi du mail avec une case à cocher permettant d'indiquer si l'ordre du jour doit être envoyé ou non en pièce jointe. La date de dernière convocation est stockée pour mémoire.

    Comment    @todo Écrire le 'Test Case'


Sélection des signataires

    [Documentation]    Un écran permet, pour chaque instance de la réunion :
    ...    - de sélectionner le membre qui la représente
    ...    - de saisir un texte libre.
    ...    L'objectif principal de ces informations est de remplir les feuilles de présence.

    Comment    @todo Écrire le 'Test Case'


Génération de la feuille de présence

    [Documentation]    Édition PDF

    Comment    @todo Écrire le 'Test Case'


Saisie du retour d'avis d'un passage en réunion

    [Documentation]    Depuis l'écran de gestion d'une réunion, le listing des dossiers planifiés (l'ordre du jour) permet d'accéder à chaque formulaire de saisie du retour d'avis. Ce retour est composé des informations suivantes :
    ...    - proposition d'avis : lecture seule,
    ...    - proposition de complément d'avis (éventuellement second avis) : lecture seule,
    ...    - avis : sélection d'un avis dans la liste des avis,
    ...    - complément d'avis (éventuellement second avis) : ligne de texte,
    ...    - motivation de l'avis : texte.

    Comment    @todo Écrire le 'Test Case'


Saisie d'une autorité de police depuis un retour d'avis

    [Documentation]    Ce 'Test Case' est traité dans le 'Test Suite' Autorité de Police.


    Depuis la demande de passage (retour d'avis) dans le contexte de la réunion  ${reunion01_code}  ${dpr02_id}
    Sleep  1
    Execute JavaScript  window.jQuery("#formSpecificContent_autorite_police #liste_autorite_police").attr("style", "display:table;");
    WUX  Element Should Be Visible  css=#formSpecificContent_autorite_police #liste_autorite_police
    Click Element  css=#add_autorite_police
    Select From List By Label    css=#autorite_police_decision    Différée
    Select From List By Label    css=#autorite_police_motif    Vérifications
    Input Text    css=#delai    10
    WUX  Click Element  css=#sousform-autorite_police.overlay div.formControls input[type="submit"]
    Sleep    1
    La page ne doit pas contenir d'erreur
    WUX  Element Should Contain  css=#sousform-autorite_police div.message  Vos modifications ont bien été enregistrées.
    Click Element  css=#sousform-autorite_police a.retour
    #


Saisie d'une demande de passage depuis un retour d'avis

    [Documentation]    Dans certains cas, il n'y a pas de prise d'avis ou de décision sur un dossier lors d'une réunion. Dans ce cas un avis tel que 'A revoir' ou 'Différé' est saisi, qui permettra la suite du processus. Il est donc nécessaire de reprogrammer un passage pour le dossier en question. Dans le même écran de saisie, une action permet d'insérer et de saisir des demandes de passage en réunion. Le formulaire est identique au formulaire de demande de passage manuel. Il est ainsi possible d'indiquer la date souhaitée de passage, le type de réunion, la catégorie et éventuellement la proposition d'avis.
    Depuis la page d'accueil  admin  admin
    Depuis la demande de passage (retour d'avis) dans le contexte de la réunion  ${reunion01_code}  ${dpr04_id}
    Sleep  1
    Click Element  css=#formSpecificContent_dossier_instruction_reunion fieldset legend
    WUX  Element Should Be Visible  css=#add_dossier_instruction_reunion
    Click Element  css=#add_dossier_instruction_reunion
    WUX  Element Should Be Visible  css=#sousform-dossier_instruction_reunion.overlay div.formControls input[type="submit"]
    Click Element  css=#sousform-dossier_instruction_reunion a.retour
    WUX  Element Should Not Be Visible  css=#sousform-dossier_instruction_reunion.overlay div.formControls input[type="submit"]
    Click Element  css=#add_dossier_instruction_reunion
    WUX  Element Should Be Visible  css=#sousform-dossier_instruction_reunion.overlay div.formControls input[type="submit"]
    Select From List By Label  css=#reunion_type  CCS Plénière
    Select From List By Label  css=#reunion_type_categorie  Plans
    Input Datepicker  date_souhaitee  10/01/2019
    WUX  Click Element  css=#sousform-dossier_instruction_reunion.overlay div.formControls input[type="submit"]
    Sleep    1
    La page ne doit pas contenir d'erreur
    WUX  Element Should Contain  css=#sousform-dossier_instruction_reunion.overlay div.message  Vos modifications ont bien été enregistrées.
    WUX  Element Should Not Be Visible  css=#sousform-dossier_instruction_reunion.overlay soustab-container
    Click Element  css=#sousform-dossier_instruction_reunion a.retour
    WUX  Element Should Not Be Visible  css=#sousform-dossier_instruction_reunion.overlay div.formControls input[type="submit"]


Génération d'un compte-rendu global

    [Documentation]    Édition PDF

    Comment    @todo Écrire le 'Test Case'


Génération d'un compte-rendu par dossier

    [Documentation]    Édition PDF

    # Modification de la lettre type pour tester les champs de fusion
    # supplémentaire de la date de visite (date_visite_texte, date_visite_jour)
    Depuis la page d'accueil    admin    admin
    Depuis le listing des lettres-types
    Depuis le contexte de la lettre-type  reunion_compte_rendu_avis
    Click On Form Portlet Action  om_lettretype  modifier
    Open Fieldset  om_lettretype  pied-de-page
    ${pied_page_html}=  Set Variable  <div style="text-align: left; color: gray;"><p class="adresse_1ligne">Adresse 1 ligne: [etablissement.adresse_complete_sur_une_ligne]</p><p class="etab_adresse_4ligne">Établissement 4 lignes: [etablissement.adresse_complete_sur_quatre_lignes]</p><p class="exp_adresse_4ligne">Exploitant 4 lignes: [exploitant.adresse_complete_sur_quatre_lignes]</p></div><p style='text-align: center; font-size: 8pt;'><em>Page &numpage/&nbpages</em></p>
    Input HTML  footer_om_htmletat  ${pied_page_html}
    Click On Submit Button
    Valid Message Should Contain  Vos modifications ont bien été enregistrées.

    Depuis la demande de passage (retour d'avis) dans le contexte de la réunion  ${reunion01_code}  ${dpr02_id}
    Sleep  1
    Click On SubForm Portlet Action  dossier_instruction_reunion_contexte_reunion  edition-compte_rendu_specifique  new_window
    # On se positionne dans le contexte du fichier PDF
    WUX  Open PDF  ${OM_PDF_TITLE}
    # On vérifie les particularités du PDF
    WUX  Page Should Contain  COMPTE RENDU SPÉCIFIQUE
    WUX  Page Should Contain  catégorie de passage : ${dpr02.reunion_categorie}
    WUX  Page Should Contain  Avis de la CCS
    WUX  Page Should Contain  Différée - 10 (jours)
    WUX  Page Should Contain  Vérifications

    # Champs de fusion 'etablissement.adresse_complete_sur_une_ligne' correctement remplacé
    WUX  Page Should Contain  Adresse 1 ligne:
    Page Should Contain  Adresse 1 ligne: 23 bis RUE DE ROME Bâtiment E Préf BP 12 13006 MARSEILLE Cedex 322

    # Champs de fusion 'etablissement.adresse_complete_sur_une_ligne' et
    # 'exploitant.adresse_complete_sur_une_ligne' correctement remplacé
    WUX  Page Should Contain  Établissement 4 lignes:
    Page Should Contain  Établissement 4 lignes: Bâtiment E
    Page Should Contain  23 bis RUE DE ROME
    Page Should Contain  Préf BP 12
    Page Should Contain  13006 MARSEILLE Cedex 322
    WUX  Page Should Contain  Exploitant 4 lignes:
    Page Should Contain  Exploitant 4 lignes: Bâtiment B
    Page Should Contain  3 A RUE BELLE
    Page Should Contain  Square BP 430
    Page Should Contain  13009 MARSEILLE Cedex 108

    # On ferme le PDF et on revient à la fenêtre principale
    Close PDF


Génération de l'ensemble des compte-rendus par dossier

    [Documentation]    Édition PDF

    Comment    @todo Écrire le 'Test Case'


Suppression d'une réunion

    [Documentation]    Une action permet de supprimer la réunion. Si une des demandes de passage a une valeur de retour alors la suppression est impossible (l'action n'est pas disponible). Lors de la suppression de la réunion, toutes les demandes de passages qui lui étaient affectées seront désaffectées et réapparaîtront dans le pool des demandes de passage.

    Comment    @todo Écrire le 'Test Case'


Clôture d'une réunion

    [Documentation]    Une action permet de clôturer la réunion. Si toutes les demandes de passage n'ont pas un avis alors la clôture est impossible (l'action n'est pas disponible). Cette action permet d'accéder à un formulaire de confirmation de la clôture de la réunion en donnant le choix à l'utilisateur de diffuser ou non par mail le compte-rendu global.
    ...    Lors de la clôture :
    ...    - diffusion du compte-rendu par mail aux instances de la réunion (aux différentes adresses paramétrées dans les instances et dans le champ « liste de diffusion »),
    ...    - génération et finalisation du compte-rendu (stockage du document),
    ...    - génération et finalisation de l'ordre du jour (stockage du document),
    ...    - marquage de la réunion comme clôturée.
    ...    Une fois la réunion clôturée :
    ...    - il n'est plus possible de modifier les avis.
    ...    - il n'est plus possible de modifier l'ordre du jour (les actions/écrans permettant de le gérer disparaissent).

    #
    Depuis la page d'accueil  admin  admin

    ##
    ## REUNION 04
    ##
    &{reunion04} =  Create Dictionary
    ...  reunion_type=${reutype01.libelle}
    ...  date_reunion=20/07/2016
    ...  date_reunion_yyyy_mm_dd=2016-07-20
    Ajouter la réunion  ${reunion04}
    ${reunion04_code} =  Set Variable  ${reutype01.code}-${reunion04.date_reunion_yyyy_mm_dd}
    ${reunion04_libelle} =  Set Variable  ${reunion04.reunion_type} du ${reunion04.date_reunion}

    #
    Planifier directement le DC ou DI pour la réunion dans la catégorie  ${dc01_libelle}  ${reunion04_code}  Plans
    Planifier directement le DC ou DI pour la réunion dans la catégorie  ${dc02_libelle}  ${reunion04_code}  Plans
    Planifier directement le DC ou DI pour la réunion dans la catégorie  ${dc01_libelle}  ${reunion04_code}  Visites
    Planifier directement le DC ou DI pour la réunion dans la catégorie  ${dc01_libelle}  ${reunion04_code}  Dossiers à enjeux
    Numéroter l'ordre du jour de la réunion  ${reunion04_code}

    #
    Depuis le contexte de la réunion  ${reunion04_code}
    # L'action 'CLOTURER' ne doit pas être disponible
    Portlet Action Should Not Be In Form    reunion    cloturer

    #
    Rendre l'avis sur tous les dossiers de la réunion  ${reunion04_code}

    # L'action 'ORDRE DU JOUR' doit afficher le PDF
    Depuis le contexte de la réunion  ${reunion04_code}
    Portlet Action Should Be In Form  reunion  edition-ordre_du_jour
    Click On Form Portlet Action  reunion  edition-ordre_du_jour  new_window
    # On se positionne dans le contexte du fichier PDF
    WUX  Open PDF  ${OM_PDF_TITLE}
    # On vérifie les particularités du PDF
    WUX  Page Should Contain  ORDRE DU JOUR
    # On ferme le PDF et on revient à la fenêtre principale
    Close PDF

    # L'action 'COMPTE RENDU GLOBAL' doit afficher le PDF
    Depuis le contexte de la réunion  ${reunion04_code}
    Portlet Action Should Be In Form    reunion    edition-compte_rendu_global
    Click On Form Portlet Action  reunion  edition-compte_rendu_global  new_window
    # On se positionne dans le contexte du fichier PDF
    WUX  Open PDF  ${OM_PDF_TITLE}
    # On vérifie les particularités du PDF
    WUX  Page Should Contain  COMPTE-RENDU GÉNÉRAL
    # On ferme le PDF et on revient
    Close PDF

    #
    Depuis le contexte de la réunion  ${reunion04_code}
    # L'action 'CLOTURER' doit être disponible
    Portlet Action Should Be In Form  reunion  cloturer
    Click On Form Portlet Action  reunion  cloturer

    # On valide le formulaire
    Click On Submit Button
    Valid Message Should Contain    La clôture de la réunion a été correctement effectuée.
    # L'action 'CLOTURER' ne doit pas être disponible
    Depuis le contexte de la réunion  ${reunion04_code}
    Portlet Action Should Not Be In Form    reunion    cloturer


    ##
    ## Gestion des métadonnées
    ##
    ## Vérifie les métadonnées pour les champs fichier :
    ##
    ## - Ordre du jour finalisé [reunion.om_fichier_reunion_odj]
    ##   > Généré
    ##   > Stockage à la finalisation de l'édition
    ##   > Mise à jour à chaque refinalisation de l'édition
    ##
    ## - Compte rendu global finalisé [reunion.om_fichier_reunion_cr_global]
    ##   > Généré
    ##   > Stockage à la finalisation de l'édition
    ##   > Mise à jour à chaque refinalisation de l'édition
    ##

    # Récupération des éléments concernant les fichiers (uid).
    ${reunion04_fichiers_uid} =  Récupérer les uid des champs fichier de la réunion  ${reunion04_code}

    ## => [reunion.om_fichier_reunion_odj] (METADATA FILESTORAGE)
    # Récupération des éléments concernant les fichiers (chemins) pour
    # effectuer les vérifications d'existence des fichiers et des métadonnées.
    ${reunion04_fichier_odj_path} =  Récupérer le chemin vers le fichier correspondant à l'uid  ${reunion04_fichiers_uid.om_fichier_reunion_odj}
    ${reunion04_fichier_odj_md_path} =  Récupérer le chemin vers le fichier de métadonnées correspondant à l'uid  ${reunion04_fichiers_uid.om_fichier_reunion_odj}
    Le fichier doit exister  ${reunion04_fichier_odj_path}
    Le fichier doit exister  ${reunion04_fichier_odj_md_path}
    # Composition du dictionnaire représentant les métadonnées qui doivent
    # apparaître dans le fichier de métadonnées.
    ${size}=  Get File Size  ${reunion04_fichier_odj_path}
    ${md} =  Create Dictionary
    # ...  filename=nom de fichier généré
    ...  mimetype=application/pdf
    ...  size=${size}
    ...  titre=(${reunion04_code}) ${reunion04_libelle} - ordre du jour
    ...  description=ordre du jour de réunion finalisé
    ...  application=openARIA
    ...  origine=généré
    ...  code_reunion=${reunion04_code}
    ...  date_reunion=${reunion04.date_reunion_yyyy_mm_dd}
    ...  type_reunion=${reunion04.reunion_type}
    ...  commission=false
    Les métadonnées (clé/valeur) doivent être présentes dans le fichier  ${md}  ${reunion04_fichier_odj_md_path}
    Les métadonnées (clé) ne doivent pas être présentes dans le fichier  ${md_no}  ${reunion04_fichier_odj_md_path}

    ## => [reunion.om_fichier_reunion_cr_global] (METADATA FILESTORAGE)
    # Récupération des éléments concernant les fichiers (chemins) pour
    # effectuer les vérifications d'existence des fichiers et des métadonnées.
    ${reunion04_fichier_cr_global_path} =  Récupérer le chemin vers le fichier correspondant à l'uid  ${reunion04_fichiers_uid.om_fichier_reunion_cr_global}
    ${reunion04_fichier_cr_global_md_path} =  Récupérer le chemin vers le fichier de métadonnées correspondant à l'uid  ${reunion04_fichiers_uid.om_fichier_reunion_cr_global}
    Le fichier doit exister  ${reunion04_fichier_cr_global_path}
    Le fichier doit exister  ${reunion04_fichier_cr_global_md_path}
    # Composition du dictionnaire représentant les métadonnées qui doivent
    # apparaître dans le fichier de métadonnées.
    ${size}=  Get File Size  ${reunion04_fichier_cr_global_path}
    ${md} =  Create Dictionary
    # ...  filename=nom de fichier généré
    ...  mimetype=application/pdf
    ...  size=${size}
    ...  titre=(${reunion04_code}) ${reunion04_libelle} - compte rendu global
    ...  description=compte rendu global de réunion finalisé
    ...  application=openARIA
    ...  origine=généré
    ...  code_reunion=${reunion04_code}
    ...  date_reunion=${reunion04.date_reunion_yyyy_mm_dd}
    ...  type_reunion=${reunion04.reunion_type}
    ...  commission=false
    Les métadonnées (clé/valeur) doivent être présentes dans le fichier  ${md}  ${reunion04_fichier_cr_global_md_path}
    Les métadonnées (clé) ne doivent pas être présentes dans le fichier  ${md_no}  ${reunion04_fichier_cr_global_md_path}

    # L'action 'ORDRE DU JOUR'doit afficher le PDF via le script file
    Depuis le contexte de la réunion  ${reunion04_code}
    Portlet Action Should Be In Form  reunion  edition-ordre_du_jour
    Click On Form Portlet Action  reunion  edition-ordre_du_jour  new_window
    # On se positionne dans le contexte du fichier PDF
    WUX  Open PDF  ${OM_PDF_TITLE}
    # On vérifie les particularités du PDF
    WUX  Page Should Contain  ORDRE DU JOUR
    # On ferme le PDF et on revient à la fenêtre principale
    Close PDF

    # L'action 'COMPTE RENDU GLOBAL' doit afficher le PDF via le script file
    Depuis le contexte de la réunion  ${reunion04_code}
    Portlet Action Should Be In Form    reunion    edition-compte_rendu_global
    Click On Form Portlet Action  reunion  edition-compte_rendu_global  new_window
    # On se positionne dans le contexte du fichier PDF
    WUX  Open PDF  ${OM_PDF_TITLE}
    # On vérifie les particularités du PDF
    WUX  Page Should Contain  COMPTE-RENDU GÉNÉRAL
    # On ferme le PDF et on revient
    Close PDF


Chargement des fichiers numérisés signés

    [Documentation]    Une action permet de charger dans la réunion le « compte-rendu global » signé numérisé ainsi que le document rassemblant l'ensemble des « compte-rendus par dossier » signés numérisé.

    #
    Depuis la page d'accueil  admin  admin

    ##
    ## REUNION 05
    ##
    &{reunion05} =  Create Dictionary
    ...  reunion_type=${reutype01.libelle}
    ...  date_reunion=14/07/2016
    ...  date_reunion_yyyy_mm_dd=2016-07-14
    Ajouter la réunion  ${reunion05}
    ${reunion05_code} =  Set Variable  ${reutype01.code}-${reunion05.date_reunion_yyyy_mm_dd}
    ${reunion05_libelle} =  Set Variable  ${reunion05.reunion_type} du ${reunion05.date_reunion}

    # L'action 'INTÉGRER DOCUMENTS SIGNÉS' ne doit pas être disponible
    Depuis le contexte de la réunion  ${reunion05_code}
    Portlet Action Should Not Be In Form    reunion    integrer-documents-numerises
    #
    Planifier directement le DC ou DI pour la réunion dans la catégorie  ${dc01_libelle}  ${reunion05_code}  Plans
    Planifier directement le DC ou DI pour la réunion dans la catégorie  ${dc02_libelle}  ${reunion05_code}  Plans
    Planifier directement le DC ou DI pour la réunion dans la catégorie  ${dc01_libelle}  ${reunion05_code}  Visites
    Planifier directement le DC ou DI pour la réunion dans la catégorie  ${dc01_libelle}  ${reunion05_code}  Dossiers à enjeux
    # L'action 'INTÉGRER DOCUMENTS SIGNÉS' ne doit pas être disponible
    Depuis le contexte de la réunion  ${reunion05_code}
    Portlet Action Should Not Be In Form    reunion    integrer-documents-numerises
    #
    Numéroter l'ordre du jour de la réunion  ${reunion05_code}
    # L'action 'INTÉGRER DOCUMENTS SIGNÉS' ne doit pas être disponible
    Depuis le contexte de la réunion  ${reunion05_code}
    Portlet Action Should Not Be In Form    reunion    integrer-documents-numerises
    #
    Rendre l'avis sur tous les dossiers de la réunion  ${reunion05_code}
    # L'action 'INTÉGRER DOCUMENTS SIGNÉS' ne doit pas être disponible
    Depuis le contexte de la réunion  ${reunion05_code}
    Portlet Action Should Not Be In Form    reunion    integrer-documents-numerises
    #
    Clôturer la réunion  ${reunion05_code}
    # L'action 'INTÉGRER DOCUMENTS SIGNÉS' doit être disponible
    Depuis le contexte de la réunion  ${reunion05_code}
    Portlet Action Should Be In Form    reunion    integrer-documents-numerises

    ##
    ##
    ##
    #
    &{reunion05_fichiers} =  Create Dictionary
    ...  om_fichier_reunion_cr_global_signe=numerisation1.pdf
    ...  om_fichier_reunion_cr_par_dossier_signe=numerisation2.pdf
    #
    Depuis l'interface de gestion des documents signés de la réunion  ${reunion05_code}
    Saisir les valeurs dans le formulaire de réunion  ${reunion05_fichiers}
    Click On Submit Button
    Valid Message Should Be  Vos modifications ont bien été enregistrées.
    La page ne doit pas contenir d'erreur

    ##
    ## Gestion des métadonnées
    ##
    ## Vérifie les métadonnées pour les champs fichier :
    ##
    ## - Compte rendu global numérisé signé [reunion.om_fichier_reunion_cr_global_signe]
    ##   > Téléversé
    ##   > Stockage à la finalisation de l'édition
    ##   > Mise à jour à chaque refinalisation de l'édition
    ##
    ## - Ensemble des comptes rendus individuels numérisés signés [reunion.om_fichier_reunion_cr_par_dossier_signe]
    ##   > Téléversé
    ##   > Stockage à la finalisation de l'édition
    ##   > Mise à jour à chaque refinalisation de l'édition
    ##

    # Récupération des éléments concernant les fichiers (uid).
    ${reunion05_fichiers_uid} =  Récupérer les uid des champs fichier de la réunion  ${reunion05_code}

    ## => [reunion.om_fichier_reunion_cr_global_signe] (METADATA FILESTORAGE)
    # Récupération des éléments concernant les fichiers (chemins) pour
    # effectuer les vérifications d'existence des fichiers et des métadonnées.
    ${reunion05_fichier_cr_global_signe_path} =  Récupérer le chemin vers le fichier correspondant à l'uid  ${reunion05_fichiers_uid.om_fichier_reunion_cr_global_signe}
    ${reunion05_fichier_cr_global_signe_md_path} =  Récupérer le chemin vers le fichier de métadonnées correspondant à l'uid  ${reunion05_fichiers_uid.om_fichier_reunion_cr_global_signe}
    Le fichier doit exister  ${reunion05_fichier_cr_global_signe_path}
    Le fichier doit exister  ${reunion05_fichier_cr_global_signe_md_path}
    # Composition du dictionnaire représentant les métadonnées qui doivent
    # apparaître dans le fichier de métadonnées.
    ${size}=  Get File Size  ${reunion05_fichier_cr_global_signe_path}
    ${md} =  Create Dictionary
    #...  filename=${reunion05_fichiers.om_fichier_reunion_cr_global_signe}
    ...  mimetype=application/pdf
    ...  size=${size}
    ...  titre=(${reunion05_code}) ${reunion05_libelle} - compte rendu global (signé)
    ...  description=compte rendu global de réunion numérisé signé
    ...  application=openARIA
    ...  origine=téléversé
    ...  code_reunion=${reunion05_code}
    ...  date_reunion=${reunion05.date_reunion_yyyy_mm_dd}
    ...  type_reunion=${reunion05.reunion_type}
    ...  commission=false
    Les métadonnées (clé/valeur) doivent être présentes dans le fichier  ${md}  ${reunion05_fichier_cr_global_signe_md_path}
    Les métadonnées (clé) ne doivent pas être présentes dans le fichier  ${md_no}  ${reunion05_fichier_cr_global_signe_md_path}

    ## => [reunion.om_fichier_reunion_cr_par_dossier_signe] (METADATA FILESTORAGE)
    # Récupération des éléments concernant les fichiers (chemins) pour
    # effectuer les vérifications d'existence des fichiers et des métadonnées.
    ${reunion05_fichier_cr_par_dossier_signe_path} =  Récupérer le chemin vers le fichier correspondant à l'uid  ${reunion05_fichiers_uid.om_fichier_reunion_cr_par_dossier_signe}
    ${reunion05_fichier_cr_par_dossier_signe_md_path} =  Récupérer le chemin vers le fichier de métadonnées correspondant à l'uid  ${reunion05_fichiers_uid.om_fichier_reunion_cr_par_dossier_signe}
    Le fichier doit exister  ${reunion05_fichier_cr_par_dossier_signe_path}
    Le fichier doit exister  ${reunion05_fichier_cr_par_dossier_signe_md_path}
    # Composition du dictionnaire représentant les métadonnées qui doivent
    # apparaître dans le fichier de métadonnées.
    ${size}=  Get File Size  ${reunion05_fichier_cr_par_dossier_signe_path}
    ${md} =  Create Dictionary
    #...  filename=${reunion05_fichiers.om_fichier_reunion_cr_par_dossier_signe}
    ...  mimetype=application/pdf
    ...  size=${size}
    ...  titre=(${reunion05_code}) ${reunion05_libelle} - compte rendu par dossier (signé)
    ...  description=ensemble des comptes rendus de réunion individuels numérisés signés
    ...  application=openARIA
    ...  origine=téléversé
    ...  code_reunion=${reunion05_code}
    ...  date_reunion=${reunion05.date_reunion_yyyy_mm_dd}
    ...  type_reunion=${reunion05.reunion_type}
    ...  commission=false
    Les métadonnées (clé/valeur) doivent être présentes dans le fichier  ${md}  ${reunion05_fichier_cr_par_dossier_signe_md_path}
    Les métadonnées (clé) ne doivent pas être présentes dans le fichier  ${md_no}  ${reunion05_fichier_cr_par_dossier_signe_md_path}


