*** Settings ***
Resource  resources/resources.robot
Suite Setup  For Suite Setup
Suite Teardown  For Suite Teardown
Documentation  Les unités d'accessibilité (UA) permettent de découper
...  les établissements en plus petites unités au sens de l'accessibilité
...  (différent d'une unité au sens de la sécurité par définition).
...  Ces unités ont vocation à stocker les données techniques particulières
...  à cette unité au sein de l'établissement.


*** Test Cases ***
Constitution d'un jeu de données

    [Documentation]  L'objet de ce 'Test Case' est de constituer un jeu de
    ...  données cohérent pour les scénarios fonctionnels qui suivent.

    #
    Depuis la page d'accueil    admin    admin
    #
    &{etab01} =  Create Dictionary
    ...  libelle=DIA
    ...  etablissement_nature=ERP Référentiel
    ...  etablissement_type=M
    ...  etablissement_categorie=1
    ...  si_autorite_competente_visite=Commission communale de sécurité
    ...  adresse_numero=41
    ...  adresse_voie=RUE DE ROME
    ...  adresse_cp=13006
    ...  adresse_ville=MARSEILLE
    ...  adresse_arrondissement=6ème
    ...  siret=73282932000074
    ...  exp_civilite=Mme
    ...  exp_nom=Leclerc
    ...  exp_prenom=Tatiana
    ${etab01_code} =  Ajouter l'établissement  ${etab01}
    ${etab01_titre} =  Set Variable  ${etab01_code} - ${etab01.libelle}
    Set Suite Variable  ${etab01}
    Set Suite Variable  ${etab01_code}
    Set Suite Variable  ${etab01_titre}
    #
    &{etab02} =  Create Dictionary
    ...  libelle=ALDI
    ...  etablissement_nature=ERP Référentiel
    ...  etablissement_type=M
    ...  etablissement_categorie=1
    ...  si_autorite_competente_visite=Commission communale de sécurité
    ...  adresse_numero=42
    ...  adresse_voie=RUE DE ROME
    ...  adresse_cp=13006
    ...  adresse_ville=MARSEILLE
    ...  adresse_arrondissement=6ème
    ...  siret=73282932000074
    ...  exp_civilite=Mme
    ...  exp_nom=Leclerc
    ...  exp_prenom=Tatiana
    ${etab02_code} =  Ajouter l'établissement  ${etab02}
    ${etab02_titre} =  Set Variable  ${etab02_code} - ${etab02.libelle}
    Set Suite Variable  ${etab02}
    Set Suite Variable  ${etab02_code}
    Set Suite Variable  ${etab02_titre}

    #
    ${ua_1_libelle} =    Set Variable    Parking
    Set Suite Variable    ${ua_1_libelle}
    Ajouter l'UA depuis le contexte d'un établissement    ${etab01_code}    ${etab01.libelle}    ${ua_1_libelle}
    ${ua_2_libelle} =    Set Variable    Chambre froide
    Set Suite Variable    ${ua_2_libelle}
    Ajouter l'UA depuis le contexte d'un établissement    ${etab02_code}    ${etab02.libelle}    ${ua_2_libelle}

    #
    &{dc01} =  Create Dictionary
    ...  dossier_coordination_type=Autorisation de Travaux
    ...  description=Description
    ...  etablissement=${etab01_titre}
    ...  date_demande=09/11/2014
    ...  a_qualifier=false
    ...  dossier_instruction_secu=true
    ...  dossier_instruction_acc=true
    ${dc01_libelle} =  Ajouter le dossier de coordination  ${dc01}
    Set Suite Variable    ${dc01}
    Set Suite Variable    ${dc01_libelle}
    ${di1} =    Catenate    SEPARATOR=-    ${dc01_libelle}    ACC
    Set Suite Variable    ${di1}


    # NE PAS UTILISER CET ETABLISSEMENT POUR CREER DES UA
    # Il est utilsé pour tester la création automatique des UA
    &{etab03} =  Create Dictionary
    ...  libelle=MUSÉE DU 6
    ...  etablissement_nature=ERP Référentiel
    ...  etablissement_type=M
    ...  etablissement_categorie=1
    ...  si_autorite_competente_visite=Commission communale de sécurité
    ...  adresse_numero=42
    ...  adresse_voie=RUE DE ROME
    ...  adresse_cp=13006
    ...  adresse_ville=MARSEILLE
    ...  adresse_arrondissement=6ème
    ...  siret=73282932000074
    ...  exp_civilite=Mme
    ...  exp_nom=Létourneau
    ...  exp_prenom=Melisande
    ${etab03_code} =  Ajouter l'établissement  ${etab03}
    ${etab03_titre} =  Set Variable  ${etab03_code} - ${etab03.libelle}
    Set Suite Variable  ${etab03}
    Set Suite Variable  ${etab03_code}
    Set Suite Variable  ${etab03_titre}

    #
    &{dc02} =  Create Dictionary
    ...  dossier_coordination_type=Permis de construire
    ...  description=Dossier de coordination concernant un permis de construire
    ...  etablissement=${etab03_titre}
    ...  date_butoir=${DATE_FORMAT_DD/MM/YYYY}
    ${dc02_libelle} =  Ajouter le dossier de coordination  ${dc02}
    Set Suite Variable  ${dc02}
    Set Suite Variable  ${dc02_libelle}
    #
    &{dc03} =  Create Dictionary
    ...  dossier_coordination_type=Permis de construire
    ...  description=Dossier de coordination concernant un permis de construire
    ...  etablissement=${etab03_titre}
    ...  date_butoir=${DATE_FORMAT_DD/MM/YYYY}
    ${dc03_libelle} =  Ajouter le dossier de coordination  ${dc03}
    Set Suite Variable  ${dc03}
    Set Suite Variable  ${dc03_libelle}
    #
    &{dc04} =  Create Dictionary
    ...  dossier_coordination_type=Permis de construire
    ...  description=Dossier de coordination concernant un permis de construire
    ...  etablissement=${etab03_titre}
    ...  date_butoir=${DATE_FORMAT_DD/MM/YYYY}
    ${dc04_libelle} =  Ajouter le dossier de coordination  ${dc04}
    Set Suite Variable  ${dc04}
    Set Suite Variable  ${dc04_libelle}

    #
    &{etab04} =  Create Dictionary
    ...  libelle=LIDL
    ...  etablissement_nature=ERP Référentiel
    ...  etablissement_type=M
    ...  etablissement_categorie=1
    ...  si_autorite_competente_visite=Commission communale de sécurité
    ...  adresse_numero=42
    ...  adresse_voie=RUE DE ROME
    ...  adresse_cp=13006
    ...  adresse_ville=MARSEILLE
    ...  adresse_arrondissement=6ème
    ...  siret=73282932000074
    ...  exp_civilite=Mme
    ...  exp_nom=Charrette
    ...  exp_prenom=Bruce
    ${etab04_code} =  Ajouter l'établissement  ${etab04}
    ${etab04_titre} =  Set Variable  ${etab04_code} - ${etab04.libelle}
    Set Suite Variable  ${etab04}
    Set Suite Variable  ${etab04_code}
    Set Suite Variable  ${etab04_titre}

    ${ua_4_1_libelle} =    Set Variable    Auditif
    Set Suite Variable    ${ua_4_1_libelle}
    Ajouter l'unité d'accessibilité depuis le contexte d'un établissement    ${etab04_code}    ${etab04.libelle}    ${ua_4_1_libelle}  t
    ${ua_4_2_libelle} =    Set Variable    Auditif, Mental
    Set Suite Variable    ${ua_4_2_libelle}
    Ajouter l'unité d'accessibilité depuis le contexte d'un établissement    ${etab04_code}    ${etab04.libelle}    ${ua_4_2_libelle}  t  t
    ${ua_4_3_libelle} =    Set Variable    Auditif, Mental, Non Physique et avec SCDA
    ${ua_4_3_libelle_new} =    Set Variable    Auditif, Mental, Non Physique et sans SCDA
    Set Suite Variable    ${ua_4_3_libelle}
    Set Suite Variable    ${ua_4_3_libelle_new}
    ${scda_libelle} =    Set Variable    Dérotype
    Ajouter une dérogation SCDA depuis le menu  dero  ${scda_libelle}
    Ajouter l'unité d'accessibilité depuis le contexte d'un établissement    ${etab04_code}    ${etab04.libelle}    ${ua_4_3_libelle}  t  t  f  aucun    null    aucun    aucun    aucun    aucun    null    null    aucun    ${scda_libelle}
    Set Suite Variable    ${scda_libelle}


Vérification de l'intégration des unités d'accessibilité

    [Documentation]   L'objet de ce 'Test Case' est de vérifier l'intégration
    ...    de la fonctionnalité, c'est-à-dire les entrées de menu, les titres
    ...    de page, le contenu des listings, les actions disponibles, ...
    ...    L'intégration des unités d'accessibilité se fait par les points
    ...    suivants :
    ...     - une entrée de menu spécifique permettant de lister toutes les UA
    ...     - aucun ajout n'est possible depuis ce listing
    ...     - un onglet UA est accessible depuis un établissement
    ...     - ce listing présente toutes les UA de l'établissement
    ...     - il est ici possible d'ajouter de nouvelles UA

    # Définition des variables du 'Test Case'
    ${onglet_libelle} =    Set Variable    UA


    #
    Depuis la page d'accueil    admin    admin

    #
    # Cas n°1 - Entrée de menu UA
    #

    # Dans la rubrique "Établissements", une rentrée de menu "Unités d'accessibilité" permet d'accéder au listing principal des UA.
    Go To Submenu In Menu    etablissements    etablissement_unite
    # L'entrée de menu reste sélectionnée lorsque l'on se trouve sur ce listing.
    Submenu In Menu Should Be Selected    etablissements    etablissement_unite
    # Le titre de l'écran est "Établissements -> Unités D'accessibilité"
    Page Title Should Be    Établissements > Unités D’accessibilité
    # La page contient un onglet dont le titre est "UA"
    First Tab Title Should Be    ${onglet_libelle}
    # L'onglet contient un bloc "Recherche avancée"
    Element Should Contain    css=#tab-etablissement_unite    Recherche avancée
    # L'onglet contient un bloc "Listing des UA"
    # On vérifie que dans la page se trouve la chaîne suivante qui se trouve toujours en haut d'un listing
    Element Should Contain    css=#tab-etablissement_unite    enregistrement(s) sur
    # Pas d'action 'ajouter' sur ce listing
    L'action ajouter ne doit pas être disponible
    # Un clic sur chaque ligne du listing permet d'accéder à la fiche de visualisation d'une UA.
    Click On Link  ${ua_1_libelle}
    # Le titre de l'écran « Visualisation d'une UA » est le suivant
    Page Title Should Be    Établissements > Unités D’accessibilité > ${ua_1_libelle} [${etab01_titre}]
    # La page contient un onglet dont le titre est "UA"
    First Tab Title Should Be    ${onglet_libelle}


    ## LISTING
    Depuis le listing des UA

    # Vérification des colonnes du listing
    Element Should Contain    css=#tab-etablissement_unite table thead tr th.col-1    libellé
    Element Should Contain    css=#tab-etablissement_unite table thead tr th.col-2    établissement
    Element Should Contain    css=#tab-etablissement_unite table thead tr th.col-3    adresse
    Element Should Contain    css=#tab-etablissement_unite table thead tr th.col-4    accessible
    Element Should Contain    css=#tab-etablissement_unite table thead tr th.col-5.lastcol    état

    # Vérification de la recherche avancée
    Element Should Contain    css=#tab-etablissement_unite #adv-search-adv-fields div.field-type-text #lib-libelle    libellé
    Element Should Contain    css=#tab-etablissement_unite #adv-search-adv-fields div.field-type-text #lib-etablissement    établissement
    Element Should Contain    css=#tab-etablissement_unite #adv-search-adv-fields div.field-type-text #lib-adresse_numero    numéro
    Element Should Contain    css=#tab-etablissement_unite #adv-search-adv-fields div.field-type-text #lib-adresse_voie    voie
    Element Should Contain    css=#tab-etablissement_unite #adv-search-adv-fields div.field-type-select #lib-adresse_arrondissement    arrondissement
    Element Should Contain    css=#tab-etablissement_unite #adv-search-adv-fields div.field-type-select #lib-etat    état
    Element Should Contain    css=#tab-etablissement_unite #adv-search-adv-fields div.field-type-select #lib-acc_handicap_auditif    accessible auditif
    Element Should Contain    css=#tab-etablissement_unite #adv-search-adv-fields div.field-type-select #lib-acc_handicap_mental    accessible mental
    Element Should Contain    css=#tab-etablissement_unite #adv-search-adv-fields div.field-type-select #lib-acc_handicap_physique    accessible physique
    Element Should Contain    css=#tab-etablissement_unite #adv-search-adv-fields div.field-type-select #lib-acc_handicap_visuel    accessible visuel


    #
    # Cas n°2 - Intégration des UA dans la fiche de l'établissement
    #

    # Il existe trois UA pour tester les valeurs des champs ACC :
    # l'objectif est de vérifier que le non prime sur le null
    # qui lui-même prime sur le oui. Une UA dispose d'une dérogation.

    Depuis le contexte de l'établissement  ${etab04_code}
    Open Fieldset  etablissement_tous  details
    Element Text Should Be  acc_handicap_auditif  Oui
    Element Text Should Be  acc_handicap_mental  ${empty}
    Element Text Should Be  acc_handicap_physique  Non
    Element Text Should Be  acc_handicap_visuel  ${empty}
    Element Text Should Be  acc_derogation_scda  Oui

    Click On Form Portlet Action  etablissement_tous  modifier
    Open Fieldset  etablissement_tous  details
    Element Should Not Be Visible  tot_ua_valid
    Element Should Not Be Visible  acc_handicap_auditif
    Element Should Not Be Visible  acc_handicap_mental
    Element Should Not Be Visible  acc_handicap_physique
    Element Should Not Be Visible  acc_handicap_visuel
    Element Should Not Be Visible  acc_derogation_scda
    Click On Back Button

    # On supprime la dérogation
    Depuis le contexte de l'établissement    ${etab04_code}
    On clique sur l'onglet    etablissement_unite    UA
    Click On Link  ${ua_4_3_libelle}
    Click On SubForm Portlet Action  etablissement_unite__contexte_etab__ua_valide  modifier
    Saisir l'unité d'accessibilité  ${ua_4_3_libelle_new}  aucun  aucun  aucun  aucun  null  aucun  aucun  aucun  aucun  null  null  aucun  Choisir Dérogation SCDA
    CLick On Submit Button In Subform
    Depuis le contexte de l'établissement  ${etab04_code}
    WUX  Click Element  css=legend.collapsed
    Element Text Should Be  acc_derogation_scda  ${empty}

    # On enlève la dérogation à l'UA concernée pour vérifier
    # que la fiche établissement en tienne compte

    #
    # Cas n°3 - Onglet UA depuis le contexte de l'établissement
    #

    #
    Depuis le contexte de l'établissement    ${etab01_code}    ${etab01.libelle}
    On clique sur l'onglet    etablissement_unite    ${onglet_libelle}

    ## LISTING 'UA validées' sur l'établissement
    Depuis le listing des 'UA validées' sur l'établissement    ${etab01_code}    ${etab01.libelle}

    # On vérifie la présence des colonnes dans le listing
    Element Should Contain    css=#sousform-etablissement_unite__contexte_etab__ua_valide table thead tr th.col-1    libellé
    Element Should Contain    css=#sousform-etablissement_unite__contexte_etab__ua_valide table thead tr th.col-2    acc. auditif
    Element Should Contain    css=#sousform-etablissement_unite__contexte_etab__ua_valide table thead tr th.col-3    acc. mental
    Element Should Contain    css=#sousform-etablissement_unite__contexte_etab__ua_valide table thead tr th.col-4    acc. physique
    Element Should Contain    css=#sousform-etablissement_unite__contexte_etab__ua_valide table thead tr th.col-5    acc. visuel
    Element Should Contain    css=#sousform-etablissement_unite__contexte_etab__ua_valide table thead tr th.col-6.lastcol    dérogation

    # Action 'ajouter' sur ce listing
    Page Should Contain Element    css=#sousform-etablissement_unite__contexte_etab__ua_valide span.add-16


    ## LISTING 'UA en projet' sur l'établissement
    Depuis le listing des 'UA en projet' sur l'établissement    ${etab01_code}    ${etab01.libelle}

    # On vérifie la présence des colonnes dans le listing
    Element Should Contain    css=#sousform-etablissement_unite__contexte_etab__ua_enprojet table thead tr th.col-1    libellé
    Element Should Contain    css=#sousform-etablissement_unite__contexte_etab__ua_enprojet table thead tr th.col-2    acc. auditif
    Element Should Contain    css=#sousform-etablissement_unite__contexte_etab__ua_enprojet table thead tr th.col-3    acc. mental
    Element Should Contain    css=#sousform-etablissement_unite__contexte_etab__ua_enprojet table thead tr th.col-4    acc. physique
    Element Should Contain    css=#sousform-etablissement_unite__contexte_etab__ua_enprojet table thead tr th.col-5    acc. visuel
    Element Should Contain    css=#sousform-etablissement_unite__contexte_etab__ua_enprojet table thead tr th.col-6.lastcol    dérogation

    # Pas d'action 'ajouter' sur ce listing
    Page Should Not Contain Element    css=#sousform-etablissement_unite__contexte_etab__ua_enprojet span.add-16


    ## LISTING 'UA archivées' sur l'établissement
    Depuis le listing des 'UA archivées' sur l'établissement    ${etab01_code}    ${etab01.libelle}

    # On vérifie la présence des colonnes dans le listing
    Element Should Contain    css=#sousform-etablissement_unite__contexte_etab__ua_archive table thead tr th.col-1    libellé
    Element Should Contain    css=#sousform-etablissement_unite__contexte_etab__ua_archive table thead tr th.col-2    acc. auditif
    Element Should Contain    css=#sousform-etablissement_unite__contexte_etab__ua_archive table thead tr th.col-3    acc. mental
    Element Should Contain    css=#sousform-etablissement_unite__contexte_etab__ua_archive table thead tr th.col-4    acc. physique
    Element Should Contain    css=#sousform-etablissement_unite__contexte_etab__ua_archive table thead tr th.col-5    acc. visuel
    Element Should Contain    css=#sousform-etablissement_unite__contexte_etab__ua_archive table thead tr th.col-6    état
    Element Should Contain    css=#sousform-etablissement_unite__contexte_etab__ua_archive table thead tr th.col-7.lastcol    dérogation

    # Pas d'action 'ajouter' sur ce listing
    Page Should Not Contain Element    css=#sousform-etablissement_unite__contexte_etab__ua_archive span.add-16

    #
    # Cas n°4 - Gestion des UA dans l'analyse du DI
    #

    ## LISTING 'UA analysées' sur le dossier d'instruction
    Depuis le listing des 'UA analysées' sur le dossier d'instruction    ${di1}

    # On vérifie la présence des colonnes dans le listing
    Element Should Contain    css=#sousform-etablissement_unite__contexte_di_analyse__ua_en_analyse table thead tr th.col-1    libellé
    Element Should Contain    css=#sousform-etablissement_unite__contexte_di_analyse__ua_en_analyse table thead tr th.col-2    état
    Element Should Contain    css=#sousform-etablissement_unite__contexte_di_analyse__ua_en_analyse table thead tr th.col-3.lastcol    archive


    # Action 'ajouter' sur ce listing
    Page Should Contain Element    css=#sousform-etablissement_unite__contexte_di_analyse__ua_en_analyse span.add-16

    ## LISTING 'UA validées sur l'établissement' sur le dossier d'instruction
    Depuis le listing des 'UA validées sur l'établissement' sur le dossier d'instruction    ${di1}

    # On vérifie la présence des colonnes dans le listing
    Element Should Contain    css=#sousform-etablissement_unite__contexte_di_analyse__ua_valide_sur_etab table thead tr th.col-1    libellé
    Element Should Contain    css=#sousform-etablissement_unite__contexte_di_analyse__ua_valide_sur_etab table thead tr th.col-2    acc. auditif
    Element Should Contain    css=#sousform-etablissement_unite__contexte_di_analyse__ua_valide_sur_etab table thead tr th.col-3    acc. mental
    Element Should Contain    css=#sousform-etablissement_unite__contexte_di_analyse__ua_valide_sur_etab table thead tr th.col-4    acc. physique
    Element Should Contain    css=#sousform-etablissement_unite__contexte_di_analyse__ua_valide_sur_etab table thead tr th.col-5    acc. visuel
    Element Should Contain    css=#sousform-etablissement_unite__contexte_di_analyse__ua_valide_sur_etab table thead tr th.col-6.lastcol    dérogation

    # Pas d'action 'ajouter' sur ce listing
    Page Should Not Contain Element    css=#sousform-etablissement_unite__contexte_di_analyse__ua_valide_sur_etab span.add-16


Vérification de l'ajout d'une UA dans le contexte de l'établissement

    [Documentation]    Une unité d'accessiblité doit être rattachée à
    ...    l'établissement du contexte, dans l'état validé et non archivée.

    #
    Ajouter l'UA depuis le contexte d'un établissement    ${etab01_code}    ${etab01.libelle}    Bâtiment 247
    #
    Ajouter l'UA depuis le contexte d'un établissement    ${etab02_code}    ${etab02.libelle}    Bâtiment 248
    #
    Depuis la fiche de l'UA    ${etab01_code}    ${etab01.libelle}    Bâtiment 247
    Element Should Contain    css=#libelle    Bâtiment 247
    Element Should Contain    css=#link_etablissement    ${etab01_titre}
    Form Static Value Should Be    css=#etat    validé
    Form Static Value Should Be    css=#archive    Non
    #
    Depuis la fiche de l'UA    ${etab02_code}    ${etab02.libelle}    Bâtiment 248
    Element Should Contain    css=#libelle    Bâtiment 248
    Element Should Contain    css=#link_etablissement    ${etab02_titre}
    Form Static Value Should Be    etat    validé
    Form Static Value Should Be    archive    Non


Vérification de l'ajout d'une UA dans le contexte d'un dossier d'instruction

    [Documentation]    Une unité d'accessiblité doit être rattachée au dossier
    ...    d'instruction du contexte, à l'établissement lié au di, dans l'état
    ...    en projet et non archivée.

    #
    Ajouter l'UA depuis le contexte d'un dossier d'instruction    ${di1}    Bâtiment 249
    #
    Depuis la fiche de l'UA    ${etab01_code}    ${etab01.libelle}    Bâtiment 249
    Element Should Contain    css=#libelle    Bâtiment 249
    Element Should Contain    css=#link_etablissement    ${etab01_titre}
    Element Should Contain    css=#link_dossier_instruction    ${di1}
    Form Static Value Should Be    css=#etat    en projet
    Form Static Value Should Be    css=#archive    Non


Vérification de l'ajout automatique d'une unité d'accessibilité

    [Documentation]    Une unité d'accessiblité doit s'ajouter automatiquement
    ...    lors de l'ajout d'un dossier d'instruction sur un établissement qui
    ...    n'en possède pas déjà.

    #
    # On ajoute un DI SECU pour vérifier que l'UA n'est pas créée
    #

    #
    Depuis la page d'accueil    admin    admin
    #
    Depuis le formulaire de modification du dossier de coordination    ${dc02_libelle}
    # On décoche "a_qualifier"
    Unselect Checkbox    css=#a_qualifier
    # On découche "dossier_instruction_acc"
    Unselect Checkbox    css=#dossier_instruction_acc
    # On valide le formulaire
    Click On Submit Button
    # On vérifie le message affiché à l'utilisateur
    Valid Message Should Contain    Vos modifications ont bien été enregistrées.
    # On vérifie qu'il n'y a pas d'unité d'accessibilité créée
    Element Should Not Contain    css=div.message.ui-state-valid p span.text    L'unité d'accessibilité
    Depuis le listing des UA
    # On fait une recherche sur le libellé de l'établissement
    Input Text    css=div#adv-search-adv-fields input#etablissement    ${etab03.libelle}
    Click On Search Button
    WUX  Total Results Should Be Equal    0

    #
    # On ajoute un DI ACC pour vérifier que l'UA est créée
    #

    #
    Depuis le formulaire de modification du dossier de coordination    ${dc02_libelle}
    # On vérifie la case à cocher des DI
    Select Checkbox    css=#dossier_instruction_acc
    # On valide le formulaire
    Click On Submit Button
    # On vérifie le message affiché à l'utilisateur
    Valid Message Should Contain    Vos modifications ont bien été enregistrées.
    Depuis le listing des UA
    # On fait une recherche sur le libellé de l'établissement
    Input Text    css=div#adv-search-adv-fields input#etablissement    ${etab03.libelle}
    Click On Search Button
    WUX  Total Results Should Be Equal    1

    #
    # On ajoute un autre DI ACC pour vérifier que l'UA est créée
    #

    #
    Depuis le formulaire de modification du dossier de coordination    ${dc03_libelle}
    # On décoche "a_qualifier"
    Unselect Checkbox    css=#a_qualifier
    # On vérifie la case à cocher des DI
    Select Checkbox    css=#dossier_instruction_acc
    # On valide le formulaire
    Click On Submit Button
    # On vérifie le message affiché à l'utilisateur
    Valid Message Should Contain    Vos modifications ont bien été enregistrées.
    Depuis le listing des UA
    # On fait une recherche sur le libellé de l'établissement
    Input Text    css=div#adv-search-adv-fields input#etablissement    ${etab03.libelle}
    Click On Search Button
    WUX  Total Results Should Be Equal    2

    #
    # On ajoute une UA valide sur l'établissement
    #
    Ajouter l'unité d'accessibilité depuis le contexte d'un établissement    ${etab03_code}    ${etab03.libelle}    UA Valide pour ${etab03.libelle}

    #
    # On ajoute encore un DI ACC pour vérifier que l'UA est n'est pas créée car
    # il y a une UA validée sur cet établissement
    #

    #
    Depuis le formulaire de modification du dossier de coordination    ${dc04_libelle}
    # On décoche "a_qualifier"
    Unselect Checkbox    css=#a_qualifier
    # On vérifie la case à cocher des DI
    Select Checkbox    css=#dossier_instruction_acc
    # On valide le formulaire
    Click On Submit Button
    # On vérifie le message affiché à l'utilisateur
    Valid Message Should Contain    Vos modifications ont bien été enregistrées.
    Element Should Not Contain    css=div.message.ui-state-valid p span.text    L'unité d'accessibilité
    Depuis le listing des UA
    # On fait une recherche sur le libellé de l'établissement
    Input Text    css=div#adv-search-adv-fields input#etablissement    ${etab03.libelle}
    Click On Search Button
    WUX  Total Results Should Be Equal    3


Vérification de la sélection d'une UA validée pour en réaliser l'analyse

    [Documentation]    ...
    Depuis la page d'accueil    admin    admin

    # On cré un établissement
    &{etab05} =  Create Dictionary
    ...  libelle=Établissement 1234
    ...  etablissement_nature=ERP Référentiel
    ...  etablissement_type=M
    ...  etablissement_categorie=1
    ...  si_autorite_competente_visite=Commission communale de sécurité
    ...  adresse_numero=60
    ...  adresse_voie=RUE DE ROME
    ...  adresse_cp=13006
    ...  adresse_ville=MARSEILLE
    ...  adresse_arrondissement=6ème
    ...  siret=73282932000074
    ...  exp_civilite=Mme
    ...  exp_nom=Leclerc
    ...  exp_prenom=Tatiana
    ${etab05_code} =  Ajouter l'établissement  ${etab05}
    ${etab05_titre} =  Set Variable  ${etab05_code} - ${etab05.libelle}
    Set Suite Variable  ${etab05}
    Set Suite Variable  ${etab05_code}
    Set Suite Variable  ${etab05_titre}

    # On cré une UA validée sur l'établissement
    ${ua_libelle} =    Set Variable    Bâtiment 1234
    Ajouter l'UA depuis le contexte d'un établissement    ${etab05_code}    ${etab05.libelle}    ${ua_libelle}

    # On cré un DI sur l'établissement
    &{dc05} =  Create Dictionary
    ...  dossier_coordination_type=Autorisation de Travaux
    ...  description=Description
    ...  etablissement=${etab05_titre}
    ...  date_demande=09/11/2014
    ...  a_qualifier=false
    ...  dossier_instruction_secu=true
    ...  dossier_instruction_acc=true
    ${dc05_libelle} =  Ajouter le dossier de coordination  ${dc05}
    Set Suite Variable  ${dc05}
    Set Suite Variable  ${dc05_libelle}
    ${di} =    Catenate    SEPARATOR=-    ${dc05_libelle}    ACC

    Depuis la page d'accueil    technicien-acc    technicien-acc

    # On analyse l'UA existante validée
    Analyser l'UA validée    ${di}    ${ua_libelle}

    # On vérifie que l'UA en projet a été créée dans l'onglet UA analysée
    Depuis la fiche de l'UA dans le contexte des 'UA analysées' sur le dossier d'instruction    ${di}    ${ua_libelle}

    # On vérifie que l'action analyser n'est plus disponible sur l'UA validée
    Depuis la fiche de l'UA dans le contexte des 'UA validées sur l'établissement' sur le dossier d'instruction    ${di}    ${ua_libelle}
    Element Should Not Be Visible    css=#action-sousform-etablissement_unite__contexte_di_analyse__ua_valide_sur_etab-analyser

    # On valide l'UA en projet
    Valider l'UA en projet    ${etab05_code}    ${etab05.libelle}    ${ua_libelle}

    # On vérifie l'UA validée
    Depuis la fiche de l'UA dans le contexte des 'UA validées' sur l'établissement    ${etab05_code}    ${etab05.libelle}    ${ua_libelle}
    Element Should Contain    css=#link_etablissement_unite_lie    ${ua_libelle}

    # On vérifie que l'UA initiale se trouve dans les UA archivées
    Depuis la fiche de l'UA dans le contexte des 'UA archivées' sur l'établissement    ${etab05_code}    ${etab05.libelle}    ${ua_libelle}

    Depuis la page d'accueil    cadre-acc    cadre-acc

    # On repasse en projet l'UA récemment validée
    Repasser en projet l'UA validée    ${etab05_code}    ${etab05.libelle}    ${ua_libelle}

    # On vérifie que l'UA initiale se trouve dans les UA validées
    Depuis la fiche de l'UA dans le contexte des 'UA validées' sur l'établissement    ${etab05_code}    ${etab05.libelle}    ${ua_libelle}
    Form Value Should Be    css=#etablissement_unite_lie    ${EMPTY}


Champs de fusion dans les éditions PDF

    [Documentation]    Permet de vérifier que les champs de fusion
    ...  [etablissement.ua_validees] et [analyse.ua_analysees]
    ...  sont bien remplacées dans les éditions PDF.

    #
    Depuis la page d'accueil    admin    admin

    # On cré un établissement
    &{etab06} =  Create Dictionary
    ...  libelle=Établissement 3458
    ...  etablissement_nature=ERP Référentiel
    ...  etablissement_type=M
    ...  etablissement_categorie=1
    ...  si_autorite_competente_visite=Commission communale de sécurité
    ...  adresse_numero=54
    ...  adresse_voie=RUE DE ROME
    ...  adresse_cp=13006
    ...  adresse_ville=MARSEILLE
    ...  adresse_arrondissement=6ème
    ...  siret=73282932000074
    ...  exp_civilite=Mme
    ...  exp_nom=Leclerc
    ...  exp_prenom=Tatiana
    ${etab06_code} =  Ajouter l'établissement  ${etab06}
    ${etab06_titre} =  Set Variable  ${etab06_code} - ${etab06.libelle}
    Set Suite Variable  ${etab06}
    Set Suite Variable  ${etab06_code}
    Set Suite Variable  ${etab06_titre}

    # On cré une UA validée sur l'établissement
    ${ua_validee1_libelle} =    Set Variable    test_ua_champfusion_valide1_libelle
    ${ua_validee1_description} =  Set Variable  test_ua_champfusion_valide1_description
    Ajouter l'UA depuis le contexte d'un établissement    ${etab06_code}    ${etab06.libelle}    ${ua_validee1_libelle}  aucun  aucun  aucun  aucun    null    aucun    aucun    aucun    aucun    null    null    aucun    null    null    ${ua_validee1_description}

    # On cré une UA validée sur l'établissement
    ${ua_validee2_libelle} =    Set Variable    test_ua_champfusion_valide2_libelle
    ${ua_validee2_description} =  Set Variable  test_ua_champfusion_valide2_description
    Ajouter l'UA depuis le contexte d'un établissement    ${etab06_code}    ${etab06.libelle}    ${ua_validee2_libelle}  aucun  aucun  aucun  aucun    null    aucun    aucun    aucun    aucun    null    null    aucun    null    null    ${ua_validee2_description}

    # On cré une UA archivée sur l'établissement
    ${ua_archivee_libelle} =    Set Variable    test_ua_champfusion_archive_libelle
    ${ua_archivee_description} =  Set Variable  test_ua_champfusion_archive_description
    Ajouter l'UA depuis le contexte d'un établissement    ${etab06_code}    ${etab06.libelle}    ${ua_archivee_libelle}  aucun  aucun  aucun  aucun    null    aucun    aucun    aucun    aucun    null    null    aucun    null    null    ${ua_archivee_description}
    Archiver l'UA validée    ${etab06_code}    ${etab06.libelle}    ${ua_archivee_libelle}

    # On cré un DI sur l'établissement
    &{dc06} =  Create Dictionary
    ...  dossier_coordination_type=Autorisation de Travaux
    ...  description=Description
    ...  etablissement=${etab06_titre}
    ...  date_demande=09/11/2014
    ...  a_qualifier=false
    ...  dossier_instruction_secu=true
    ...  dossier_instruction_acc=true
    ${dc06_libelle} =  Ajouter le dossier de coordination  ${dc06}
    Set Suite Variable  ${dc06}
    Set Suite Variable  ${dc06_libelle}
    ${di} =    Catenate    SEPARATOR=-    ${dc06_libelle}    ACC

    # Vérification du champ de fusion [etablissement.ua_validees] dans le compte rendu d'analyse
    # On accède à l'analyse SI du DI
    Accéder à l'analyse du dossier d'instruction    ${di}
    # On édite le compte-rendu SI
    Cliquer action analyse    compte_rendu_analyse
    # On ouvre le PDF
    Open PDF  ${OM_PDF_TITLE}
    #
    PDF Page Number Should Contain  2  ${ua_validee1_libelle}
    PDF Page Number Should Contain  2  ${ua_validee1_description}
    PDF Page Number Should Contain  2  ${ua_validee2_libelle}
    PDF Page Number Should Contain  2  ${ua_validee2_description}
    PDF Page Number Should Not Contain  2  ${ua_archivee_libelle}
    PDF Page Number Should Not Contain  2  ${ua_archivee_description}
    # On revient à la fenêtre principale
    Close PDF

    # On cré une UA en projet sur le DI
    ${ua_enprojet1_libelle} =    Set Variable    test_ua_champfusion_enprojet1_libelle
    Ajouter l'UA depuis le contexte d'un dossier d'instruction    ${di}    ${ua_enprojet1_libelle}

    # On cré une UA en projet sur le DI
    ${ua_enprojet2_libelle} =    Set Variable    test_ua_champfusion_enprojet2_libelle
    Ajouter l'UA depuis le contexte d'un dossier d'instruction    ${di}    ${ua_enprojet2_libelle}

    # Vérification du champ de fusion [analyse.ua_analysees] dans le compte rendu d'analyse
    # On accède à l'analyse SI du DI
    Accéder à l'analyse du dossier d'instruction    ${di}
    # On édite le compte-rendu SI
    Cliquer action analyse    compte_rendu_analyse
    # On ouvre le PDF
    Open PDF  ${OM_PDF_TITLE}
    #
    PDF Page Number Should Contain  2  ${ua_enprojet1_libelle}
    #Page Should Contain    ${ua_enprojet1_description}
    PDF Page Number Should Contain  2  ${ua_enprojet2_libelle}
    #Page Should Contain    ${ua_enprojet2_description}
    # On revient à la fenêtre principale
    Close PDF


Matrice des permissions

    [Documentation]    ...

    #
    Depuis la page d'accueil    admin    admin

    # On cré un établissement
    &{etab07} =  Create Dictionary
    ...  libelle=Établissement 3459
    ...  etablissement_nature=ERP Référentiel
    ...  etablissement_type=M
    ...  etablissement_categorie=1
    ...  si_autorite_competente_visite=Commission communale de sécurité
    ...  adresse_numero=54
    ...  adresse_voie=RUE DE ROME
    ...  adresse_cp=13006
    ...  adresse_ville=MARSEILLE
    ...  adresse_arrondissement=6ème
    ...  siret=73282932000074
    ...  exp_civilite=Mme
    ...  exp_nom=Leclerc
    ...  exp_prenom=Tatiana
    ${etab07_code} =  Ajouter l'établissement  ${etab07}
    ${etab07_titre} =  Set Variable  ${etab07_code} - ${etab07.libelle}
    Set Suite Variable  ${etab07}
    Set Suite Variable  ${etab07_code}
    Set Suite Variable  ${etab07_titre}

    # On cré une UA validée sur l'établissement
    ${ua_validee_libelle} =    Set Variable    Bâtiment 3459
    Ajouter l'UA depuis le contexte d'un établissement    ${etab07_code}    ${etab07.libelle}    ${ua_validee_libelle}

    # On cré une UA archivée sur l'établissement
    ${ua_archivee_libelle} =    Set Variable    Bâtiment 3460
    Ajouter l'UA depuis le contexte d'un établissement    ${etab07_code}    ${etab07.libelle}    ${ua_archivee_libelle}
    Archiver l'UA validée    ${etab07_code}    ${etab07.libelle}    ${ua_archivee_libelle}

    # On cré un DI sur l'établissement
    &{dc07} =  Create Dictionary
    ...  dossier_coordination_type=Autorisation de Travaux
    ...  description=Description
    ...  etablissement=${etab07_titre}
    ...  date_demande=09/11/2014
    ...  a_qualifier=false
    ...  dossier_instruction_secu=true
    ...  dossier_instruction_acc=true
    ${dc07_libelle} =  Ajouter le dossier de coordination  ${dc07}
    Set Suite Variable  ${dc07}
    Set Suite Variable  ${dc07_libelle}
    ${di} =    Catenate    SEPARATOR=-    ${dc07_libelle}    ACC

    # On cré une UA en projet sur le DI
    ${ua_enprojet_libelle} =    Set Variable    Bâtiment 3459
    Ajouter l'UA depuis le contexte d'un dossier d'instruction    ${di}    ${ua_enprojet_libelle}


    ##
    Depuis la page d'accueil    technicien-acc    technicien-acc

    # Contexte LISTING 'UA'
    # Action 'ajouter' sur ce listing
    Depuis le listing des UA
    L'action ajouter ne doit pas être disponible
    # Actions diverses sur la fiche dans ce contexte
    Depuis la fiche de l'UA    ${etab07_code}    ${etab07.libelle}    ${ua_validee_libelle}
    Portlet Action Should Not Be In Form    etablissement_unite    analyser
    Portlet Action Should Not Be In Form    etablissement_unite    archiver
    Portlet Action Should Not Be In Form    etablissement_unite    desarchiver
    Portlet Action Should Not Be In Form    etablissement_unite    modifier
    Portlet Action Should Not Be In Form    etablissement_unite    supprimer
    Portlet Action Should Not Be In Form    etablissement_unite    valider
    Portlet Action Should Not Be In Form    etablissement_unite    repasser_en_projet

    # Contexte LISTING 'UA validées' sur l'établissement
    # Action 'ajouter' sur ce listing
    Depuis le listing des 'UA validées' sur l'établissement    ${etab07_code}    ${etab07.libelle}
    Page Should Contain Element    css=#sousform-etablissement_unite__contexte_etab__ua_valide span.add-16
    # Actions diverses sur la fiche dans ce contexte
    Depuis la fiche de l'UA dans le contexte des 'UA validées' sur l'établissement    ${etab07_code}    ${etab07.libelle}    ${ua_validee_libelle}
    Portlet Action Should Be In SubForm    etablissement_unite__contexte_etab__ua_valide    archiver
    Portlet Action Should Not Be In SubForm    etablissement_unite__contexte_etab__ua_valide    analyser
    Portlet Action Should Not Be In SubForm    etablissement_unite__contexte_etab__ua_valide    desarchiver
    Portlet Action Should Not Be In SubForm    etablissement_unite__contexte_etab__ua_valide    modifier
    Portlet Action Should Not Be In SubForm    etablissement_unite__contexte_etab__ua_valide    supprimer
    Portlet Action Should Not Be In SubForm    etablissement_unite__contexte_etab__ua_valide    valider
    Portlet Action Should Not Be In SubForm    etablissement_unite__contexte_etab__ua_valide    repasser_en_projet

    # Contexte LISTING 'UA en projet' sur l'établissement
    # Action 'ajouter' sur ce listing
    Depuis le listing des 'UA en projet' sur l'établissement    ${etab07_code}    ${etab07.libelle}
    Page Should Not Contain Element    css=#sousform-etablissement_unite__contexte_etab__ua_enprojet span.add-16
    # Actions diverses sur la fiche dans ce contexte
    Depuis la fiche de l'UA dans le contexte des 'UA en projet' sur l'établissement    ${etab07_code}    ${etab07.libelle}    ${ua_enprojet_libelle}
    Portlet Action Should Be In SubForm    etablissement_unite__contexte_etab__ua_enprojet    archiver
    Portlet Action Should Be In SubForm    etablissement_unite__contexte_etab__ua_enprojet    valider
    Portlet Action Should Not Be In SubForm    etablissement_unite__contexte_etab__ua_enprojet    analyser
    Portlet Action Should Not Be In SubForm    etablissement_unite__contexte_etab__ua_enprojet    desarchiver
    Portlet Action Should Not Be In SubForm    etablissement_unite__contexte_etab__ua_enprojet    modifier
    Portlet Action Should Not Be In SubForm    etablissement_unite__contexte_etab__ua_enprojet    supprimer
    Portlet Action Should Not Be In SubForm    etablissement_unite__contexte_etab__ua_enprojet    repasser_en_projet

    # Contexte LISTING 'UA archivées' sur l'établissement
    # Action 'ajouter' sur ce listing
    Depuis le listing des 'UA archivées' sur l'établissement    ${etab07_code}    ${etab07.libelle}
    Page Should Not Contain Element    css=#sousform-etablissement_unite__contexte_etab__ua_archive span.add-16
    # Actions diverses sur la fiche dans ce contexte
    Depuis la fiche de l'UA dans le contexte des 'UA archivées' sur l'établissement    ${etab07_code}    ${etab07.libelle}    ${ua_archivee_libelle}
    Portlet Action Should Not Be In SubForm    etablissement_unite__contexte_etab__ua_archive    analyser
    Portlet Action Should Not Be In SubForm    etablissement_unite__contexte_etab__ua_archive    archiver
    Portlet Action Should Not Be In SubForm    etablissement_unite__contexte_etab__ua_archive    desarchiver
    Portlet Action Should Not Be In SubForm    etablissement_unite__contexte_etab__ua_archive    modifier
    Portlet Action Should Not Be In SubForm    etablissement_unite__contexte_etab__ua_archive    supprimer
    Portlet Action Should Not Be In SubForm    etablissement_unite__contexte_etab__ua_archive    valider
    Portlet Action Should Not Be In SubForm    etablissement_unite__contexte_etab__ua_archive    repasser_en_projet

    ## LISTING 'UA analysées' sur le dossier d'instruction
    Depuis le listing des 'UA analysées' sur le dossier d'instruction    ${di}
    # Action 'ajouter' sur ce listing
    Page Should Contain Element    css=#sousform-etablissement_unite__contexte_di_analyse__ua_en_analyse span.add-16
    # Contexte LISTING 'UA analysées' sur le dossier d'instruction
    Depuis la fiche de l'UA dans le contexte des 'UA analysées' sur le dossier d'instruction    ${di}    ${ua_enprojet_libelle}
    Portlet Action Should Be In SubForm    etablissement_unite__contexte_di_analyse__ua_en_analyse    modifier
    Portlet Action Should Not Be In SubForm    etablissement_unite__contexte_di_analyse__ua_en_analyse    analyser
    Portlet Action Should Not Be In SubForm    etablissement_unite__contexte_di_analyse__ua_en_analyse    archiver
    Portlet Action Should Not Be In SubForm    etablissement_unite__contexte_di_analyse__ua_en_analyse    desarchiver
    Portlet Action Should Not Be In SubForm    etablissement_unite__contexte_di_analyse__ua_en_analyse    supprimer
    Portlet Action Should Not Be In SubForm    etablissement_unite__contexte_di_analyse__ua_en_analyse    valider
    Portlet Action Should Not Be In SubForm    etablissement_unite__contexte_di_analyse__ua_en_analyse    repasser_en_projet

    ## LISTING 'UA validées sur l'établissement' sur le dossier d'instruction
    Depuis le listing des 'UA validées sur l'établissement' sur le dossier d'instruction    ${di}
    # Pas d'action 'ajouter' sur ce listing
    Page Should Not Contain Element    css=#sousform-etablissement_unite__contexte_di_analyse__ua_valide_sur_etab span.add-16
    # Contexte LISTING 'UA validées sur l'établissement' sur le dossier d'instruction
    Depuis la fiche de l'UA dans le contexte des 'UA validées sur l'établissement' sur le dossier d'instruction    ${di}    ${ua_validee_libelle}
    Portlet Action Should Be In SubForm    etablissement_unite__contexte_di_analyse__ua_valide_sur_etab    analyser
    Portlet Action Should Not Be In SubForm    etablissement_unite__contexte_di_analyse__ua_valide_sur_etab    archiver
    Portlet Action Should Not Be In SubForm    etablissement_unite__contexte_di_analyse__ua_valide_sur_etab    desarchiver
    Portlet Action Should Not Be In SubForm    etablissement_unite__contexte_di_analyse__ua_valide_sur_etab    modifier
    Portlet Action Should Not Be In SubForm    etablissement_unite__contexte_di_analyse__ua_valide_sur_etab    supprimer
    Portlet Action Should Not Be In SubForm    etablissement_unite__contexte_di_analyse__ua_valide_sur_etab    valider
    Portlet Action Should Not Be In SubForm    etablissement_unite__contexte_di_analyse__ua_valide_sur_etab    repasser_en_projet

    ##
    Depuis la page d'accueil    secretaire-acc    secretaire-acc

    # Contexte LISTING 'UA'
    # Action 'ajouter' sur ce listing
    Depuis le listing des UA
    L'action ajouter ne doit pas être disponible
    # Actions diverses sur la fiche dans ce contexte
    Depuis la fiche de l'UA    ${etab07_code}    ${etab07.libelle}    ${ua_validee_libelle}
    Portlet Action Should Not Be In Form    etablissement_unite    analyser
    Portlet Action Should Not Be In Form    etablissement_unite    archiver
    Portlet Action Should Not Be In Form    etablissement_unite    desarchiver
    Portlet Action Should Not Be In Form    etablissement_unite    modifier
    Portlet Action Should Not Be In Form    etablissement_unite    supprimer
    Portlet Action Should Not Be In Form    etablissement_unite    valider
    Portlet Action Should Not Be In Form    etablissement_unite    repasser_en_projet

    # Contexte LISTING 'UA validées' sur l'établissement
    # Action 'ajouter' sur ce listing
    Depuis le listing des 'UA validées' sur l'établissement    ${etab07_code}    ${etab07.libelle}
    Page Should Not Contain Element    css=#sousform-etablissement_unite__contexte_etab__ua_valide span.add-16
    # Actions diverses sur la fiche dans ce contexte
    Depuis la fiche de l'UA dans le contexte des 'UA validées' sur l'établissement    ${etab07_code}    ${etab07.libelle}    ${ua_validee_libelle}
    Portlet Action Should Not Be In SubForm    etablissement_unite__contexte_etab__ua_valide    archiver
    Portlet Action Should Not Be In SubForm    etablissement_unite__contexte_etab__ua_valide    analyser
    Portlet Action Should Not Be In SubForm    etablissement_unite__contexte_etab__ua_valide    desarchiver
    Portlet Action Should Not Be In SubForm    etablissement_unite__contexte_etab__ua_valide    modifier
    Portlet Action Should Not Be In SubForm    etablissement_unite__contexte_etab__ua_valide    supprimer
    Portlet Action Should Not Be In SubForm    etablissement_unite__contexte_etab__ua_valide    valider
    Portlet Action Should Not Be In SubForm    etablissement_unite__contexte_etab__ua_valide    repasser_en_projet

    # Contexte LISTING 'UA en projet' sur l'établissement
    # Action 'ajouter' sur ce listing
    Depuis le listing des 'UA en projet' sur l'établissement    ${etab07_code}    ${etab07.libelle}
    Page Should Not Contain Element    css=#sousform-etablissement_unite__contexte_etab__ua_enprojet span.add-16
    # Actions diverses sur la fiche dans ce contexte
    Depuis la fiche de l'UA dans le contexte des 'UA en projet' sur l'établissement    ${etab07_code}    ${etab07.libelle}    ${ua_enprojet_libelle}
    Portlet Action Should Not Be In SubForm    etablissement_unite__contexte_etab__ua_enprojet    archiver
    Portlet Action Should Not Be In SubForm    etablissement_unite__contexte_etab__ua_enprojet    valider
    Portlet Action Should Not Be In SubForm    etablissement_unite__contexte_etab__ua_enprojet    analyser
    Portlet Action Should Not Be In SubForm    etablissement_unite__contexte_etab__ua_enprojet    desarchiver
    Portlet Action Should Not Be In SubForm    etablissement_unite__contexte_etab__ua_enprojet    modifier
    Portlet Action Should Not Be In SubForm    etablissement_unite__contexte_etab__ua_enprojet    supprimer
    Portlet Action Should Not Be In SubForm    etablissement_unite__contexte_etab__ua_enprojet    repasser_en_projet

    # Contexte LISTING 'UA archivées' sur l'établissement
    # Action 'ajouter' sur ce listing
    Depuis le listing des 'UA archivées' sur l'établissement    ${etab07_code}    ${etab07.libelle}
    Page Should Not Contain Element    css=#sousform-etablissement_unite__contexte_etab__ua_archive span.add-16
    # Actions diverses sur la fiche dans ce contexte
    Depuis la fiche de l'UA dans le contexte des 'UA archivées' sur l'établissement    ${etab07_code}    ${etab07.libelle}    ${ua_archivee_libelle}
    Portlet Action Should Not Be In SubForm    etablissement_unite__contexte_etab__ua_archive    analyser
    Portlet Action Should Not Be In SubForm    etablissement_unite__contexte_etab__ua_archive    archiver
    Portlet Action Should Not Be In SubForm    etablissement_unite__contexte_etab__ua_archive    desarchiver
    Portlet Action Should Not Be In SubForm    etablissement_unite__contexte_etab__ua_archive    modifier
    Portlet Action Should Not Be In SubForm    etablissement_unite__contexte_etab__ua_archive    supprimer
    Portlet Action Should Not Be In SubForm    etablissement_unite__contexte_etab__ua_archive    valider
    Portlet Action Should Not Be In SubForm    etablissement_unite__contexte_etab__ua_archive    repasser_en_projet

    ## LISTING 'UA analysées' sur le dossier d'instruction
    Depuis le listing des 'UA analysées' sur le dossier d'instruction    ${di}
    # Action 'ajouter' sur ce listing
    Page Should Not Contain Element    css=#sousform-etablissement_unite__contexte_di_analyse__ua_en_analyse span.add-16
    # Contexte LISTING 'UA analysées' sur le dossier d'instruction
    Depuis la fiche de l'UA dans le contexte des 'UA analysées' sur le dossier d'instruction    ${di}    ${ua_enprojet_libelle}
    Portlet Action Should Be In SubForm    etablissement_unite__contexte_di_analyse__ua_en_analyse    modifier
    Portlet Action Should Not Be In SubForm    etablissement_unite__contexte_di_analyse__ua_en_analyse    analyser
    Portlet Action Should Not Be In SubForm    etablissement_unite__contexte_di_analyse__ua_en_analyse    archiver
    Portlet Action Should Not Be In SubForm    etablissement_unite__contexte_di_analyse__ua_en_analyse    desarchiver
    Portlet Action Should Not Be In SubForm    etablissement_unite__contexte_di_analyse__ua_en_analyse    supprimer
    Portlet Action Should Not Be In SubForm    etablissement_unite__contexte_di_analyse__ua_en_analyse    valider
    Portlet Action Should Not Be In SubForm    etablissement_unite__contexte_di_analyse__ua_en_analyse    repasser_en_projet

    ## LISTING 'UA validées sur l'établissement' sur le dossier d'instruction
    Depuis le listing des 'UA validées sur l'établissement' sur le dossier d'instruction    ${di}
    # Pas d'action 'ajouter' sur ce listing
    Page Should Not Contain Element    css=#sousform-etablissement_unite__contexte_di_analyse__ua_valide_sur_etab span.add-16
    # Contexte LISTING 'UA validées sur l'établissement' sur le dossier d'instruction
    Depuis la fiche de l'UA dans le contexte des 'UA validées sur l'établissement' sur le dossier d'instruction    ${di}    ${ua_validee_libelle}
    Portlet Action Should Be In SubForm    etablissement_unite__contexte_di_analyse__ua_valide_sur_etab    analyser
    Portlet Action Should Not Be In SubForm    etablissement_unite__contexte_di_analyse__ua_valide_sur_etab    archiver
    Portlet Action Should Not Be In SubForm    etablissement_unite__contexte_di_analyse__ua_valide_sur_etab    desarchiver
    Portlet Action Should Not Be In SubForm    etablissement_unite__contexte_di_analyse__ua_valide_sur_etab    modifier
    Portlet Action Should Not Be In SubForm    etablissement_unite__contexte_di_analyse__ua_valide_sur_etab    supprimer
    Portlet Action Should Not Be In SubForm    etablissement_unite__contexte_di_analyse__ua_valide_sur_etab    valider
    Portlet Action Should Not Be In SubForm    etablissement_unite__contexte_di_analyse__ua_valide_sur_etab    repasser_en_projet

    ##
    Depuis la page d'accueil    cadre-acc    cadre-acc

    # Contexte LISTING 'UA'
    # Action 'ajouter' sur ce listing
    Depuis le listing des UA
    L'action ajouter ne doit pas être disponible
    # Actions diverses sur la fiche dans ce contexte
    Depuis la fiche de l'UA    ${etab07_code}    ${etab07.libelle}    ${ua_validee_libelle}
    Portlet Action Should Not Be In Form    etablissement_unite    analyser
    Portlet Action Should Not Be In Form    etablissement_unite    archiver
    Portlet Action Should Not Be In Form    etablissement_unite    desarchiver
    Portlet Action Should Not Be In Form    etablissement_unite    modifier
    Portlet Action Should Not Be In Form    etablissement_unite    supprimer
    Portlet Action Should Not Be In Form    etablissement_unite    valider
    Portlet Action Should Not Be In Form    etablissement_unite    repasser_en_projet

    # Contexte LISTING 'UA validées' sur l'établissement
    # Action 'ajouter' sur ce listing
    Depuis le listing des 'UA validées' sur l'établissement    ${etab07_code}    ${etab07.libelle}
    Page Should Contain Element    css=#sousform-etablissement_unite__contexte_etab__ua_valide span.add-16
    # Actions diverses sur la fiche dans ce contexte
    Depuis la fiche de l'UA dans le contexte des 'UA validées' sur l'établissement    ${etab07_code}    ${etab07.libelle}    ${ua_validee_libelle}
    Portlet Action Should Be In SubForm    etablissement_unite__contexte_etab__ua_valide    archiver
    Portlet Action Should Be In SubForm    etablissement_unite__contexte_etab__ua_valide    modifier
    Portlet Action Should Be In SubForm    etablissement_unite__contexte_etab__ua_valide    repasser_en_projet
    Portlet Action Should Not Be In SubForm    etablissement_unite__contexte_etab__ua_valide    analyser
    Portlet Action Should Not Be In SubForm    etablissement_unite__contexte_etab__ua_valide    desarchiver
    Portlet Action Should Not Be In SubForm    etablissement_unite__contexte_etab__ua_valide    supprimer
    Portlet Action Should Not Be In SubForm    etablissement_unite__contexte_etab__ua_valide    valider

    # Contexte LISTING 'UA en projet' sur l'établissement
    # Action 'ajouter' sur ce listing
    Depuis le listing des 'UA en projet' sur l'établissement    ${etab07_code}    ${etab07.libelle}
    Page Should Not Contain Element    css=#sousform-etablissement_unite__contexte_etab__ua_enprojet span.add-16
    # Actions diverses sur la fiche dans ce contexte
    Depuis la fiche de l'UA dans le contexte des 'UA en projet' sur l'établissement    ${etab07_code}    ${etab07.libelle}    ${ua_enprojet_libelle}
    Portlet Action Should Be In SubForm    etablissement_unite__contexte_etab__ua_enprojet    archiver
    Portlet Action Should Be In SubForm    etablissement_unite__contexte_etab__ua_enprojet    valider
    Portlet Action Should Not Be In SubForm    etablissement_unite__contexte_etab__ua_enprojet    analyser
    Portlet Action Should Not Be In SubForm    etablissement_unite__contexte_etab__ua_enprojet    desarchiver
    Portlet Action Should Not Be In SubForm    etablissement_unite__contexte_etab__ua_enprojet    modifier
    Portlet Action Should Not Be In SubForm    etablissement_unite__contexte_etab__ua_enprojet    supprimer
    Portlet Action Should Not Be In SubForm    etablissement_unite__contexte_etab__ua_enprojet    repasser_en_projet

    # Contexte LISTING 'UA archivées' sur l'établissement
    # Action 'ajouter' sur ce listing
    Depuis le listing des 'UA archivées' sur l'établissement    ${etab07_code}    ${etab07.libelle}
    Page Should Not Contain Element    css=#sousform-etablissement_unite__contexte_etab__ua_archive span.add-16
    # Actions diverses sur la fiche dans ce contexte
    Depuis la fiche de l'UA dans le contexte des 'UA archivées' sur l'établissement    ${etab07_code}    ${etab07.libelle}    ${ua_archivee_libelle}
    Portlet Action Should Be In SubForm    etablissement_unite__contexte_etab__ua_archive    desarchiver
    Portlet Action Should Not Be In SubForm    etablissement_unite__contexte_etab__ua_archive    analyser
    Portlet Action Should Not Be In SubForm    etablissement_unite__contexte_etab__ua_archive    archiver
    Portlet Action Should Not Be In SubForm    etablissement_unite__contexte_etab__ua_archive    modifier
    Portlet Action Should Not Be In SubForm    etablissement_unite__contexte_etab__ua_archive    supprimer
    Portlet Action Should Not Be In SubForm    etablissement_unite__contexte_etab__ua_archive    valider
    Portlet Action Should Not Be In SubForm    etablissement_unite__contexte_etab__ua_archive    repasser_en_projet

    ## LISTING 'UA analysées' sur le dossier d'instruction
    Depuis le listing des 'UA analysées' sur le dossier d'instruction    ${di}
    # Action 'ajouter' sur ce listing
    Page Should Contain Element    css=#sousform-etablissement_unite__contexte_di_analyse__ua_en_analyse span.add-16
    # Contexte LISTING 'UA analysées' sur le dossier d'instruction
    Depuis la fiche de l'UA dans le contexte des 'UA analysées' sur le dossier d'instruction    ${di}    ${ua_enprojet_libelle}
    Portlet Action Should Be In SubForm    etablissement_unite__contexte_di_analyse__ua_en_analyse    modifier
    Portlet Action Should Not Be In SubForm    etablissement_unite__contexte_di_analyse__ua_en_analyse    analyser
    Portlet Action Should Not Be In SubForm    etablissement_unite__contexte_di_analyse__ua_en_analyse    archiver
    Portlet Action Should Not Be In SubForm    etablissement_unite__contexte_di_analyse__ua_en_analyse    desarchiver
    Portlet Action Should Not Be In SubForm    etablissement_unite__contexte_di_analyse__ua_en_analyse    supprimer
    Portlet Action Should Not Be In SubForm    etablissement_unite__contexte_di_analyse__ua_en_analyse    valider
    Portlet Action Should Not Be In SubForm    etablissement_unite__contexte_di_analyse__ua_en_analyse    repasser_en_projet

    ## LISTING 'UA validées sur l'établissement' sur le dossier d'instruction
    Depuis le listing des 'UA validées sur l'établissement' sur le dossier d'instruction    ${di}
    # Pas d'action 'ajouter' sur ce listing
    Page Should Not Contain Element    css=#sousform-etablissement_unite__contexte_di_analyse__ua_valide_sur_etab span.add-16
    # Contexte LISTING 'UA validées sur l'établissement' sur le dossier d'instruction
    Depuis la fiche de l'UA dans le contexte des 'UA validées sur l'établissement' sur le dossier d'instruction    ${di}    ${ua_validee_libelle}
    Portlet Action Should Be In SubForm    etablissement_unite__contexte_di_analyse__ua_valide_sur_etab    analyser
    Portlet Action Should Not Be In SubForm    etablissement_unite__contexte_di_analyse__ua_valide_sur_etab    archiver
    Portlet Action Should Not Be In SubForm    etablissement_unite__contexte_di_analyse__ua_valide_sur_etab    desarchiver
    Portlet Action Should Not Be In SubForm    etablissement_unite__contexte_di_analyse__ua_valide_sur_etab    modifier
    Portlet Action Should Not Be In SubForm    etablissement_unite__contexte_di_analyse__ua_valide_sur_etab    supprimer
    Portlet Action Should Not Be In SubForm    etablissement_unite__contexte_di_analyse__ua_valide_sur_etab    valider
    Portlet Action Should Not Be In SubForm    etablissement_unite__contexte_di_analyse__ua_valide_sur_etab    repasser_en_projet


