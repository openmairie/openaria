*** Settings ***
Resource  resources/resources.robot
Suite Setup  For Suite Setup
Suite Teardown  For Suite Teardown
Documentation  Ce test suite permet de falider le bon fonctionnement des fonctionnalités
...  des menus Statistiques, Édition et Requêtes mémorisées


*** Test Cases ***
Constitution d'un jeu de données
    [Documentation]  L'objet de ce 'Test Case' est de constituer un jeu de
    ...  données cohérent pour les scénarios fonctionnels qui suivent.

    Depuis la page d'accueil  admin  admin
    #
    &{etab_g_01} =  Create Dictionary
    ...  libelle=ETAB G01 T150
    ...  etablissement_nature=ERP Référentiel
    ...  etablissement_type=M
    ...  etablissement_categorie=1
    ...  si_autorite_competente_visite=Commission communale de sécurité
    ...  adresse_numero=152
    ...  adresse_voie=RUE DE ROME
    ...  adresse_cp=13006
    ...  adresse_ville=MARSEILLE
    ...  adresse_arrondissement=6ème
    ...  siret=73282932000074
    ...  exp_civilite=Mme
    ...  exp_nom=Martel
    ...  exp_prenom=Lyne
    ${etab_g_01.code} =  Ajouter l'établissement  ${etab_g_01}
    ${etab_g_01.titre} =  Set Variable  ${etab_g_01.code} - ${etab_g_01.libelle}
    Set Suite Variable  ${etab_g_01}


WIDGET DASHBOARD direct_search "Recherche directe d'établissement"
    [Documentation]

    Depuis la page d'accueil  admin  admin
    # Le champ de recherche est positionné en autofocus à chaque chargement du
    # tableau de bord
    Element Should Be Focused  css=.widget_direct_search #filter
    # Lorque l'on valide le formulaire de recherche sans renseigner de valeurs
    # à chercher, on se retrouve sur le listing de tous les établissements
    Click Element  css=.widget_direct_search button
    La page ne doit pas contenir d'erreur
    Page Title Should Be  Établissements > Tous Les Établissements
    Form Value Should Be  css=div#adv-search-adv-fields div.form-content input#etablissement  ${EMPTY}
    # Lorsque l'on renseigne un code établissement qui n'existe pas, le listing
    # de tous les établissements est filtré par la valeur renseignée
    Depuis la page d'accueil  admin  admin
    Input Text  css=.widget_direct_search #filter  AZERTY
    Click Element  css=.widget_direct_search button
    La page ne doit pas contenir d'erreur
    Page Title Should Be  Établissements > Tous Les Établissements
    Form Value Should Be  css=div#adv-search-adv-fields div.form-content input#etablissement  AZERTY
    Depuis la page d'accueil  admin  admin
    Input Text  css=.widget_direct_search #filter  999999
    Click Element  css=.widget_direct_search button
    La page ne doit pas contenir d'erreur
    Page Title Should Be  Établissements > Tous Les Établissements
    Form Value Should Be  css=div#adv-search-adv-fields div.form-content input#etablissement  T999999
    # Lorsque l'on renseigne un code établissement qui correspond exactement à
    # un établissement existant, on accède à la fiche de l'établissement et le
    # bouton retour nous renvoi au listing tous les établissements filtré par
    # la valeur renseignée
    Depuis la page d'accueil  admin  admin
    Input Text  css=.widget_direct_search #filter  ${etab_g_01.code}
    Click Element  css=.widget_direct_search button
    La page ne doit pas contenir d'erreur
    Page Title Should Be  Établissements > Tous Les Établissements > ${etab_g_01.titre}
    Click On Back Button
    La page ne doit pas contenir d'erreur
    Page Title Should Be  Établissements > Tous Les Établissements
    Form Value Should Be  css=div#adv-search-adv-fields div.form-content input#etablissement  ${etab_g_01.code}


Naviguer dans les statistiques accessibilité

    [Documentation]   Vérification des valeurs des statistiques accessibillité
    Comment    @todo Écrire le Test Case'

Naviguer dans les statistiques sécurité incendie

    [Documentation]   Vérification des valeurs des statistiques sécurité incendie
    Comment    @todo Écrire le Test Case'

Générer les éditions pdf SI de pilotage
    [Documentation]   Vérification du bon fonctionnement des éditions pdf

    #
    Depuis la page d'accueil    cadre-si    cadre-si
    # On ouvre la vue statistiques
    Go To Submenu In Menu    suivi    statistiques
    # On génère l'édition
    Click Element    css=input[name=editions-stats-submit]
    # On ouvre le PDF
    Open PDF  ${OM_PDF_TITLE}
    # On vérifie le contenu
    WUX  Page Should Contain    Visites réalisées
    PDF Pages Number Should Be    4
    # On revient à la fenêtre principale
    Close PDF


Générer les éditions pdf ACC de pilotage
    [Documentation]   Vérification du bon fonctionnement des éditions pdf
    Comment    @todo Ajouter les permissions au cadre-acc

    #
    Depuis la page d'accueil    cadre-acc    cadre-acc
    # On ouvre la vue statistiques
    Go To Submenu In Menu    suivi    statistiques
    # On génère l'édition
    Click Element    css=input[name=editions-stats-submit]
    # On ouvre le PDF
    Open PDF  ${OM_PDF_TITLE}
    # On vérifie le contenu
    WUX  Page Should Contain    Par date et avis de réunion
    PDF Pages Number Should Be    1
    # On revient à la fenêtre principale
    Close PDF


Générer les exports csv de pilotage

    [Documentation]   Vérification du bon fonctionnement de la génération des csv


    #
    Depuis la page d'accueil    cadre-si    cadre-si

    # csv etablissement
    Go To Submenu In Menu    suivi    reqmo
    # On accède à la configuration de l'export
    Click On Link    établissement
    # On valide le formulaire (sortie tableau)
    Click On Submit Button In Reqmo
    # Retour
    Click On Back Button
    # Vérification du csv
    # On sélectionne la sortie csv
    Select From List By Value    css=#sortie    csv
    # On valide le formulaire (sortie tableau)
    Click On Submit Button In Reqmo
    # On click sur le lien du csv
    Click On Link    css=#reqmo-out-link

    # csv liste_modeles
    Go To Submenu In Menu    suivi    reqmo
    # On accède à la configuration de l'export
    Click On Link    modèle d'édition
    # On valide le formulaire (sortie tableau)
    Click On Submit Button In Reqmo
    # Retour
    Click On Back Button
    # Vérification du csv
    # On sélectionne la sortie csv
    Select From List By Value    css=#sortie    csv
    # On valide le formulaire (sortie tableau)
    Click On Submit Button In Reqmo
    # On click sur le lien du csv
    Click On Link    css=#reqmo-out-link


