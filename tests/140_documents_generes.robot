*** Settings ***
Resource  resources/resources.robot
Suite Setup  For Suite Setup
Suite Teardown  For Suite Teardown
Documentation  Les documents générés...


*** Test Cases ***
Constitution d'un jeu de données

    [Documentation]  L'objet de ce 'Test Case' est de constituer un jeu de
    ...  données cohérent pour les scénarios fonctionnels qui suivent.

    # En tant que profil CADRE SI
    Depuis la page d'accueil    cadre-si    cadre-si

    # On définit une liste de clés pour lesquelles on souhaite vérifier
    # l'absence dans le fichier de métadonnées. Ce sont les métadonnées
    # spécifiques aux arrêtés, aux PV et aux réunions.
    @{md_no} =  Create List
    ...  pv_erp_numero
    ...  pv_erp_nature_analyse
    ...  pv_erp_reference_urbanisme
    ...  pv_erp_avis_rendu
    ...  code_reunion
    ...  date_reunion
    ...  type_reunion
    ...  commission
    ...  arrete_numero
    ...  arrete_reglementaire
    ...  arrete_notification
    ...  arrete_date_notification
    ...  arrete_publication
    ...  arrete_date_publication
    ...  arrete_temporaire
    ...  arrete_expiration
    ...  arrete_date_controle_legalite
    ...  arrete_nature_acte
    ...  arrete_nature_acte_niv1
    ...  arrete_nature_acte_niv2
    Set Suite Variable  ${md_no}

    ##
    ## ETABLISSEMENT 01
    ##
    &{etab01} =  Create Dictionary
    ...  libelle=Magasin NINE
    ...  etablissement_nature=ERP potentiel
    ...  adresse_numero=1
    ...  adresse_voie=RUE MARCEL ROMAN
    ...  adresse_cp=13015
    ...  adresse_ville=MARSEILLE
    ...  adresse_arrondissement=15ème
    ...  exp_civilite=M.
    ...  exp_nom=Dupont
    ...  exp_prenom=Jacques
    ${etab01_code} =  Ajouter l'établissement  ${etab01}
    ${etab01_titre} =  Set Variable  ${etab01_code} - ${etab01.libelle}
    Set Suite Variable  ${etab01}
    Set Suite Variable  ${etab01_code}
    Set Suite Variable  ${etab01_titre}

    # On ajoute des contacts à l'établissement
    Ajouter un particulier depuis un établissement  ${etab01_code}  Mandataire  M.  Dupont  Jean  true
    Ajouter un particulier depuis un établissement  ${etab01_code}  Mandataire  M.  Dupont  Paul  true

    ##
    ## DOSSIER DE COORDINATION 01
    ##
    &{dc01} =  Create Dictionary
    ...  dossier_coordination_type=Visite de contrôle SI
    ...  description=Visite de contrôle du service sécurité incendie
    ...  etablissement=${etab01_titre}
    ${dc01_libelle} =  Ajouter le dossier de coordination  ${dc01}
    Set Suite Variable  ${dc01}
    Set Suite Variable  ${dc01_libelle}
    ${dc01_di_si} =  Catenate  SEPARATOR=-  ${dc01_libelle}  SI
    Set Suite Variable  ${dc01_di_si}

    #
    Depuis la page d'accueil    secretaire-si    secretaire-si
    # On ajoute un document généré pour l'utiliser en courrier joint
    @{contacts_lies}    Create List    (Exploitant) M. Dupont Jacques
    ${params}    Create Dictionary    contacts_lies=@{contacts_lies}
    Ajouter le document généré depuis le contexte du dossier d'instruction    ${dc01_di_si}     ${params}    Courrier simple    Courrier simple
    # On clique sur le bouton retour
    Click On Back Button In Subform
    # On clique sur l'enregistrement nouvellement crée
    Click On Link    Dupont Jacques
    # On récupère le code barres
    ${code_barres_1} =    Get Text    css=#code_barres
    ${courrier_joint} =    Get Text    css=#form-content span#courrier
    Set Suite Variable    ${code_barres_1}
    Set Suite Variable    ${courrier_joint}
    #
    Finaliser le document généré    ${code_barres_1}

    #
    Depuis la page d'accueil    admin    admin

    # On ajoute un modèle d'édition pour faciliter la recherche dans un onglet
    &{args_modele_edition} =  Create Dictionary
    ...  code=COUD
    ...  libelle=Courrier par défaut
    ...  courrier_type=Courrier simple
    ...  om_lettretype_id=courrier_standard_modele0
    Ajouter le modèle d'édition  ${args_modele_edition}

    &{courrier_type_01} =  Create Dictionary
    ...  code=DG-SPEC1
    ...  libelle=Documents Générés Spécifiques T400 SANS
    ...  description=Description des documents générés spécifiques
    ...  courrier_type_categorie=Documents générés liés aux établissements, aux dossiers de coordination et aux dossiers d'instruction
    ${courrier_type_01_id} =  Ajouter le type de courrier  ${courrier_type_01}
    Set Suite Variable  ${courrier_type_01}
    Set Suite Variable  ${courrier_type_01_id}

    &{courrier_type_02} =  Create Dictionary
    ...  code=DG-SPEC2
    ...  libelle=Documents Générés Spécifiques T400 AVEC
    ...  description=Description des documents générés spécifiques
    ...  courrier_type_categorie=Documents générés liés aux établissements, aux dossiers de coordination et aux dossiers d'instruction
    ${courrier_type_02_id} =  Ajouter le type de courrier  ${courrier_type_02}
    Set Suite Variable  ${courrier_type_02}
    Set Suite Variable  ${courrier_type_02_id}

    &{modele_edition_02} =  Create Dictionary
    ...  code=T140-1
    ...  libelle=Modèle d'édition Test 140 02
    ...  courrier_type=${courrier_type_02.libelle}
    ...  om_lettretype_id=courrier_standard_modele0
    Ajouter le modèle d'édition  ${modele_edition_02}
    Set Suite Variable  ${modele_edition_02}

    # On ajoute des textes-type de document généré
    ${courrier_texte_type_contenu_1} =  Set Variable  Premier test
    ${courrier_texte_type_contenu_2} =  Set Variable  Deuxième test
    Set Suite Variable  ${courrier_texte_type_contenu_1}
    Set Suite Variable  ${courrier_texte_type_contenu_2}
    Ajouter le texte type depuis le menu  TEST_1  ${courrier_type_02.libelle}  ${courrier_texte_type_contenu_1}
    Ajouter le texte type depuis le menu  TEST_2  ${courrier_type_02.libelle}  ${courrier_texte_type_contenu_2}


Paramétrage des modèles d'édition
    [Documentation]  Paramétrage des modèles d'édition
    ...
    ...  TNR Le formulaire de modification d'un modèle d'édition produit une erreur de base de données (cause requête vide) si l'identifiant de lettre type qu'il possède n'existe plus.
    Depuis la page d'accueil  admin  admin
    ## Constitution du jeu de données
    # lettre type
    &{lt_l_01} =  Create Dictionary
    ...  id=t140parammelt1
    Ajouter la lettre-type depuis le menu
    ...  ${lt_l_01.id}
    ...  ${lt_l_01.id}
    ...  <p>lettre type ${lt_l_01.id}</p>
    ...  ...
    ...  Contexte 'document genere'
    ...  true
    # modèle d'édition rattaché à la lettre type
    &{me_l_01} =  Create Dictionary
    ...  code=t140parammeme1
    ...  libelle=Modèle d'édition ${lt_l_01.id}
    ...  courrier_type=Courrier simple
    ...  om_lettretype_id=${lt_l_01.id}
    ${me_l_01.id} =  Ajouter le modèle d'édition  ${me_l_01}
    #
    Depuis le contexte du modèle d'édition (accès par URL)  ${me_l_01.id}
    Click On Form Portlet Action  modele_edition  modifier
    #
    Depuis le contexte de la lettre-type  ${lt_l_01.id}
    Click On Form Portlet Action  om_lettretype  supprimer
    Click On Submit Button
    Depuis le contexte du modèle d'édition (accès par URL)  ${me_l_01.id}
    Click On Form Portlet Action  modele_edition  modifier


Imprimé pour les envois recommandés avec AR
    [Documentation]  Imprimé officiel pour les envois recommandés nationaux avec avis de passage, preuve de distribution, avis de réception (AR)

    Depuis la page d'accueil  admin  admin
    #
    Go To Submenu In Menu  suivi  courrier_rar
    Page Title Should Be  Suivi > Documents Générés > Édition RAR
    #
    Depuis le formulaire de l'impression des éditions RAR
    # On vérifie que la date est renseignée par défaut avec la date du jour
    Form Value Should Be  css=#date  ${DATE_FORMAT_DD/MM/YYYY}
    # On valide le formulaire sans renseigner de code barre
    # On attend un message d'erreur en ce sens
    Click On Submit Button
    WUX  Error Message Should Be  Tous les champs doivent être remplis.
    # On renseigne un code barre qui n'a pas un format correct
    # On attend un message d'erreur en ce sens
    Input Text  css=#liste_code_barres_instruction  nexistepas
    Click On Submit Button
    WUX  Error Message Should Be  Le code barres nexistepas n'est pas valide.
    # On renseigne un code barre qui n'existe pas mais qui a un format correct
    # On attend un message d'erreur en ce sens
    Input Text  css=#liste_code_barres_instruction  123
    Click On Submit Button
    WUX  Error Message Should Be  Le numéro 123 ne correspond à aucun code barre existant.
    # On renseigne un code barre qui existe
    # On attend un message de validation en ce sens
    # On ouvre le PDF pour vérifier son contenu
    Input Text  css=#liste_code_barres_instruction  ${code_barres_1}
    Click On Submit Button
    WUX  Valid Message Should Contain  Cliquez sur le lien ci-dessous pour télécharger votre document :
    Click Link  Télécharger le document pour 1 RAR
    Open PDF  ${OM_PDF_TITLE}
    WUX  Page Should Contain  ${code_barres_1}
    WUX  Page Should Contain  Dupont Jacques
    Close PDF


Modification d'un document généré finalisé
    [Documentation]  TNR La modification d'un document généré finalisé entrainaît la perte
    ...  des champs complement1_om_html et complement2_om_html.

    Depuis la page d'accueil  admin  admin

    # document généré 01
    ${dg_l_01} =  Create Dictionary
    ...  complement1=plop
    ...  complement2=plip
    ...  lien=dossier_instruction
    ...  dossier_instruction=${dc01_di_si}
    @{contacts_lies}    Create List    (Exploitant) M. Dupont Jacques
    ${params}    Create Dictionary    contacts_lies=@{contacts_lies}
    Ajouter le document généré depuis le contexte du dossier d'instruction
    ...  ${dg_l_01.dossier_instruction}
    ...  ${params}
    ...  Courrier simple
    ...  Courrier simple
    ...  complement1_om_html=${dg_l_01.complement1}
    ...  complement2_om_html=${dg_l_01.complement2}
    ${dg_l_01.id} =  Get Text  css=#courrier
    ${dg_l_01.code_barres} =  Get Text  css=#code_barres

    # On finalise le document généré et on vérifie que les compléments 1 & 2 sont bien présents
    Depuis le contexte du document généré par le menu suivi par code barres  ${dg_l_01.code_barres}
    Finaliser le document généré  ${dg_l_01.code_barres}
    Element Should Contain  css=#complement1_om_html  ${dg_l_01.complement1}
    Element Should Contain  css=#complement2_om_html  ${dg_l_01.complement2}

    # On modifie le document généré et on vérifie que les compléments 1 & 2 sont bien présents
    Click On Form Portlet Action  courrier  modifier
    Click On Submit Button
    Valid Message Should Contain  Vos modifications ont bien été enregistrées.
    Element Should Contain  css=#complement1_om_html  ${dg_l_01.complement1}
    Element Should Contain  css=#complement2_om_html  ${dg_l_01.complement2}


Création et finalisation du document généré
    [Documentation]  Créer un document généré mailing avec 3 destinataires et le finalise pour générer les 3 courriers enfants.

    Depuis la page d'accueil    secretaire-si    secretaire-si

    # document généré 01
    ${dg_l_01} =  Create Dictionary
    ...  complement1=plop
    ...  complement2=plip
    ...  lien=dossier_instruction
    ...  dossier_instruction=${dc01_di_si}

    # Depuis le formulaire d'ajout d'un document généré dans le contexte d'un DI
    Depuis l'onglet document généré du dossier d'instruction  ${dg_l_01.dossier_instruction}
    Click On Add Button JS

    # Vérification de la non présence du type de courrier 01 (qui ne possède
    # aucun modèle d'édition) et de la présence du type de courrier 02 (qui
    # possède un modèle d'édition)
    ${should_not_be_present} =  Create List  ${courrier_type_01.libelle}
    Select List Should Not Contain List  css=#courrier_type  ${should_not_be_present}
    ${should_be_present} =  Create List  ${courrier_type_02.libelle}
    Select List Should Contain List  css=#courrier_type  ${should_be_present}

    # Vérification du filtre automatique du champ 'modele_edition' en fonction
    # du courrier type sélectionné. Par défaut au chargement du formulaire d'ajout
    # ce champ doit être vide. Ensuite lorsque l'on sélection un courrier type
    # le champ doit se remplir avec les modèles d'édition rattaché à ce type de
    # courrier.
    # Vide
    ${list_before} =  Create List  Choisir modèle d'édition
    Select List Should Be  css=#modele_edition  ${list_before}
    # On sélectionne le type de courrier
    Select From List By Label  css=#courrier_type  ${courrier_type_02.libelle}
    ${list_after} =  Create List  Choisir modèle d'édition  ${modele_edition_02.libelle}
    Select List Should Be  css=#modele_edition  ${list_after}

    # On sélectionne le modèle d'édition
    Select From List By Label  css=#modele_edition  ${modele_edition_02.libelle}

    # On sélectionne le courrier joint
    Input Text  css=#autocomplete-courrier-search  ${courrier_joint}
    WUX  Click Link  css=a.autocomplete-courrier-result-${courrier_joint}

    # On saisit le complément 1
    Input HTML  complement1_om_html  Complément 1
    @{liste_texte_type_id}  Create List  checkbox_1  checkbox_2
    @{liste_texte_type_contenu}  Create List  ${courrier_texte_type_contenu_1}  ${courrier_texte_type_contenu_2}
    ${params}  Create Dictionary  liste_texte_type_id=@{liste_texte_type_id}  liste_texte_type_contenu=@{liste_texte_type_contenu}
    Insérer une liste de texte type  courrier_texte_type_complement1  complement1_om_html  ${params}

    # On saisit le complément 2
    WUX  Input HTML  complement2_om_html  Complément 2

    # On ajoute les destinataires
    @{contacts_lies}  Create List  (Exploitant) M. Dupont Jacques  (Mandataire) M. Dupont Jean  (Mandataire) M. Dupont Paul
    Select From List By Label  css=#contacts_lies  @{contacts_lies}

    # On clique sur le bouton de validation
    Click On Submit Button In Subform
    Valid Message Should Contain In Subform  Création de 3 nouvelles liaisons réalisée avec succès.
    Valid Message Should Contain In Subform  Vos modifications ont bien été enregistrées.
    ${dg_l_01.code_barres} =  Get Text  css=#code_barres
    # On clique sur le bouton retour
    Click On Back Button In Subform

    #
    Prévisualiser le document généré  ${dg_l_01.code_barres}  ${courrier_texte_type_contenu_1}
    Finaliser le document généré  ${dg_l_01.code_barres}

    # On vérifie que le fieldset contenant les documents générés liés est
    # présent
    WUX  Page Should Contain    Documents générés liés


Gestion des métadonnées

    [Documentation]  Vérifie les métadonnées pour les champs fichier :
    ...
    ...  - Document généré finalisé [courrier.om_fichier_finalise_courrier]
    ...    > Généré
    ...    > Stockage à la finalisation de l'édition
    ...    > Mise à jour à chaque refinalisation de l'édition
    ...    > Mise à jour XXX
    ...
    ...  - Document généré numérisé signé [courrier.om_fichier_signe_courrier]
    ...    > Téléversé
    ...    > Stockage à l'ajout du fichier
    ...    > Mise à jour à chaque mise à jour du champ fichier
    ...    > Mise à jour XXX
    ...
    ...  Plusieurs cas d'utilisation sur les documents générés :
    ...  - A - document généré finalisé depuis un DI
    ...  - B - document généré signé numérisé depuis un DI
    ...  - C - document généré finalisé depuis un DC
    ...  - D - document généré signé numérisé depuis un DC
    ...  - E - document généré finalisé depuis un établissement
    ...  - F - document généré signé numérisé depuis un établissement
    ...  - 7 - document généré de type Arrêté finalisé depuis un DC
    ...  - 8 - document généré de type Arrêté signé numérisé depuis un DC
    ...  - 9 - document généré de type PV finalisé depuis un DI
    ...  - 10 - document généré de type PV signé numérisé depuis un DI

    # En tant que profil ADMINISTRATEUR
    Depuis la page d'accueil  admin  admin

    ##
    ## Document 01 - Cas d'utilisation A & B
    ##
    Ajouter un particulier depuis un établissement    ${etab01_code}    Mandataire    M.    AQWEDC01    Jacques    true
    @{contacts_lies}    Create List    (Mandataire) M. AQWEDC01 Jacques
    ${params}    Create Dictionary    contacts_lies=@{contacts_lies}
    Ajouter le document généré depuis le contexte du dossier d'instruction    ${dc01_di_si}     ${params}    Courrier simple    Courrier simple
    Click On Back Button In Subform
    Input Text  css=span#recherche_onglet input#recherchedyn  Courrier simple
    Click On Link  AQWEDC01 Jacques
    ${docgen01_id} =  Get Text  css=#sousform-courrier #courrier
    ${docgen01_code_barres} =  Get Text  css=#code_barres

    #
    Finaliser le document généré  ${docgen01_code_barres}
    ## => [courrier.om_fichier_finalise_courrier] (METADATA FILESTORAGE)
    ${docgen01_fichier_finalise_uid} =  Récupérer l'uid du fichier finalisé du document généré  ${docgen01_code_barres}
    ${docgen01_fichier_finalise_path} =  Récupérer le chemin vers le fichier correspondant à l'uid  ${docgen01_fichier_finalise_uid}
    ${docgen01_fichier_finalise_md_path} =  Récupérer le chemin vers le fichier de métadonnées correspondant à l'uid  ${docgen01_fichier_finalise_uid}
    Le fichier doit exister  ${docgen01_fichier_finalise_path}
    Le fichier doit exister  ${docgen01_fichier_finalise_md_path}
    # Composition du dictionnaire représentant les métadonnées qui doivent
    # apparaître dans le fichier de métadonnées.
    ${size}=  Get File Size  ${docgen01_fichier_finalise_path}
    ${md} =  Create Dictionary
    ...  filename=document_genere_${docgen01_id}.pdf
    ...  mimetype=application/pdf
    ...  size=${size}
    ...  titre=Établissement ${etab01_code} - Dossier ${dc01_di_si} - Courrier simple
    ...  description=document généré finalisé
    ...  application=openARIA
    ...  origine=généré
    ...  etablissement_code=${etab01_code}
    ...  etablissement_libelle=${etab01.libelle}
    ...  etablissement_siret=
    ...  etablissement_referentiel=false
    ...  etablissement_exploitant=${etab01.exp_prenom} ${etab01.exp_nom}
    ...  etablissement_adresse_numero=${etab01.adresse_numero}
    ...  etablissement_adresse_mention=
    ...  etablissement_adresse_voie=${etab01.adresse_voie}
    ...  etablissement_adresse_cp=${etab01.adresse_cp}
    ...  etablissement_adresse_ville=${etab01.adresse_ville}
    ...  etablissement_adresse_arrondissement=${etab01.adresse_arrondissement}
    ...  etablissement_ref_patrimoine=
    ...  dossier_coordination=${dc01_libelle}
    ...  dossier_instruction=${dc01_di_si}
    ...  signataire=
    ...  signataire_qualite=
    ...  date_signature=
    Les métadonnées (clé/valeur) doivent être présentes dans le fichier  ${md}  ${docgen01_fichier_finalise_md_path}
    Les métadonnées (clé) ne doivent pas être présentes dans le fichier  ${md_no}  ${docgen01_fichier_finalise_md_path}

    #
    Ajouter le fichier signé du document généré  ${docgen01_code_barres}  numerisation1.pdf
    ## => [courrier.om_fichier_signe_courrier] (METADATA FILESTORAGE)
    ${docgen01_fichier_signe_uid} =  Récupérer l'uid du fichier signé du document généré  ${docgen01_code_barres}
    ${docgen01_fichier_signe_path} =  Récupérer le chemin vers le fichier correspondant à l'uid  ${docgen01_fichier_signe_uid}
    ${docgen01_fichier_signe_md_path} =  Récupérer le chemin vers le fichier de métadonnées correspondant à l'uid  ${docgen01_fichier_signe_uid}
    Le fichier doit exister  ${docgen01_fichier_signe_path}
    Le fichier doit exister  ${docgen01_fichier_signe_md_path}
    # Composition du dictionnaire représentant les métadonnées qui doivent
    # apparaître dans le fichier de métadonnées.
    ${size}=  Get File Size  ${docgen01_fichier_signe_path}
    ${md} =  Create Dictionary
    ...  filename=document_genere_${docgen01_id}_signe.pdf
    ...  mimetype=application/pdf
    ...  size=${size}
    ...  titre=Établissement ${etab01_code} - Dossier ${dc01_di_si} - Courrier simple (signé)
    ...  description=document généré numérisé signé
    ...  application=openARIA
    ...  origine=téléversé
    ...  etablissement_code=${etab01_code}
    ...  etablissement_libelle=${etab01.libelle}
    ...  etablissement_siret=
    ...  etablissement_referentiel=false
    ...  etablissement_exploitant=${etab01.exp_prenom} ${etab01.exp_nom}
    ...  etablissement_adresse_numero=${etab01.adresse_numero}
    ...  etablissement_adresse_mention=
    ...  etablissement_adresse_voie=${etab01.adresse_voie}
    ...  etablissement_adresse_cp=${etab01.adresse_cp}
    ...  etablissement_adresse_ville=${etab01.adresse_ville}
    ...  etablissement_adresse_arrondissement=${etab01.adresse_arrondissement}
    ...  etablissement_ref_patrimoine=
    ...  dossier_coordination=${dc01_libelle}
    ...  dossier_instruction=${dc01_di_si}
    ...  signataire=
    ...  signataire_qualite=
    ...  date_signature=
    Les métadonnées (clé/valeur) doivent être présentes dans le fichier  ${md}  ${docgen01_fichier_signe_md_path}
    Les métadonnées (clé) ne doivent pas être présentes dans le fichier  ${md_no}  ${docgen01_fichier_signe_md_path}

    ##
    ##
    ##
    Depuis le contexte du document généré  ${docgen01_code_barres}
    Click On Form Portlet Action  courrier  modifier
    Input Text  css=#date_retour_signature  10/01/2016
    Click On Submit Button
    Valid Message Should Contain    Vos modifications ont bien été enregistrées.
    # Composition du dictionnaire représentant les métadonnées qui doivent
    # apparaître dans le fichier de métadonnées.
    ${size}=  Get File Size  ${docgen01_fichier_signe_path}
    ${md} =  Create Dictionary
    ...  filename=document_genere_${docgen01_id}_signe.pdf
    ...  mimetype=application/pdf
    ...  size=${size}
    ...  titre=Établissement ${etab01_code} - Dossier ${dc01_di_si} - Courrier simple (signé)
    ...  description=document généré numérisé signé
    ...  application=openARIA
    ...  origine=téléversé
    ...  etablissement_code=${etab01_code}
    ...  etablissement_libelle=${etab01.libelle}
    ...  etablissement_siret=
    ...  etablissement_referentiel=false
    ...  etablissement_exploitant=${etab01.exp_prenom} ${etab01.exp_nom}
    ...  etablissement_adresse_numero=${etab01.adresse_numero}
    ...  etablissement_adresse_mention=
    ...  etablissement_adresse_voie=${etab01.adresse_voie}
    ...  etablissement_adresse_cp=${etab01.adresse_cp}
    ...  etablissement_adresse_ville=${etab01.adresse_ville}
    ...  etablissement_adresse_arrondissement=${etab01.adresse_arrondissement}
    ...  etablissement_ref_patrimoine=
    ...  dossier_coordination=${dc01_libelle}
    ...  dossier_instruction=${dc01_di_si}
    ...  signataire=
    ...  signataire_qualite=
    ...  date_signature=2016-01-10
    Les métadonnées (clé/valeur) doivent être présentes dans le fichier  ${md}  ${docgen01_fichier_signe_md_path}
    Les métadonnées (clé) ne doivent pas être présentes dans le fichier  ${md_no}  ${docgen01_fichier_signe_md_path}

    ##
    ##
    ##
    Depuis le contexte du document généré  ${docgen01_code_barres}
    Click On Form Portlet Action  courrier  modifier
    Input Text  css=#date_retour_signature  ${EMPTY}
    Click On Submit Button
    Valid Message Should Contain    Vos modifications ont bien été enregistrées.
    # Composition du dictionnaire représentant les métadonnées qui doivent
    # apparaître dans le fichier de métadonnées.
    ${size}=  Get File Size  ${docgen01_fichier_signe_path}
    ${md} =  Create Dictionary
    ...  filename=document_genere_${docgen01_id}_signe.pdf
    ...  mimetype=application/pdf
    ...  size=${size}
    ...  titre=Établissement ${etab01_code} - Dossier ${dc01_di_si} - Courrier simple (signé)
    ...  description=document généré numérisé signé
    ...  application=openARIA
    ...  origine=téléversé
    ...  etablissement_code=${etab01_code}
    ...  etablissement_libelle=${etab01.libelle}
    ...  etablissement_siret=
    ...  etablissement_referentiel=false
    ...  etablissement_exploitant=${etab01.exp_prenom} ${etab01.exp_nom}
    ...  etablissement_adresse_numero=${etab01.adresse_numero}
    ...  etablissement_adresse_mention=
    ...  etablissement_adresse_voie=${etab01.adresse_voie}
    ...  etablissement_adresse_cp=${etab01.adresse_cp}
    ...  etablissement_adresse_ville=${etab01.adresse_ville}
    ...  etablissement_adresse_arrondissement=${etab01.adresse_arrondissement}
    ...  etablissement_ref_patrimoine=
    ...  dossier_coordination=${dc01_libelle}
    ...  dossier_instruction=${dc01_di_si}
    ...  signataire=
    ...  signataire_qualite=
    ...  date_signature=
    Les métadonnées (clé/valeur) doivent être présentes dans le fichier  ${md}  ${docgen01_fichier_signe_md_path}
    Les métadonnées (clé) ne doivent pas être présentes dans le fichier  ${md_no}  ${docgen01_fichier_signe_md_path}

    ##
    ## Document 02 - Cas d'utilisation C & D
    ##
    Ajouter un particulier depuis un établissement    ${etab01_code}    Mandataire    M.    AQWEDC02    Jacques    true
    @{contacts_lies}    Create List    (Mandataire) M. AQWEDC02 Jacques
    ${params}    Create Dictionary    contacts_lies=@{contacts_lies}
    Ajouter le document généré depuis le contexte du dossier de coordination    ${dc01_libelle}     ${params}    Courrier simple    Courrier simple
    Click On Back Button In Subform
    Input Text  css=span#recherche_onglet input#recherchedyn  Courrier simple
    Click On Link  AQWEDC02 Jacques
    ${docgen02_id} =  Get Text  css=#sousform-courrier #courrier
    ${docgen02_code_barres} =  Get Text  css=#code_barres


    #
    Finaliser le document généré  ${docgen02_code_barres}
    ## => [courrier.om_fichier_finalise_courrier] (METADATA FILESTORAGE)
    ${docgen02_fichier_finalise_uid} =  Récupérer l'uid du fichier finalisé du document généré  ${docgen02_code_barres}
    ${docgen02_fichier_finalise_path} =  Récupérer le chemin vers le fichier correspondant à l'uid  ${docgen02_fichier_finalise_uid}
    ${docgen02_fichier_finalise_md_path} =  Récupérer le chemin vers le fichier de métadonnées correspondant à l'uid  ${docgen02_fichier_finalise_uid}
    Le fichier doit exister  ${docgen02_fichier_finalise_path}
    Le fichier doit exister  ${docgen02_fichier_finalise_md_path}
    # Composition du dictionnaire représentant les métadonnées qui doivent
    # apparaître dans le fichier de métadonnées.
    ${size}=  Get File Size  ${docgen02_fichier_finalise_path}
    ${md} =  Create Dictionary
    ...  filename=document_genere_${docgen02_id}.pdf
    ...  mimetype=application/pdf
    ...  size=${size}
    ...  titre=Établissement ${etab01_code} - Dossier ${dc01_libelle} - Courrier simple
    ...  description=document généré finalisé
    ...  application=openARIA
    ...  origine=généré
    ...  etablissement_code=${etab01_code}
    ...  etablissement_libelle=${etab01.libelle}
    ...  etablissement_siret=
    ...  etablissement_referentiel=false
    ...  etablissement_exploitant=${etab01.exp_prenom} ${etab01.exp_nom}
    ...  etablissement_adresse_numero=${etab01.adresse_numero}
    ...  etablissement_adresse_mention=
    ...  etablissement_adresse_voie=${etab01.adresse_voie}
    ...  etablissement_adresse_cp=${etab01.adresse_cp}
    ...  etablissement_adresse_ville=${etab01.adresse_ville}
    ...  etablissement_adresse_arrondissement=${etab01.adresse_arrondissement}
    ...  etablissement_ref_patrimoine=
    ...  dossier_coordination=${dc01_libelle}
    ...  dossier_instruction=
    ...  signataire=
    ...  signataire_qualite=
    ...  date_signature=
    Les métadonnées (clé/valeur) doivent être présentes dans le fichier  ${md}  ${docgen02_fichier_finalise_md_path}
    Les métadonnées (clé) ne doivent pas être présentes dans le fichier  ${md_no}  ${docgen02_fichier_finalise_md_path}

    #
    Ajouter le fichier signé du document généré  ${docgen02_code_barres}  numerisation2.pdf
    ## => [courrier.om_fichier_signe_courrier] (METADATA FILESTORAGE)
    ${docgen02_fichier_signe_uid} =  Récupérer l'uid du fichier signé du document généré  ${docgen02_code_barres}
    ${docgen02_fichier_signe_path} =  Récupérer le chemin vers le fichier correspondant à l'uid  ${docgen02_fichier_signe_uid}
    ${docgen02_fichier_signe_md_path} =  Récupérer le chemin vers le fichier de métadonnées correspondant à l'uid  ${docgen02_fichier_signe_uid}
    Le fichier doit exister  ${docgen02_fichier_signe_path}
    Le fichier doit exister  ${docgen02_fichier_signe_md_path}
    # Composition du dictionnaire représentant les métadonnées qui doivent
    # apparaître dans le fichier de métadonnées.
    ${size}=  Get File Size  ${docgen02_fichier_signe_path}
    ${md} =  Create Dictionary
    ...  filename=document_genere_${docgen02_id}_signe.pdf
    ...  mimetype=application/pdf
    ...  size=${size}
    ...  titre=Établissement ${etab01_code} - Dossier ${dc01_libelle} - Courrier simple (signé)
    ...  description=document généré numérisé signé
    ...  application=openARIA
    ...  origine=téléversé
    ...  etablissement_code=${etab01_code}
    ...  etablissement_libelle=${etab01.libelle}
    ...  etablissement_siret=
    ...  etablissement_referentiel=false
    ...  etablissement_exploitant=${etab01.exp_prenom} ${etab01.exp_nom}
    ...  etablissement_adresse_numero=${etab01.adresse_numero}
    ...  etablissement_adresse_mention=
    ...  etablissement_adresse_voie=${etab01.adresse_voie}
    ...  etablissement_adresse_cp=${etab01.adresse_cp}
    ...  etablissement_adresse_ville=${etab01.adresse_ville}
    ...  etablissement_adresse_arrondissement=${etab01.adresse_arrondissement}
    ...  etablissement_ref_patrimoine=
    ...  dossier_coordination=${dc01_libelle}
    ...  dossier_instruction=
    ...  signataire=
    ...  signataire_qualite=
    ...  date_signature=
    Les métadonnées (clé/valeur) doivent être présentes dans le fichier  ${md}  ${docgen02_fichier_signe_md_path}
    Les métadonnées (clé) ne doivent pas être présentes dans le fichier  ${md_no}  ${docgen02_fichier_signe_md_path}

    ##
    ## Document 03 - Cas d'utilisation E & F
    ##
    Ajouter un particulier depuis un établissement    ${etab01_code}    Mandataire    M.    AQWEDC03    Jacques    true
    @{contacts_lies}    Create List    (Mandataire) M. AQWEDC03 Jacques
    ${params}    Create Dictionary    contacts_lies=@{contacts_lies}
    Ajouter le document généré depuis le contexte de l'établissement    ${etab01_code}  ${etab01.libelle}     ${params}    Courrier simple    Courrier simple
    Click On Back Button In Subform
    Input Text  css=span#recherche_onglet input#recherchedyn  Courrier simple
    Click On Link  AQWEDC03 Jacques
    ${docgen03_id} =  Get Text  css=#sousform-courrier #courrier
    ${docgen03_code_barres} =  Get Text  css=#code_barres

    #
    Finaliser le document généré    ${docgen03_code_barres}
    ## => [courrier.om_fichier_finalise_courrier] (METADATA FILESTORAGE)
    ${docgen03_fichier_finalise_uid} =  Récupérer l'uid du fichier finalisé du document généré  ${docgen03_code_barres}
    ${docgen03_fichier_finalise_path} =  Récupérer le chemin vers le fichier correspondant à l'uid  ${docgen03_fichier_finalise_uid}
    ${docgen03_fichier_finalise_md_path} =  Récupérer le chemin vers le fichier de métadonnées correspondant à l'uid  ${docgen03_fichier_finalise_uid}
    Le fichier doit exister  ${docgen03_fichier_finalise_path}
    Le fichier doit exister  ${docgen03_fichier_finalise_md_path}
    # Composition du dictionnaire représentant les métadonnées qui doivent
    # apparaître dans le fichier de métadonnées.
    ${size}=  Get File Size  ${docgen03_fichier_finalise_path}
    ${md} =  Create Dictionary
    ...  filename=document_genere_${docgen03_id}.pdf
    ...  mimetype=application/pdf
    ...  size=${size}
    ...  titre=Établissement ${etab01_code} - Courrier simple
    ...  description=document généré finalisé
    ...  application=openARIA
    ...  origine=généré
    ...  etablissement_code=${etab01_code}
    ...  etablissement_libelle=${etab01.libelle}
    ...  etablissement_siret=
    ...  etablissement_referentiel=false
    ...  etablissement_exploitant=${etab01.exp_prenom} ${etab01.exp_nom}
    ...  etablissement_adresse_numero=${etab01.adresse_numero}
    ...  etablissement_adresse_mention=
    ...  etablissement_adresse_voie=${etab01.adresse_voie}
    ...  etablissement_adresse_cp=${etab01.adresse_cp}
    ...  etablissement_adresse_ville=${etab01.adresse_ville}
    ...  etablissement_adresse_arrondissement=${etab01.adresse_arrondissement}
    ...  etablissement_ref_patrimoine=
    ...  dossier_coordination=
    ...  dossier_instruction=
    ...  signataire=
    ...  signataire_qualite=
    ...  date_signature=
    Les métadonnées (clé/valeur) doivent être présentes dans le fichier  ${md}  ${docgen03_fichier_finalise_md_path}
    Les métadonnées (clé) ne doivent pas être présentes dans le fichier  ${md_no}  ${docgen03_fichier_finalise_md_path}

    Ajouter le fichier signé du document généré  ${docgen03_code_barres}  numerisation2.pdf
    ## => [courrier.om_fichier_signe_courrier] (METADATA FILESTORAGE)
    ${docgen03_fichier_signe_uid} =  Récupérer l'uid du fichier signé du document généré  ${docgen03_code_barres}
    ${docgen03_fichier_signe_path} =  Récupérer le chemin vers le fichier correspondant à l'uid  ${docgen03_fichier_signe_uid}
    ${docgen03_fichier_signe_md_path} =  Récupérer le chemin vers le fichier de métadonnées correspondant à l'uid  ${docgen03_fichier_signe_uid}
    Le fichier doit exister  ${docgen03_fichier_signe_path}
    Le fichier doit exister  ${docgen03_fichier_signe_md_path}
    # Composition du dictionnaire représentant les métadonnées qui doivent
    # apparaître dans le fichier de métadonnées.
    ${size}=  Get File Size  ${docgen03_fichier_signe_path}
    ${md} =  Create Dictionary
    ...  filename=document_genere_${docgen03_id}_signe.pdf
    ...  mimetype=application/pdf
    ...  size=${size}
    ...  titre=Établissement ${etab01_code} - Courrier simple (signé)
    ...  description=document généré numérisé signé
    ...  application=openARIA
    ...  origine=téléversé
    ...  etablissement_code=${etab01_code}
    ...  etablissement_libelle=${etab01.libelle}
    ...  etablissement_siret=
    ...  etablissement_referentiel=false
    ...  etablissement_exploitant=${etab01.exp_prenom} ${etab01.exp_nom}
    ...  etablissement_adresse_numero=${etab01.adresse_numero}
    ...  etablissement_adresse_mention=
    ...  etablissement_adresse_voie=${etab01.adresse_voie}
    ...  etablissement_adresse_cp=${etab01.adresse_cp}
    ...  etablissement_adresse_ville=${etab01.adresse_ville}
    ...  etablissement_adresse_arrondissement=${etab01.adresse_arrondissement}
    ...  etablissement_ref_patrimoine=
    ...  dossier_coordination=
    ...  dossier_instruction=
    ...  signataire=
    ...  signataire_qualite=
    ...  date_signature=
    Les métadonnées (clé/valeur) doivent être présentes dans le fichier  ${md}  ${docgen03_fichier_signe_md_path}
    Les métadonnées (clé) ne doivent pas être présentes dans le fichier  ${md_no}  ${docgen03_fichier_signe_md_path}


Suivi du document généré par code barres

    [Documentation]    Met à jour les dates de suivi du documets généré en
    ...    passant par le menu de suivi par code barres.

    #
    Depuis la page d'accueil    secretaire-si    secretaire-si
    #
    Depuis le contexte du document généré par le menu suivi par code barres    ${code_barres_1}
    # On clique sur l'action modifier
    Click On Form Portlet Action    courrier   modifier
    #
    Suivi des dates du documente généré    ${DATE_FORMAT_DD/MM/YYYY}
    # On valide le formulaire
    Click On Submit Button
    # On vérifie le message de validation
    Valid Message Should Contain    Vos modifications ont bien été enregistrées.

    # TNR Après finalisation du document, on vient de modifier le document
    # généré via le formulaire, le fichier finalisé doit toujours être
    # disponible au téléchargement. Un bug supprimé l'uid du fichier de la base
    # de données.
    Depuis le contexte du document généré par le menu suivi par code barres    ${code_barres_1}
    Element Should Contain  css=#om_fichier_finalise_courrier  Télécharger
    Click Element  css=#om_fichier_finalise_courrier span.reqmo-16 a
    # On ouvre le PDF
    Open PDF  ${OM_PDF_TITLE}
    # On vérifie le contenu
    WUX  Page Should Contain  Madame, Monsieur
    Page Should Not Contain  DOCUMENT DE TRAVAIL
    # On revient à la fenêtre principale
    Close PDF


Documents générés à éditer

    [Documentation]  Vérifie le widget de tableau de bord, le tableau et le
    ...  formulaire des documents générés à éditer.
    ...  Les critères de sélection sont :
    ...  - le document généré est finalisé
    ...  - date d'envoi à signature vide
    ...  - date retour signature vide
    ...  - date envoi AR vide
    ...  - date retour AR vide

    Depuis la page d'accueil  cadre-si  cadre-si

    # Il n'est pas possible de vérifie que le widget ne soit pas affiché à cause
    # des tests précédents qui pourraient créer des courriers respectant les
    # critères de sélection

    # Ajoute le document généré
    Ajouter un particulier depuis un établissement    ${etab01_code}    Mandataire    M.    Talon    Michel    true
    @{contacts_lies}    Create List    (Mandataire) M. Talon Michel
    ${params}    Create Dictionary    contacts_lies=@{contacts_lies}
    Ajouter le document généré depuis le contexte de l'établissement    ${etab01_code}  ${etab01.libelle}     ${params}    Courrier simple    Courrier par défaut
    Click On Back Button In Subform
    Input Text  css=span#recherche_onglet input#recherchedyn  Courrier par défaut
    Click On Link  Talon Michel
    ${courrier} =  Get Text  css=#code_barres

    # On finalise le document
    Finaliser le document généré  ${courrier}

    # On vérifie que le courrier est présent dans le listing des courriers à
    # éditer
    Go To Dashboard
    Element Should Be Visible  css=.widget_documents_generes_a_editer
    Click Link  css=.widget_documents_generes_a_editer a
    Page Title Should Be  Suivi > Documents Générés > À Éditer
    Use Simple Search  code barres  ${courrier}
    Element Should Contain  css=table.tab-tab  ${courrier}

    # On saisit la date de retour signature
    Click On Link  ${courrier}
    Click On Form Portlet Action  courrier_a_editer  modifier
    Suivi des dates du documente généré  ${DATE_FORMAT_DD/MM/YYYY}
    Click On Submit Button
    Valid Message Should Contain    Vos modifications ont bien été enregistrées.

    # On retourne sur le listing pour vérifier que le courrier n'est plus
    # présent
    Depuis le listing  courrier_a_editer
    Submenu In Menu Should Be Selected  suivi  courrier_a_editer
    Page Title Should Be  Suivi > Documents Générés > À Éditer
    Use Simple Search  code barres  ${courrier}
    Element Should Not Contain  css=table.tab-tab  ${courrier}

    # On vérifie le filtre entre service
    # Ajoute les documents générés
    # Sur établissement
    Ajouter un particulier depuis un établissement    ${etab01_code}    Mandataire    M.    Huard    Raymond    true
    @{contacts_lies}    Create List    (Mandataire) M. Huard Raymond
    ${params}    Create Dictionary    contacts_lies=@{contacts_lies}
    Ajouter le document généré depuis le contexte de l'établissement    ${etab01_code}  ${etab01.libelle}     ${params}    Courrier simple    Courrier par défaut
    Click On Back Button In Subform
    Input Text  css=span#recherche_onglet input#recherchedyn  Courrier par défaut
    Click On Link  Huard Raymond
    ${courrier_1} =  Get Text  css=#code_barres
    Finaliser le document généré  ${courrier_1}
    # Sur dossier d'instruction
    Ajouter un particulier depuis un établissement    ${etab01_code}    Mandataire    M.    Gour    Pascal    true
    @{contacts_lies}    Create List    (Mandataire) M. Gour Pascal
    ${params}    Create Dictionary    contacts_lies=@{contacts_lies}
    Ajouter le document généré depuis le contexte du dossier d'instruction    ${dc01_di_si}     ${params}    Courrier simple    Courrier par défaut
    Click On Back Button In Subform
    Input Text  css=span#recherche_onglet input#recherchedyn  Courrier par défaut
    Click On Link  Gour Pascal
    ${courrier_2} =  Get Text  css=#code_barres
    Finaliser le document généré  ${courrier_2}
    # En tant que cadre-si
    Depuis le listing  courrier_a_editer
    Use Simple Search  code barres  ${courrier_1}
    Element Should Contain  css=table.tab-tab  ${courrier_1}
    Use Simple Search  code barres  ${courrier_2}
    Element Should Contain  css=table.tab-tab  ${courrier_2}
    # En tant que cadre-acc
    Depuis la page d'accueil  cadre-acc  cadre-acc
    Depuis le listing  courrier_a_editer
    Use Simple Search  code barres  ${courrier_1}
    Element Should Contain  css=table.tab-tab  ${courrier_1}
    Use Simple Search  code barres  ${courrier_2}
    Element Should Not Contain  css=table.tab-tab  ${courrier_2}


Documents générés en attente de signature

    [Documentation]  Vérifie le widget de tableau de bord, le tableau et le
    ...  formulaire des documents générés en attente de signature.
    ...  Les critères de sélection sont :
    ...  - le document est finalisé
    ...  - date d'envoi à signature rempli
    ...  - date de retour de signature vide
    ...  - date d'envoi AR vide
    ...  - date de retour AR vide

    Depuis la page d'accueil  cadre-si  cadre-si

    # Il n'est pas possible de vérifie que le widget ne soit pas affiché à cause
    # des tests précédents qui pourraient créer des courriers respectant les
    # critères de sélection

    # Ajoute le document généré
    Ajouter un particulier depuis un établissement    ${etab01_code}    Mandataire    M.    Rubio    Daniel    true
    @{contacts_lies}    Create List    (Mandataire) M. Rubio Daniel
    ${params}    Create Dictionary    contacts_lies=@{contacts_lies}
    Ajouter le document généré depuis le contexte de l'établissement    ${etab01_code}  ${etab01.libelle}     ${params}    Courrier simple    Courrier par défaut
    Click On Back Button In Subform
    Input Text  css=span#recherche_onglet input#recherchedyn  Courrier par défaut
    Click On Link  Rubio Daniel
    ${courrier} =  Get Text  css=#code_barres

    # On finalise le document et on modifie la date d'envoi signature
    Finaliser le document généré  ${courrier}
    Modifier le document généré depuis le menu  ${courrier}  ${DATE_FORMAT_DD/MM/YYYY}

    # On vérifie que le courrier est présent dans le listing des courriers en
    # attente de signature
    Go To Dashboard
    Element Should Be Visible  css=.widget_documents_generes_attente_signature
    Click Link  css=.widget_documents_generes_attente_signature a
    Page Title Should Be  Suivi > Documents Générés > Attente Signature
    Use Simple Search  code barres  ${courrier}
    Element Should Contain  css=table.tab-tab  ${courrier}

    # On saisit la date de retour signature
    Click On Link  ${courrier}
    Click On Form Portlet Action  courrier_attente_signature  modifier
    Suivi des dates du documente généré  null  ${DATE_FORMAT_DD/MM/YYYY}
    Click On Submit Button
    Valid Message Should Contain    Vos modifications ont bien été enregistrées.

    # On retourne sur le listing pour vérifier que le courrier n'est plus
    # présent
    Depuis le listing  courrier_attente_signature
    Submenu In Menu Should Be Selected  suivi  courrier_attente_signature
    Page Title Should Be  Suivi > Documents Générés > Attente Signature
    Use Simple Search  code barres  ${courrier}
    Element Should Not Contain  css=table.tab-tab  ${courrier}

    # On vérifie le filtre entre service
    # Ajoute les documents générés
    # Sur établissement
    Ajouter un particulier depuis un établissement    ${etab01_code}    Mandataire    M.    Croquetaigne    Patrick    true
    @{contacts_lies}    Create List    (Mandataire) M. Croquetaigne Patrick
    ${params}    Create Dictionary    contacts_lies=@{contacts_lies}
    Ajouter le document généré depuis le contexte de l'établissement    ${etab01_code}  ${etab01.libelle}     ${params}    Courrier simple    Courrier par défaut
    Click On Back Button In Subform
    Input Text  css=span#recherche_onglet input#recherchedyn  Courrier par défaut
    Click On Link  Croquetaigne Patrick
    ${courrier_1} =  Get Text  css=#code_barres
    Finaliser le document généré  ${courrier_1}
    Modifier le document généré depuis le menu  ${courrier_1}  ${DATE_FORMAT_DD/MM/YYYY}
    # Sur dossier d'instruction
    Ajouter un particulier depuis un établissement    ${etab01_code}    Mandataire    M.    Émond    Soren    true
    @{contacts_lies}    Create List    (Mandataire) M. Émond Soren
    ${params}    Create Dictionary    contacts_lies=@{contacts_lies}
    Ajouter le document généré depuis le contexte du dossier d'instruction    ${dc01_di_si}     ${params}    Courrier simple    Courrier par défaut
    Click On Back Button In Subform
    Input Text  css=span#recherche_onglet input#recherchedyn  Courrier par défaut
    Click On Link  Émond Soren
    ${courrier_2} =  Get Text  css=#code_barres
    Finaliser le document généré  ${courrier_2}
    Modifier le document généré depuis le menu  ${courrier_2}  ${DATE_FORMAT_DD/MM/YYYY}
    # En tant que cadre-si
    Depuis le listing  courrier_attente_signature
    Use Simple Search  code barres  ${courrier_1}
    Element Should Contain  css=table.tab-tab  ${courrier_1}
    Use Simple Search  code barres  ${courrier_2}
    Element Should Contain  css=table.tab-tab  ${courrier_2}
    # En tant que cadre-acc
    Depuis la page d'accueil  cadre-acc  cadre-acc
    Depuis le listing  courrier_attente_signature
    Use Simple Search  code barres  ${courrier_1}
    Element Should Contain  css=table.tab-tab  ${courrier_1}
    Use Simple Search  code barres  ${courrier_2}
    Element Should Not Contain  css=table.tab-tab  ${courrier_2}


Documents générés en attente d'AR

    [Documentation]  Vérifie le widget de tableau de bord, le tableau et le
    ...  formulaire des documents générés en attente de retour d'AR.
    ...  Les critères de sélection sont :
    ...  - le document est finalisé
    ...  - date d'envoi AR rempli
    ...  - date de retour AR vide

    Depuis la page d'accueil  cadre-si  cadre-si

    # Il n'est pas possible de vérifie que le widget ne soit pas affiché à cause
    # des tests précédents qui pourraient créer des courriers respectant les
    # critères de sélection

    # Ajoute le document généré
    Ajouter un particulier depuis un établissement    ${etab01_code}    Mandataire    M.    Loiseau    Bruno    true
    @{contacts_lies}    Create List    (Mandataire) M. Loiseau Bruno
    ${params}    Create Dictionary    contacts_lies=@{contacts_lies}
    Ajouter le document généré depuis le contexte de l'établissement    ${etab01_code}  ${etab01.libelle}     ${params}    Courrier simple    Courrier par défaut
    Click On Back Button In Subform
    Input Text  css=span#recherche_onglet input#recherchedyn  Courrier par défaut
    Click On Link  Loiseau Bruno
    ${courrier} =  Get Text  css=#code_barres

    # On finalise le document
    Finaliser le document généré  ${courrier}
    Modifier le document généré depuis le menu  ${courrier}  date_envoi_rar=${DATE_FORMAT_DD/MM/YYYY}

    # On vérifie que le courrier est présent dans le listing des courriers en attente
    # de retour AR
    Go To Dashboard
    Element Should Be Visible  css=.widget_documents_generes_attente_retour_ar
    Click Link  css=.widget_documents_generes_attente_retour_ar a
    Page Title Should Be  Suivi > Documents Générés > Attente Retour AR
    Use Simple Search  code barres  ${courrier}
    Element Should Contain  css=table.tab-tab  ${courrier}

    # On saisit la date de retour AR
    Click On Link  ${courrier}
    Click On Form Portlet Action  courrier_attente_retour_ar  modifier
    Suivi des dates du documente généré  date_retour_rar=${DATE_FORMAT_DD/MM/YYYY}
    Click On Submit Button
    Valid Message Should Contain    Vos modifications ont bien été enregistrées.

    # On retourne sur le listing pour vérifier que le courrier n'est plus
    # présent
    Depuis le listing  courrier_attente_retour_ar
    Submenu In Menu Should Be Selected  suivi  courrier_attente_retour_ar
    Page Title Should Be  Suivi > Documents Générés > Attente Retour AR
    Use Simple Search  code barres  ${courrier}
    Element Should Not Contain  css=table.tab-tab  ${courrier}

    # On vérifie le filtre entre service
    # Ajoute les documents générés
    # Sur établissement
    Ajouter un particulier depuis un établissement    ${etab01_code}    Mandataire    M.    Desnoyer    Marc    true
    @{contacts_lies}    Create List    (Mandataire) M. Desnoyer Marc
    ${params}    Create Dictionary    contacts_lies=@{contacts_lies}
    Ajouter le document généré depuis le contexte de l'établissement    ${etab01_code}  ${etab01.libelle}     ${params}    Courrier simple    Courrier par défaut
    Click On Back Button In Subform
    Input Text  css=span#recherche_onglet input#recherchedyn  Courrier par défaut
    Click On Link  Desnoyer Marc
    ${courrier_1} =  Get Text  css=#code_barres
    Finaliser le document généré  ${courrier_1}
    Modifier le document généré depuis le menu  ${courrier_1}  date_envoi_rar=${DATE_FORMAT_DD/MM/YYYY}
    # Sur dossier d'instruction
    Ajouter un particulier depuis un établissement    ${etab01_code}    Mandataire    M.    Morneau    Denis    true
    @{contacts_lies}    Create List    (Mandataire) M. Morneau Denis
    ${params}    Create Dictionary    contacts_lies=@{contacts_lies}
    Ajouter le document généré depuis le contexte du dossier d'instruction    ${dc01_di_si}     ${params}    Courrier simple    Courrier par défaut
    Click On Back Button In Subform
    Input Text  css=span#recherche_onglet input#recherchedyn  Courrier par défaut
    Click On Link  Morneau Denis
    ${courrier_2} =  Get Text  css=#code_barres
    Finaliser le document généré  ${courrier_2}
    Modifier le document généré depuis le menu  ${courrier_2}  date_envoi_rar=${DATE_FORMAT_DD/MM/YYYY}
    # En tant que cadre-si
    Depuis le listing  courrier_attente_retour_ar
    Use Simple Search  code barres  ${courrier_1}
    Element Should Contain  css=table.tab-tab  ${courrier_1}
    Use Simple Search  code barres  ${courrier_2}
    Element Should Contain  css=table.tab-tab  ${courrier_2}
    # En tant que cadre-acc
    Depuis la page d'accueil  cadre-acc  cadre-acc
    Depuis le listing  courrier_attente_retour_ar
    Use Simple Search  code barres  ${courrier_1}
    Element Should Contain  css=table.tab-tab  ${courrier_1}
    Use Simple Search  code barres  ${courrier_2}
    Element Should Not Contain  css=table.tab-tab  ${courrier_2}


Variable de remplacement &lettre_type_edition
    [Documentation]  Variable de remplacement &lettre_type_identifiant.
    ...
    ...  L'objet de ce 'TestCase' est de vérifier le bon fonctionnement de la
    ...  variable de remplacement &lettre_type_identifiant. Elle permet d'afficher
    ...  l’identifiant de la lettre-type utilisée. On vérifie son bon fonctionnement
    ...  en édition simple et en édition multiple pour vérifier que chaque page possède
    ...  l'identifiant de sa propre lettre-type.
    ...
    ...  Le scénario est le suivant :
    ...  - créer une lettre-type 01 contenant la variable de remplacement et
    ...    son modèle d'édition 01 rattaché,
    ...  - créer une lettre-type 02 contenant la variable de remplacement et
    ...    son modèle d'édition 02 rattaché,
    ...  - ajouter un document généré 01 qui se base sur le modèle d'édition
    ...    01, le finaliser et vérifier que la variable de remplacement est
    ...    correctement remplacée dans l'édition PDF (cas édition simple),
    ...  - ajouter un document généré 02 qui se base sur le modèle d'édition
    ...    02, avec comme courrier joint le document généré 01, le finlaiser
    ...    et vérifier que la page 1 contient la variable de remplacement de
    ...    la lettre-type 02 et pas celle de la 01 et que la page 2 contient
    ...    la variable de remplacement de la lettre-type 01 et pas celle de la
    ...    02 (cas édition multiple).

    Depuis la page d'accueil    admin    admin

    # lettre-type 01 / modèle d'édition 01
    &{lt_l_01} =  Create Dictionary
    ...  id=t140subsvarlt1
    Ajouter la lettre-type depuis le menu
    ...  ${lt_l_01.id}
    ...  ${lt_l_01.id}
    ...  <p>&lettre_type_identifiant</p>
    ...  <p><span style="font-weight: bold;">Ma lettre-type 1 pour tester la variable de remplacement qui permet d afficher l identifiant de la lettre-type utilisée</span></p>
    ...  Contexte 'document genere'
    ...  true
    &{me_l_01} =  Create Dictionary
    ...  code=t140subsvarme1
    ...  libelle=Modèle d'édition Test 140 subsvar ME1
    ...  courrier_type=Courrier simple
    ...  om_lettretype_id=${lt_l_01.id}
    Ajouter le modèle d'édition  ${me_l_01}

    # lettre-type 02 / modèle d'édition 02
    &{lt_l_02} =  Create Dictionary
    ...  id=t140subsvarlt2
    Ajouter la lettre-type depuis le menu
    ...  ${lt_l_02.id}
    ...  ${lt_l_02.id}
    ...  <p>&lettre_type_identifiant</p>
    ...  <p><span style="font-weight: bold;">Ma lettre-type 2 pour tester la variable de remplacement qui permet d afficher l identifiant de la lettre-type utilisée</span></p>
    ...  Contexte 'document genere'
    ...  true
    &{me_l_02} =  Create Dictionary
    ...  code=t140subsvarme2
    ...  libelle=Modèle d'édition Test 140 subsvar ME2
    ...  courrier_type=Courrier simple
    ...  om_lettretype_id=${lt_l_02.id}
    Ajouter le modèle d'édition  ${me_l_02}

    #
    Ajouter un particulier depuis un établissement    ${etab01_code}    Mandataire    M.    DUPONTT140subsvar    Paul    true
    @{contacts_lies}    Create List    (Mandataire) M. DUPONTT140subsvar Paul
    ${params}    Create Dictionary    contacts_lies=@{contacts_lies}

    # document généré 01 (cas édition simple)
    Ajouter le document généré depuis le contexte de l'établissement
    ...  ${etab01_code}
    ...  ${etab01.libelle}
    ...  ${params}
    ...  Courrier simple
    ...  ${me_l_01.libelle}
    ${dg_l_01_code_barres} =  Get Text  css=#code_barres
    Depuis le contexte du document généré par le menu suivi par code barres  ${dg_l_01_code_barres}
    ${dg_l_01_id} =  Get Text  css=#courrier
    Finaliser le document généré  ${dg_l_01_code_barres}
    Depuis le contexte du document généré par le menu suivi par code barres  ${dg_l_01_code_barres}
    Element Should Contain  css=#om_fichier_finalise_courrier  Télécharger
    Click Element  css=#om_fichier_finalise_courrier span.reqmo-16 a
    Open PDF  ${OM_PDF_TITLE}
    WUX  PDF Page Number Should Contain  1  ${lt_l_01.id}
    Close PDF

    # document généré 02 (cas édition multiple)
    Ajouter le document généré depuis le contexte de l'établissement
    ...  ${etab01_code}
    ...  ${etab01.libelle}
    ...  ${params}
    ...  Courrier simple
    ...  ${me_l_02.libelle}
    ${dg_l_02_code_barres} =  Get Text  css=#code_barres
    Depuis le contexte du document généré  ${dg_l_02_code_barres}
    ${dg_l_02_id} =  Get Text  css=#courrier
    Click On Form Portlet Action  courrier  modifier
    WUX  Select value in autocomplete  courrier  ${dg_l_01_code_barres}  ${dg_l_01_id} - ${DATE_FORMAT_DD/MM/YYYY} - ${etab01.libelle} - COUS - ${me_l_01.code} - Mandataire M. DUPONTT140subsvar Paul  ${dg_l_01_id}
    Click On Submit Button
    Valid Message Should Contain    Vos modifications ont bien été enregistrées.
    Finaliser le document généré  ${dg_l_02_code_barres}
    Depuis le contexte du document généré par le menu suivi par code barres    ${dg_l_02_code_barres}
    Element Should Contain  css=#om_fichier_finalise_courrier  Télécharger
    Click Element  css=#om_fichier_finalise_courrier span.reqmo-16 a
    Open PDF  ${OM_PDF_TITLE}
    WUX  PDF Page Number Should Contain  1  ${lt_l_02.id}
    PDF Page Number Should Not Contain  1  ${lt_l_01.id}
    WUX  PDF Page Number Should Contain  2  ${lt_l_01.id}
    PDF Page Number Should Not Contain  2  ${lt_l_02.id}
    Close PDF
