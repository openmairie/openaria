*** Settings ***
Resource  resources/resources.robot
Suite Setup  For Suite Setup
Suite Teardown  For Suite Teardown
Documentation  Les établissements...


*** Test Cases ***
Constitution d'un jeu de données

    [Documentation]  L'objet de ce 'Test Case' est de constituer un jeu de
    ...  données cohérent pour les scénarios fonctionnels qui suivent.

    #
    Depuis la page d'accueil    cadre-si    cadre-si

    # Création d'un établissement
    &{etab01} =  Create Dictionary
    ...  libelle=JEUX DE PLEIN AIR
    ...  etablissement_nature=ERP Référentiel
    ...  etablissement_type=PA
    ...  etablissement_categorie=1
    ...  si_autorite_competente_visite=Commission communale de sécurité
    ...  adresse_numero=12
    ...  adresse_numero2=bis
    ...  adresse_voie=RUE DE LA REPUBLIQUE
    ...  adresse_cp=13001
    ...  adresse_ville=MARSEILLE
    ...  adresse_arrondissement=1er
    ...  siret=73482742011234
    ${etab01_code} =  Ajouter l'établissement  ${etab01}
    ${etab01_titre} =  Set Variable  ${etab01_code} - ${etab01.libelle}
    Set Suite Variable  ${etab01}
    Set Suite Variable  ${etab01_code}
    Set Suite Variable  ${etab01_titre}

    # Création d'un établissement
    &{etab02} =  Create Dictionary
    ...  libelle=MAGASIN SPAR TEST010
    ...  etablissement_nature=ERP Référentiel
    ...  etablissement_type=M
    ...  etablissement_categorie=1
    ...  si_autorite_competente_visite=Commission communale de sécurité
    ...  adresse_numero=31
    ...  adresse_voie=RUE DE ROME
    ...  adresse_cp=13006
    ...  adresse_ville=MARSEILLE
    ...  adresse_arrondissement=6ème
    ...  siret=73282932000074
    ...  exp_civilite=M.
    ...  exp_nom=Gaultier
    ...  exp_prenom=Jacques
    ...  etablissement_etat=Ouvert
    ${etab02_code} =  Ajouter l'établissement  ${etab02}
    ${etab02_titre} =  Set Variable  ${etab02_code} - ${etab02.libelle}
    Set Suite Variable  ${etab02}
    Set Suite Variable  ${etab02_code}
    Set Suite Variable  ${etab02_titre}

    # Création d'un établissement avec un exploitant
    &{etab03} =  Create Dictionary
    ...  libelle=KGB
    ...  etablissement_nature=ERP Référentiel
    ...  etablissement_type=PA
    ...  etablissement_categorie=3
    ...  si_autorite_competente_visite=Commission communale de sécurité
    ...  exp_civilite=M.
    ...  exp_nom=Poutine
    ...  exp_prenom=Vladimir
    ...  exp_adresse_numero=1
    ...  exp_adresse_cp=101000
    ...  exp_adresse_ville=Moscou
    ...  exp_pays=Russie
    ${etab03_code} =  Ajouter l'établissement  ${etab03}
    ${etab03_titre} =  Set Variable  ${etab03_code} - ${etab03.libelle}
    Set Suite Variable  ${etab03}
    Set Suite Variable  ${etab03_code}
    Set Suite Variable  ${etab03_titre}

    # Création d'un établissement avec une date de visite previsionnel à J+300
    &{etab04} =  Create Dictionary
    ...  libelle=MAGASIN SPAR ENDOUME
    ...  etablissement_nature=ERP Référentiel
    ...  etablissement_type=M
    ...  etablissement_categorie=1
    ...  si_autorite_competente_visite=Commission communale de sécurité
    ...  adresse_numero=198
    ...  adresse_voie=RUE D ENDOUME
    ...  adresse_cp=13007
    ...  adresse_ville=MARSEILLE
    ...  adresse_arrondissement=7ème
    ...  siret=73282932000074
    ...  exp_civilite=M.
    ...  exp_nom=Guillaume
    ...  exp_prenom=Canet
    ...  etablissement_etat=Ouvert
    ...  si_derniere_visite_periodique_date=10/01/2016
    ${etab04_code} =  Ajouter l'établissement  ${etab04}
    ${etab04_titre} =  Set Variable  ${etab04_code} - ${etab04.libelle}
    Set Suite Variable  ${etab04}
    Set Suite Variable  ${etab04_code}
    Set Suite Variable  ${etab04_titre}

    # Création d'un établissement
    &{etab05} =  Create Dictionary
    ...  libelle=ETBLISSEMENT05 TEST010
    ...  etablissement_nature=ERP Référentiel
    ...  etablissement_type=M
    ...  etablissement_categorie=1
    ...  si_autorite_competente_visite=Commission communale de sécurité
    ...  adresse_numero=305
    ...  adresse_voie=RUE D ENDOUME
    ...  adresse_cp=13007
    ...  adresse_ville=MARSEILLE
    ...  adresse_arrondissement=7ème
    ...  siret=73282932000074
    ...  exp_civilite=M.
    ...  exp_nom=Andre
    ...  exp_prenom=Pierre
    ...  etablissement_etat=Ouvert
    ...  si_derniere_visite_periodique_date=10/01/2015
    ${etab05_code} =  Ajouter l'établissement  ${etab05}
    ${etab05_titre} =  Set Variable  ${etab05_code} - ${etab05.libelle}
    Set Suite Variable  ${etab05}
    Set Suite Variable  ${etab05_code}
    Set Suite Variable  ${etab05_titre}

    # Création d'un établissement
    &{etab06} =  Create Dictionary
    ...  libelle=ETABLISSEMENT06 TEST010
    ...  etablissement_nature=ERP potentiel
    ...  etablissement_type=M
    ...  etablissement_categorie=1
    ...  adresse_numero=305
    ...  adresse_voie=RUE D ENDOUME
    ...  adresse_cp=13007
    ...  adresse_ville=MARSEILLE
    ...  adresse_arrondissement=7ème
    ...  exp_civilite=M.
    ...  exp_nom=Martin
    ...  exp_prenom=Pierre
    ...  etablissement_etat=non suivi
    ${etab06_code} =  Ajouter l'établissement  ${etab06}
    ${etab06_titre} =  Set Variable  ${etab06_code} - ${etab06.libelle}
    Set Suite Variable  ${etab06}
    Set Suite Variable  ${etab06_code}
    Set Suite Variable  ${etab06_titre}


Ajouter un établissement
    [Documentation]  Ce 'Test Case' permet de vérifier l'ajout d'un établissement.
    ...  L'objectif est de vérifier les spécificités du formulaire d'ajout.
    ...
    ...  - ...
    ...  - TNR - La saisie de trop de caractères sur certains champs du formulaire de l'établissement produisait une erreur qui ne créait pas l'établissement.
    ...  - ...

    Depuis la page d'accueil    admin    admin

    # TNR - Le scénario est de saisir plus de carctères que nécessaires dans le formulaire
    # d'ajout et de vérifier que l'établissement a bien été créé avec les valeurs
    # (mais seulement avec le nombre max de caractères), les autres n'ont pas pu être
    # saisies dans le formulaire.
    # Création de l'établissement avec des valeurs trop grandes
    &{etab_l_01} =  Create Dictionary
    ...  exp_civilite=M.
    ...  exp_nom=Nam quis nulla. Integer malesuada. In in enim a arcu imperdiet malesuada. Sed vel lectus. Donec odioA
    ...  exp_prenom=Nam quis nulla. Integer malesuada. In in enim a arcu imperdiet malesuada. Sed vel lectus. Donec odioA
    ...  exp_adresse_numero=00000000
    ...  exp_adresse_numero2=0000000000
    ...  exp_adresse_voie=Nam quis nulla. Integer malesuada. In in enim a arcu imperdiet malesuada. Sed vel lectus. Donec odio urna, tempus molestie, porttitor ut, iaculis quis, sem. Phasellus rhoncus. Aenean id metus id velit ullamcorper pulvinar. Vestibulum fermentum tortor id mA
    ...  exp_adresse_complement=Nam quis nulla. Integer malesuada. In inA
    ...  exp_adresse_cp=000000
    ...  exp_adresse_ville=Nam quis nulla. Integer malesuada. In in
    ...  exp_lieu_dit=Nam quis nulla. Integer malesuada. In i
    ...  exp_boite_postale=00000
    ...  exp_cedex=00000
    ...  exp_pays=Nam quis nulla. Integer malesuada. In in
    ...  adresse_numero=00000000
    ...  si_effectif_public=00000000
    ...  si_effectif_personnel=00000000
    ...  si_periodicite_visites=00000000
    ...  si_personnel_jour=00000000
    ...  si_personnel_nuit=00000000
    ...  libelle=ETABLISSEMENT07 TEST010
    ...  etablissement_etat=non suivi
    ...  etablissement_nature=ERP non référentiels
    ${etab_l_01.code} =  Ajouter l'établissement  ${etab_l_01}
    # Vérification du succès de la création avec des valeurs limitées
    Depuis le contexte de l'établissement  ${etab_l_01.code}  ${etab_l_01.libelle}
    Form Static Value Should Be  css=#exp_nom  Nam quis nulla. Integer malesuada. In in enim a arcu imperdiet malesuada. Sed vel lectus. Donec odio
    Form Static Value Should Be  css=#exp_prenom  Nam quis nulla. Integer malesuada. In in enim a arcu imperdiet malesuada. Sed vel lectus. Donec odio
    Form Static Value Should Be  css=#exp_adresse_voie  Nam quis nulla. Integer malesuada. In in enim a arcu imperdiet malesuada. Sed vel lectus. Donec odio urna, tempus molestie, porttitor ut, iaculis quis, sem. Phasellus rhoncus. Aenean id metus id velit ullamcorper pulvinar. Vestibulum fermentum tortor id m
    Form Static Value Should Be  css=#exp_adresse_complement  Nam quis nulla. Integer malesuada. In in

Modifier un établissement
    [Documentation]

    Depuis la page d'accueil    cadre-si    cadre-si

    #
    Depuis le contexte de l'établissement  ${etab01_code}  ${etab01.libelle}
    # Pourquoi vérifier ça ?
    Element Should Contain  css=#adresse_ville  MARSEILLE
    # Pourquoi vérifier ça ?
    Page Should Contain  ${DATE_FORMAT_DD/MM/YYYY}

    #
    Click On Form Portlet Action  etablissement_tous  modifier
    #
    Input Text  css=#libelle  CHEVEUX AUX VENTS
    # On s'assure que la nature de l'établissement est ERP Référentiel
    Select From List By Label  css=#etablissement_nature  ERP Référentiel
    # On déselectionne les valeurs obligatoires pour un établissement de nature ERP Référentiel
    Unselect From Chosen List  css=#etablissement_type
    Select From List By Value  css=#si_autorite_competente_visite  ${EMPTY}
    # On valide le formulaire
    Click On Submit Button
    # On vérifie le message affiché à l'utilisateur
    Error Message Should Contain  Le type et la catégorie de l'établissement doivent être renseignés si l'établissement est un ERP Référentiel
    Error Message Should Contain  La commission compétente visite SI de l'établissement doit être renseignée si l'établissement est un ERP Référentiel
    # On change le type de l'établissement
    Select From Chosen List  css=#etablissement_type  [PA]
    Select From List By Label  css=#si_autorite_competente_visite  Commission communale de sécurité
    # On valide le formulaire
    Click On Submit Button
    Element Should Contain    css=#libelle    CHEVEUX AUX VENTS
    #
    Click On Form Portlet Action    etablissement_tous    modifier
    #
    Input Text    css=#libelle    ${etab01.libelle}
    # On valide le formulaire
    Click On Submit Button

    # vérifie le bon fonctionnement de l'autocomplete
    Click On Form Portlet Action    etablissement_tous    modifier
    WUX  Page Should Contain Element  xpath=//*[@id='autocomplete-voie-search']
    Element Should Be Visible  css=#autocomplete-voie-check
    Element Should Be Visible  css=#autocomplete-voie-empty
    Page Should Contain Element  xpath=//*[@id='autocomplete-voie-search' and @readonly='']
    Click Element  css=#autocomplete-voie-empty
    WUX  Page Should Contain Element  xpath=//input[@id='autocomplete-voie-search' and not(@readonly)]
    Input Text  css=#autocomplete-voie-search  RIVOLI
    WUX  Page Should Contain Element  xpath=//ul[contains(@class, 'ui-autocomplete')]/li[contains(@class, 'ui-menu-item')]/a[text()="RUE DE RIVOLI"]
    Click Element  xpath=//ul[contains(@class, 'ui-autocomplete')]/li[contains(@class, 'ui-menu-item')]/a[text()="RUE DE RIVOLI"]
    Element Should Be Visible  css=#autocomplete-voie-check
    Element Should Be Visible  css=#autocomplete-voie-empty
    Page Should Contain Element  xpath=//*[@id='autocomplete-voie-search' and @readonly='']
    Click Element  css=#autocomplete-voie-empty
    WUX  Page Should Contain Element  xpath=//input[@id='autocomplete-voie-search' and not(@readonly)]
    Element Should Not Be Visible  css=#autocomplete-voie-check
    Element Should Not Be Visible  css=#autocomplete-voie-empty
    Click On Back Button


Supprimer un établissement
    [Documentation]

    Depuis la page d'accueil    cadre-si    cadre-si
    #
    Supprimer l'établissement    ${etab01.libelle}


Consulter la fiche de l'établissement
    [Documentation]  On vérifie que la fiche établissement contient les informations qu'elle doit contenir.

    Depuis la page d'accueil    cadre-si    cadre-si
    #
    Depuis le contexte de l'établissement    ${etab02_code}    ${etab02.libelle}
    # On vérifie que le libellé du champ dossier_coordination_periodique est correctement traduit
    Element Should Contain    css=#lib-dossier_coordination_periodique     dossier de coordination périodique


Fiche 'Établissement' - Saisie du code manuelle
    [Documentation]   Le code de l'établissement est composé par défaut de
    ...  manière automatique, en récupérant dans le paramétrage le préfixe du
    ...  code d'établissement et le dernier numéro +1 du code d'établissement
    ...  le plus élevé.
    ...
    ...  Une option permet de renseigner manuellement ce code à l'ajout de
    ...  l'établissement.
    ...
    ...  - par défaut le champ code est caché
    ...  - on active l'option, le champ code apparaît
    ...  - au chargement du formulaire d'ajout la valeur AUTO est positionnée
    ...  - si on valide avec AUTO le code de l'établissement est composé de manière automatique
    ...  - si on renseigne une valeur vide, un message d'erreur nous indique que le format n'est pas valide
    ...  - si on renseigne une chaine de texte, un message d'erreur nous indique que le format n'est pas valide
    ...  - si on renseigne un code qui existe déjà, un message d'erreur nous indique que le code existe déjà
    ...  - si on renseigne un code dont le numéro commence par des zéros ils sont supprimés de manière transparente
    ...  - si on renseigne une valeur correcte l'établissement est ajouté
    ...  - une action numéroter permet de modifer le code après création
    ...  - un message indique si des documents générés existent sur cet établissement

    Depuis la page d'accueil  admin  admin
    # Constitution du jeu de données
    ${etab_l_01} =  Create Dictionary
    ...  libelle=ETABCODEAUTO
    ...  etablissement_nature=ERP potentiel
    ${etab_l_02} =  Create Dictionary
    ...  libelle=ETABCODEMANUAL
    ...  etablissement_nature=ERP potentiel
    ...  exp_civilite=M.
    ...  exp_nom=DUPOPANT
    ...  exp_prenom=JEAN
    # Par défaut le champ code est caché dans le formulaire d'ajout
    # et l'action numeroter n'est pas disponible sur un établissement
    Depuis le formulaire d'ajout d'établissement
    Element Should Not Be Visible  css=#code
    Input Text  css=#libelle  ETABCODEAUTO OPTIONAUTO
    Select From List By Label  css=#etablissement_nature  ERP potentiel
    Click On Submit Button
    Valid Message Should Be  Vos modifications ont bien été enregistrées.
    Portlet Action Should Not Be In Form  etablissement_tous  numeroter
    # Activation de l'option et vérification de la présence du champ code dans le formulaire d'ajout
    &{values} =  Create Dictionary
    ...  libelle=option_etablissement_code
    ...  valeur=manual
    Ajouter ou modifier l'enregistrement de type 'paramètre' (om_parametre)  ${values}
    Depuis le formulaire d'ajout d'établissement
    Element Should Be Visible  css=#code
    #
    Form Value Should Be  css=#code  AUTO
    Page Should Contain Element  xpath=//*[@id='code' and @readonly='']
    Saisir les valeurs dans le formulaire de l'établissement  ${etab_l_01}
    Click On Submit Button
    Valid Message Should Be  Vos modifications ont bien été enregistrées.
    ${etab_l_01.id} =  Get Mandatory Value  css=#etablissement
    ${etab_l_01.code} =  Get Text  css=#code
    #
    Depuis le formulaire d'ajout d'établissement
    # si on renseigne une valeur vide, un message d'erreur nous indique que le format n'est pas valide
    Select Checkbox  css=#etablissement_code_toggl
    Element Should Be Focused  css=#code
    Page Should Not Contain Element  xpath=//*[@id='code' and @readonly='']
    #
    Unselect Checkbox  css=#etablissement_code_toggl
    Page Should Contain Element  xpath=//*[@id='code' and @readonly='']
    Form Value Should Be  css=#code  AUTO
    #
    Select Checkbox  css=#etablissement_code_toggl
    Input Text  css=#code  ${EMPTY}
    Click On Submit Button
    Error Message Should Contain  Le champ code est obligatoire
    Error Message Should Contain  SAISIE NON ENREGISTRÉE
    # si on renseigne un code qui existe déjà, un message d'erreur nous indique que le code existe déjà
    Input Text  css=#code  ${etab_l_01.code}
    Saisir les valeurs dans le formulaire de l'établissement  ${etab_l_02}
    Click On Submit Button
    Error Message Should Contain  La valeur saisie dans le champ code existe déjà, veuillez saisir une nouvelle valeur.
    Error Message Should Contain  SAISIE NON ENREGISTRÉE
    # si on renseigne une chaine de texte, un message d'erreur nous indique que le format n'est pas valide
    Input Text  css=#code  AZERTY
    Click On Submit Button
    Error Message Should Contain  Le champ code doit commencer par T suivi de chiffres
    Error Message Should Contain  SAISIE NON ENREGISTRÉE
    # si on renseigne une valeur correcte l'établissement est ajouté
    # si on renseigne un code dont le numéro commence par des zéros ils sont supprimés de manière transparente
    Input Text  css=#code  T000200
    Click On Submit Button
    Valid Message Should Be  Vos modifications ont bien été enregistrées.
    ${etab_l_02.id} =  Get Mandatory Value  css=#etablissement
    ${etab_l_02.code} =  Get Text  css=#code
    Should Be Equal  ${etab_l_02.code}  T200
    #
    Click on Form Portlet Action  etablissement_tous  numeroter
    #
    Page Should Not Contain   documents ont été générés pour cet établissement, contenant potentiellement le code actuel.
    # si on renseigne une valeur vide, un message d'erreur nous indique que le format n'est pas valide
    Input Text  css=#code  ${EMPTY}
    Click On Submit Button
    Error Message Should Contain  Le champ code est obligatoire
    Error Message Should Contain  SAISIE NON ENREGISTRÉE
    # si on renseigne un code qui existe déjà, un message d'erreur nous indique que le code existe déjà
    Input Text  css=#code  ${etab_l_01.code}
    Click On Submit Button
    Error Message Should Contain  La valeur saisie dans le champ code existe déjà, veuillez saisir une nouvelle valeur.
    Error Message Should Contain  SAISIE NON ENREGISTRÉE
    # si on renseigne une chaine de texte, un message d'erreur nous indique que le format n'est pas valide
    Input Text  css=#code  AZERTY
    Click On Submit Button
    Error Message Should Contain  Le champ code doit commencer par T suivi de chiffres
    Error Message Should Contain  SAISIE NON ENREGISTRÉE
    # si on renseigne une valeur correcte l'établissement est ajouté
    # si on renseigne un code dont le numéro commence par des zéros ils sont supprimés de manière transparente
    Input Text  css=#code  T000201
    Click On Submit Button
    Valid Message Should Contain  Vos modifications ont bien été enregistrées.
    Valid Message Should Contain  Etablissement ${etab_l_02.code} renuméroté T201
    ${etab_l_02.code} =  Get Text  css=#code
    Should Be Equal  ${etab_l_02.code}  T201
    # document généré du modèle d'édition sur l'établissement
    # un message indique si des documents générés existent sur cet établissement
    @{contacts_lies}  Create List  (Exploitant) ${etab_l_02.exp_civilite} ${etab_l_02.exp_nom} ${etab_l_02.exp_prenom}
    ${params}  Create Dictionary  contacts_lies=@{contacts_lies}
    &{dg_l_01} =  Create Dictionary
    ...  modele_edition=Courrier de courtoisie
    ...  courrier_type=Courrier simple
    Ajouter le document généré depuis le contexte de l'établissement
    ...  ${etab_l_02.code}
    ...  ${etab_l_02.libelle}
    ...  ${params}
    ...  ${dg_l_01.courrier_type}
    ...  ${dg_l_01.modele_edition}
    Depuis le contexte de l'établissement  ${etab_l_02.code}  ${etab_l_02.libelle}
    Click on Form Portlet Action  etablissement_tous  numeroter
    Error Message Should Be  Attention : 1 documents ont été générés pour cet établissement, contenant potentiellement le code actuel.
    Input Text  css=#code  T000202
    Click On Submit Button
    Valid Message Should Contain  Vos modifications ont bien été enregistrées.
    Valid Message Should Contain  Etablissement ${etab_l_02.code} renuméroté T202
    Valid Message Should Contain  Attention : 1 documents ont été générés pour cet établissement, contenant potentiellement le code actuel.
    ${etab_l_02.code} =  Get Text  css=#code


Fiche 'Établissement' - Numéro de SIRET
    #
    Depuis la page d'accueil    cadre-si    cadre-si
    #
    Go To Submenu In Menu    etablissements    etablissement_tous
    Click On Add Button

    Input Text    css=#libelle    FAKE
    Select From List By Label  css=#etablissement_nature    ERP Référentiel
    Select From Chosen List  css=#etablissement_type  [PA]
    Input Text    css=#siret    73482742011233

    Click On Submit Button
    Error Message Should Contain    Le numéro de SIRET est invalide.


Fiche 'Établissement' - Références cadastrales
    #
    Depuis la page d'accueil    cadre-si    cadre-si

    # Saisie
    Depuis le contexte de l'établissement    ${etab02_code}    ${etab02.libelle}
    Click On Form Portlet Action    etablissement_tous    modifier
    Input Text    css=input.refquart    77
    Input Text    css=input.refsect    G
    Input Text    css=input.refparc    77
    Click On Submit Button
    Valid Message Should Be    Vos modifications ont bien été enregistrées.

    #
    Depuis le contexte de l'établissement    ${etab02_code}    ${etab02.libelle}
    # Vérification de la présence des références cadastrales saisies en mode 'CONSULTER'
    Element Should Contain    css=div.field-type-referencescadastralesstatic    077G0077
    # Suppression des informations de références cadastrales
    Click On Form Portlet Action    etablissement_tous    modifier
    Input Text    css=input.refquart    ${empty}
    Input Text    css=input.refsect    ${empty}
    Input Text    css=input.refparc    ${empty}
    Click On Submit Button
    Valid Message Should Be    Vos modifications ont bien été enregistrées.

    #
    Depuis le contexte de l'établissement    ${etab02_code}    ${etab02.libelle}
    # Vérification de la non présence des références cadastrales saisies en mode 'CONSULTER'
    Page Should Not Contain    077G0077


Fiche 'Établissement' - Références patrimoine
    #
    Depuis la page d'accueil    admin    admin
    #Activation du paramètre option_referentiel_patrimoine
    Modifier le paramètre  option_referentiel_patrimoine  true

    #
    Depuis la page d'accueil    cadre-si    cadre-si
    #Ajout d'un nouvel établissement
    Go To Submenu In Menu    etablissements    etablissement_tous
    Click On Add Button

    #On vérifie que le champ "ref_patrimoine" n'est pas affiché
    Element Should Not Be Visible    css=#ref_patrimoine
    #On vérifie que le bouton "Récupérer le(s) référence(s) patrimoine" n'est pas affiché
    Element Should Not Be Visible    css=#lien_referentiel_patrimoine

    Input Text    css=#libelle    test_etablissement_patrimoine
    Select From List By Label    css=#etablissement_nature    ERP potentiel
    Select From List By Label    css=#etablissement_statut_juridique    Ville
    Sleep    1

    #On vérifie que le champ "ref_patrimoine" est affiché
    Element Should Be Visible    css=#ref_patrimoine
    #On vérifie que le bouton "Récupérer le(s) référence(s) patrimoine" est affiché
    Element Should Be Visible    css=#lien_referentiel_patrimoine

    Click Button    css=#lien_referentiel_patrimoine
    Sleep    1
    Confirm Action

    Input Text    css=input.refquart    801
    Input Text    css=input.refsect    G
    Input Text    css=input.refparc    0077
    Click Button    css=#lien_referentiel_patrimoine
    Page Should Contain    Aucune référence patrimoine liée.
    Click Element    css=.ui-dialog-titlebar-close

    Input Text    css=input.refquart    812
    Input Text    css=input.refsect    A
    Input Text    css=input.refparc    0077
    Click Button    css=#lien_referentiel_patrimoine
    WUX  Page Should Contain    Liste des références patrimoine trouvées :
    Click Button    css=input[value='Valider']
    Element Text Should Be    css=#ref_patrimoine    ${EMPTY}

    Input Text    css=input.refquart    812
    Input Text    css=input.refsect    A
    Input Text    css=input.refparc    0077
    Click Button    css=#lien_referentiel_patrimoine
    WUX  Page Should Contain    Liste des références patrimoine trouvées :
    Select Checkbox    css=input[value='91']
    Click Button    css=input[value='Valider']
    Sleep    2
    Element Text Should Be    css=#ref_patrimoine    91

    Click On Submit Button
    Valid Message Should Be    Vos modifications ont bien été enregistrées.
    La page ne doit pas contenir d'erreur

    #Test en modification
    Click On Back Button
    La page ne doit pas contenir d'erreur
    Input Text  css=div#adv-search-adv-fields input#libelle  test_etablissement_patrimoine
    Click On Search Button
    WUX  Element Should Contain  css=table.tab-tab  test_etablissement_patrimoine
    Click On Link    test_etablissement_patrimoine
    La page ne doit pas contenir d'erreur
    Element Text Should Be    css=#ref_patrimoine    91
    Click On Form Portlet Action    etablissement_tous    modifier
    La page ne doit pas contenir d'erreur

    Element Text Should Be    css=#ref_patrimoine    91

    #On vérifie que le champ "ref_patrimoine" est affiché
    Element Should Be Visible    css=#ref_patrimoine
    #On vérifie que le bouton "Récupérer le(s) référence(s) patrimoine" est affiché
    Element Should Be Visible    css=#lien_referentiel_patrimoine

    Select From List By Label    css=#etablissement_statut_juridique    Public
    Sleep    1
    #On vérifie que le champ "ref_patrimoine" n'est pas affiché
    Element Should Not Be Visible    css=#ref_patrimoine
    #On vérifie que le bouton "Récupérer le(s) référence(s) patrimoine" n'est pas affiché
    Element Should Not Be Visible    css=#lien_referentiel_patrimoine

    Select From List By Label    css=#etablissement_statut_juridique    Ville
    #On vérifie que le champ "ref_patrimoine" est affiché
    Element Should Be Visible    css=#ref_patrimoine
    #On vérifie que le bouton "Récupérer le(s) référence(s) patrimoine" est affiché
    Element Should Be Visible    css=#lien_referentiel_patrimoine

    Click Button    css=#lien_referentiel_patrimoine
    WUX  Page Should Contain    Liste des références patrimoine trouvées :
    Select Checkbox    css=input[value='91']
    Click Button    css=input[value='Valider']
    ${ref_result}=  catenate  SEPARATOR=\n
    ...  91
    ...  91
    Element Text Should Be  css=#ref_patrimoine  ${ref_result}
    Input Text    css=#ref_patrimoine    1284

    Click On Submit Button
    Valid Message Should Be    Vos modifications ont bien été enregistrées.
    La page ne doit pas contenir d'erreur

    Element Text Should Be    css=#ref_patrimoine    1284

    #Désactivation du paramètre option_referentiel_patrimoine
    Depuis la page d'accueil    admin    admin
    Modifier le paramètre  option_referentiel_patrimoine  false


Fiche 'Établissement' - Suivi des visites - mise à jour manuelle
    [Documentation]  Une permission spéciale permet de donner accès à la
    ...  modification des champs si_derniere_visite_* et acc_derniere_visite_*
    ...  en ajout et en modification d'établissement. La permission complète
    ...  sur l'objet établissement ne donne pas accès à cette modification.

    ${tcvid} =  Set Variable  T010 FESDVMAJM

    Depuis la page d'accueil  admin  admin

    #
    &{profil_l_01} =  Create Dictionary
    ...  libelle=PROFILL01 ${tcvid}
    ...  hierarchie=0
    Ajouter le profil depuis le menu  ${profil_l_01.libelle}  ${profil_l_01.hierarchie}
    &{util_l_01} =  Create Dictionary
    ...  nom=utill01t010fesdvmajm
    ...  login=utill01t010fesdvmajm
    ...  email=no-reply@openmairie.org
    ...  profil=${profil_l_01.libelle}
    ...  password=azerty
    Ajouter l'utilisateur
    ...  ${util_l_01.nom}
    ...  ${util_l_01.email}
    ...  ${util_l_01.login}
    ...  ${util_l_01.password}
    ...  ${util_l_01.profil}

    #
    Depuis la page d'accueil  admin  admin
    ${permissions} =  Create List
    ...  menu_etablissement
    ...  etablissement_tous
    ...  etablissement_referentiel_erp
    Ajouter les permissions au profil  ${profil_l_01.libelle}  ${permissions}
    Depuis la page d'accueil  ${util_l_01.login}  ${util_l_01.password}
    Depuis le formulaire d'ajout d'établissement
    Input Text  css=#libelle  ETABL01 ${tcvid}
    Select From List By Label  css=#etablissement_nature  ERP potentiel
    Element Should Not Be Visible  css=#si_derniere_visite_date
    Element Should Not Be Visible  css=#si_derniere_visite_avis
    Element Should Not Be Visible  css=#si_derniere_visite_technicien
    Element Should Not Be Visible  css=#acc_derniere_visite_date
    Element Should Not Be Visible  css=#acc_derniere_visite_avis
    Element Should Not Be Visible  css=#acc_derniere_visite_technicien
    Click On Submit Button
    Valid Message Should Be  Vos modifications ont bien été enregistrées.
    Click On Form Portlet Action  etablissement_tous  modifier
    Element Should Not Be Visible  css=#si_derniere_visite_date
    Element Should Not Be Visible  css=#si_derniere_visite_avis
    Element Should Not Be Visible  css=#si_derniere_visite_technicien
    Element Should Not Be Visible  css=#acc_derniere_visite_date
    Element Should Not Be Visible  css=#acc_derniere_visite_avis
    Element Should Not Be Visible  css=#acc_derniere_visite_technicien
    Click On Submit Button
    Valid Message Should Be  Vos modifications ont bien été enregistrées.

    #
    Depuis la page d'accueil  admin  admin
    ${permissions} =  Create List
    ...  etablissement_modifier_suivi_dernieres_visites
    Ajouter les permissions au profil  ${profil_l_01.libelle}  ${permissions}
    Depuis la page d'accueil  ${util_l_01.login}  ${util_l_01.password}
    Depuis le formulaire d'ajout d'établissement
    Input Text  css=#libelle  ETABL02 ${tcvid}
    Select From List By Label  css=#etablissement_nature  ERP potentiel
    Element Should Be Visible  css=#si_derniere_visite_date
    Input Text  css=#si_derniere_visite_date  01/01/2001
    Element Should Be Visible  css=#si_derniere_visite_avis
    Select From List By Label  css=#si_derniere_visite_avis  FAVORABLE
    Element Should Be Visible  css=#si_derniere_visite_technicien
    Select From List By Label  css=#si_derniere_visite_technicien  Jean DUPONT
    Element Should Be Visible  css=#acc_derniere_visite_date
    Input Text  css=#acc_derniere_visite_date  01/01/2001
    Element Should Be Visible  css=#acc_derniere_visite_avis
    Select From List By Label  css=#acc_derniere_visite_avis  FAVORABLE
    Element Should Be Visible  css=#acc_derniere_visite_technicien
    Select From List By Label  css=#acc_derniere_visite_technicien  Pierre BERNARD
    Click On Submit Button
    Valid Message Should Be  Vos modifications ont bien été enregistrées.
    Form Static Value Should Be  css=#si_derniere_visite_date  01/01/2001
    Form Static Value Should Be  css=#si_derniere_visite_avis  FAVORABLE
    Form Static Value Should Be  css=#si_derniere_visite_technicien  Jean DUPONT
    Form Static Value Should Be  css=#acc_derniere_visite_date  01/01/2001
    Form Static Value Should Be  css=#acc_derniere_visite_avis  FAVORABLE
    Form Static Value Should Be  css=#acc_derniere_visite_technicien  Pierre BERNARD
    Click On Form Portlet Action  etablissement_tous  modifier
    Element Should Be Visible  css=#si_derniere_visite_date
    Input Text  css=#si_derniere_visite_date  01/01/2002
    Element Should Be Visible  css=#si_derniere_visite_avis
    Select From List By Label  css=#si_derniere_visite_avis  DEFAVORABLE
    Element Should Be Visible  css=#si_derniere_visite_technicien
    Select From List By Label  css=#si_derniere_visite_technicien  Paul DURAND
    Element Should Be Visible  css=#acc_derniere_visite_date
    Input Text  css=#acc_derniere_visite_date  01/01/2002
    Element Should Be Visible  css=#acc_derniere_visite_avis
    Select From List By Label  css=#acc_derniere_visite_avis  DEFAVORABLE
    Element Should Be Visible  css=#acc_derniere_visite_technicien
    Select From List By Label  css=#acc_derniere_visite_technicien  Martine DUBOIS
    Click On Submit Button
    Valid Message Should Be  Vos modifications ont bien été enregistrées.
    Form Static Value Should Be  css=#si_derniere_visite_date  01/01/2002
    Form Static Value Should Be  css=#si_derniere_visite_avis  DEFAVORABLE
    Form Static Value Should Be  css=#si_derniere_visite_technicien  Paul DURAND
    Form Static Value Should Be  css=#acc_derniere_visite_date  01/01/2002
    Form Static Value Should Be  css=#acc_derniere_visite_avis  DEFAVORABLE
    Form Static Value Should Be  css=#acc_derniere_visite_technicien  Martine DUBOIS


Fiche 'Établissement' - Suivi des visites - mise à jour automatique
    [Documentation]  Mise à jour automatique des champs de l’établissement : la
    ...  dernière visite SI (si_derniere_visite_date, si_derniere_visite_avis,
    ...  si_derniere_visite_technicien), la prochaine visite SI
    ...  (si_prochaine_visite_date, si_prochaine_visite_type), et la dernière
    ...  visite ACC (acc_derniere_visite_date, acc_derniere_visite_avis,
    ...  acc_derniere_visite_technicien). Les déclencheurs sont l’ajout,
    ...  modification, annulation, suppression d’une visite ainsi que la
    ...  clôture d’une réunion.
    ...
    ...  Règles de mise à jour automatique :
    ...  - la dernière visite SI :
    ...    * Si on ne trouve aucune valeur pour la dernière visite SI on ne vide pas l’éventuelle précédente valeur existante pour permettre de faire une saisie manuelle ou de conserver la donnée présente lors de l’import de l’établissement.
    ...    * Les éléments de dernière visite sont ceux de la visite dans l’état planifié la plus récente lié à un DI lié par une demande de passage à une réunion clôturée.
    ...    * La date et le technicien sont ceux de la visite, l’avis est celui de la demande de passage en réunion.
    ...  - la dernière visite ACC
    ...    * Si on ne trouve aucune valeur pour la dernière visite ACC on ne vide pas l’éventuelle précédente valeur existante pour permettre de faire une saisie manuelle ou de conserver la donnée présente lors de l’import de l’établissement.
    ...    * Les éléments de dernière visite sont ceux de la visite dans l’état planifié la plus récente lié à un DI lié par une demande de passage à une réunion clôturée.
    ...    * La date et le technicien sont ceux de la visite, l’avis est celui de la demande de passage en réunion.
    ...  - la prochaine visite SI
    ...    * Si on ne trouve aucune valeur pour la prochaine visite SI on vide les valeurs précédentes.
    ...    * La prochaine visite est la visite la plus proche dans le futur, à défaut la visite la plus proche dans le passé si elle est strictement supérieure à la date de dernière visite, et à défaut vide.

    ${tcvid} =  Set Variable  T010 FESDVMAJA
    ${current_year} =  Get Time  year
    ${next_year} =  Evaluate  ${current_year} + 1

    Depuis la page d'accueil  admin  admin

    ## Constitution du jeu de données
    # Création d'un établissement et de deux DC de visites
    &{etab_l_01} =  Create Dictionary
    ...  libelle=ETABL01 ${tcvid}
    ...  etablissement_nature=ERP potentiel
    ...  etablissement_type=M
    ...  etablissement_categorie=1
    ...  adresse_numero=306
    ...  adresse_voie=RUE D ENDOUME
    ...  adresse_cp=13007
    ...  adresse_ville=MARSEILLE
    ...  adresse_arrondissement=7ème
    ...  exp_civilite=M.
    ...  exp_nom=Martin
    ...  exp_prenom=Pierre
    ...  etablissement_etat=non suivi
    ${etab_l_01.code} =  Ajouter l'établissement  ${etab_l_01}
    ${etab_l_01.titre} =  Set Variable  ${etab_l_01.code} - ${etab_l_01.libelle}
    &{dc_l_01} =  Create Dictionary
    ...  dossier_coordination_type=Visite de contrôle SI
    ...  description=DCL01 ${tcvid}
    ...  etablissement=${etab_l_01.titre}
    ...  a_qualifier=false
    ...  date_butoir=02/01/2018
    ${dc_l_01.libelle} =  Ajouter le dossier de coordination  ${dc_l_01}
    &{dc_l_02} =  Create Dictionary
    ...  dossier_coordination_type=Visite de réception
    ...  description=DCL02 ${tcvid}
    ...  etablissement=${etab_l_01.titre}
    ...  a_qualifier=false
    ...  date_butoir=${DATE_FORMAT_DD/MM/YYYY}
    ${dc_l_02.libelle} =  Ajouter le dossier de coordination  ${dc_l_02}
    &{etab_l_02} =  Create Dictionary
    ...  libelle=ETABL02 ${tcvid}
    ...  etablissement_nature=ERP potentiel
    ...  si_derniere_visite_date=01/01/2001
    ${etab_l_02.code} =  Ajouter l'établissement  ${etab_l_02}
    ${etab_l_02.titre} =  Set Variable  ${etab_l_02.code} - ${etab_l_02.libelle}
    &{dc_l_03} =  Create Dictionary
    ...  dossier_coordination_type=Visite de contrôle SI
    ...  description=DCL03 ${tcvid}
    ...  etablissement=${etab_l_02.titre}
    ...  a_qualifier=false
    ${dc_l_03.libelle} =  Ajouter le dossier de coordination  ${dc_l_03}

    # Par défaut à la création de l'établissement, il n'y a aucune information
    # dans le suivi des visites sauf celle renseignée manuellement
    #
    Depuis le contexte de l'établissement  ${etab_l_01.code}  ${etab_l_01.libelle}
    Form Static Value Should Be  css=#si_derniere_visite_date  ${EMPTY}
    Form Static Value Should Be  css=#si_derniere_visite_avis  ${EMPTY}
    Form Static Value Should Be  css=#si_derniere_visite_technicien  ${EMPTY}
    Form Static Value Should Be  css=#si_prochaine_visite_date  ${EMPTY}
    Form Static Value Should Be  css=#si_prochaine_visite_type  ${EMPTY}
    Form Static Value Should Be  css=#acc_derniere_visite_date  ${EMPTY}
    Form Static Value Should Be  css=#acc_derniere_visite_avis  ${EMPTY}
    Form Static Value Should Be  css=#acc_derniere_visite_technicien  ${EMPTY}
    #
    Depuis le contexte de l'établissement  ${etab_l_02.code}  ${etab_l_02.libelle}
    Form Static Value Should Be  css=#si_derniere_visite_date  ${etab_l_02.si_derniere_visite_date}
    Form Static Value Should Be  css=#si_derniere_visite_avis  ${EMPTY}
    Form Static Value Should Be  css=#si_derniere_visite_technicien  ${EMPTY}
    Form Static Value Should Be  css=#si_prochaine_visite_date  ${EMPTY}
    Form Static Value Should Be  css=#si_prochaine_visite_type  ${EMPTY}
    Form Static Value Should Be  css=#acc_derniere_visite_date  ${EMPTY}
    Form Static Value Should Be  css=#acc_derniere_visite_avis  ${EMPTY}
    Form Static Value Should Be  css=#acc_derniere_visite_technicien  ${EMPTY}

    # Création d'une visite dans le passé : aucune information ajoutée dans le
    # suivi des dernières visites et on écrase pas la valeur existante dans le champ
    # derniere_visite par contre on met à jour le champ prochaine_visite
    &{prog_l_01} =  Create Dictionary
    ...  service=Sécurité Incendie
    ...  annee=2018
    ...  semaine=01
    ${prog_l_01.code} =  Ajouter une semaine de programmation
    ...  service_libelle=${prog_l_01.service}
    ...  annee=${prog_l_01.annee}
    ...  numero_semaine=${prog_l_01.semaine}
    ${visite_l_01} =  Create Dictionary
    ...  programmation=${prog_l_01.code}
    ...  technicien=Paul DURAND
    ...  dossier_instruction_libelle=${dc_l_01.libelle}-SI
    ${visite_l_01.id} =  Ajouter une visite depuis la programmation
    ...  semaine_programmation=${visite_l_01.programmation}
    ...  technicien=${visite_l_01.technicien}
    ...  dossier_instruction_libelle=${visite_l_01.dossier_instruction_libelle}
    ...  jour_semaine=1
    ...  a_poursuivre=true
    Depuis la visite dans le contexte de la programmation
    ...  ${visite_l_01.programmation}
    ...  ${visite_l_01.id}
    ${visite_l_01_date_initiale} =  Get text  css=#date_visite
    ${visite_l_03} =  Create Dictionary
    ...  programmation=${prog_l_01.code}
    ...  technicien=Jean DUPONT
    ...  dossier_instruction_libelle=${dc_l_03.libelle}-SI
    ${visite_l_03.id} =  Ajouter une visite depuis la programmation
    ...  semaine_programmation=${visite_l_03.programmation}
    ...  technicien=${visite_l_03.technicien}
    ...  dossier_instruction_libelle=${visite_l_03.dossier_instruction_libelle}
    ...  jour_semaine=2
    ...  a_poursuivre=true
    Depuis la visite dans le contexte de la programmation
    ...  ${visite_l_03.programmation}
    ...  ${visite_l_03.id}
    ${visite_l_03_date_initiale} =  Get text  css=#date_visite
    #
    Depuis le contexte de l'établissement  ${etab_l_01.code}  ${etab_l_01.libelle}
    Form Static Value Should Be  css=#si_derniere_visite_date  ${EMPTY}
    Form Static Value Should Be  css=#si_derniere_visite_avis  ${EMPTY}
    Form Static Value Should Be  css=#si_derniere_visite_technicien  ${EMPTY}
    Form Static Value Should Be  css=#si_prochaine_visite_date  ${visite_l_01_date_initiale}
    Form Static Value Should Be  css=#si_prochaine_visite_type  Visite de contrôle sécurité
    Form Static Value Should Be  css=#acc_derniere_visite_date  ${EMPTY}
    Form Static Value Should Be  css=#acc_derniere_visite_avis  ${EMPTY}
    Form Static Value Should Be  css=#acc_derniere_visite_technicien  ${EMPTY}
    #
    Depuis le contexte de l'établissement  ${etab_l_02.code}  ${etab_l_02.libelle}
    Form Static Value Should Be  css=#si_derniere_visite_date  ${etab_l_02.si_derniere_visite_date}
    Form Static Value Should Be  css=#si_derniere_visite_avis  ${EMPTY}
    Form Static Value Should Be  css=#si_derniere_visite_technicien  ${EMPTY}
    Form Static Value Should Be  css=#si_prochaine_visite_date  ${visite_l_03_date_initiale}
    Form Static Value Should Be  css=#si_prochaine_visite_type  Visite de contrôle sécurité
    Form Static Value Should Be  css=#acc_derniere_visite_date  ${EMPTY}
    Form Static Value Should Be  css=#acc_derniere_visite_avis  ${EMPTY}
    Form Static Value Should Be  css=#acc_derniere_visite_technicien  ${EMPTY}

    # Création d'une visite dans le futur : information ajoutée dans le suivi
    # des visites
    &{prog_l_02} =  Create Dictionary
    ...  service=Sécurité Incendie
    ...  annee=${next_year}
    ...  semaine=52
    ${prog_l_02.code} =  Ajouter une semaine de programmation
    ...  service_libelle=${prog_l_02.service}
    ...  annee=${prog_l_02.annee}
    ...  numero_semaine=${prog_l_02.semaine}
    ${visite_l_02} =  Create Dictionary
    ...  programmation=${prog_l_02.code}
    ...  technicien=Jean DUPONT
    ...  dossier_instruction_libelle=${dc_l_02.libelle}-SI
    ${visite_l_02.id} =  Ajouter une visite depuis la programmation
    ...  semaine_programmation=${visite_l_02.programmation}
    ...  technicien=${visite_l_02.technicien}
    ...  dossier_instruction_libelle=${visite_l_02.dossier_instruction_libelle}
    ...  jour_semaine=1
    ...  a_poursuivre=true
    Depuis la visite dans le contexte de la programmation
    ...  ${visite_l_02.programmation}
    ...  ${visite_l_02.id}
    ${visite_l_02_date_initiale} =  Get text  css=#date_visite
    #
    Depuis le contexte de l'établissement  ${etab_l_01.code}  ${etab_l_01.libelle}
    Form Static Value Should Be  css=#si_derniere_visite_date  ${EMPTY}
    Form Static Value Should Be  css=#si_derniere_visite_avis  ${EMPTY}
    Form Static Value Should Be  css=#si_derniere_visite_technicien  ${EMPTY}
    Form Static Value Should Be  css=#si_prochaine_visite_date  ${visite_l_02_date_initiale}
    Form Static Value Should Be  css=#si_prochaine_visite_type  Visite de réception sécurité
    Form Static Value Should Be  css=#acc_derniere_visite_date  ${EMPTY}
    Form Static Value Should Be  css=#acc_derniere_visite_avis  ${EMPTY}
    Form Static Value Should Be  css=#acc_derniere_visite_technicien  ${EMPTY}

    # Modification d'une visite dans le futur, la date de prochaine visite sur l'établissement
    # est bien mise à jour
    ${visite_l_02_date_modifiee} =  Convert Date  ${visite_l_02_date_initiale}  exclude_millis=yes  date_format=%d/%m/%Y
    ${visite_l_02_date_modifiee} =  Add Time To Date  ${visite_l_02_date_modifiee}  1 day  result_format=%d/%m/%Y
    Depuis le formulaire de modification d'une visite dans le contexte de la programmation
    ...  ${visite_l_02.programmation}
    ...  ${visite_l_02.id}
    Input Datepicker  date_visite  ${visite_l_02_date_modifiee}
    Click On Submit Button In Subform
    #
    Depuis le contexte de l'établissement  ${etab_l_01.code}  ${etab_l_01.libelle}
    Form Static Value Should Be  css=#si_derniere_visite_date  ${EMPTY}
    Form Static Value Should Be  css=#si_derniere_visite_avis  ${EMPTY}
    Form Static Value Should Be  css=#si_derniere_visite_technicien  ${EMPTY}
    Form Static Value Should Be  css=#si_prochaine_visite_date  ${visite_l_02_date_modifiee}
    Form Static Value Should Be  css=#si_prochaine_visite_type  Visite de réception sécurité
    Form Static Value Should Be  css=#acc_derniere_visite_date  ${EMPTY}
    Form Static Value Should Be  css=#acc_derniere_visite_avis  ${EMPTY}
    Form Static Value Should Be  css=#acc_derniere_visite_technicien  ${EMPTY}

    # A la clôture de la réunion, on boucle sur tous les dossiers pour mettre à
    # jour le suivi des visites, on écrase une précédente valeur existante
    # si une visite sort pour la champ derniere_visite.
    ${reu_l_01} =  Create Dictionary
    ...  reunion_type=Commisson Communale de Sécurité
    ...  libelle=REUL01 ${tcvid}
    ...  date_reunion=15/01/2018
    ...  listes_de_diffusion=noreply@openmairie.org
    ${reu_l_01.code} =  Ajouter la réunion  ${reu_l_01}
    Planifier directement les dossiers de la programmation pour la réunion dans la catégorie
    ...  Programmation n°${prog_l_01.code}
    ...  ${reu_l_01.code}
    ...  Visites
    Numéroter l'ordre du jour de la réunion  ${reu_l_01.code}
    Rendre l'avis sur tous les dossiers de la réunion  ${reu_l_01.code}
    Clôturer la réunion  ${reu_l_01.code}
    #
    Depuis le contexte de l'établissement  ${etab_l_01.code}  ${etab_l_01.libelle}
    Form Static Value Should Be  css=#si_derniere_visite_date  ${visite_l_01_date_initiale}
    Form Static Value Should Be  css=#si_derniere_visite_avis  FAVORABLE
    Form Static Value Should Be  css=#si_derniere_visite_technicien  ${visite_l_01.technicien}
    Form Static Value Should Be  css=#si_prochaine_visite_date  ${visite_l_02_date_modifiee}
    Form Static Value Should Be  css=#si_prochaine_visite_type  Visite de réception sécurité
    Form Static Value Should Be  css=#acc_derniere_visite_date  ${EMPTY}
    Form Static Value Should Be  css=#acc_derniere_visite_avis  ${EMPTY}
    Form Static Value Should Be  css=#acc_derniere_visite_technicien  ${EMPTY}
    #
    Depuis le contexte de l'établissement  ${etab_l_02.code}  ${etab_l_02.libelle}
    Form Static Value Should Be  css=#si_derniere_visite_date  ${visite_l_03_date_initiale}
    Form Static Value Should Be  css=#si_derniere_visite_avis  FAVORABLE
    Form Static Value Should Be  css=#si_derniere_visite_technicien  ${visite_l_03.technicien}
    Form Static Value Should Be  css=#si_prochaine_visite_date  ${EMPTY}
    Form Static Value Should Be  css=#si_prochaine_visite_type  ${EMPTY}
    Form Static Value Should Be  css=#acc_derniere_visite_date  ${EMPTY}
    Form Static Value Should Be  css=#acc_derniere_visite_avis  ${EMPTY}
    Form Static Value Should Be  css=#acc_derniere_visite_technicien  ${EMPTY}

    # Suppression de la visite, le champs prochaine visite est bien
    # mis à jour sur l'établissement
    Supprimer la visite dans le contexte de la programmation
    ...  ${visite_l_02.programmation}
    ...  ${visite_l_02.id}
    #
    Depuis le contexte de l'établissement  ${etab_l_01.code}  ${etab_l_01.libelle}
    Form Static Value Should Be  css=#si_derniere_visite_date  ${visite_l_01_date_initiale}
    Form Static Value Should Be  css=#si_derniere_visite_avis  FAVORABLE
    Form Static Value Should Be  css=#si_derniere_visite_technicien  ${visite_l_01.technicien}
    Form Static Value Should Be  css=#si_prochaine_visite_date  ${EMPTY}
    Form Static Value Should Be  css=#si_prochaine_visite_type  ${EMPTY}
    Form Static Value Should Be  css=#acc_derniere_visite_date  ${EMPTY}
    Form Static Value Should Be  css=#acc_derniere_visite_avis  ${EMPTY}
    Form Static Value Should Be  css=#acc_derniere_visite_technicien  ${EMPTY}


Listing 'Référentiel ERP'
    [Documentation]  ...
    Depuis la page d'accueil  admin  admin

    # Cas n°1 : La nature n'est pas ERP Référentiel alors l'établissement n'est
    # pas présent dans le listing
    Depuis le listing des ERP référentiels
    Input Text  css=#libelle  ${etab06.libelle}
    Click On Search Button
    Element Should Not Contain  css=table.tab-tab  ${etab06.libelle}

    # Cas n°2 : La nature est ERP Référentiel alors l'établissement est
    # présent dans le listing
    Depuis le listing des ERP référentiels
    Input Text  css=#libelle  ${etab05.libelle}
    Click On Search Button
    Element Should Contain  css=table.tab-tab  ${etab05.libelle}

    # Cas n°3 : (TNR) La nature n'a pas le libellé exact "ERP Référentiel"
    # mais a bien le code ERPR alors l'établissement est présent dans le
    # listing
    &{etablissement_nature_mauvais_libelle} =  Create Dictionary
    ...  nature=ERP Raie fée rend ciel
    Modifier la nature d'établissement  ERPR  ${etablissement_nature_mauvais_libelle}
    Depuis le listing des ERP référentiels
    Input Text  css=#libelle  ${etab05.libelle}
    Click On Search Button
    Element Should Contain  css=table.tab-tab  ${etab05.libelle}
    &{etablissement_nature_bon_libelle} =  Create Dictionary
    ...  nature=ERP Référentiel
    Modifier la nature d'établissement  ERPR  ${etablissement_nature_bon_libelle}


Export CSV

    [Documentation]  Export CSV depuis les deux listings d'établissement

    Depuis la page d'accueil  admin  admin

    Go To Submenu In Menu    etablissements    etablissement_tous
    ${link} =  Get Element Attribute  css=div.tab-export a  href
    ${output_dir}  ${output_name} =  Télécharger un fichier  ${SESSION_COOKIE}  ${link}  ${EXECDIR}${/}results${/}
    ${full_path_to_file} =  Catenate  SEPARATOR=  ${output_dir}  ${output_name}
    # On vérifie dans le fichier téléchargé que l'entête correspond à ce qui est attendu
    ${content_file} =  Get File  ${full_path_to_file}
    ${header_csv_file} =  Set Variable  id;code;libellé;adresse;nature;type;catégorie;"statut juridique";état;"date d'arrêté d'ouverture";"date de prochaine visite";"date de prochaine visite périodique";"date de création de la fiche";archive
    Should Contain  ${content_file}  ${header_csv_file}

    Go To Submenu In Menu    etablissements    etablissement_referentiel_erp
    ${link} =  Get Element Attribute  css=div.tab-export a  href
    ${output_dir}  ${output_name} =  Télécharger un fichier  ${SESSION_COOKIE}  ${link}  ${EXECDIR}${/}results${/}
    ${full_path_to_file} =  Catenate  SEPARATOR=  ${output_dir}  ${output_name}
    # On vérifie dans le fichier téléchargé que l'entête correspond à ce qui est attendu
    ${content_file} =  Get File  ${full_path_to_file}
    ${header_csv_file} =  Set Variable  id;code;libellé;adresse;nature;type;catégorie;"statut juridique";état;"date d'arrêté d'ouverture";"date de prochaine visite";"date de prochaine visite périodique";"date de création de la fiche";archive
    Should Contain  ${content_file}  ${header_csv_file}


Recherche avancée d'un établissement
    [Documentation]  L'objet de ce TestCase est de vérifier le fonctionnement
    ...  du formulaire de recherche avancée d'un établissement.
    ...
    ...  - ...
    ...  - La recherche sur le critère geolocalise n'est pas disponible
    ...    si l'option sig externe n'est pas activé (c'est le cas dans ce
    ...    TestSuite)
    ...  - Vérification du filtre via le critère de recherche sur l'autorité compétente de visite SI.
    ...  - Vérification du critère "prochaine date de visite periodique"

    Depuis la page d'accueil    cadre-si    cadre-si

    #
    Go To Dashboard
    Go To Submenu In Menu    etablissements    etablissement_tous

    # Par ID
    Input Text    css=#etablissement    ${etab02_code}
    # Par libellé
    Input Text    css=#libelle    ${etab02.libelle}
    # Par numéro, voie et arrondissement
    Input Text    css=#adresse_numero    31
    Input Text    css=#adresse_voie    RUE DE ROME
    Select From List By Label    css=#adresse_arrondissement    6ème
    # Par type
    Select From List By Label    css=#etablissement_type    M
    # Par catégorie
    Select From List By Label    css=#etablissement_type    M
    # Par état
    Select From List By Label    css=#etablissement_type    M
    # Par nom de contact
    Input Text    css=#nom    Gaultier
    # Par numéro de SIRET
    Input Text    css=#siret    73282932000074

    #
    Click On Search Button
    Page Should Contain    ${etab02.libelle}

    # Nous sommes dans le contexte standard (aucune option_sig activée)
    # La recherche avancée sur le listing des établissements ne doit pas faire
    # apparaître le champ localise dédiée aux options SIG
    Depuis le listing de tous les établissements
    Element Should Not Be Visible  css=#adv-search-adv-fields #geolocalise
    Element Should Not Be Visible  css=#adv-search-adv-fields #lib-geolocalise

    # Vérification du critère si_autorite_competente_visite
    # Création d'un établissement CCS
    &{etab_l_02} =  Create Dictionary
    ...  libelle=KAER TEST010 MORHEN
    ...  etablissement_nature=ERP potentiel
    ...  si_autorite_competente_visite=Commission communale de sécurité
    ${etab_l_02.code} =  Ajouter l'établissement  ${etab_l_02}
    # Création d'un établissement SCDS
    &{etab_l_03} =  Create Dictionary
    ...  libelle=KAER TEST010 TROLDE
    ...  etablissement_nature=ERP potentiel
    ...  si_autorite_competente_visite=Sous-commission départementale de sécurité
    ${etab_l_03.code} =  Ajouter l'établissement  ${etab_l_03}
    # Recherche des deux établissements dans le listing
    Depuis le listing  etablissement_tous
    Input Text  css=#libelle  KAER TEST010
    Click On Search Button
    Element Should Contain  css=table.tab-tab  ${etab_l_02.libelle}
    Element Should Contain  css=table.tab-tab  ${etab_l_03.libelle}
    # Recherche de l'établissement CCS
    Depuis le listing  etablissement_tous
    Input Text  css=#libelle  KAER TEST010
    Select From List By Label  css=#si_autorite_competente_visite  Commission communale de sécurité
    Click On Search Button
    Element Should Contain  css=table.tab-tab  ${etab_l_02.libelle}
    Element Should Not Contain  css=table.tab-tab  ${etab_l_03.libelle}
    # Recherche de l'établissement SCDS
    Depuis le listing  etablissement_tous
    Input Text  css=#libelle  KAER TEST010
    Select From List By Label  css=#si_autorite_competente_visite  Sous-commission départementale de sécurité
    Click On Search Button
    Element Should Contain  css=table.tab-tab  ${etab_l_03.libelle}
    Element Should Not Contain  css=table.tab-tab  ${etab_l_02.libelle}

    # Vérification du critère "prochaine date de visite periodique"
    Depuis le listing  etablissement_tous
    Input Text    css=#si_prochaine_visite_periodique_date_previsionnelle_min  01/01/2018
    #
    Click On Search Button
    Page Should Not Contain    ${etab05.libelle}
    Page Should Contain    ${etab04.libelle}

    Click Element  class=raz_advs
    # On renseigne uniquement la date minimal avec la date du jour
    Input Text    css=#si_prochaine_visite_periodique_date_previsionnelle_max  15/01/2017
    Click On Search Button
    Page Should Contain    ${etab05.libelle}
    Page Should Not Contain    ${etab04.libelle}

    Click Element  class=raz_advs
    # On renseigne la date maximal avec la date du jour + 300 jours et la date minimal à la date du jour
    Input Text    css=#si_prochaine_visite_periodique_date_previsionnelle_min  01/01/2017
    Input Text    css=#si_prochaine_visite_periodique_date_previsionnelle_max  12/01/2018
    Click On Search Button
    Page Should Contain    ${etab05.libelle}
    Page Should Contain    ${etab04.libelle}


Ajout d'un exploitant
    #
    Depuis la page d'accueil    cadre-si    cadre-si

    # Vérification
    Depuis le contexte de l'établissement    ${etab03_code}    ${etab03.libelle}
    Element Should Contain    css=#exp_nom    Poutine
    On clique sur l'onglet    contact    Contacts
    Click On Link    M. Poutine Vladimir
    Sleep    1
    Page Should Contain    Russie


Modification d'un exploitant
    #
    Depuis la page d'accueil    cadre-si    cadre-si

    # Modification de l'exploitant d'un établissement
    Depuis le contexte de l'établissement    ${etab03_code}    ${etab03.libelle}
    Click On Form Portlet Action    etablissement_tous    modifier
    Select From List By Label    css=#exp_civilite    Mme
    Input Text    css=#exp_nom    Merkel
    Input Text    css=#exp_prenom    Angela
    Input Text    css=#exp_adresse_numero    1
    Input Text    css=#exp_adresse_cp    ${empty}
    Input Text    css=#exp_adresse_ville    Berlin
    Input Text    css=#exp_pays    Allemagne
    Click On Submit Button
    Valid Message Should Be    Vos modifications ont bien été enregistrées.

    # Vérification
    Depuis le contexte de l'établissement    ${etab03_code}    ${etab03.libelle}
    Element Should Contain    css=#exp_nom    Merkel
    On clique sur l'onglet    contact    Contacts
    Click On Link    Mme Merkel Angela
    Sleep    1
    Page Should Contain    Allemagne


Suppression d'un exploitant
    #
    Depuis la page d'accueil    cadre-si    cadre-si

    # Suppression de l'exploitant d'un établissement
    Depuis le contexte de l'établissement    ${etab03_code}    ${etab03.libelle}
    Click On Form Portlet Action    etablissement_tous    modifier
    Select From List By Index    css=#exp_civilite    0
    Input Text    css=#exp_nom    ${empty}
    Input Text    css=#exp_prenom    ${empty}
    Input Text    css=#exp_adresse_numero    ${empty}
    Input Text    css=#exp_adresse_numero2    ${empty}
    Input Text    css=#exp_adresse_cp    ${empty}
    Input Text    css=#exp_adresse_ville    ${empty}
    Input Text    css=#exp_pays    ${empty}
    Click On Submit Button
    Valid Message Should Be    Vos modifications ont bien été enregistrées.

    # Vérification
    Depuis le contexte de l'établissement    ${etab03_code}    ${etab03.libelle}
    Page Should Not Contain    Merkel
    Element Should Contain    css=#exp    Aucun
    On clique sur l'onglet    contact    Contacts
    Page Should Not Contain    Merkel


Copie de l'adresse
    #
    Depuis la page d'accueil    cadre-si    cadre-si
    #
    Go To Submenu In Menu    etablissements    etablissement_tous
    Click On Add Button

    # Saisie informations établissement
    Input Text    css=#libelle    Open Inc

    # Saisie adresse établissement
    Input Text    css=#adresse_numero    1
    Input Text    css=#adresse_numero2    bis
    Input Text    css=#autocomplete-voie-search    RUE PAPERE
    WUX  Click Link    RUE PAPERE
    Select From List By Label    css=#adresse_arrondissement    1er
    Select From List By Label    css=#etablissement_nature    ERP Référentiel
    Sleep    1

    # Copie l'adresse
    Select Checkbox    css=#meme_adresse

    # Vérification
    Textfield Value Should Be    css=#exp_adresse_ville   LIBREVILLE
    Textfield Value Should Be    css=#exp_adresse_numero   1
    Textfield Value Should Be    css=#exp_adresse_numero2   bis
    Textfield Value Should Be    css=#exp_adresse_voie   RUE PAPERE
    Element Should Be Disabled    css=#exp_adresse_numero
    Element Should Be Disabled    css=#exp_adresse_numero2
    Element Should Be Disabled    css=#exp_adresse_voie

    # Efface l'adresse
    Unselect Checkbox   css=#meme_adresse

    # Nouvelle vérification
    Textfield Value Should Be    css=#exp_adresse_ville   ${EMPTY}
    Textfield Value Should Be    css=#exp_adresse_numero   ${EMPTY}
    Textfield Value Should Be    css=#exp_adresse_numero2   ${EMPTY}
    Textfield Value Should Be    css=#exp_adresse_voie   ${EMPTY}
    Element Should Be Enabled    css=#exp_adresse_numero
    Element Should Be Enabled    css=#exp_adresse_numero2
    Element Should Be Enabled    css=#exp_adresse_voie

    # A FAIRE : tester le submit du form
    # Actuellement fail modal dialog present


Archiver un établissement
    [Documentation]  L'action 'Archiver' sur un établissement permet de
    ...  l'exclure du listing des établissements.

    Depuis la page d'accueil    cadre-si    cadre-si

    #
    Depuis le contexte de l'établissement    ${etab02_code}    ${etab02.libelle}

    # L'action 'DESARCHIVER' ne doit pas être disponible
    Portlet Action Should Not Be In Form    etablissement_tous    desarchiver
    # On clique sur l'action directe 'ARCHIVER'
    Click On Form Portlet Action    etablissement_tous    archiver
    #
    WUX  Valid Message Should Be    Archivage correctement effectué.
    # L'action 'ARCHIVER' ne doit pas être disponible
    Portlet Action Should Not Be In Form    etablissement_tous    archiver
    # L'action 'DESARCHIVER' doit être disponible
    Portlet Action Should Be In Form    etablissement_tous    desarchiver

    # Vérification
    Go To Dashboard
    Go To Submenu In Menu    etablissements    etablissement_tous
    Input Text  css=div#adv-search-adv-fields input#libelle  ${etab02.libelle}
    Click On Search Button
    WUX  Element Should Not Contain  css=table.tab-tab  ${etab02.libelle}
    Click On Link    Afficher les éléments archivés

    # Désarchivage
    WUX  Element Should Contain  css=table.tab-tab  ${etab02.libelle}
    Click On Link    ${etab02.libelle}
    # On clique sur l'action directe 'DESARCHIVER'
    Click On Form Portlet Action    etablissement_tous    desarchiver
    #
    WUX  Valid Message Should Be    Désarchivage correctement effectué.
    # L'action 'DESARCHIVER' ne doit pas être disponible
    Portlet Action Should Not Be In Form    etablissement_tous    desarchiver
    # L'action 'ARCHIVER' doit être disponible
    Portlet Action Should Be In Form    etablissement_tous    archiver

    # Nouvelle vérification
    Go To Dashboard
    Go To Submenu In Menu    etablissements    etablissement_tous
    Input Text  css=div#adv-search-adv-fields input#libelle  ${etab02.libelle}
    Click On Search Button
    WUX  Element Should Contain  css=table.tab-tab  ${etab02.libelle}


Importer un établissement depuis un fichier CSV
    #
    Depuis la page d'accueil    admin    admin
    # On importe l'établissement
    Depuis l'import    import_etablissement
    Add File    fic1    import_etablissement.csv
    Click On Submit Button In Import CSV
    WUX  Valid Message Should Contain    1 ligne(s) importée(s)
    # On vérifie les valeurs importées
    Depuis le contexte de l'établissement    T777
    Element Text Should Be    css=#libelle    Musée de l'Import-Export
    Element Text Should Be    css=#annee_de_construction    2015
    Element Text Should Be    css=#adresse_numero    91
    Element Text Should Be    css=#adresse_numero2    bis
    Element Text Should Be    css=#adresse_voie    BD DALLEST
    Element Text Should Be    css=#adresse_cp    13009
    Element Text Should Be    css=#adresse_ville    MARSEILLE
    Element Text Should Be    css=#adresse_arrondissement    9ème
    Element Text Should Be    css=#etablissement_nature    ERP potentiel
    Open Fieldset    etablissement_tous    details
    Element Text Should Be    css=#si_locaux_sommeil    Non


Gestion de la périodicité des visites
    [Documentation]  Les visites périodiques concernent les établissements
    ...  en fonction de trois critères primaires : nature, etat, autorité
    ...  compétente.
    ...
    ...  Le paramètre *etablissement_etat_periodique* ...
    ...
    ...  Le paramètre *etablissement_nature_periodique* ...
    ...
    ...  Le paramètre *etablissement_autorite_competente_periodique* permet
    ...  de définir la ou les autorités compétentes concernées. Si le
    ...  paramètre n'est pas renseigné ou vide alors on prend tous les
    ...  établissements sans vérification de leur autorité compétente.

    Depuis la page d'accueil  admin  admin
    # On crée 3 établissements correspondant aux critères etat et nature
    # avec chacun une autrorité compétente différente : 1 CCS, 1 SCDS et
    # 1 sans autrorité compétente.
    &{etab_l_01} =  Create Dictionary
    ...  libelle=ETAB01 TEST010 PERIODIQUE AUTORITE
    ...  etablissement_nature=ERP Référentiel
    ...  etablissement_type=PA
    ...  etablissement_categorie=1
    ...  si_autorite_competente_visite=Commission communale de sécurité
    ...  adresse_numero=12
    ...  adresse_numero2=bis
    ...  adresse_voie=RUE DE LA REPUBLIQUE
    ...  adresse_cp=13001
    ...  adresse_ville=MARSEILLE
    ...  adresse_arrondissement=1er
    ...  siret=73482742011234
    ...  etablissement_etat=Ouvert
    ${etab_l_01.code} =  Ajouter l'établissement  ${etab_l_01}
    ${etab_l_01.titre} =  Set Variable  ${etab_l_01.code} - ${etab_l_01.libelle}
    &{etab_l_02} =  Create Dictionary
    ...  libelle=ETAB02 TEST010 PERIODIQUE AUTORITE
    ...  etablissement_nature=ERP Référentiel
    ...  etablissement_type=PA
    ...  etablissement_categorie=1
    ...  si_autorite_competente_visite=Commission communale de sécurité
    ...  adresse_numero=12
    ...  adresse_numero2=bis
    ...  adresse_voie=RUE DE LA REPUBLIQUE
    ...  adresse_cp=13001
    ...  adresse_ville=MARSEILLE
    ...  adresse_arrondissement=1er
    ...  siret=73482742011234
    ...  etablissement_etat=Ouvert
    ${etab_l_02.code} =  Ajouter l'établissement  ${etab_l_02}
    ${etab_l_02.titre} =  Set Variable  ${etab_l_02.code} - ${etab_l_02.libelle}
    &{etab_l_03} =  Create Dictionary
    ...  libelle=ETAB03 TEST010 PERIODIQUE AUTORITE
    ...  etablissement_nature=ERP Référentiel
    ...  etablissement_type=PA
    ...  etablissement_categorie=1
    ...  si_autorite_competente_visite=Sous-commission départementale de sécurité
    ...  adresse_numero=12
    ...  adresse_numero2=bis
    ...  adresse_voie=RUE DE LA REPUBLIQUE
    ...  adresse_cp=13001
    ...  adresse_ville=MARSEILLE
    ...  adresse_arrondissement=1er
    ...  siret=73482742011234
    ...  etablissement_etat=Ouvert
    ${etab_l_03.code} =  Ajouter l'établissement  ${etab_l_03}
    ${etab_l_03.titre} =  Set Variable  ${etab_l_03.code} - ${etab_l_03.libelle}
    # Paramètre NULL
    # On vérifie l'affichage du paramètre
    # On vérifie que les marqueurs rouges sont appliqués sur les bonnes ligne
    Modifier le paramètre  etablissement_autorite_competente_periodique  null  null
    Depuis le controlpanel 'Gestion de la périodicité'
    Click Element  css=#onglet-etablissements-par-criteres-de-periodicite
    Sleep  1
    Page Should Contain  > Nombre d'établissements correspondant aux critères primaires "ERPR/OUVE/" :
    Page Should Contain  > Nombre d'établissements correspondant aux critères primaires "ERPR/OUVE/" et aux critères secondaires (type/catégorie/locaux à sommeil) du paramétrage de la périodicité des visites :
    Element Should Not Be Visible  css=.erpr-ouve-ccs.default
    Element Should Be Visible  css=.erpr-ouve-ccs.danger
    Element Should Not Be Visible  css=.erpr-ouve-scds.default
    Element Should Be Visible  css=.erpr-ouve-scds.danger
    Element Should Not Be Visible  css=.erpr-ouve-.default
    Element Should Be Visible  css=.erpr-ouve-.danger
    # Paramètre CCS
    # On vérifie l'affichage du paramètre
    # On vérifie que les marqueurs rouges sont appliqués sur les bonnes ligne
    # On vérifie que le calcul du compteur est correct, en récupérant la valeur
    # du compteur, en modifiant l'établissement CCS en SCDS, puis en vérifiant
    # que le compteur est correctement mis à jour
    Modifier le paramètre  etablissement_autorite_competente_periodique  CCS  null
    Depuis le controlpanel 'Gestion de la périodicité'
    Click Element  css=#onglet-etablissements-par-criteres-de-periodicite
    Sleep  1
    ${original_counter} =  Get Text  css=span.nb-etab-criteres-periodiques-primaires
    Page Should Contain  > Nombre d'établissements correspondant aux critères primaires "ERPR/OUVE/CCS" :
    Page Should Contain  > Nombre d'établissements correspondant aux critères primaires "ERPR/OUVE/CCS" et aux critères secondaires (type/catégorie/locaux à sommeil) du paramétrage de la périodicité des visites :
    Element Should Not Be Visible  css=.erpr-ouve-ccs.default
    Element Should Be Visible  css=.erpr-ouve-ccs.danger
    Element Should Not Be Visible  css=.erpr-ouve-scds.danger
    Element Should Be Visible  css=.erpr-ouve-scds.default
    Element Should Be Visible  css=.erpr-ouve-.default
    Element Should Not Be Visible  css=.erpr-ouve-.danger
    &{etab_l_01_modif} =  Create Dictionary
    ...  si_autorite_competente_visite=Sous-commission départementale de sécurité
    Modifier l'établissement  ${etab_l_01_modif}  ${etab_l_01.code}  ${etab_l_01.libelle}
    Depuis le controlpanel 'Gestion de la périodicité'
    Click Element  css=#onglet-etablissements-par-criteres-de-periodicite
    Sleep  1
    ${new_counter} =  Get Text  css=span.nb-etab-criteres-periodiques-primaires
    ${expected_counter} =  Evaluate  ${original_counter} - 1
    Should Be Equal As Integers  ${new_counter}  ${expected_counter}
    # Paramètre CCS/SCDS + un code inexistant
    # On vérifie l'affichage du paramètre (le code inexistant doit être exclus)
    # On vérifie que les marqueurs rouges sont appliqués sur les bonnes ligne
    Modifier le paramètre  etablissement_autorite_competente_periodique  CCS;SCDS;PLOP  null
    Depuis le controlpanel 'Gestion de la périodicité'
    Click Element  css=#onglet-etablissements-par-criteres-de-periodicite
    Sleep  1
    Page Should Contain  > Nombre d'établissements correspondant aux critères primaires "ERPR/OUVE/CCS;SCDS" :
    Page Should Contain  > Nombre d'établissements correspondant aux critères primaires "ERPR/OUVE/CCS;SCDS" et aux critères secondaires (type/catégorie/locaux à sommeil) du paramétrage de la périodicité des visites :
    Element Should Not Be Visible  css=.erpr-ouve-ccs.default
    Element Should Be Visible  css=.erpr-ouve-ccs.danger
    Element Should Not Be Visible  css=.erpr-ouve-scds.default
    Element Should Be Visible  css=.erpr-ouve-scds.danger
    Element Should Not Be Visible  css=.erpr-ouve-.danger
    Element Should Be Visible  css=.erpr-ouve-.default
    # On positionne le paramètre à null (peu import l'autorité compétente de l'établissement)
    Modifier le paramètre  etablissement_autorite_competente_periodique  null  null


Périodicité des visites - Lier / Délier le DC VPS
    [Documentation]  Un établissement qui possède les critères le caractérisant
    ...  comme périodique, doit se voir créer et lier un DC VPS lors du traitement
    ...  de périodicité. Un établissement qui perd les critères, doit se voir délier
    ...  le DC VPS lors de l'enregistrement du formulaire (ajout et modification).

    ${tcvid} =  Set Variable  T010 PDVLDLDV
    Depuis la page d'accueil  admin  admin
    &{etab_l_01} =  Create Dictionary
    ...  libelle=ETABL01 ${tcvid}
    ...  etablissement_nature=ERP Référentiel
    ...  etablissement_type=PA
    ...  etablissement_categorie=1
    ...  si_autorite_competente_visite=Commission communale de sécurité
    ...  etablissement_etat=Ouvert
    ${etab_l_01.code} =  Ajouter l'établissement  ${etab_l_01}
    Form Static Value Should Be  css=#si_periodicite_visites  NA
    Form Static Value Should Be  css=#si_prochaine_visite_periodique_date_previsionnelle  NA
    Form Static Value Should Be  css=#si_derniere_visite_periodique_date  ${EMPTY}
    Element Should Not Be Visible  css=#link_dossier_coordination_periodique
    #
    &{etab_l_01_modif01} =  Create Dictionary
    ...  etablissement_categorie=4
    Modifier l'établissement  ${etab_l_01_modif01}  ${etab_l_01.code}
    Form Static Value Should Be  css=#si_periodicite_visites  5 ans
    Form Static Value Should Be  css=#si_prochaine_visite_periodique_date_previsionnelle  ${EMPTY}
    Form Static Value Should Be  css=#si_derniere_visite_periodique_date  ${EMPTY}
    Element Should Not Be Visible  css=#link_dossier_coordination_periodique
    #
    &{etab_l_01_modif02} =  Create Dictionary
    ...  si_derniere_visite_periodique_date=01/03/2018
    Modifier l'établissement  ${etab_l_01_modif02}  ${etab_l_01.code}
    Form Static Value Should Be  css=#si_periodicite_visites  5 ans
    Form Static Value Should Be  css=#si_prochaine_visite_periodique_date_previsionnelle  01/03/2023
    Form Static Value Should Be  css=#si_derniere_visite_periodique_date  01/03/2018
    Element Should Not Be Visible  css=#link_dossier_coordination_periodique
    #
    Déclencher le traitement de périodicité
    #
    Depuis le contexte de l'établissement  ${etab_l_01.code}  ${etab_l_01.libelle}
    Form Static Value Should Be  css=#si_periodicite_visites  5 ans
    Form Static Value Should Be  css=#si_prochaine_visite_periodique_date_previsionnelle  01/03/2023
    Form Static Value Should Be  css=#si_derniere_visite_periodique_date  01/03/2018
    Element Should Contain  css=#link_dossier_coordination_periodique  VPS
    ${dc_l_01_libelle} =  Get Text  css=#link_dossier_coordination_periodique
    #
    &{etab_l_01_modif03} =  Create Dictionary
    ...  etablissement_etat=Fermé
    Modifier l'établissement  ${etab_l_01_modif03}  ${etab_l_01.code}
    Valid Message Should Contain  Cet établissement vient de perdre les critères le classant comme soumis à visite périodique. Le DC périodique attaché est désormais orphelin.
    Form Static Value Should Be  css=#si_periodicite_visites  NA
    Form Static Value Should Be  css=#si_prochaine_visite_periodique_date_previsionnelle  NA
    Form Static Value Should Be  css=#si_derniere_visite_periodique_date  01/03/2018
    Element Should Not Be Visible  css=#link_dossier_coordination_periodique
    #
    &{etab_l_01_modif04} =  Create Dictionary
    ...  etablissement_etat=Ouvert
    Modifier l'établissement  ${etab_l_01_modif04}  ${etab_l_01.code}
    Form Static Value Should Be  css=#si_periodicite_visites  5 ans
    Form Static Value Should Be  css=#si_prochaine_visite_periodique_date_previsionnelle  01/03/2023
    Form Static Value Should Be  css=#si_derniere_visite_periodique_date  01/03/2018
    Element Should Not Be Visible  css=#link_dossier_coordination_periodique
    #
    Déclencher le traitement de périodicité
    #
    Depuis le contexte de l'établissement  ${etab_l_01.code}  ${etab_l_01.libelle}
    Form Static Value Should Be  css=#si_periodicite_visites  5 ans
    Form Static Value Should Be  css=#si_prochaine_visite_periodique_date_previsionnelle  01/03/2023
    Form Static Value Should Be  css=#si_derniere_visite_periodique_date  01/03/2018
    Element Should Contain  css=#link_dossier_coordination_periodique  ${dc_l_01_libelle}
    #
    &{etab_l_01_modif03} =  Create Dictionary
    ...  etablissement_etat=Fermé
    Modifier l'établissement  ${etab_l_01_modif03}  ${etab_l_01.code}
    Qualifier le dossier d'instruction  ${dc_l_01_libelle}-SI
    Clôturer le dossier de coordination   ${dc_l_01_libelle}
    #
    &{etab_l_01_modif04} =  Create Dictionary
    ...  etablissement_etat=Ouvert
    Modifier l'établissement  ${etab_l_01_modif04}  ${etab_l_01.code}
    #
    Déclencher le traitement de périodicité
    #
    Depuis le contexte de l'établissement  ${etab_l_01.code}  ${etab_l_01.libelle}
    Element Should Contain  css=#link_dossier_coordination_periodique  VPS
    ${dc_l_02_libelle} =  Get Text  css=#link_dossier_coordination_periodique
    Should Not Be Equal  ${dc_l_01_libelle}  ${dc_l_02_libelle}


Gestion des contraintes appliquées aux établissements

    [Documentation]    Test des vues et actions contraintes appliquées :
    ...  - tableau
    ...  - ajouter
    ...  - modifier
    ...  - supprimer
    ...  - récupérer
    ...  - démarquer comme récupérée

    @{ref_cad} =  Create List  800  A  1
    &{etab} =  Create Dictionary
    ...  libelle=FIX N MIX
    ...  etablissement_nature=ERP Référentiel
    ...  etablissement_type=PA
    ...  etablissement_categorie=1
    ...  si_autorite_competente_visite=Commission communale de sécurité
    ...  etablissement_etat=Ouvert
    ...  adresse_numero=28
    ...  adresse_voie=BD DE LA BLANCARDE
    ...  adresse_cp=13004
    ...  adresse_ville=MARSEILLE
    ...  adresse_arrondissement=4ème
    ...  siret=73282932000074
    ...  exp_nom=Ribérou
    ...  exp_prenom=Laurent
    Depuis la page d'accueil  cadre-si  cadre-si
    ${etab_code} =  Ajouter l'établissement  ${etab}
    # Cas 1 : pas d'option SIG
    Depuis le contexte de l'établissement  ${etab_code}  FIX N MIX
    On clique sur l'onglet  lien_contrainte_etablissement  Contraintes
    Page Should Not Contain Link  recuperer_contraintes
    Page Should Contain  Aucune contrainte appliquée à l'établissement.
    # Cas 2 : pas de référence cadastrale
    Depuis la page d'accueil  admin  admin
    Activer le plugin geoaria_tests et l'option sig
    &{contrainte} =  Create Dictionary
    ...  groupe=ZONE DU POS
    ...  sousgroupe=M5
    ...  nature=POS
    ...  libelle=Contrainte 13055.M50
    ...  texte=Texte
    ...  texte_surcharge=Surcharge
    Ajouter la contrainte  ${contrainte}
    Depuis la page d'accueil  cadre-si  cadre-si
    Depuis le contexte de l'établissement  ${etab_code}
    On clique sur l'onglet  lien_contrainte_etablissement  Contraintes
    Click Element  recuperer_contraintes
    WUX  Click Element  css=input[name="btn_recuperer"]
    Error Message Should Be In Subform  L'établissement n'a aucune référence cadastrale renseignée.
    # Cas 3 : Besoin de synchronisation
    Depuis le formulaire de modification de l'établissement  ${etab_code}
    Saisir les références cadastrales  ${ref_cad}
    Click On Submit Button
    Valid Message Should Be  Vos modifications ont bien été enregistrées.
    On clique sur l'onglet  lien_contrainte_etablissement  Contraintes
    Page Should Contain  Aucune contrainte appliquée à l'établissement.
    Click Element  recuperer_contraintes
    WUX  Click Element  css=input[name="btn_recuperer"]
    Error Message Should Be In Subform  Les contraintes doivent être synchronisées.
    Click On Back Button In Subform
    # Cas 4 :Ajout
    Click Element  ajouter_contrainte
    WUX  Click Element  css=#fieldset-sousform-lien_contrainte_etablissement-zone-du-pos legend
    WUX  Click Element  css=#fieldset-sousform-lien_contrainte_etablissement-m5 legend
    Sleep  1
    WUX  Select Checkbox  css=input[id^="contrainte_"]
    WUX  Click Element  css=#sformulaire div.formControls input[type="submit"]
    WUX  Element Should Be Visible  css=#sousform-lien_contrainte_etablissement div.message
    La page ne doit pas contenir d'erreur
    WUX  Element Text Should Be  css=#sousform-lien_contrainte_etablissement div.message.ui-state-valid p span.text  La contrainte Contrainte 13055.M50 a été appliquée à l'établissement.
    Click On Back Button In Subform
    Page Should Contain  Surcharge
    # Cas 5 : Récupération
    Depuis la page d'accueil  admin  admin
    Synchroniser les contraintes
    Valid Message Should Contain  4 ajoutées
    Depuis la page d'accueil  cadre-si  cadre-si
    Depuis le contexte de l'établissement  ${etab_code}
    On clique sur l'onglet  lien_contrainte_etablissement  Contraintes
    Click Element  recuperer_contraintes
    WUX  Click Element  css=input[name="btn_recuperer"]
    Valid Message Should Contain In Subform  2 ajoutées
    Click Element  css=input[name="btn_recuperer"]
    Valid Message Should Contain In Subform  0 ajoutée
    Valid Message Should Contain In Subform  2 mises à jour
    Click On Back Button In Subform
    # Cas 6 : Démarquage
    Click On Link  Une première contrainte du PLU
    Click On SubForm Portlet Action  lien_contrainte_etablissement  demarquer_recuperee
    Valid Message Should Be In Subform  La contrainte n'est plus marquée comme récupérée.
    Click On Back Button In Subform
    Click On Link  Une première contrainte du PLU
    Click On SubForm Portlet Action  lien_contrainte_etablissement  modifier
    Input Text  texte_complete  Une première contrainte du PLU modifiée
    Click On Submit Button In Subform
    Valid Message Should Be In Subform  Vos modifications ont bien été enregistrées.
    Click On Back Button In Subform
    # Cas 7 : Modification
    Click On Link  Une seconde contrainte du PLU
    Click On SubForm Portlet Action  lien_contrainte_etablissement  modifier
    Input Text  texte_complete  Une seconde contrainte du PLU modifiée
    Click On Submit Button In Subform
    Valid Message Should Be In Subform  Vos modifications ont bien été enregistrées.
    Click On Back Button In Subform
    # Cas 8 : Nouvelle récupération
    # → la contrainte modifiée doit être écrasée
    # → une nouvelle contrainte doit apparaître, une ayant été démarquée
    Click Element  recuperer_contraintes
    WUX  Click Element  css=input[name="btn_recuperer"]
    Valid Message Should Contain In Subform  1 ajoutée
    Valid Message Should Contain In Subform  1 mise à jour
    Click On Back Button In Subform
    Page Should Contain  Une première contrainte du PLU modifiée
    Page Should Not Contain  Une seconde contrainte du PLU modifiée
    Page Should Contain  Une seconde contrainte du PLU
    # Désactivation du SIG et des contraintes afin de revenir à l'état initial
    Depuis la page d'accueil  admin  admin
    Désactiver le plugin geoaria_tests et l'option sig
    Depuis le contexte de l'établissement  ${etab_code}
    On clique sur l'onglet  lien_contrainte_etablissement  Contraintes
    Click On Link  Une première contrainte du PLU modifiée
    Click On SubForm Portlet Action  lien_contrainte_etablissement  supprimer
    Click On Submit Button In Subform
    Click On Link  Une première contrainte du PLU
    Click On SubForm Portlet Action  lien_contrainte_etablissement  supprimer
    Click On Submit Button In Subform
    Click On Link  Une seconde contrainte du PLU
    Click On SubForm Portlet Action  lien_contrainte_etablissement  supprimer
    Click On Submit Button In Subform
    Supprimer la contrainte  Contrainte 13055.M70
    Supprimer la contrainte  Contrainte 13055.M80
    Supprimer la contrainte  Contrainte 13055.M81
    Supprimer la contrainte  Contrainte 13055.M90


Gestion des types d'établissement - champs de fusion
    [Documentation]  Un établissement possède un type d'établissement
    ...  principal et un ou plusieurs types d'établissement secondaires.
    ...
    ...  Cinq champs de fusion concernant les types d'établissement sont
    ...  disponibles sur l'établissement (
    ...  [etablissement.etablissement_type],
    ...  [etablissement.etablissement_type_description],
    ...  [etablissement.etablissement_type_libelle],
    ...  [etablissement.etablissement_type_secondaire],
    ...  [etablissement.etablissement_type_secondaire_libelle]).

    Depuis la page d'accueil  admin  admin

    ## Constitution du jeu de données
    # lettre type avec les champs de fusion
    &{lt_l_01} =  Create Dictionary
    ...  id=t010etabtypelt1
    Ajouter la lettre-type depuis le menu
    ...  ${lt_l_01.id}
    ...  ${lt_l_01.id}
    ...  <p>lettre type ${lt_l_01.id}</p>
    ...  ETAB_ET_BEGIN_[etablissement.etablissement_type]_ETAB_ET_END ETAB_ET_DESCRIPTION_BEGIN_[etablissement.etablissement_type_description]_ETAB_ET_DESCRIPTION_END ETAB_ET_LIBELLE_BEGIN_[etablissement.etablissement_type_libelle]_ETAB_ET_LIBELLE_END ETAB_ET_SECONDAIRE_BEGIN_[etablissement.etablissement_type_secondaire]_ETAB_ET_SECONDAIRE_END ETAB_ET_SECONDAIRE_LIBELLE_BEGIN_[etablissement.etablissement_type_secondaire_libelle]_ETAB_ET_SECONDAIRE_LIBELLE_END
    ...  Contexte 'document genere'
    ...  true
    # modèle d'édition rattaché à la lettre type
    &{me_l_01} =  Create Dictionary
    ...  code=t010etabtypeme1
    ...  libelle=Modèle d'édition test010 etablissement_type ME2
    ...  courrier_type=Courrier simple
    ...  om_lettretype_id=${lt_l_01.id}
    Ajouter le modèle d'édition  ${me_l_01}
    # établissement sans valeurs dans les champs *type d'établissement*
    &{etab_l_01} =  Create Dictionary
    ...  libelle=ETAB_L_01 TEST010 etablissement_type
    ...  etablissement_nature=ERP potentiel
    ${etab_l_01.code} =  Ajouter l'établissement  ${etab_l_01}
    # contact sur l'établissement
    &{contact_l_01} =   Create Dictionary
    ...  contact_type=Mandataire
    ...  nom=AGSHSYDK08
    ...  prenom=Jacques
    ...  civilite=M.
    Ajouter un particulier depuis un établissement
    ...  ${etab_l_01.code}
    ...  ${contact_l_01.contact_type}
    ...  ${contact_l_01.civilite}
    ...  ${contact_l_01.nom}
    ...  ${contact_l_01.prenom}
    ...  true
    # document généré du modèle d'édition sur l'établissement
    @{contacts_lies}  Create List  (${contact_l_01.contact_type}) ${contact_l_01.civilite} ${contact_l_01.nom} ${contact_l_01.prenom}
    ${params}  Create Dictionary  contacts_lies=@{contacts_lies}
    &{dg_l_01} =  Create Dictionary
    ...  modele_edition=${me_l_01.libelle}
    ...  courrier_type=${me_l_01.courrier_type}
    Ajouter le document généré depuis le contexte de l'établissement
    ...  ${etab_l_01.code}
    ...  ${etab_l_01.libelle}
    ...  ${params}
    ...  ${dg_l_01.courrier_type}
    ...  ${dg_l_01.modele_edition}
    ${dg_l_01.code_barres} =  Get Text  css=#code_barres

    ## Cas n°1 : pas de valeur => champs de fusion vides
    # L'établissement créé n'a aucune valeur dans les champs *type d'établissement*.
    # Les champs de fusion dans l'édition PDF doivent donc retourner une chaîne vide.
    ${contenu_pdf} =  Create List
    ...  ETAB_ET_BEGIN__ETAB_ET_END
    ...  ETAB_ET_DESCRIPTION_BEGIN__ETAB_ET_DESCRIPTION_END
    ...  ETAB_ET_LIBELLE_BEGIN__ETAB_ET_LIBELLE_END
    ...  ETAB_ET_SECONDAIRE_BEGIN__ETAB_ET_SECONDAIRE_END
    ...  ETAB_ET_SECONDAIRE_LIBELLE_BEGIN__ETAB_ET_SECONDAIRE_LIBELLE_END
    Depuis le contexte du document généré  ${dg_l_01.code_barres}
    Click On Form Portlet Action  courrier  previsualiser  new_window
    La page du fichier PDF doit contenir les chaînes de caractères
    ...  ${OM_PDF_TITLE}
    ...  ${contenu_pdf}
    ...  page_number=1

    ## Cas n°2 : valeurs renseignées => champs de fusion correctement rendus
    # On renseigne des valeurs dans les champs *type d'établissement* de l'établissement.
    # Les champs de fusion dans l'édition PDF doivent retourner les bonnes valeurs.
    &{etab_l_01_modif01} =  Create Dictionary
    ...  etablissement_type=J
    Modifier l'établissement  ${etab_l_01_modif01}  ${etab_l_01.code}
    Depuis le formulaire de modification de l'établissement  ${etab_l_01.code}  ${etab_l_01.libelle}
    @{ITEMS} =  Create List  [CTS]  [EF]
    Select From Multiple Chosen List  css=#etablissement_type_secondaire  ${ITEMS}
    Click On Submit Button
    ${contenu_pdf} =  Create List
    ...  ETAB_ET_BEGIN_[J] Structures d'accueil pour personnes âgées et personnes handicapées_ETAB_ET_END
    ...  ETAB_ET_DESCRIPTION_BEGIN_Structures d'accueil pour personnes âgées et personnes handicapées_ETAB_ET_DESCRIPTION_END
    ...  ETAB_ET_LIBELLE_BEGIN_J_ETAB_ET_LIBELLE_END
    ...  ETAB_ET_SECONDAIRE_BEGIN_[CTS] Chapiteaux, Tentes et Structures toile, [EF] Établissements flottants (eaux intérieures)_ETAB_ET_SECONDAIRE_END
    ...  ETAB_ET_SECONDAIRE_LIBELLE_BEGIN_CTS, EF_ETAB_ET_SECONDAIRE_LIBELLE_END
    Depuis le contexte du document généré  ${dg_l_01.code_barres}
    Click On Form Portlet Action  courrier  previsualiser  new_window
    La page du fichier PDF doit contenir les chaînes de caractères
    ...  ${OM_PDF_TITLE}
    ...  ${contenu_pdf}
    ...  page_number=1


Gestion des types d'établissement - intégration dans les formulaires
    [Documentation]  Un établissement possède un type d'établissement
    ...  principal et un ou plusieurs types d'établissement secondaires.
    ...
    ...  Les sélecteurs des champs *type d'établissement* utilisent la
    ...  librairie JS chosen. On vérifie en ajout et modification la saisie
    ...  des valeurs et leur enregistrement.

    Depuis la page d'accueil  admin  admin

    #On vérifie lors d'une Création
    Depuis le formulaire d'ajout d'établissement
    Input Text  css=#libelle  MAGASIN TYPE SECONDAIRE
    Select From List By Label  css=#etablissement_nature  ERP Référentiel
    Select From Chosen List  css=#etablissement_type  [Y]
    Select From Chosen List  css=#etablissement_categorie  [1]
    Select From List By Label  css=#si_autorite_competente_visite  Commission communale de sécurité
    # On sélectionne des types secondaires
    @{ITEMS} =  Create List  [CTS]  [EF]
    Select From Multiple Chosen List  css=#etablissement_type_secondaire  ${ITEMS}
    # On valide le formulaire
    Click On Submit Button
    # On vérifie le message affiché à l'utilisateur
    Valid Message Should Be    Vos modifications ont bien été enregistrées.
    # On vérifie que les types secondaires sélectionnés sont bien ceux saisis
    Element Should Contain  css=#etablissement_type_secondaire  CTS
    Element Should Contain  css=#etablissement_type_secondaire  EF

    #On vérifie lors d'une modification
    Click link  css=#action-form-etablissement_tous-modifier
    # On déselectionne les anciens types secondaires
    WUX  Click Element  css=#etablissement_type_secondaire_chosen .search-choice-close
    WUX  Click Element  css=#etablissement_type_secondaire_chosen .search-choice-close
    # On sélectionne des types secondaires
    @{ITEMS} =  Create List  [X]  [W]
    Select From Multiple Chosen List  css=#etablissement_type_secondaire  ${ITEMS}
    # On valide le formulaire
    Click On Submit Button
    # On vérifie le message affiché à l'utilisateur
    Valid Message Should Be    Vos modifications ont bien été enregistrées.
    # On vérifie que les types secondaires sélectionnés sont bien ceux saisis
    Element Should Contain  css=#etablissement_type_secondaire  X
    Element Should Contain  css=#etablissement_type_secondaire  W
    Element Should Not Contain  css=#etablissement_type_secondaire  CTS
    Element Should Not Contain  css=#etablissement_type_secondaire  EF

