<?php
/**
 * Ce script contient la définition de la classe 'GeneralTest'.
 *
 * @package openaria
 * @version SVN : $Id$
 */

require_once "000_test_unitaire_common.php";
final class GeneralTest extends GeneralCommon {
    public function setUp(): void {
        $this->common_setUp();
    }
    public function tearDown(): void {
        $this->common_tearDown();
    }
    public function onNotSuccessfulTest(Throwable $e): void {
        $this->common_onNotSuccessfulTest($e);
    }
}
