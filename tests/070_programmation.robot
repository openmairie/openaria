*** Settings ***
Resource  resources/resources.robot
Suite Setup  For Suite Setup
Suite Teardown  For Suite Teardown
Documentation  La programmations des visites...


*** Test Cases ***
Constitution d'un jeu de données
    [Documentation]  L'objet de ce 'Test Case' est de constituer un jeu de
    ...  données cohérent pour les scénarios fonctionnels qui suivent.

    Depuis la page d'accueil  admin  admin

    #
    &{etab_g_01} =  Create Dictionary
    ...  libelle=MONOP
    ...  etablissement_nature=ERP Référentiel
    ...  etablissement_type=M
    ...  etablissement_categorie=1
    ...  si_autorite_competente_visite=Commission communale de sécurité
    ...  adresse_numero=39
    ...  adresse_voie=RUE DE ROME
    ...  adresse_cp=13006
    ...  adresse_ville=MARSEILLE
    ...  adresse_arrondissement=6ème
    ...  siret=73282932000074
    ...  exp_civilite=Mme
    ...  exp_nom=Martel
    ...  exp_prenom=Lyne
    ${etab_g_01.code} =  Ajouter l'établissement  ${etab_g_01}
    ${etab_g_01.titre} =  Set Variable  ${etab_g_01.code} - ${etab_g_01.libelle}
    Set Suite Variable  ${etab_g_01}

    #
    Ajouter le contact institutionnel depuis le menu    null    Sécurité Incendie    null    M.    Bouchard    Gerard    null    null    null    null    null    null    null    null    null    null    null    null    null    null    nospam@openmairie.org    true

    #
    &{prog_g_01} =  Create Dictionary
    ...  service=Sécurité Incendie
    ...  annee=2014
    ...  semaine=39
    ${prog_g_01.code} =  Ajouter une semaine de programmation
    ...  service_libelle=${prog_g_01.service}
    ...  annee=${prog_g_01.annee}
    ...  numero_semaine=${prog_g_01.semaine}
    Set Suite Variable  ${prog_g_01}


Ajouter une programmation
    [Documentation]  La création d'une programmation nécessite trois éléments :
    ...  une année, un numéro de semaine et un service. Ce 'Test Case' vérifie
    ...  l'intégration du formulaire d'ajout d'une programmation : entrée de
    ...  menu, titre de la page, contrainte unique, valeurs par défaut dans le
    ...  formulaire,

    Depuis la page d'accueil    admin    admin

    # 1ère semaine
    # On ouvre le menu
    Go To Submenu In Menu  suivi  programmation
    # On passe en mode ajout
    Click On Add Button
    # On saisit les valeurs
    Input Value With JS    annee    2026
    Input Value With JS    numero_semaine    53
    Select From List By Label    css=#service    Sécurité Incendie
    # On valide le formulaire
    Click On Submit Button
    # On vérifie le message de validation affiché à l'utilisateur
    Valid Message Should Be  Vos modifications ont bien été enregistrées.
    # On vérifie le fil d'ariane
    Breadcrumb Should Contain    Suivi > Programmations > Gestion > Semaine N°53 Du 28/12/2026 Au 03/01/2027 V1
    # On vérifie les valeurs par défaut positionnées à la création de la programmation
    Element Text Should Be  css=#version  1
    Element Text Should Be  css=#programmation_etat  En préparation
    Element Text Should Be  css=#date_modification  ${DATE_FORMAT_DD/MM/YYYY}
    Element Text Should Be  css=#service  Sécurité Incendie

    # 2ème semaine
    Click On Back Button
    Click On Add Button
    Select From List By Label    css=#service    Sécurité Incendie
    Click On Submit Button
    Valid Message Should Be    Vos modifications ont bien été enregistrées.
    Click On Back Button
    Click On Link    2027/01

    # Vérification de l'unicité
    Click On Back Button
    Click On Add Button
    Input Value With JS    numero_semaine    1
    Select From List By Label    css=#service    Sécurité Incendie
    Click On Submit Button
    Error Message Should Contain    Les valeurs saisies dans les champs année, numéro de la semaine, service existent déjà, veuillez saisir de nouvelles valeurs.

    # Contrôle de la saisie
    Click On Back Button
    Click On Add Button
    Input Value With JS Failed    annee   123    Vous n'avez pas saisi une année valide.
    Input Value With JS Failed    numero_semaine   123    Vous n'avez pas saisi un numéro de semaine valide.
    Input Value With JS    annee   2027
    Input Value With JS    numero_semaine   53
    Select From List By Label    css=#service    Sécurité Incendie
    Click On Submit Button
    Error Message Should Contain    L'année 2027 ne comporte pas de semaine 53.


Bloc de propositions - Listing des visites à programmer
    [Documentation]  L'interface de planification de la programmation est
    ...  composée du bloc de propositions et du bloc agenda. Le bloc de
    ...  propositions est un listing des visites à programmer.
    ...
    ...   - ...
    ...   - Le libellé du dernier avis est présent dans le listing des visites à programmer
    ...   - TNR Les DI clôturés ne sont plus proposés dans le listing des visites à programmer
    ...   - ...

    Depuis la page d'accueil    admin    admin

    # Le libellé du dernier avis est présent dans le listing des visites à programmer

    # On importe un établissement via un CSV avec un dernier avis de visite (ce champ
    # n'est pas modifiable via le formulaire de l'établissement).
    Depuis l'import  import_etablissement
    Add File  fic1  import_etablissement_070.csv
    Click On Submit Button In Import CSV
    WUX  Valid Message Should Contain  1 ligne(s) importée(s)
    &{etab_l_01} =  Create Dictionary
    ...  code=T776
    ...  libelle=MUSEE DE LA PROGRAMMATION DERNIER AVIS
    ${etab_l_01.titre} =  Set Variable  ${etab_l_01.code} - ${etab_l_01.libelle}
    # On crée un dossier de coordination de visite qui porte sur l'établissment importé
    &{dc_l_01} =  Create Dictionary
    ...  dossier_coordination_type=Visite périodique SI
    ...  description=Dossier de coordination concernant une visite périodique
    ...  etablissement=${etab_l_01.titre}
    ...  a_qualifier=false
    ...  dossier_instruction_secu=true
    ...  date_demande=${DATE_FORMAT_DD/MM/YYYY}
    ${dc_l_01.libelle} =  Ajouter le dossier de coordination  ${dc_l_01}
    # On vérifie que le libellé du dernier avis est correctement affiché dans le listing des visites à programmer
    Depuis l'interface de planification de la programmation  ${prog_g_01.code}
    Input Text  css=#tableau_propositions tr.search_tab_row .dossier_instruction_libelle input.search_tab_input  ${dc_l_01.libelle}
    Element Text Should Be  css=td.etablissement_libelle  ${etab_l_01.titre}
    Element Text Should Be  css=td.dernier_avis  FAVORABLE
    # On crée un dossier de coordination de visite qui porte sur l'établissment importé
    &{dc_l_02} =  Create Dictionary
    ...  dossier_coordination_type=Visite de réception
    ...  description=Dossier de coordination concernant une visite de réception
    ...  etablissement=${etab_l_01.titre}
    ...  a_qualifier=false
    ...  dossier_instruction_secu=true
    ...  date_demande=${DATE_FORMAT_DD/MM/YYYY}
    ${dc_l_02.libelle} =  Ajouter le dossier de coordination  ${dc_l_02}
    # On vérifie que le DI apparaît bien dans le listing des visites à programmer
    Depuis l'interface de planification de la programmation  ${prog_g_01.code}
    Input Text  css=#tableau_propositions tr.search_tab_row .dossier_instruction_libelle input.search_tab_input  ${dc_l_02.libelle}
    Element Text Should Be  css=td.etablissement_libelle  ${etab_l_01.titre}
    # On clôture le DI
    Qualifier le dossier d'instruction  ${dc_l_02.libelle}-SI
    Clôturer le dossier d'instruction  ${dc_l_02.libelle}-SI
    # On vérifie que le DI n'apparaît pas dans le listing des visites à programmer
    Depuis l'interface de planification de la programmation  ${prog_g_01.code}
    Input Text  css=#tableau_propositions tr.search_tab_row .dossier_instruction_libelle input.search_tab_input  ${dc_l_02.libelle}
    Element Text Should Be  css=td.dataTables_empty  Aucun enregistrement.


Ajouter une visite
    [Documentation]  L'ajout d'une visite à la programmation se fait par le
    ...  drag & drop d'une ligne du bloc de propositions vers une case du bloc
    ...  agenda.

    Depuis la page d'accueil    cadre-si    cadre-si

    #
    &{dc_l_01} =  Create Dictionary
    ...  dossier_coordination_type=Visite de contrôle SI
    ...  description=Visite de contrôle du service sécurité incendie
    ...  etablissement=${etab_g_01.titre}
    ${dc_l_01.libelle} =  Ajouter le dossier de coordination  ${dc_l_01}
    #
    Depuis l'interface de planification de la programmation     ${prog_g_01.code}

    # On vérifie que le drag and drop est désactivé
    Element Should Be Visible    css=#filtre_technicien_info

    # On sélectionne un technicien
    Select From List By Label    css=#acteur    Paul DURAND
    # On vérifie que le drag and drop est activé
    Element Should Not Be Visible    css=#filtre_technicien_info

    # On fait une recherche sur le libelle du DI
    Input Text    css=table#tableau_propositions tr.search_tab_row th.dossier_instruction_libelle input.search_tab_input   ${dc_l_01.libelle}-SI

    ## Création de la visite n°1 : à poursuivre
    # On crée une visite
    # XXX Le Drag And Drop ne fonctionne pas, on exécute donc du javascript pour le simuler
    #Drag And Drop    css=table#tableau_propositions tbody tr    css=tr.fc-minor:nth-child(4) td.fc-widget-content:nth-child(2)
    Execute JavaScript  ${EXECDIR}${/}binary_files${/}jquery.simulate.js
    Execute Javascript  var element = $("table#tableau_propositions tbody tr"); element.simulate( "drag", { dx: 0, dy: 450 } );
    # On vérifie que l'overlay est correctement ouvert
    WUX  Element Should Be Visible  css=#sousform-visite.overlay div.subtitle h3
    # On coche "a_poursuivre"
    Select Checkbox  css=#sousform-visite #a_poursuivre
    # On valide le formulaire
    Click On Submit Button In Subform
    # On vérifie le message de validation
    Valid Message Should Contain  Vos modifications ont bien été enregistrées.
    # On ferme l'overlay
    Click On Back Button In Subform
    # On vérifie que le di est toujours présent dans le tableau
    WUX  Page Should Contain  ${dc_l_01.libelle}-SI

    ## Création de la visite n°2 : à poursuivre + vérification de la validité de la date de la visite
    # On crée une autre visite
    # XXX Le Drag And Drop ne fonctionne pas, on exécute donc du javascript pour le simuler
    #Drag And Drop    css=table#tableau_propositions tbody tr    css=tr.fc-minor:nth-child(4) td.fc-widget-content:nth-child(2)
    Execute JavaScript  ${EXECDIR}${/}binary_files${/}jquery.simulate.js
    Execute Javascript  var element = $("table#tableau_propositions tbody tr"); element.simulate( "drag", { dx: 0, dy: 450 } );
    # On vérifie que l'overlay est correctement ouvert
    WUX  Element Should Be Visible  css=#sousform-visite.overlay div.subtitle h3
    # On coche "a_poursuivre"
    Select Checkbox    css=#sousform-visite #a_poursuivre
    #
    Input Text    css=#date_visite    21/09/2014
    # On valide le formulaire
    Click On Submit Button In Subform
    #
    Error Message Should Contain   La date doit être comprise dans la semaine de programmation.
    #
    Input Text    css=#date_visite    ${EMPTY}
    # On valide le formulaire
    Click On Submit Button In Subform
    #
    Error Message Should Contain   Le champ date de la visite est obligatoire
    #
    Input Text    css=#date_visite    29/09/2014
    # On valide le formulaire
    Click On Submit Button In Subform
    #
    Error Message Should Contain   La date doit être comprise dans la semaine de programmation.
    #
    Input Text    css=#date_visite    27/09/2014
    # On valide le formulaire
    Click On Submit Button In Subform
    #
    Valid Message Should Contain    Vos modifications ont bien été enregistrées.
    #
    # On ferme l'overlay
    Click On Back Button In Subform
    # On vérifie que le di est toujours présent dans le tableau
    WUX  Page Should Contain  ${dc_l_01.libelle}-SI

    ## Création de la visite n°3 : PAS à poursuivre + vérification de la validité des heures de début et de fin
    # On crée une autre visite le 25/09 de 12:30 à 15:30
    # XXX Le Drag And Drop ne fonctionne pas, on exécute donc du javascript pour le simuler
    #Drag And Drop    css=table#tableau_propositions tbody tr    css=tr.fc-minor:nth-child(4) td.fc-widget-content:nth-child(2)
    Execute JavaScript  ${EXECDIR}${/}binary_files${/}jquery.simulate.js
    Execute Javascript  var element = $("table#tableau_propositions tbody tr"); element.simulate( "drag", { dx: 0, dy: 450 } );
    # On vérifie que l'overlay est correctement ouvert
    WUX  Element Should Be Visible  css=#sousform-visite.overlay div.subtitle h3
    # On récupère l'heure de début positionnée
    Sleep  2
    ${heure_debut_heure} =  Get Value  css=#heure_debut_heure
    ${heure_debut_minute} =  Get Value  css=#heure_debut_minute
    ${heure_fin_heure} =  Evaluate  ${heure_debut_heure} + 3
    ${heure_fin_heure} =  Convert To String  ${heure_fin_heure}
    ${heure_fin_heure_ok} =  Set Variable If
    ...  '${heure_fin_heure}' == '0'  00
    ...  '${heure_fin_heure}' == '1'  01
    ...  '${heure_fin_heure}' == '2'  02
    ...  '${heure_fin_heure}' == '3'  03
    ...  '${heure_fin_heure}' == '4'  04
    ...  '${heure_fin_heure}' == '5'  05
    ...  '${heure_fin_heure}' == '6'  06
    ...  '${heure_fin_heure}' == '7'  07
    ...  '${heure_fin_heure}' == '8'  08
    ...  '${heure_fin_heure}' == '9'  09
    ${heure_fin_heure} =  Set Variable If  ${heure_fin_heure_ok} is None  ${heure_fin_heure}  ${heure_fin_heure_ok}
    ${heure_fin_minute} =  Evaluate  ${heure_debut_minute}
    ${heure_fin_minute} =  Convert To String  ${heure_fin_minute}
    ${heure_fin_minute_ok} =  Set Variable If
    ...  '${heure_fin_minute}' == '0'  00
    ${heure_fin_minute} =  Set Variable If  ${heure_fin_minute_ok} is None  ${heure_fin_minute}  ${heure_fin_minute_ok}
    # On vérifie que les heures de fin correspondent bien à 3 heures plus tard
    WUX  Selected List Label Should Be  heure_fin_heure  ${heure_fin_heure}
    WUX  Selected List Label Should Be  heure_fin_minute  ${heure_fin_minute}
    # On saisit une heure de début égale à l'heure de fin
    # Cela crée une alerte que l'on vérifie
    Select Value With JS Failed   heure_debut_heure  ${heure_fin_heure}  L'heure de fin doit être supérieure à l'heure de début.
    ${new_heure_fin_minute} =  Set Variable If
    ...  '${heure_debut_minute}' == '00'  15
    ...  '${heure_debut_minute}' == '15'  30
    ...  '${heure_debut_minute}' == '30'  45
    ...  '${heure_debut_minute}' == '45'  00
    ${heure_fin_heure_plus_un} =  Evaluate  ${heure_fin_heure} + 1
    ${new_heure_fin_heure} =  Set Variable If
    ...  '${new_heure_fin_minute}' == '00'  ${heure_fin_heure_plus_un}
    ...  '${new_heure_fin_minute}' == '15'  ${heure_fin_heure}
    ...  '${new_heure_fin_minute}' == '30'  ${heure_fin_heure}
    ...  '${new_heure_fin_minute}' == '45'  ${heure_fin_heure}
    ${new_heure_fin_minute} =  Convert To String  ${new_heure_fin_minute}
    ${new_heure_fin_heure} =  Convert To String  ${new_heure_fin_heure}
    # On vérifie que le JS a corrigé les heures
    WUX  Selected List Label Should Be  heure_debut_heure  ${heure_fin_heure}
    WUX  Selected List Label Should Be  heure_fin_heure  ${new_heure_fin_heure}
    WUX  Selected List Label Should Be  heure_fin_minute  ${new_heure_fin_minute}
    # On désactive le contrôle JavaScript pour vérifier le contrôle PHP
    Désactiver évènements heure minute
    # On saisit une heure de début égale à celle de fin
    Select From List By Label  heure_fin_heure  ${heure_fin_heure}
    Select From List By Label  heure_fin_minute  ${heure_debut_minute}
    Execute JavaScript  window.heure_minute('heure_fin', 'heure');
    Execute JavaScript  window.heure_minute('heure_fin', 'minute');
    #
    Click On Submit Button In Subform
    #
    Error Message Should Contain  L' heure de début est égale à l' heure de fin.
    # On redésactive le contrôle JavaScript (nouveau formulaire = nouveau DOM)
    Désactiver évènements heure minute
    # On saisit une heure de début supérieure à celle de fin
    Select From List By Label  heure_debut_heure  16
    Execute JavaScript  window.heure_minute('heure_debut', 'heure');
    Select From List By Label  heure_debut_minute  45
    Execute JavaScript  window.heure_minute('heure_debut', 'minute');
    Select From List By Label  heure_fin_heure  16
    Execute JavaScript  window.heure_minute('heure_fin', 'heure');
    Select From List By Label  heure_fin_minute  30
    Execute JavaScript  window.heure_minute('heure_fin', 'minute');
    #
    Click On Submit Button In Subform
    #
    Error Message Should Contain  L' heure de début est postérieure à l' heure de fin.
    # On désactive encore le contrôle JavaScript
    Désactiver évènements heure minute
    # On ne saisit ni l'heure de début ni celle de fin
    Select From List By Label  heure_debut_heure  Choisir
    Execute JavaScript  window.heure_minute('heure_debut', 'heure');
    Select From List By Label  heure_debut_minute  Choisir
    Execute JavaScript  window.heure_minute('heure_debut', 'minute');
    Select From List By Label  heure_fin_heure  Choisir
    Execute JavaScript  window.heure_minute('heure_fin', 'heure');
    Select From List By Label  heure_fin_minute  Choisir
    Execute JavaScript  window.heure_minute('heure_fin', 'minute');
    #
    Click On Submit Button In Subform
    #
    Error Message Should Contain  Le champ heure de début est obligatoire
    Error Message Should Contain  Le champ heure de fin est obligatoire
    # On désactive une dernière fois le contrôle JavaScript
    Désactiver évènements heure minute
    # On saisit des heures valides > 12:30 > 15:30
    Select From List By Label  heure_debut_heure  12
    Execute JavaScript  window.heure_minute('heure_debut', 'heure');
    Select From List By Label  heure_debut_minute  30
    Execute JavaScript  window.heure_minute('heure_debut', 'minute');
    Select From List By Label  heure_fin_heure  15
    Execute JavaScript  window.heure_minute('heure_fin', 'heure');
    Select From List By Label  heure_fin_minute  30
    Execute JavaScript  window.heure_minute('heure_fin', 'minute');
    #
    Click On Submit Button In Subform
    Valid Message Should Contain  Vos modifications ont bien été enregistrées.
    # On ferme l'overlay
    Click On Back Button In Subform
    # On vérifie que le di n'est plus présent dans le tableau
    WUX  Page Should Not Contain  ${dc_l_01.libelle}-SI

    #
    # Fiche de visualisation d'une visite
    #
    # On clique sur une visite que l'on vient d'ajouter
    Click Element  css=div.fc-event-container a
    # On vérifie que l'overlay est correctement ouvert
    WUX  Element Should Be Visible  css=#sousform-visite.overlay div.subtitle h3
    # Vérification du lien vers l'établissement, on clqiue sur le lien et on doit arriver sur la fiche de l'établissement
    Click Element  css=#link_etablissement
    WUX  Page Title Should Be  Établissements > Tous Les Établissements > ${etab_g_01.titre}


Générer les convocations exploitants
    [Documentation]  L'action 'Générer les convocations exploitants' génère une
    ...  édition PDF qui concatène la lettre type
    ...  programmation_convocation_exploitant pour chaque visite planifiée dans
    ...  la programmation. La finalité est d'imprimer une convocation à la visite
    ...  à destination de l'exploitant de l'établissement.

    # Modification de la lettre type pour tester les champs de fusion
    # supplémentaire de la date de visite (date_visite_texte, date_visite_jour)
    Depuis la page d'accueil    admin    admin
    Depuis le listing des lettres-types
    Depuis le contexte de la lettre-type  programmation_convocation_exploitant
    Click On Form Portlet Action  om_lettretype  modifier
    Open Fieldset  om_lettretype  pied-de-page
    ${pied_page_html}=  Set Variable  <p style="text-align: left; color: gray;">Visite le [visite.date_visite_texte] ([visite.date_visite]), qui est un [visite.date_visite_jour]</p><p style='text-align: center; font-size: 8pt;'><em>Page &numpage/&nbpages</em></p>
    Input HTML  footer_om_htmletat  ${pied_page_html}
    Click On Submit Button
    Valid Message Should Contain  Vos modifications ont bien été enregistrées.

    #
    Depuis la page d'accueil    cadre-si    cadre-si
    #
    Depuis le contexte de la programmation    ${prog_g_01.code}
    # On finalise la programmation
    Click On Form Portlet Action    programmation    finaliser
    # On vérifie le message de validation
    WUX  Valid Message Should Contain     Finalisation correctement effectuée.
    # On valide la programmation
    Click On Form Portlet Action    programmation    valider
    # On vérifie le message de validation
    WUX  Valid Message Should Contain     Validation correctement effectuée.
    #
    Depuis la page d'accueil    secretaire-si    secretaire-si
    #
    Depuis le contexte de la programmation    ${prog_g_01.code}
    # On génère toutes les convocations aux visites
    Click On Form Portlet Action    programmation    envoyer_convoc_exploit
    # Le message de validation
    WUX  Valid Message Should Contain    Toutes les convocations exploitant ont été générées.
    WUX  Valid Message Should Contain    Lettre de convocation des exploitants : Convocation

    # Ouverture du PDF généré pour vérifier que les champs de fusion
    # supplémentaire de la date de visite (date_visite_texte, date_visite_jour)
    # sont correctement remplacés
    ${pdf_link_selector}=  Set Variable  //div[contains(@class, 'message') and contains(@class, 'ui-state-valid')]/descendant::a[text()[contains(., 'Convocation')]]
    Click Element  xpath=${pdf_link_selector}
    Open PDF  ${OM_PDF_TITLE}
    WUX  Page Should Contain  Visite le
    PDF Page Number Should Contain  1  Visite le 27 septembre 2014 (27/09/2014), qui est un samedi
    PDF Page Number Should Contain  2  Visite le 25 septembre 2014 (25/09/2014), qui est un jeudi
    PDF Page Number Should Contain  3  Visite le 25 septembre 2014 (25/09/2014), qui est un jeudi
    Close PDF


Envoyer les convocations partenaires
    [Documentation]  L'action 'Envoyer les convocations partenaires envoi un
    ...  courriel aux partenaires avec le planning de la semaine de
    ...  programmation en pièce jointe.

    Depuis la page d'accueil    secretaire-si    secretaire-si
    #
    Depuis le contexte de la programmation    ${prog_g_01.code}
    # On génère toutes les convocations aux visites
    Click On Form Portlet Action    programmation    envoyer_part  modale
    #
    Wait Until Element Is Visible    css=.ui-dialog .ui-dialog-buttonset button
    Click Element    css=.ui-dialog .ui-dialog-buttonset button
    # Le message de validation
    WUX  Valid Message Should Contain    La convocations des partenaires a été générée et envoyée par mail.


Édition PDF du planning
    [Documentation]  Lorsque la programmation est validée, alors une action permet
    ...  de télécharger une édition pdf du planning de la semaine de programmation.
    ...  Attention ce TestCase dépend du paramétrage de la lettre type
    ...  'programmation_planning'.

    Depuis la page d'accueil  admin  admin
    Depuis le contexte de la programmation  ${prog_g_01.code}
    Click On Form Portlet Action  programmation  edition-programmation_planning  new_window
    # On vérifie le bon fonctionnement des champs de fusion [programmation.semaine_annee],
    # [programmation.premier_jour_lettre], [programmation.dernier_jour_lettre],
    # [programmation.nb_visites].
    ${contenu_pdf} =  Create List
    ...  SEMAINE 2014/39
    ...  du 22 septembre 2014 au 28 septembre 2014
    ...  / 3 visites)
    La page du fichier PDF doit contenir les chaînes de caractères
    ...  ${OM_PDF_TITLE}
    ...  ${contenu_pdf}
    ...  page_number=1


Widget 'Programmations urgentes'
    [Documentation]  Le widget de tableau de bord 'Programmations urgente'
    ...  rassemble les ...
    Comment    @todo Écrire le 'Test Case'


Widget 'Programmations à valider'
    [Documentation]  Le widget de tableau de bord 'Programmations à valider'
    ...  rassemble les ......
    Comment    @todo Écrire le 'Test Case'


Widget 'Convocations exploitants à envoyer'
    [Documentation]  Le widget de tableau de bord 'Convocations exploitants
    ...  à envoyer' rassemble les ...
    Comment    @todo Écrire le 'Test Case'


Widget 'Convocations membres à envoyer'
    [Documentation]  Le widget de tableau de bord 'Convocations membres à
    ...  envoyer' rassemble les ...
    Comment    @todo Écrire le 'Test Case'

