*** Settings ***
Resource  resources/resources.robot
Suite Setup  For Suite Setup
Suite Teardown  For Suite Teardown
Documentation  TestCase services REST


*** Variables ***
${json_data_voies_1}    {"module":"voies","data":{"file_name":"../tests/binary_files/ref_voies_test_1.csv"}}
${json_data_voies_2}    {"module":"voies","data":{"file_name":"../tests/binary_files/ref_voies_test_2.csv"}}
#Numérisation des documents ACC
${numerisation_acc}    {"module": "import","data": {"service": "ACC"}}
#Numérisation des documents SI
${numerisation_si}    {"module": "import","data": {"service": "SI"}}
#Purge des documents numérisés et importés ACC
${purge_acc}    {"module":"purge", "data":{"service":"ACC"}}
#Purge des documents numérisés et importés SI
${purge_si}    {"module":"purge", "data":{"service":"SI"}}
#Envoie des accusés de réception des consultations officielles
${consultations}    {"module":"ar_consultation"}


*** Test Cases ***
Constitution d'un jeu de données
    [Documentation]  L'objet de ce 'Test Case' est de constituer un jeu de
    ...  données cohérent pour les scénarios fonctionnels qui suivent.

    Depuis la page d'accueil  admin  admin

    Depuis l'import  import_etablissement
    Add File  fic1  import_etablissements_040.csv
    Click On Submit Button In Import CSV
    WUX  Valid Message Should Contain  1 ligne(s) importée(s)

    &{etab_g_01} =  Create Dictionary
    ...  code=T4001
    ...  libelle=ETAB T4501 TEST040
    ...  etablissement_nature=ERP Référentiel
    ...  etablissement_type=N
    ...  etablissement_categorie=1
    ...  si_autorite_competente_visite=Commission communale de sécurité
    ...  adresse_numero=91
    ...  adresse_numero2=bis
    ...  adresse_voie=BD DALLEST
    ...  adresse_cp=13009
    ...  adresse_ville=MARSEILLE
    ...  adresse_arrondissement=9ème
    # Formatage sur 5 chiffres du code établissement
    # Ex. : T1 devient T00001
    ${etab_g_01.code_sur_6_caracteres} =  STR_REPLACE  T  ${EMPTY}  ${etab_g_01.code}
    ${etab_g_01.code_sur_6_caracteres} =  STR_PAD_LEFT  ${etab_g_01.code_sur_6_caracteres}  5  0
    ${etab_g_01.code_sur_6_caracteres} =  Catenate  SEPARATOR=  T  ${etab_g_01.code_sur_6_caracteres}
    Set Suite Variable  ${etab_g_01}


Referentiel voies
    [Documentation]  Synchronisation des voies
    [Tags]  post  voie voie_arrondissement

    Depuis la page d'accueil  admin  admin
    ${tcvid} =  Set Variable  T040 RV

    # On ajoute une voie référentielle déjà archivée
    &{voie_l_01} =  Create Dictionary
    ...  libelle=VOIL01 ${tcvid}
    ...  id_voie_ref=ZYX123
    ...  om_validite_debut=01/01/2016
    ...  om_validite_fin=31/12/2016
    Ajouter voie  ${voie_l_01}

    #
    Vérifier le code retour du web service et vérifier que son message est
    ...  Post
    ...  maintenance
    ...  ${json_data_voies_1}
    ...  200
    ...  Synchronisation terminée : 3 ajout(s) - 0 mise(s) à jour - 0 fusionnée(s) - 0 archivée(s)

    # On vérifie que la voie déjà archivée avant traitement n'a pas changée de date de fin de validité
    Depuis le listing  voie
    Click Element  css=#action-tab-voie-global-om_validite-false
    La page ne doit pas contenir d'erreur
    Input Text  name=recherche  ${voie_l_01.libelle}
    Select From List By Label  name=selectioncol  libellé
    Click Element  css=button[name="s1"]
    La page ne doit pas contenir d'erreur
    Click On Link  ${voie_l_01.libelle}
    La page ne doit pas contenir d'erreur
    Form Static Value Should Be  css=#om_validite_fin  ${voie_l_01.om_validite_fin}

    #Vérification de l'ajout des voies
    Depuis le listing  voie
    Input Text    name=recherche    XXX
    Select From List By Label    name=selectioncol    RIVOLI
    Click Element  css=button[name="s1"]
    La page ne doit pas contenir d'erreur
    Page Should Contain    Voie1
    Page Should Contain    Voie2
    Page Should Contain    Voie3
    Click On Link    Voie3
    La page ne doit pas contenir d'erreur
    Element Should Contain    css=#libelle    Voie3
    Element Should Contain    css=#rivoli    XXX3
    Click Link    css=#voie_arrondissement
    Sleep    1
    Page Should Contain    6ème
    Page Should Contain    7ème

    Vérifier le code retour du web service et vérifier que son message est
    ...  Post
    ...  maintenance
    ...  ${json_data_voies_2}
    ...  200
    ...  Synchronisation terminée : 0 ajout(s) - 2 mise(s) à jour - 0 fusionnée(s) - 1 archivée(s)

    #Vérification de la mise à jour des voies
    Depuis la page d'accueil  admin  admin
    Depuis le listing  voie
    Input Text    name=recherche    XXX
    Select From List By Label    name=selectioncol    RIVOLI
    Click Element  css=button[name="s1"]
    Sleep  1
    La page ne doit pas contenir d'erreur
    Page Should Contain    Voie2ModifArr
    Page Should Contain    Voie3
    Click On Link    Voie3
    La page ne doit pas contenir d'erreur
    Element Should Contain    css=#libelle    Voie3
    Element Should Contain    css=#rivoli    XXX3
    Click Link    css=#voie_arrondissement
    Sleep    1
    Page Should Contain    6ème
    Page Should Contain    8ème


Numérisation des documents
    [Tags]    post
    [Documentation]    Import des documents numérisés

    Copy File  ${EXECDIR}${/}binary_files${/}numerisation1.pdf  ${EXECDIR}${/}..${/}var${/}digitalization${/}ACC${/}Todo${/}${etab_g_01.code_sur_6_caracteres}DVISITE23-10-2016.pdf
    Copy File  ${EXECDIR}${/}binary_files${/}numerisation1.pdf  ${EXECDIR}${/}..${/}var${/}digitalization${/}ACC${/}Todo${/}numerisation1.pdf
    Copy File  ${EXECDIR}${/}binary_files${/}test.jpg  ${EXECDIR}${/}..${/}var${/}digitalization${/}ACC${/}Todo${/}test.jpg
    Copy File  ${EXECDIR}${/}binary_files${/}numerisation1.pdf  ${EXECDIR}${/}..${/}var${/}digitalization${/}SI${/}Todo${/}${etab_g_01.code_sur_6_caracteres}DVISITE23-10-2016.pdf
    Copy File  ${EXECDIR}${/}binary_files${/}numerisation1.pdf  ${EXECDIR}${/}..${/}var${/}digitalization${/}ACC${/}Todo${/}T99999DVISITE23-10-2016.pdf
    Copy File  ${EXECDIR}${/}binary_files${/}numerisation1.pdf  ${EXECDIR}${/}..${/}var${/}digitalization${/}ACC${/}Todo${/}${etab_g_01.code_sur_6_caracteres}PLOP23-10-2016.pdf
    Copy File  ${EXECDIR}${/}binary_files${/}numerisation1.pdf  ${EXECDIR}${/}..${/}var${/}digitalization${/}ACC${/}Todo${/}${etab_g_01.code_sur_6_caracteres}DVISITE23-13-2016.pdf
    Copy File  ${EXECDIR}${/}binary_files${/}numerisation1.pdf  ${EXECDIR}${/}..${/}var${/}digitalization${/}ACC${/}Todo${/}${etab_g_01.code_sur_6_caracteres}ARRETE23-10-2016.pdf
    Copy File  ${EXECDIR}${/}binary_files${/}numerisation1.pdf  ${EXECDIR}${/}..${/}var${/}digitalization${/}SI${/}Todo${/}${etab_g_01.code_sur_6_caracteres}ARRETE23-10-2016.pdf

    # Accessibilité
    # Documents dans le dossier
    Create Session    httpopenaria    http://localhost/openaria/services/rest_entry.php
    ${headers}=    Create Dictionary    'Content-Type=application/json'
    ${resp}=    Post Request    httpopenaria    /maintenance    data=${numerisation_acc}    headers=${headers}
    ${jsondata}=    To Json    ${resp.content}
    Should Be Equal As Strings    ${resp.status_code}    200
    Should Contain    ${jsondata['message']}    Tous les documents ont été traités
    Should Contain    ${jsondata['message']}    Liste des fichiers en erreur : ${etab_g_01.code_sur_6_caracteres}DVISITE23-13-2016.pdf,${etab_g_01.code_sur_6_caracteres}PLOP23-10-2016.pdf,T99999DVISITE23-10-2016.pdf,test.jpg

    # Sécurité
    # Documents dans le dossier
    Create Session    httpopenaria    http://localhost/openaria/services/rest_entry.php
    ${headers}=    Create Dictionary    'Content-Type=application/json'
    ${resp}=    Post Request    httpopenaria    /maintenance    data=${numerisation_si}    headers=${headers}
    ${jsondata}=    To Json    ${resp.content}
    Should Be Equal As Strings    ${resp.status_code}    200
    Should be Equal    ${jsondata['message']}    Tous les documents ont été traités

    # Aucun document dans le dossier
    Create Session    httpopenaria    http://localhost/openaria/services/rest_entry.php
    ${headers}=    Create Dictionary    'Content-Type=application/json'
    ${resp}=    Post Request    httpopenaria    /maintenance    data=${numerisation_si}    headers=${headers}
    ${jsondata}=    To Json    ${resp.content}
    Should Be Equal As Strings    ${resp.status_code}    200
    Should be Equal    ${jsondata['message']}    Aucun document à traiter

    Remove File    ${EXECDIR}${/}..${/}var${/}digitalization${/}ACC${/}Todo${/}T99999DVISITE23-10-2016.pdf
    Remove File    ${EXECDIR}${/}..${/}var${/}digitalization${/}ACC${/}Todo${/}${etab_g_01.code_sur_6_caracteres}PLOP23-10-2016.pdf
    Remove File    ${EXECDIR}${/}..${/}var${/}digitalization${/}ACC${/}Todo${/}${etab_g_01.code_sur_6_caracteres}DVISITE23-13-2016.pdf
    Remove File    ${EXECDIR}${/}..${/}var${/}digitalization${/}ACC${/}Todo${/}test.jpg

Purge des documents numérisés et importés
    [Tags]    post
    [Documentation]    Purge des documents numérisés et importés

    # Accessibilité
    # Documents dans le dossier
    Create Session    httpopenaria    http://localhost/openaria/services/rest_entry.php
    ${headers}=    Create Dictionary    'Content-Type=application/json'
    ${resp}=    Post Request    httpopenaria    /maintenance    data=${purge_acc}    headers=${headers}
    ${jsondata}=    To Json    ${resp.content}
    Should Be Equal As Strings    ${resp.status_code}    200
    Should be Equal    ${jsondata['message']}    Tous les documents ont été traités

    # Aucun document dans le dossier
    Create Session    httpopenaria    http://localhost/openaria/services/rest_entry.php
    ${headers}=    Create Dictionary    'Content-Type=application/json'
    ${resp}=    Post Request    httpopenaria    /maintenance    data=${purge_acc}    headers=${headers}
    ${jsondata}=    To Json    ${resp.content}
    Should Be Equal As Strings    ${resp.status_code}    200
    Should be Equal    ${jsondata['message']}    Aucun document à traiter

    # Sécurité
    # Documents dans le dossier
    Create Session    httpopenaria    http://localhost/openaria/services/rest_entry.php
    ${headers}=    Create Dictionary    'Content-Type=application/json'
    ${resp}=    Post Request    httpopenaria    /maintenance    data=${purge_si}    headers=${headers}
    ${jsondata}=    To Json    ${resp.content}
    Should Be Equal As Strings    ${resp.status_code}    200
    Should be Equal    ${jsondata['message']}    Tous les documents ont été traités

    # Aucun document dans le dossier
    Create Session    httpopenaria    http://localhost/openaria/services/rest_entry.php
    ${headers}=    Create Dictionary    'Content-Type=application/json'
    ${resp}=    Post Request    httpopenaria    /maintenance    data=${purge_si}    headers=${headers}
    ${jsondata}=    To Json    ${resp.content}
    Should Be Equal As Strings    ${resp.status_code}    200
    Should be Equal    ${jsondata['message']}    Aucun document à traiter


Envoi des accusés réception des consultations
    [Tags]    post
    [Documentation]    Envoi des accusés réception des consultations


    # Création du dossier connecté
    # [102]
    ${type} =  Set Variable  ADS_ERP__PC__PRE_DEMANDE_DE_COMPLETUDE_ERP
    ${emetteur} =  Set Variable  102_instr_pc_test_040
    ${json} =  Set Variable  { "type" : "${type}", "date" : "31/12/2015 14:42", "emetteur" : "${emetteur}", "dossier_instruction" : "PC0990991000002P0" }
    Vérifier le code retour du web service et vérifier que son message est  Post  messages  ${json}  200  Insertion du message '${type}' OK.

    # [103]
    ${type} =  Set Variable  ADS_ERP__PC__PRE_DEMANDE_DE_QUALIFICATION_ERP
    ${emetteur} =  Set Variable  103_instr_pc_test_040
    ${json} =  Set Variable  { "type" : "${type}", "date" : "31/12/2015 14:42", "emetteur" : "${emetteur}", "dossier_instruction" : "PC0990991000002P0" }
    Vérifier le code retour du web service et vérifier que son message est  Post  messages  ${json}  200  Insertion du message '${type}' OK.

    # Ajout des consultations
    # [106]
    ${type} =  Set Variable  ADS_ERP__PC__CONSULTATION_OFFICIELLE_POUR_CONFORMITE
    ${emetteur} =  Set Variable  106_instr_daact_test_040
    ${json} =  Set Variable  { "type" : "${type}", "date" : "31/12/2015 14:42", "emetteur" : "${emetteur}", "dossier_instruction" : "PC0990991000002P0", "contenu" : { "consultation" : 2, "date_envoi" : "31/12/2015", "service_abrege" : "ACC", "service_libelle" : "Service Accessibilité", "date_limite" : "31/01/2016" } }
    Vérifier le code retour du web service et vérifier que son message est  Post  messages  ${json}  200  Insertion du message '${type}' OK.

    # [104]
    ${type} =  Set Variable  ADS_ERP__PC__CONSULTATION_OFFICIELLE_POUR_AVIS
    ${emetteur} =  Set Variable  104_instr_pc_test_040
    ${json} =  Set Variable  { "type" : "${type}", "date" : "31/12/2015 14:42", "emetteur" : "${emetteur}", "dossier_instruction" : "PC0990991000002P0", "contenu" : { "consultation" : 2, "date_envoi" : "01/03/2013", "service_abrege" : "SI", "service_libelle" : "Service Sécurité", "date_limite" : "01/04/2013" } }
    Vérifier le code retour du web service et vérifier que son message est  Post  messages  ${json}  200  Insertion du message '${type}' OK.

    # Test du service
    Create Session    httpopenaria    http://localhost/openaria/services/rest_entry.php
    ${headers}=    Create Dictionary    'Content-Type=application/json'
    ${resp}=    Post Request    httpopenaria    /maintenance    data=${consultations}    headers=${headers}
    Should Be Equal As Strings    ${resp.status_code}    200
    ${json} =  Evaluate  json.loads('''${resp.content}''')    json
    Should Contain    ${json["message"]}  2 message(s)

    Create Session    httpopenaria    http://localhost/openaria/services/rest_entry.php
    ${headers}=    Create Dictionary    'Content-Type=application/json'
    ${resp}=    Post Request    httpopenaria    /maintenance    data=${consultations}    headers=${headers}
    Should Be Equal As Strings    ${resp.status_code}    200
    ${json} =  Evaluate  json.loads('''${resp.content}''')    json
    Should Contain    ${json["message"]}  0 message(s)
