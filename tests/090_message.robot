*** Settings ***
Resource  resources/resources.robot
Suite Setup  For Suite Setup
Suite Teardown  For Suite Teardown
Documentation  Les messages...


*** Keywords ***
L'intégration 'Tous les messages' est correcte
    Go To Submenu In Menu  dossiers  dossier_coordination_message_tous
    Page Title Should Be  Dossiers > Messages > Tous Les Messages
    First Tab Title Should Be  Message
    Element Should Be Visible  css=#adv-search-adv-fields


L'intégration 'Mes non lus' est correcte
    Go To Submenu In Menu  dossiers  dossier_coordination_message_mes_non_lu
    Page Title Should Be  Dossiers > Messages > Mes Non Lus
    First Tab Title Should Be  Message
    Element Should Not Be Visible  css=#adv-search-adv-fields


L'intégration 'Onglet Messages sur DC' est correcte
    [Arguments]  ${dc_libelle}
    Depuis le contexte du dossier de coordination  ${dc_libelle}
    On clique sur l'onglet  dossier_coordination_message_contexte_dc  Messages


L'intégration 'Onglet Messages sur DI' est correcte
    [Arguments]  ${di_libelle}
    Depuis le contexte du dossier d'instruction  ${di_libelle}
    On clique sur l'onglet  dossier_coordination_message_contexte_di  Messages


*** Test Cases ***
Constitution d'un jeu de données

    [Documentation]  L'objet de ce 'Test Case' est de constituer un jeu de
    ...  données cohérent pour les scénarios fonctionnels qui suivent.

    # En tant que profil ADMINISTRATEUR
    Depuis la page d'accueil  admin  admin
    Activer l'option référentiel ADS

    ##
    ## ETABLISSEMENT 01
    ##
    &{etab01} =  Create Dictionary
    ...  libelle=Magasin EIGHT TEST090
    ...  etablissement_nature=ERP potentiel
    ...  adresse_numero=85
    ...  adresse_voie=RUE GASTON BERGER
    ...  adresse_cp=13010
    ...  adresse_ville=MARSEILLE
    ...  adresse_arrondissement=10ème
    ${etab01_code} =  Ajouter l'établissement  ${etab01}
    ${etab01_titre} =  Set Variable  ${etab01_code} - ${etab01.libelle}
    Set Suite Variable  ${etab01}
    Set Suite Variable  ${etab01_code}
    Set Suite Variable  ${etab01_titre}

    ##
    ## ETABLISSEMENT 02
    ##
    &{etab02} =  Create Dictionary
    ...  libelle=MONDIAL EV TEST090
    ...  etablissement_nature=ERP potentiel
    ...  adresse_numero=4
    ...  adresse_numero2=bis
    ...  adresse_voie=AVE ROGER SALENGRO
    ...  adresse_complement=CHEZ M. DURAND
    ...  adresse_cp=13002
    ...  adresse_ville=MARSEILLE
    ...  adresse_arrondissement=2ème
    ...  siret=73282932000074
    ...  exp_nom=DURAND
    ...  exp_prenom=Jean
    ...  etablissement_statut_juridique=Ville
    ...  ref_patrimoine=120\n90
    ${etab02_code} =  Ajouter l'établissement  ${etab02}
    ${etab02_titre} =  Set Variable  ${etab02_code} - ${etab02.libelle}
    Set Suite Variable  ${etab02}
    Set Suite Variable  ${etab02_code}
    Set Suite Variable  ${etab02_titre}

    ##
    ## DOSSIER DE COORDINATION 01
    ##
    &{dc01} =  Create Dictionary
    ...  dossier_coordination_type=Autorisation de Travaux
    ...  description=Description
    ...  etablissement=${etab01_titre}
    ...  date_demande=09/11/2014
    ...  a_qualifier=false
    ...  dossier_instruction_secu=true
    ...  dossier_instruction_acc=true
    ${dc01_libelle} =  Ajouter le dossier de coordination  ${dc01}
    Set Suite Variable  ${dc01}
    Set Suite Variable  ${dc01_libelle}
    ${dc01_di_acc} =  Catenate  SEPARATOR=-  ${dc01_libelle}  ACC
    ${dc01_di_si} =  Catenate  SEPARATOR=-  ${dc01_libelle}  SI
    Set Suite Variable  ${dc01_di_acc}
    Set Suite Variable  ${dc01_di_si}


    ##
    ## DOSSIER DE COORDINATION 02
    ##
    &{dc02} =  Create Dictionary
    ...  dossier_coordination_type=Autorisation de Travaux
    ...  description=Description
    ...  etablissement=${etab02_titre}
    ...  date_demande=09/11/2014
    ...  a_qualifier=false
    ...  dossier_instruction_secu=true
    ...  dossier_instruction_acc=true
    ${dc02_libelle} =  Ajouter le dossier de coordination  ${dc02}
    Set Suite Variable  ${dc02}
    Set Suite Variable  ${dc02_libelle}
    ${dc02_di_acc} =  Catenate  SEPARATOR=-  ${dc02_libelle}  ACC
    ${dc02_di_si} =  Catenate  SEPARATOR=-  ${dc02_libelle}  SI
    Set Suite Variable  ${dc02_di_acc}
    Set Suite Variable  ${dc02_di_si}


Intégration

    [Documentation]  ...

    #
    Depuis la page d'accueil  cadre-si  cadre-si
    L'intégration 'Tous les messages' est correcte
    L'intégration 'Mes non lus' est correcte
    L'intégration 'Onglet Messages sur DC' est correcte  ${dc01_libelle}
    L'intégration 'Onglet Messages sur DI' est correcte  ${dc01_di_si}
    #
    Depuis la page d'accueil  cadre-acc  cadre-acc
    L'intégration 'Tous les messages' est correcte
    L'intégration 'Mes non lus' est correcte
    L'intégration 'Onglet Messages sur DC' est correcte  ${dc01_libelle}
    L'intégration 'Onglet Messages sur DI' est correcte  ${dc01_di_acc}
    #
    Depuis la page d'accueil  technicien-si  technicien-si
    L'intégration 'Mes non lus' est correcte
    L'intégration 'Onglet Messages sur DC' est correcte  ${dc01_libelle}
    L'intégration 'Onglet Messages sur DI' est correcte  ${dc01_di_si}
    #
    Depuis la page d'accueil  technicien-acc  technicien-acc
    L'intégration 'Mes non lus' est correcte
    L'intégration 'Onglet Messages sur DC' est correcte  ${dc01_libelle}
    L'intégration 'Onglet Messages sur DI' est correcte  ${dc01_di_acc}


Déconstitution du jeu de données

    [Documentation]  ...

    Depuis la page d'accueil  admin  admin
    Désactiver l'option référentiel ADS

