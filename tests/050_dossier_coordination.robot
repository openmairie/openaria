*** Settings ***
Resource  resources/resources.robot
Suite Setup  For Suite Setup
Suite Teardown  For Suite Teardown
Documentation  Les dossiers de coordination (DC)...


*** Test Cases ***
Création d'un dossier de coordination

    #
    Depuis la page d'accueil  cadre-si  cadre-si

    # On ouvre le menu
    Go To Submenu In Menu    dossiers    dossier_coordination_nouveau
    # On sélectionne le type du dossier de coordination
    Select From List By Label    css=#dossier_coordination_type    Autorisation de Travaux
    # On saisie une description
    Input Text    css=#description    Dossier de coordination concernant une autorisation de travaux
    # On vérifie que la date de la demande est à aujourd'hui
    Form Value Should Be    css=#date_demande    ${DATE_FORMAT_DD/MM/YYYY}
    # On vérifie les données valorisé par le paramétrage
    Checkbox Should Be Selected    css=#a_qualifier
    Checkbox Should Be Selected    css=#dossier_instruction_secu
    Checkbox Should Be Selected    css=#dossier_instruction_acc
    # On recherche un établissement grâce au champ "autocomplete"
    Input Text    css=#autocomplete-etablissement_tous-search    LE PACIFIQUE
    # On ajoute un établissement
    WUX  Click Link    css=a.autocomplete-etablissement_tous-newrecord
    # On renseigne seulement les champs obligatoire
    WUX  Input Text    css=#form-etablissement_tous-overlay #libelle    LE PACIFIQUE
    Select From List By Label    css=#form-etablissement_tous-overlay #etablissement_nature    ERP potentiel
    # On valide le sous-formulaire
    Click On Submit Button In Overlay    etablissement_tous
    # On vérifie le message affiché à l'utilisateur
    WUX  Valid Message Should Be    Vos modifications ont bien été enregistrées.
    # On ferme l'overlay
    Click On Back Button In Overlay    etablissement_tous
    # On vérifie que le dossier de coordination est bien lié à l'établissement
    Element Should Be Visible    css=#autocomplete-etablissement_tous-check
    # On sélectionne deux types secondaires
    @{ITEMS} =  Create List  [M]  [GA]
    Select From Multiple Chosen List  css=#etablissement_type_secondaire  ${ITEMS}
    # On valide le formulaire
    Click On Submit Button
    # On vérifie le message affiché à l'utilisateur
    Valid Message Should Be    Vos modifications ont bien été enregistrées.
    # On récupère le libelle du dossier de coordination
    ${dossier_coordination_libelle} =    Get Text    css=div.form-content span#libelle
    Set Suite Variable    ${dossier_coordination_libelle}
    #
    &{contact01} =   Create Dictionary
    ...  contact_type=(*) Demandeur Principal
    ...  nom=DUPONTTEST050
    Set Suite Variable  ${contact01}
    Ajouter le contact depuis le DC  ${dossier_coordination_libelle}  ${contact01}


Listing des dossiers de coordination
    [Documentation]  L'objet de ce TestCase est de vérifier l'inétgration des
    ...  listings de DC.
    ...
    ...  - tri sur la colonne adresse ne produit pas une erreur de base de données.

    Depuis la page d'accueil  admin  admin
    #
    Depuis le listing des dossiers de coordination

    Element Text Should Be  css=#tab-dossier_coordination tr.tab-title th.col-3 span a  adresse
    Click Element  css=#tab-dossier_coordination tr.tab-title th.col-3 span a
    La page ne doit pas contenir d'erreur


Recherche avancée d'un dossier de coordination
    [Documentation]  L'objet de ce TestCase est de vérifier le fonctionnement
    ...  du formulaire de recherche avancée d'un dossier de coordination.
    ...
    ...  - La recherche sur le critère geolocalise n'est pas disponible
    ...    si l'option sig externe n'est pas activé (c'est le cas dans ce
    ...    TestSuite)

    Depuis la page d'accueil  admin  admin
    # Nous sommes dans le contexte standard (aucune option_sig activée)
    # La recherche avancée sur le listing des DC ne doit pas faire
    # apparaître le champ localise dédiée aux options SIG
    Depuis le listing des dossiers de coordination
    Element Should Not Be Visible  css=#adv-search-adv-fields #geolocalise
    Element Should Not Be Visible  css=#adv-search-adv-fields #lib-geolocalise


Modification d'un dossier de coordination

    [Documentation]  L'objet de ce test case est de gérer les aspects
    ...  particuliers de la modification d'un dossier de coordination :
    ...  - modification des types secondaires pour vérifier le bon
    ...    fonctionnement de la liaison n à n.

    #
    Depuis la page d'accueil  cadre-si  cadre-si

    # On accède au formulaire de modification du DC
    Depuis le formulaire de modification du dossier de coordination  ${dossier_coordination_libelle}

    # On sélectionne deux types secondaires des deux sélectionnés précédemment
    # lors de la création
    @{ITEMS} =  Create List  [EF]  [J]  [L]
    Select From Multiple Chosen List  css=#etablissement_type_secondaire  ${ITEMS}

    # On valide le formulaire et on vérifie que les messages sont corrects.
    Click On Submit Button
    Valid Message Should Be  Vos modifications ont bien été enregistrées.

    # On accède à la fiche de visualisation du DC
    Depuis le contexte du dossier de coordination  ${dossier_coordination_libelle}
    #
    Element Should Contain  css=#etablissement_type_secondaire  [J] Structures d'accueil pour personnes âgées et personnes handicapées
    Element Should Contain  css=#etablissement_type_secondaire  [EF] Établissements flottants (eaux intérieures)
    Element Should Contain  css=#etablissement_type_secondaire  [L] Salles d'audition, de conférences, de réunions, de spectacles ou à usages multiples
    Element Should Contain  css=#etablissement_type_secondaire  [M] Magasin de vente, de centre commerciaux
    Element Should Contain  css=#etablissement_type_secondaire  [GA] Gares Accessibles au public (chemins de fer, téléphériques, remonte-pentes...)

    # TNR - Lors de la modification du DC, le lien avec ses contacts étaient
    # perdus, on vérifie donc qu'après la modification le contact est toujours
    # lié.
    Depuis l'onglet "Contacts" du DC  ${dossier_coordination_libelle}
    Page Should Contain  ${contact01.nom}


Qualification d'un dossier de coordination
    #
    Depuis la page d'accueil  cadre-si  cadre-si
    #
    Depuis le formulaire de modification du dossier de coordination    ${dossier_coordination_libelle}
    # # On ajoute un contact
    # WUX  Click Element    css=legend.collapsed
    # WUX  Click Element    css=#add_contact
    # Select From List By Label    css=#contact_type    Mandataire
    # Select From List By Label    css=#civilite    M.
    # Input Text    css=#nom    Zhang
    # Input Text    css=#prenom    Cheng
    # Click On Submit Button In Overlay    contact
    # Click On Back Button In Overlay    contact
    # WUX  Page Should Contain    M. Zhang Cheng
    # On décoche "a_qualifier"
    Unselect Checkbox    css=#a_qualifier
    # On vérifie que les DI sont cochés
    Checkbox Should Be Selected    css=#dossier_instruction_secu
    Checkbox Should Be Selected    css=#dossier_instruction_acc
    # On ajoute les références cadastrales
    Input Text    css=input.refquart    77
    Input Text    css=input.refsect    G
    Input Text    css=input.refparc    77
    # On qualifie l'établissement
    Select From Chosen List  css=#etablissement_type  [CTS]
    Select From Chosen List  css=#etablissement_categorie  [1]
    Select Checkbox    css=#etablissement_locaux_sommeil
    # On déqualifie l'établissement en supprimant son type
    Select Checkbox    css=#erp
    Unselect From Chosen List  css=#etablissement_type
    # On valide le formulaire
    Click On Submit Button
    # On vérifie le message d'erreur affiché à l'utilisateur
    Error Message Should Contain    Le type de l'établissement doit être saisi lorsqu'il s'agit d'un ERP.
    # On qualifie l'établissement
    Select From Chosen List  css=#etablissement_type  [CTS]
    # On valide le formulaire
    Click On Submit Button
    # On vérifie le message affiché à l'utilisateur
    Valid Message Should Contain    Vos modifications ont bien été enregistrées.
    Valid Message Should Contain    Le dossier d'instruction ${dossier_coordination_libelle}-SI a été créé.
    Valid Message Should Contain    Le dossier d'instruction ${dossier_coordination_libelle}-ACC a été créé.
    #
    Form Static Value Should Be  css=#erp  Oui


Dossiers de coordination à clôturer
    [Documentation]  Un widget et un listing permettent d'afficher et de lister
    ...  les dossiers de coordination à clôturer, c'est-à-dire les DC non clôturés,
    ...  qualifiés, dont tous les DI sont clôturés.
    ...
    ...  - ...
    ...  - Vérification des filtres des onglets Contraintes, DC Fils, Documents
    ...    Générés dans le contexte d'un DC à clôturer (sur un DC clôturé et sur
    ...    un DC non clôturé)
    ...  - ...

    Depuis la page d'accueil  admin  admin

    # Constitution du jeu de données spécifique au 'Test Case' :
    # - 1 établissement etab_l_01 commun aux 3 DC
    &{etab_l_01} =  Create Dictionary
    ...  libelle=L'ATLANTIQUE TEST050
    ...  etablissement_nature=ERP potentiel
    ${etab_l_01.code} =  Ajouter l'établissement  ${etab_l_01}
    ${etab_l_01.titre} =  Set Variable  ${etab_l_01.code} - ${etab_l_01.libelle}
    # - 1 DC dc_l_01 que l'on qualifie auquel on rattache un contact contact_l_01
    #   et un document généré dg_l_01, on qualifie et clôture le DI ce qui
    #   positionne le DC comme à clôturer
    &{dc_l_01} =  Create Dictionary
    ...  dossier_coordination_type=Visite de contrôle ACC
    ...  description=Dossier de coordination concernant une autorisation de travaux
    ...  etablissement=${etab_l_01.titre}
    ...  date_demande=${DATE_FORMAT_DD/MM/YYYY}
    ...  a_qualifier=false
    ${dc_l_01.libelle} =  Ajouter le dossier de coordination  ${dc_l_01}
    &{contact_l_01} =   Create Dictionary
    ...  contact_type=(*) Demandeur Principal
    ...  nom=DURANTTEST050
    Ajouter le contact depuis le DC  ${dc_l_01.libelle}  ${contact_l_01}
    @{contacts_lies}  Create List  (${contact_l_01.contact_type}) ${contact_l_01.nom}
    ${params}  Create Dictionary  contacts_lies=@{contacts_lies}
    &{dg_l_01} =  Create Dictionary
    ...  modele_edition=Courrier simple
    ...  courrier_type=Courrier simple
    Ajouter le document généré depuis le contexte du dossier de coordination
    ...  ${dc_l_01.libelle}
    ...  ${params}
    ...  ${dg_l_01.courrier_type}
    ...  ${dg_l_01.modele_edition}
    ${dg_l_01.code_barres} =  Get Text  css=#code_barres
    Qualifier le dossier d'instruction  ${dc_l_01.libelle}-ACC
    Click On Form Portlet Action  dossier_instruction  cloturer
    # - 1 DC dc_l_02 avec le DC dc_l_01 comme DC Parent, que l'on qualifie,
    #   auquel on rattache un contact contact_l_02 et un document généré dg_l_02,
    #   on qualifie et clôture le DI ce qui positionne le DC comme à clôturer
    &{dc_l_02} =  Create Dictionary
    ...  dossier_coordination_type=Visite de contrôle ACC
    ...  description=Dossier de coordination concernant une autorisation de travaux
    ...  etablissement=${etab_l_01.titre}
    ...  date_demande=${DATE_FORMAT_DD/MM/YYYY}
    ...  a_qualifier=false
    ...  dossier_coordination_parent=${dc_l_01.libelle}
    ${dc_l_02.libelle} =  Ajouter le dossier de coordination  ${dc_l_02}
    &{contact_l_02} =   Create Dictionary
    ...  contact_type=(*) Demandeur Principal
    ...  nom=DURANDTEST050
    Ajouter le contact depuis le DC  ${dc_l_02.libelle}  ${contact_l_02}
    @{contacts_lies}  Create List  (${contact_l_02.contact_type}) ${contact_l_02.nom}
    ${params}  Create Dictionary  contacts_lies=@{contacts_lies}
    &{dg_l_02} =  Create Dictionary
    ...  modele_edition=Courrier simple
    ...  courrier_type=Courrier simple
    Ajouter le document généré depuis le contexte du dossier de coordination
    ...  ${dc_l_02.libelle}
    ...  ${params}
    ...  ${dg_l_02.courrier_type}
    ...  ${dg_l_02.modele_edition}
    ${dg_l_02.code_barres} =  Get Text  css=#code_barres
    Qualifier le dossier d'instruction  ${dc_l_02.libelle}-ACC
    Click On Form Portlet Action  dossier_instruction  cloturer
    # - 1 DC dc_l_03 que l'on qualifie, auquel on rattache un contact
    #   contact_l_03 et un document généré dg_l_03
    &{dc_l_03} =  Create Dictionary
    ...  dossier_coordination_type=Permis de construire
    ...  description=Dossier de coordination concernant un permis de construire
    ...  etablissement=${etab_l_01.titre}
    ...  a_qualifier=false
    ...  date_butoir=${DATE_FORMAT_DD/MM/YYYY}
    ${dc_l_03.libelle} =  Ajouter le dossier de coordination  ${dc_l_03}
    &{contact_l_03} =   Create Dictionary
    ...  contact_type=(*) Demandeur Principal
    ...  nom=DURANXTEST050
    Ajouter le contact depuis le DC  ${dc_l_03.libelle}  ${contact_l_03}
    @{contacts_lies}  Create List  (${contact_l_03.contact_type}) ${contact_l_03.nom}
    ${params}  Create Dictionary  contacts_lies=@{contacts_lies}
    &{dg_l_03} =  Create Dictionary
    ...  modele_edition=Courrier simple
    ...  courrier_type=Courrier simple
    Ajouter le document généré depuis le contexte du dossier de coordination
    ...  ${dc_l_03.libelle}
    ...  ${params}
    ...  ${dg_l_03.courrier_type}
    ...  ${dg_l_03.modele_edition}
    ${dg_l_03.code_barres} =  Get Text  css=#code_barres

    # Vérification des filtres sur certains onglets depuis le contexte du DC
    # dc_l_01 à clôturer (dossier non clôturé)
    Go To Submenu In Menu  dossiers  dossier_coordination_a_cloturer
    Use Simple Search  DC  ${dc_l_01.libelle}
    Click On Link  ${dc_l_01.libelle}
    # TNR - Le message "Droits insuffisants. Vous n'avez pas suffisament de
    # droits pour accéder à cette page." apparaissait. On s'assure que ça
    # n'arrive plus.
    On clique sur l'onglet  lien_contrainte_dossier_coordination  Contraintes
    Element Should Contain  css=#sousform-lien_contrainte_dossier_coordination  Aucune contrainte appliquée au DC.
    Element Should Not Contain  css=#sousform-lien_contrainte_dossier_coordination  Droits insuffisants. Vous n'avez pas suffisament de droits pour accéder à cette page.
    # Vérification du filtre sur les DC Fils, on doit retrouver le dc_l_02
    # pour lequel on a paramétré le dc_l_01 comme dossier parent.
    On clique sur l'onglet  dossier_coordination  DC Fils
    Element Should Contain  css=#sousform-dossier_coordination  ${dc_l_02.libelle}
    Element Should Not Contain  css=#sousform-dossier_coordination  ${dc_l_01.libelle}
    Element Should Not Contain  css=#sousform-dossier_coordination  ${dc_l_03.libelle}
    # Vériication du filtre sur les documents générés, on doit retrouver seulement
    # le document généré ajouté sur le DC du contexte.
    On clique sur l'onglet  courrier  Documents Générés
    Element Should Contain  css=#sousform-courrier  ${dg_l_01.code_barres}
    Element Should Not Contain  css=#sousform-courrier  ${dg_l_02.code_barres}
    Element Should Not Contain  css=#sousform-courrier  ${dg_l_03.code_barres}

    # Vérification des filtres sur certains onglets depuis le contexte du DC
    # dc_l_02 à clôturer (dossier clôturé)
    Go To Submenu In Menu  dossiers  dossier_coordination_a_cloturer
    Use Simple Search  DC  ${dc_l_02.libelle}
    Click On Link  ${dc_l_02.libelle}
    # On clôture le DC avant de faire les vérifications dans les onglets
    Click On Form Portlet Action  dossier_coordination_a_cloturer  cloturer
    # TNR - Le message "Droits insuffisants. Vous n'avez pas suffisament de
    # droits pour accéder à cette page." apparaissait. On s'assure que ça
    # n'arrive plus.
    On clique sur l'onglet  lien_contrainte_dossier_coordination  Contraintes
    Element Should Contain  css=#sousform-lien_contrainte_dossier_coordination  Aucune contrainte appliquée au DC.
    Element Should Not Contain  css=#sousform-lien_contrainte_dossier_coordination  Droits insuffisants. Vous n'avez pas suffisament de droits pour accéder à cette page.
    # Vérification du filtre sur les DC Fils, on doit retrouver aucun enregistrement.
    On clique sur l'onglet  dossier_coordination  DC Fils
    Element Should Contain  css=#sousform-dossier_coordination  Aucun enregistrement.
    # Vériication du filtre sur les documents générés, on doit retrouver seulement
    # le document généré ajouté sur le DC du contexte.
    On clique sur l'onglet  courrier  Documents Générés
    Element Should Contain  css=#sousform-courrier  ${dg_l_02.code_barres}
    Element Should Not Contain  css=#sousform-courrier  ${dg_l_01.code_barres}
    Element Should Not Contain  css=#sousform-courrier  ${dg_l_03.code_barres}


Clôture d'un dossier de coordination
    [Documentation]  Clôture/Déclôture d'un dossier de coordination :
    ...  - ...
    ...  - Clôture automatique des DI du DC
    ...  - TNR Clôture possible si au moins un DI est déjà clôturé
    ...  - ...
    #
    Depuis la page d'accueil  cadre-si  cadre-si
    Qualifier le dossier d'instruction    ${dossier_coordination_libelle}-SI

    #
    Depuis la page d'accueil    cadre-acc    cadre-acc
    Qualifier le dossier d'instruction    ${dossier_coordination_libelle}-ACC

    #
    Depuis la page d'accueil  cadre-si  cadre-si
    #
    Depuis le contexte du dossier de coordination    ${dossier_coordination_libelle}
    # On clique sur le bouton clôturer
    Click On Form Portlet Action    dossier_coordination    cloturer
    # On vérifie le message affiché à l'utilisateur
    WUX  Valid Message Should Be    Le dossier de coordination a été correctement cloturé.
    # On vérifie que le champ "Clôturé" est à oui
    Form Static Value Should Be    css=#dossier_cloture    Oui
    # On vérifie que les actions modifier et supprimer ne sont plus affichés
    Portlet Action Should Not Be In Form    dossier_coordination    modifier
    Portlet Action Should Not Be In Form    dossier_coordination    supprimer
    # On vérifie que le dossier d'instruction SI est aussi clôturé
    Click On Link    ${dossier_coordination_libelle}-SI
    # On vérifie que le champ "Clôturé" est à oui
    Form Static Value Should Be    css=#dossier_cloture    Oui
    # On vérifie que les actions modifier et supprimer ne sont plus affichés
    Portlet Action Should Not Be In Form    dossier_instruction_tous_plans    modifier
    Portlet Action Should Not Be In Form    dossier_instruction_tous_plans    supprimer

    #
    Depuis la page d'accueil    cadre-acc    cadre-acc
    #
    Depuis le contexte du dossier de coordination    ${dossier_coordination_libelle}
    # On vérifie que le dossier d'instruction ACC est aussi clôturé
    Click On Link    ${dossier_coordination_libelle}-ACC
    # On vérifie que le champ "Clôturé" est à oui
    Form Static Value Should Be    css=#dossier_cloture    Oui
    # On vérifie que les actions modifier et supprimer ne sont plus affichés
    Portlet Action Should Not Be In Form    dossier_instruction_tous_plans    modifier
    Portlet Action Should Not Be In Form    dossier_instruction_tous_plans    supprimer
    # On clique sur le dossier de coordination
    Click On Link    ${dossier_coordination_libelle}

    # Le DC ne doit pas apparaître dans le listing des dossier à clôturer
    Go To Submenu In Menu    dossiers    dossier_coordination_a_cloturer
    Page Should Not Contain    ${dossier_coordination_libelle}

    #
    Depuis la page d'accueil    admin    admin
    #
    Depuis le contexte du dossier de coordination    ${dossier_coordination_libelle}
    # On clique sur le bouton réouvrir
    Click On Form Portlet Action    dossier_coordination    decloturer
    # On vérifie que le champ "Clôturé" est à non
    WUX  Form Static Value Should Be    css=#dossier_cloture    Non
    # On vérifie que les actions modifier et supprimer ne sont plus affichés
    Portlet Action Should Be In Form    dossier_coordination    modifier
    Portlet Action Should Be In Form    dossier_coordination    supprimer

    Depuis la page d'accueil    cadre-acc    cadre-acc
    # Le DC doit apparaître dans le listing des dossier à clôturer
    Go To Submenu In Menu    dossiers    dossier_coordination_a_cloturer
    Page Should Contain    ${dossier_coordination_libelle}

    # TNR Clôture possible si au moins un DI est déjà clôturé
    Depuis la page d'accueil  cadre-si  cadre-si
    Depuis le contexte du dossier de coordination  ${dossier_coordination_libelle}
    Click On Form Portlet Action  dossier_coordination  cloturer
    WUX  Form Static Value Should Be  css=#dossier_cloture  Oui


Récupération de la qualification d'un DC parent
    [Documentation]  ...
    #
    Depuis la page d'accueil  cadre-si  cadre-si
    #
    Depuis le contexte de l'établissement  null  LE PACIFIQUE
    ${etablissement_id} =  Get Value  css=#etablissement

    # On clique sur le tableau de bord
    Go To Dashboard
    # On ouvre le menu
    Go To Submenu In Menu    dossiers    dossier_coordination_nouveau
    # On recherche un établissement grâce au champ "autocomplete"
    Input Text    css=#autocomplete-etablissement_tous-search    LE PACIFIQUE
    # On lie cet établissement
    WUX  Click Link  css=a.autocomplete-etablissement_tous-result-${etablissement_id}

    # On sélectionne le DC parent nouvellement créé
    WUX  Select From List By Label  css=#dossier_coordination_parent  ${dossier_coordination_libelle}

    # On vérifie la récupération de sa qualification
    WUX  Checkbox Should Be Selected  css=#erp
    Checkbox Should Be Selected  css=#etablissement_locaux_sommeil
    WUX  Selected Label From Chosen List Should Be  css=#etablissement_type  [CTS] Chapiteaux, Tentes et Structures toile
    WUX  Selected Label From Chosen List Should Be  css=#etablissement_categorie  [1] plus de 1500 personnes


Gestion des contacts
    #
    Depuis la page d'accueil  cadre-si  cadre-si
    # On ouvre le menu
    Go To Submenu In Menu    dossiers    dossier_coordination_nouveau
    # On sélectionne le type du dossier de coordination
    Select From List By Label    css=#dossier_coordination_type    Autorisation de Travaux
    # On ajoute un contact
    WUX  Click Element    css=legend.collapsed
    WUX  Click Element    css=#add_contact
    ${status} =  Run Keyword And Return Status  WUX  Element Should Be Visible  css=#contact_type
    Run Keyword Unless  ${status}  Click Element  css=#add_contact
    WUX  Select From List By Label    css=#contact_type    Mandataire
    Select From List By Label    css=#civilite    M.
    Input Text    css=#nom    Zhang
    Input Text    css=#prenom    Cheng
    Click On Submit Button In Overlay    contact
    Click On Back Button In Overlay    contact
    WUX  Page Should Contain    M. Zhang Cheng
    # On supprime le contact
    Click Element    css=span.contact_del
    Page Should Not Contain    M. Zhang Cheng
    # On rajoute deux contacts
    WUX  Click Element    css=#add_contact
    Input Text    css=#nom    CONTACT1
    Click On Submit Button In Overlay    contact
    Click On Back Button In Overlay    contact
    WUX  Click Element    css=#add_contact
    Input Text    css=#nom    CONTACT2
    Click On Submit Button In Overlay    contact
    Click On Back Button In Overlay    contact
    # On édite un contact
    Click On Link    CONTACT2
    WUX  Input Text    css=#nom    ${empty}
    Click On Submit Button In Overlay    contact
    Page Should Contain    Le champ nom est obligatoire.
    Input Text    css=#nom    CONTACT3
    Click On Submit Button In Overlay    contact
    Click On Back Button In Overlay    contact
    # Cas de la personne morale
    WUX  Click Element    css=#add_contact
    Select From List By Label    css=#qualite    personne morale
    Input Text    css=#adresse_ville    Paris
    Click On Submit Button In Overlay    contact
    WUX  Error Message Should Contain    Un des champs dénomination ou raison sociale doit être rempli.
    Input Text    css=#denomination    MORALE1
    Click On Submit Button In Overlay    contact
    Click On Back Button In Overlay    contact
    # Vérification
    Page Should Contain   CONTACT1
    Page Should Not Contain    CONTACT2
    Page Should Contain   CONTACT3
    Page Should Contain   MORALE1
    # Validation et revérification
    Click On Submit Button
    Valid Message Should Be    Vos modifications ont bien été enregistrées.
    #
    On clique sur l'onglet    contact_contexte_dossier_coordination    Contacts
    WUX  Page Should Contain   CONTACT1
    Page Should Not Contain    M. Zhang Cheng
    Page Should Not Contain    CONTACT2
    Page Should Contain   CONTACT3
    Page Should Contain   MORALE1


Gestion des données de l'établissement
    #
    Depuis la page d'accueil  cadre-si  cadre-si
    # On ajoute un établissement ERP
    &{etab01} =  Create Dictionary
    ...  libelle=etab_gestion_donnees_etab
    ...  etablissement_nature=ERP Référentiel
    ...  etablissement_type=PA
    ...  etablissement_categorie=1
    ...  si_autorite_competente_visite=Commission communale de sécurité
    ${etab01_code} =  Ajouter l'établissement  ${etab01}
    ${etab01_titre} =  Set Variable  ${etab01_code} - ${etab01.libelle}
    Set Suite Variable  ${etab01}
    Set Suite Variable  ${etab01_code}
    Set Suite Variable  ${etab01_titre}

    # On crée un dossier de coordination
    Go To Dashboard
    Go To Submenu In Menu    dossiers    dossier_coordination_nouveau
    # On sélectionne le type du dossier de coordination
    Select From List By Label    css=#dossier_coordination_type    Autorisation de Travaux
    # On saisie une description
    Input Text    css=#description    description
    # On sélectionne un établissement
    Input Text    css=#autocomplete-etablissement_tous-search    ${etab01_titre}
    WUX  Click Link    ${etab01_titre}
    # On vérifie les données du formulaire
    WUX  Checkbox Should Be Selected    css=#erp
    WUX  Selected Label From Chosen List Should Be  css=#etablissement_type  [PA] Établissements de Plein Air
    WUX  Selected Label From Chosen List Should Be  css=#etablissement_categorie  [1] plus de 1500 personnes
    # On change les valeurs
    Select From Chosen List  css=#etablissement_type  [REF]
    Select From Chosen List  css=#etablissement_categorie  [3]
    Select Checkbox    css=#etablissement_locaux_sommeil
    # On valide le formulaire
    Click On Submit Button
    # On vérifie le message affiché à l'utilisateur
    Valid Message Should Contain    Vos modifications ont bien été enregistrées.
    # On récupère le libellé du dossier de coordination
    ${dossier_coordination_parent_libelle} =    Get Text    id=libelle

    # On crée un dossier de coordination avec un dossier de coordination parent
    Go To Dashboard
    Go To Submenu In Menu    dossiers    dossier_coordination_nouveau
    # On sélectionne le type du dossier de coordination
    Select From List By Label    css=#dossier_coordination_type    Autorisation de Travaux
    # On saisie une description
    Input Text    css=#description    description
    # On sélectionne un établissement
    Input Text    css=#autocomplete-etablissement_tous-search    ${etab01_titre}
    WUX  Click Link    ${etab01_titre}
    # On vérifie les données du formulaire
    WUX  Checkbox Should Be Selected    css=#erp
    WUX  Selected Label From Chosen List Should Be  css=#etablissement_type  [PA] Établissements de Plein Air
    WUX  Selected Label From Chosen List Should Be  css=#etablissement_categorie  [1] plus de 1500 personnes
    # On ajoute le dossier de coordination parent
    WUX  Select From List By Label    css=#dossier_coordination_parent    ${dossier_coordination_parent_libelle}
    # On vérifie les valeurs
    WUX  Checkbox Should Be Selected    css=#erp
    WUX  Selected Label From Chosen List Should Be  css=#etablissement_type  [REF] Refuges de montagne
    WUX  Selected Label From Chosen List Should Be  css=#etablissement_categorie  [3] de 301 à 700 personnes
    Checkbox Should Be Selected    css=#etablissement_locaux_sommeil
    # On valide le formulaire
    Click On Submit Button
    # On vérifie le message affiché à l'utilisateur
    Valid Message Should Contain    Vos modifications ont bien été enregistrées.
    # On vérifie qu'il n'y a pas d'erreur
    La page ne doit pas contenir d'erreur


Gestion des données de l'établissement ajouté depuis le dossier de coordination
    [Documentation]  Depuis le formuaire d'ajout et de modification d'un DC,
    ...  un autocomplete permet de lier un établissement existant ou d'en créer
    ...  un nouveau directement dans un overlay sans sortir du formulaire du
    ...  DC.

    ${tcvid} =  Set Variable  T050 GDDDLEADLDDC
    Depuis la page d'accueil  cadre-si  cadre-si

    # On accède au formulaire d'ajout d'un DC
    Depuis le formulaire d'ajout du dossier de coordination
    # On rensiegne les informations sur le DC
    Select From List By Label  css=#dossier_coordination_type  Autorisation de Travaux
    Input Text  css=#description  DCL01 ${tcvid}
    # On recherche un établissement grâce au champ "autocomplete"
    Input Text  css=#autocomplete-etablissement_tous-search  cherche
    # On clique sur le lien qui apparaît dans les résultats de recherche 'Ajouter un établissement'
    WUX  Click Link  css=a.autocomplete-etablissement_tous-newrecord
    # On renseigne les champs obligatoires dans le formulaire d'ajout d'un établissement dans l'overlay
    WUX  Input Text  css=#form-etablissement_tous-overlay #libelle  ETABl01 ${tcvid}
    Select From List By Label  css=#form-etablissement_tous-overlay #etablissement_nature  ERP Référentiel
    Select From Chosen List  css=#form-etablissement_tous-overlay #etablissement_type  [PA]
    Select From Chosen List  css=#form-etablissement_tous-overlay #etablissement_categorie  [4]
    Select From List By Label  css=#si_autorite_competente_visite  Commission communale de sécurité
    # On valide le formulaire de création de l'établissement
    Click On Submit Button In Overlay  etablissement_tous
    # On vérifie le message affiché à l'utilisateur
    WUX  Valid Message Should Be  Vos modifications ont bien été enregistrées.
    # On ferme l'overlay
    Click On Back Button In Overlay  etablissement_tous
    # On vérifie que le dossier de coordination est bien lié à l'établissement dans le formulaire du DC
    Element Should Be Visible  css=#autocomplete-etablissement_tous-check
    # On vérifie les données préremplies avec les donées de l'établissement dans le formulaire du DC
    WUX  Checkbox Should Be Selected  css=#erp
    WUX  Selected Label From Chosen List Should Be  css=#etablissement_type  [PA] Établissements de Plein Air
    WUX  Selected Label From Chosen List Should Be  css=#etablissement_categorie  [4] 300 personnes et moins, à l'exception des établissements compris dans la 5e catégorie
    # On  modifie les valeurs de ces champs dans le DC
    Select From Chosen List  css=#etablissement_type  [REF]
    Select From Chosen List  css=#etablissement_categorie  [3]
    Select Checkbox  css=#etablissement_locaux_sommeil
    # On valide le formulaire
    Click On Submit Button
    Valid Message Should Contain  Vos modifications ont bien été enregistrées.
    La page ne doit pas contenir d'erreur


Gestion du dossier de coordination de visites périodiques

    [Documentation]    Création puis clôture d'un dossier de coordination de
    ...    visites périodiques et vérification sur l'établissement des
    ...    changements de valeurs.

    #
    Depuis la page d'accueil  cadre-si  cadre-si
    #
    &{etab02} =  Create Dictionary
    ...  libelle=MONDIAL EVASION
    ...  etablissement_nature=ERP Référentiel
    ...  etablissement_type=R
    ...  etablissement_categorie=1
    ...  si_autorite_competente_visite=Commission communale de sécurité
    ...  adresse_numero=15
    ...  adresse_voie=RUE DE ROME
    ...  adresse_cp=13006
    ...  adresse_ville=MARSEILLE
    ...  adresse_arrondissement=6ème
    ...  siret=73282932000074
    ...  exp_civilite=M.
    ...  exp_nom=Norman
    ...  exp_prenom=Paul
    ...  etablissement_etat=Ouvert
    ...  si_derniere_visite_periodique_date=28/01/2015
    ${etab02_code} =  Ajouter l'établissement  ${etab02}
    ${etab02_titre} =  Set Variable  ${etab02_code} - ${etab02.libelle}
    Set Suite Variable  ${etab02}
    Set Suite Variable  ${etab02_code}
    Set Suite Variable  ${etab02_titre}
    #
    &{dc01} =  Create Dictionary
    ...  dossier_coordination_type=Visite périodique SI
    ...  description=Dossier de coordination de type périodique
    ...  etablissement=${etab02_titre}
    ...  date_demande=28/01/2017
    ${dc01_libelle} =  Ajouter le dossier de coordination  ${dc01}
    Set Suite Variable    ${dc01}
    Set Suite Variable    ${dc01_libelle}
    # On vérifie qu'il n'y a pas la possiblité de clôturer le DC
    Portlet Action Should Not Be In Form    dossier_coordination    cloturer
    #
    Qualifier le dossier d'instruction    ${dc01_libelle}-SI
    #
    Depuis le contexte du dossier d'instruction de type visite    ${dc01_libelle}-SI
    # On vérifie qu'il n'y a pas la possiblité de clôturer le DI
    Portlet Action Should Not Be In Form    dossier_instruction_tous_visites    cloturer
    #
    Depuis le contexte de l'établissement    ${etab02_code}    ${etab02.libelle}
    #
    Vérifier les champs périodiques de l'établissement    ${dc01_libelle}    28/01/2015    28/01/2017    2 ans
    #
    ${semaine_programmation} =    Ajouter une semaine de programmation    2015    6
    #
    Ajouter une visite depuis la programmation    ${semaine_programmation}    Paul DURAND    ${dc01_libelle}-SI    2    true
    #
    Depuis le contexte du dossier de coordination    ${dc01_libelle}
    # On vérifie qu'il y a la possiblité de clôturer le DC
    Portlet Action Should Be In Form    dossier_coordination    cloturer
    #
    Depuis le contexte du dossier d'instruction de type visite    ${dc01_libelle}-SI
    # On vérifie qu'il y a la possiblité de clôturer le DI
    Portlet Action Should Be In Form    dossier_instruction_tous_visites    cloturer
    # On clôture le dossier de coordination
    Clôturer le dossier de coordination    ${dc01_libelle}
    # On vérifie qu'il n'y a pas la possiblité de réouvrir le DC
    Portlet Action Should Not Be In Form    dossier_coordination    decloturer
    #
    Depuis le contexte du dossier d'instruction de type visite    ${dc01_libelle}-SI
    # On vérifie qu'il n'y a pas la possiblité de réouvrir le DI
    Portlet Action Should Not Be In Form    dossier_coordination    decloturer
    #
    Depuis le contexte de l'établissement    ${etab02_code}    ${etab02.libelle}
    # On vérifie que le libellé de l'ancien DC périodique n'apparaît plus
    Page Should Not Contain    ${dc01_libelle}
    #
    Vérifier les champs périodiques de l'établissement    null    05/02/2015    05/02/2017    2 ans

Gestion des contraintes appliquées aux dossiers de coordination

    [Documentation]    Test des vues et actions contraintes appliquées :
    ...  - tableau
    ...  - ajouter
    ...  - modifier
    ...  - supprimer
    ...  - récupérer
    ...  - démarquer comme récupérée

    @{ref_cad} =  Create List  800  A  2
    ${date_fr} =    Date du jour FR
    &{etab} =  Create Dictionary
    ...  libelle=FIT N GIT
    ...  etablissement_nature=ERP Référentiel
    ...  etablissement_type=PA
    ...  etablissement_categorie=1
    ...  si_autorite_competente_visite=Commission communale de sécurité
    ...  etablissement_etat=Ouvert
    ...  adresse_numero=28
    ...  adresse_voie=BD DE LA BLANCARDE
    ...  adresse_cp=13004
    ...  adresse_ville=MARSEILLE
    ...  adresse_arrondissement=4ème
    ...  siret=73282932000074
    ...  exp_nom=Shouho
    ...  exp_prenom=Pierre
    Depuis la page d'accueil  cadre-si  cadre-si
    ${etab} =  Ajouter l'établissement  ${etab}
    ${etab} =  Set Variable  ${etab} - FIT N GIT
    &{dc} =  Create Dictionary
    ...  dossier_coordination_type=Visite de réception
    ...  description=Agrandissement
    ...  etablissement=${etab}
    ...  date_demande=${date_fr}
    ${dc} =  Ajouter le dossier de coordination  ${dc}
    # Cas 1 : pas d'option SIG
    Depuis le contexte du dossier de coordination  ${dc}
    On clique sur l'onglet  lien_contrainte_dossier_coordination  Contraintes
    Page Should Not Contain Link  recuperer_contraintes
    Page Should Contain  Aucune contrainte appliquée au DC.
    # Cas 2 : pas de référence cadastrale
    Depuis la page d'accueil  admin  admin
    Activer le plugin geoaria_tests et l'option sig
    &{contrainte} =  Create Dictionary
    ...  groupe=ZONE DU POS
    ...  sousgroupe=M5
    ...  nature=POS
    ...  libelle=Contrainte 13055.M50
    ...  texte=Texte
    ...  texte_surcharge=Surcharge
    Ajouter la contrainte  ${contrainte}
    Depuis la page d'accueil  cadre-si  cadre-si
    Depuis le contexte du dossier de coordination  ${dc}
    On clique sur l'onglet  lien_contrainte_dossier_coordination  Contraintes
    Click Element  recuperer_contraintes
    WUX  Click Element  css=input[name="btn_recuperer"]
    Error Message Should Be In Subform  Le dossier de coordination n'a aucune référence cadastrale renseignée.
    # Cas 3 : Besoin de synchronisation
    Depuis le formulaire de modification du dossier de coordination  ${dc}
    Saisir les références cadastrales  ${ref_cad}
    Click On Submit Button
    Valid Message Should Be  Vos modifications ont bien été enregistrées.
    On clique sur l'onglet  lien_contrainte_dossier_coordination  Contraintes
    Page Should Contain  Aucune contrainte appliquée au DC.
    Click Element  recuperer_contraintes
    WUX  Click Element  css=input[name="btn_recuperer"]
    Error Message Should Be In Subform  Les contraintes doivent être synchronisées.
    Click On Back Button In Subform
    # Cas 4 :Ajout
    Click Element  ajouter_contrainte
    WUX  Click Element  css=#fieldset-sousform-lien_contrainte_dossier_coordination-zone-du-pos legend
    WUX  Click Element  css=#fieldset-sousform-lien_contrainte_dossier_coordination-m5 legend
    Sleep  1
    WUX  Select Checkbox  css=input[id^="contrainte_"]
    WUX  Click Element  css=#sformulaire div.formControls input[type="submit"]
    WUX  Element Should Be Visible  css=#sousform-lien_contrainte_dossier_coordination div.message
    La page ne doit pas contenir d'erreur
    WUX  Element Text Should Be  css=#sousform-lien_contrainte_dossier_coordination div.message.ui-state-valid p span.text  La contrainte Contrainte 13055.M50 a été appliquée au DC.
    Click On Back Button In Subform
    Page Should Contain  Surcharge
    # Cas 5 : Récupération
    Depuis la page d'accueil  admin  admin
    Synchroniser les contraintes
    Valid Message Should Contain  4 ajoutées
    Depuis la page d'accueil  cadre-si  cadre-si
    Depuis le contexte du dossier de coordination  ${dc}
    On clique sur l'onglet  lien_contrainte_dossier_coordination  Contraintes
    Click Element  recuperer_contraintes
    WUX  Click Element  css=input[name="btn_recuperer"]
    Valid Message Should Contain In Subform  2 ajoutées
    Click Element  css=input[name="btn_recuperer"]
    Valid Message Should Contain In Subform  0 ajoutée
    Valid Message Should Contain In Subform  2 mises à jour
    Click On Back Button In Subform
    # Cas 6 : Démarquage
    Click On Link  Une troisième contrainte du PLU
    Click On SubForm Portlet Action  lien_contrainte_dossier_coordination  demarquer_recuperee
    Valid Message Should Be In Subform  La contrainte n'est plus marquée comme récupérée.
    Click On Back Button In Subform
    Click On Link  Une troisième contrainte du PLU
    Click On SubForm Portlet Action  lien_contrainte_dossier_coordination  modifier
    Input Text  texte_complete  Une troisième contrainte du PLU modifiée
    Click On Submit Button In Subform
    Valid Message Should Be In Subform  Vos modifications ont bien été enregistrées.
    Click On Back Button In Subform
    # Cas 7 : Modification
    Click On Link  Une quatrième contrainte du PLU
    Click On SubForm Portlet Action  lien_contrainte_dossier_coordination  modifier
    Input Text  texte_complete  Une quatrième contrainte du PLU modifiée
    Click On Submit Button In Subform
    Valid Message Should Be In Subform  Vos modifications ont bien été enregistrées.
    Click On Back Button In Subform
    # Cas 8 : Nouvelle récupération
    # → la contrainte modifiée doit être écrasée
    # → une nouvelle contrainte doit apparaître, une ayant été démarquée
    Click Element  recuperer_contraintes
    WUX  Click Element  css=input[name="btn_recuperer"]
    Valid Message Should Contain In Subform  1 ajoutée
    Valid Message Should Contain In Subform  1 mise à jour
    Click On Back Button In Subform
    Page Should Contain  Une troisième contrainte du PLU modifiée
    Page Should Not Contain  Une quatrième contrainte du PLU modifiée
    Page Should Contain  Une quatrième contrainte du PLU
    # Désactivation du SIG et des contraintes afin de revenir à l'état initial
    Depuis la page d'accueil  admin  admin
    Désactiver le plugin geoaria_tests et l'option sig
    Depuis le contexte du dossier de coordination  ${dc}
    On clique sur l'onglet  lien_contrainte_dossier_coordination  Contraintes
    Click On Link  Une troisième contrainte du PLU modifiée
    Click On SubForm Portlet Action  lien_contrainte_dossier_coordination  supprimer
    Click On Submit Button In Subform
    Click On Link  Une troisième contrainte du PLU
    Click On SubForm Portlet Action  lien_contrainte_dossier_coordination  supprimer
    Click On Submit Button In Subform
    Click On Link  Une quatrième contrainte du PLU
    Click On SubForm Portlet Action  lien_contrainte_dossier_coordination  supprimer
    Click On Submit Button In Subform
    Supprimer la contrainte  Contrainte 13055.M70
    Supprimer la contrainte  Contrainte 13055.M80
    Supprimer la contrainte  Contrainte 13055.M81
    Supprimer la contrainte  Contrainte 13055.M90


Gestion des types d'établissement - champs de fusion
    [Documentation]  Un dossier de coordination possède un type d'établissement
    ...  principal et un ou plusieurs types d'établissement secondaires.
    ...
    ...  Cinq champs de fusion concernant les types d'établissement sont
    ...  disponibles sur le dossier de coordination (
    ...  [dossier_coordination.etablissement_type],
    ...  [dossier_coordination.etablissement_type_description],
    ...  [dossier_coordination.etablissement_type_libelle],
    ...  [dossier_coordination.etablissement_type_secondaire],
    ...  [dossier_coordination.etablissement_type_secondaire_libelle]).

    Depuis la page d'accueil  admin  admin

    ## Constitution du jeu de données
    # lettre type avec les champs de fusion
    &{lt_l_01} =  Create Dictionary
    ...  id=t050etabtypelt1
    Ajouter la lettre-type depuis le menu
    ...  ${lt_l_01.id}
    ...  ${lt_l_01.id}
    ...  <p>lettre type ${lt_l_01.id}</p>
    ...  DC_ET_BEGIN_[dossier_coordination.etablissement_type]_DC_ET_END DC_ET_DESCRIPTION_BEGIN_[dossier_coordination.etablissement_type_description]_DC_ET_DESCRIPTION_END DC_ET_LIBELLE_BEGIN_[dossier_coordination.etablissement_type_libelle]_DC_ET_LIBELLE_END DC_ET_SECONDAIRE_BEGIN_[dossier_coordination.etablissement_type_secondaire]_DC_ET_SECONDAIRE_END DC_ET_SECONDAIRE_LIBELLE_BEGIN_[dossier_coordination.etablissement_type_secondaire_libelle]_DC_ET_SECONDAIRE_LIBELLE_END
    ...  Contexte 'document genere'
    ...  true
    # modèle d'édition rattaché à la lettre type
    &{me_l_01} =  Create Dictionary
    ...  code=t050etabtypeme1
    ...  libelle=Modèle d'édition ${lt_l_01.id}
    ...  courrier_type=Courrier simple
    ...  om_lettretype_id=${lt_l_01.id}
    Ajouter le modèle d'édition  ${me_l_01}
    # dossier de coordination sans valeurs dans les champs *type d'établissement*
    &{dc_l_01} =  Create Dictionary
    ...  dossier_coordination_type=Permis de construire
    ...  description=Dossier de coordination concernant un permis de construire
    ...  a_qualifier=true
    ...  date_butoir=${DATE_FORMAT_DD/MM/YYYY}
    ${dc_l_01.libelle} =  Ajouter le dossier de coordination  ${dc_l_01}
    # contact sur le DC
    &{contact_l_01} =   Create Dictionary
    ...  contact_type=(*) Demandeur Principal
    ...  nom=HKJHKJHXTEST050
    ...  prenom=Jean
    Ajouter le contact depuis le DC
    ...  ${dc_l_01.libelle}
    ...  ${contact_l_01}
    # document généré du modèle d'édition sur le DC
    @{contacts_lies}  Create List  (${contact_l_01.contact_type}) ${contact_l_01.nom} ${contact_l_01.prenom}
    ${params}  Create Dictionary  contacts_lies=@{contacts_lies}
    &{dg_l_01} =  Create Dictionary
    ...  modele_edition=${me_l_01.libelle}
    ...  courrier_type=${me_l_01.courrier_type}
    Ajouter le document généré depuis le contexte du dossier de coordination
    ...  ${dc_l_01.libelle}
    ...  ${params}
    ...  ${dg_l_01.courrier_type}
    ...  ${dg_l_01.modele_edition}
    ${dg_l_01.code_barres} =  Get Text  css=#code_barres

    ## Cas n°1 : pas de valeur => champs de fusion vides
    # Le dossier coordination créé n'a aucune valeur dans les champs *type d'établissement*.
    # Les champs de fusion dans l'édition PDF doivent donc retourner une chaîne vide.
    ${contenu_pdf} =  Create List
    ...  DC_ET_BEGIN__DC_ET_END
    ...  DC_ET_DESCRIPTION_BEGIN__DC_ET_DESCRIPTION_END
    ...  DC_ET_LIBELLE_BEGIN__DC_ET_LIBELLE_END
    ...  DC_ET_SECONDAIRE_BEGIN__DC_ET_SECONDAIRE_END
    ...  DC_ET_SECONDAIRE_LIBELLE_BEGIN__DC_ET_SECONDAIRE_LIBELLE_END
    Depuis le contexte du document généré  ${dg_l_01.code_barres}
    Click On Form Portlet Action  courrier  previsualiser  new_window
    La page du fichier PDF doit contenir les chaînes de caractères
    ...  ${OM_PDF_TITLE}
    ...  ${contenu_pdf}
    ...  page_number=1

    ## Cas n°2 : valeurs renseignées => champs de fusion correctement rendus
    # On renseigne des valeurs dans les champs *type d'établissement* du DC.
    # Les champs de fusion dans l'édition PDF doivent retourner les bonnes valeurs.
    &{dc_l_01_modif01} =  Create Dictionary
    ...  etablissement_type=J
    Modifier le dossier de coordination  ${dc_l_01_modif01}  ${dc_l_01.libelle}
    Depuis le formulaire de modification du dossier de coordination  ${dc_l_01.libelle}
    @{ITEMS} =  Create List  [CTS]  [EF]
    Select From Multiple Chosen List  css=#etablissement_type_secondaire  ${ITEMS}
    Click On Submit Button
    ${contenu_pdf} =  Create List
    ...  DC_ET_BEGIN_[J] Structures d'accueil pour personnes âgées et personnes handicapées_DC_ET_END
    ...  DC_ET_DESCRIPTION_BEGIN_Structures d'accueil pour personnes âgées et personnes handicapées_DC_ET_DESCRIPTION_END
    ...  DC_ET_LIBELLE_BEGIN_J_DC_ET_LIBELLE_END
    ...  DC_ET_SECONDAIRE_BEGIN_[CTS] Chapiteaux, Tentes et Structures toile, [EF] Établissements flottants (eaux intérieures)_DC_ET_SECONDAIRE_END
    ...  DC_ET_SECONDAIRE_LIBELLE_BEGIN_CTS, EF_DC_ET_SECONDAIRE_LIBELLE_END
    Depuis le contexte du document généré  ${dg_l_01.code_barres}
    Click On Form Portlet Action  courrier  previsualiser  new_window
    La page du fichier PDF doit contenir les chaînes de caractères
    ...  ${OM_PDF_TITLE}
    ...  ${contenu_pdf}
    ...  page_number=1

