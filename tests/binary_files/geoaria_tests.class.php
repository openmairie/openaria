<?php
/**
 * Connecteur ET ressource de test du SIG openARIA.
 * Il permet de tester les fonctionnalités de géolocalisation au travers de :
 *  - l'IHM du logiciel ;
 *  - l'implémentation de l'abstracteur SIG.
 * 
 * @package openaria
 * @version SVN : $Id: geoaria_tests.class.php 6467 2016-08-02 16:23:29Z jymadier $
 */


class geoaria_tests extends geoaria_base{


    /**
     * Vérification de l'existence des éléments fournis (numero, voie, dossier ads,
     * liste de parcelles) et tentative de localisation de l'élémént.
     *
     * Le SIG renvoie un booléen vrai si l'élément a pu être localiser, sinon faux.
     * 
     * @param string $obj    Le type d'élément openARIA, "etablissement" ou "dossier".
     * @param string $idx    Identifiant de l'objet.
     * @param array  $params Un tableau contenant un ou plusieurs des éléments suivants :
     *                          array (
     *                              'parcelles' => array (
     *                                  integer array(
     *                                      'quartier' => string,
     *                                      'section' => string,
     *                                      'parcelle' => string
     *                                  ),
     *                                  ...
     *                              )
     *                              'numero' => string,
     *                              'ref_voie' => string,
     *                              'dossier_ads' => string
     *                          ).
     *
     * @throws geoaria_connector_4XX_exception demande de dessin OU déjà gécodé
     * @return integer Précision de la localisation (distance en mètres)
     */
    public function geocoder_objet($obj, $idx, array $params) {
        //
        $flag_path = '../var/tmp/'.$obj.'-'.$idx.'-is-being-geolocalised';
        if (file_exists($flag_path)){
            unlink($flag_path);
            if ($obj === "dossier_coordination") {
                return '10';
            }
            return array(
                "precision" => '10',
            );
        }

        // Géolocalisation d'un établissement
        if ($obj === 'etablissement'
            AND isset($params['parcelles'][0]['quartier']) AND $params['parcelles'][0]['quartier'] === '806'
            AND isset($params['parcelles'][0]['section']) AND $params['parcelles'][0]['section'] === 'AB'
            AND isset($params['parcelles'][0]['parcelle']) AND $params['parcelles'][0]['parcelle'] === '0025'
            AND $params['numero'] === '150') {
            //
            return array(
                "precision" => '0',
            );
        }
        // Valeurs inexistantes pour le SIG, demande de dessin du point
        if ($obj === 'etablissement'
            && $params['numero'] === '350'
            && $params['ref_voie'] === '100') {
            //
            throw new geoaria_connector_4XX_exception(
                'Bad Request',
                404,
                null,
                'geocoder_objet_cant_geocode'
            );
        }
        // L'établissement déjà géolocalisé
        if ($obj === 'etablissement'
            && $params['numero'] === '9797') {
            //
            throw new geoaria_connector_4XX_exception(
                'Bad Request',
                409,
                null,
                'geocoder_objet_already_exist'
            );
        }

        // On géolocalise un dossier de coordination
        if ($obj === 'dossier_coordination'
            AND isset($params['parcelles'][0]['quartier']) AND $params['parcelles'][0]['quartier'] === '806'
            AND isset($params['parcelles'][0]['section']) AND $params['parcelles'][0]['section'] === 'AB'
            AND isset($params['parcelles'][0]['parcelle']) AND $params['parcelles'][0]['parcelle'] === '0025'
            AND isset($params['type']) AND ($params['type'] == 'PC' OR $params['type'] == 'VCS')
            AND $params['dossier_ads'] === 'PC0130551600001P0') {
            //
            return '0';
        }
        // Valeur inexistante pour le SIG, demande de dessin du point
        if ($obj === 'dossier_coordination'
            && $params['dossier_ads'] === 'PC0130551609998P0'
            && ($params['type'] === 'AT' || $params['type'] === 'VCS')) {
            //
            throw new geoaria_connector_4XX_exception(
                'Bad Request',
                404,
                null,
                'geocoder_objet_cant_geocode'
            );
        }
        // Le dossier de coordination est déjà géolocalisé
        if ($obj === 'dossier_coordination'
            && $params['dossier_ads'] === 'PC0130551609999P0') {
            //
            throw new geoaria_connector_4XX_exception(
                'Bad Request',
                409,
                null,
                'geocoder_objet_already_exist'
            );
        }

        // Retour par défaut
        if ($obj === 'dossier_coordination') {
            return '15';
        }
        return array(
                "precision" => '15',
        );
    }


    /**
     * Récupération de toutes les contraintes existantes pour une collectivite.
     * 
     * openARIA appelle le SIG en précisant seulement le code INSEE de la collectivite.
     * Il renvoie une collection de l'intégralité des contraintes existantes.
     *
     * @param string $insee Le code INSEE de la commune dont on souhaite synchroniser les
     * contraintes.
     * @return array Tableau de toutes les contraintes existantes.
     *         array(
     *            "M70" => array(
     *                "id_referentiel" => "M70",
     *                "libelle" => "Contrainte 13055.M70",
     *                "texte" => "Une première contrainte du PLU",
     *                "groupe" => "ZONE DU PLU",
     *                "sousgroupe" => "M7",
     *            )
     *         )
     * Le retour peut être un tableau vide s'il n'y a pas de contraintes.
     */
    public function synchro_contraintes($insee = null) {
        $liste_contraintes_SIG = array();
        switch ($insee) {
            // Marseille
            case '13055':
                $liste_contraintes_SIG['M70'] = array(
                    "id_referentiel" => "M70",
                    "libelle" => "Contrainte 13055.M70",
                    "texte" => "Une première contrainte du PLU",
                    "groupe" => "ZONE DU PLU",
                    "sousgroupe" => "M7",
                );
                $liste_contraintes_SIG['M80'] = array(
                    "id_referentiel" => "M80",
                    "libelle" => "Contrainte 13055.M80",
                    "texte" => "Une seconde contrainte du PLU",
                    "groupe" => "ZONE DU PLU",
                    "sousgroupe" => "M8",
                );
                $liste_contraintes_SIG['M81'] = array(
                    "id_referentiel" => "M81",
                    "libelle" => "Contrainte 13055.M81",
                    "texte" => "Une troisième contrainte du PLU",
                    "groupe" => "ZONE DU PLU",
                    "sousgroupe" => "M8",
                );
                $liste_contraintes_SIG['M90'] = array(
                    "id_referentiel" => "M90",
                    "libelle" => "Contrainte 13055.M90",
                    "texte" => "Une quatrième contrainte du PLU",
                    "groupe" => "ZONE DU PLU",
                    "sousgroupe" => "M9",
                );
                break;
            // Aubagne
            case '13005':
                $liste_contraintes_SIG['A70'] = array(
                    "id_referentiel" => "A70",
                    "libelle" => "Contrainte 13005.A70",
                    "texte" => "Une première contrainte du PLU",
                    "groupe" => "ZONE DU PLU",
                    "sousgroupe" => "A7",
                );
                $liste_contraintes_SIG['A80'] = array(
                    "id_referentiel" => "A80",
                    "libelle" => "Contrainte 13005.A80",
                    "texte" => "Une seconde contrainte du PLU",
                    "groupe" => "ZONE DU PLU",
                    "sousgroupe" => "A8",
                );
                break;
        }
        return $liste_contraintes_SIG;
    }

    /**
     * Récupération des contraintes applicables sur une ou plusieurs parcelles.
     * 
     * openARIA appelle le web service contrainte en lui fournissant la liste des
     * parcelles dont on souhaite récupérer les contraintes.
     * Le SIG renvoie une collection de contraintes qui s'y appliquent.
     * 
     * @param array $parcelles Un tableau contenant une ou plusieurs parcelles.
     *
     * @return array Tableau des contraintes applicables aux parcelles fournies.
     *         array(
     *            "M70" => array(
     *                "id_referentiel" => "M70",
     *                "libelle" => "Contrainte 13055.M70",
     *                "texte" => "Une première contrainte du PLU",
     *                "groupe" => "ZONE DU PLU",
     *                "sousgroupe" => "M7",
     *            )
     *         )
     * Le retour peut être un tableau vide s'il n'y a pas de contrainte.
     */
    public function recup_contraintes(array $parcelles) {
        $liste_contraintes_SIG = array();
        foreach ($parcelles as $parcelle) {
            switch ($parcelle['prefixe'].$parcelle['quartier'].$parcelle['section'].$parcelle['parcelle']) {
                // Parcelle de l'établissement
                case '800A0001':
                    $liste_contraintes_SIG['M70'] = array(
                        "id_referentiel" => "M70",
                        "libelle" => "Contrainte 13055.M70",
                        "texte" => "Une première contrainte du PLU",
                        "groupe" => "ZONE DU PLU",
                        "sousgroupe" => "M7",
                    );
                    $liste_contraintes_SIG['M80'] = array(
                        "id_referentiel" => "M80",
                        "libelle" => "Contrainte 13055.M80",
                        "texte" => "Une seconde contrainte du PLU",
                        "groupe" => "ZONE DU PLU",
                        "sousgroupe" => "M8",
                    );
                    break;
                // Parcelle du dossier de coordination
                case '800A0002':
                    $liste_contraintes_SIG['M81'] = array(
                        "id_referentiel" => "M81",
                        "libelle" => "Contrainte 13055.M81",
                        "texte" => "Une troisième contrainte du PLU",
                        "groupe" => "ZONE DU PLU",
                        "sousgroupe" => "M8",
                    );
                    $liste_contraintes_SIG['M90'] = array(
                        "id_referentiel" => "M90",
                        "libelle" => "Contrainte 13055.M90",
                        "texte" => "Une quatrième contrainte du PLU",
                        "groupe" => "ZONE DU PLU",
                        "sousgroupe" => "M9",
                    );
                    break;
            }
        }
        return $liste_contraintes_SIG;
    }


    /**
     * Redirection vers le SIG dans le contexte de visualisation de l'objet,
     * qui peut être un dossier de coordination, un établissement, ou une parcelle.
     *
     * @param string $obj  Le type d'objet à visualiser :
     *                     - 'parcelle'
     *                     - 'etablissement'
     *                     - 'dossier_coordination'
     * @param array  $data Tableau contenant un ou plusieurs identifiants de l'objet :
     *                     - une ou plusieurs parcelles
     *                         array (
     *                             0 => array (
     *                                     'prefixe' => '201',
     *                                     'quartier' => '806',
     *                                     'section' => 'AB',
     *                                     'parcelle' => '0025',
     *                                  ),
     *                             1 => array(
     *                                     'prefixe' => '208',
     *                                     'quartier' => 806,
     *                                     'section' => ' A',
     *                                     'parcelle' => '0050',
     *                                 ),
     *                             ),
     *                         )
     *                     - un ou plusieurs numéros d'établissements
     *                         array(
     *                             0 => 'T5',
     *                             1 => 'T10',
     *                             2 => 'T1111',
     *                         )
     *                     - un ou plusieurs numéros de dossiers de coordination
     *                         array(
     *                             0 => array(
     *                                 'id' => 'VPS-VISIT-000010',
     *                                 'type' => 'VPS',
     *                                 )
     *                             1 => array(
     *                                 'id' => 'PC-PLAN-000001',
     *                                 'type' => 'PC',
     *                                 )
     *                         )
     * 
     * @return string  L'URL du SIG centré sur le ou les éléments si l'objet a été
     *                 précisé, sinon l'URL vers la vue par défaut du SIG.
     */
    public function redirection_web($obj = null, array $data = null) {
        //
        $url = $this->get_sig_config('url_web');
        //
        if ($obj === null) {
            return $url;
        }
        switch ($obj) {
            case 'dossier_coordination':
                if (isset($data[0]['id'])) {
                    $url .='?context=dc&id=';
                    $i = 0;
                    // On boucle sur les DC
                    foreach ($data as $dc) {
                        // Si pas de type dÃ©fini pour un dossier on lÃšve une erreur
                        if (isset($dc['type']) !== true) {
                            throw new geoaria_argument_exception();
                        }
                        if ($i !== 0) {
                            $url .= ',';
                        }
                        $url .= $dc['id'];
                        $i++;
                    }
                }
            break;
            case 'etablissement':
                if (isset($data[0])) {
                    $url .= '?context=etab&id=';
                    $i = 0;
                    // On boucle sur les DC
                    foreach ($data as $etab) {
                        if ($i !== 0) {
                            $url .= ',';
                        }
                        $url .= $etab;
                        $i++;
                    }
                }
            break;

            case 'parcelle' :
                if ($data[0]['prefixe'] == '201'
                    AND $data[0]['quartier'] == '806'
                    AND $data[0]['section'] == 'AB'
                    AND $data[0]['parcelle'] == '0025') {
                   
                    $url .= '?context=parcelle&id=201806AB0025';
                }
            break;
        }
        return $url;

    }


    /**
     * Redirection vers le SIG dans le contexte de création manuelle d'un élément sur le
     * SIG.
     *
     * @param string $obj  Le type d'objet :
     *                     - 'etablissement'
     *                     - 'dossier_coordination'
     * @param array  $data Tableau de parmètres
     *        array(
     *           'id' => '5478', // identifiant de l'objet
     *           'type' => 'AT', // type de dossier si objet 'dossier_coordination'
     *           'ref_voie' => '2', // identifiant de la voie
     *        )
     * @return string L'url du SIG pour la création manuelle.
     */
    public function redirection_web_emprise($obj, array $data) {
        $url = $this->get_sig_config('url_web');
        //
        if ($obj == 'dossier_coordination' AND isset($data['id']) AND isset($data['type'])) {
            //
            if ($data['type'] === 'AT') {
                //
                $flag_path = '../var/tmp/'.$obj.'-'.$data['id'].'-is-being-geolocalised';
                // Création du flag utilisé par le géocodage
                touch($flag_path);
            }
            // sinon on ne retourne que l'url
            return $url.'?context=dc_emprise&id='.$data['id'].'&couche='.$data['type'].'';
        }
        if ($obj == 'etablissement' AND isset($data['id'])) {
            //
            if (isset($data['ref_voie']) === true
                && $data['ref_voie'] === '100') {
                //
                $flag_path = '../var/tmp/'.$obj.'-'.$data['id'].'-is-being-geolocalised';
                // Création du flag utilisé par le géocodage
                touch($flag_path);
            }
            // sinon on ne retourne que l'url
            return $url.'?context=etab_emprise&id='.$data['id'];
        }


    }


    /**
     * Récupération d'une liste d'établissements proches de l'objet.
     *
     * openARIA appelle le SIG en lui fournissant un ou plusieurs des éléments suivants :
     * array(
     *     "idcentre" => array (
     *                      "id" => string
     *                      "type" => string // 'etablissement' ou 'dossier_ads'
     *                   )
     *     "listeparcelles" => array (
     *                             0 => array (
     *                                     'prefixe' => '201',
     *                                     'quartier' => '806',
     *                                     'section' => 'AB',
     *                                     'parcelle' => '0025',
     *                                  ),
     *                             1 => array(
     *                                     'prefixe' => '208',
     *                                     'quartier' => 806,
     *                                     'section' => ' A',
     *                                     'parcelle' => '0050',
     *                                 ),
     *                             ),
     *                         )
     *     "limite" => string // nombre total maximum de résultats
     *     "distance" => integer // distance maximum acceptée en mètres
     * )
     * 
     * @param array $params Un tableau associatif contenant
     *                      un ou plusieurs des paramètres ci-dessus
     * @return array Tableau d'établissement(s)
     *         array(
     *             "id" => string // code établissement (numéro T)
     *             "distance" => integer // distance en mètres qui sépare
     *             // l'établissement de l'objet qui a été passé en paramètre
     *         )
     */
    public function lister_etablissements_proches(array $data) {
        if (isset($data['listeparcelles'][0])
            AND $data['listeparcelles'][0]['prefixe'] == '201'
            AND $data['listeparcelles'][0]['quartier'] == '806'
            AND $data['listeparcelles'][0]['section'] == 'AB'
            AND $data['listeparcelles'][0]['parcelle'] == '0025') {
            $return = array(
                array(
                    "distance" => 0,
                    "id" => "T2398",
                ),
                array(
                    "distance" => 5,
                    "id" => "T7001",
                ),
                array(
                    "distance" => 7,
                    "id" => "T2399",
                ),
                array(
                    "distance" => 9,
                    "id" => "T2397",
                ),
                array(
                    "distance" => 12,
                    "id" => "T2402",
                ),
                array(
                    "distance" => 19,
                    "id" => "T2444",
                ),
                array(
                    "distance" => 28,
                    "id" => "T7004",
                ),
                array(
                    "distance" => 41,
                    "id" => "T7002",
                ),
                array(
                    "distance" => 50,
                    "id" => "T2400",
                ),
                array(
                    "distance" => 74,
                    "id" => "T7003",
                ),
            );
            if (isset($data['limite']) === false OR
                (isset($data['limite']) === true AND intval($data['limite']) > 10)) {
                array_push(
                    $return,
                    array(
                        "distance" => 100,
                        "id" => "T2401",
                    )
                );
            }
            return $return;
        }


        if (isset($data['idcentre']['id']) === true
            && $data['idcentre']['id'] === 'T666') {
            //
            $etab_proche = array();
            switch ($data['distance']) {
                case '500':
                case '300':
                    $etab_proche[] = array(
                        "distance" => 223,
                        "id" => "T2502",
                    );
                case '200':
                    $etab_proche[] = array(
                        "distance" => 66,
                        "id" => "T2501",
                    );
                case '100':
                    $etab_proche[] = array(
                        "distance" => 12,
                        "id" => "T2444",
                    );

            }
            return $etab_proche;
        }
        return array();
    }


    /**
     * Récupération des informations sur les propriétaires des parcelles.
     *
     * openARIA appelle le SIG en lui fournissant une ou plusieurs parcelles dont on
     * souhaite obtenir les détails sur leur(s) propriétaire(s). Le résultat est un
     * ensemble de propriétaires confondus (non identifiés par leur parcelle).
     *
     * @param array $parcelles Tableau de parcelles.
     * @throws geoaria_connector_4XX_exception  Parcelle inexistante
     * @header(409)  Simulation d'erreur HTTP
     * @return array Un tableau contenant les informations d'un ou plusieurs propriétaires
     * d'une ou plusieurs parcelles.
     *         array(
     *             array(
     *                 "ident1" => "DUPONT",
     *                 "ident2" => "CLAUDE",
     *                 "adr1" => "4EME ETAGE DROITE",
     *                 "adr2" => "42 AV ROGER SALENGRO",
     *                 "adr3" => "QUARTIER EUROMED",
     *                 "cp_ville_pays" => "13003 MARSEILLE",
     *                 "code_pays" => ""
     *             ),
     *         )
     */
    public function lister_proprietaires_parcelles(array $parcelles) {

        //
        $list_plot_owner = array();

        // Cette parcelle retourne un seul propriétaire
        if (isset($parcelles[0]['quartier']) && $parcelles[0]['quartier'] === '806'
            && isset($parcelles[0]['section']) && $parcelles[0]['section'] === 'AB'
            && isset($parcelles[0]['parcelle']) && $parcelles[0]['parcelle'] === '0025') {
            //
            $list_plot_owner[] = array(
                'ident1' => 'DUPONT',
                'ident2' => 'Claude',
                'adr1' => '17 Rue de la république',
                'adr2' => 'Domaine des Saints',
                'adr3' => 'Bât A1',
                'cpville_pays' => '13999 Marseille',
                'code_pays' => '123'
            );
        }

        // Cette parcelle retourne cinq proprétaires
        if (isset($parcelles[0]['quartier']) && $parcelles[0]['quartier'] === '806'
            && isset($parcelles[0]['section']) && $parcelles[0]['section'] === 'AB'
            && isset($parcelles[0]['parcelle']) && $parcelles[0]['parcelle'] === '0026') {
            //
            $list_plot_owner[] = array(
                'ident1' => 'Archambault',
                'ident2' => 'Sibyla',
                'adr1' => '74, rue de Geneve',
                'adr2' => '',
                'adr3' => '',
                'cpville_pays' => '30100 ALÈS',
                'code_pays' => ''
            );
            //
            $list_plot_owner[] = array(
                'ident1' => 'Fongemie',
                'ident2' => 'Oriel',
                'adr1' => '61, quai Saint-Nicolas',
                'adr2' => '',
                'adr3' => '',
                'cpville_pays' => '59200 TOURCOING ',
                'code_pays' => ''
            );
            //
            $list_plot_owner[] = array(
                'ident1' => 'Ruel',
                'ident2' => 'Bruno',
                'adr1' => '54, rue Bonneterie',
                'adr2' => 'Lotissement des Cèdres',
                'adr3' => '',
                'cpville_pays' => '13140 MIRAMAS',
                'code_pays' => ''
            );
            //
            $list_plot_owner[] = array(
                'ident1' => 'Gladu',
                'ident2' => 'Kerman',
                'adr1' => '',
                'adr2' => '',
                'adr3' => '',
                'cpville_pays' => '',
                'code_pays' => ''
            );
            //
            $list_plot_owner[] = array(
                'ident1' => 'Lajoie',
                'ident2' => 'Tanguy',
                'adr1' => '39, rue du Château',
                'adr2' => '',
                'adr3' => '',
                'cpville_pays' => '97480 SAINT-JOSEPH ',
                'code_pays' => ''
            );
            //
            $list_plot_owner[] = array(
                'ident1' => 'Archambault',
                'ident2' => 'Sibyla',
                'adr1' => '74, rue de Geneve',
                'adr2' => '',
                'adr3' => '',
                'cpville_pays' => '30100 ALÈS',
                'code_pays' => ''
            );
            //
            $list_plot_owner[] = array(
                'ident1' => 'Fongemie',
                'ident2' => 'Oriel',
                'adr1' => '61, quai Saint-Nicolas',
                'adr2' => '',
                'adr3' => '',
                'cpville_pays' => '59200 TOURCOING ',
                'code_pays' => ''
            );
            //
            $list_plot_owner[] = array(
                'ident1' => 'Ruel',
                'ident2' => 'Bruno',
                'adr1' => '54, rue Bonneterie',
                'adr2' => 'Lotissement des Cèdres',
                'adr3' => '',
                'cpville_pays' => '13140 MIRAMAS',
                'code_pays' => ''
            );
            //
            $list_plot_owner[] = array(
                'ident1' => 'Gladu',
                'ident2' => 'Kerman',
                'adr1' => '',
                'adr2' => '',
                'adr3' => '',
                'cpville_pays' => '',
                'code_pays' => ''
            );
            //
            $list_plot_owner[] = array(
                'ident1' => 'Lajoie',
                'ident2' => 'Tanguy',
                'adr1' => '39, rue du Château',
                'adr2' => '',
                'adr3' => '',
                'cpville_pays' => '97480 SAINT-JOSEPH ',
                'code_pays' => ''
            );
        }

        // Cette parcelle retourne une exception de georia
        if (isset($parcelles[0]['quartier']) && $parcelles[0]['quartier'] === '777'
            && isset($parcelles[0]['section']) && $parcelles[0]['section'] === 'ZZ'
            && isset($parcelles[0]['parcelle']) && $parcelles[0]['parcelle'] === '7777') {
            //
            throw new geoaria_connector_4XX_exception('Bad Request', 404);
        }

        // Cette parcelle retourne une erreur HTTP non catchée
        if (isset($parcelles[0]['quartier']) && $parcelles[0]['quartier'] === '888'
            && isset($parcelles[0]['section']) && $parcelles[0]['section'] === 'BB'
            && isset($parcelles[0]['parcelle']) && $parcelles[0]['parcelle'] === '8888') {
            //
            header("HTTP/1.1 409 Conflict");
            die();
        }

        //
        return $list_plot_owner;
    }


    public function init_message_sender() {

    }

}
