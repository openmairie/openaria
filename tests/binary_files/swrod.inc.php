<?php
/**
 * Connecteur SWROD spécifique ressources de tests
 *
 * @package openaria
 * @version SVN : $Id$
 */

$swrod = array(
    "type" => "swrodaria_tests",
    "path" => "../tests/binary_files/swrodaria_tests.class.php",
);
