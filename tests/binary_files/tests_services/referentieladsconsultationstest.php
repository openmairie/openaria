<?php
/**
 * Ce fichier permet de déclarer la classe referentieladsconsultationstest, ressource 
 * exposée à travers l'interface REST qui hérite de la classe de base Services. 
 * Cette classe servira pour les tests afin de simuler l'échange avec le 
 * référentiel patrimoine
 *
 * @package openaria
 * @version SVN : $Id: referentieladsconsultationstest.php 1526 2015-12-18 17:07:38Z fmichon $
 */
 
//Inclusion de la classe de base Services
include_once('../services/REST/services.php');

/**
 * Cette classe définie la ressource 'referentieladsconsultationstest' qui permet de 
 * tester l'échange avec le référentiel patrimoine
 */
class referentieladsconsultationstest extends Services {

    public function put($id, $request_data) {
        //
        if (!isset($request_data["date_retour"])
            || $request_data["date_retour"] == ""
            || !isset($request_data["avis"])
            || $request_data["avis"] == ""
            || !in_array($request_data["avis"], array("Favorable", "Favorable avec réserve", "Défavorable", ))) {
            header("HTTP/1.0 400 Bad Request");
            header('Content-Type: application/json');
            echo json_encode(array());
        }
        //
        header("HTTP/1.0 200 OK");
        header('Content-Type: application/json');
        echo json_encode(array());
    }

}
