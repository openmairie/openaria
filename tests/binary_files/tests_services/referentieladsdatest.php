<?php
/**
 * Ce fichier permet de déclarer la classe referentieladsdatest, ressource 
 * exposée à travers l'interface REST qui hérite de la classe de base Services. 
 * Cette classe servira pour les tests afin de simuler l'échange avec le 
 * référentiel patrimoine
 *
 * @package openaria
 * @version SVN : $Id: referentieladsdatest.php 1526 2015-12-18 17:07:38Z fmichon $
 */
 
//Inclusion de la classe de base Services
include_once('../services/REST/services.php');

/**
 * Cette classe définie la ressource 'referentieladsdatest' qui permet de 
 * tester l'échange avec le référentiel patrimoine
 */
class referentieladsdatest extends Services {
    
    public function put($id, $request_data) {
        header("HTTP/1.0 200 OK");
        header('Content-Type: application/json');
        echo json_encode(array());
    }

    /**
     * Cette méthode permet de définir le traitement du GET sur une requête
     * REST. Elle retourne des données.
     *
     * @param string $id L'identifiant de la ressource
     */
    public function get($da) {
        if ($da == "PC0130421500001") {
            header("HTTP/1.0 200 OK");
            header('Content-Type: application/json');
            echo json_encode(array(
                "dossier_autorisation" => "PC0130421500001",
                "dossier_autorisation_type_detaille" => "1",
                "exercice" => "",
                "insee" => "01234",
                "terrain_references_cadastrales" => "",
                "terrain_adresse_voie_numero" => "",
                "terrain_adresse_voie" => "",
                "terrain_adresse_lieu_dit" => "",
                "terrain_adresse_localite" => "",
                "terrain_adresse_code_postal" => "",
                "terrain_adresse_bp" => "",
                "terrain_adresse_cedex" => "",
                "terrain_superficie" => "",
                "arrondissement" => "",
                "depot_initial" => "2012-12-17",
                "erp_numero_batiment" => "",
                "erp_ouvert" => "",
                "erp_date_ouverture" => "",
                "erp_arrete_decision" => "",
                "erp_date_arrete_decision" => "",
                "numero_version" => "0",
                "etat_dossier_autorisation" => "1",
                "date_depot" => "",
                "date_decision" => "",
                "date_validite" => "",
                "date_chantier" => "",
                "date_achevement" => "",
                "avis_decision" => "",
                "etat_dernier_dossier_instruction_accepte" => "",
                "dossier_autorisation_libelle" => "PC 013 042 15 00001",
                "om_collectivite" => "2",
                "cle_acces_citoyen" => "",
            ));
            return;
        } elseif ($da == "AT0130551300001") {
            header("HTTP/1.0 200 OK");
            header('Content-Type: application/json');
            echo json_encode(array(
                "dossier_autorisation" => "AT0130551300001",
                "dossier_autorisation_type_detaille" => "1",
                "exercice" => "",
                "insee" => "01234",
                "terrain_references_cadastrales" => "",
                "terrain_adresse_voie_numero" => "",
                "terrain_adresse_voie" => "",
                "terrain_adresse_lieu_dit" => "",
                "terrain_adresse_localite" => "",
                "terrain_adresse_code_postal" => "",
                "terrain_adresse_bp" => "",
                "terrain_adresse_cedex" => "",
                "terrain_superficie" => "",
                "arrondissement" => "",
                "depot_initial" => "2012-12-17",
                "erp_numero_batiment" => "",
                "erp_ouvert" => "",
                "erp_date_ouverture" => "",
                "erp_arrete_decision" => "",
                "erp_date_arrete_decision" => "",
                "numero_version" => "0",
                "etat_dossier_autorisation" => "1",
                "date_depot" => "",
                "date_decision" => "",
                "date_validite" => "",
                "date_chantier" => "",
                "date_achevement" => "",
                "avis_decision" => "",
                "etat_dernier_dossier_instruction_accepte" => "",
                "dossier_autorisation_libelle" => "AT 013 055 13 00001",
                "om_collectivite" => "2",
                "cle_acces_citoyen" => "",
            ));
            return;
        } elseif ($da == "AT0990991000001") {
            header("HTTP/1.0 200 OK");
            header('Content-Type: application/json');
            echo json_encode(array(
                "dossier_autorisation" => "AT0990991000001",
                "dossier_autorisation_type_detaille" => "1",
                "exercice" => "",
                "insee" => "01234",
                "terrain_references_cadastrales" => "",
                "terrain_adresse_voie_numero" => "",
                "terrain_adresse_voie" => "",
                "terrain_adresse_lieu_dit" => "",
                "terrain_adresse_localite" => "",
                "terrain_adresse_code_postal" => "",
                "terrain_adresse_bp" => "",
                "terrain_adresse_cedex" => "",
                "terrain_superficie" => "",
                "arrondissement" => "",
                "depot_initial" => "2012-12-17",
                "erp_numero_batiment" => "",
                "erp_ouvert" => "",
                "erp_date_ouverture" => "",
                "erp_arrete_decision" => "",
                "erp_date_arrete_decision" => "",
                "numero_version" => "0",
                "etat_dossier_autorisation" => "1",
                "date_depot" => "",
                "date_decision" => "",
                "date_validite" => "",
                "date_chantier" => "",
                "date_achevement" => "",
                "avis_decision" => "",
                "etat_dernier_dossier_instruction_accepte" => "",
                "dossier_autorisation_libelle" => "AT 099 099 10 00001 P0",
                "om_collectivite" => "2",
                "cle_acces_citoyen" => "",
            ));
            return;
        } elseif ($da == "AT0990991000002") {
            header("HTTP/1.0 200 OK");
            header('Content-Type: application/json');
            echo json_encode(array(
                "dossier_autorisation" => "AT0990991000002",
                "dossier_autorisation_type_detaille" => "1",
                "exercice" => "",
                "insee" => "01234",
                "terrain_references_cadastrales" => "",
                "terrain_adresse_voie_numero" => "",
                "terrain_adresse_voie" => "",
                "terrain_adresse_lieu_dit" => "",
                "terrain_adresse_localite" => "",
                "terrain_adresse_code_postal" => "",
                "terrain_adresse_bp" => "",
                "terrain_adresse_cedex" => "",
                "terrain_superficie" => "",
                "arrondissement" => "",
                "depot_initial" => "2012-12-17",
                "erp_numero_batiment" => "",
                "erp_ouvert" => "",
                "erp_date_ouverture" => "",
                "erp_arrete_decision" => "",
                "erp_date_arrete_decision" => "",
                "numero_version" => "0",
                "etat_dossier_autorisation" => "1",
                "date_depot" => "",
                "date_decision" => "",
                "date_validite" => "",
                "date_chantier" => "",
                "date_achevement" => "",
                "avis_decision" => "",
                "etat_dernier_dossier_instruction_accepte" => "",
                "dossier_autorisation_libelle" => "AT 099 099 10 00002 P0",
                "om_collectivite" => "2",
                "cle_acces_citoyen" => "",
            ));
            return;
        } elseif ($da == "PC0990991000001") {
            header("HTTP/1.0 200 OK");
            header('Content-Type: application/json');
            echo json_encode(array(
                "dossier_autorisation" => "PC0990991000001",
                "dossier_autorisation_type_detaille" => "1",
                "exercice" => "",
                "insee" => "01234",
                "terrain_references_cadastrales" => "",
                "terrain_adresse_voie_numero" => "",
                "terrain_adresse_voie" => "",
                "terrain_adresse_lieu_dit" => "",
                "terrain_adresse_localite" => "",
                "terrain_adresse_code_postal" => "",
                "terrain_adresse_bp" => "",
                "terrain_adresse_cedex" => "",
                "terrain_superficie" => "",
                "arrondissement" => "",
                "depot_initial" => "2012-12-17",
                "erp_numero_batiment" => "",
                "erp_ouvert" => "",
                "erp_date_ouverture" => "",
                "erp_arrete_decision" => "",
                "erp_date_arrete_decision" => "",
                "numero_version" => "0",
                "etat_dossier_autorisation" => "1",
                "date_depot" => "",
                "date_decision" => "",
                "date_validite" => "",
                "date_chantier" => "",
                "date_achevement" => "",
                "avis_decision" => "",
                "etat_dernier_dossier_instruction_accepte" => "",
                "dossier_autorisation_libelle" => "PC 099 099 10 00001",
                "om_collectivite" => "2",
                "cle_acces_citoyen" => "",
            ));
            return;
        } elseif ($da == "PC0990991000002") {
            header("HTTP/1.0 200 OK");
            header('Content-Type: application/json');
            echo json_encode(array(
                "dossier_autorisation" => "PC0990991000002",
                "dossier_autorisation_type_detaille" => "1",
                "exercice" => "",
                "insee" => "01234",
                "terrain_references_cadastrales" => "",
                "terrain_adresse_voie_numero" => "",
                "terrain_adresse_voie" => "",
                "terrain_adresse_lieu_dit" => "",
                "terrain_adresse_localite" => "",
                "terrain_adresse_code_postal" => "",
                "terrain_adresse_bp" => "",
                "terrain_adresse_cedex" => "",
                "terrain_superficie" => "",
                "arrondissement" => "",
                "depot_initial" => "2012-12-17",
                "erp_numero_batiment" => "",
                "erp_ouvert" => "",
                "erp_date_ouverture" => "",
                "erp_arrete_decision" => "",
                "erp_date_arrete_decision" => "",
                "numero_version" => "0",
                "etat_dossier_autorisation" => "1",
                "date_depot" => "",
                "date_decision" => "",
                "date_validite" => "",
                "date_chantier" => "",
                "date_achevement" => "",
                "avis_decision" => "",
                "etat_dernier_dossier_instruction_accepte" => "",
                "dossier_autorisation_libelle" => "PC 099 099 10 00002",
                "om_collectivite" => "2",
                "cle_acces_citoyen" => "",
            ));
            return;
        }
        // Aucun de nos da de test
        header("HTTP/1.0 400 Bad Request");
        header('Content-Type: application/json');
        echo json_encode(array(
            "http_code" => 400,
            "http_code_message" => "400 Bad Request",
            "message" => "Le dossier d'autorisation ".$da." n'etait pas trouve",
        ));
        return;
    }
}
