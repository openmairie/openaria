<?php
/**
 * Ce fichier est le point d'entrée pour les requêtes REST dans l'application.
 * Voir le fichier README.txt qui se trouve dans ce dossier pour vérifier les
 * pré-requis concernant la librairie restler.
 *
 * @package openaria
 * @version SVN : $Id: rest_entry.php 1150 2015-03-12 17:46:38Z fmichon $
 */

// Inclusion de la librairie restler
require_once('../services/php/restler/restler/restler.php');

// Instanciation de restler
$r = new Restler();

//Déclaration de la ressource de test 'referentielpatrimoinetest'
require_once('./referentielpatrimoinetest.php');
$r->addAPIClass('referentielpatrimoinetest');

//Déclaration de la ressource de test 'referentieladsdatest'
require_once('./referentieladsdatest.php');
$r->addAPIClass('referentieladsdatest');

//Déclaration de la ressource de test 'referentieladsditest'
require_once('./referentieladsditest.php');
$r->addAPIClass('referentieladsditest');

//Déclaration de la ressource de test 'referentieladsmessagestest'
require_once('./referentieladsmessagestest.php');
$r->addAPIClass('referentieladsmessagestest');

//Déclaration de la ressource de test 'referentieladsconsultationstest'
require_once('./referentieladsconsultationstest.php');
$r->addAPIClass('referentieladsconsultationstest');

//
$r->setSupportedFormats('JsonFormat', 'XmlFormat');

// Exécution de l'API
$r->handle();
