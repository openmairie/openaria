<?php
/**
 * Fichier de configuration de test du connecteur geoaria_vdm
 * Ce fichier permet de paramétrer la connexion au SIG
 *
 * @package openaria
 * @version SVN : $Id: sig.inc.php 6450 2016-08-01 16:34:43Z jymadier $
 */

/**
 *
 */
$conf["sig-default"] = array (
    "1" => array( // Identifiant de la collectivité
        'connector' => 'tests', // Le nom du connecteur
        'path' => '../tests/binary_files/', // Le chemin relatif vers le connecteur
        'login' => '', // L'identifiant de connexion au SIG
        'password' => '', // Le mot de passe de connexion au SIG
        'url_ws' => '', // L'adresse URL du web service SIG retournant des données
        'url_web' => 'http://localhost/openaria/app/geoaria_redirection_test.html', // Adresse URL du SIG utilisé lors de la redirection web
        'sig_couche_etablissements' => 'etab_layer', // La couche sur laquelle se trouvent les objets établissement
        'sig_couches_dc' => array( // Correspondance entre un type de DC et la couche sur laquelle il est situé
            'PC' => 'pc_layer',
            'AT' => 'at_layer',
            'VCS' => 'vcs_layer',
        ),
        'sig_referentiel' => '', // Identificateur de référence spatiale
    ),
);
