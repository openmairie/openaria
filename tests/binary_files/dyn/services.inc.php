<?php
/**
 * Contient les liens vers des services REST externes
 *
 * @package openaria
 * @version SVN : $Id: services.inc.php 1403 2015-06-17 13:44:03Z fmichon $
 */

$services_login = "admin";
$services_password = "admin";

//URL du web-service vers le référentiel patrimoine
$URL_REFERENTIEL_PATRIMOINE_EQUIPEMENT = "http://localhost/openaria/tests_services/rest_entry.php/referentielpatrimoinetest/";


$ADS_URL_MESSAGES = "http://localhost/openaria/tests_services/rest_entry.php/referentieladsmessagestest";


$ADS_URL_CONSULTATIONS = "http://localhost/openaria/tests_services/rest_entry.php/referentieladsconsultationstest";


$ADS_URL_DOSSIER_AUTORISATIONS = "http://localhost/openaria/tests_services/rest_entry.php/referentieladsdatest";


$ADS_URL_DOSSIER_INSTRUCTIONS = "http://localhost/openaria/tests_services/rest_entry.php/referentieladsditest";


$ADS_URL_VISUALISATION_DA = "http://demo.openmairie.org/a/openads/latest/app/web_entry.php?obj=dossier_autorisation&value=<ID_DA>";
