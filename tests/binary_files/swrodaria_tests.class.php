<?php
/**
 * Connecteur SWROD spécifique ressources de tests
 *
 * @package openaria
 * @version SVN : $Id$
 */

class swrodaria_tests extends swrod_base {

    /**
     *
     */
    function get_list($dossier) {
        //
        if ($dossier == "AT0130551200001P0") {
            //
            return array(
                array(
                    "date_creation" => "2016-12-01",
                    "categorie" => "Arrêté",
                    "nom_fichier" => "20161201ARR-01.pdf",
                    "type_document" => "arrêté de conformité",
                    "uid" => "5",
                    "dossier" => "AT0130551200001P0",
                ),
                array(
                    "date_creation" => "2016-12-01",
                    "categorie" => "Arrêté",
                    "nom_fichier" => "20161201ARR-02.pdf",
                    "type_document" => "arrêté de conformité",
                    "uid" => "5",
                    "dossier" => "AT0130551200001P0",
                ),
                array(
                    "date_creation" => "2013-12-01",
                    "categorie" => "Arrêté",
                    "nom_fichier" => "20131201ARR.pdf",
                    "type_document" => "arrêté de conformité",
                    "uid" => "5",
                    "dossier" => "AT0130551200001P0",
                ),
            );
        } elseif ($dossier == "PC0130551200001"
                  || $dossier == "PC0130551200001P0"
                  || $dossier == "PC0130551200001M01") {
            //
            $di_p0 = array(
                array(
                    "date_creation" => "2014-03-12",
                    "categorie" => "Arrêté",
                    "nom_fichier" => "20140312ARR.pdf",
                    "type_document" => "arrêté de conformité",
                    "uid" => "5",
                    "dossier" => "PC0130551200001P0",
                ),
            );
            $di_m1 = array(
                array(
                    "date_creation" => "2014-09-12",
                    "categorie" => "Arrêté",
                    "nom_fichier" => "20140912ARR-01.pdf",
                    "type_document" => "arrêté de conformité",
                    "uid" => "5",
                    "dossier" => "PC0130551200001M01",
                ),
                array(
                    "date_creation" => "2014-09-12",
                    "categorie" => "Arrêté",
                    "nom_fichier" => "20140912ARR-02.pdf",
                    "type_document" => "arrêté de conformité",
                    "uid" => "5",
                    "dossier" => "PC0130551200001M01",
                ),
            );
            //
            if ($dossier == "PC0130551200001") {
                return array_merge($di_p0, $di_m1);
            } elseif ($dossier == "PC0130551200001P0") {
                return $di_p0;
            } elseif ($dossier == "PC0130551200001M01") {
                return $di_m1;
            }
        }
        //
        return array();
    }

    /**
     *
     */
    function get_file($id) {
        //
        $file_content = file_get_contents("../tests/binary_files/numerisation1.pdf");
        //
        $metadata = array(
            "filename" => "truc.pdf",
            "mimetype" => "application/pdf",
        );
        //
        return array(
            'file_content' => $file_content,
            'metadata' => $metadata,
        );
    }

}
