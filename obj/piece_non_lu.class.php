<?php
/**
 * Ce script définit la classe 'piece_non_lu'.
 *
 * @package openaria
 * @version SVN : $Id$
 */

require_once "../obj/piece.class.php";

/**
 * Définition de la classe 'piece_non_lu' (om_dbform).
 *
 * Surcharge de la classe 'piece'. Cette classe permet d'afficher seulement les
 * pièces qui sont non lu, c'est-à-dire dont le champ lu est à false.
 */
class piece_non_lu extends piece {

    /**
     *
     */
    protected $_absolute_class_name = "piece_non_lu";

}

