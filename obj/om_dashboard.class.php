<?php
/**
 * Ce script définit la classe 'om_dashboard'.
 *
 * @package openaria
 * @version SVN : $Id$
 */

require_once PATH_OPENMAIRIE."obj/om_dashboard.class.php";

/**
 * Définition de la classe 'om_dashboard' (om_dbform).
 */
class om_dashboard extends om_dashboard_core {

    /**
     * Permet de modifier le fil d'Ariane depuis l'objet pour un formulaire
     * @param string    $ent    Fil d'Ariane récupéréré
     * @return                  Fil d'Ariane
     */
    function getFormTitle($ent) {
        //
        if ($this->getParameter("maj") == 4) {
            return __("administration_parametrage")." -> ".__("tableaux de bord")." -> ".__("composition");
        }
        //
        return $ent;
    }
}
