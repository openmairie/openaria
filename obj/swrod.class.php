<?php
/**
 * Ce script définit les classes 'swrod' et 'swrod_base'.
 *
 * La classe 'swrod' est une classe d'abstraction, spécifique à openARIA,
 * permettant de gérer les requêtes vers divers webservices et ainsi
 * proposer aux utilisateurs une interface de consultation et de téléchargement
 * des documents du guichet unique en lecture seule.
 * Son objectif est d'instancier les classes spécifiques aux différents
 * environnements aussi appelées connecteurs correspondant au paramétrage de la
 * collectivité.
 *
 * Ces connecteurs héritent de la classe 'swrod_base' qui leur sert de modèle.
 *
 * @package openaria
 * @version SVN : $Id$
 */

/**
 * Définition de la classe 'swrod'.
 *
 * Abstracteur SWROD spécifique à openARIA
 */
class swrod {

    /**
     *
     */
    var $swrod = null;

    /**
     *
     */
    var $type = null;

    /**
     *
     */
    function __construct($conf = array()) {
        //
        if (!isset($conf['type']) || $conf['type'] == "") {
            return false;
        }
        $this->type = $conf['type'];
        //
        if (!isset($conf['path'])
            || $conf['path'] == ""
            || !file_exists($conf['path'])) {
            //
            return false;
        }
        require_once $conf["path"];
        //
        if (class_exists($this->type) !== true) {
            //
            return false;
        }
        $this->swrod = new $this->type($conf);
    }

    /**
     *
     */
    function get_list($dossier) {
        return $this->swrod->get_list($dossier);
    }

    /**
     *
     */
    function get_file($id) {
        return $this->swrod->get_file($id);
    }

}

/**
 * Définition de la classe 'swrod_base'.
 *
 * Classe parente de tous les connecteurs SWROD
 */
class swrod_base {

    /**
     *
     */
    var $conf = null;

    /**
     *
     */
    function __construct($conf) {
        //
        $this->conf = $conf;
    }

    /**
     *
     */
    function get_list($dossier) {
        return false;
    }

    /**
     *
     */
    function get_file($id) {
        return false;
    }

}

