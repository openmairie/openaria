<?php
/**
 * Ce script définit la classe 'pilotage'.
 *
 * @package openaria
 * @version SVN : $Id$
 */

/**
 * Définition de la classe 'pilotage'.
 *
 * Cette classe est un utilitaire gérant les statistiques
 * et les widgets disponibles sur les tableaux de bord.
 * Elle permet de faire des requêtes, d'afficher des graphiques,
 * des tableaux, ...
 */
class pilotage {

    /**
     *
     */
    var $f = null;

    /**
     *
     */
    var $filter_mode = null;

    /**
     *
     */
    var $filter_by_service = -1;

    /**
     *
     */
    var $filter_by_acteur = -1;

    /**
     *
     */
    function __construct() {
        //
        if (isset($GLOBALS["f"])) {
            $this->f = $GLOBALS["f"];
        }
        //
        $this->init_stats();
        $this->init_services();
    }

    /**
     *
     */
    function get_filter_mode() {
        return $this->filter_mode;
    }
    /**
     *
     */
    function set_filter_mode($filter_mode = null) {
        if ($filter_mode != "by_service"
            && $filter_mode != "by_acteur"
            && $filter_mode != null) {
            //
            $filter_mode = null;
        }
        //
        $this->filter_mode = $filter_mode;
    }

    /**
     *
     */
    function get_service() {
        return $this->filter_by_service;
    }

    /**
     *
     */
    function set_service($service) {
        if (intval($service) > 0) {
            $this->filter_by_service = intval($service);
        } else {
            $this->filter_by_service = -1;
        }
    }

    /**
     *
     */
    function get_acteur() {
        return $this->filter_by_acteur;
    }

    /**
     *
     */
    function set_acteur($acteur) {
        if (intval($acteur) > 0) {
            $this->filter_by_acteur = intval($acteur);
        } else {
            $this->filter_by_acteur = -1;
        }
    }

    /**
     *
     */
    function get_service_instance($service) {
        return $this->f->get_inst__om_dbform(array(
            "obj" => "service",
            "idx" => $service,
        ));
    }

    /**
     *
     */
    function get_acteur_instance($acteur) {
        return $this->f->get_inst__om_dbform(array(
            "obj" => "acteur",
            "idx" => $acteur,
        ));
    }

    /**
     *
     */
    function get_stat($identifier) {
        if (!isset($this->stats[$identifier])) {
            return null;
        }
        //
        return $this->stats[$identifier];
    }

    /**
     *
     */
    function init_stats() {
        //
        $this->stats["nb-visites-programmees"] = array(
            "title" => __("Visites programmées"),
            "description" => __("Nombre total de visites programmées (c'est-à-dire les visites non annulées dont la version de création de la programmation a été validée) dans l'année courante"),
            "method"=> "nombre_de_visites_programmees",
            "type" => "numberby",
            "category" => __("statistiques operationnelles"),
            "class" => "bg-info",
        );
        //
        $this->stats["nb-visites-realisees"] = array(
            "title" => __("Visites réalisées"),
            "description" => __("Nombre total de visites réalisées depuis le début de l'année courante"),
            "method"=> "nombre_de_visites_realisees",
            "type" => "numberby",
            "category" => __("statistiques operationnelles"),
            "class" => "bg-valid",
        );
        //
        $this->stats["nb-visites-en-retard"] = array(
            "title" => __("Visites en retard"),
            "description" => __("Nombre d'établissements sous périodique dont la date prévisionnelle de prochaine visite périodique est dans le passé"),
            "method"=> "nombre_de_visites_en_retard",
            "type" => "numberby",
            "category" => __("statistiques operationnelles"),
            "class" => "bg-danger",
        );
        //
        $this->stats["nb-avis-favorables"] = array(
            "title" => __("Avis favorables"),
            "description" => __("Nombre d'avis favorables émis depuis le début de l'année courante"),
            "method"=> "nombre_d_avis_favorables",
            "type" => "numberby",
            "category" => __("statistiques operationnelles"),
            "class" => "bg-valid",
            "colors" => array("#4DBD74", "#4DBD74"),
        );
        //
        $this->stats["nb-avis-defavorables"] = array(
            "title" => __("Avis défavorables"),
            "description" => __("Nombre d'avis défavorables émis depuis le début de l'année courante"),
            "method"=> "nombre_d_avis_defavorables",
            "type" => "numberby",
            "category" => __("statistiques operationnelles"),
            "class" => "bg-danger",
            "colors" => array("#E94B3B", "#E94B3B"),
        );
        //
        $this->stats["nb-dc-en-ap"] = array(
            "title" => __("AP en cours"),
            "description" => __("Nombre de DC avec une AP en cours"),
            "method"=> "nombre_de_dc_avec_ap_en_cours",
            "type" => "numberby",
            "category" => __("statistiques operationnelles"),
            "class" => "bg-warning",
        );
        //
        $this->stats["nb-di-plan-etudies"] = array(
            "title" => __("Plans étudiés"),
            "description" => __("Nombre total de dossiers plan depuis le début de l'année courante"),
            "method"=> "nombre_de_di_plan_etudies",
            "type" => "numberby",
            "category" => __("statistiques operationnelles"),
            "class" => "bg-valid",
        );
        //
        $this->stats["nb-di-encours"] = array(
            "title" => __("Dossiers en cours"),
            "description" => __("Nombre total de DI en cours"),
            "method"=> "nombre_de_di_encours",
            "type" => "numberby",
            "category" => __("statistiques operationnelles"),
            "class" => "bg-info",
        );
        //
        $this->stats["delai-moyen-instruction-dossier"] = array(
            "title" => __("Délai moyen d'instruction"),
            "description" => __("Temps moyen entre la date de demande d'un dossier et sa cloture sur les six derniers mois."),
            "method"=> "delai_moyen_instruction",
            "type" => "numberby",
            "category" => __("statistiques operationnelles"),
            "class" => "bg-info",
        );
        //
        $this->stats["delai-moyen-suivi-ap"] = array(
            "title" => __("Delai moyen de suivi d'une autorite de police"),
            "description" => __("Temps moyen entre la date de cloture d'un dossier et sa date de premier passage en commission sur les six derniers mois."),
            "method"=> "delai_moyen_suivi_ap",
            "type" => "numberby",
            "category" => __("statistiques informatives"),
            "class" => "bg-info",
        );
        //
        $this->stats["delai-moyen-notification"] = array(
            "title" => __("Delai moyen d'une notification par courrier"),
            "description" => __("Temps moyen entre la date de première présentation du courrier et son envoi."),
            "method"=> "delai_moyen_notification",
            "type" => "numberby",
            "category" => __("statistiques informatives"),
            "class" => "bg-info",
        );

    }

    /**
     *
     */
    function init_services() {
        //
        $sql = "
        SELECT * FROM ".DB_PREFIXE."service
        ";
        //
        $this->services = $this->get_db_query_all($sql);
    }

    /**
     * Cet méthode permet de formater, la chaîne de caractères reçue du 
     * paramétrage du widget en un tableau de valeurs dont les clés 
     * correspondent aux clés passées en paramètre.
     *
     * @param string $content
     * @param array $params
     *
     * @return array
     */
    function get_arguments($content = null, $params = array()) {
        //
        $arguments = array();
        // On explose les paramètres reçus avec un élément par ligne
        $params_tmp1 = explode("\n", $content);
        // On boucle sur chaque ligne de paramètre
        foreach ($params_tmp1 as $key => $value) {
            // On explose le paramètre de sa valeur avec le séparateur '='
            $params_tmp2[] = explode("=", $value);
        }
        // On boucle sur chaque paramètre reçu pour vérifier si la valeur reçue
        // est acceptable ou non
        foreach ($params_tmp2 as $key => $value) {
            //
            if (!isset($value[0]) || !isset($value[1])) {
                continue;
            }
            //
            if (in_array(trim($value[0]), $params)) {
                $arguments[trim($value[0])] = trim($value[1]);
            }
        }
        //
        return $arguments;
    }

    /**
     * Retourne le nombre de visites correspondant au mode passé en paramètre.
     *
     * La requête compte les visites de l'année courante considérées comme
     * planifiées, ce qui exclue les visites annulées ou pas encore validées
     * dans leur semaine de programmation.
     *
     * @param string $mode Le mode est soit "programmees" ou "realisees"
     *
     * @return intval Nombre de visites
     */
    function nombre_de_visites($mode) {
        //
        $sql = "
        SELECT 
            count(visite.visite)
        FROM
            ".DB_PREFIXE."visite
                LEFT JOIN ".DB_PREFIXE."programmation
                    ON visite.programmation=programmation.programmation
                LEFT JOIN ".DB_PREFIXE."programmation_etat
                    ON programmation.programmation_etat=programmation_etat.programmation_etat
                LEFT JOIN ".DB_PREFIXE."visite_etat
                    ON visite.visite_etat=visite_etat.visite_etat
                LEFT JOIN ".DB_PREFIXE."acteur
                    ON visite.acteur=acteur.acteur
        WHERE
            LOWER(visite_etat.code) <> 'ann'
            AND NOT (
                visite.programmation_version_creation IS NOT NULL
                AND visite.programmation_version_creation=programmation.version
                AND LOWER(programmation_etat.code) <> 'val'
            )
        ";
        //
        //
        if ($mode == "programmees") {
            $sql .= "
            AND EXTRACT(YEAR FROM visite.date_visite)=EXTRACT(YEAR FROM NOW())
            ";
        } elseif ($mode == "realisees") {
            $sql .= "
            AND EXTRACT(YEAR FROM visite.date_visite)=EXTRACT(YEAR FROM NOW())
            AND visite.date_visite < CURRENT_DATE
            ";
        }
        //
        if ($this->get_filter_mode() == "by_service") {
            $sql .= " AND acteur.service=".$this->get_service()." ";
        }
         if ($this->get_filter_mode() == "by_acteur") {
            $sql .= " AND acteur.acteur=".$this->get_acteur()." ";
        }
        //
        $res = $this->get_db_query_one($sql);
        //
        return intval($res);
    }

    /**
     *
     */
    function nombre_de_visites_programmees() {
        //
        return $this->nombre_de_visites("programmees");
    }

    /**
     *
     */
    function nombre_de_visites_realisees() {
        //
        return $this->nombre_de_visites("realisees");
    }

    /**
     *
     */
    function nombre_d_etablissements($mode) {

        //
        $sql = "
        SELECT
        ";
        if ($mode == "detail" || $mode = "retard") {
            $sql .= "
            (etablissement_type.libelle) as abscisse,
            (etablissement_categorie.libelle) as ordonnee,
            count(etablissement.etablissement)
            ";
        } else {
            $sql .= "
            count(etablissement.etablissement)
            ";
        }
        //
        $sql .= "
        FROM
            ".DB_PREFIXE."etablissement
        LEFT JOIN ".DB_PREFIXE."etablissement_type
            ON etablissement.etablissement_type=etablissement_type.etablissement_type
        LEFT JOIN ".DB_PREFIXE."etablissement_categorie
            ON etablissement.etablissement_categorie=etablissement_categorie.etablissement_categorie
        ";
        //
        if ($mode == "retard") {
            $sql .= "
            AND etablissement.si_prochaine_visite_periodique_date_previsionnelle < CURRENT_DATE
            ";
        }
        //
        if ($this->get_filter_mode() == "by_service") {
            //$sql .= " AND acteur.service=".$this->get_service()." ";
        }
         if ($this->get_filter_mode() == "by_acteur") {
            // $sql .= " AND acteur.acteur=".$this->get_acteur()." ";
        }

        //
        if ($mode == "detail" || $mode = "retard") {
            $sql .= " 
            GROUP BY 
                etablissement_type.libelle, 
                etablissement_categorie.libelle
            ORDER BY 
                etablissement_type.libelle, 
                etablissement_categorie.libelle
            ";
        }

        /**
         *
         */
        if ($mode == "detail" || $mode = "retard") {
            //
            $results = $this->get_db_query_all($sql);
            //
            $sql = "select libelle as ordonnee from ".DB_PREFIXE."etablissement_categorie order by libelle";

            $ordonnee = $this->get_db_query_all($sql);
            //
            $sql = "select libelle as abscisse from ".DB_PREFIXE."etablissement_type order by libelle";

            $abscisse = $this->get_db_query_all($sql);
            //
            $table = $this->prepare_table($abscisse, $ordonnee, $results);
            //
            return $table;
        } else {
            //
            $res = $this->get_db_query_one($sql);
            //
            return $res;
        }
    }
    /**
     *
     */
    function nombre_visites_realisees_edition($annee, $params) {

        //
        $champ_abs = $params["table_abs"].".".$params["champ_abs"];
        $champ_ord = $params["table_ord"].".".$params["champ_ord"];

        $sql = "
        SELECT
        ";
        $sql .= $champ_abs." as abscisse, ";
        $sql .= $champ_ord." as ordonnee, ";
        $sql .= "
            count(visite.visite)
        ";

        //
        $sql .= "FROM

                ".DB_PREFIXE."visite
            LEFT JOIN ".DB_PREFIXE."programmation
                ON visite.programmation=programmation.programmation
            LEFT JOIN ".DB_PREFIXE."programmation_etat
                ON programmation.programmation_etat=programmation_etat.programmation_etat
            LEFT JOIN ".DB_PREFIXE."visite_etat
                ON visite.visite_etat=visite_etat.visite_etat
            LEFT JOIN ".DB_PREFIXE."acteur
                ON visite.acteur=acteur.acteur
            LEFT JOIN ".DB_PREFIXE."dossier_instruction
                ON visite.dossier_instruction=dossier_instruction.dossier_instruction
            LEFT JOIN ".DB_PREFIXE."dossier_coordination
                ON dossier_instruction.dossier_coordination=dossier_coordination.dossier_coordination
            LEFT JOIN ".DB_PREFIXE."dossier_coordination_type
                ON dossier_coordination.dossier_coordination_type=dossier_coordination_type.dossier_coordination_type
            LEFT JOIN ".DB_PREFIXE."etablissement
                ON dossier_coordination.etablissement=etablissement.etablissement
            LEFT JOIN ".DB_PREFIXE."etablissement_type
                ON etablissement.etablissement_type=etablissement_type.etablissement_type
            LEFT JOIN ".DB_PREFIXE."etablissement_categorie
                ON etablissement.etablissement_categorie=etablissement_categorie.etablissement_categorie
        
        ";
        $sql .= "WHERE
            LOWER(visite_etat.code) <> 'ann'
            AND NOT (
                visite.programmation_version_creation IS NOT NULL
                AND visite.programmation_version_creation=programmation.version
                AND LOWER(programmation_etat.code) <> 'val'
            )
            AND EXTRACT(YEAR FROM visite.date_visite)=".$annee."
            AND visite.date_visite < CURRENT_DATE
            ";

        //
        if ($this->get_filter_mode() == "by_service") {
            $sql .= " AND acteur.service=".$this->get_service()." ";
        }
         if ($this->get_filter_mode() == "by_acteur") {
            $sql .= " AND acteur.acteur=".$this->get_acteur()." ";
        }

        $sql .= " 
        GROUP BY 
            ".$champ_abs.",
            ".$champ_ord."
        ORDER BY 
            ".$champ_abs.",
            ".$champ_ord."
        ";

        /**
         *
         */
        //
        $results = $this->get_db_query_all($sql);
        //
        if(isset($params["display_ord"]) and $params["display_ord"] === false) {
            $ordonnee = false;
        } else {
            $sql = "select ".$champ_ord." as ordonnee from ".DB_PREFIXE.$params["table_ord"]." order by libelle";
            $ordonnee = $this->get_db_query_all($sql);
        }
        //
        $sql = "select ".$champ_abs." as abscisse from ".DB_PREFIXE.$params["table_abs"]." order by libelle";
        $abscisse = $this->get_db_query_all($sql);

        $table = $this->prepare_table($abscisse, $ordonnee, $results);
        //
        return $table;

    }

    /**
     *
     */
    function nombre_plan_categorie_type($annee) {

        //
        $sql = "
        SELECT
        ";
        $sql .= "
        etablissement_type.libelle as abscisse,
        etablissement_categorie.libelle as ordonnee,
        count(dossier_instruction.dossier_instruction)
        ";

        //
        $sql .= "FROM

                ".DB_PREFIXE."dossier_instruction
            LEFT JOIN ".DB_PREFIXE."acteur
                ON dossier_instruction.technicien=acteur.acteur
            LEFT JOIN ".DB_PREFIXE."dossier_coordination
                ON dossier_instruction.dossier_coordination=dossier_coordination.dossier_coordination
            LEFT JOIN ".DB_PREFIXE."etablissement
                ON dossier_coordination.etablissement=etablissement.etablissement
            LEFT JOIN ".DB_PREFIXE."etablissement_type
                ON etablissement.etablissement_type=etablissement_type.etablissement_type
            LEFT JOIN ".DB_PREFIXE."etablissement_categorie
                ON etablissement.etablissement_categorie=etablissement_categorie.etablissement_categorie
            LEFT JOIN ".DB_PREFIXE."dossier_coordination_type
                ON dossier_coordination.dossier_coordination_type=dossier_coordination_type.dossier_coordination_type
            LEFT JOIN ".DB_PREFIXE."dossier_type
                ON dossier_coordination_type.dossier_type=dossier_type.dossier_type
            LEFT JOIN ".DB_PREFIXE."dossier_instruction_reunion
                ON dossier_instruction_reunion.dossier_instruction=dossier_instruction.dossier_instruction
            LEFT JOIN ".DB_PREFIXE."reunion
                ON dossier_instruction_reunion.reunion=reunion.reunion
        ";
        $sql .= "WHERE
            EXTRACT(YEAR FROM reunion.date_reunion)=".$annee."
            AND reunion.date_reunion < NOW()
            AND dossier_type.code='PLAN'
            ";

        //
        if ($this->get_filter_mode() == "by_service") {
            $sql .= " AND dossier_instruction.service=".$this->get_service()." ";
        }
         if ($this->get_filter_mode() == "by_acteur") {
            $sql .= " AND acteur.acteur=".$this->get_acteur()." ";
        }

        $sql .= " 
        GROUP BY 
            etablissement_type.libelle, 
            etablissement_categorie.libelle
        ORDER BY 
            etablissement_type.libelle, 
            etablissement_categorie.libelle
        ";

        /**
         *
         */
        //
        $results = $this->get_db_query_all($sql);
        //
        $sql = "select libelle as ordonnee from ".DB_PREFIXE."etablissement_categorie order by libelle";
        $ordonnee = $this->get_db_query_all($sql);
        //
        $sql = "select libelle as abscisse from ".DB_PREFIXE."etablissement_type order by libelle";
        $abscisse = $this->get_db_query_all($sql);
        $table = $this->prepare_table($abscisse, $ordonnee, $results);
        //
        return $table;

    }

    /**
     *
     */
    function nombre_plan_type_avis($annee) {

        //
        $sql = "
        SELECT
        ";
        $sql .= "
        etablissement_type.libelle as abscisse,
        reunion_type.code as ordonnee, 
        count(dossier_instruction_reunion.dossier_instruction_reunion)
        ";

        //
        $sql .= "FROM

                ".DB_PREFIXE."dossier_instruction
            LEFT JOIN ".DB_PREFIXE."acteur
                ON dossier_instruction.technicien=acteur.acteur
            LEFT JOIN ".DB_PREFIXE."dossier_coordination
                ON dossier_instruction.dossier_coordination=dossier_coordination.dossier_coordination
            LEFT JOIN ".DB_PREFIXE."etablissement
                ON dossier_coordination.etablissement=etablissement.etablissement
            LEFT JOIN ".DB_PREFIXE."etablissement_type
                ON etablissement.etablissement_type=etablissement_type.etablissement_type
            LEFT JOIN ".DB_PREFIXE."etablissement_categorie
                ON etablissement.etablissement_categorie=etablissement_categorie.etablissement_categorie
            LEFT JOIN ".DB_PREFIXE."dossier_coordination_type
                ON dossier_coordination.dossier_coordination_type=dossier_coordination_type.dossier_coordination_type
            LEFT JOIN ".DB_PREFIXE."dossier_type
                ON dossier_coordination_type.dossier_type=dossier_type.dossier_type
            LEFT JOIN ".DB_PREFIXE."dossier_instruction_reunion
                ON dossier_instruction_reunion.dossier_instruction=dossier_instruction.dossier_instruction
            LEFT JOIN ".DB_PREFIXE."reunion
                ON dossier_instruction_reunion.reunion=reunion.reunion
            LEFT JOIN ".DB_PREFIXE."reunion_type
                ON reunion.reunion_type=reunion_type.reunion_type

        ";
        $sql .= "WHERE
            EXTRACT(YEAR FROM reunion.date_reunion)=".$annee."
            AND reunion.date_reunion < NOW()
            AND dossier_type.code='PLAN'
            ";

        //
        if ($this->get_filter_mode() == "by_service") {
            $sql .= " AND dossier_instruction.service=".$this->get_service()." ";
        }
         if ($this->get_filter_mode() == "by_acteur") {
            $sql .= " AND acteur.acteur=".$this->get_acteur()." ";
        }

        $sql .= " 
        GROUP BY 
            etablissement_type.libelle, 
            reunion_type.code
        ORDER BY 
            etablissement_type.libelle, 
            reunion_type.code
        ";

        /**
         *
         */
        //
        $results = $this->get_db_query_all($sql);
        //
        $sql = "select code as ordonnee from ".DB_PREFIXE."reunion_type order by libelle";
        $ordonnee = $this->get_db_query_all($sql);
        //
        $sql = "select libelle as abscisse from ".DB_PREFIXE."etablissement_type order by libelle";
        $abscisse = $this->get_db_query_all($sql);
        $table = $this->prepare_table($abscisse, $ordonnee, $results);
        //
        return $table;

    }

    /**
     *
     */
    function nombre_visites_realisees_type($annee) {

        //
        $sql = "
        SELECT
        ";
        $sql .= "
        (etablissement_type.libelle) as abscisse,
        (etablissement_categorie.libelle) as ordonnee,
        count(visite.visite)
        ";

        //
        $sql .= "FROM

                ".DB_PREFIXE."visite
            LEFT JOIN ".DB_PREFIXE."programmation
                ON visite.programmation=programmation.programmation
            LEFT JOIN ".DB_PREFIXE."programmation_etat
                ON programmation.programmation_etat=programmation_etat.programmation_etat
            LEFT JOIN ".DB_PREFIXE."visite_etat
                ON visite.visite_etat=visite_etat.visite_etat
            LEFT JOIN ".DB_PREFIXE."acteur
                ON visite.acteur=acteur.acteur
            LEFT JOIN ".DB_PREFIXE."dossier_instruction
                ON visite.dossier_instruction=dossier_instruction.dossier_instruction
            LEFT JOIN ".DB_PREFIXE."dossier_coordination
                ON dossier_instruction.dossier_coordination=dossier_coordination.dossier_coordination
            LEFT JOIN ".DB_PREFIXE."etablissement
                ON dossier_coordination.etablissement=etablissement.etablissement
            LEFT JOIN ".DB_PREFIXE."etablissement_type
                ON etablissement.etablissement_type=etablissement_type.etablissement_type
            LEFT JOIN ".DB_PREFIXE."etablissement_categorie
                ON etablissement.etablissement_categorie=etablissement_categorie.etablissement_categorie
        
        ";
        $sql .= "WHERE
            LOWER(visite_etat.code) <> 'ann'
            AND NOT (
                visite.programmation_version_creation IS NOT NULL
                AND visite.programmation_version_creation=programmation.version
                AND LOWER(programmation_etat.code) <> 'val'
            )
            AND EXTRACT(YEAR FROM visite.date_visite)=".$annee."
            AND visite.date_visite < CURRENT_DATE
            ";

        //
        if ($this->get_filter_mode() == "by_service") {
            $sql .= " AND acteur.service=".$this->get_service()." ";
        }
         if ($this->get_filter_mode() == "by_acteur") {
            $sql .= " AND acteur.acteur=".$this->get_acteur()." ";
        }

        $sql .= " 
        GROUP BY 
            etablissement_type.libelle, 
            etablissement_categorie.libelle
        ORDER BY 
            etablissement_type.libelle, 
            etablissement_categorie.libelle
        ";
        /**
         *
         */
        //
        $results = $this->get_db_query_all($sql);
        //
        $sql = "select libelle from ".DB_PREFIXE."etablissement_categorie order by libelle";
        $ordonnee = $this->get_db_query_all($sql);
        //
        $sql = "select libelle from ".DB_PREFIXE."etablissement_type order by libelle";
        $abscisse = $this->get_db_query_all($sql);
        $table = $this->prepare_table($abscisse, $ordonnee, $results);
        //
        return $table;

    }

    function prepare_table($abscisse, $ordonnee, $data) {
        // Ligne d'entêtes
        $table = array(
            "attr" => array(
                "head" => array(),
            ),
        );
        // colonnes
        $empty_line = array();
        // première colonne : libelle pour la catégorie
        $table["attr"]["head"][] = "";
        $empty_line["firstcol"] = "";
        // Création d'une ligne avec autant de d'éléments que de colonnes à nulle
        foreach ($abscisse as $key => $value) {
            $table["attr"]["head"][] = $value["abscisse"];
            $empty_line[$value["abscisse"]] = 0;
        }
        // dernière colonne : total
        $table["attr"]["head"][] = __("Total");
        $empty_line["total"] = 0;
        if ($ordonnee !== false) {
            // Ajout des lignes nulles avec entête de ligne
            foreach ($ordonnee as $key => $value) {
                $table[$value["ordonnee"]] = $empty_line;
                $table[$value["ordonnee"]]["firstcol"] = $value["ordonnee"];
            }
            // Ajout de la ligne de totaux
            $table["total"] = $empty_line;
            $table["total"]["firstcol"] = __("Total");
        }
        //On boucle sur les données et les inserts dans les bonnes cases
        foreach ($data as $key => $value) {
            // Si une ligne ou colonne est nulle aucunne valeur ajoutée.
            if(($value["ordonnee"] == "" and $ordonnee !== false) or $value["abscisse"] == "") {
                continue;
            }
            $table[$value["ordonnee"]][$value["abscisse"]] = intval($value["count"]);
            // Totaux
            $table["total"][$value["abscisse"]] += intval($value["count"]);
            $table[$value["ordonnee"]]["total"] += intval($value["count"]);
            $table["total"]["total"] += intval($value["count"]);
        }
        return $table;
    }

    /**
     *
     */
    function nombre_de_visites_en_retard() {
        $sql = sprintf(
            'SELECT
                count(etablissement.etablissement)
            FROM
                %1$setablissement
            WHERE
                etablissement.si_prochaine_visite_periodique_date_previsionnelle < CURRENT_DATE',
            DB_PREFIXE
        );
        $res = $this->get_db_query_one($sql);
        return $res;
    }

    /**
     *
     */
    function nombre_d_avis($mode) {

        //
        $sql = "
        SELECT
        ";
        if ($mode == "detail") {
            $sql .= "
            (dossier_coordination_type.code || ' - '||dossier_coordination_type.libelle) as abscisse,
            reunion_avis.code as ordonnee, 
            count(dossier_instruction_reunion.dossier_instruction_reunion)
            ";
        } else {
            $sql .= "
            count(dossier_instruction_reunion.dossier_instruction_reunion)
            ";
        }
        //
        $sql .= "
        FROM
            ".DB_PREFIXE."dossier_instruction_reunion
                LEFT JOIN ".DB_PREFIXE."dossier_instruction
                    ON dossier_instruction_reunion.dossier_instruction=dossier_instruction.dossier_instruction
                LEFT JOIN ".DB_PREFIXE."dossier_coordination
                    ON dossier_instruction.dossier_coordination=dossier_coordination.dossier_coordination
                LEFT JOIN ".DB_PREFIXE."dossier_coordination_type
                    ON dossier_coordination.dossier_coordination_type=dossier_coordination_type.dossier_coordination_type
                LEFT JOIN ".DB_PREFIXE."reunion
                    ON dossier_instruction_reunion.reunion=reunion.reunion
                LEFT JOIN ".DB_PREFIXE."reunion_avis
                    ON dossier_instruction_reunion.avis=reunion_avis.reunion_avis
                LEFT JOIN ".DB_PREFIXE."reunion_type
                    ON dossier_instruction_reunion.reunion_type=reunion_type.reunion_type
                LEFT JOIN ".DB_PREFIXE."acteur
                    ON dossier_instruction.technicien=acteur.acteur
        WHERE
            reunion_type.commission = true
            AND EXTRACT(YEAR FROM reunion.date_reunion)=EXTRACT(YEAR FROM NOW())
        ";
        //
        //
        if ($mode == "favorables") {
            $sql .= "
            AND reunion_avis.code='FAV'
            ";
        } elseif ($mode == "defavorables") {
            $sql .= "
            AND reunion_avis.code='DEF'
            ";
        }
        //
        if ($this->get_filter_mode() == "by_service") {
            $sql .= " AND dossier_instruction.service=".$this->get_service()." ";
        }
         if ($this->get_filter_mode() == "by_acteur") {
            $sql .= " AND acteur.acteur=".$this->get_acteur()." ";
        }

        //
        if ($mode == "detail") {
            $sql .= " 
            GROUP BY 
                dossier_coordination_type.dossier_coordination_type,
                reunion_avis.reunion_avis
            ORDER BY 
                dossier_coordination_type.dossier_coordination_type,
                reunion_avis.reunion_avis
            ";
        }

        /**
         *
         */
        if ($mode == "detail") {
            //
            $res = $this->get_db_query_all($sql);
            //
            $new_results = array(
                "attr" => array(
                    "head" => array(
                        __("Type de dossier"),
                        __("Avis favorables"),
                        __("Avis défavorables"),
                    ),
                ),
            );
            //
            $elems = array();
            //
            $elem = array(
                "type" => "",
                "fav" => 0,
                "def" => 0,
            );
            foreach ($res as $key_line => $line) {
                //
                if (!isset($elems[$line["abscisse"]])) {
                    $elems[$line["abscisse"]] = $elem;
                    $elems[$line["abscisse"]]["type"] = $line["abscisse"];
                }
                //
                if ($line["ordonnee"] === "FAV") {
                    $elems[$line["abscisse"]]["fav"] = $line["count"];
                } elseif ($line["ordonnee"] === "DEF") {
                    $elems[$line["abscisse"]]["def"] = $line["count"];
                }
            }
            //
            return array_merge(
                $new_results,
                $elems
            );
        } else {
            //
            $res = $this->get_db_query_one($sql);
            //
            return $res;
        }
    }

    /**
     *
     */
    function nombre_d_avis_type($annee) {

        //
        $sql = "
        SELECT
        ";
        $sql .= "
        etablissement_type.libelle as abscisse,
        reunion_avis.code as ordonnee, 
        count(dossier_instruction_reunion.dossier_instruction_reunion)
        ";

        //
        $sql .= "
        FROM
            ".DB_PREFIXE."dossier_instruction_reunion
                LEFT JOIN ".DB_PREFIXE."dossier_instruction
                    ON dossier_instruction_reunion.dossier_instruction=dossier_instruction.dossier_instruction
                LEFT JOIN ".DB_PREFIXE."dossier_coordination
                    ON dossier_instruction.dossier_coordination=dossier_coordination.dossier_coordination
                LEFT JOIN ".DB_PREFIXE."etablissement
                    ON dossier_coordination.etablissement=etablissement.etablissement
                LEFT JOIN ".DB_PREFIXE."etablissement_type
                    ON etablissement.etablissement_type=etablissement_type.etablissement_type
                LEFT JOIN ".DB_PREFIXE."dossier_coordination_type
                    ON dossier_coordination.dossier_coordination_type=dossier_coordination_type.dossier_coordination_type
                LEFT JOIN ".DB_PREFIXE."reunion
                    ON dossier_instruction_reunion.reunion=reunion.reunion
                LEFT JOIN ".DB_PREFIXE."reunion_avis
                    ON dossier_instruction_reunion.avis=reunion_avis.reunion_avis
                LEFT JOIN ".DB_PREFIXE."reunion_type
                    ON dossier_instruction_reunion.reunion_type=reunion_type.reunion_type
                LEFT JOIN ".DB_PREFIXE."acteur
                    ON dossier_instruction.technicien=acteur.acteur
        WHERE
            reunion_type.commission = true
            AND EXTRACT(YEAR FROM reunion.date_reunion)=".$annee."
        ";
        //
        if ($this->get_filter_mode() == "by_service") {
            $sql .= " AND dossier_instruction.service=".$this->get_service()." ";
        }
        if ($this->get_filter_mode() == "by_acteur") {
            $sql .= " AND acteur.acteur=".$this->get_acteur()." ";
        }

        $sql .= " 
        GROUP BY 
            etablissement_type.libelle, 
            reunion_avis.code
        ORDER BY 
            abscisse,
            ordonnee
        ";
        /**
         *
         */
        //
        $res = $this->get_db_query_all($sql);
        $sql = "select libelle as abscisse from ".DB_PREFIXE."etablissement_type order by libelle";
        $abscisse = $this->get_db_query_all($sql);
        //
        $sql = "select code as ordonnee from ".DB_PREFIXE."reunion_avis where service=".$this->get_service()."  order by code";
        $ordonnee = $this->get_db_query_all($sql);
        $table = $this->prepare_table($abscisse, $ordonnee, $res);
        //
        return $table;

    }

    /**
     *
     */
    function nombre_d_avis_categorie($annee) {

        //
        $sql = "
        SELECT
        ";
        $sql .= "
        etablissement_categorie.libelle as abscisse,
        reunion_avis.code as ordonnee, 
        count(dossier_instruction_reunion.dossier_instruction_reunion)
        ";

        //
        $sql .= "
        FROM
            ".DB_PREFIXE."dossier_instruction_reunion
                LEFT JOIN ".DB_PREFIXE."dossier_instruction
                    ON dossier_instruction_reunion.dossier_instruction=dossier_instruction.dossier_instruction
                LEFT JOIN ".DB_PREFIXE."dossier_coordination
                    ON dossier_instruction.dossier_coordination=dossier_coordination.dossier_coordination
                LEFT JOIN ".DB_PREFIXE."etablissement
                    ON dossier_coordination.etablissement=etablissement.etablissement
                LEFT JOIN ".DB_PREFIXE."etablissement_categorie
                    ON etablissement.etablissement_categorie=etablissement_categorie.etablissement_categorie
                LEFT JOIN ".DB_PREFIXE."dossier_coordination_type
                    ON dossier_coordination.dossier_coordination_type=dossier_coordination_type.dossier_coordination_type
                LEFT JOIN ".DB_PREFIXE."reunion
                    ON dossier_instruction_reunion.reunion=reunion.reunion
                LEFT JOIN ".DB_PREFIXE."reunion_avis
                    ON dossier_instruction_reunion.avis=reunion_avis.reunion_avis
                LEFT JOIN ".DB_PREFIXE."reunion_type
                    ON dossier_instruction_reunion.reunion_type=reunion_type.reunion_type
                LEFT JOIN ".DB_PREFIXE."acteur
                    ON dossier_instruction.technicien=acteur.acteur
        WHERE
            reunion_type.commission = true
            AND EXTRACT(YEAR FROM reunion.date_reunion)=".$annee."
        ";
        //
        if ($this->get_filter_mode() == "by_service") {
            $sql .= " AND dossier_instruction.service=".$this->get_service()." ";
        }
        if ($this->get_filter_mode() == "by_acteur") {
            $sql .= " AND acteur.acteur=".$this->get_acteur()." ";
        }

        $sql .= " 
        GROUP BY 
            etablissement_categorie.libelle, 
            reunion_avis.code
        ORDER BY 
            abscisse,
            ordonnee
        ";
        /**
         *
         */
        //
        $res = $this->get_db_query_all($sql);
        $sql = "select libelle as abscisse from ".DB_PREFIXE."etablissement_categorie order by libelle";
        $abscisse = $this->get_db_query_all($sql);
        //
        $sql = "select code as ordonnee from ".DB_PREFIXE."reunion_avis where service=".$this->get_service()."  order by code";
        $ordonnee = $this->get_db_query_all($sql);
        $table = $this->prepare_table($abscisse, $ordonnee, $res);
        //
        return $table;

    }

     function nombre_ap_decision_categorie($annee) {
        //
        $sql = "
        SELECT
        ";
        $sql .= "
        autorite_police_decision.libelle as ordonnee,
        etablissement_categorie.libelle as abscisse,
        count(autorite_police.autorite_police)
        ";

        //
        $sql .= "
        FROM ".DB_PREFIXE."autorite_police
                LEFT JOIN ".DB_PREFIXE."autorite_police_decision
                    ON autorite_police.autorite_police_decision=autorite_police_decision.autorite_police_decision
                LEFT JOIN ".DB_PREFIXE."dossier_instruction_reunion
                    ON autorite_police.dossier_instruction_reunion=dossier_instruction_reunion.dossier_instruction_reunion
                LEFT JOIN ".DB_PREFIXE."dossier_instruction
                    ON dossier_instruction_reunion.dossier_instruction=dossier_instruction.dossier_instruction
                LEFT JOIN ".DB_PREFIXE."dossier_coordination
                    ON autorite_police.dossier_coordination=dossier_coordination.dossier_coordination
                LEFT JOIN ".DB_PREFIXE."etablissement
                    ON dossier_coordination.etablissement=etablissement.etablissement
                LEFT JOIN ".DB_PREFIXE."etablissement_categorie
                    ON etablissement.etablissement_categorie=etablissement_categorie.etablissement_categorie
                LEFT JOIN ".DB_PREFIXE."dossier_coordination_type
                    ON dossier_coordination.dossier_coordination_type=dossier_coordination_type.dossier_coordination_type
                LEFT JOIN ".DB_PREFIXE."acteur
                    ON dossier_instruction.technicien=acteur.acteur
        WHERE
            EXTRACT(YEAR FROM autorite_police.date_decision)=".$annee."
        ";
        //
        if ($this->get_filter_mode() == "by_service") {
            $sql .= " AND autorite_police.service=".$this->get_service()." ";
        }
        if ($this->get_filter_mode() == "by_acteur") {
            $sql .= " AND acteur.acteur=".$this->get_acteur()." ";
        }

        $sql .= " 
        GROUP BY 
            autorite_police_decision.libelle, 
            etablissement_categorie.libelle
        ORDER BY 
            autorite_police_decision.libelle, 
            etablissement_categorie.libelle
        ";
        /**
         *
         */
        //
        $res = $this->get_db_query_all($sql);

        $sql = "select libelle as abscisse from ".DB_PREFIXE."etablissement_categorie order by libelle";
        $abscisse = $this->get_db_query_all($sql);
        //
        $sql = "select libelle as ordonnee from ".DB_PREFIXE."autorite_police_decision order by libelle";
        $ordonnee = $this->get_db_query_all($sql);
        $table = $this->prepare_table($abscisse, $ordonnee, $res);
        //
        return $table;
    }

    /**
     * CCA : Nombre de dossiers par date et par type de dossiers.
     *
     * @param string  $annee année de tableau
     *
     * @return array tableau tel qu'il doit être affiché
     */
    function nombre_dossiers_date_param($annee, $col_abscisse) {
        //
        $sql = "
        SELECT
        ";
        $sql .= "
        to_char(reunion.date_reunion,'DD/MM/YYYY') as ordonnee,
        ".$col_abscisse["table"].".".$col_abscisse["column"]." as abscisse,
        count(dossier_instruction.dossier_instruction)
        ";

        //
        $sql .= "
        FROM ".DB_PREFIXE."dossier_instruction
            LEFT JOIN ".DB_PREFIXE."dossier_instruction_reunion
                ON dossier_instruction_reunion.dossier_instruction=dossier_instruction.dossier_instruction
            LEFT JOIN ".DB_PREFIXE."reunion_avis
                ON dossier_instruction_reunion.avis=reunion_avis.reunion_avis
            LEFT JOIN ".DB_PREFIXE."reunion
                ON dossier_instruction_reunion.reunion=reunion.reunion
            LEFT JOIN ".DB_PREFIXE."reunion_type
                    ON reunion.reunion_type=reunion_type.reunion_type
            LEFT JOIN ".DB_PREFIXE."dossier_coordination
                ON dossier_instruction.dossier_coordination=dossier_coordination.dossier_coordination
            LEFT JOIN ".DB_PREFIXE."dossier_coordination_type
                ON dossier_coordination.dossier_coordination_type=dossier_coordination_type.dossier_coordination_type
            LEFT JOIN ".DB_PREFIXE."etablissement
                ON dossier_coordination.etablissement=etablissement.etablissement
            LEFT JOIN ".DB_PREFIXE."etablissement_categorie
                ON etablissement.etablissement_categorie=etablissement_categorie.etablissement_categorie
            LEFT JOIN ".DB_PREFIXE."acteur
                ON dossier_instruction.technicien=acteur.acteur
        WHERE
            reunion_type.commission = true
            AND EXTRACT(YEAR FROM reunion.date_reunion)=".$annee."
        ";
        //
        if ($this->get_filter_mode() == "by_service") {
            $sql .= " AND dossier_instruction.service=".$this->get_service()." ";
        }
         if ($this->get_filter_mode() == "by_acteur") {
            $sql .= " AND acteur.acteur=".$this->get_acteur()." ";
        }

        $sql .= " 
        GROUP BY 
            reunion.date_reunion, 
            ".$col_abscisse["table"].".".$col_abscisse["column"]."
        ORDER BY 
            reunion.date_reunion, 
            ".$col_abscisse["table"].".".$col_abscisse["column"]."
        ";
        /**
         *
         */
        //
        $res = $this->get_db_query_all($sql);

        $sql = "select ".$col_abscisse["column"]." as abscisse
                from ".DB_PREFIXE.$col_abscisse["table"];
        if(isset($col_abscisse["where"])) {
            $sql .= " WHERE ".$col_abscisse["where"];
        }
        $sql .= " order by ".$col_abscisse["column"];
        $abscisse = $this->get_db_query_all($sql);
        //
        $sql = "SELECT to_char(reunion.date_reunion,'DD/MM/YYYY') as ordonnee
            FROM ".DB_PREFIXE."reunion
            LEFT JOIN ".DB_PREFIXE."reunion_type
                ON reunion.reunion_type=reunion_type.reunion_type
            LEFT JOIN ".DB_PREFIXE."dossier_instruction_reunion
                ON dossier_instruction_reunion.reunion=reunion.reunion
            LEFT JOIN ".DB_PREFIXE."dossier_instruction
                ON dossier_instruction_reunion.dossier_instruction=dossier_instruction.dossier_instruction
            WHERE
                reunion_type.commission = true
                AND dossier_instruction.service=".$this->get_service()."
                AND EXTRACT(YEAR FROM reunion.date_reunion)=".$annee."
            ORDER BY reunion.date_reunion";
        $ordonnee = $this->get_db_query_all($sql);
        $table = $this->prepare_table($abscisse, $ordonnee, $res);
        //
        return $table;
    }

    /**
     *
     */
    function nombre_d_avis_favorables() {
        //
        return $this->nombre_d_avis("favorables");
    }

    /**
     *
     */
    function nombre_d_avis_defavorables() {
        //
        return $this->nombre_d_avis("defavorables");
    }

    /**
     *
     */
    function nombre_de_dc_avec_ap_en_cours() {
        //
        $sql = "
        SELECT 
            count(dossier_coordination.dossier_coordination)
        FROM
            ".DB_PREFIXE."dossier_coordination
                LEFT JOIN ".DB_PREFIXE."autorite_police
                    ON dossier_coordination.dossier_coordination=autorite_police.dossier_coordination
        WHERE
            dossier_coordination.autorite_police_encours is TRUE
        ";
        //
        if ($this->get_filter_mode() == "by_service") {
            $sql .= " AND autorite_police.service=".$this->get_service()." ";
        }
        if ($this->get_filter_mode() == "by_acteur") {
            $sql .= " AND autorite_police.service=".$this->get_service()." ";
        }
        //
        $res = $this->get_db_query_one($sql);
        //
        return $res;
    }

    /**
     *
     */
    function nombre_de_di_plan_etudies() {
        //
        $sql = "SELECT count(dossier_instruction.dossier_instruction)
                FROM ".DB_PREFIXE."dossier_instruction
                LEFT JOIN ".DB_PREFIXE."dossier_coordination
                    ON dossier_instruction.dossier_coordination = dossier_coordination.dossier_coordination
                LEFT JOIN ".DB_PREFIXE."dossier_coordination_type
                    ON dossier_coordination.dossier_coordination_type = dossier_coordination_type.dossier_coordination_type
                LEFT JOIN ".DB_PREFIXE."dossier_type
                    ON dossier_coordination_type.dossier_type = dossier_type.dossier_type
                WHERE LOWER(dossier_type.code) = 'plan'
                AND EXTRACT(YEAR FROM dossier_instruction.date_cloture) = EXTRACT(YEAR FROM NOW())";
        //
        if ($this->get_filter_mode() == "by_service") {
            $sql .= " AND dossier_instruction.service=".$this->get_service()." ";
        }
        if ($this->get_filter_mode() == "by_acteur") {
            $sql .= " AND dossier_instruction.service=".$this->get_service()." ";
        }
        //
        $res = $this->get_db_query_one($sql);
        //
        return $res;
    }

    /**
     *
     */
    function nombre_de_di_encours() {
        //
        $sql = "
        SELECT 
            count(dossier_instruction.dossier_instruction)
        FROM
            ".DB_PREFIXE."dossier_instruction
        WHERE
            dossier_instruction.dossier_cloture is FALSE
            AND dossier_instruction.a_qualifier is FALSE
        ";
        //
        if ($this->get_filter_mode() == "by_service") {
            $sql .= " AND dossier_instruction.service=".$this->get_service()." ";
        }
        if ($this->get_filter_mode() == "by_acteur") {
            $sql .= " AND dossier_instruction.technicien=".$this->get_acteur()." ";
        }
        //
        $res = $this->get_db_query_one($sql);
        //
        return $res;
    }

    /**
     * Nombre de dossier d'instruction par type et par statut.
     *
     * @param string $mode Mode de composition de la requête
     *
     * @return mixed
     */
    function nombre_de_di_par_statut($mode) {

        // Requête SQL
        $sql = "
        SELECT
        ";
        //
        if ($mode == "detail") {
            $sql .= "
            (dossier_type.libelle) AS abscisse,
            (CASE WHEN dossier_instruction.a_qualifier IS TRUE THEN '".__("a_qualifier")."' WHEN dossier_instruction.dossier_cloture IS TRUE THEN '".__("dossier_cloture")."' ELSE '".__("en cours")."' END) AS ordonnee,
            COUNT(dossier_instruction.dossier_instruction)
            ";
        } else {
            $sql .= "
            COUNT(dossier_instruction.dossier_instruction)
            ";
        }
        //
        $sql .= "
        FROM
            ".DB_PREFIXE."dossier_instruction
                LEFT JOIN ".DB_PREFIXE."dossier_coordination
                    ON dossier_instruction.dossier_coordination=dossier_coordination.dossier_coordination
                LEFT JOIN ".DB_PREFIXE."dossier_coordination_type
                    ON dossier_coordination.dossier_coordination_type=dossier_coordination_type.dossier_coordination_type
                LEFT JOIN ".DB_PREFIXE."dossier_type
                    ON dossier_coordination_type.dossier_type=dossier_type.dossier_type
        ";
        //
        if ($this->get_filter_mode() == "by_service") {
            $sql .= " AND dossier_instruction.service=".$this->get_service()." ";
        }
        //
        if ($this->get_filter_mode() == "by_acteur") {
            $sql .= " AND dossier_instruction.technicien=".$this->get_acteur()." ";
        }

        //
        if ($mode == "detail") {
            $sql .= " 
            GROUP BY 
                abscisse,
                ordonnee
            ORDER BY 
                abscisse,
                ordonnee
            ";
        }

        /**
         *
         */
        if ($mode == "detail") {
            //
            $results = $this->get_db_query_all($sql);
            //
            $sql = "select (CASE WHEN dossier_instruction.a_qualifier IS TRUE THEN '".__("a_qualifier")."' WHEN dossier_instruction.dossier_cloture IS TRUE THEN '".__("dossier_cloture")."' ELSE '".__("en cours")."' END) AS ordonnee from ".DB_PREFIXE."dossier_instruction order by ordonnee";
            $ordonnee = $this->get_db_query_all($sql);
            //
            $sql = "select libelle as abscisse from ".DB_PREFIXE."dossier_type order by libelle";
            $abscisse = $this->get_db_query_all($sql);
            //
            $table = $this->prepare_table($abscisse, $ordonnee, $results);
            //
            return $table;
        } else {
            //
            $res = $this->get_db_query_one($sql);
            //
            return $res;
        }
    }

    /**
     *
     */
    function delai_moyen_instruction() {
        // Requête SQL
        $sql = "SELECT avg(date_cloture - date_ouverture)::int as delai_moyen
                FROM ".DB_PREFIXE."dossier_instruction
                LEFT JOIN ".DB_PREFIXE."acteur
                    ON dossier_instruction.technicien=acteur.acteur
                WHERE date_cloture >= (date_cloture - interval '6 month')";

        //
        if ($this->get_filter_mode() == "by_service") {
            $sql .= " AND acteur.service=".$this->get_service()." ";
        }
        if ($this->get_filter_mode() == "by_acteur") {
            $sql .= " AND acteur.acteur=".$this->get_acteur()." ";
        }

        //
        $res = $this->get_db_query_one($sql);
        //
        if (empty($res)) {
            return 0;
        }
        //
        return $res;
    }

    function delai_moyen_suivi_ap() {
        // Requête SQL
        $sql = "SELECT avg(date_cloture - date_decision)::int as delai_moyen
                FROM ".DB_PREFIXE."autorite_police
                LEFT JOIN ".DB_PREFIXE."dossier_instruction_reunion
                    ON autorite_police.dossier_instruction_reunion = dossier_instruction_reunion.dossier_instruction_reunion
                LEFT JOIN ".DB_PREFIXE."dossier_instruction
                    ON dossier_instruction_reunion.dossier_instruction = dossier_instruction.dossier_instruction
                LEFT JOIN ".DB_PREFIXE."acteur
                    ON dossier_instruction.technicien = acteur.acteur
                WHERE date_cloture >= (date_cloture - interval '6 month')";

        //
        if ($this->get_filter_mode() == "by_service") {
            $sql .= " AND acteur.service=".$this->get_service()." ";
        }
         if ($this->get_filter_mode() == "by_acteur") {
            $sql .= " AND acteur.acteur=".$this->get_acteur()." ";
        }

        //
        $res = $this->get_db_query_one($sql);
        //
        if (empty($res)) {
            return 0;
        }
        //
        return $res;
    }

    function delai_moyen_notification() {
        // Requête SQL
        $sql = "SELECT avg(om_date_creation - date_envoi_rar)::int as delai_moyen
                FROM ".DB_PREFIXE."courrier
                LEFT JOIN ".DB_PREFIXE."dossier_instruction
                    ON courrier.dossier_instruction = dossier_instruction.dossier_instruction
                LEFT JOIN ".DB_PREFIXE."acteur
                    ON dossier_instruction.technicien = acteur.acteur";

        //
        if ($this->get_filter_mode() == "by_service") {
            $sql .= " AND acteur.service=".$this->get_service()." ";
        }
         if ($this->get_filter_mode() == "by_acteur") {
            $sql .= " AND acteur.acteur=".$this->get_acteur()." ";
        }

        //
        $res = $this->get_db_query_one($sql);
        //
        if (empty($res)) {
            return 0;
        }
        //
        return $res;
    }

    /**
     *
     * @return string
     */
    function sprint_stat_numberby($identifier) {
        //
        $stat = $this->get_stat($identifier);
        if (is_null($stat)) {
            return "";
        }
        return sprintf(
            $this->template_panel_elem_box,
            $stat["class"],
            call_user_func(array($this, $stat["method"])),
            sprintf(
                '%s %s',
                $stat["title"],
                $this->sprint_icon_info($stat["description"])
            )
        );
    }

    /**
     *
     * @return string
     */
    function sprint_form_edition_stats() {
        $option_annee = "";
        for ($date=intval(date("Y")); $date >= 2014 ; $date--) {
            $option_annee .= "<option value=\"$date\">$date</option>";
        }
        // XXX améliorer
        $form_action = "../app/index.php?module=pilotage&view=edition-pdf";
        if (isset($_GET["service"])) {
            $form_action .= "&service=".$_GET["service"];
        }
        //
        $template_elem = "
        <li>
            <form method=\"post\" target=\"_blank\" name=\"editions-stats\" action=\"%s\">
            <select name=\"editions-stats-annee\" />%s</select>
            <input type=\"submit\" value=\"%s\" name=\"editions-stats-submit\" class=\"om-button\"/>
            </form>
        </li>
        ";
        return sprintf(
            $template_elem,
            $form_action,
            $option_annee,
            __("Generer"),("Generer")
        );
    }

    function sprint_widget($header, $content) {
        $template_widget = '
<div class="widget">
    <div class="widget-header">
        <h3>%s</h3>
    </div>
    <div class="widget-content-wrapper">
        <div class="widget-content">
            %s
        </div>
    </div>
</div>
';
        return sprintf(
            $template_widget,
            $header,
            $content
        );
    }


    function sprint_stat_pie($identifiers) {
        //
        $id = rand();
        //
        $stats = array();
        foreach ($identifiers as $value) {
            $stats[] = $this->get_stat($value);
        }
        //
        $template_data = '
{
    value: %s,
    color:"%s",
    highlight: "%s",
    label: "%s"
}
';
        $datas = "";
        foreach($stats as $key => $stat) {
            $stat_method = $stat["method"];
            $datas .= sprintf(
                $template_data,
                $this->$stat_method(),
                $stat["colors"][0],
                $stat["colors"][1],
                $stat["title"]
            );
            if ($key != (count($stats)-1)) {
                $datas .= ",";
            }
        }
        //
        $template_pie = '
<div class="chart">
    <canvas id="myChart-%s" class="chart-canvas" width="200" height="200"></canvas>
    <div id="myChart-%s-legend" class="chart-legend"></div>
</div>
<script type="text/javascript">
var data = [%s];
var options = {};
var ctx = document.getElementById("myChart-%s").getContext("2d");
var myPieChart = new Chart(ctx).Pie(data,options);
var legend = myPieChart.generateLegend();
$("#myChart-%s-legend").html(legend);
</script>
';
        return sprintf(
            $template_pie,
            $id,
            $id,
            $datas,
            $id,
            $id
        );
    }


    function sprint_service_selector() {
        //
        $template_service_selector = '
        <div>
            %s
        </div>
        <br/>
        ';
        $template_service_selector_element = '
                <a class="btn %s" href="../app/index.php?module=pilotage&service=%s">%s</a>
        ';
        //
        $service_selector_elements = "";
        foreach ($this->services as $key => $value) {
            $service_selector_elements .= sprintf(
                $template_service_selector_element,
                ($this->get_service() == $value["service"] ? "btn-primary" : "btn-default"),
                $value["service"],
                $value["libelle"]
            );
        }
        //
        return sprintf($template_service_selector, $service_selector_elements);
    }



    /**
     * Gère les filtres qui seront utilisés pour générer les statistiques
     * selon l'environnement.
     */
    function handle_filter() {
        // Si l'utilisateur n'a pas de service associé
        if (is_null($_SESSION["service"])) {
            // Si le service est précisé en paramètre dans le GET
            if (isset($_GET["service"])) {
                // On définit 
                $this->set_filter_mode("by_service");
                $this->set_service($_GET["service"]);
            }
            //
            if (isset($_GET["acteur"])) {
                $this->set_filter_mode("by_acteur");
                $this->set_acteur($_GET["acteur"]);
            }
        } else {
            // Sinon l'utilisateur a un service associé
            // 
            $this->set_filter_mode("by_service");
            $this->set_service($_SESSION["service"]);
            //
            if (isset($_GET["acteur"])) {
                $this->set_filter_mode("by_acteur");
                $this->set_acteur($_GET["acteur"]);
            }
        }
    }




    /**
     * Cette méthode permet de tester si le filtre est géré ou non
     * 
     * @return boolean
     */
    function is_handled_filter() {

        //
        if (($this->get_filter_mode() == null)
            || ($this->get_filter_mode() == "by_service"
             && $this->get_service() == -1)
            || ($this->get_filter_mode() == "by_acteur"
                && $this->get_acteur() == -1)
            ) {
            //
            return false;
        }
        return true;
    }

    /**
     * Vue principale.
     * 
     * S'il y a un paramètre spécifique il s'agit d'une génération PDF,
     * sinon de la vue des statistiques.
     */
    function view_main() {
        //
        $view = (isset($_GET["view"]) ? $_GET["view"] : "");
        //
        switch ($view) {
            case "edition-pdf":
                $view = "render_pdf";
                break;
            default :
                $view = "statistiques";
        }
        //
        $view_name = "view_".$view;
        $this->$view_name();
    }


    /**
     * Vue des statistiques.
     * 
     * Propose les statistiques et la possibilité de les éditer.
     * Si l'utilisateur connecté n'est pas rattaché à un service
     * alors il devient possible d'en sélectionner un.
     */
    function view_statistiques() {
        //
        $this->f->addHTMLHeadJs(array("../app/js/Chart.min.js"));
        //
        $this->f->setTitle(__("Statistiques"));
        //
        $this->f->setFlag(null);
        $this->f->display();
        //
        $this->handle_filter();
        //
        if ($this->is_handled_filter() === false) {
            // on affiche le sélecteur
            echo $this->sprint_service_selector();
            return;
        }

        /**
         *
         */
        //
        echo '
        <div id="dashboard">
        <div class="container-fluid">
        ';

        echo '
        <div class="row">
        <h2>'.__("État des lieux des éléments réalisés sur l'année courante").' ('.date("Y").')</h2>
        </div>
        ';

        //
        echo '
        <div class="row">
            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">';
        //
        $widget_ct = "";
        $widget_ct .= $this->sprint_stat_numberby("nb-visites-programmees");
        $widget_ct .= $this->sprint_stat_numberby("nb-visites-realisees");
        $widget_ct = sprintf($this->template_panel, $widget_ct);
        echo $this->sprint_widget("Visites", $widget_ct);
        //
        echo '
            </div>
            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
        ';
        //
        $widget_ct = "";
        $widget_ct .= $this->sprint_stat_numberby("nb-di-plan-etudies");
        $widget_ct = sprintf($this->template_panel, $widget_ct);
        echo $this->sprint_widget("Plans", $widget_ct);
        //
        echo '
            </div>
        </div>';
        //
        echo '
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">';
        // WIDGET 2
        echo $this->sprint_widget(
            "Avis",
            '<div class="container-fluid">
            <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">'.
            $this->sprint_stat_pie(
                array("nb-avis-favorables", "nb-avis-defavorables", )
            )
            .'</div>
            <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">'.
            $this->sprint_table($this->nombre_d_avis("detail"))
            .'</div>
            </div>
            </div>'
        );
        //
        echo '
            </div>
        </div>';
        //
        echo '
        <div class="row">
        <h2>'.__("État des lieux des éléments en cours").'</h2>
        </div>
        ';
        //
        echo '
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">';
        //
        $widget_ct = $this->sprint_stat_numberby("nb-visites-en-retard");
        echo $this->sprint_widget(
            "Visites en retard",
            '<div class="container-fluid">
            <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">'.
            sprintf($this->template_panel, $widget_ct)
            .'</div>
            <div class="col-xs-12 col-sm-12 col-md-10 col-lg-10">'.
             $this->sprint_table($this->nombre_d_etablissements("retard"))
            .'</div>
            </div>
            </div>'
        );
        //
        echo '
            </div>
        </div>';
        //
        echo '<div class="col2">';
        echo '<div class="column">';
        //
        $widget_ct = "";
        $widget_ct .= $this->sprint_stat_numberby("nb-di-encours");
        $widget_ct .= $this->sprint_stat_numberby("nb-dc-en-ap");
        $widget_ct = sprintf($this->template_panel, $widget_ct);
        echo $this->sprint_widget("Dossiers en cours", $widget_ct);
        //
        echo '</div>';
        echo '<div class="column">';
        //
        $widget_ct = "";
        $widget_ct .= $this->sprint_stat_numberby("delai-moyen-instruction-dossier");
        $widget_ct .= $this->sprint_stat_numberby("delai-moyen-suivi-ap");
        $widget_ct .= $this->sprint_stat_numberby("delai-moyen-notification");
        $widget_ct = sprintf($this->template_panel, $widget_ct);
        echo $this->sprint_widget("Délai", $widget_ct);
        //
        echo '</div>';
        echo '</div>';

        // Statut des traitements des dossiers en cours par type de dossier
        $widget_ct = "";
        $widget_ct = $this->sprint_table($this->nombre_de_di_par_statut("detail"));
        echo $this->sprint_widget("Dossiers par type et par statut", $widget_ct);

        // éditions
        echo '
        <div class="row">
            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">';
                //
        $widget_ct = "";
        $widget_ct .= $this->sprint_form_edition_stats();
        $widget_ct = sprintf($this->template_panel, $widget_ct);
        echo $this->sprint_widget(__("Éditions"), $widget_ct);
                echo '
            </div>
        </div>';

        echo '<div class="visualClear"><!-- --></div>';
        echo '
        </div>
        </div>
        ';
    }

    /**
     * Génère un PDF des statistisques de l'année passée en POST,
     * selon le service sélectionné.
     * 
     */
    function view_render_pdf() {
        // Récupération de l'éventuelle année postée
        (isset($_POST['editions-stats-annee']) ?
            $annee = $_POST['editions-stats-annee'] : $annee = "");
        //
        $this->handle_filter();
        //
        if ($this->is_handled_filter() === false
            || $annee == "") {
            $this->view_statistiques();
            return;
        }

        $this->instanciate_pdf_stat();

        // Si CCS (Sécurité Incendie)
        if ($this->f->get_service_code($this->get_service()) == 'si') {
            // Ajoute une nouvelle page à l'édition
            $this->pdf_edition->AddPage();
            $this->add_title_to_pdf_stat(sprintf(__("Visites realisees en %s"), $annee));
            // Visites réalisées : catégorie d'étab / type d'étab
            $params = array(
                'table_abs' => 'etablissement_type',
                'champ_abs' => 'libelle',
                'table_ord' => 'etablissement_categorie',
                'champ_ord' => 'libelle',
            );
            $this->add_array_to_pdf_stat(
                __("Par categorie et type d'etablissement"),
                $this->nombre_visites_realisees_edition($annee, $params)
            );
            // Visites réalisées : type de DC / catégorie d'étab
            $params = array(
                'table_ord' => 'dossier_coordination_type',
                'champ_ord' => 'libelle',
                'table_abs' => 'etablissement_categorie',
                'champ_abs' => 'libelle',
            );
            $this->add_array_to_pdf_stat(
                __("Par type de dossier et categorie d'etablissement"),
                $this->nombre_visites_realisees_edition($annee, $params)
            );
            // Ajoute une nouvelle page à l'édition
            $this->pdf_edition->AddPage();
            $this->add_title_to_pdf_stat(sprintf(__("Avis rendus en %s"), $annee));
            // Avis sur dossier : type d'avis / type d'étab
            $this->add_array_to_pdf_stat(
                __("Par avis et type d'etablissement"),
                $this->nombre_d_avis_type($annee)
            );
            // Avis sur dossier : type d'avis / catégorie d'étab
            $this->add_array_to_pdf_stat(
                __("Par avis et categorie d'etablissement"),
                $this->nombre_d_avis_categorie($annee)
            );
            // Ajoute une nouvelle page à l'édition
            $this->pdf_edition->AddPage();
            $this->add_title_to_pdf_stat(sprintf(__("Decisions d'autorite de police prises en %s"), $annee));
            // Décisions : décision / catégorie d'étab
            $this->add_array_to_pdf_stat(
                __("Par decision et categorie d'etablissement"),
                $this->nombre_ap_decision_categorie($annee)
            );
            // Ajoute une nouvelle page à l'édition
            $this->pdf_edition->AddPage();
            $this->add_title_to_pdf_stat(sprintf(__("Etudes de plan realisees en %s"), $annee));
            // DC de type plan : catégorie d'étab / type d'étab
            $this->add_array_to_pdf_stat(
                __("Par categorie et type d'etablissement"),
                $this->nombre_plan_categorie_type($annee)
            );
            // Ajoute des sauts de ligne à l'édition
            $this->pdf_edition->ln(7);
            // DC de type plan : type de réunion / type d'étab
            $this->add_array_to_pdf_stat(
                __("Par type de reunion et d'etablissement"),
                $this->nombre_plan_type_avis($annee)
            );
            $service = "CCS";
        }
        // Sinon CCA (Accessibilité)
        elseif ($this->f->get_service_code($this->get_service()) == 'acc') {
            // Ajoute une nouvelle page à l'édition
            $this->pdf_edition->AddPage();
            $this->add_title_to_pdf_stat(sprintf(__("Dossiers traites en reunion en %s"), $annee));
            // avis réunion
            $param = array(
                    'table' => 'reunion_avis',
                    'column' => 'code',
                    'where' => 'reunion_avis.service='.$this->get_service(),
                );
            $this->add_array_to_pdf_stat(
                __("Par date et avis de reunion"),
                $this->nombre_dossiers_date_param($annee, $param)
            );
            // Ajoute des sauts de ligne à l'édition
            $this->pdf_edition->ln(7);
            // type DC
            $param = array(
                    'table' => 'dossier_coordination_type',
                    'column' => 'code',
                );
            $this->add_array_to_pdf_stat(
                __("Par date de reunion et type de dossier de coordination"),
                $this->nombre_dossiers_date_param($annee, $param)
            );
            // Ajoute des sauts de ligne à l'édition
            $this->pdf_edition->ln(7);
            // catégorie établissement
            $param = array(
                    'table' => 'etablissement_categorie',
                    'column' => 'libelle',
                );
            $this->add_array_to_pdf_stat(
                __("Par date de reunion et categorie d'etablissement"),
                $this->nombre_dossiers_date_param($annee, $param)
            );
            // Ajoute des sauts de ligne à l'édition
            $this->pdf_edition->ln(7);
            // Avis sur dossier : type d'avis / catégorie d'étab
            $this->add_array_to_pdf_stat(
                __("Par avis et categorie d'etablissement"),
                $this->nombre_d_avis_categorie($annee)
            );
            $service = "CCA";
        } else {
            die();
        }
        // render
        $this->pdf_edition->Output("statistique_".$service."_".$annee.".pdf", "I");
    }

    function add_array_to_pdf_stat($title, $data) {
        $html = $this->add_css_to_pdf_stat();
        $html .= sprintf("<h3>%s</h3>", $title);
        $html .= $this->sprint_table($data);
        //echo htmlentities($html);
        $this->pdf_edition->writeHTML($html, true, false, true, false, '');
    }

    function add_title_to_pdf_stat($title) {
        $html = $this->add_css_to_pdf_stat();
        $html .= sprintf("<h2>%s</h2>", $title);
        $this->pdf_edition->writeHTML($html, true, false, true, false, '');
        $this->pdf_edition->ln(7);
    }

    function add_css_to_pdf_stat() {
        return "<style>
        h2 {
            text-align : center;
        }
        .table-bordered, td {
            border : solid 1px grey;
            font-size : 8px;
        }
        th {
            background-color : #ddd;
            font-weight : bold;
        }
        </style>";
    }

    function instanciate_pdf_stat() {
        /**
         * Inclusion de la classe TCPDF qui permet de generer des fichiers PDF.
         */
        require_once "tcpdf.php";
        $this->pdf_edition = new TCPDF(
            "L",
            "mm",
            "A4",
            true,
            'HTML-ENTITIES');
        $this->pdf_edition->setPrintHeader(false);
        // set margins
        $this->pdf_edition->setMargins(
            PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        $this->pdf_edition->SetHeaderMargin(PDF_MARGIN_TOP);
        $this->pdf_edition->SetFooterMargin(PDF_MARGIN_BOTTOM);
        // set auto page breaks
        $this->pdf_edition->SetAutoPageBreak(true, PDF_MARGIN_BOTTOM);

    }

    /**
     * VIEW - view_widget_activite_service.
     *
     * L'activité du service.
     *
     * L'objet de ce widget est de présenter les statistiques du service 
     * de l'utilisateur.
     *
     * @return boolean
     */
    public function view_widget_activite_service($content = null) {
        $widget_is_empty = false;
        $more_filter_by_service = "";
        /**
         * Ce widget est configurable via l'interface Web. Lors de la création
         * du widget dans le paramétrage il est possible de spécifier la ou les
         * options suivantes :
         *
         * - service : identifiant d'un service
         */
        $arguments = $this->get_arguments(
            $content,
            array("service", )
        );
        /**
         * Gestion du filtre sur le service
         */
        //
        if (array_key_exists("service", $arguments) === true
            && intval($arguments["service"]) > 0) {
            $inst_service = $this->f->get_inst__om_dbform(array(
                "obj" => "service",
                "idx" => intval($arguments["service"]),
            ));
            if ($inst_service->exists() === true) {
                $this->set_filter_mode("by_service");
                $this->set_service(intval($arguments["service"]));
                $more_filter_by_service = sprintf('&service=%s', intval($arguments["service"]));
            }
        } elseif (!is_null($_SESSION["service"])) {
            $this->set_filter_mode("by_service");
            $this->set_service($_SESSION["service"]);
        }
        //
        if (($this->get_filter_mode() == null)
            || ($this->get_filter_mode() == "by_service"
             && $this->get_service() == -1)
            || ($this->get_filter_mode() == "by_acteur"
                && $this->get_acteur() == -1)
            ) {
            echo __("Aucune activité");
            return;
        }
        /**
         * Affichage des chiffres
         */
        //
        $panel = "";
        //
        $panel_elems = "";
        $panel_elems .= $this->sprint_stat_numberby("nb-visites-programmees");
        $panel_elems .= $this->sprint_stat_numberby("nb-visites-realisees");
        $panel_elems .= $this->sprint_stat_numberby("nb-visites-en-retard");
        //
        $panel .= sprintf(
            $this->template_panel_list_by_3,
            $panel_elems
        );
        //
        $panel_elems = "";
        $panel_elems .= $this->sprint_stat_numberby("nb-di-encours");
        $panel_elems .= $this->sprint_stat_numberby("nb-di-plan-etudies");
        $panel_elems .= $this->sprint_stat_numberby("delai-moyen-instruction-dossier");
        //
        $panel .= sprintf(
            $this->template_panel_list_by_3,
            $panel_elems
        );
        //
        $panel_elems = "";
        $panel_elems .= $this->sprint_stat_numberby("nb-avis-favorables");
        $panel_elems .= $this->sprint_stat_numberby("nb-avis-defavorables");
        $panel_elems .= $this->sprint_stat_numberby("nb-dc-en-ap");
        //
        $panel .= sprintf(
            $this->template_panel_list_by_3,
            $panel_elems
        );
        //
        printf($panel);
        /**
         * Affichage du lien vers la page complète des statistiques
         */
        //
        if ($this->f->isAccredited("statistiques")) {
            printf($this->sprint_link(array(
                "href" => sprintf(
                    '../app/index.php?module=pilotage%s',
                    $more_filter_by_service
                ),
                "libelle" => __("Voir +")
            )));
        }
        return $widget_is_empty;
    }

    /**
     * VIEW - view_widget_mes_di_en_reunions.
     *
     * Mes dossiers en réunion.
     *
     * L'objet de ce widget est de permettre au technicien de visualiser les 
     * dossiers dont il est l'instructeur et qui vont passer en réunion dans 
     * le mois suivant la date du jour.
     *
     * @return boolean
     */
    public function view_widget_mes_di_en_reunions($content = null) {
        // Si l'utilisateur n'a pas les permissions nécessaires, le widget
        // n'est pas affiché.
        if ($this->f->isAccredited(array(
                "dossier_instruction",
                "dossier_instruction_tab", ), "OR") !== true) {
            // Le widget est vide.
            return true;
        }
        //
        $widget_is_empty = false;
        /**
         * Gestion du filtre sur l'acteur
         */
        //
        $this->set_filter_mode("by_acteur");
        $sql = "
        SELECT acteur
        FROM ".DB_PREFIXE."acteur 
            LEFT JOIN ".DB_PREFIXE."om_utilisateur
                ON  acteur.om_utilisateur=om_utilisateur.om_utilisateur
        WHERE om_utilisateur.login='".$_SESSION["login"]."'
        ";
        $acteur = $this->get_db_query_one($sql);
        $this->set_acteur($acteur);
        //
        if (!is_null($_SESSION["service"])) {
            //
            $this->set_service($_SESSION["service"]);
        }
        //
        if ($this->get_acteur() == -1) {
            return true;
        }
        $query = "
        SELECT
            reunion.reunion,
            reunion.date_reunion,
            reunion.libelle as reunion_libelle,
            dossier_instruction_reunion.reunion_type_categorie,
            reunion_categorie.libelle as reunion_categorie_libelle,
            dossier_type.code as dossier_type_code,
            dossier_instruction.dossier_instruction,
            dossier_instruction.libelle as dossier_instruction_libelle,
            concat(etablissement.code, ' - ', etablissement.libelle) as etablissement_libelle
        FROM
            ".DB_PREFIXE."dossier_instruction_reunion
                LEFT JOIN ".DB_PREFIXE."dossier_instruction
                    ON dossier_instruction_reunion.dossier_instruction=dossier_instruction.dossier_instruction
                LEFT JOIN ".DB_PREFIXE."dossier_coordination
                    ON dossier_instruction.dossier_coordination=dossier_coordination.dossier_coordination
                LEFT JOIN ".DB_PREFIXE."etablissement
                    ON dossier_coordination.etablissement=etablissement.etablissement
                LEFT JOIN ".DB_PREFIXE."dossier_coordination_type
                    ON dossier_coordination.dossier_coordination_type=dossier_coordination_type.dossier_coordination_type
                LEFT JOIN ".DB_PREFIXE."dossier_type
                    ON dossier_coordination_type.dossier_type=dossier_type.dossier_type
                LEFT JOIN ".DB_PREFIXE."reunion_categorie
                    ON dossier_instruction_reunion.reunion_type_categorie=reunion_categorie.reunion_categorie                
                LEFT JOIN ".DB_PREFIXE."reunion
                    ON dossier_instruction_reunion.reunion=reunion.reunion
                LEFT JOIN ".DB_PREFIXE."acteur
                    ON dossier_instruction.technicien=acteur.acteur
        WHERE
            reunion.date_reunion >= CURRENT_DATE
            AND reunion.date_reunion <= CURRENT_DATE+30 
        ";
        if ($this->get_filter_mode() == "by_acteur") {
            $query .= " AND acteur.acteur=".$this->get_acteur()." ";
        }
        $query .= "
        GROUP BY
            reunion.reunion,
            reunion.date_reunion,
            dossier_instruction_reunion.reunion_type_categorie,
            reunion_categorie.libelle,
            dossier_type.code,
            dossier_instruction.dossier_instruction,
            dossier_instruction.libelle,
            etablissement.code,
            etablissement.libelle
        ORDER BY
            reunion.date_reunion
        ";
        $results = $this->get_db_query_all($query);
        if (count($results) == 0) {
            echo "Aucun dossier planifié dans les 30 jours à venir.";
        }
        $reunion = -1;
        $categorie = -1;
        foreach ($results as $key => $value) {
            //
            if ($reunion != $value["reunion"]) {
                echo "<strong>";
                echo $this->f->formatDate($value["date_reunion"])." - ".$value["reunion_libelle"];
                echo "</strong>";
                echo "<br/>";
                $categorie = -1;
                $reunion = $value["reunion"];
            }
            //
            if ($categorie != $value["reunion_type_categorie"]) {
                echo "<u>";
                echo $value["reunion_categorie_libelle"];
                echo "</u><br/>";
                $categorie = $value["reunion_type_categorie"];
            }
            //
            $href = "";
            if ($value["dossier_type_code"] == "VISIT") {
                $href = sprintf(
                    "%s&obj=dossier_instruction_mes_visites&action=3&idx=%s",
                    OM_ROUTE_FORM,
                    $value["dossier_instruction"]
                );
            } elseif ($value["dossier_type_code"] == "PLAN") {
                $href = sprintf(
                    "%s&obj=dossier_instruction_mes_plans&action=3&idx=%s",
                    OM_ROUTE_FORM,
                    $value["dossier_instruction"]
                );
            }
            $link = "<li>";
            if ($href != "") {
                $link .= sprintf(
                    '<a href="%s">',
                    $href
                );
            }
            $link .= $value["dossier_instruction_libelle"]." (".$value["etablissement_libelle"].")";
            if ($href != "") {
                $link .= sprintf(
                    '</a>'
                );
            }
            $link .= "</li>";//<br/>";
            echo $link;
        }
        return $widget_is_empty;
    }

    /**
     * VIEW - view_widget_direct_search.
     *
     * Recherche directe d'établissement.
     *
     * Affiche un champ de recherche autofocus permettant l'accès direct
     * à une fiche d'établissement par la saisie de son code. Si le code
     * n'existe pas la valeur est postée dans le formulaire de recherche
     * avancée de tous les établissements.
     *
     * @return boolean
     */
    public function view_widget_direct_search($content = null) {
        if ($this->f->isAccredited(array(
            "etablissement_tous",
            "etablissement_tous_consulter",
            "etablissement_tous_tab", ), "OR") !== true) {
            //
            return true;
        }
        //
        $filtericon = sprintf(
            '<div class="filter-icon"><svg xmlns="http://www.w3.org/2000/svg" width="24px" viewBox="0 0 24 24" height="24px"><path d="M15.5 14h-.79l-.28-.27C15.41 12.59 16 11.11 16 9.5 16 5.91 13.09 3 9.5 3S3 5.91 3 9.5 5.91 16 9.5 16c1.61 0 3.09-.59 4.23-1.57l.27.28v.79l5 4.99L20.49 19l-4.99-5zm-6 0C7.01 14 5 11.99 5 9.5S7.01 5 9.5 5 14 7.01 14 9.5 11.99 14 9.5 14z"/><path fill="none" d="M0 0h24v24H0z"/></svg></div>'
        );
        printf('
        <form id="direct-search" action="%1$s" class="styled" method="post">
            <input type="text" class="text-input om-autofocus" id="filter" autocomplete="off" value="" placeholder="%3$s12345 ou 12345" name="filter" />
            <button type="submit">%2$s</button>
        </form>',
            OM_ROUTE_FORM."&obj=etablissement_tous&action=35",
            $filtericon,
            trim($this->f->getParameter("etablissement_code_prefixe"))
        );
        return false;
    }

    /**
     * VIEW - view_widget_mes_infos.
     *
     * Mes infos.
     *
     * Informations sur l'utilisateur connecté : profil, service auquel il 
     * appartient, acteur rattaché, ...
     *
     * @return boolean
     */
    public function view_widget_mes_infos($content = null) {
        //
        $user_infos = $this->f->retrieveUserProfile($_SESSION["login"]);
        //
        $sql = "
        SELECT acteur
        FROM ".DB_PREFIXE."acteur 
            LEFT JOIN ".DB_PREFIXE."om_utilisateur
                ON  acteur.om_utilisateur=om_utilisateur.om_utilisateur
        WHERE om_utilisateur.login='".$_SESSION["login"]."'
        ";
        $acteur = $this->get_db_query_one($sql);
        $this->set_acteur($acteur);
        //
        $service = "";
        if (!is_null($_SESSION["service"])) {
            //
            $this->set_service($_SESSION["service"]);
            $service = $this->get_service();
        }
        //
        $acteur = $this->get_acteur_instance($acteur);
        $service = $this->get_service_instance($service);
        //
        $template_mes_infos = '
<div class="row">
    <div class="col-md-6">
        <p><strong>login :</strong> %s</p>
        <p><strong>profil :</strong> %s</p>
    </div>
    <div class="col-md-6">
        <p><strong>service :</strong> %s (%s)</p>
        <p><strong>acteur :</strong> %s (%s)</p>
        <p><strong>role :</strong> %s</p>
    </div>
</div>
';
        printf(
            $template_mes_infos,
            $_SESSION["login"],
            $user_infos["libelle"],
            $service->getVal("libelle"),
            $service->getVal("code"),
            $acteur->getVal("nom_prenom"),
            $acteur->getVal("acronyme"),
            $acteur->getVal("role")
        );
        return false;
    }

    /**
     * VIEW - view_widget_mon_activite.
     *
     * Mon activité.
     *
     * L'objet de ce widget est de présenter les statistiques de l'utilisateur
     * connecté.
     *
     * @return boolean
     */
    public function view_widget_mon_activite($content = null) {
        $widget_is_empty = false;
        /**
         * Gestion du filtre sur l'acteur
         */
        //
        $this->set_filter_mode("by_acteur");
        $sql = "
        SELECT acteur
        FROM ".DB_PREFIXE."acteur 
            LEFT JOIN ".DB_PREFIXE."om_utilisateur
                ON  acteur.om_utilisateur=om_utilisateur.om_utilisateur
        WHERE om_utilisateur.login='".$_SESSION["login"]."'
        ";
        $acteur = $this->get_db_query_one($sql);
        $this->set_acteur($acteur);
        //
        if (!is_null($_SESSION["service"])) {
            //
            $this->set_service($_SESSION["service"]);
        }
        //
        if ($this->get_acteur() == -1 || $this->get_service() == -1) {
            echo __("Aucune activité");
            return;
        }
        /**
         * Affichage des chiffres
         */
        //
        $panel = "";
        //
        $panel_elems = "";
        $panel_elems .= $this->sprint_stat_numberby("nb-visites-programmees");
        $panel_elems .= $this->sprint_stat_numberby("nb-visites-realisees");
        //
        $panel .= sprintf(
            $this->template_panel_list_by_2,
            $panel_elems
        );
        //
        $panel_elems = "";
        $panel_elems .= $this->sprint_stat_numberby("nb-di-encours");
        $panel_elems .= $this->sprint_stat_numberby("nb-di-plan-etudies");
        $panel_elems .= $this->sprint_stat_numberby("delai-moyen-instruction-dossier");
        //
        $panel .= sprintf(
            $this->template_panel_list_by_3,
            $panel_elems
        );
        //
        $panel_elems = "";
        $panel_elems .= $this->sprint_stat_numberby("nb-avis-favorables");
        $panel_elems .= $this->sprint_stat_numberby("nb-avis-defavorables");
        //
        $panel .= sprintf(
            $this->template_panel_list_by_2,
            $panel_elems
        );
        //
        printf($panel);
        return $widget_is_empty;
    }

    /**
     *
     * @return boolean
     */
    protected function common_widget_analyses($content = null, $mode = null) {
        /**
         * Ce widget est configurable via l'interface Web. Lors de la création
         * du widget dans le paramétrage il est possible de spécifier la ou les
         * options suivantes :
         *
         * - listing :
         *    = false
         *    = true
         *    (defaut) = true. Par défaut le listing est affiché.
         *
         * - entries :
         *    = 5
         *    = all
         *    (defaut) = all. Par défaut le listing affiche tous les enregsitrements.
         *
         * - hide_if_empty :
         *    = false
         *    = true
         *    (defaut) = true. Par défaut le widget est caché si aucun enregistrement.
         */
        $arguments = $this->get_arguments(
            $content,
            array("listing", "entries", "hide_if_empty", )
        );
        if (array_key_exists("listing", $arguments) === true
            && $arguments["listing"] === "false") {
            //
            $arguments__listing = false;
        } else {
            $arguments__listing = true;
        }
        if (array_key_exists("entries", $arguments) === true
            && is_numeric($arguments["entries"]) === true) {
            //
            $arguments__entries = intval($arguments["entries"]);
        } else {
            $arguments__entries = "all";
        }
        if (array_key_exists("hide_if_empty", $arguments) === true
            && $arguments["hide_if_empty"] === "false") {
            //
            $arguments__hide_if_empty = false;
        } else {
            $arguments__hide_if_empty = true;
        }

        // Si l'utilisateur n'a pas les permissions nécessaires, le widget
        // n'est pas affiché.
        if ($this->f->isAccredited(array(
                "dossier_instruction",
                "dossier_instruction_tab", ), "OR") !== true) {
            // Le widget est vide.
            return true;
        }
        //
        if ($mode == "a_valider") {
            $description_no_content = __("Aucune analyse à valider.");
            $description_entries = __("Toutes les analyses à valider.");
            if ($arguments__entries !== "all") {
                if ($arguments__entries == 1) {
                    $description_entries = sprintf(__("La plus ancienne analyse à valider."));
                } else {
                    $description_entries = sprintf(__("Les %s plus anciennes analyses à valider."), $arguments__entries);
                }
            }
            $description_listing = __("Une analyse à valider c'est une analyse dans l'état 'terminée' sur un DI non clôturé. Elle est en attente de validation, étape nécessaire avant de pouvoir générer son PV.");
            $description_see_more = __("Voir toutes les analyses à valider");
            $label_see_more_singular = __("analyse à valider");
            $label_see_more_plural = __("analyses à valider");
            $analyses_etat = "termine";
        } elseif ($mode == "a_acter") {
            $description_no_content = __("Aucune analyse à acter.");
            $description_entries = __("Toutes les analyses à acter.");
            if ($arguments__entries !== "all") {
                if ($arguments__entries == 1) {
                    $description_entries = sprintf(__("La plus ancienne analyse à acter."));
                } else {
                    $description_entries = sprintf(__("Les %s plus anciennes analyses à acter."), $arguments__entries);
                }
            }
            $description_listing = __("Une analyse à acter c'est une analyse dans l'état 'validée' sur un DI non clôturé. Elle est en attente de génération de son PV.");
            $description_see_more = __("Voir toutes les analyses à acter");            
            $label_see_more_singular = __("analyse à acter");
            $label_see_more_plural = __("analyses à acter");
            $analyses_etat = "valide";
        } else {
            // Le widget est vide.
            return true;
        }
        $href_see_more = OM_ROUTE_FORM."&obj=dossier_instruction&idx=0&action=601&analyses_etat=".$analyses_etat."&dossier_cloture=false";

        // Composition des clauses FROM et WHERE communes aux deux requêtes
        // (celle du listing et celle du compteur).
        $query_from_where = sprintf(
            '
            FROM %1$sanalyses
                LEFT JOIN %1$sanalyses_type
                    ON analyses.analyses_type = analyses_type.analyses_type
                LEFT JOIN %1$sdossier_instruction
                    ON dossier_instruction.dossier_instruction = analyses.dossier_instruction
                LEFT JOIN %1$sdossier_coordination
                    ON dossier_instruction.dossier_coordination = dossier_coordination.dossier_coordination
                LEFT JOIN %1$setablissement
                    ON dossier_coordination.etablissement = etablissement.etablissement
            WHERE 
                dossier_instruction.dossier_cloture is FALSE
                AND analyses.analyses_etat = \'%2$s\'
                %3$s
            ',
            DB_PREFIXE,
            $analyses_etat,
            (!is_null($_SESSION['service']) ? " AND analyses.service=".$_SESSION['service']." " : "")
        );

        // Récupération du nombre d'enregistrements
        $count = $this->get_db_query_one(sprintf(
            'SELECT
                count(*)
            %1$s
            ',
            $query_from_where
        ));

        // Si il n'y a pas d'enregistrements et que le paramétrage dit que le
        // widget doit être caché quand c'est le cas, alors on retourne la
        // réponse à la question "le widget est vide ?"
        if ($count == 0 && $arguments__hide_if_empty === true) {
            return true;
        }

        // Affichage du compteur d'enregistrements
        $panel = "";
        $panel_elem = sprintf(
            $this->template_panel_elem_box,
            "rounded bg-danger counter-analyses-".$mode,
            $count,
            $this->sprint_link(array(
                "href" => $href_see_more,
                "title" => $description_listing,
                "class" => "counter-link-analyses-".$mode,
                "libelle" => ($count > 1 ? $label_see_more_plural : $label_see_more_singular),
            ))
        );
        $panel .= sprintf(
            $this->template_panel,
            $panel_elem
        );
        echo $panel;

        // Récupération de toutes les analyses dans l'état choisi sur des DI non clôturés
        $results = $this->get_db_query_all(sprintf(
            'SELECT
                dossier_instruction.dossier_instruction as id_di,
                CASE
                    WHEN dossier_coordination.etablissement IS NULL
                        THEN
                            dossier_instruction.libelle
                    ELSE
                        CONCAT(
                            etablissement.code, \' - \', etablissement.libelle, 
                            \'<br/>\', dossier_instruction.libelle
                        )
                END as etab_di_libelle,
                analyses.objet as objet_analyse,
                analyses.analyses_etat as etat_libelle
            %1$s
            ORDER BY
                dossier_coordination.date_demande ASC
            %2$s
            ',
            $query_from_where,
            ($arguments__entries !== "all" ? "LIMIT ".$arguments__entries : "")
        ));
        // Définition des attributs du tableau
        $results["attr"] = array(
            // On définit les entêtes de colonnes
            "head" => array(
                $this->sprint_icon_info($description_entries." ".$description_listing),
                __("Etablissement / DI"),
                __("Objet"),
                __("État"),
            ),
            // On stocke le nombre de ligne résultats de la requête
            "count" => count($results),
        );
        // Les lignes résultat ne contiennent aucun lien
        // On boucle donc sur le tableau pour rajouter les liens vers
        // les éléments
        foreach ($results as $key_line => $line) {
            //
            if ($key_line === "attr") {
                continue;
            }
            // Le lien est identique pour chaque élément de la ligne
            $href = sprintf(
                '%s&direct_idx=%s',
                $href_see_more,
                $line["id_di"]
            );
            // On remplace le contenu de chaque cellule par le lien
            foreach ($line as $key_cell => $cell) {
                //
                if ($key_cell == "etat_libelle") {
                    $cell = sprintf(
                        '<span class="label label-warning">%s</span>',
                        $cell
                    );
                } elseif ($key_cell == "id_di") {
                    $cell = "->";
                }
                //
                $results[$key_line][$key_cell] = $this->sprint_link(array(
                    "href" => $href,
                    "libelle" => $cell,
                ));
            }
        }
        // La dernière ligne du tableau permet d'afficher qu'il n'y a aucun
        // résultat ou d'afficher un lien vers le listing complet des éléments 
        // en question
        if ($count == 0) {
            $results[] = array(
                "lastline" => array(
                    "content" => $description_no_content,
                    "colspan" => count($results["attr"]["head"]),
                ),
            );
        } else {
            if ($arguments__entries !== "all") {
                $results[] = array(
                    "lastline" => array(
                        "content" => $this->sprint_link(array(
                            "href" => $href_see_more,
                            "libelle" => "-> ".$description_see_more,
                        )),
                        "colspan" => count($results["attr"]["head"]),
                    ),
                );
            }
        }
        // Affichage du listing d'enregistrements
        echo $this->sprint_table($results);
        // Le widget n'est pas vide.
        return false;
    }

    /**
     * VIEW - view_widget_analyses_a_valider.
     *
     * L'objet de ce widget est de permettre au cadre de visualiser les 
     * analyses terminées et donc à valider.
     *
     * @return boolean
     */
    public function view_widget_analyses_a_valider($content = null) { 
        return $this->common_widget_analyses($content, "a_valider");
    }

    /**
     * VIEW - view_widget_analyses_a_acter.
     *
     * L'objet de ce widget est de permettre au cadre de visualiser les 
     * analyses validées et donc à acter.
     *
     * @return boolean
     */
    public function view_widget_analyses_a_acter($content = null) {
        return $this->common_widget_analyses($content, "a_acter");
    }

    /**
     * @return boolean
     */
    protected function common_widget_programmations($content = null, $mode = null) {
        // Si l'utilisateur n'a pas les permissions nécessaires, le widget
        // n'est pas affiché.
        if ($this->f->isAccredited(array(
                "programmation",
                "programmation_tab", ), "OR") !== true) {
            // Le widget est vide.
            return true;
        }
        //
        $widget_is_empty = false;
        //
        if ($mode == "programmations_urgentes") {
            //
            $description_listing = __("Les programmations arrivant dans moins ".
            "de 3 semaines pour lesquelles les envois de convocations ne ".
            "sont pas termines.");
            //
            $filter_sql = sprintf(
                '
                (
                    programmation.convocation_exploitants != \'envoyees\'
                    OR programmation.convocation_membres != \'envoyees\'
                    OR programmation.convocation_membres IS NULL
                    OR programmation.convocation_exploitants IS NULL
                )
                AND (
                    (to_date(concat(programmation.annee, \'-\', programmation.numero_semaine), \'IYYY-IW\') >= now() - interval \'7 days\')
                    AND (to_date(concat(programmation.annee, \'-\', programmation.numero_semaine), \'IYYY-IW\') <= now() + interval \'21 days\')
                )
                '
            );
            //
            $description_no_content = __("Aucune programmation urgente.");
        } elseif ($mode == "programmations_a_valider") {
            //
            $description_listing = __("Les programmations à valider.");
            //
            $filter_sql = " 
            lower(programmation_etat.code) = 'fin' ";
            //
            $description_no_content = __("Aucune programmation à valider.");
        } elseif ($mode == "convocations_exploitants_a_envoyer") {
            //
            $description_listing = __("Les programmations validees pour lesquelles ".
            "les envois de convocations exploitants ne sont ".
            "pas termines.");
            //
            $filter_sql = sprintf(
                '
                (
                    programmation.convocation_exploitants != \'envoyees\'
                    OR programmation.convocation_exploitants IS NULL
                )
                AND
                (
                    (to_date(concat(programmation.annee, \'-\', programmation.numero_semaine), \'IYYY-IW\') >= now() - interval \'7 days\')
                )
                AND lower(programmation_etat.code) = \'val\'
                '
            );
            //
            $description_no_content = __("Aucune programmation avec convocations exploitants a envoyer.");
        } elseif ($mode == "convocations_membres_a_envoyer") {
            //
            $description_listing = __("Les programmations validees arrivant ".
            "dans moins de 2 semaines pour lesquelles ".
            "les envois de convocations membres ne sont ".
            "pas termines.");
            //
            $filter_sql = sprintf(
                '
                (
                    programmation.convocation_membres != \'envoyees\'
                    OR programmation.convocation_membres IS NULL
                )
                AND
                (
                    (to_date(concat(programmation.annee, \'-\', programmation.numero_semaine), \'IYYY-IW\') >= now() - interval \'7 days\')
                    AND (to_date(concat(programmation.annee, \'-\', programmation.numero_semaine), \'IYYY-IW\') <= now() + interval \'14 days\')
                )
                AND lower(programmation_etat.code) = \'val\'
                '
            );
            //
            $description_no_content = __("Aucune programmation avec convocations membres a envoyer.");
        } else {
            return false;
        }

        /**
         * Listing de toutes les programmations urgentes
         */
        // Requête SQL
        $sql = "
        SELECT
            programmation.programmation,
            programmation.annee,
            programmation.numero_semaine,
            CONCAT('V', programmation.version) as version,
            programmation_etat.libelle
        FROM
            ".DB_PREFIXE."programmation
                LEFT JOIN ".DB_PREFIXE."programmation_etat
                    ON programmation.programmation_etat = programmation_etat.programmation_etat
        WHERE ";
        if (!is_null($_SESSION['service'])) {
            $sql .= "
                  programmation.service = ".$_SESSION['service']." AND 
            ";
        }
        $sql .= $filter_sql;
        $sql .= "
        ORDER BY annee ASC, numero_semaine ASC
        ";
        // Exécution de la requête et récupération des lignes résultats de la requête
        $results = $this->get_db_query_all($sql);
        // Définition des attributs du tableau
        $results["attr"] = array(
            // On définit les entêtes de colonnes
            "head" => array(
                $this->sprint_icon_info($description_listing),
                __("Semaine"),
                __("Version"),
                __("Etat")
            ),
            // On stocke le nombre de ligne résultats de la requête
            "count" => count($results),
        );
        // Les lignes résultat ne contiennent aucun lien
        // On boucle donc sur le tableau pour rajouter les liens vers
        // les éléments
        foreach ($results as $key_line => $line) {
            //
            if ($key_line === "attr") {
                continue;
            }
            // Le lien est identique pour chaque élément de la ligne
            $href = sprintf(
                '%s&obj=programmation&amp;action=3&amp;idx=%s',
                OM_ROUTE_FORM,
                $line["programmation"]
            );
            // Initialisation des variables
            $annee = "";
            // On remplace le contenu de chaque cellule par le lien
            foreach ($line as $key_cell => $cell) {
                //
                if ($key_cell == "libelle") {
                    $cell = sprintf(
                        '<span style="font-size:14px;" class="label label-warning">%s</span>',
                        $cell
                    );
                } elseif ($key_cell == "annee"){
                    $annee = $cell;
                    continue;
                }elseif ($key_cell == "numero_semaine"){
                    // Si la semaine et l'année sont fournies
                    if (!empty($annee)&&!empty($cell)){
                        //
                        $lundi = new DateTime();
                        $lundi->setISODate($annee,$cell,1);
                        //
                        $dimanche = new DateTime();
                        $dimanche->setISODate($annee,$cell,7);

                        $cell = __("N°").$cell." du ".$lundi->format('d/m/Y')." au ".$dimanche->format('d/m/Y');
                    }
                    else {
                        $cell = "";
                    }
                    unset($results[$key_line]["annee"]);
                }elseif ($key_cell == "programmation") {
                    $cell = "->";
                }
                //
                $results[$key_line][$key_cell] = $this->sprint_link(array(
                    "href" => $href,
                    "libelle" =>  $cell,
                ));
            }
        }
        // La dernière ligne du tableau permet d'afficher qu'il n'y a aucun
        // résultat ou d'afficher un lien vers le listing complet des éléments 
        // en question
        if ($results["attr"]["count"] == 0) {
            $results[] = array(
                "lastline" => array(
                    "content" => $description_no_content,
                    "colspan" => count($results["attr"]["head"]),
                ),
            );
            // On cache le widget si aucun résultat
            $widget_is_empty = true;
        }

        /**
         *
         */
        //
        echo $this->sprint_table($results);
        //
        return $widget_is_empty;
    }

    /**
     * VIEW - view_widget_convocations_membres_a_envoyer.
     *
     * L'objet de ce widget est de permettre de visualiser la liste des
     * programmations pour laquelle il faut envoyer la convocation aux
     * membres.
     *
     * @return boolean
     */
    public function view_widget_convocations_membres_a_envoyer($content = null) { 
        return $this->common_widget_programmations($content, "convocations_membres_a_envoyer");
    }

    /**
     * VIEW - view_widget_convocations_exploitants_a_envoyer.
     *
     * L'objet de ce widget est de permettre de visualiser la liste des
     * programmations pour laquelle il faut envoyer la convocation aux
     * exploitants.
     *
     * @return boolean
     */
    public function view_widget_convocations_exploitants_a_envoyer($content = null) {
        return $this->common_widget_programmations($content, "convocations_exploitants_a_envoyer");
    }

    /**
     * VIEW - view_widget_programmations_a_valider.
     *
     * L'objet de ce widget est de permettre de visualiser la liste des
     * programmations à valider.
     *
     * @return boolean
     */
    public function view_widget_programmations_a_valider($content = null) {
        return $this->common_widget_programmations($content, "programmations_a_valider");
    }

    /**
     * VIEW - view_widget_programmations_urgentes.
     *
     * L'objet de ce widget est de permettre de visualiser la liste des
     * programmations urgentes.
     *
     * @return boolean
     */
    public function view_widget_programmations_urgentes($content = null) {
        return $this->common_widget_programmations($content, "programmations_urgentes");
    }

    /**
     *
     */
    protected function common_widget_documents_entrants($content = null, $mode = null) {
        //
        if ($mode == "mes_documents_entrants_non_lus") {
            //
            $description_listing = __("Les documents entrants non lus qui se trouvent sur un dossier d'instruction dont l'utilisateur connecté est l'instructeur.");
            //
            $filter_sql = " 
            piece.lu IS FALSE
            AND om_utilisateur.login = '".$_SESSION["login"]."' 
            ";
            $order_by = "
            piece.date_butoir ASC
            ";
            //
            $description_no_content = __("Aucun document entrant non lu.");
            //
            $description_see_more = __("Voir tous mes documents entrants non lus");
            //
            $dest_obj = "dossier_instruction_tous_visites";
            $dest_id = "dossier_instruction";
            $obj_see_more = "piece_non_lu";
            $obj = "piece";
        } elseif ($mode == "documents_entrants_suivis") {
            //
            $description_listing = __("Les documents entrants qui ont le marqueur suivi activé et une date butoir dans le passé ou aucune date butoir.");
            //
            $filter_sql = " 
            piece.suivi IS TRUE
            AND (piece.date_butoir <= NOW() OR piece.date_butoir IS NULL)
            ";
            $order_by = "
            piece.date_butoir ASC
            ";
            //
            $description_no_content = __("Aucun document entrant suivi.");
            //
            $description_see_more = __("Voir tous ces documents entrants");
            //
            $dest_obj = "piece_suivi";
            $dest_id = "piece";
            $obj_see_more = "piece_suivi";
            $obj = "piece_suivi";
        } elseif ($mode == "documents_entrants_a_valider") {
            //
            $description_listing = __("Les documents entrants qualifiés à valider.");
            //
            $filter_sql = " 
            piece_statut.code = 'QUALIF' 
            ";
            $order_by = "
            piece.om_date_creation ASC
            ";
            //
            $description_no_content = __("Aucun document entrant à valider.");
            //
            $description_see_more = __("Voir tous ces documents entrants");
            //
            $dest_obj = "piece_a_valider";
            $dest_id = "piece";
            $obj_see_more = "piece_a_valider";
            $obj = "piece_a_valider";
        } else {
            return false;
        }
        // Si l'utilisateur n'a pas les permissions nécessaires, le widget
        // n'est pas affiché.
        if ($this->f->isAccredited(array(
                $obj,
                $obj."_tab", ), "OR") !== true) {
            // Le widget est vide.
            return true;
        }
        //
        $widget_is_empty = false;
        /**
         * 
         */
        // Requête SQL
        $sql_count = "
        SELECT
            count(*) ";
        $sql = "
        SELECT
            piece.piece,
            dossier_instruction.dossier_instruction,
            piece.nom,
            CASE WHEN dossier_instruction.libelle <> '' 
                    THEN  dossier_instruction.libelle
                    ELSE dossier_coordination.libelle 
                END as di_dc,
            concat(etablissement.code, ' - ', etablissement.libelle) as etablissement_libelle,
            to_char(piece.date_butoir, 'DD/MM/YYYY') as date_butoir ";
        //
        $sql_from = "
        FROM
            ".DB_PREFIXE."piece
            LEFT JOIN ".DB_PREFIXE."dossier_coordination 
                ON piece.dossier_coordination=dossier_coordination.dossier_coordination 
            LEFT JOIN ".DB_PREFIXE."dossier_instruction 
                ON piece.dossier_instruction=dossier_instruction.dossier_instruction 
            LEFT JOIN ".DB_PREFIXE."etablissement 
                ON piece.etablissement=etablissement.etablissement 
            LEFT JOIN ".DB_PREFIXE."piece_statut 
                ON piece.piece_statut=piece_statut.piece_statut 
            LEFT JOIN ".DB_PREFIXE."piece_type 
                ON piece.piece_type=piece_type.piece_type 
            LEFT JOIN ".DB_PREFIXE."acteur
                ON dossier_instruction.technicien = acteur.acteur
            LEFT JOIN ".DB_PREFIXE."om_utilisateur
                ON acteur.om_utilisateur = om_utilisateur.om_utilisateur ";
        $sql .= $sql_from;
        $sql_count .= $sql_from;
        //
        $sql_where = "
        WHERE ";
        if (!is_null($_SESSION['service'])) {
            $sql_where .= "
                piece.service = ".$_SESSION['service']." AND 
            ";
        }
        $sql_where .= $filter_sql;
        $sql .= $sql_where;
        $sql_count .= $sql_where;
        //
        $sql .= " ORDER BY ".$order_by." LIMIT 5 ";
        // Exécution de la requête et récupération des lignes résultats de la requête
        $results = $this->get_db_query_all($sql);
        //
        $count = $this->get_db_query_one($sql_count);
        // Définition des attributs du tableau
        $results["attr"] = array(
            // On définit les entêtes de colonnes
            "head" => array(
                $this->sprint_icon_info($description_listing),
                __("Nom"),
                __("DI ou DC"),
                __("Etablissement"),
                __("Date Butoir")
            ),
            // On stocke le nombre de ligne résultats de la requête
            "count" => count($results),
        );
        // Les lignes résultat ne contiennent aucun lien
        // On boucle donc sur le tableau pour rajouter les liens vers
        // les éléments
        foreach ($results as $key_line => $line) {
            //
            if ($key_line === "attr") {
                continue;
            }
            // Le lien est identique pour chaque élément de la ligne
            $href = sprintf(
                '%s&obj=%s&amp;action=3&amp;idx=%s',
                OM_ROUTE_FORM,
                $dest_obj,
                $line[$dest_id]
            );
            // Initialisation des variables
            $annee = "";
            // On remplace le contenu de chaque cellule par le lien
            foreach ($line as $key_cell => $cell) {
                //
                if ($key_cell == "di") {
                    $cell = sprintf(
                        '<span style="font-size:14px;" class="label label-warning">%s</span>',
                        $cell
                    );
                } elseif ($key_cell == "dossier_instruction") {
                    unset($results[$key_line][$key_cell]);
                    continue;
                } elseif ($key_cell == "annee"){
                    $annee = $cell;
                    continue;
                } elseif ($key_cell == "piece") {
                    $cell = "->";
                }
                //
                $results[$key_line][$key_cell] = $this->sprint_link(array(
                    "href" => $href,
                    "libelle" =>  $cell,
                ));
            }
        }
        // La dernière ligne du tableau permet d'afficher qu'il n'y a aucun
        // résultat ou d'afficher un lien vers le listing complet des éléments 
        // en question
        if ($results["attr"]["count"] == 0) {
            $last_line_content = $description_no_content;
        } else {
            $last_line_content = $this->sprint_link(array(
                "href" => OM_ROUTE_TAB."&obj=".$obj_see_more,
                "libelle" => "-> ".$description_see_more,
            ));
        }
        $results[] = array(
            "lastline" => array(
                "content" => $last_line_content,
                "colspan" => count($results["attr"]["head"]),
            ),
        );

        /**
         *
         */
        $panel = "";
        $panel_elem = sprintf(
            $this->template_panel_elem_box,
            "bg-info",
            $count,
            __("En cours")
        );
        //
        $panel .= sprintf(
            $this->template_panel,
            $panel_elem
        );
        echo $panel;
        //
        //
        echo $this->sprint_table($results);
        //
        return $widget_is_empty;
    }

    /**
     * VIEW - view_widget_mes_documents_entrants_non_lus.
     *
     * @return boolean
     */
    public function view_widget_mes_documents_entrants_non_lus($content = null) { 
        return $this->common_widget_documents_entrants($content, "mes_documents_entrants_non_lus");
    }

    /**
     * VIEW - view_widget_documents_entrants_suivis.
     *
     * @return boolean
     */
    public function view_widget_documents_entrants_suivis($content = null) {
        return $this->common_widget_documents_entrants($content, "documents_entrants_suivis");
    }

    /**
     * VIEW - view_widget_documents_entrants_a_valider.
     *
     * @return boolean
     */
    public function view_widget_documents_entrants_a_valider($content = null) {
        return $this->common_widget_documents_entrants($content, "documents_entrants_a_valider");
    }


    /**
     * @return boolean
     */
    protected function common_widget_documents_generes($content = null, $mode = null) {
        //
        if ($mode == "documents_generes_a_editer") {
            //
            $description_listing = __("Les documents générés finalisés, dont les dates d'envoi et de retour de signature ne sont pas saisies et dont les dates d'envoi et de retour AR ne sont pas saisies.");
            // CONDITION
            $filter_sql = " 
            courrier.finalise IS TRUE
            AND courrier.date_envoi_signature IS NULL
            AND courrier.date_retour_signature IS NULL
            AND courrier.date_envoi_rar IS NULL
            AND courrier.date_retour_rar IS NULL
            ";
            //
            $description_see_more = __("Voir tous les documents générés à editer");
            //
            $obj_see_more = "courrier_a_editer";

        } elseif ($mode == "documents_generes_attente_signature") {
            //
            $description_listing = __("Les documents générés finalisés, dont la date d'envoi signature est saisie et dont la date de retour de signature et les dates d'envoi et de retour AR ne sont pas saisies.");
            // CONDITION
            $filter_sql = " 
            courrier.finalise IS TRUE
            AND courrier.date_envoi_signature IS NOT NULL
            AND courrier.date_retour_signature IS NULL
            AND courrier.date_envoi_rar IS NULL
            AND courrier.date_retour_rar IS NULL
            ";
            //
            $description_see_more = __("Voir tous les documents générés en attente de signature");
            //
            $obj_see_more = "courrier_attente_signature";

        } elseif ($mode == "documents_generes_attente_retour_ar") {
            //
            $description_listing = __("Les documents générés finalisés, dont la date de retour signature ou la date d'envoi AR sont saisies et dont la date de retour AR n'est pas saisie.");
            // CONDITION
            $filter_sql = " 
            courrier.finalise IS TRUE
            AND courrier.date_envoi_rar IS NOT NULL
            AND courrier.date_retour_rar IS NULL ";
            //
            $description_see_more = __("Voir tous les documents générés en attente de retour AR");
            //
            $obj_see_more = "courrier_attente_retour_ar";

        } else {
            return false;
        }
        // Si l'utilisateur n'a pas les permissions nécessaires, le widget
        // n'est pas affiché.
        if ($this->f->isAccredited(array(
                $obj_see_more,
                $obj_see_more."_tab", ), "OR") !== true) {
            // Le widget est vide.
            return true;
        }
        //
        $widget_is_empty = false;
        /**
         * 
         */
        // Requête SQL
        $sql_count = "
        SELECT
            count(*) ";
        // FROM
        $sql_from = "
        FROM ".DB_PREFIXE."courrier
        LEFT JOIN ".DB_PREFIXE."courrier as courrier0 
            ON courrier.courrier_joint=courrier0.courrier 
        LEFT JOIN ".DB_PREFIXE."courrier as courrier1 
            ON courrier.courrier_parent=courrier1.courrier 
        LEFT JOIN ".DB_PREFIXE."courrier_type 
            ON courrier.courrier_type=courrier_type.courrier_type 
        LEFT JOIN ".DB_PREFIXE."dossier_coordination 
            ON courrier.dossier_coordination=dossier_coordination.dossier_coordination 
        LEFT JOIN ".DB_PREFIXE."dossier_instruction 
            ON courrier.dossier_instruction=dossier_instruction.dossier_instruction 
        LEFT JOIN ".DB_PREFIXE."etablissement 
            ON courrier.etablissement=etablissement.etablissement 
        LEFT JOIN ".DB_PREFIXE."modele_edition 
            ON courrier.modele_edition=modele_edition.modele_edition 
        LEFT JOIN ".DB_PREFIXE."proces_verbal 
            ON courrier.proces_verbal=proces_verbal.proces_verbal 
        LEFT JOIN ".DB_PREFIXE."signataire 
            ON courrier.signataire=signataire.signataire 
        LEFT JOIN ".DB_PREFIXE."visite 
            ON courrier.visite=visite.visite";
        $sql_count .= $sql_from;
        // CONDITION
        $sql_where = "
        WHERE ";
        // Filtre par service
        if (!is_null($_SESSION['service'])) {
            $sql_where .= "
                  (dossier_instruction.service = ".$_SESSION['service']."  
                    OR dossier_instruction.service is NULL) AND 
            ";
        }
        //
        $sql_where .= $filter_sql;
        $sql_count .= $sql_where;
        //
        $count = $this->get_db_query_one($sql_count);

        // S'il n'y a pas de résultat, le widget ne s'affiche pas
        if ($count == 0) {
            $widget_is_empty = true;
        }

        // Affichage
        printf(
            $this->template_panel,
            sprintf(
                $this->template_panel_elem_box,
                "bg-info",
                $count,
                __("En cours")
            )
        );
        // Dernière ligne du widget est un lien vers le listing
        echo $this->sprint_link(array(
            "href" => OM_ROUTE_TAB."&obj=".$obj_see_more,
            "libelle" => "-> ".$description_see_more,
        ));
        //
        return $widget_is_empty;
    }

    /**
     * VIEW - view_widget_documents_generes_a_editer.
     *
     * @return boolean
     */
    public function view_widget_documents_generes_a_editer($content = null) { 
        return $this->common_widget_documents_generes($content, "documents_generes_a_editer");
    }

    /**
     * VIEW - view_widget_documents_generes_attente_signature.
     *
     * @return boolean
     */
    public function view_widget_documents_generes_attente_signature($content = null) { 
        return $this->common_widget_documents_generes($content, "documents_generes_attente_signature");
    }

    /**
     * VIEW - view_widget_documents_generes_attente_retour_ar.
     *
     * @return boolean
     */
    public function view_widget_documents_generes_attente_retour_ar($content = null) { 
        return $this->common_widget_documents_generes($content, "documents_generes_attente_retour_ar");
    }


    /**
     *
     */
    function get_config_message_mes_non_lu($params = array()) {
        //
        if (!is_null($_SESSION["service"])) {
            $this->set_service($_SESSION["service"]);
        }
        //
        $arguments = array();
        //
        $dossier_instruction_id = -1;
        if (isset($params["dossier_instruction_id"])) {
            $dossier_instruction_id = $params["dossier_instruction_id"];
        }
        //
        if ($this->f->is_user_with_role_and_service("cadre", "si") === true) {
            //
            $message_help = __("Les messages sur les DC avec le marqueur de lecture cadre SI à non lu et les messages sur les DI dont je suis le technicien affecté avec le marqueur de lecture tech SI à non lu. Le lien amène vers le message dans le contexte de son DC.");
            //
            $arguments["dest_obj"] = "dossier_coordination";
            $arguments["dest_obj_abrege"] = "dc";
            $arguments["dest_obj_title"] = __("DC");
            $arguments["dest_id"] = "dossier_coordination";
            //
            $query_ct_where = "
            (
                dossier_coordination_message.si_cadre_lu is false
                AND (dossier_coordination_message.si_mode_lecture = 'mode1'
                    OR dossier_coordination_message.si_mode_lecture = 'mode3')
            ) OR (
                dossier_coordination_message.si_technicien_lu is false
                AND om_utilisateur.login = '".$_SESSION["login"]."'
                AND (dossier_coordination_message.si_mode_lecture = 'mode2'
                    OR dossier_coordination_message.si_mode_lecture = 'mode3')
            )
            ";
        } elseif ($this->f->is_user_with_role_and_service("cadre", "acc") === true) {
            //
            $message_help = __("Les messages sur les DC avec le marqueur de lecture cadre ACC à non lu et les messages sur les DI dont je suis le technicien affecté avec le marqueur de lecture tech ACC à non lu. Le lien amène vers le message dans le contexte de son DC.");
            //
            $arguments["dest_obj"] = "dossier_coordination";
            $arguments["dest_obj_abrege"] = "dc";
            $arguments["dest_obj_title"] = __("DC");
            $arguments["dest_id"] = "dossier_coordination";
            //
            $query_ct_where = "
            (
                dossier_coordination_message.acc_cadre_lu is false
                AND (dossier_coordination_message.acc_mode_lecture = 'mode1'
                    OR dossier_coordination_message.acc_mode_lecture = 'mode3')
            ) OR (
                dossier_coordination_message.acc_technicien_lu is false
                AND om_utilisateur.login = '".$_SESSION["login"]."'
                AND (dossier_coordination_message.acc_mode_lecture = 'mode2'
                    OR dossier_coordination_message.acc_mode_lecture = 'mode3')
            )
            ";
        } elseif ($this->f->is_user_with_role_and_service("technicien", "si") === true) {
            //
            $message_help = __("Les messages su les DI dont je suis le technicien affecté avec le marqueur de lecture tech SI à non lu. Le lien amène vers le message dans le contexte de son DI.");
            //
            $arguments["dest_obj"] = "dossier_instruction";
            $arguments["dest_obj_abrege"] = "di";
            $arguments["dest_obj_title"] = __("DI");
            $arguments["dest_id"] = "dossier_instruction";
            //
            $query_ct_where = "
                dossier_coordination_message.si_technicien_lu is false
                AND om_utilisateur.login = '".$_SESSION["login"]."'
                AND (dossier_coordination_message.si_mode_lecture = 'mode2'
                    OR dossier_coordination_message.si_mode_lecture = 'mode3')
            ";
        } elseif ($this->f->is_user_with_role_and_service("technicien", "acc") === true) {
            //
            $message_help = __("Les messages su les DI dont je suis le technicien affecté avec le marqueur de lecture tech ACC à non lu. Le lien amène vers le message dans le contexte de son DI.");
            //
            $arguments["dest_obj"] = "dossier_instruction";
            $arguments["dest_obj_abrege"] = "di";
            $arguments["dest_obj_title"] = __("DI");
            $arguments["dest_id"] = "dossier_instruction";
            //
            $query_ct_where = "
                dossier_coordination_message.acc_technicien_lu is false
                AND om_utilisateur.login = '".$_SESSION["login"]."'
                AND (dossier_coordination_message.acc_mode_lecture = 'mode2'
                    OR dossier_coordination_message.acc_mode_lecture = 'mode3')
            ";
        } else {
            //
            $message_help = "";
            //
            $arguments["dest_obj"] = "dossier_coordination";
            $arguments["dest_obj_abrege"] = "dc";
            $arguments["dest_obj_title"] = __("DC");
            $arguments["dest_id"] = "dossier_coordination";
            //
            $query_ct_where = "
                dossier_coordination_message.dossier_coordination_message = -1
            ";
        }

        if ($this->f->is_option_display_ads_field_in_tables_enabled() !== true) {
            $champaffiche_dossier_ads = array();
            $sql_case_dossier_ads = "";
        } else {
            $champaffiche_dossier_ads = array(
                "CASE 
                WHEN dossier_coordination.dossier_autorisation_ads is null AND dossier_coordination.dossier_instruction_ads is not null THEN dossier_coordination.dossier_instruction_ads
                WHEN dossier_coordination.dossier_autorisation_ads is not null AND dossier_coordination.dossier_instruction_ads is not null THEN dossier_coordination.dossier_instruction_ads
                ELSE dossier_coordination.dossier_autorisation_ads
                END as \"".__("dossier ADS")."\"",
            );
            $sql_case_dossier_ads = $champaffiche_dossier_ads[0].", ";
        }

	// Mise en valeur d'un message important (reconsultation)
        $query_case_reconsultation = "(
            SELECT 
                CASE COUNT(1) WHEN 0 THEN '' ELSE 'rouge' END
            FROM
                ".DB_PREFIXE."dossier_coordination_message autre_msg
            WHERE
                autre_msg.dossier_coordination_message < dossier_coordination_message.dossier_coordination_message
                AND autre_msg.dossier_coordination = dossier_coordination_message.dossier_coordination
                AND autre_msg.type = 'ADS_ERP__PC__CONSULTATION_OFFICIELLE_POUR_AVIS'
                AND dossier_coordination_message.type = 'ADS_ERP__PC__CONSULTATION_OFFICIELLE_POUR_AVIS'
        )";
        $query_ct_select = "
            dossier_coordination_message.dossier_coordination_message,
            dossier_instruction.dossier_instruction,
            dossier_coordination.dossier_coordination,
            ".$arguments["dest_obj"].".libelle as libelle,
            ".$sql_case_dossier_ads."
            concat(etablissement.code, ' - ', etablissement.libelle) as etablissement_libelle,
            dossier_coordination_message.type,
            to_char(dossier_coordination_message.date_emission , 'DD/MM/YYYY HH24:MI:SS') as date,
            ".$query_case_reconsultation." as reconsultation
        ";

        $champaffiche_message_infos = array(
            'dossier_coordination_message.dossier_coordination_message as "'.__("identifiant").'"',
            'to_char(dossier_coordination_message.date_emission ,\'DD/MM/YYYY HH24:MI:SS\') as "'.__("date").'"',
            'dossier_coordination_message.type as "'.__("type").'"',
            'dossier_coordination_message.categorie as "'.__("categorie").'"',
            'dossier_coordination_message.emetteur as "'.__("emetteur").'"',
        );
        $champaffiche_dc_di_libelle = array(
            ''.$arguments["dest_obj"].'.libelle as "'.__($arguments["dest_obj_abrege"]).'"',
        );
        $champaffiche_etablissement = array(
            'CONCAT(etablissement.code,\' - \',etablissement.libelle) as "'.__("etablissement").'"',
        );
        $query_ct_select_champaffiche = array_merge(
            $champaffiche_message_infos,
            $champaffiche_dc_di_libelle,
            $champaffiche_dossier_ads,
            $champaffiche_etablissement
        );
        $query_ct_select_champaffiche[] = $query_case_reconsultation." as reconsultation";

        if ($dossier_instruction_id == -1) {
            $query_ct_from = "
                ".DB_PREFIXE."dossier_coordination_message
                LEFT JOIN ".DB_PREFIXE."dossier_coordination 
                    ON dossier_coordination_message.dossier_coordination=dossier_coordination.dossier_coordination
                LEFT JOIN ".DB_PREFIXE."dossier_instruction 
                    ON dossier_coordination_message.dossier_coordination=dossier_instruction.dossier_coordination 
                    AND (dossier_instruction.service=".$this->get_service()." 
                         OR dossier_instruction.service IS NULL)
                LEFT JOIN ".DB_PREFIXE."etablissement 
                    ON dossier_coordination.etablissement=etablissement.etablissement 
                LEFT JOIN ".DB_PREFIXE."acteur
                    ON dossier_instruction.technicien = acteur.acteur
                LEFT JOIN ".DB_PREFIXE."om_utilisateur
                    ON acteur.om_utilisateur = om_utilisateur.om_utilisateur ";
        } else {
            $query_ct_from = "
                ".DB_PREFIXE."dossier_coordination_message
                LEFT JOIN ".DB_PREFIXE."dossier_coordination 
                    ON dossier_coordination_message.dossier_coordination=dossier_coordination.dossier_coordination
                LEFT JOIN ".DB_PREFIXE."dossier_instruction 
                    ON dossier_coordination_message.dossier_coordination=dossier_instruction.dossier_coordination 
                LEFT JOIN ".DB_PREFIXE."etablissement 
                    ON dossier_coordination.etablissement=etablissement.etablissement 
                LEFT JOIN ".DB_PREFIXE."acteur
                    ON dossier_instruction.technicien = acteur.acteur
                LEFT JOIN ".DB_PREFIXE."om_utilisateur
                    ON acteur.om_utilisateur = om_utilisateur.om_utilisateur ";
        }

        $query_ct_orderby = " dossier_coordination_message.date_emission DESC ";

        /**
         * Return
         */
        //
        return array(
            "arguments" => $arguments,
            "message_help" => $message_help,
            "query_ct_select" => $query_ct_select,
            "query_ct_select_champaffiche" => $query_ct_select_champaffiche,
            "query_ct_from" => $query_ct_from,
            "query_ct_where" => $query_ct_where,
            "query_ct_orderby" => $query_ct_orderby,
            "query_case_reconsultation" => $query_case_reconsultation,
        );
    }


    /**
     *
     */
    function get_config_di_a_qualifier_affecter($params = array()) {
        //
        if (!is_null($_SESSION["service"])) {
            $this->set_filter_mode("by_service");
            $this->set_service($_SESSION["service"]);
        }
        $arguments_filtre_dossier_type = "";
        if (array_key_exists("filtre_dossier_type", $params) === true
            && in_array($params["filtre_dossier_type"], array("PLAN", "VISIT", )) === true) {
            //
            $arguments_filtre_dossier_type = $params["filtre_dossier_type"];
        }
        //
        $message_help = __("Les cinq plus anciens DI à affecter et/ou à qualifier.");
        $description_see_more = __("Voir tous les DI");
        $description_no_content = __("Aucun DI à affecter et/ou à qualifier.");
        $obj_see_more = "dossier_instruction";
        if ($arguments_filtre_dossier_type == "PLAN") {
            $message_help = __("Les cinq plus anciens DI plans à affecter et/ou à qualifier.");
            $description_see_more = __("Voir tous les DI plans");
            $description_no_content = __("Aucun DI plan à affecter et/ou à qualifier.");
            $obj_see_more = "dossier_instruction_tous_plans";
        } elseif ($arguments_filtre_dossier_type == "VISIT") {
            $message_help = __("Les cinq plus anciens DI visites à affecter et/ou à qualifier.");
            $description_see_more = __("Voir tous les DI visites");
            $description_no_content = __("Aucun DI visite à affecter et/ou à qualifier.");
            $obj_see_more = "dossier_instruction_tous_visites";
        }
        //
        $arguments = array();
        //
        $arguments["dest_obj"] = "dossier_instruction";
        $arguments["dest_obj_abrege"] = "di";
        $arguments["dest_obj_title"] = __("DI");
        $arguments["dest_id"] = "dossier_instruction";
        //
        $query_count_a_qualifier = sprintf(
            'SELECT count(*) 
            FROM %1$sdossier_instruction
                LEFT JOIN %1$sdossier_coordination
                    ON dossier_instruction.dossier_coordination=dossier_coordination.dossier_coordination
                LEFT JOIN %1$sdossier_coordination_type
                    ON dossier_coordination.dossier_coordination_type=dossier_coordination_type.dossier_coordination_type
                LEFT JOIN %1$sdossier_type
                    ON dossier_coordination_type.dossier_type=dossier_type.dossier_type
            WHERE dossier_instruction.a_qualifier IS true',
            DB_PREFIXE
        );
        $query_count_affecter = sprintf(
            'SELECT count(*) 
            FROM %1$sdossier_instruction
                LEFT JOIN %1$sdossier_coordination
                    ON dossier_instruction.dossier_coordination=dossier_coordination.dossier_coordination
                LEFT JOIN %1$sdossier_coordination_type
                    ON dossier_coordination.dossier_coordination_type=dossier_coordination_type.dossier_coordination_type
                LEFT JOIN %1$sdossier_type
                    ON dossier_coordination_type.dossier_type=dossier_type.dossier_type
            WHERE dossier_instruction.technicien IS NULL ',
            DB_PREFIXE
        );
        //
        $query_ct_where_a_qualifier = " dossier_instruction.a_qualifier = TRUE ";
        $query_ct_where_a_affecter = " dossier_instruction.technicien IS NULL ";
        $query_ct_where = sprintf(
            ' (%s OR %s) ',
            $query_ct_where_a_qualifier,
            $query_ct_where_a_affecter
        );
        //
        if ($arguments_filtre_dossier_type !== "") {
            $query_count_a_qualifier .= " AND dossier_type.code='".$this->f->db->escapeSimple($arguments_filtre_dossier_type)."' ";
            $query_count_affecter .= " AND dossier_type.code='".$this->f->db->escapeSimple($arguments_filtre_dossier_type)."' ";
            $query_ct_where .= " AND dossier_type.code='".$this->f->db->escapeSimple($arguments_filtre_dossier_type)."' ";
        }
        //
        if ($this->get_filter_mode() == "by_service") {
            $query_count_a_qualifier .= " AND dossier_instruction.service=".$this->get_service()." ";
            $query_count_affecter .= " AND dossier_instruction.service=".$this->get_service()." ";
            $query_ct_where .= " AND dossier_instruction.service=".$this->get_service()." ";
        }
        //
        if ($this->f->is_option_display_ads_field_in_tables_enabled() !== true) {
            $sql_case_dossier_ads = "";
        } else {
            $sql_case_dossier_ads = "CASE 
                WHEN dossier_coordination.dossier_autorisation_ads is null AND dossier_coordination.dossier_instruction_ads is not null THEN dossier_coordination.dossier_instruction_ads
                WHEN dossier_coordination.dossier_autorisation_ads is not null AND dossier_coordination.dossier_instruction_ads is not null THEN dossier_coordination.dossier_instruction_ads
                ELSE dossier_coordination.dossier_autorisation_ads
                END as \"".__("dossier ADS")."\", ";
        }
        $query_ct_select = "
            dossier_instruction.dossier_instruction as dossier_instruction_identifiant,
            dossier_instruction.libelle as dossier_instruction_libelle,
            ".$sql_case_dossier_ads."
            etablissement.libelle as etablissement,
            CASE
                WHEN dossier_instruction.a_qualifier = true THEN 'Oui'
                WHEN dossier_instruction.a_qualifier = false THEN 'Non'
            END,
            CASE WHEN acteur.acronyme <> ''
                THEN CONCAT('<span title=''', acteur.nom_prenom, '''>', acteur.acronyme, '</span>')
                ELSE acteur.nom_prenom
            END as \"".__("technicien")."\",
            to_char(dossier_instruction.date_ouverture, 'DD/MM/YYYY') as date
        ";
        $query_ct_from = "
            ".DB_PREFIXE."dossier_instruction
            LEFT JOIN ".DB_PREFIXE."dossier_coordination
                ON dossier_instruction.dossier_coordination=dossier_coordination.dossier_coordination
            LEFT JOIN ".DB_PREFIXE."dossier_coordination_type
                ON dossier_coordination.dossier_coordination_type=dossier_coordination_type.dossier_coordination_type
            LEFT JOIN ".DB_PREFIXE."dossier_type
                ON dossier_coordination_type.dossier_type=dossier_type.dossier_type
            LEFT JOIN ".DB_PREFIXE."etablissement
                ON dossier_coordination.etablissement=etablissement.etablissement
            LEFT JOIN ".DB_PREFIXE."acteur
                ON acteur.acteur = dossier_instruction.technicien
            ";
        $query_ct_orderby = " dossier_instruction.date_ouverture ASC, dossier_instruction.libelle ";
        

        /**
         * Return
         */
        //
        return array(
            "arguments" => $arguments,
            "message_help" => $message_help,
            "description_see_more" => $description_see_more,
            "description_no_content" => $description_no_content,
            "obj_see_more" => $obj_see_more,
            "query_ct_select" => $query_ct_select,
            "query_ct_from" => $query_ct_from,
            "query_ct_where" => $query_ct_where,
            "query_ct_where_a_qualifier" => $query_ct_where_a_qualifier,
            "query_ct_where_a_affecter" => $query_ct_where_a_affecter,
            "query_ct_orderby" => $query_ct_orderby,
            "query_count_affecter" => $query_count_affecter,
            "query_count_a_qualifier" => $query_count_a_qualifier,
        );
    }


    /**
     * VIEW - view_widget_dossier_instruction_a_qualifier_affecter.
     *
     * @return boolean
     */
    function view_widget_dossier_instruction_a_qualifier_affecter($content = null) {
        /**
         * Ce widget est configurable via l'interface Web. Lors de la création
         * du widget dans le paramétrage il est possible de spécifier la ou les
         * options suivantes :
         *
         * - listing :
         *    = false
         *    = true
         *    (defaut) Par défaut le listing n'est pas affiché = false.
         * - filtre_dossier_type :
         *    = PLAN
         *    = VISIT
         *    (default) Par défaut aucun filtre_dossier_type = "".
         */
        $arguments = $this->get_arguments(
            $content,
            array("listing", "filtre_dossier_type", )
        );
        if (array_key_exists("filtre_dossier_type", $arguments) === true
            && in_array($arguments["filtre_dossier_type"], array("PLAN", "VISIT", )) === true) {
            $arguments_filtre_dossier_type = $arguments["filtre_dossier_type"];
        } else {
            $arguments_filtre_dossier_type = "";
        }

        /**
         * La configuration du widget est commune à celle des listings, on
         * la récupère donc depuis la méthode déidiée.
         */
        $conf = $this->get_config_di_a_qualifier_affecter(array(
            "filtre_dossier_type" => $arguments_filtre_dossier_type,
        ));

        /**
         * Si l'utilisateur n'a pas les permissions nécessaires, le widget
         * n'est pas affiché.
         */
        if ($this->f->isAccredited(array(
                $conf["obj_see_more"]."_tab",
                $conf["obj_see_more"], ), "OR") !== true) {
            // Le widget est vide.
            return true;
        }

        /**
         *
         */
        if ($this->f->is_option_display_ads_field_in_tables_enabled() !== true) {
            $head = array(
                $this->sprint_icon_info($conf["message_help"]),
                $conf["arguments"]["dest_obj_title"],
                __("Établissement"),
                __("À Qualifier"),
                __("Technicien"),
                __("Date ouverture")
            );
        } else {
            $head = array(
                $this->sprint_icon_info($conf["message_help"]),
                $conf["arguments"]["dest_obj_title"],
                __("Dossier ADS"),
                __("Établissement"),
                __("À Qualifier"),
                __("Technicien"),
                __("Date ouverture")
            );
        }

        /**
         * Composition de la requête
         */
        //
        $query = sprintf("
            SELECT
                %s
            FROM
                %s
            WHERE
                %s
            ORDER BY
                %s
            LIMIT 5",
            $conf["query_ct_select"],
            $conf["query_ct_from"],
            $conf["query_ct_where"],
            $conf["query_ct_orderby"]
        );

        $query_count = sprintf("
            SELECT
                count(*)
            FROM
                %s
            WHERE
                %s",
            $conf["query_ct_from"],
            $conf["query_ct_where"]
        );

        // Exécution de la requête et récupération des lignes résultats de la requête
        $results = $this->get_db_query_all($query);
        //
        $count = $this->get_db_query_one($query_count);
        $count_affecter = $this->get_db_query_one($conf["query_count_affecter"]);
        $count_a_qualifier = $this->get_db_query_one($conf["query_count_a_qualifier"]);

        // Définition des attributs du tableau
        $results["attr"] = array(
            // On définit les entêtes de colonnes
            "head" => $head,
            // On stocke le nombre de ligne résultats de la requête
            "count" => count($results),
        );
        // Les lignes résultat ne contiennent aucun lien
        // On boucle donc sur le tableau pour rajouter les liens vers
        // les éléments
        foreach ($results as $key_line => $line) {
            //
            if ($key_line === "attr") {
                continue;
            }
            // On remplace le contenu de chaque cellule par le lien
            foreach ($line as $key_cell => $cell) {
                //
                if ($key_cell == "dossier_instruction_identifiant") {
                    $cell = "->";
                }
                //
                if ($this->f->isAccredited(array(
                        $conf["obj_see_more"],
                        $conf["obj_see_more"]."_consulter", ), "OR") === true) {
                    //
                    $results[$key_line][$key_cell] = $this->sprint_link(array(
                        "href" => sprintf(
                            '%s&obj=%s&action=3&idx=%s&advs_id=&premier=0&tricol=&valide=&retour=tab',
                            OM_ROUTE_FORM,
                            $conf["obj_see_more"],
                            $line["dossier_instruction_identifiant"]
                        ),
                        "libelle" =>  $cell,
                    ));
                } else {
                    $results[$key_line][$key_cell] = $cell;
                }
            }
        }
        // La dernière ligne du tableau permet d'afficher qu'il n'y a aucun
        // résultat ou d'afficher un lien vers le listing complet des éléments
        // en question
        if ($results["attr"]["count"] == 0) {
            $last_line_content = $conf["description_no_content"];
        } else {
            $last_line_content = $this->sprint_link(array(
                "href" => OM_ROUTE_TAB."&obj=".$conf["obj_see_more"],
                "libelle" => "-> ".$conf["description_see_more"],
            ));
        }
        $results[] = array(
            "lastline" => array(
                "content" => $last_line_content,
                "colspan" => count($results["attr"]["head"]),
            ),
        );

        //
        $panel_elem = "";
        if ($this->f->isAccredited(array(
                $conf["obj_see_more"]."_tab",
                $conf["obj_see_more"], ), "OR") === true) {
            //
            $label = $this->sprint_link(array(
                "href" => sprintf(
                    '%s&obj=%s&action=601&a_affecter=true',
                    OM_ROUTE_FORM,
                    $conf["obj_see_more"]
                ),
                "libelle" => __("À affecter"),
            ));
        } else {
            $label = __("À affecter");
        }
        $panel_elem .= sprintf(
            $this->template_panel_elem_box,
            "rounded bg-danger counter-di-a-affecter",
            $count_affecter,
            $label
        );
        $panel_elem .= sprintf(
            $this->template_panel_elem_box,
            "bg-info counter-di-a-affecter-et-ou-a-qualifier",
            $count,
            __("À affecter et / ou à qualifier")
        );
        if ($this->f->isAccredited(array(
                $conf["obj_see_more"]."_tab",
                $conf["obj_see_more"], ), "OR") === true) {
            //
            $label = $this->sprint_link(array(
                "href" => sprintf(
                    '%s&obj=%s&action=601&a_qualifier=true',
                    OM_ROUTE_FORM,
                    $conf["obj_see_more"]
                ),
                "libelle" => __("À qualifier"),
            ));
        } else {
            $label = __("À qualifier");
        }
        $panel_elem .= sprintf(
            $this->template_panel_elem_box,
            "rounded bg-danger counter-di-a-qualifier",
            $count_a_qualifier,
            $label
        );
        //
        printf(
            $this->template_panel,
            $panel_elem
        );
        //
        if ($this->f->isAccredited(array(
                $conf["obj_see_more"]."_tab",
                $conf["obj_see_more"], ), "OR") === true
            && array_key_exists("listing", $arguments) === true
            && $arguments["listing"] === "true") {
            //
            echo $this->sprint_table($results);
        }
        // Le widget est tout le temps affiché, même si aucun dossier ne
        // correspond.
        $widget_is_empty = false;
        return $widget_is_empty;
    }

    /**
     * VIEW - view_widget_message_mes_non_lu.
     *
     * @return boolean
     */
    public function view_widget_message_mes_non_lu($content = null) {
        // Si l'option interface avec le référentiel ADS n'est pas
        // activée, le widget n'est pas affiché.
        if ($this->f->is_option_referentiel_ads_enabled() !== true) {
            // Le widget est vide.
            return true;
        }
        // Si l'utilisateur n'a pas les permissions nécessaires, le widget
        // n'est pas affiché.
        if ($this->f->isAccredited(array(
                "dossier_coordination_message_mes_non_lu",
                "dossier_coordination_message_mes_non_lu_tab", ), "OR") !== true) {
            // Le widget est vide.
            return true;
        }
        //
        if ($this->f->is_user_with_role_and_service("cadre", "si") !== true
            && $this->f->is_user_with_role_and_service("cadre", "acc") !== true
            && $this->f->is_user_with_role_and_service("technicien", "si") !== true
            && $this->f->is_user_with_role_and_service("technicien", "acc") !== true) {
            // Le widget est vide.
            return true;
        }

        /**
         *
         */
        //
        $conf = $this->get_config_message_mes_non_lu();
        //
        $widget_is_empty = false;
        //
        $description_see_more = __("Voir tous mes messages non lus");
        $description_no_content = __("Aucun message non lu.");
        $obj_see_more = "dossier_coordination_message_mes_non_lu";
        //
        if ($this->f->is_option_display_ads_field_in_tables_enabled() !== true) {
            $head = array(
                $this->sprint_icon_info($conf["message_help"]),
                $conf["arguments"]["dest_obj_title"],
                __("Etablissement"),
                __("Type"),
                __("Date")
            );
        } else {
            $head = array(
                $this->sprint_icon_info($conf["message_help"]),
                $conf["arguments"]["dest_obj_title"],
                __("Dossier ADS"),
                __("Etablissement"),
                __("Type"),
                __("Date")
            );
        }

        /**
         * Composition de la requête
         */
        //
        $query = sprintf("
            SELECT
                %s
            FROM
                %s
            WHERE
                %s
            ORDER BY
                %s
            LIMIT 5",
            $conf["query_ct_select"],
            $conf["query_ct_from"],
            $conf["query_ct_where"],
            $conf["query_ct_orderby"]
        );
        $query_count = sprintf("
            SELECT
                count(*)
            FROM
                %s
            WHERE
                %s",
            $conf["query_ct_from"],
            $conf["query_ct_where"]
        );
        // Exécution de la requête et récupération des lignes résultats de la requête
        $results = $this->get_db_query_all($query);
        //
        $count = $this->get_db_query_one($query_count);

        // Définition des attributs du tableau
        $results["attr"] = array(
            // On définit les entêtes de colonnes
            "head" => $head,
            // On stocke le nombre de ligne résultats de la requête
            "count" => count($results),
        );
        // Les lignes résultat ne contiennent aucun lien
        // On boucle donc sur le tableau pour rajouter les liens vers
        // les éléments
        foreach ($results as $key_line => $line) {
            //
            if ($key_line === "attr") {
                continue;
            }
            // Le lien est identique pour chaque élément de la ligne
            $href = sprintf(
                '%s&obj=dossier_coordination_message_contexte_%s&amp;idx=%s&amp;action=21',
                OM_ROUTE_FORM,
                $conf["arguments"]["dest_obj_abrege"],
                $line["dossier_coordination_message"]
            );
            
            if ($line["reconsultation"] == "rouge") {
                $results[$key_line]["attr"] = array(
                    "class" => "ligne-rouge",
                );
            }
            // On remplace le contenu de chaque cellule par le lien
            foreach ($line as $key_cell => $cell) {
                //
                if ($key_cell == "dossier_coordination"
                    or $key_cell == "dossier_instruction"
                    or $key_cell == "di_dc"
                    or $key_cell == "reconsultation") {
                    unset($results[$key_line][$key_cell]);
                    continue;
                } elseif ($key_cell == "dossier_coordination_message") {
                    $cell = "->";
                }
                //
                $results[$key_line][$key_cell] = $this->sprint_link(array(
                    "href" => $href,
                    "libelle" =>  $cell,
                ));
            }
        }
        // La dernière ligne du tableau permet d'afficher qu'il n'y a aucun
        // résultat ou d'afficher un lien vers le listing complet des éléments
        // en question
        if ($results["attr"]["count"] == 0) {
            $last_line_content = $description_no_content;
        } else {
            $last_line_content = $this->sprint_link(array(
                "href" => OM_ROUTE_TAB."&obj=".$obj_see_more,
                "libelle" => "-> ".$description_see_more,
            ));
        }
        $results[] = array(
            "lastline" => array(
                "content" => $last_line_content,
                "colspan" => count($results["attr"]["head"]),
            ),
        );

        /**
         *
         */
        //
        printf(
            $this->template_panel,
            sprintf(
                $this->template_panel_elem_box,
                "bg-info",
                $count,
                __("Non lus")
            )
        );
        //
        echo $this->sprint_table($results);
        //
        return $widget_is_empty;
    }

    /**
     * VIEW - view_widget_etablissements_npai.
     *
     * Liste des établissements NPAI.
     *
     * L'objet de ce widget est de permettre de visualiser les 
     * établissements dont l'adresse de contact est incorrecte afin d'effectuer
     * des recherches hors logiciel.
     *
     * @return boolean
     */
    public function view_widget_etablissements_npai($content = null) {
        // Si l'utilisateur n'a pas les permissions nécessaires, le widget
        // n'est pas affiché.
        if ($this->f->isAccredited(array(
                "etablissement_tous",
                "etablissement_tous_tab", ), "OR") !== true) {
            // Le widget est vide.
            return true;
        }
        //
        $widget_is_empty = false;

        /**
         *
         */
        //
        $description_listing = __("Tous les etablissement dont le marqueur NPAI est actif");
        //
        $filter_sql = " 
        npai IS TRUE ";
        //
        $description_no_content = __("Aucun etablissement NPAI.");

        /**
         * Listing de toutes les programmations urgentes
         */
        // Requête SQL
        $sql = "
        SELECT
            etablissement.etablissement as etablissement, 
            etablissement.libelle as etablissement_libelle, 
            etablissement_nature.nature as etablissement_nature,
            etablissement_statut_juridique.libelle as etablissement_statut_juridique
        FROM ".DB_PREFIXE."etablissement
            LEFT JOIN ".DB_PREFIXE."etablissement_nature
                ON etablissement.etablissement_nature = etablissement_nature.etablissement_nature
            LEFT JOIN ".DB_PREFIXE."etablissement_statut_juridique
                ON etablissement.etablissement_statut_juridique = etablissement_statut_juridique.etablissement_statut_juridique
        WHERE ";
        $sql .= $filter_sql;
        $sql .= "
        ORDER BY etablissement ASC
        ";
        // Exécution de la requête et récupération des lignes résultats de la requête
        $results = $this->get_db_query_all($sql);
        // Définition des attributs du tableau
        $results["attr"] = array(
            // On définit les entêtes de colonnes
            "head" => array(
                $this->sprint_icon_info($description_listing),
                __("Etablissement"),
                __("Nature"),
                __("Statut juridique")
            ),
            // On stocke le nombre de ligne résultats de la requête
            "count" => count($results),
        );
        // Les lignes résultat ne contiennent aucun lien
        // On boucle donc sur le tableau pour rajouter les liens vers
        // les éléments
        foreach ($results as $key_line => $line) {
            //
            if ($key_line === "attr") {
                continue;
            }
            // Le lien est identique pour chaque élément de la ligne
            $href = sprintf(
                '%s&obj=etablissement_tous&amp;action=3&amp;idx=%s',
                OM_ROUTE_FORM,
                $line["etablissement"]
            );
            // On remplace le contenu de chaque cellule par le lien
            foreach ($line as $key_cell => $cell) {
                //
                if ($key_cell == "etablissement") {
                    $cell = "->";
                }
                //
                $results[$key_line][$key_cell] = $this->sprint_link(array(
                    "href" => $href,
                    "libelle" =>  $cell,
                ));
            }
        }
        // La dernière ligne du tableau permet d'afficher qu'il n'y a aucun
        // résultat ou d'afficher un lien vers le listing complet des éléments 
        // en question
        if ($results["attr"]["count"] == 0) {
            $results[] = array(
                "lastline" => array(
                    "content" => $description_no_content,
                    "colspan" => count($results["attr"]["head"]),
                ),
            );
            // On cache le widget si aucun résultat
            $widget_is_empty = true;
        }

        /**
         *
         */
        //
        echo $this->sprint_table($results);
        //
        return $widget_is_empty;
    }


    /**
     * VIEW - view_widget_mes_visites_a_realiser.
     *
     * Mes visites a realiser.
     *
     * Liste des établissements programmés avec date et horaire de la visite, 
     * triés par date croissante.
     *
     * @return boolean
     */
    public function view_widget_mes_visites_a_realiser($content = null) {
        // Si l'utilisateur n'a pas les permissions nécessaires, le widget
        // n'est pas affiché.
        if ($this->f->isAccredited(array(
                "visite_mes_visites_a_realiser",
                "visite_mes_visites_a_realiser_tab", ), "OR") !== true) {
            // Le widget est vide.
            return true;
        }
        //
        $widget_is_empty = false;
        /**
         *
         */
        //
        $description_listing = __("Mes cinq prochaines visites.");
        //
        $filter_sql = sprintf(
            '
            visite.date_visite >= CURRENT_DATE
            AND (
                (
                    programmation_etat.code=\'VAL\'
                    AND visite.programmation_version_creation<=programmation.version
                )
                OR
                (
                    programmation_etat.code!=\'VAL\'
                    AND visite.programmation_version_creation<programmation.version
                )
            )
            AND (
                visite.programmation_version_annulation!=1
                OR visite.programmation_version_annulation IS NULL
            )
            AND om_utilisateur.login=\'%1$s\'
            ',
            $_SESSION["login"]
        );
        //
        $description_no_content = __("Aucune visite.");

        /**
         * Listing de toutes les programmations urgentes
         */
        // Requête SQL
        $sql = "
        SELECT
            dossier_instruction.dossier_instruction as id_di,
            dossier_instruction.libelle as di_libelle,
            CONCAT(etablissement.code,' - ',etablissement.libelle) as etablissement,
            CONCAT('".__("Le ")."', to_char(visite.date_visite, 'TMDay'), ' ', to_char(visite.date_visite, 'DD/MM/YYYY'), '".__(" de ")."', visite.heure_debut, '".__(" à ")."', visite.heure_fin) as date,
            CASE 
                WHEN
                    visite.programmation_version_annulation IS NOT NULL
                    AND visite.programmation_version_annulation > 1 
                THEN
                    '".__("annulée")."'
                WHEN
                    visite.programmation_version_modification IS NOT NULL
                    AND visite.programmation_version_modification=programmation.version
                    AND programmation_etat.code != 'VAL' 
                THEN
                    '".__("en cours de modification")."'
                ELSE
                    '".__("planifiée")."'
            END as etat
        FROM ".DB_PREFIXE."visite
            LEFT JOIN ".DB_PREFIXE."dossier_instruction
                ON dossier_instruction.dossier_instruction = visite.dossier_instruction
            LEFT JOIN ".DB_PREFIXE."dossier_coordination
                ON dossier_coordination.dossier_coordination = dossier_instruction.dossier_coordination
            LEFT JOIN ".DB_PREFIXE."etablissement
                ON etablissement.etablissement = dossier_coordination.etablissement
            LEFT JOIN ".DB_PREFIXE."acteur
                ON acteur.acteur = visite.acteur 
            LEFT JOIN ".DB_PREFIXE."om_utilisateur
                ON acteur.om_utilisateur = om_utilisateur.om_utilisateur 
            LEFT JOIN ".DB_PREFIXE."programmation
                ON visite.programmation = programmation.programmation
            LEFT JOIN ".DB_PREFIXE."programmation_etat
                ON programmation.programmation_etat = programmation_etat.programmation_etat
        WHERE ";
        $sql .= $filter_sql;
        $sql .= "
        ORDER BY visite.date_visite ASC, visite.heure_debut ASC NULLS LAST
        LIMIT 5
        ";
        // Exécution de la requête et récupération des lignes résultats de la requête
        $results = $this->get_db_query_all($sql);
        // Définition des attributs du tableau
        $results["attr"] = array(
            // On définit les entêtes de colonnes
            "head" => array(
                $this->sprint_icon_info($description_listing),
                __("DI"),
                __("Etablissement"),
                __("Date"),
                __("État"),
            ),
            // On stocke le nombre de ligne résultats de la requête
            "count" => count($results),
        );
        // Les lignes résultat ne contiennent aucun lien
        // On boucle donc sur le tableau pour rajouter les liens vers
        // les éléments
        foreach ($results as $key_line => $line) {
            //
            if ($key_line === "attr") {
                continue;
            }
            // Le lien est identique pour chaque élément de la ligne
            $href = sprintf(
                '%s&obj=dossier_instruction_tous_visites&amp;action=3&amp;idx=%s',
                OM_ROUTE_FORM,
                $line["id_di"]
            );
            // Tableau de correspondance des états
            $state = array(
                __("annulée")=>"danger",
                __("en cours de modification")=>"warning",
                __("planifiée")=>"valid"
                
            );
            // On remplace le contenu de chaque cellule par le lien
            foreach ($line as $key_cell => $cell) {
                //
                if ($key_cell == "etat") {
                    $cell = sprintf(
                        '<span style="font-size:14px;" class="label bg-'.$state[$cell].'">%s</span>',
                        $cell
                    );
                } elseif ($key_cell == "id_di") {
                    $cell = "->";
                }
                //
                $results[$key_line][$key_cell] = $this->sprint_link(array(
                    "href" => $href,
                    "libelle" =>  $cell,
                ));
            }
        }
        // La dernière ligne du tableau permet d'afficher qu'il n'y a aucun
        // résultat ou d'afficher un lien vers le listing complet des éléments 
        // en question
        if ($results["attr"]["count"] == 0) {
            $last_line_content = $description_no_content;
        } else {
            $last_line_content = $this->sprint_link(array(
                "href" => OM_ROUTE_TAB."&obj=visite_mes_visites_a_realiser",
                "libelle" => "-> ".__("Voir toutes mes visites à réaliser"),
            ));
        }
        $results[] = array(
            "lastline" => array(
                "content" => $last_line_content,
                "colspan" => count($results["attr"]["head"]),
            ),
        );

        /**
         *
         */
        //
        echo $this->sprint_table($results);
        //
        return $widget_is_empty;
    }

    /**
     * VIEW - view_widget_autorites_police_non_notifiees_executees.
     * 
     * Autorités de police non notifiées ou exécutées.
     *
     * L'objet de ce widget est de permettre de visualiser les 
     * autorités de police non notifiées ou exécutées.
     *
     * @return boolean
     */
    public function view_widget_autorites_police_non_notifiees_executees($content = null) {
        // Si l'utilisateur n'a pas les permissions nécessaires, le widget
        // n'est pas affiché.
        if ($this->f->isAccredited(array(
                "autorite_police_non_notifiee_executee",
                "autorite_police_non_notifiee_executee_tab", ), "OR") !== true) {
            // Le widget est vide.
            return true;
        }
        //
        $widget_is_empty = false;
        // Description du listing
        $description_listing = __("Les autorites de police qui n'ont pas ete notifiees ou executees.");
        // Condition de la requête
        $filter_sql = " (autorite_police.date_butoir < NOW()
                AND (autorite_police.dossier_instruction_reunion_prochain IS NULL
                    OR dossier_instruction_reunion_prochain.reunion IS NULL))
            OR ((LOWER(autorite_police_decision.avis) = LOWER('favorable')
                    OR LOWER(autorite_police_decision.avis) = LOWER('defavorable'))
                AND autorite_police.autorite_police NOT IN 
                    (SELECT autorite_police 
                    FROM ".DB_PREFIXE."lien_autorite_police_courrier 
                    WHERE courrier IS NOT NULL)
            ) ";
        // Description s'il n'y a pas de contenu
        $description_no_content = ("Aucune autorite de police.");

        // Requête SQL
        $sql = "SELECT autorite_police.autorite_police as ap_id,
                    to_char(autorite_police.date_decision, 'DD/MM/YYYY') as ap_date_decision,
                    autorite_police.delai as ap_delai,
                    to_char(autorite_police.date_butoir, 'DD/MM/YYYY') as ap_date_butoir,
                    dossier_coordination.libelle as dc_libelle
                FROM ".DB_PREFIXE."autorite_police
                LEFT JOIN ".DB_PREFIXE."dossier_coordination 
                    ON autorite_police.dossier_coordination = dossier_coordination.dossier_coordination
                LEFT JOIN ".DB_PREFIXE."autorite_police_decision
                    ON autorite_police.autorite_police_decision = autorite_police_decision.autorite_police_decision
                LEFT JOIN ".DB_PREFIXE."dossier_instruction_reunion
                    ON autorite_police.dossier_instruction_reunion = dossier_instruction_reunion.dossier_instruction_reunion
                LEFT JOIN ".DB_PREFIXE."dossier_instruction_reunion as dossier_instruction_reunion_prochain
                    ON autorite_police.dossier_instruction_reunion_prochain = dossier_instruction_reunion_prochain.dossier_instruction_reunion
                WHERE ";
        $sql .= $filter_sql;
        $sql .= " ORDER BY autorite_police.date_butoir ASC ";
        // Exécution de la requête et récupération des lignes résultats de la requête
        $results = $this->get_db_query_all($sql);
        // Définition des attributs du tableau
        $results["attr"] = array(
            // On définit les entêtes de colonnes
            "head" => array(
                $this->sprint_icon_info($description_listing),
                __("Date de decision"),
                __("Delai"),
                __("Date butoir"),
                __("Dossier de coordination")
            ),
            // On stocke le nombre de ligne résultats de la requête
            "count" => count($results),
            );
        // Les lignes résultat ne contiennent aucun lien
        // On boucle donc sur le tableau pour rajouter les liens vers
        // les éléments
        foreach ($results as $key_line => $line) {
            //
            if ($key_line === "attr") {
                continue;
            }
            // Le lien est identique pour chaque élément de la ligne
            $href = sprintf(
                '%s&obj=autorite_police_non_notifiee_executee&amp;action=3&amp;idx=%s',
                OM_ROUTE_FORM,
                $line["ap_id"]
            );
            // On remplace le contenu de chaque cellule par le lien
            foreach ($line as $key_cell => $cell) {
                //
                if ($key_cell == "ap_date_butoir") {
                    $cell = sprintf(
                        '<span style="font-size:14px;" class="label label-warning">%s</span>',
                        $cell
                    );
                } elseif ($key_cell == "ap_id") {
                    $cell = "->";
                }
                //
                $results[$key_line][$key_cell] = $this->sprint_link(array(
                    "href" => $href,
                    "libelle" =>  $cell,
                ));
            }
        }
        // La dernière ligne du tableau permet d'afficher qu'il n'y a aucun
        // résultat ou d'afficher un lien vers le listing complet des éléments 
        // en question
        if ($results["attr"]["count"] == 0) {
            $last_line_content = $description_no_content;
        } else {
            $last_line_content = $this->sprint_link(array(
                "href" => OM_ROUTE_TAB."&obj=autorite_police_non_notifiee_executee",
                "libelle" => "-> ".__("Voir toutes les autorités de police non notifiees ou executees"),
            ));
        }
        $results[] = array(
            "lastline" => array(
                "content" => $last_line_content,
                "colspan" => count($results["attr"]["head"]),
            ),
        );

        /**
         *
         */
        //
        echo $this->sprint_table($results);
        //
        return $widget_is_empty;
    }

    /**
     * VIEW - view_widget_dossier_coordination_a_cloturer.
     *
     * Liste les cinq plus anciens dossiers de coordination à clore.
     *
     * L'objectif de ce widget est de signaler aux cadres les Dossier de Coordination non clos,
     * qui ne contiennent plus aucun Dossier d'Instruction ouvert
     *
     * @return boolean
     */
    public function view_widget_dossier_coordination_a_cloturer($content = null) {
        // Si l'utilisateur n'a pas les permissions nécessaires, le widget
        // n'est pas affiché.
        if ($this->f->isAccredited(array(
                "dossier_coordination_a_cloturer",
                "dossier_coordination_a_cloturer_tab", ), "OR") !== true) {
            // Le widget est vide.
            return true;
        }
        //
        $widget_is_empty = false;

        /**
         * Listing des cinq plus anciens dossiers de coordination à clôturer
         */
        $help = __("Les 5 plus anciens dossiers de coordination à clôturer");

        if ($this->f->is_option_display_ads_field_in_tables_enabled() !== true) {
            $sql_case_dossier_ads = "";
            $head = array(
                $this->sprint_icon_info($help),
                __("DC"),
                __("Etablissement"),
                __("Demande")
            );
        } else {
            $sql_case_dossier_ads = "CASE 
            WHEN dc.dossier_autorisation_ads is null AND dc.dossier_instruction_ads is not null THEN dc.dossier_instruction_ads
            WHEN dc.dossier_autorisation_ads is not null AND dc.dossier_instruction_ads is not null THEN dc.dossier_instruction_ads
            ELSE dc.dossier_autorisation_ads
            END as dossier_ads, ";
            $head = array(
                $this->sprint_icon_info($help),
                __("DC"),
                __("Dossier ADS"),
                __("Etablissement"),
                __("Demande")
            );
        }

        // Requête SQL
        $sql = "
        SELECT dc.dossier_coordination, 
               dc.libelle as libelle,
               ".$sql_case_dossier_ads."
               CONCAT(et.code,' - ',et.libelle) as etablissement,
               to_char(dc.date_demande,'DD/MM/YYYY') as date_demande
        FROM ".DB_PREFIXE."dossier_coordination dc 
        LEFT JOIN ".DB_PREFIXE."etablissement et
                ON dc.etablissement=et.etablissement
        LEFT JOIN ".DB_PREFIXE."dossier_coordination_type dct
                ON dc.dossier_coordination_type=dct.dossier_coordination_type
        LEFT JOIN ".DB_PREFIXE."dossier_type dt
                ON dct.dossier_type=dt.dossier_type
        WHERE dt.code='PLAN'
          AND dc.dossier_cloture IS FALSE
          AND dc.a_qualifier IS FALSE
          AND dc.dossier_coordination NOT IN (
            SELECT dossier_coordination
            FROM ".DB_PREFIXE."dossier_instruction
            WHERE  dossier_instruction.dossier_cloture IS FALSE
            GROUP BY dossier_coordination HAVING count(*)>0
          )
        ORDER BY date_demande
        LIMIT 5
        ";
        // Exécution de la requête et récupération des lignes résultats de la requête
        $results = $this->get_db_query_all($sql);
        // Définition des attributs du tableau
        $results["attr"] = array(
            // On définit les entêtes de colonnes
            "head" => $head,
            // On stocke le nombre de ligne résultats de la requête
            "count" => count($results),
        );
        // Les lignes résultat ne contiennent aucun lien
        // On boucle donc sur le tableau pour rajouter les liens vers
        // les éléments
        foreach ($results as $key_line => $line) {
            //
            if ($key_line === "attr") {
                continue;
            }
            // Le lien est identique pour chaque élément de la ligne
            $href = sprintf(
                '%s&obj=dossier_coordination_a_cloturer&amp;action=3&amp;idx=%s',
                OM_ROUTE_FORM,
                $line["dossier_coordination"]
            );
            // On remplace le contenu de chaque cellule par le lien
            foreach ($line as $key_cell => $cell) {
                $results[$key_line][$key_cell] = $this->sprint_link(array(
                    "href" => $href,
                    "libelle" =>  ($key_cell == "dossier_coordination" ? "->" : $cell),
                ));
            }
        }
        // La dernière ligne du tableau permet d'afficher qu'il n'y a aucun
        // résultat ou d'afficher un lien vers le listing complet des éléments 
        // en question
        if ($results["attr"]["count"] == 0) {
            $last_line_content = __("Aucun dossier de coordination à clôturer.");
        } else {
            $last_line_content = $this->sprint_link(array(
                "href" => OM_ROUTE_TAB."&obj=dossier_coordination_a_cloturer",
                "libelle" => "-> ".__("Voir tous les dossiers de coordination à clôturer"),
            ));
        }
        $results[] = array(
            "lastline" => array(
                "content" => $last_line_content,
                "colspan" => count($results["attr"]["head"]),
            ),
        );

        /**
         * Statistiques sur le nombre de dossiers de coordination à clôturer
         */
        // Requête SQL
        $sql = "
        SELECT
            dct.code,
            count(dc.dossier_coordination)
            FROM ".DB_PREFIXE."dossier_coordination dc 
        LEFT JOIN ".DB_PREFIXE."dossier_coordination_type dct
                ON dc.dossier_coordination_type=dct.dossier_coordination_type
        LEFT JOIN ".DB_PREFIXE."dossier_type dt
                ON dct.dossier_type=dt.dossier_type
        WHERE dt.code='PLAN'
          AND NOT dc.dossier_cloture 
          AND dc.a_qualifier IS FALSE
          AND dc.dossier_coordination NOT IN (
            SELECT dossier_coordination
            FROM ".DB_PREFIXE."dossier_instruction
            WHERE  dossier_instruction.dossier_cloture IS FALSE
            GROUP BY dossier_coordination HAVING count(*)>0
          )
        GROUP BY dct.code
        ";
        // Exécution de la requête et récupération des lignes résultats de la requête
        $stats = $this->get_db_query_all($sql);
        //
        $main_elem = array(
            "count" => 0,
            "code" => "DC ".__("à clôturer"),
        );
        // Récupération des enregistrements
        $others_elems = array();
        foreach ($stats as $key => $value) {
            $main_elem["count"] += $value["count"];
            $others_elems[] = array(
                "count" => $value["count"],
                "code" => "DC ".$value["code"],
            );
        }

        /**
         *
         */
        //
        $panel_main_elem = sprintf(
            $this->template_panel_elem_box,
            "rounded bg-danger",
            $main_elem["count"],
            $main_elem["code"]
        );
        //
        $panel_others_elems = "";
        foreach ($others_elems as $key => $value) {
            $panel_others_elems .= sprintf(
                $this->template_panel_elem_other,
                $value["count"],
                $value["code"]
            );
        }
        //
        $panel = sprintf(
            $this->template_panel,
            $panel_main_elem." ".$panel_others_elems
        );

        /**
         *
         */
        //
        echo $panel;
        //
        echo $this->sprint_table($results);
        return $widget_is_empty;
    }

    /**
     * VIEW - view_widget_dossier_coordination_a_qualifier.
     *
     * Liste les cinq plus anciens dossiers de coordination à qualifier.
     *
     * @return boolean
     */
    public function view_widget_dossier_coordination_a_qualifier($content = null) {
        // Si l'utilisateur n'a pas les permissions nécessaires, le widget
        // n'est pas affiché.
        if ($this->f->isAccredited(array(
                "dossier_coordination_a_qualifier",
                "dossier_coordination_a_qualifier_tab", ), "OR") !== true) {
            // Le widget est vide.
            return true;
        }
        //
        $widget_is_empty = false;
        //
        $redlimit = 15;
        if (isset($this->f) && $this->f->getParameter("dc_a_qualifier_redlimit")) {
            $redlimit = intval($this->f->getParameter("dc_a_qualifier_redlimit"));
        }
        //
        $case_ddp = "case 
        when dossier_coordination.date_demande < now() - interval '".intval($redlimit)." days'
            then 'rouge'
        when dossier_coordination.depot_de_piece is TRUE
            then 'vert'
        end";

        /**
         * Listing des cinq plus anciens dossiers de coordination à qualifier
         */
        $help = __("Les 5 plus anciens dossiers de coordination à qualifier");

        if ($this->f->is_option_display_ads_field_in_tables_enabled() !== true) {
            $sql_case_dossier_ads = "";
            $head = array(
                $this->sprint_icon_info($help),
                __("DC"),
                __("Etablissement"),
                __("Demande")
            );
        } else {
            $sql_case_dossier_ads = " CASE 
            WHEN dossier_coordination.dossier_autorisation_ads is null AND dossier_coordination.dossier_instruction_ads is not null THEN dossier_coordination.dossier_instruction_ads
            WHEN dossier_coordination.dossier_autorisation_ads is not null AND dossier_coordination.dossier_instruction_ads is not null THEN dossier_coordination.dossier_instruction_ads
            ELSE dossier_coordination.dossier_autorisation_ads
            END as dossier_ads, ";
            $head = array(
                $this->sprint_icon_info($help),
                __("DC"),
                __("Dossier ADS"),
                __("Etablissement"),
                __("Demande")
            );
        }

        // Requête SQL
        $sql = "
        SELECT
            dossier_coordination.dossier_coordination,
            dossier_coordination.libelle as libelle,
            ".$sql_case_dossier_ads."
            CONCAT(etablissement.code,' - ',etablissement.libelle) as etablissement,
            to_char(dossier_coordination.date_demande,'DD/MM/YYYY') as date_demande,
            ".$case_ddp." as ddp

        FROM ".DB_PREFIXE."dossier_coordination 
            LEFT JOIN ".DB_PREFIXE."etablissement 
                ON dossier_coordination.etablissement=etablissement.etablissement
        WHERE
            dossier_coordination.a_qualifier = TRUE
        ORDER BY dossier_coordination.date_demande
        LIMIT 5
        ";
        // Exécution de la requête et récupération des lignes résultats de la requête
        $results = $this->get_db_query_all($sql);
        // Définition des attributs du tableau
        $results["attr"] = array(
            // On définit les entêtes de colonnes
            "head" => $head,
            // On stocke le nombre de ligne résultats de la requête
            "count" => count($results),
        );
        // Les lignes résultat ne contiennent aucun lien
        // On boucle donc sur le tableau pour rajouter les liens vers
        // les éléments
        foreach ($results as $key_line => $line) {
            //
            if ($key_line === "attr") {
                continue;
            }
            // Le lien est identique pour chaque élément de la ligne
            $href = sprintf(
                '%s&obj=dossier_coordination_a_qualifier&amp;action=3&amp;idx=%s',
                OM_ROUTE_FORM,
                $line["dossier_coordination"]
            );
            if ($line["ddp"] != "") {
                $results[$key_line]["attr"] = array(
                    "class" => "ligne-".$line["ddp"],
                );
            }
            // On remplace le contenu de chaque cellule par le lien
            foreach ($line as $key_cell => $cell) {
                if ($key_cell == "ddp") {
                    unset($results[$key_line][$key_cell]);
                    continue;
                }
                $results[$key_line][$key_cell] = $this->sprint_link(array(
                    "href" => $href,
                    "libelle" =>  ($key_cell == "dossier_coordination" ? "->" : $cell),
                ));
            }
        }
        // La dernière ligne du tableau permet d'afficher qu'il n'y a aucun
        // résultat ou d'afficher un lien vers le listing complet des éléments 
        // en question
        if ($results["attr"]["count"] == 0) {
            $last_line_content = __("Aucun dossier de coordination à qualifier.");
        } else {
            $last_line_content = $this->sprint_link(array(
                "href" => OM_ROUTE_TAB."&obj=dossier_coordination_a_qualifier",
                "libelle" => "-> ".__("Voir tous les dossiers de coordination a qualifier"),
            ));
        }
        $results[] = array(
            "lastline" => array(
                "content" => $last_line_content,
                "colspan" => count($results["attr"]["head"]),
            ),
        );

        /**
         * Statistiques sur le nombre de dossiers de coordination à qualifier
         */
        // Requête SQL
        $sql = "
        SELECT
            dossier_type.libelle,
            count(dossier_coordination.dossier_coordination)
        FROM ".DB_PREFIXE."dossier_coordination
            LEFT JOIN ".DB_PREFIXE."dossier_coordination_type 
                    ON dossier_coordination.dossier_coordination_type=dossier_coordination_type.dossier_coordination_type 
            LEFT JOIN ".DB_PREFIXE."dossier_type 
                    ON dossier_coordination_type.dossier_type=dossier_type.dossier_type
        WHERE
            dossier_coordination.a_qualifier = TRUE
        GROUP BY dossier_type.dossier_type
        ";
        // Exécution de la requête et récupération des lignes résultats de la requête
        $stats = $this->get_db_query_all($sql);
        //
        $main_elem = array(
            "count" => 0,
            "libelle" => "DC ".__("a_qualifier"),
        );
        // Récupération des enregistrements
        $others_elems = array();
        foreach ($stats as $key => $value) {
            $main_elem["count"] += $value["count"];
            $others_elems[] = array(
                "count" => $value["count"],
                "libelle" => "DC ".$value["libelle"],
            );
        }

        /**
         *
         */
        //
        $panel_main_elem = sprintf(
            $this->template_panel_elem_box,
            "rounded bg-danger",
            $main_elem["count"],
            $main_elem["libelle"]
        );
        //
        $panel_others_elems = "";
        foreach ($others_elems as $key => $value) {
            $panel_others_elems .= sprintf(
                $this->template_panel_elem_other,
                $value["count"],
                $value["libelle"]
            );
        }
        //
        $panel = sprintf(
            $this->template_panel,
            $panel_main_elem." ".$panel_others_elems
        );

        /**
         *
         */
        //
        echo $panel;
        //
        echo $this->sprint_table($results);
        return $widget_is_empty;
    }

    /**
     * @return boolean
     */
    protected function common_widget_dossier_instruction($content = null, $mode = null) {
        if ($mode === "mes_plans") {
            $obj = "dossier_instruction_mes_plans";
            $message_help = __("Mes 5 plus recents dossiers plans");
            $label_encours = __("plans en cours");
            $label_a_qualifier = __("plans a qualifier");
            $label_cloture = __("plans clotures");
            $message_see_all = __("Voir tous mes dossiers plans");
            $message_no_di = __("Aucun dossier plan en cours.");
            $sql_where = sprintf(
                'WHERE om_utilisateur.login = \'%1$s\' AND dossier_type.code = \'%2$s\'',
                $_SESSION["login"],
                "PLAN"
            );
            $sql_order_by = sprintf(
                'ORDER BY dossier_coordination.date_demande DESC'
            );
        } elseif ($mode === "mes_visites") {
            $obj = "dossier_instruction_mes_visites";
            $message_help = __("Mes 5 plus anciens dossiers visites");
            $label_encours = __("visites en cours");
            $label_a_qualifier = __("visites a qualifier");
            $label_cloture = __("visites clotures");
            $message_see_all = __("Voir tous mes dossiers visites");
            $message_no_di = __("Aucun dossier visite en cours.");
            $sql_where = sprintf(
                'WHERE om_utilisateur.login = \'%1$s\' AND dossier_type.code = \'%2$s\'',
                $_SESSION["login"],
                "VISIT"
            );
            $sql_order_by = sprintf(
                'ORDER BY dossier_coordination.date_demande ASC'
            );
        } else {
            // widget_is_empty
            return true;
        }
        // Si l'utilisateur n'a pas les permissions nécessaires, le widget
        // n'est pas affiché.
        if ($this->f->isAccredited(array(
                $obj,
                $obj."_tab", ), "OR") !== true) {
            // Le widget est vide.
            return true;
        }
        //
        $widget_is_empty = false;

        /**
         * Listing des cinq dossiers d'instruction à afficher
         */
        if ($this->f->is_option_display_ads_field_in_tables_enabled() !== true) {
            $sql_case_dossier_ads = "";
            $head = array(
                $this->sprint_icon_info($message_help),
                __("DI"),
                __("Etablissement"),
                __("Demande")
            );
        } else {
            $sql_case_dossier_ads = " CASE 
            WHEN dossier_coordination.dossier_autorisation_ads is null AND dossier_coordination.dossier_instruction_ads is not null THEN dossier_coordination.dossier_instruction_ads
            WHEN dossier_coordination.dossier_autorisation_ads is not null AND dossier_coordination.dossier_instruction_ads is not null THEN dossier_coordination.dossier_instruction_ads
            ELSE dossier_coordination.dossier_autorisation_ads
            END as dossier_ads, ";
            $head = array(
                $this->sprint_icon_info($message_help),
                __("DI"),
                __("Dossier ADS"),
                __("Etablissement"),
                __("Demande")
            );
        }
        //
        $sql_from = sprintf(
            'FROM %1$sdossier_instruction
                LEFT JOIN %1$sdossier_coordination
                    ON dossier_instruction.dossier_coordination = dossier_coordination.dossier_coordination
                LEFT JOIN %1$sdossier_coordination_type
                    ON dossier_coordination.dossier_coordination_type = dossier_coordination_type.dossier_coordination_type
                LEFT JOIN %1$sdossier_type
                    ON dossier_coordination_type.dossier_type = dossier_type.dossier_type
                LEFT JOIN %1$sacteur
                    ON dossier_instruction.technicien = acteur.acteur
                LEFT JOIN %1$som_utilisateur
                    ON acteur.om_utilisateur = om_utilisateur.om_utilisateur
                LEFT JOIN %1$setablissement
                    ON dossier_coordination.etablissement = etablissement.etablissement',
            DB_PREFIXE
        );
        /**
         * Listing des cinq dossiers d'instruction à afficher
         */
        $mes_di_encours = $this->get_db_query_all(sprintf(
            'SELECT
                dossier_instruction.dossier_instruction,
                dossier_instruction.libelle as libelle,
                %4$s
                CONCAT(etablissement.code, \' - \',etablissement.libelle) as etablissement,
                to_char(dossier_coordination.date_demande, \'DD/MM/YYYY\')  as date_demande
            %1$s
            %2$s
            %3$s
            LIMIT 5',
            $sql_from,
            $sql_where,
            $sql_order_by,
            $sql_case_dossier_ads
        ));

        /**
         * Statistiques sur le nombre de dossiers d'instruction
         */
        // Requête SQL
        $sql = "
        SELECT
            count(dossier_instruction.dossier_instruction)
        ".$sql_from."
        ".$sql_where;

        // Conditions pour les DI en cours
        $sql_encours = $sql." AND dossier_instruction.dossier_cloture IS FALSE
            AND dossier_instruction.a_qualifier IS FALSE ";
        $res_encours = $this->get_db_query_one($sql_encours);

        // Conditions pour les DI à qualifier
        $sql_a_qualifier = $sql." AND dossier_instruction.a_qualifier IS TRUE ";
        // Exécution de la requête
        $res_a_qualifier = $this->get_db_query_one($sql_a_qualifier);

        // Conditions pour les DI en cours
        $sql_cloture = $sql." AND dossier_instruction.dossier_cloture IS TRUE ";
        // Exécution de la requête
        $res_cloture = $this->get_db_query_one($sql_cloture);

        // Les trois chiffres clés du widget
        $main_elem = array(
            "en_cours" => array(
                "count" => $res_encours,
                "libelle" => "DI ".$label_encours,
            ),
            "a_qualifier" => array(
                "count" => $res_a_qualifier,
                "libelle" => "DI ".$label_a_qualifier,
            ),
            "cloture" => array(
                "count" => $res_cloture,
                "libelle" => "DI ".$label_cloture,
            ),
        );
        //
        $others_elems = array();

        /**
         *
         */
        //
        $panel_main_elem = sprintf(
            $this->template_panel_elem_box,
            "rounded bg-danger",
            $main_elem["en_cours"]["count"],
            $main_elem["en_cours"]["libelle"]
        );
        //
        $panel_main_elem .= sprintf(
            $this->template_panel_elem_box,
            "rounded bg-danger",
            $main_elem["a_qualifier"]["count"],
            $main_elem["a_qualifier"]["libelle"]
        );
        //
        $panel_main_elem .= sprintf(
            $this->template_panel_elem_box,
            "rounded bg-danger",
            $main_elem["cloture"]["count"],
            $main_elem["cloture"]["libelle"]
        );
        //
        $panel_others_elems = "";
        foreach ($others_elems as $key => $value) {
            $panel_others_elems .= sprintf(
                $this->template_panel_elem_other,
                $value["count"],
                $value["libelle"]
            );
        }
        //
        $panel = sprintf(
            $this->template_panel,
            sprintf(
                '%s %s',
                $panel_main_elem,
                $panel_others_elems
            )
        );

        // Définition des attributs du tableau
        $mes_di_encours["attr"] = array(
            // On définit les entêtes de colonnes
            "head" => $head,
            // On stocke le nombre de ligne résultats de la requête
            "count" => count($mes_di_encours),
        );
        // Les lignes résultat ne contiennent aucun lien
        // On boucle donc sur le tableau pour rajouter les liens vers
        // les éléments
        foreach ($mes_di_encours as $key_line => $line) {
            //
            if ($key_line === "attr") {
                continue;
            }
            // Le lien est identique pour chaque élément de la ligne
            $href = sprintf(
                '%s&obj=dossier_instruction_mes_plans&action=3&idx=%s',
                OM_ROUTE_FORM,
                $line["dossier_instruction"]
            );
            // On remplace le contenu de chaque cellule par le lien
            foreach ($line as $key_cell => $cell) {
                //
                if ($key_cell == "dossier_instruction") {
                    $cell = "->";
                }
                //
                $mes_di_encours[$key_line][$key_cell] = $this->sprint_link(array(
                    "href" => $href,
                    "libelle" =>  $cell,
                ));
            }
        }
        // La dernière ligne du tableau permet d'afficher qu'il n'y a aucun
        // résultat ou d'afficher un lien vers le listing complet des éléments 
        // en question
        if ($mes_di_encours["attr"]["count"] == 0) {
            $last_line_content = $message_no_di;
        } else {
            $last_line_content = $this->sprint_link(array(
                "href" => OM_ROUTE_TAB."&obj=".$obj,
                "libelle" => "-> ".$message_see_all,
            ));
        }
        $mes_di_encours[] = array(
            "lastline" => array(
                "content" => $last_line_content,
                "colspan" => count($mes_di_encours["attr"]["head"]),
            ),
        );

        /**
         *
         */
        //
        echo $panel;
        echo $this->sprint_table($mes_di_encours);
        return $widget_is_empty;
    }

    /**
     * VIEW - view_widget_dossier_instruction_mes_plans.
     *
     * @return boolean
     */
    public function view_widget_dossier_instruction_mes_plans($content = null) {
        return $this->common_widget_dossier_instruction($content, "mes_plans");
    }

    /**
     * VIEW - view_widget_dossier_instruction_mes_visites.
     *
     * @return boolean
     */
    public function view_widget_dossier_instruction_mes_visites($content = null) {
        return $this->common_widget_dossier_instruction($content, "mes_visites");
    }

    /**
     *
     */
    var $template_icon_consulter = '
<span class="om-icon om-icon-16 om-icon-fix consult-16">-></span>';

    /**
     *
     */
    var $template_link = '
<a href="%s"%s%s>%s</a>';

    /**
     *
     */
    function sprint_link($params) {
        //
        if (!isset($params["href"])) {
            $href = "#";
        } else {
            $href = $params["href"];
        }
        //
        if (!isset($params["title"])) {
            $attr_title = "";
        } else {
            $attr_title = sprintf(' title="%s"', $params["title"]);
        }
        //
        if (!isset($params["class"])) {
            $attr_class = "";
        } else {
            $attr_class = sprintf(' class="%s"', $params["class"]);
        }
        //
        if (!isset($params["libelle"])) {
            $libelle = "?";
        } else {
            $libelle = str_replace(
                "->",
                sprintf($this->template_icon_consulter),
                $params["libelle"]
            );
        }
        return sprintf(
            $this->template_link,
            $href,
            $attr_title,
            $attr_class,
            $libelle        
        );
    }

    /**
     *
     */
    var $template_panel = '
<div class="panel panel-box">
    <div class="list-justified-container">
        <ul class="list-justified text-center">
            %s
        </ul>
    </div>
</div>';

    /**
     *
     */
    var $template_panel_list_by_2 = '
<div class="panel panel-box">
    <ul class="list-by-2">
        %s
    </ul>
</div>';

    /**
     *
     */
    var $template_panel_list_by_3 = '
<div class="panel panel-box">
    <ul class="list-by-3">
        %s
    </ul>
</div>';

    /**
     *
     */
    var $template_panel_elem_box = '
        <li>
            <span class="size-h3 box-icon %s">%s</span>
            <p class="text-muted">%s</p> 
        </li>';

    /**
     *
     */
    var $template_panel_elem_other = '
        <li>
            <p class="size-h3">%s</p>
            <p class="text-muted">%s</p>
        </li>';

    /**
     *
     */
    var $template_table = '
<table class="table table-condensed table-bordered table-striped table-hover">
    <thead>%s
    </thead>
    <tbody>%s
    </tbody>
</table>';

    /**
     *
     */
    var $template_icon_info = '
<span title="%s" class="info-16"><!-- --></span>';

    /**
     *
     */
    function sprint_icon_info($info) {
        return sprintf(
            $this->template_icon_info,
            $info
        );
    }
    /**
     *
     */
    var $template_table_head = '
        <tr>%s
        </tr>';
    /**
     *
     */
    var $template_table_head_cell = '
            <th>%s</th>';
    /**
     *
     */
    var $template_table_line = '
        <tr>%s
        </tr>';
    /**
     *
     */
    var $template_table_line_with_class = '
        <tr class="%s">%s
        </tr>';
    /**
     *
     */
    var $template_table_line_cell = '
            <td>%s</td>';
    /**
     *
     */
    var $template_table_line_cell_colspan = '
            <td colspan="%s">%s</td>';
    /**
     *
     */
    function sprint_table($table) {
        //
        $table_head_ct = "";
        //
        if (isset($table["attr"]["head"])) {
            //
            foreach ($table["attr"]["head"] as $key => $value) {
                $table_head_ct .= sprintf(
                    $this->template_table_head_cell,
                    $value
                );
            }
        }
        //
        $table_head = sprintf(
            $this->template_table_head,
            $table_head_ct
        );
        //
        $table_body_ct = "";
        //
        foreach ($table as $key_line => $line) {
            //
            if ($key_line === "attr") {
                continue;
            }
            //
            $table_line_ct = "";
            //
            foreach ($line as $key_cell => $cell) {
                //
                if ($key_cell === "attr") {
                    continue;
                }
                //
                if (is_array($cell)) {
                    //
                    if (isset($cell["colspan"])) {
                        //
                        $table_line_ct .= sprintf(
                            $this->template_table_line_cell_colspan,
                            $cell["colspan"],
                            $cell["content"]
                        );
                    }
                } else {
                    //
                    $table_line_ct .= sprintf(
                        $this->template_table_line_cell,
                        $cell
                    );
                }

            }
            if (isset($line["attr"])
                && is_array($line["attr"])
                && isset($line["attr"]["class"])
                && $line["attr"]["class"] != "") {
                //
                $table_body_ct .= sprintf(
                    //
                    $this->template_table_line_with_class,
                    $line["attr"]["class"],
                    $table_line_ct
                );
            } else {
                //
                $table_body_ct .= sprintf(
                    //
                    $this->template_table_line,
                    $table_line_ct
                );
            }
        }
        //
        return sprintf(
            $this->template_table,
            $table_head,
            $table_body_ct
        );
    }

    /**
     *
     */
    function get_db_query_all($query) {
        //
        $res = $this->f->db->query($query);
        //
        $this->f->addToLog(__METHOD__."(): db->query(\"".$query."\");", VERBOSE_MODE);
        //
        $this->f->isDatabaseError($res);
        //
        $results = array();
        while ($row =& $res->fetchrow(DB_FETCHMODE_ASSOC)) {
            $results[] = $row;
        }
        //
        return $results;
    }

    /**
     *
     */
    function get_db_query_one($query) {
        //
        $res = $this->f->db->getone($query);
        //
        $this->f->addToLog(__METHOD__."(): db->getone(\"".$query."\");", VERBOSE_MODE);
        //
        $this->f->isDatabaseError($res);
        //
        return $res;
    }

}

