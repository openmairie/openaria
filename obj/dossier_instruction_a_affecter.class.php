<?php
/**
 * Ce script définit la classe 'dossier_instruction_a_affecter'.
 *
 * @package openaria
 * @version SVN : $Id$
 */

require_once "../obj/dossier_instruction.class.php";

/**
 * Définition de la classe 'dossier_instruction_a_affecter' (om_dbform).
 *
 * Surcharge de la classe 'dossier_instruction'.
 */
class dossier_instruction_a_affecter extends dossier_instruction {

    /**
     *
     */
    protected $_absolute_class_name = "dossier_instruction_a_affecter";

}

