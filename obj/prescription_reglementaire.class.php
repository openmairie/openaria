<?php
/**
 * Ce script définit la classe 'prescription_reglementaire'.
 *
 * @package openaria
 * @version SVN : $Id$
 */

require_once "../gen/obj/prescription_reglementaire.class.php";

/**
 * Définition de la classe 'prescription_reglementaire' (om_dbform).
 */
class prescription_reglementaire extends prescription_reglementaire_gen {

    /**
     * Liaison NaN
     */
    var $liaisons_nan = array(
        //
        "lien_prescription_reglementaire_etablissement_type" => array(
            "table_l" => "lien_prescription_reglementaire_etablissement_type",
            "table_f" => "etablissement_type",
            "field" => "etablissement_type",
        ),
        "lien_prescription_reglementaire_etablissement_categorie" => array(
            "table_l" => "lien_prescription_reglementaire_etablissement_categorie",
            "table_f" => "etablissement_categorie",
            "field" => "etablissement_categorie",
        ),
    );


    /**
     * Définition des actions disponibles sur la classe.
     *
     * @return void
     */
    function init_class_actions() {
        // On récupère les actions génériques définies dans la méthode 
        // d'initialisation de la classe parente
        parent::init_class_actions();

        // ACTION - 001 - modifier
        //
        $this->class_actions[1]["condition"] = array("is_from_good_service", );

        // ACTION - 002 - supprimer
        //
        $this->class_actions[2]["condition"] = array("is_from_good_service", );

        // ACTION - 004 - get_values
        //
        $this->class_actions[4] = array(
            "identifier" => "get_values",
            "view" => "get_values",
            "permission_suffix" => "consulter",
        );
    }

    /**
     *
     * @return array
     */
    function get_var_sql_forminc__champs() {
        return array(
            "prescription_reglementaire.prescription_reglementaire",
            "prescription_reglementaire.service",
            "prescription_reglementaire.tete_de_chapitre1",
            "prescription_reglementaire.tete_de_chapitre2",
            "prescription_reglementaire.libelle",
            "prescription_reglementaire.description_pr_om_html",
            "prescription_reglementaire.defavorable",
            "array_to_string(
                array_agg(
                    distinct(lien_prescription_reglementaire_etablissement_categorie.etablissement_categorie)),
            ';') as etablissement_categorie",
            "array_to_string(
                array_agg(
                    distinct(lien_prescription_reglementaire_etablissement_type.etablissement_type)),
            ';') as etablissement_type",
            "prescription_reglementaire.om_validite_debut",
            "prescription_reglementaire.om_validite_fin",
        );
    }

    /**
     * Liaison avec les catégories d'établissement
     * Liaison avec les types d'établissement
     * @return string
     */
    function get_var_sql_forminc__tableSelect() {
        return sprintf(
            '%1$s%2$s
            LEFT JOIN %1$slien_prescription_reglementaire_etablissement_categorie
                ON lien_prescription_reglementaire_etablissement_categorie.prescription_reglementaire = prescription_reglementaire.prescription_reglementaire
            LEFT JOIN %1$setablissement_categorie
                ON lien_prescription_reglementaire_etablissement_categorie.etablissement_categorie = etablissement_categorie.etablissement_categorie
            LEFT JOIN %1$slien_prescription_reglementaire_etablissement_type
                ON lien_prescription_reglementaire_etablissement_type.prescription_reglementaire = prescription_reglementaire.prescription_reglementaire
            LEFT JOIN %1$setablissement_type
                ON lien_prescription_reglementaire_etablissement_type.etablissement_type = etablissement_type.etablissement_type',
            DB_PREFIXE,
            $this->table
        );
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__selection() {
        return " GROUP BY prescription_reglementaire.prescription_reglementaire ";
    }

    /**
     * Liaison NaN - prescriptions réglementaires/ types d'établissement
     * @return string
     */
    function get_var_sql_forminc__sql_etablissement_type() {
        return "SELECT etablissement_type.etablissement_type, etablissement_type.libelle as lib FROM ".DB_PREFIXE."etablissement_type WHERE ((etablissement_type.om_validite_debut IS NULL AND (etablissement_type.om_validite_fin IS NULL OR etablissement_type.om_validite_fin > CURRENT_DATE)) OR (etablissement_type.om_validite_debut <= CURRENT_DATE AND (etablissement_type.om_validite_fin IS NULL OR etablissement_type.om_validite_fin > CURRENT_DATE))) ORDER BY lib ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_etablissement_type_by_id() {
        return "
SELECT
    etablissement_type.etablissement_type,
    etablissement_type.libelle as lib
FROM ".DB_PREFIXE."etablissement_type
WHERE etablissement_type IN (<idx>)
ORDER BY lib ASC";
    }

    /**
     * Liaison NaN - prescriptions réglementaires/ catégories d'établissement
     * @return string
     */
    function get_var_sql_forminc__sql_etablissement_categorie() {
        return "SELECT etablissement_categorie.etablissement_categorie, etablissement_categorie.libelle as lib FROM ".DB_PREFIXE."etablissement_categorie WHERE ((etablissement_categorie.om_validite_debut IS NULL AND (etablissement_categorie.om_validite_fin IS NULL OR etablissement_categorie.om_validite_fin > CURRENT_DATE)) OR (etablissement_categorie.om_validite_debut <= CURRENT_DATE AND (etablissement_categorie.om_validite_fin IS NULL OR etablissement_categorie.om_validite_fin > CURRENT_DATE))) ORDER BY lib ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_etablissement_categorie_by_id() {
        return "
SELECT
    etablissement_categorie.etablissement_categorie,
    etablissement_categorie.libelle as lib
FROM ".DB_PREFIXE."etablissement_categorie
WHERE etablissement_categorie IN (<idx>)
ORDER BY lib ASC";
    }

    /**
     *
     */
    function set_form_specificity(&$form, $maj) {
        //
        parent::set_form_specificity($form, $this->getParameter("maj"));

        /**
         * Gestion du champ service
         */
        // 
        $this->set_form_specificity_service_common($form, $this->getParameter("maj"));
    }


    /**
     * Permet de définir le type des champs.
     *
     * @param object  &$form Instance du formulaire
     * @param integer $maj   Mode du formulaire
     */
    function setType(&$form, $maj) {
        parent::setType($form, $maj);
        //
        if ($maj==0){ //ajout
            $form->setType('etablissement_type', 'select_multiple');
            $form->setType('etablissement_categorie', 'select_multiple');
        }// fin ajout
        if ($maj==1){ //modifier
            $form->setType('etablissement_type', 'select_multiple');
            $form->setType('etablissement_categorie', 'select_multiple');
        }// fin modifier
        if ($maj==2){ //supprimer
            $form->setType('etablissement_type', 'select_multiple_static');
            $form->setType('etablissement_categorie', 'select_multiple_static');
        }//fin supprimer
        if ($maj==3){ //consulter
            $form->setType('etablissement_type', 'select_multiple_static');
            $form->setType('etablissement_categorie', 'select_multiple_static');
        }//fin consulter
    }

    /**
     * Permet de définir la taille des champs.
     *
     * @param object  &$form Instance du formulaire
     * @param integer $maj   Mode du formulaire
     */
    function setTaille(&$form, $maj) {
        $form->setTaille("etablissement_type", 10);
        $form->setTaille("etablissement_categorie", 6);
        $form->setTaille("om_validite_debut", 8);
        $form->setTaille("om_validite_fin", 8);
    }

    /**
     * Méthode qui effectue les requêtes de configuration des champs.
     *
     * @param object  $form  Instance du formulaire.
     * @param integer $maj   Mode du formulaire.
     * @param null    $dnu1  @deprecated Ancienne ressource de base de données.
     * @param null    $dnu2  @deprecated Ancien marqueur de débogage.
     *
     * @return void
     */
    function setSelect(&$form, $maj, &$dnu1 = null, $dnu2 = null) {
        //
        parent::setSelect($form, $maj, $dnu1, $dnu2);
        // Récupération de l'identifiant de la prescription réglementaire
        $pr_id = 0;
        if (!empty($form->val['prescription_reglementaire'])
            && $form->val['prescription_reglementaire'] != ''
            && $form->val['prescription_reglementaire'] != ']') {
            $pr_id = $form->val['prescription_reglementaire'];
        }
        // Types d'établissement
        $this->init_select($form, $this->f->db, $maj, null, "etablissement_type", $this->get_var_sql_forminc__sql("etablissement_type"), $this->get_var_sql_forminc__sql("etablissement_type_by_id"), true, true);
        // Catégories d'établissement
        $this->init_select($form, $this->f->db, $maj, null, "etablissement_categorie", $this->get_var_sql_forminc__sql("etablissement_categorie"), $this->get_var_sql_forminc__sql("etablissement_categorie_by_id"), true, true);
    }

    /**
     * Permet de définir le libellé des champs.
     *
     * @param object  &$form Instance du formulaire
     * @param integer $maj   Mode du formulaire
     */
    function setLib(&$form,$maj) {
        parent::setLib($form,$maj);
        //
        $form->setLib('etablissement_type', __("type(s) d'etablissement"));
        $form->setLib('etablissement_categorie', __("categorie(s) d'etablissement"));
    }

    /**
     * TRIGGER - triggerajouterapres.
     *
     * - ...
     *
     * @return boolean
     */
    function triggerajouterapres($id, &$dnu1 = null, $val = array(), $dnu2 = null) {
        // Liaisons NaN
        foreach ($this->liaisons_nan as $liaison_nan) {
            // Ajout des liaisons table Nan
            $nb_liens = $this->ajouter_liaisons_table_nan(
                $liaison_nan["table_l"], 
                $liaison_nan["table_f"], 
                $liaison_nan["field"]
            );
            // Message de confirmation
            if ($nb_liens > 0) {
                if ($nb_liens == 1 ){
                    $this->addToMessage(sprintf(__("Creation d'une nouvelle liaison realisee avec succes.")));
                } else {
                    $this->addToMessage(sprintf(__("Creation de %s nouvelles liaisons realisee avec succes."), $nb_liens));
                }
            }
        }
    }

    /**
     * TRIGGER - triggermodifierapres.
     *
     * - ...
     *
     * @return boolean
     */
    function triggermodifierapres($id, &$dnu1 = null, $val = array(), $dnu2 = null) {
        // Liaisons NaN
        foreach ($this->liaisons_nan as $liaison_nan) {
            // Suppression des liaisons table NaN
            $this->supprimer_liaisons_table_nan($liaison_nan["table_l"]);
            // Ajout des liaisons table Nan
            $nb_liens = $this->ajouter_liaisons_table_nan(
                $liaison_nan["table_l"], 
                $liaison_nan["table_f"], 
                $liaison_nan["field"]
            );
            // Message de confirmation
            if ($nb_liens > 0) {
                $this->addToMessage(__("Mise a jour des liaisons realisee avec succes."));
            }
        }

    }

    /**
     *
     */
    function triggersupprimer($id, &$dnu1 = null, $val = array(), $dnu2 = null) {

        // Liaisons NaN
        foreach ($this->liaisons_nan as $liaison_nan) {
            // Suppression des liaisons table NaN
            $this->supprimer_liaisons_table_nan($liaison_nan["table_l"]);
        }

    }

    /**
     *
     */
    function ajouter_liaisons_table_nan($table_l, $table_f, $field) {
        // Récupération des données du select multiple
        $postvar = $this->getParameter("postvar");
        if (isset($postvar[$field])
            && is_array($postvar[$field])) {
            $multiple_values = $postvar[$field];
        } else {
            $multiple_values = array();
        }
        // Ajout des liaisons
        $nb_liens = 0;
        // Boucle sur la liste des valeurs sélectionnées
        foreach ($multiple_values as $value) {
            // Test si la valeur par défaut est sélectionnée
            if ($value == "") {
                continue;
            }
            // On compose les données de l'enregistrement
            $donnees = array(
                $this->clePrimaire => $this->valF[$this->clePrimaire],
                $table_f => $value,
                $table_l => "",
            );
            // On ajoute l'enregistrement
            $obj_l = $this->f->get_inst__om_dbform(array(
                "obj" => $table_l,
                "idx" => "]",
            ));
            $obj_l->ajouter($donnees);
            // On compte le nombre d'éléments ajoutés
            $nb_liens++;
        }
        //
        return $nb_liens;
    }

    /**
     *
     */
    function supprimer_liaisons_table_nan($table) {
        // Suppression de tous les enregistrements correspondants à l'id 
        // de l'objet instancié en cours dans la table NaN
        $sql = "DELETE FROM ".DB_PREFIXE.$table." WHERE ".$this->clePrimaire."=".$this->getVal($this->clePrimaire);
        $res = $this->f->db->query($sql);
        $this->f->addToLog(__METHOD__."(): db->query(\"".$sql."\");", VERBOSE_MODE);
        if (database::isError($res)) {
            die();
        }
    }

    /**
     * VIEW - 004 - get_values.
     *
     * Retourne en ajax les valeurs de la prescription réglementaire instanciée
     * 
     * @return void
     */
    function get_values() {
        // Vérification de l'accessibilité sur l'élément
        $this->checkAccessibility();
        //
        $this->f->disableLog();
        //
        if ($this->getVal("defavorable") == 't') {
            $check = true;
            $def = __("Oui");
        } else {
            $check = false;
            $def = __("Non");
        }
        //
        $prescriptions_specifiques = $this->get_prescriptions_specifiques();
        //
        $values = array(
            "desc" => $this->getVal("description_pr_om_html"),
            "check" => $check,
            "def" => $def,
            "prescriptions_specifiques" => (count($prescriptions_specifiques) == 0 ? false : true),
        );
        //
        echo json_encode($values);
    }

    /**
     * Fonction générique permettant de récupérer les données d'un champ postées.
     *
     * @param string $champ Nom du champ
     *
     * @return mixed Valeur posté
     */
    function getPostedValues($champ) {
        // Récupération des demandeurs dans POST
        if (isset($_POST[$champ]) ) {
            //
            return $_POST[$champ];
        }
    }

    /**
     *
     */
    function get_prescriptions_specifiques() {
        // Récupération de tous les documents présentés
        $sql = "
        SELECT
            prescription_specifique as id,
            libelle as lib,
            description_ps_om_html as desc
        FROM 
            ".DB_PREFIXE."prescription_specifique
        WHERE 
            prescription_reglementaire = ".$this->getVal($this->clePrimaire)."
        ORDER BY 
            libelle ASC
        ";
        $res = $this->f->db->query($sql);
        $this->f->addToLog(__METHOD__."(): db->query(\"".$sql."\");", VERBOSE_MODE);
        if ($this->f->isDatabaseError($res, true)) {
            $this->erreur_db($res->getDebugInfo(), $res->getMessage(), '');
            return array();
        }
        $prescriptions_specifiques = array();
        //
        while($row=& $res->fetchRow(DB_FETCHMODE_ASSOC)){
            //
            $prescriptions_specifiques[] = array(
                "id" => intval($row["id"]),
                "lib" => $row["lib"],
                "desc" => $row["desc"],
            );
        }
        //
        return $prescriptions_specifiques;
    }

}

