<?php
/**
 * Ce script définit la classe 'dossier_coordination_type'.
 *
 * @package openaria
 * @version SVN : $Id$
 */

require_once "../gen/obj/dossier_coordination_type.class.php";

/**
 * Définition de la classe 'dossier_coordination_type' (om_dbform).
 */
class dossier_coordination_type extends dossier_coordination_type_gen {

    /**
     * Liaison NaN
     */
    var $liaisons_nan = array(
        "analyses_type_disponible_si" => array(
            "table_l" => "lien_dossier_coordination_type_analyses_type",
            "table_f" => "analyses_type",
            "field" => "analyses_type_disponible_si",
            "service" => 0,
        ),
        "analyses_type_disponible_acc" => array(
            "table_l" => "lien_dossier_coordination_type_analyses_type",
            "table_f" => "analyses_type",
            "field" => "analyses_type_disponible_acc",
            "service" => 0,
        )
    );

    function __construct($id, &$dnu1 = null, $dnu2 = null) {
        $this->constructeur($id);
        $this->liaisons_nan["analyses_type_disponible_si"]["service"] = $this->f->get_service_id('SI');
        $this->liaisons_nan["analyses_type_disponible_acc"]["service"] = $this->f->get_service_id('ACC');
    }

    /**
     *
     * @return array
     */
    function get_var_sql_forminc__champs() {
        return array(
            "dossier_coordination_type.dossier_coordination_type",
            "dossier_coordination_type.code",
            "dossier_coordination_type.libelle",
            "dossier_coordination_type.description",
            "dossier_coordination_type.om_validite_debut",
            "dossier_coordination_type.om_validite_fin",
            "dossier_type",
            "dossier_instruction_secu",
            "dossier_instruction_acc",
            "a_qualifier",
            "analyses_type_si",
            "analyses_type_acc",
            "array_to_string(
                array_agg(
                    DISTINCT lien_SI.analyses_type
                ),
            ';') as analyses_type_disponible_si",
            //
            "array_to_string(
                array_agg(
                    DISTINCT lien_ACC.analyses_type
                ),
            ';') as analyses_type_disponible_acc",
            "piece_attendue_si",
            "piece_attendue_acc",
        );
    }

    /**
     *
     */
    function get_var_sql_forminc__tableSelect() {
        return sprintf(
            '%1$s%2$s
            LEFT JOIN %1$slien_dossier_coordination_type_analyses_type lien_SI
                ON lien_SI.dossier_coordination_type = dossier_coordination_type.dossier_coordination_type
                AND lien_SI.service = (SELECT service FROM %1$sservice WHERE code = \'SI\')
            LEFT JOIN %1$slien_dossier_coordination_type_analyses_type lien_ACC
                ON lien_ACC.dossier_coordination_type = dossier_coordination_type.dossier_coordination_type
                AND lien_ACC.service = (SELECT service FROM %1$sservice WHERE code = \'ACC\')',
            DB_PREFIXE,
            $this->table
        );
    }

    /**
     *
     */
    function get_var_sql_forminc__selection() {
        return " GROUP BY dossier_coordination_type.dossier_coordination_type ";
    }

    /**
     * Liaison N à N - type de DC / type d'analyse
     */
    function get_var_sql_forminc__sql_analyses_type_disponible() {
        return "
        SELECT analyses_type.analyses_type, analyses_type.libelle as lib
        FROM ".DB_PREFIXE."analyses_type
        WHERE analyses_type.service = <idx_service>
        ORDER BY analyses_type.libelle";
    }

    /**
     *
     */
    function get_var_sql_forminc__sql_analyses_type_disponible_by_id() {
        return "
        SELECT analyses_type.analyses_type, analyses_type.libelle as lib
        FROM ".DB_PREFIXE."analyses_type
        WHERE analyses_type.analyses_type IN (<idx>)
        AND analyses_type.service = <idx_service>
        ORDER BY analyses_type.libelle";
    }

    /**
     *
     */
    function get_var_sql_forminc__sql_analyses_type_acc() {
        return "SELECT analyses_type.analyses_type, analyses_type.libelle FROM ".DB_PREFIXE."analyses_type WHERE service = (SELECT service FROM ".DB_PREFIXE."service WHERE code ='ACC') AND
    ((analyses_type.om_validite_debut IS NULL AND (analyses_type.om_validite_fin IS NULL OR analyses_type.om_validite_fin > CURRENT_DATE)) OR (analyses_type.om_validite_debut <= CURRENT_DATE AND (analyses_type.om_validite_fin IS NULL OR analyses_type.om_validite_fin > CURRENT_DATE))) ORDER BY analyses_type.libelle ASC";
    }

    /**
     *
     */
    function get_var_sql_forminc__sql_analyses_type_acc_by_id() {
        return "SELECT analyses_type.analyses_type, analyses_type.libelle FROM ".DB_PREFIXE."analyses_type WHERE service = (SELECT service FROM ".DB_PREFIXE."service WHERE code ='ACC') AND
    analyses_type = <idx>";
    }

    /**
     *
     */
    function get_var_sql_forminc__sql_analyses_type_si() {
        return "SELECT analyses_type.analyses_type, analyses_type.libelle FROM ".DB_PREFIXE."analyses_type WHERE service = (SELECT service FROM ".DB_PREFIXE."service WHERE code ='SI') AND
    ((analyses_type.om_validite_debut IS NULL AND (analyses_type.om_validite_fin IS NULL OR analyses_type.om_validite_fin > CURRENT_DATE)) OR (analyses_type.om_validite_debut <= CURRENT_DATE AND (analyses_type.om_validite_fin IS NULL OR analyses_type.om_validite_fin > CURRENT_DATE))) ORDER BY analyses_type.libelle ASC";
    }

    /**
     *
     */
    function get_var_sql_forminc__sql_analyses_type_si_by_id() {
        return  "SELECT analyses_type.analyses_type, analyses_type.libelle FROM ".DB_PREFIXE."analyses_type WHERE service = (SELECT service FROM ".DB_PREFIXE."service WHERE code ='SI') AND
    analyses_type = <idx>";
    }

    /**
     * Permet de définir le type des champs.
     *
     * @param object  &$form Instance du formulaire
     * @param integer $maj   Mode du formulaire
     */
    function setType(&$form, $maj) {
        parent::setType($form, $maj);
        //
        $form->setType("dossier_instruction_reunion", "hidden");
        $form->setType("dossier_coordination", "hidden");
        $form->setType("etablissement", "hidden");
        //
        if ($maj==0){ //ajout
            $form->setType('analyses_type_disponible_si', 'select_multiple');
            $form->setType('analyses_type_disponible_acc', 'select_multiple');
        }// fin ajout
        if ($maj==1){ //modifier
            $form->setType('analyses_type_disponible_si', 'select_multiple');
            $form->setType('analyses_type_disponible_acc', 'select_multiple');
        }// fin modifier
        if ($maj==2){ //supprimer
            $form->setType('analyses_type_disponible_si', 'select_multiple_static');
            $form->setType('analyses_type_disponible_acc', 'select_multiple_static');
        }//fin supprimer
        if ($maj==3){ //consulter
            $form->setType('analyses_type_disponible_si', 'select_multiple_static');
            $form->setType('analyses_type_disponible_acc', 'select_multiple_static');
        }//fin consulter
    }

    /**
     *
     */
    function setLib(&$form, $maj) {
        //
        parent::setLib($form, $this->getParameter("maj"));
        // Liaison NaN
        $form->setLib("analyses_type_disponible_si", __("analyses_type_disponible_si"));
        $form->setLib("analyses_type_disponible_acc", __("analyses_type_disponible_acc"));
    }

    /**
     * Méthode qui effectue les requêtes de configuration des champs.
     *
     * @param object  $form Instance du formulaire.
     * @param integer $maj  Mode du formulaire.
     * @param null    $dnu1 @deprecated Ancienne ressource de base de données.
     * @param null    $dnu2 @deprecated Ancien marqueur de débogage.
     *
     * @return void
     */
    function setSelect(&$form, $maj, &$dnu1 = null, $dnu2 = null) {
        //
        parent::setSelect($form, $maj, $dnu1, $dnu2);
        // Filtre par service
        $id_SI = $this->liaisons_nan["analyses_type_disponible_si"]["service"];
        $id_ACC = $this->liaisons_nan["analyses_type_disponible_acc"]["service"];
        // Filtre par service
        $sql_analyses_type_disponible_si = str_replace('<idx_service>', $id_SI, $this->get_var_sql_forminc__sql("analyses_type_disponible"));
        $sql_analyses_type_disponible_si_by_id = str_replace('<idx_service>', $id_SI, $this->get_var_sql_forminc__sql("analyses_type_disponible_by_id"));
        $sql_analyses_type_disponible_acc = str_replace('<idx_service>', $id_ACC, $this->get_var_sql_forminc__sql("analyses_type_disponible"));
        $sql_analyses_type_disponible_acc_by_id = str_replace('<idx_service>', $id_ACC, $this->get_var_sql_forminc__sql("analyses_type_disponible_by_id"));
        //
        $this->init_select($form, $dnu1, $maj, $dnu2, "analyses_type_disponible_si", $sql_analyses_type_disponible_si, $sql_analyses_type_disponible_si_by_id, false, true);
        //
        $this->init_select($form, $dnu1, $maj, $dnu2, "analyses_type_disponible_acc", $sql_analyses_type_disponible_acc, $sql_analyses_type_disponible_acc_by_id, false, true);
    }

    /**
     * TRIGGER - triggerajouterapres.
     *
     * - ...
     *
     * @return boolean
     */
    function triggerajouterapres($id, &$dnu1 = null, $val = array(), $dnu2 = null) {
        // Liaisons NaN
        foreach ($this->liaisons_nan as $liaison_nan) {
            // Ajout des liaisons table Nan
            $nb_liens = $this->ajouter_liaisons_table_nan(
                $liaison_nan["table_l"], 
                $liaison_nan["table_f"], 
                $liaison_nan["field"],
                $liaison_nan["service"]
            );
        }
    }

    /**
     * TRIGGER - triggermodifierapres.
     *
     * - ...
     *
     * @return boolean
     */
    function triggermodifierapres($id, &$dnu1 = null, $val = array(), $dnu2 = null) {
        // Liaisons NaN
        foreach ($this->liaisons_nan as $liaison_nan) {
            // Suppression des liaisons table NaN
            $this->supprimer_liaisons_table_nan($liaison_nan["table_l"], $liaison_nan["service"]);
            // Ajout des liaisons table Nan
            $this->ajouter_liaisons_table_nan(
                $liaison_nan["table_l"], 
                $liaison_nan["table_f"], 
                $liaison_nan["field"],
                $liaison_nan["service"]
            );
        }

    }

    /**
     *
     */
    function triggersupprimer($id, &$dnu1 = null, $val = array(), $dnu2 = null) {

        // Liaisons NaN
        foreach ($this->liaisons_nan as $liaison_nan) {
            // Suppression des liaisons table NaN
            $this->supprimer_liaisons_table_nan($liaison_nan["table_l"], $liaison_nan["service"]);
        }

    }

    /**
     *
     */
    function ajouter_liaisons_table_nan($table_l, $table_f, $field, $id_service) {
        // Récupération des données du select multiple
        $postvar = $this->getParameter("postvar");
        if (isset($postvar[$field])
            && is_array($postvar[$field])) {
            $multiple_values = $postvar[$field];
        } else {
            $multiple_values = array();
        }
        // Ajout des liaisons
        $nb_liens = 0;
        // Boucle sur la liste des valeurs sélectionnées
        foreach ($multiple_values as $value) {
            // Test si la valeur par défaut est sélectionnée
            if ($value == "") {
                continue;
            }
            // On compose les données de l'enregistrement
            $donnees = array(
                $this->clePrimaire => $this->valF[$this->clePrimaire],
                $table_f => $value,
                $table_l => "",
                "service" => $id_service
            );
            // On ajoute l'enregistrement
            $obj_l = $this->f->get_inst__om_dbform(array(
                "obj" => $table_l,
                "idx" => "]",
            ));
            $obj_l->ajouter($donnees);
            // On compte le nombre d'éléments ajoutés
            $nb_liens++;
        }
        //
        return $nb_liens;
    }

    /**
     *
     */
    function supprimer_liaisons_table_nan($table, $id_service) {
        // Suppression de tous les enregistrements correspondants à l'id 
        // de l'objet instancié en cours dans la table NaN
        $sql = "DELETE FROM ".DB_PREFIXE.$table." WHERE ".$this->clePrimaire."=".$this->getVal($this->clePrimaire)." AND service = ".$id_service;
        $res = $this->f->db->query($sql);
        $this->f->addToLog(__METHOD__."(): db->query(\"".$sql."\");", VERBOSE_MODE);
        if (database::isError($res)) {
            die();
        }
    }

    /**
     * Surcharge de la méthode rechercheTable pour éviter de court-circuiter le générateur
     * en devant surcharger la méthode cleSecondaire afin de supprimer les éléments liés dans
     * les tables NaN.
     */
    function rechercheTable(&$dnu1 = null,  $table = "", $field = "", $id = null, $dnu2 = null, $selection = "") {
        //
        if (in_array($table, array_keys($this->liaisons_nan))) {
            //
            $this->addToLog(__METHOD__."(): On ne vérifie pas la table ".$table." car liaison nan.", EXTRA_VERBOSE_MODE);
            return;
        }
        //
        parent::rechercheTable($this->f->db, $table, $field, $id, null, $selection);
    }

    /**
     * Récupère l'identifiant du type de dossier de coordination par le code.
     *
     * @param string $code Code du type de dossier de coordination
     *
     * @return integer
     */
    function get_dossier_coordination_type_by_code($code) {
        // Initialisation du trouvé
        $dossier_coordination_type = "";

        // Si le code n'est pas vide
        if (!empty($code)) {

            // Requête SQL
            $sql = "
                SELECT dossier_coordination_type
                FROM ".DB_PREFIXE."dossier_coordination_type
                WHERE LOWER(code) = LOWER('".$this->db->escapesimple($code)."')";
            $dossier_coordination_type = $this->f->db->getOne($sql);
            $this->f->isDatabaseError($code);
        }

        // Résultat
        return $dossier_coordination_type;
    }

    /**
     * Récupère le paramétrage par défaut du dossier de coordination.
     *
     * @return array
     */
    function get_dossier_coordination_type_param() {

        // Initialisation du tableau des résultats
        $return = array();

        // Identifiant de l'enregistrement
        $id = $this->getVal($this->clePrimaire);

        // Si l'identifiant n'est pas vide
        if (!empty($id)) {

            // Requête SQL
            $sql = "SELECT a_qualifier, dossier_instruction_secu, dossier_instruction_acc
                FROM ".DB_PREFIXE."dossier_coordination_type
                WHERE dossier_coordination_type = ".$id;
            $res = $this->f->db->query($sql);
            $this->f->isDatabaseError($res);

            // Ligne de résultat
            $row = &$res->fetchRow(DB_FETCHMODE_ASSOC);

            // Retourne le résultat
            $return = $row;
        }

        // Retourne les résultat
        return $return;
    }

    /**
     * Indique si le type est pour les visites périodique.
     *
     * @param integer $id Identifiant du type de DC
     *
     * @return boolean
     */
    function is_periodic($id=null) {

        // Si l'identifiant n'est pas passé
        if ($id == null) {
            // Utilise l'objet en cours
            $dossier_coordination_type = $this;
        } else {
            // Sinon instancie l'établissement
            $dossier_coordination_type = $this->f->get_inst__om_dbform(array(
                "obj" => "dossier_coordination_type",
                "idx" => $id,
            ));
        }

        // Récupère le type de dossier de coordination qui
        // correspond à la visite périodique
        $dossier_coordination_type_periodique = $dossier_coordination_type->get_dossier_coordination_type_by_code($this->f->getParameter("dossier_coordination_type_periodique"));

        // Récupère l'identifiant de l'enregistrement en cours
        $dossier_coordination_type_id = $dossier_coordination_type->getVal($dossier_coordination_type->clePrimaire);

        // Si les deux identifiants sont identique
        if ($dossier_coordination_type_periodique == $dossier_coordination_type_id) {

            // Retourne vrai
            return true;
        }

        // Retourne faux
        return false;

    }

}

