<?php
/**
 * Ce script définit la classe 'dossier_instruction_reunion_contexte_reunion'.
 *
 * @package openaria
 * @version SVN : $Id$
 */

require_once "../obj/dossier_instruction_reunion.class.php";

/**
 * Définition de la classe 'dossier_instruction_reunion_contexte_reunion' (om_dbform).
 *
 * Surcharge de la classe 'dossier_instruction_reunion'.
 */
class dossier_instruction_reunion_contexte_reunion extends dossier_instruction_reunion {

    /**
     *
     */
    protected $_absolute_class_name = "dossier_instruction_reunion_contexte_reunion";

    var $inst_dossier_instruction = null;

    /**
     * Définition des actions disponibles sur la classe.
     *
     * @return void
     */
    function init_class_actions() {
        
        parent::init_class_actions();

        // ACTION - 003 - consulter
        // On ajoute la vérification du service de l'utilisateur
        $this->class_actions[3]["view"] = "view_passage_en_reunion";

        // ACTION - 00? - ???
        // 
        $this->class_actions[12] = array(
            "identifier" => "rendre_l_avis",
            "view" => "formulaire",
            "permission_suffix" => "rendre_l_avis",
            "method" => "rendre_l_avis",
            "condition" => array(
                "is_from_good_service",
                "is_planned",
                "is_numbered",
                "is_reunion_not_closed",
            ),
            "portlet" => array(
                "type" => "action-self",
                "libelle" => __("rendre l'avis"),
                "order" => 40,
                "class" => "zip-16",
            ),
        );

        // ACTION - 00? - ???
        // 
        $this->class_actions[21] = array(
            "identifier" => "next",
            "view" => "view_next",
            "permission_suffix" => "rendre_l_avis",
            "condition" => array(
                "is_from_good_service",
                "is_planned",
                "is_not_the_last",
            ),
            "portlet" => array(
                "type" => "action-self",
                "libelle" => __("point suivant"),
                "order" => 130,
                "class" => "next-16",
            ),
        );
        // ACTION - 00? - ???
        // 
        $this->class_actions[22] = array(
            "identifier" => "prev",
            "view" => "view_prev",
            "permission_suffix" => "rendre_l_avis",
            "condition" => array(
                "is_from_good_service",
                "is_planned",
                "is_not_the_first",
            ),
            "portlet" => array(
                "type" => "action-self",
                "libelle" => __("point précédent"),
                "order" => 120,
                "class" => "prev-16",
            ),
        );
    }

    function is_not_the_last() {
        $reunion = $this->get_inst_reunion();
        $ordre_du_jour = $reunion->get_ordre_du_jour();
        $ordre_du_jour = array_reverse($ordre_du_jour);
        if ($ordre_du_jour[0]["dossier_instruction_reunion"] != $this->getVal($this->clePrimaire)) {
            return true;
        } else {
            return false;
        }
    }

    function is_not_the_first() {
        $reunion = $this->get_inst_reunion();
        $ordre_du_jour = $reunion->get_ordre_du_jour();
        if ($ordre_du_jour[0]["dossier_instruction_reunion"] != $this->getVal($this->clePrimaire)) {
            return true;
        } else {
            return false;
        }
        return true;
    }


    function view_prev() {
        //
        $reunion = $this->get_inst_reunion();
        $ordre_du_jour = $reunion->get_ordre_du_jour();
        $ordre_du_jour = array_reverse($ordre_du_jour);
        $idx = $this->getVal($this->clePrimaire);
        $flag = null;
        foreach ($ordre_du_jour as $key => $value) {
            //
            if ($flag == true) {
                $idx = $value["dossier_instruction_reunion"];
                $flag = false;
                break;
            }
            //
            if ($value["dossier_instruction_reunion"] == $this->getVal($this->clePrimaire)) {
                //
                $flag = true;
            }

        }
        //
        $url = $this->compose_form_url(
            "sousform", 
            array(
                "idx" => $idx,
                "maj" => 3,
            )
        );
        $url = str_replace("&amp;", "&", $url);
        header("location:".$url);
        die();
    }

    function view_next() {
        //
        $reunion = $this->get_inst_reunion();
        $ordre_du_jour = $reunion->get_ordre_du_jour();
        $idx = $this->getVal($this->clePrimaire);
        $flag = null;
        foreach ($ordre_du_jour as $key => $value) {
            //
            if ($flag == true) {
                $idx = $value["dossier_instruction_reunion"];
                $flag = false;
                break;
            }
            //
            if ($value["dossier_instruction_reunion"] == $this->getVal($this->clePrimaire)) {
                //
                $flag = true;
            }

        }
        //
        $url = $this->compose_form_url(
            "sousform", 
            array(
                "idx" => $idx,
                "maj" => 3,
            )
        );
        $url = str_replace("&amp;", "&", $url);
        header("location:".$url);
        die();
    }

    /**
     * Cette méthode permet de composer le lien retour et de l'afficher.
     *
     * Utilisé par la VIEW sousformulaire.
     *
     * @return void
     */
    function retoursousformulaire($dnu1 = null, $dnu2 = null, $dnu3 = null, $dnu4 = null, $dnu5 = null, $dnu6 = null, $dnu7 = null, $dnu8 = null, $dnu9 = null, $dnu10 = null) {
        //
        if ($this->getParameter("maj") == 3) {
            // Objet à charger
            $obj = "dossier_instruction_reunion_contexte_reunion";
            // Construction de l'url de sousformulaire à appeler
            $url = OM_ROUTE_FORM."&obj=".$this->getParameter("retourformulaire");
            $url .= "&idx=".$this->getParameter("idxformulaire");
            $url .= "&action=301";
            $url .= "&contentonly=true";
            $url .= "&retour=form";
            //
            echo sprintf('
                <a href="#" class="retour" onclick="ajaxIt(\'%s\', \'%s\');">
                %s
                </a>',
                $obj,
                $url,
                __("Retour")
            );
            //
            return;
        }
        //
        parent::retoursousformulaire();
    }

    /**
     *
     */
    function getSubFormTitle($ent) {
        //
        $ordre = $this->getVal("ordre");
        //
        $ent = "-> ".__("Ordre du jour");
        $ent .= " -> n°".($ordre == "" ? "?" : $ordre);
        return $ent;
    }

    /**
     *
     */
    function formSpecificContent($maj) {
        //
        if ($maj == 12) {
            // Bloc établissement
            echo $this->get_display_bloc_etablissement();
            // Bloc dossier d'instruction
            echo $this->get_display_bloc_dossier_instruction();
        }
    }

    /**
     *
     */
    function sousFormSpecificContent($maj) {
        //
        $this->formSpecificContent($maj);
    }

    /**
     *
     */
    function view_passage_en_reunion() {
        // Logger
        $this->addToLog(__METHOD__."() - begin", EXTRA_VERBOSE_MODE);
        // Vérification de l'accessibilité sur l'élément
        $this->checkAccessibility();
        //
        $this->form = $this->f->get_inst__om_formulaire(array(
            "maj" => $this->getParameter("maj"),
            "validation" => null,
        ));
        //
        $this->f->handle_and_display_session_message();
        $this->retoursousformulaire();
        // Ouverture du conteneur de formulaire
        $this->form->entete();
        // Composition du tableau d'action à afficher dans le portlet
        $this->compose_portlet_actions();
        //
        $this->form->afficher_portlet(
            $this->getParameter("idx"),
            $this->user_actions
        );

        //
        echo '<div id="passage_en_reunion">';

        // Infos demande
        printf("<div class=\"bloc_demande\">\n");
        printf("<fieldset class=\"cadre ui-corner-all ui-widget-content\">");
        printf("<legend class=\"ui-corner-all ui-widget-content ui-state-active\">".__("Demande de passage")."</legend>");
        printf(
            '<div class="bloc-ordre">            
            <span class="size-h3 box-icon bg-info">%s</span>
            </div>',
            $this->getVal("ordre")
        );
        printf(
            $this->get_template("template_field"),
            "dircr-reunion_type_categorie", 
            __("categorie"), 
            $this->get_field_from_table_by_id($this->getVal("reunion_type_categorie"), "libelle", "reunion_categorie"));
        printf(
            $this->get_template("template_field"),
            "dircr-date_souhaitee", 
            __("date_souhaitee"), 
            $this->dateDBToForm($this->getVal("date_souhaitee")));
        printf(
            $this->get_template("template_field"),
            "dircr-motivation", 
            __("motivation"), 
            $this->getVal("motivation"));
        printf(
            $this->get_template("template_field"),
            "dircr-proposition_avis", 
            __("proposition_avis"), 
            $this->get_field_from_table_by_id($this->getVal("proposition_avis"), "libelle", "reunion_avis")
        );
        printf(
            $this->get_template("template_field"),
            "dircr-proposition_avis_complement", 
            __("proposition_avis_complement"), 
            $this->getVal("proposition_avis_complement")
        );
        printf("</fieldset>");
        printf("</div>\n");

        // Avis
        printf("<div class=\"bloc_avis\">\n");
        printf("<fieldset class=\"cadre ui-corner-all ui-widget-content\">");
        printf("<legend class=\"ui-corner-all ui-widget-content ui-state-active\">".__("Avis")."</legend>");
        if ($this->getVal("avis") != ""
            ||$this->getVal("avis_complement") != ""
            || $this->getVal("avis_motivation") != "") {
            //
            printf(
                $this->get_template("template_field"),
                "dircr-avis", 
                __("avis"), 
                $this->get_field_from_table_by_id($this->getVal("avis"), "libelle", "reunion_avis")
            );
            //
            printf(
                $this->get_template("template_field"),
                "dircr-avis_complement",
                __("avis_complement"),
                $this->getVal("avis_complement")
            );
            //
            printf(
                $this->get_template("template_field"), 
                "dircr-avis_motivation", 
                __("avis_motivation"), 
                $this->getVal("avis_motivation")
            );
        } else {
            echo "Aucun avis rendu.";
        }
        printf("</fieldset>");
        printf("</div>\n");

        // Bloc établissement
        printf($this->get_display_bloc_etablissement());

        // Bloc dossier d'instruction
        printf($this->get_display_bloc_dossier_instruction());

        //
        echo '<div id="dossier_instruction_reunion-id" style="display:none">';
        echo $this->getVal("dossier_instruction_reunion");
        echo '</div>';

        //
        echo '<div id="dossier_instruction-id" style="display:none">';
        echo $this->getVal("dossier_instruction");
        echo '</div>';

        //
        echo '</div>';
        // Fermeture du conteneur de formulaire
        $this->form->enpied();
        // Affichage du bouton et du bouton retour
        echo "\n<!-- ########## START FORMCONTROLS ########## -->\n";
        echo "<div class=\"formControls formControls-bottom\">\n";
        $this->retoursousformulaire();
        echo "</div>\n";
        //
        $this->afterFormSpecificContent($this->getParameter("maj"));
        // Logger
        $this->addToLog(__METHOD__."() - end", EXTRA_VERBOSE_MODE);
    }

    /**
     * Récupère l'instance du dossier d'instruction.
     *
     * @param integer $dossier_instruction Identifiant du DI
     *
     * @return object
     */
    function get_inst_dossier_instruction($dossier_instruction = null) {
        //
        if (!is_null($dossier_instruction)) {
            return $this->f->get_inst__om_dbform(array(
                "obj" => "dossier_instruction",
                "idx" => $dossier_instruction,
            ));
        }
        //
        if (is_null($this->inst_dossier_instruction)) {
            //
            $dossier_instruction = $this->getVal("dossier_instruction");
            $this->inst_dossier_instruction = $this->f->get_inst__om_dbform(array(
                "obj" => "dossier_instruction",
                "idx" => $dossier_instruction,
            ));
        }
        //
        return $this->inst_dossier_instruction;
    }

    /**
     * Cette méthode permet de récupérer les template utilisés dans les 
     * différentes vues.
     *
     * @return string
     */
    function get_template($template_name = null) {
        //
        if ($template_name == "template_field") {
            //
            return '
            <div class="field">
                <div class="form-libelle">
                    <label id="lib-%1$s" class="libelle-%1$s" for="%1$s">%2$s</label>
                </div>
                <div class="form-content">
                    <span id="%1$s" class="field_value">%3$s</span>
                </div>
            </div>
            ';
        } else {
            return '';
        }
    }

    /**
     *
     * @return string
     */
    function get_display_bloc_etablissement() {
        //
        $inst_etab = $this->get_inst_etablissement();
        //
        return sprintf('
<div class="bloc_etablissement">
    <fieldset class="cadre ui-corner-all ui-widget-content">
        <legend class="ui-corner-all ui-widget-content ui-state-active">
        %1$s
        </legend>
%2$s
    </fieldset>
</div>
',
            __("Établissement"),
            $inst_etab->get_display_synthesis()
        );
    }

    /**
     *
     * @return string
     */
    function get_display_bloc_dossier_instruction() {
        //
        $inst_di = $this->get_inst_dossier_instruction();
        $inst_analyses = $inst_di->get_inst_analyse();
        // Si c'est un dossier visite
        if ($inst_di->get_dossier_type_code_by_dossier_coordination($inst_di->getVal('dossier_coordination')) == "visit") {
            //
            $field_visit = sprintf(
                $this->get_template("template_field"), 
                "di-liste_visites", 
                __("visite(s)"), 
                $inst_di->display_list_visits()
            );
        } else {
            //
            $field_visit = "";
        }
        // Affiche la liste des visites en plus
        $template = '
<div class="bloc_dossier">
    <fieldset class="cadre ui-corner-all ui-widget-content">
        <legend class="ui-corner-all ui-widget-content ui-state-active">
        %1$s
        </legend>
        <a 
            id="link_dossier_instruction" 
            class="lienFormulaire" 
            href="'.OM_ROUTE_FORM.'&obj=dossier_instruction&amp;action=3&amp;idx=%2$s"
        >
            %3$s
        </a>
        %4$s
        %5$s
        %6$s
        %7$s
    </fieldset>
</div>
        ';
        //
        return sprintf(
            //
            $template,
            //
            __("Dossier"),
            //
            $this->getVal("dossier_instruction"),
            //
            $this->get_field_from_table_by_id(
                $this->getVal("dossier_instruction"), 
                "libelle", 
                "dossier_instruction"
            ),
            //
            sprintf(
                $this->get_template("template_field"),
                "analyses-objet", 
                __("objet de l'analyse"),
                $inst_analyses->getVal('objet')
            ),
            //
            sprintf(
                $this->get_template("template_field"),
                "analyses-proposition_avis",
                __("proposition_avis"),
                $this->get_field_from_table_by_id(
                    $inst_analyses->getVal('reunion_avis'),
                    "libelle",
                    "reunion_avis"
                )
            ),
            //
            sprintf(
                $this->get_template("template_field"),
                "analyses-proposition_avis_complement", 
                __("proposition_avis_complement"), 
                $inst_analyses->getVal('avis_complement')
            ),
            //
            $field_visit
        );

    }

}

