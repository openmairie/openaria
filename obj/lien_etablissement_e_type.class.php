<?php
/**
 * Ce script définit la classe 'lien_etablissement_e_type'.
 *
 * @package openaria
 * @version SVN : $Id$
 */

require_once "../gen/obj/lien_etablissement_e_type.class.php";

/**
 * Définition de la classe 'lien_etablissement_e_type' (om_dbform).
 */
class lien_etablissement_e_type extends lien_etablissement_e_type_gen {


    /**
     * Supprime tous les enregistrements liés à un etablissement
     *
     * @param integer $etab Identifiant du etab
     *
     * @return boolean
     */
    function delete_by_etablissement($etab) {
        // Récupère la liste des enregistrements
        $liens = $this->get_liens_by_etablissement($etab);
        // S'il y a des liens
        if (is_array($liens) && count($liens) > 0) {
            // Pour chaque enregistrement
            foreach ($liens as $lien) {
                // Instancie l'enregistrment
                $obj = $this->f->get_inst__om_dbform(array(
                    "obj" => "lien_etablissement_e_type",
                    "idx" => $lien['lien_etablissement_e_type'],
                ));
                // initialisation de la clé primaire
                $val= array(
                    'lien_etablissement_e_type' => $lien['lien_etablissement_e_type'],
                );
                // Supprime l'enregistrement
                if ($obj->supprimer($val) == false) {
                    return false;
                }
            }
        }
        //
        return true;
    }

    /**
     * Récupère la liste des liens par l'identifiant du dossier de coordination.
     *
     * @param integer $etab Identifiant du etab
     *
     * @return boolean
     */
    function get_liens_by_etablissement($etab) {
        // Initialisation de la variable de résultat
        $result = array();
        // Si l'autorité de police est renseigné
        if (!empty($etab)) {
            // Requête SQL
            $sql = sprintf(
                'SELECT *
                FROM %slien_etablissement_e_type
                WHERE etablissement = %s',
                DB_PREFIXE,
                intval($etab)
            );
            $res = $this->f->db->query($sql);
            $this->f->addToLog(__METHOD__."(): db->query(\"".$sql."\");", VERBOSE_MODE);
            $this->f->isDatabaseError($res);
            // Stockage du résultat dans un tableau
            while ($row =& $res->fetchRow(DB_FETCHMODE_ASSOC)) {
                $result[] = $row;
            }
        }
        // Retourne le résultat
        return $result;
    }

}

