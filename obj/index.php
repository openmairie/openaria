<?php
/**
 * Ce script redirige vers le fichier index.php à la racine de l'application.
 *
 * @package openaria
 * @version SVN : $Id$
 */

header("Location: ../index.php");
exit();

