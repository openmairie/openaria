<?php
/**
 * Ce script définit la classe 'courrier_a_editer'.
 *
 * @package openaria
 * @version SVN : $Id$
 */

require_once "../obj/courrier.class.php";

/**
 * Définition de la classe 'courrier_a_editer' (om_dbform).
 *
 * Surcharge de la classe 'courrier'. Cette classe permet d'afficher seulement
 * les pièces qui sont à editer.
 */
class courrier_a_editer extends courrier {

    /**
     *
     */
    protected $_absolute_class_name = "courrier_a_editer";

}

