<?php
/**
 * Ce script définit la classe 'prescription_specifique'.
 *
 * @package openaria
 * @version SVN : $Id$
 */

require_once "../gen/obj/prescription_specifique.class.php";

/**
 * Définition de la classe 'prescription_specifique' (om_dbform).
 */
class prescription_specifique extends prescription_specifique_gen {


    /**
     * Définition des actions disponibles sur la classe.
     *
     * @return void
     */
    function init_class_actions() {
        // On récupère les actions génériques définies dans la méthode 
        // d'initialisation de la classe parente
        parent::init_class_actions();

        // ACTION - 001 - modifier
        //
        $this->class_actions[1]["condition"] = array("is_from_good_service", );

        // ACTION - 002 - supprimer
        //
        $this->class_actions[2]["condition"] = array("is_from_good_service", );
    }

    /**
     *
     */
    function set_form_specificity(&$form, $maj) {
        //
        parent::set_form_specificity($form, $this->getParameter("maj"));

        /**
         * Gestion du champ service
         */
        // 
        $this->set_form_specificity_service_common($form, $this->getParameter("maj"));
    }

    /**
     *
     */
    function setType(&$form, $maj) {
        //
        parent::setType($form, $maj);
        // MODE AJOUTER
        if ($maj == 0) {
            if ($this->is_in_context_of_foreign_key("prescription_reglementaire", $this->retourformulaire)
                && $form->val["prescription_reglementaire"] == $this->getParameter("idxformulaire")) {
                $form->setType("service", "selecthiddenstatic");
            }
        }
        // MDOE MODIFIER
        if ($maj == 1) {
            if ($this->is_in_context_of_foreign_key("prescription_reglementaire", $this->retourformulaire)
                && $form->val["prescription_reglementaire"] == $this->getParameter("idxformulaire")) {
                $form->setType("service", "selecthiddenstatic");
            }
        }
    }

    /**
     * Permet de définir les valeurs des champs en sous-formualire.
     *
     * @param object  $form             Instance du formulaire.
     * @param integer $maj              Mode du formulaire.
     * @param integer $validation       Validation du form.
     * @param integer $idxformulaire    Identifiant de l'objet parent.
     * @param string  $retourformulaire Objet du formulaire.
     * @param mixed   $typeformulaire   Type du formulaire.
     * @param null    $dnu1 @deprecated Ancienne ressource de base de données.
     * @param null    $dnu2 @deprecated Ancien marqueur de débogage.
     */
    function setValsousformulaire(&$form, $maj, $validation, $idxformulaire, $retourformulaire, $typeformulaire, &$dnu1 = null, $dnu2 = null) {
        //
        parent::setValsousformulaire($form, $maj, $validation, $idxformulaire, $retourformulaire, $typeformulaire, $dnu1, $dnu2);
        //
        if ($maj == 0 && $validation == 0) {
            //
            if ($this->is_in_context_of_foreign_key('prescription_reglementaire', $this->retourformulaire)) {
                //
                $prescription_reglementaire = $this->f->get_inst__om_dbform(array(
                    "obj" => "prescription_reglementaire",
                    "idx" => $idxformulaire,
                ));
                $service = $prescription_reglementaire->getVal("service");
                $form->setVal('service', $service);
            }
        }
    }// fin setValsousformulaire
}

