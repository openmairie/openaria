<?php
/**
 * Ce script définit la classe 'etablissement'.
 *
 * @package openaria
 * @version SVN : $Id$
 */

require_once "../gen/obj/etablissement.class.php";

/**
 * Définition de la classe 'etablissement' (om_dbform).
 */
class etablissement extends etablissement_gen {


    var $inst_voie = null;
    var $inst_exploitant = null;
    var $tot_ua_valid = null;

    public $exploitant;

    function __construct($id, &$dnu1 = null, $dnu2 = null) {
        parent::__construct($id);
        
        // Création de l'exploitant suivant l'id de l'établissement
        $this->exploitant = $this->f->get_inst__om_dbform(array(
            "obj" => "contact",
            "idx" => $this->recuperer_ID_exploitant($id),
        ));
    }// fin constructeur

    /**
     *
     */
    var $merge_fields_to_avoid_obj = array(
        "etablissement",
        "geom_point",
        "geom_emprise",
    );

    /**
     *
     * @return array
     */
    function get_var_sql_forminc__champs() {
        if (isset($_GET["action"]) && $_GET["action"] == 3) {
            return array(
                "etablissement.etablissement",
                //
                // Détails sécurité et accessibilité
                //
                // COL1
                // Fieldset détail accessibilité
                "acc_consignes_om_html",
                "acc_descriptif_om_html",
                "'' as tot_ua_valid",
                "'' as acc_handicap_auditif",
                "'' as acc_handicap_mental",
                "'' as acc_handicap_physique",
                "'' as acc_handicap_visuel",
                "'' as acc_derogation_scda",
                // COL2
                // Fieldset détail sécurité
                "si_consignes_om_html",
                "si_descriptif_om_html",
                "si_type_alarme",
                "si_type_ssi",
                "si_conformite_l16",
                "si_alimentation_remplacement",
                "si_service_securite",
                "si_personnel_jour",
                "si_personnel_nuit",
                //
                // Fiche établissement
                //
                // COL 1
                // Fieldset établissement
                "etablissement.code",
                "etablissement.libelle",
                "adresse_numero",
                "adresse_numero2",
                "adresse_voie",
                "adresse_complement",
                "lieu_dit",
                "boite_postale",
                "adresse_cp",
                "adresse_ville",
                "adresse_arrondissement",
                "cedex",
                "npai",
                "telephone",
                "fax",
                "etablissement_nature",
                "siret",
                "annee_de_construction",
                "etablissement_statut_juridique",
                "etablissement_tutelle_adm",
                // Fieldset exploitant
                "'Aucun' as exp",
                "'' as exp_civilite",
                "'' as exp_nom",
                "'' as exp_prenom",
                "'' as exp_blank",
                "'' as meme_adresse",
                //
                "'' as exp_adresse_numero",
                "'' as exp_adresse_numero2",
                "'' as exp_adresse_voie",
                //
                "'' as exp_adresse_complement",
                //
                "'' as exp_lieu_dit",
                "'' as exp_boite_postale",
                //
                "'' as exp_adresse_cp",
                "'' as exp_adresse_ville",
                "'' as exp_cedex",
                //
                "'' as exp_pays",
                // Fieldset localisation
                "references_cadastrales",
                    "ref_patrimoine",
                "geom_point",
                "geom_emprise",
                    "geolocalise",
                // COL 2
                // Fieldset statut
                "etablissement.etablissement_type",
                "etablissement_categorie",
                "etablissement_etat",
                "array_to_string(
                    array_agg(
                        lien_etablissement_e_type.etablissement_type
                        ORDER BY etablissement_type.libelle),
                ';') as etablissement_type_secondaire",
                "date_arrete_ouverture",
                "autorite_police_encours",
                "etablissement.om_validite_debut",
                "etablissement.om_validite_fin",
                // Fieldset détail technique
                "si_effectif_public",
                "si_effectif_personnel",
                "si_locaux_sommeil",
                "si_visite_duree",
                // Fieldset suivi des visites
                // Visites sécurité
                "si_autorite_competente_visite",
                "si_periodicite_visites",
                "si_prochaine_visite_periodique_date_previsionnelle",
                "si_derniere_visite_periodique_date",
                "dossier_coordination_periodique",
                "si_derniere_visite_date",
                "si_derniere_visite_avis",
                "si_derniere_visite_technicien",
                "si_prochaine_visite_date",
                "si_prochaine_visite_type",
                // Visites accessibilité
                "acc_derniere_visite_date",
                "acc_derniere_visite_avis",
                "acc_derniere_visite_technicien",
                // Fieldset suivi des plans
                // Plans sécurité
                "si_autorite_competente_plan",
                "si_dernier_plan_avis",
            );
        } else {
            return array(
                "etablissement.etablissement",
                //
                // Détails sécurité et accessibilité
                //
                // COL1
                // Fieldset détail accessibilité
                "acc_consignes_om_html",
                "acc_descriptif_om_html",
                "'' as tot_ua_valid",
                "'' as acc_handicap_auditif",
                "'' as acc_handicap_mental",
                "'' as acc_handicap_physique",
                "'' as acc_handicap_visuel",
                "'' as acc_derogation_scda",
                // COL2
                // Fieldset détail sécurité
                "si_consignes_om_html",
                "si_descriptif_om_html",
                "si_type_alarme",
                "si_type_ssi",
                "si_conformite_l16",
                "si_alimentation_remplacement",
                "si_service_securite",
                "si_personnel_jour",
                "si_personnel_nuit",
                //
                // Fiche établissement
                //
                // COL 1
                // Fieldset établissement
                "etablissement.code",
                "etablissement.libelle",
                "adresse_numero",
                "adresse_numero2",
                "adresse_voie",
                "adresse_complement",
                "lieu_dit",
                "boite_postale",
                "adresse_cp",
                "adresse_ville",
                "adresse_arrondissement",
                "cedex",
                "npai",
                "telephone",
                "fax",
                "etablissement_nature",
                "siret",
                "annee_de_construction",
                "etablissement_statut_juridique",
                "etablissement_tutelle_adm",
                // Fieldset exploitant
                "'Aucun' as exp",
                "'' as exp_civilite",
                "'' as exp_nom",
                "'' as exp_prenom",
                "'' as exp_blank",
                "'' as meme_adresse",
                //
                "'' as exp_adresse_numero",
                "'' as exp_adresse_numero2",
                "'' as exp_adresse_voie",
                //
                "'' as exp_adresse_complement",
                //
                "'' as exp_lieu_dit",
                "'' as exp_boite_postale",
                //
                "'' as exp_adresse_cp",
                "'' as exp_adresse_ville",
                "'' as exp_cedex",
                //
                "'' as exp_pays",
                // Fieldset localisation
                "references_cadastrales",
                    "ref_patrimoine",
                "geom_point",
                "geom_emprise",
                    "geolocalise",
                // COL 2
                // Fieldset statut
                "etablissement.etablissement_type",
                "etablissement_categorie",
                "array_to_string(
                    array_agg(
                        lien_etablissement_e_type.etablissement_type
                        ORDER BY etablissement_type.libelle),
                ';') as etablissement_type_secondaire",
                "etablissement_etat",
                "date_arrete_ouverture",
                "autorite_police_encours",
                "etablissement.om_validite_debut",
                "etablissement.om_validite_fin",
                // Fieldset détail technique
                "si_effectif_public",
                "si_effectif_personnel",
                "si_locaux_sommeil",
                "si_visite_duree",
                // Fieldset suivi des visites
                // Visites sécurité
                "si_autorite_competente_visite",
                "si_periodicite_visites",
                "si_prochaine_visite_periodique_date_previsionnelle",
                "si_derniere_visite_periodique_date",
                "dossier_coordination_periodique",
                "si_derniere_visite_date",
                "si_derniere_visite_avis",
                "si_derniere_visite_technicien",
                "si_prochaine_visite_date",
                "si_prochaine_visite_type",
                // Visites accessibilité
                "acc_derniere_visite_date",
                "acc_derniere_visite_avis",
                "acc_derniere_visite_technicien",
                // Fieldset suivi des plans
                // Plans sécurité
                "si_autorite_competente_plan",
                "si_dernier_plan_avis",
            );
        }
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__tableSelect() {
        return sprintf(
            '%1$s%2$s
            LEFT JOIN %1$slien_etablissement_e_type
                ON lien_etablissement_e_type.etablissement = etablissement.etablissement
            LEFT JOIN %1$setablissement_type
                ON etablissement_type.etablissement_type = lien_etablissement_e_type.etablissement_type',
            DB_PREFIXE,
            $this->table
        );
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__selection() {
        return " GROUP BY etablissement.etablissement ";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_civilite() {
        return "SELECT contact_civilite.contact_civilite, contact_civilite.libelle FROM ".DB_PREFIXE."contact_civilite ORDER BY contact_civilite.libelle ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_civilite_by_id() {
        return "SELECT contact_civilite.contact_civilite, contact_civilite.libelle FROM ".DB_PREFIXE."contact_civilite WHERE contact_civilite = <idx>";
    }

    /**
     * Requête SQL pour filtrer l'arrondissement en fonction de la voie
     */
    function get_var_sql_forminc__sql_adresse_arrondissement_by_adresse_voie() {
        return "SELECT arrondissement.arrondissement, arrondissement.libelle FROM ".DB_PREFIXE."arrondissement LEFT JOIN ".DB_PREFIXE."voie_arrondissement ON voie_arrondissement.arrondissement = arrondissement.arrondissement WHERE voie_arrondissement.voie = <idx_voie>";
    }

    /**
     * On modifie le select nature pour supprimer la voie publique
     */
    function get_var_sql_forminc__sql_etablissement_nature() {
        if ($this->getParameter("maj") == 999) {
            $lib = "concat(etablissement_nature.code, ' - ', etablissement_nature.nature)";
        } else {
            $lib = "etablissement_nature.nature";
        }
        return "SELECT etablissement_nature.etablissement_nature, ".$lib." 
        FROM ".DB_PREFIXE."etablissement_nature 
        WHERE 
            (
                (
                    (etablissement_nature.om_validite_debut IS NULL 
                        AND (etablissement_nature.om_validite_fin IS NULL 
                            OR etablissement_nature.om_validite_fin > CURRENT_DATE)
                    ) OR ( etablissement_nature.om_validite_debut <= CURRENT_DATE 
                          AND (etablissement_nature.om_validite_fin IS NULL 
                                OR etablissement_nature.om_validite_fin > CURRENT_DATE)
                          )
                ) AND etablissement_nature.nature != 'Aménagement voie publique'
            ) 
        ORDER BY etablissement_nature.nature ASC";
    }

    /**
     *
     */
    function get_var_sql_forminc__sql_autorite_competente() {
        return "
        SELECT
            autorite_competente.autorite_competente, autorite_competente.libelle
        FROM 
            ".DB_PREFIXE."autorite_competente 
                LEFT JOIN ".DB_PREFIXE."service
                    ON autorite_competente.service=service.service
        WHERE
            service.code='<service>'
        ORDER BY 
            autorite_competente.libelle ASC
        ";
    }

    /**
     *
     */
    function get_var_sql_forminc__sql_si_autorite_competente() {
        return str_replace("<service>", "SI", $this->get_var_sql_forminc__sql("autorite_competente"));
    }

    /**
     *
     */
    function get_var_sql_forminc__sql_si_autorite_competente_plan() {
        return $this->get_var_sql_forminc__sql("si_autorite_competente");
    }

    /**
     *
     */
    function get_var_sql_forminc__sql_si_autorite_competente_visite() {
        return $this->get_var_sql_forminc__sql("si_autorite_competente");
    }

    /**
     *
     */
    function get_var_sql_forminc__sql_reunion_avis() {
        return "
        SELECT 
            reunion_avis.reunion_avis, reunion_avis.libelle 
        FROM 
            ".DB_PREFIXE."reunion_avis
                LEFT JOIN ".DB_PREFIXE."service
                    ON reunion_avis.service=service.service
        WHERE 
            ((reunion_avis.om_validite_debut IS NULL AND (reunion_avis.om_validite_fin IS NULL OR reunion_avis.om_validite_fin > CURRENT_DATE)) OR (reunion_avis.om_validite_debut <= CURRENT_DATE AND (reunion_avis.om_validite_fin IS NULL OR reunion_avis.om_validite_fin > CURRENT_DATE))) 
            AND service.code='<service>'
        ORDER BY 
            reunion_avis.libelle ASC
        ";
    }

    /**
     *
     */
    function get_var_sql_forminc__sql_si_reunion_avis() {
        return str_replace("<service>", "SI", $this->get_var_sql_forminc__sql("reunion_avis"));
    }

    /**
     *
     */
    function get_var_sql_forminc__sql_si_derniere_visite_avis() {
        return $this->get_var_sql_forminc__sql("si_reunion_avis");
    }

    /**
     *
     */
    function get_var_sql_forminc__sql_si_dernier_plan_avis() {
        return $this->get_var_sql_forminc__sql("si_reunion_avis");
    }

    /**
     *
     */
    function get_var_sql_forminc__sql_acc_reunion_avis() {
        return str_replace("<service>", "ACC", $this->get_var_sql_forminc__sql("reunion_avis"));
    }

    /**
     *
     */
    function get_var_sql_forminc__sql_acc_derniere_visite_avis() {
        return $this->get_var_sql_forminc__sql("acc_reunion_avis");
    }

    /**
     *
     */
    function get_var_sql_forminc__sql_acc_dernier_plan_avis() {
        return $this->get_var_sql_forminc__sql("acc_reunion_avis");
    }

    /**
     * GETTER_FORMINC - sql_si_derniere_visite_technicien.
     */
    function get_var_sql_forminc__sql_si_derniere_visite_technicien() {
        return str_replace("<service>", "SI", $this->get_var_sql_forminc__sql("visite_technicien"));
    }

    /**
     * GETTER_FORMINC - sql_acc_derniere_visite_technicien.
     */
    function get_var_sql_forminc__sql_acc_derniere_visite_technicien() {
        return str_replace("<service>", "ACC", $this->get_var_sql_forminc__sql("visite_technicien"));
    }

    /**
     * GETTER_FORMINC - sql_visite_technicien.
     */
    function get_var_sql_forminc__sql_visite_technicien() {
        return sprintf(
            'SELECT
                acteur.acteur,
                acteur.nom_prenom
            FROM
                %1$sacteur
                    LEFT JOIN %1$sservice
                        ON acteur.service=service.service
            WHERE
                ((acteur.om_validite_debut IS NULL AND (acteur.om_validite_fin IS NULL OR acteur.om_validite_fin > CURRENT_DATE)) OR (acteur.om_validite_debut <= CURRENT_DATE AND (acteur.om_validite_fin IS NULL OR acteur.om_validite_fin > CURRENT_DATE)))
                AND (acteur.service IS NULL OR service.code=\'<service>\')
                AND acteur.role IN (\'cadre\', \'technicien\')
            ORDER BY
                acteur.nom_prenom ASC',
            DB_PREFIXE
        );
    }

    /**
     *
     */
    function get_var_sql_forminc__sql_adresse_arrondissement() {
        return "SELECT arrondissement.arrondissement, arrondissement.libelle FROM ".DB_PREFIXE."arrondissement WHERE ((arrondissement.om_validite_debut IS NULL AND (arrondissement.om_validite_fin IS NULL OR arrondissement.om_validite_fin > CURRENT_DATE)) OR (arrondissement.om_validite_debut <= CURRENT_DATE AND (arrondissement.om_validite_fin IS NULL OR arrondissement.om_validite_fin > CURRENT_DATE))) ORDER BY arrondissement.code::int ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_etablissement_categorie() {
        if ($this->getParameter("maj") == 999) {
            return "SELECT etablissement_categorie.etablissement_categorie, etablissement_categorie.libelle FROM ".DB_PREFIXE."etablissement_categorie WHERE ((etablissement_categorie.om_validite_debut IS NULL AND (etablissement_categorie.om_validite_fin IS NULL OR etablissement_categorie.om_validite_fin > CURRENT_DATE)) OR (etablissement_categorie.om_validite_debut <= CURRENT_DATE AND (etablissement_categorie.om_validite_fin IS NULL OR etablissement_categorie.om_validite_fin > CURRENT_DATE))) ORDER BY etablissement_categorie.libelle ASC";
        } else {
            return "SELECT etablissement_categorie.etablissement_categorie, concat('[',etablissement_categorie.libelle, '] ', etablissement_categorie.description) FROM ".DB_PREFIXE."etablissement_categorie WHERE ((etablissement_categorie.om_validite_debut IS NULL AND (etablissement_categorie.om_validite_fin IS NULL OR etablissement_categorie.om_validite_fin > CURRENT_DATE)) OR (etablissement_categorie.om_validite_debut <= CURRENT_DATE AND (etablissement_categorie.om_validite_fin IS NULL OR etablissement_categorie.om_validite_fin > CURRENT_DATE))) ORDER BY etablissement_categorie.libelle ASC";
        }
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_etablissement_type() {
        if ($this->getParameter("maj") == 999) {
            return "SELECT etablissement_type.etablissement_type, etablissement_type.libelle FROM ".DB_PREFIXE."etablissement_type WHERE ((etablissement_type.om_validite_debut IS NULL AND (etablissement_type.om_validite_fin IS NULL OR etablissement_type.om_validite_fin > CURRENT_DATE)) OR (etablissement_type.om_validite_debut <= CURRENT_DATE AND (etablissement_type.om_validite_fin IS NULL OR etablissement_type.om_validite_fin > CURRENT_DATE))) ORDER BY etablissement_type.libelle ASC";
        } else {
            return "SELECT etablissement_type.etablissement_type, concat('[',etablissement_type.libelle, '] ', etablissement_type.description) FROM ".DB_PREFIXE."etablissement_type WHERE ((etablissement_type.om_validite_debut IS NULL AND (etablissement_type.om_validite_fin IS NULL OR etablissement_type.om_validite_fin > CURRENT_DATE)) OR (etablissement_type.om_validite_debut <= CURRENT_DATE AND (etablissement_type.om_validite_fin IS NULL OR etablissement_type.om_validite_fin > CURRENT_DATE))) ORDER BY etablissement_type.libelle ASC";
        }
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_etablissement_statut_juridique() {
        if ($this->getParameter("maj") == 999) {
            return "SELECT etablissement_statut_juridique.etablissement_statut_juridique, concat(etablissement_statut_juridique.code, ' - ', etablissement_statut_juridique.libelle) FROM ".DB_PREFIXE."etablissement_statut_juridique WHERE ((etablissement_statut_juridique.om_validite_debut IS NULL AND (etablissement_statut_juridique.om_validite_fin IS NULL OR etablissement_statut_juridique.om_validite_fin > CURRENT_DATE)) OR (etablissement_statut_juridique.om_validite_debut <= CURRENT_DATE AND (etablissement_statut_juridique.om_validite_fin IS NULL OR etablissement_statut_juridique.om_validite_fin > CURRENT_DATE))) ORDER BY etablissement_statut_juridique.libelle ASC";
        }
        return parent::get_var_sql_forminc__sql_etablissement_statut_juridique();
    }

    /**
     * Liaison NaN - etablissement / types secondaires d'établissement
     */
    function get_var_sql_forminc__sql_etablissement_type_secondaire() {
        return "
        SELECT etablissement_type.etablissement_type,
        concat('[',etablissement_type.libelle, '] ', etablissement_type.description)
        FROM ".DB_PREFIXE."etablissement_type
        ORDER BY etablissement_type.libelle";
    }

    /**
     *
     */
    function get_var_sql_forminc__sql_etablissement_type_secondaire_by_id() {
        if ($this->getParameter("maj") == 3) {
            return "
            SELECT lien_etablissement_e_type.etablissement_type,
            etablissement_type.libelle
            FROM ".DB_PREFIXE."lien_etablissement_e_type
            LEFT JOIN ".DB_PREFIXE."etablissement_type
            ON etablissement_type.etablissement_type = lien_etablissement_e_type.etablissement_type
            WHERE lien_etablissement_e_type.etablissement = <idx>
            ORDER BY etablissement_type.libelle";
        } else {
            return "
            SELECT lien_etablissement_e_type.etablissement_type,
            concat('[',etablissement_type.libelle, '] ', etablissement_type.description)
            FROM ".DB_PREFIXE."lien_etablissement_e_type
            LEFT JOIN ".DB_PREFIXE."etablissement_type
            ON etablissement_type.etablissement_type = lien_etablissement_e_type.etablissement_type
            WHERE lien_etablissement_e_type.etablissement = <idx>
            ORDER BY etablissement_type.libelle";
        }
    }

    /**
     *
     */
    function get_inst_exploitant($exploitant = null) {
        //
        if (!is_null($exploitant)) {
            //
            return $this->f->get_inst__om_dbform(array(
                "obj" => "contact",
                "idx" => $exploitant,
            ));
        }
        //
        if (is_null($this->inst_exploitant)) {
            //
            $exploitant = $this->recuperer_ID_exploitant($this->getVal($this->clePrimaire));
            // Si l'établissement n'a pas d'exploitant
            if ($exploitant == "]") {
                $exploitant = 0;
            }
            //
            $this->inst_exploitant = $this->f->get_inst__om_dbform(array(
                "obj" => "contact",
                "idx" => $exploitant,
            ));
        }
        //
        return $this->inst_exploitant;
    }

    /**
     *
     */
    function get_inst_voie($voie = null) {
        //
        if (is_null($this->inst_voie)) {
            //
            if (is_null($voie)) {
                $voie = $this->getVal("adresse_voie");
            }
            //
            $this->inst_voie = $this->f->get_inst__om_dbform(array(
                "obj" => "voie",
                "idx" => $voie,
            ));
        }
        //
        return $this->inst_voie;
    }

    function get_values_merge_fields() {
        //
        $values = parent::get_values_merge_fields();
        //
        $ua = "";
        $inst_ua = $this->f->get_inst__om_dbform(array(
            "obj" => "etablissement_unite",
            "idx" => 0,
        ));
        $ua = $inst_ua->get_display_for_merge_field(array(
            "obj" => "etablissement", 
            "idx" => $this->getVal("etablissement"),
        ));
        $values["etablissement.ua_validees"] = $ua;
        //
        $inst_voie = $this->get_inst_voie();
        $values["etablissement.adresse_voie"] = $inst_voie->getVal("libelle");
        // Adresse complète sur une seule ligne
        $values["etablissement.adresse_complete_sur_une_ligne"] = $this->get_formatted_address(array(
            "format" => "1_line",
            "complement" => $this->getVal('adresse_complement'),
            "numero" => $this->getVal('adresse_numero'),
            "numero2" => $this->getVal('adresse_numero2'),
            "voie" => !empty($inst_voie) ? $inst_voie->getVal('libelle') : '',
            "lieu_dit" => $this->getVal('lieu_dit'),
            "boite_postale" => $this->getVal('boite_postale'),
            "cp" => $this->getVal('adresse_cp'),
            "ville" => $this->getVal('adresse_ville'),
            "cedex" => $this->getVal('cedex'),
        ));
        // Adresse complète sur quatre lignes
        $values["etablissement.adresse_complete_sur_quatre_lignes"] = nl2br($this->get_formatted_address(array(
            "format" => "4_lines",
            "complement" => $this->getVal('adresse_complement'),
            "numero" => $this->getVal('adresse_numero'),
            "numero2" => $this->getVal('adresse_numero2'),
            "voie" => !empty($inst_voie) ? $inst_voie->getVal('libelle') : '',
            "lieu_dit" => $this->getVal('lieu_dit'),
            "boite_postale" => $this->getVal('boite_postale'),
            "cp" => $this->getVal('adresse_cp'),
            "ville" => $this->getVal('adresse_ville'),
            "cedex" => $this->getVal('cedex'),
        )));
        //
        $values["etablissement.si_derniere_visite_avis"] = $this->f->get_label_of_foreign_key(
            "etablissement",
            "si_derniere_visite_avis",
            $this->getVal("si_derniere_visite_avis")
        );
        //
        $values["etablissement.si_dernier_plan_avis"] = $this->f->get_label_of_foreign_key(
            "etablissement",
            "si_dernier_plan_avis",
            $this->getVal("si_dernier_plan_avis")
        );
        //
        $values["etablissement.si_autorite_competente_visite"] = $this->f->get_label_of_foreign_key(
            "etablissement",
            "si_autorite_competente_visite",
            $this->getVal("si_autorite_competente_visite")
        );
        //
        $values["etablissement.si_autorite_competente_plan"] = $this->f->get_label_of_foreign_key(
            "etablissement",
            "si_autorite_competente_plan",
            $this->getVal("si_autorite_competente_plan")
        );
        // Champs de fusions liés au type d'établissement
        $values_mf_etablissement_type = $this->get_mf_etablissement_type("values");
        foreach ($values_mf_etablissement_type as $key => $value) {
            $values["etablissement.".$key] = $value;
        }
        // Champs de fusions liés à la catégorie d'établissement
        $values_mf_etablissement_categorie = $this->get_mf_etablissement_categorie("values");
        foreach ($values_mf_etablissement_categorie as $key => $value) {
            $values["etablissement.".$key] = $value;
        }
        // Gestion de l'exploitant
        $inst_expl = $this->get_inst_exploitant();
        $contact_values = $inst_expl->get_values_merge_fields();
        foreach ($contact_values as $key => $value) {
            $new_key = str_replace("contact.", "exploitant.", $key);
            $exploitant_values[$new_key] = $value;
        }
        $values = array_merge(
            $values,
            $exploitant_values
        );
        // Si clé primaire existante on la transmet pour la gestion spécifique des contraintes
        if ($this->getVal($this->clePrimaire) != '') {
            $values['etablissement.etablissement'] = $this->getVal($this->clePrimaire);
        }
        //
        return $values;
    }


    function get_labels_merge_fields() {
        //
        $labels = parent::get_labels_merge_fields();
        // Champs de fusions liés au type d'établissement
        $labels_mf_etablissement_type = $this->get_mf_etablissement_type("labels");
        foreach ($labels_mf_etablissement_type as $key => $value) {
            $labels[__("etablissement")]["etablissement.".$key] = $value;
        }
        // Champs de fusions liés à la catégorie d'établissement
        $labels_mf_etablissement_categorie = $this->get_mf_etablissement_categorie("labels");
        foreach ($labels_mf_etablissement_categorie as $key => $value) {
            $labels[__("etablissement")]["etablissement.".$key] = $value;
        }
        if ($this->f->is_option_unite_accessibilite_enabled() === true) {
            $labels[__("etablissement")]["etablissement.ua_validees"] = __("liste des UA validées");
        } else {
            $ua_fields = array(
                "tot_ua_valid",
                "acc_handicap_auditif",
                "acc_handicap_mental",
                "acc_handicap_physique",
                "acc_handicap_visuel",
                "acc_derogation_scda",
            );
            foreach ($ua_fields as $ua_field) {
                if (isset($labels[__("etablissement")]["etablissement.".$ua_field])) {
                    unset($labels[__("etablissement")]["etablissement.".$ua_field]);
                }
            }
        }
        // Gestion de l'exploitant
        $inst_expl = $this->get_inst_exploitant();
        $contact_labels = $inst_expl->get_labels_merge_fields();
        foreach ($contact_labels[__("contact")] as $key => $value) {
            $new_key = str_replace("contact.", "exploitant.", $key);
            $exploitant_labels[$new_key] = $value;
        }
        $labels[__("etablissement")] = array_merge(
            $labels[__("etablissement")],
            $exploitant_labels
        );
        // Adresse complète sur une seule ligne
        $labels[__("etablissement")]["etablissement.adresse_complete_sur_une_ligne"] = __("adresse complète sur une ligne");
        // Adresse complète sur quatre lignes
        $labels[__("etablissement")]["etablissement.adresse_complete_sur_quatre_lignes"] = __("adresse complète sur quatre lignes");
        //
        return $labels;
    }

    /**
     * Surcharge du fil d'ariane.
     *
     * @param string $ent Chaîne initiale
     *
     * @return chaîne de sortie
     */
    function getFormTitle($ent) {
        if ($this->getParameter("maj") == 0) {
            return $ent." -> +";
        } else {
            return $ent." -> ".$this->getVal("code")." - ".$this->getVal("libelle");
        }
    }

    /**
     * Définition des actions disponibles sur la classe.
     *
     * @return void
     */
    function init_class_actions() {

        // On récupère les actions génériques définies dans la méthode 
        // d'initialisation de la classe parente
        parent::init_class_actions();

        // ACTION - 004 - contrainte
        //
        $this->class_actions[4] = array(
            "identifier" => "contrainte",
            "view" => "view_contrainte",
            "permission_suffix" => "contrainte",
        );

        // ACTION - 005 - archiver
        //
        $this->class_actions[5] = array(
            "identifier" => "archiver",
            "portlet" => array(
                "type" => "action-direct",
                "libelle" => __("archiver"),
                "order" => 40,
                "class" => "zip-16",
            ),
            "view" => "formulaire",
            "method" => "archiver",
            "button" => "archiver",
            "permission_suffix" => "archiver",
            "condition" => "is_archivable",
        );

        // ACTION - 006 - desarchiver
        //
        $this->class_actions[6] = array(
            "identifier" => "desarchiver",
            "portlet" => array(
                "type" => "action-direct",
                "libelle" => __("desarchiver"),
                "order" => 50,
                "class" => "unzip-16",
            ),
            "view" => "formulaire",
            "method" => "desarchiver",
            "button" => "desarchiver",
            "permission_suffix" => "desarchiver",
            "condition" => "is_desarchivable",
        );

        // ACTION - 007 - masquer_si_periodicite_visites
        //
        $this->class_actions[7] = array(
            "identifier" => "masquer_si_periodicite_visites",
            "view" => "masquer_si_periodicite_visites",
            "permission_suffix" => "json",
        );
        
        // ACTION - 008 - view_numeroter
        //
        $this->class_actions[8] = array(
            "identifier" => "numeroter",
            "portlet" => array(
                "libelle" => _("numeroter"),
                "order" => 60,
            ),
            "crud" => "update",
            "view" => "view_numeroter",
            "permission_suffix" => "numeroter",
            "condition" => array(
                "is_option_etablissement_code_manual_enabled",
            ),
        );

        // ACTION - 009 - view_data_etablissement
        //
        $this->class_actions[9] = array(
            "identifier" => "data_etablissement",
            "view" => "view_data_etablissement",
            "permission_suffix" => "json",
        );
        // ACTION - 011 - etablissement-map
        //
        $this->class_actions[11] = array(
            "identifier" => "etablissement-map",
            "view" => "view_om_sig_map",
            "permission_suffix" => "consulter",
            "crud" => "read",
            "condition" => array(
                "exists",
                "is_option_sig_interne_enabled",
            ),
        );
        // ACTION - 012 - etablissement-summary
        //
        $this->class_actions[12] = array(
            "identifier" => "etablissement-summary",
            "view" => "view_summary",
            "permission_suffix" => "consulter",
            "crud" => "read",
            "condition" => array(
                "exists",
                "is_option_sig_interne_enabled",
            ),
        );
        // ACTION - 015 - view_synthesis
        //
        $this->class_actions[15] = array(
            "identifier" => "synthesis",
            "view" => "view_synthesis",
            "permission_suffix" => "consulter",
        );

        // ACTION - 015 - view_lien_referentiel_patrimoine
        //
        $this->class_actions[16] = array(
            "identifier" => "lien_referentiel_patrimoine",
            "view" => "view_lien_referentiel_patrimoine",
            "permission_suffix" => "consulter",
        );

        // ACTION - 020 - view_ua_tab
        //
        $this->class_actions[20] = array(
            "identifier" => "ua-tab",
            "view" => "view_ua_tab",
            "permission_suffix" => "consulter",
        );

        // ACTION - 030 - Redirection vers le SIG
        $this->class_actions[30] = array(
            "identifier" => "localiser",
            "view" => "view_localiser",
            "permission_suffix" => "consulter",
            "condition" => "is_option_sig_enabled",
        );

        // ACTION - 032 - geocoder
        //
        $this->class_actions[32] = array(
            "identifier" => "geocoder",
            "portlet" => array(
                "type" => "action-direct",
                "libelle" => __("géolocaliser"),
                "order" => 110,
                "class" => "sig-16",
            ),
            "view" => "formulaire",
            "method" => "geocoder",
            "permission_suffix" => "consulter",
            "condition" => array(
                "is_not_geolocalised",
                "is_option_sig_enabled",
            ),
        );

        // ACTION - 033 - plot_owner
        //
        $this->class_actions[33] = array(
            "identifier" => "plot_owner",
            "view" => "view_plot_owner",
            "permission_suffix" => "consulter",
            "condition" => "is_option_sig_enabled",
        );

        // ACTION - 035 - direct_search
        // Vue cible lors d'une recherche directe
        $this->class_actions[35] = array(
            "identifier" => "direct_search",
            "view" => "view_direct_search",
            "permission_suffix" => "consulter",
        );

        // ACTION - 031 - Redirection vers le SIG pour la sélection d'établissements
        $this->class_actions[31] = array(
            "identifier" => "localiser_selection",
            "view" => "view_localiser_selection",
            "permission_suffix" => "consulter",
            "condition" => "is_option_sig_enabled",
        );
    }

    /**
     * SETTER_FORM - setType.
     *
     * @return void
     */
    function setType(&$form, $maj) {
        parent::setType($form, $maj);

        // En mode "ajout" et "modifier"
        if ($maj == 0 || $maj == 1) {
            // Champ en autocomplete
            $form->setType('adresse_voie', 'autocomplete');
            $form->setType('geolocalise', 'hidden');
            $form->setType("etablissement_type_secondaire", "select_multiple");
        }
        if ($maj == 0) {
            if ($this->is_option_etablissement_code_manual_enabled() === true) {
                $form->setType('code', 'text');
            } else {
                $form->setType('code', 'hidden');
            }
            $form->setType('si_type_ssi','select');
            $form->setType('si_type_alarme','select');
        }
        if ($maj == 1) {
            $form->setType('code', 'hiddenstatic');
            $form->setType('si_type_ssi','select');
            $form->setType('si_type_alarme','select');
        }
        // En mode "supprimer" et "consulter"
        if ($maj == 2 || $maj == 3) {
            $form->setType("etablissement_type_secondaire", "select_multiple_static");
        }
        //
        $form->setType('geom_point', 'hidden');
        $form->setType('geom_emprise', 'hidden');

        // Pour les actions appelée en POST en Ajax, il est nécessaire de
        // qualifier le type de chaque champs (Si le champ n'est pas défini un
        // PHP Notice:  Undefined index dans core/om_formulaire.class.php est
        // levé). On sélectionne donc les actions de portlet de type
        // action-direct ou assimilé et les actions spécifiques avec le même
        // comportement.
        if ($this->get_action_param($maj, "portlet_type") == "action-direct"
            || $this->get_action_param($maj, "portlet_type") == "action-direct-with-confirmation") {
            //
            foreach ($this->champs as $key => $value) {
                $form->setType($value, 'hidden');
            }
            $form->setType($this->clePrimaire, "hiddenstatic");
        }
        // Si on n'est pas en mode "ajout"
        if ($maj == 1 || $maj == 2 || $maj == 3) {
            // On récupère l'éventuelle date d'archivage
            $om_validite_fin = $this->getVal("om_validite_fin");
            $form->setType('etablissement', 'hidden');
        }

        // En mode "ajout"
        if ($maj == 0) {

            // On cache la date d'archivage
            $form->setType('om_validite_debut', 'hiddendate');
            $form->setType('om_validite_fin', 'hiddendate');
            
            // Cache le champ "etablissement_tutelle_adm"
            $form->setType('etablissement_tutelle_adm', 'selecthidden');
            // Cache le champ "ref_patrimoine"
            $form->setType('ref_patrimoine', 'referentielpatrimoinehidden');
        }

        // En mode "modification"
        if ($maj == 1) {

            $etablissement_statut_juridique_code = 
                $this->get_etablissement_statut_juridique_code($this->getVal("etablissement_statut_juridique"));

            // On cache le champ "etablissement_tutelle_adm" si statut juridique différent de public
            if ($etablissement_statut_juridique_code != 'pub') {
                $form->setType('etablissement_tutelle_adm', 'selecthidden');
            }
            
            // On cache le champ "ref_patrimoine" si statut juridique différent de ville
            if ($etablissement_statut_juridique_code != 'vle') {
                $form->setType('ref_patrimoine', 'referentielpatrimoinehidden');
            }
            else{
                $form->setType('ref_patrimoine', 'referentielpatrimoine');
            }

            // S'il y a une date de validité de fin et qu'elle est antérieure ou égale au jour en cours c'est que l'état est archivé
            if ($om_validite_fin != NULL && $om_validite_fin <= date('Y-m-d')) {
                // On rend non modifiable les dates de création et d'archivage
                $form->setType('om_validite_debut', 'hiddenstaticdate');
                $form->setType('om_validite_fin', 'hiddenstaticdate');
            }
            else{ // si désarchivé
                // On rend non modifiable la date de création et masque celle d'archivage
                $form->setType('om_validite_debut', 'hiddenstaticdate');
                $form->setType('om_validite_fin', 'hiddendate');
            }
        }

        // En modes "ajout" et "modification"
        if ($maj == 0 || $maj == 1) {

            //
            $form->setType('references_cadastrales','referencescadastrales');

            // Transformation des champs de type booléen en select
            $form->setType("exp_civilite", 'select');
            $form->setType("si_conformite_l16", 'select');
            $form->setType("si_alimentation_remplacement", 'select');
            $form->setType("si_service_securite", 'select');

            //
            $form->setType("exp_adresse_voie", 'text');

            // On cache le message qu'il n'y a aucun exploitant
            $form->setType('exp','hidden');

            // On type la copie d'adresse en case à cocher
            $form->setType('meme_adresse','checkbox');
            // et son interligne pour l'affichage
            $form->setType('exp_blank','static');

            // On cache le champ de la durée de la périodicité des visites
            $form->setType('si_periodicite_visites', 'hidden');
            $form->setType('si_prochaine_visite_periodique_date_previsionnelle', 'hiddendate');
            //
            $form->setType('dossier_coordination_periodique', 'hidden');
            //
            $form->setType("si_prochaine_visite_date", 'hidden');
            $form->setType("si_prochaine_visite_type", 'hidden');
            // Le suivi des visites se fait automatiquement mais certains
            // champs sont modiables manuellement si une permission
            // spécifique leur est attribuée.
            if ($this->f->isAccredited("etablissement_modifier_suivi_dernieres_visites") === true) {
                //
                $form->setType("si_derniere_visite_date", 'date');
                $form->setType("si_derniere_visite_avis", 'select');
                $form->setType("si_derniere_visite_technicien", 'select');
                //
                $form->setType("acc_derniere_visite_date", 'date');
                $form->setType("acc_derniere_visite_avis", 'select');
                $form->setType("acc_derniere_visite_technicien", 'select');
            } else {
                //
                $form->setType("si_derniere_visite_date", 'hidden');
                $form->setType("si_derniere_visite_avis", 'hidden');
                $form->setType("si_derniere_visite_technicien", 'hidden');
                //
                $form->setType("acc_derniere_visite_date", 'hidden');
                $form->setType("acc_derniere_visite_avis", 'hidden');
                $form->setType("acc_derniere_visite_technicien", 'hidden');
            }
        }

        // En mode consultation
        if ($maj == 3) {
            //
            $form->setType('dossier_coordination_periodique', 'link');
        }

        // En mode "consultation" et "suppression"
        if ($maj == 2 || $maj == 3) {

            $etablissement_statut_juridique_code = 
                $this->get_etablissement_statut_juridique_code($this->getVal("etablissement_statut_juridique"));

            // On cache le champ "etablissement_tutelle_adm" si statut juridique différent de public
            if ($etablissement_statut_juridique_code != 'pub') {
              $form->setType('etablissement_tutelle_adm', 'selecthidden');
            }

            // On cache le champ "ref_patrimoine" si statut juridique différent de ville
            if ($etablissement_statut_juridique_code != 'vle') {
                $form->setType('ref_patrimoine', 'referentielpatrimoinehidden');
            }
            else{
                $form->setType('ref_patrimoine', 'referentielpatrimoinestatic');
            }

            // On cache la copie d'adresse et son interligne
            $form->setType('meme_adresse','hidden');
            $form->setType('exp_blank','hidden');

            // Transformation des champs de type booléen en select statique
            $form->setType('references_cadastrales','referencescadastralesstatic');
            $form->setType("exp_civilite", 'selectstatic');
            $form->setType("si_conformite_l16", 'selectstatic');
            $form->setType("si_alimentation_remplacement", 'selectstatic');
            $form->setType("si_service_securite", 'selectstatic');

            //
            $form->setType("exp_adresse_voie", 'textstatic');
            if ($this->getVal('geolocalise') === 't') {
                $form->setType('geolocalise', 'link_geolocalisation');
            }

            // On cache le message qu'il n'y a aucun exploitant
            // s'il y en a un
            if ($this->recuperer_ID_exploitant($this->getVal('etablissement')) != "]") {

                $form->setType('exp','hidden');
            } else { // sinon on cache les champs exploitant

                $form->setType('exp_civilite','hidden');
                $form->setType('exp_nom','hidden');
                $form->setType('exp_prenom','hidden');
                $form->setType('exp_adresse_numero','hidden');
                $form->setType('exp_adresse_numero2','hidden');
                $form->setType('exp_adresse_voie','hidden');
                $form->setType('exp_adresse_complement','hidden');
                $form->setType('exp_adresse_cp','hidden');
                $form->setType('exp_adresse_ville','hidden');
                $form->setType('exp_lieu_dit','hidden');
                $form->setType('exp_boite_postale','hidden');
                $form->setType('exp_cedex','hidden');
                $form->setType('exp_pays','hidden');
            }

            // S'il y a une date de validité de fin et qu'elle est antérieure ou égale au jour en cours c'est que l'état est archivé
            if ($om_validite_fin != NULL && $om_validite_fin <= date('Y-m-d')) {
                // On rend non modifiable les dates de création et d'archivage
                $form->setType('om_validite_debut', 'hiddenstaticdate');
                $form->setType('om_validite_fin', 'hiddenstaticdate');
            }
            else{ // si désarchivé
                // On rend non modifiable la date de création et masque celle d'archivage
                $form->setType('om_validite_debut', 'hiddenstaticdate');
                $form->setType('om_validite_fin', 'hiddendate');
            }

        }
        // On affiche les champs calculés des UA uniquement en mode consultation
        // et si il y a des UA sur l'établissement.
        if ($this->f->is_option_unite_accessibilite_enabled() !== true
            || $maj != 3) {
            $form->setType('tot_ua_valid', 'hidden');
        }
        if ($this->f->is_option_unite_accessibilite_enabled() !== true
            || $this->get_tot_ua_valid() === 0
            || $maj != 3) {
            $form->setType('acc_handicap_auditif', 'hidden');
            $form->setType('acc_handicap_mental', 'hidden');
            $form->setType('acc_handicap_physique', 'hidden');
            $form->setType('acc_handicap_visuel', 'hidden');
            $form->setType('acc_derogation_scda', 'hidden');
        }
    }

    /**
     * SETTER_FORM - setTaille.
     *
     * @return void
     */
    function setTaille(&$form, $maj) {
        parent::setTaille($form, $maj);
        // longueur maximale d'une chaine à convertir en entier
        // 32 bits:  9 (2147483647)
        // 64 bits: 18 (9223372036854775807)
        $max_int_size = strlen(PHP_INT_MAX) -1;
        // ajout des champs de l'exploitant
        $exp_tailles = array(
            'civilite' => 11,
            'nom' => 30,
            'prenom' => 30,
            'adresse_numero' => $max_int_size,
            'adresse_numero2' => 10,
            'adresse_voie' => 30,
            'adresse_complement' => 30,
            'adresse_cp' => 10,
            'adresse_ville' => 30,
            'lieu_dit' => 30,
            'boite_postale' => 10,
            'cedex' => 10,
            'pays' => 30,
        );
        foreach ($exp_tailles as $key => $value) {
            $form->setTaille('exp_' . $key, $value);
        }
        // protection des entiers
        $int_fields = array(
            'adresse_numero',
            'si_effectif_public',
            'si_effectif_personnel',
            'si_periodicite_visites',
            'si_personnel_jour',
            'si_personnel_nuit',
        );
        foreach ($int_fields as $key) {
            $form->setTaille($key, $max_int_size);
        }
    }

    /**
     * SETTER_FORM - setMax.
     *
     * @return void
     */
    function setMax(&$form, $maj) {
        parent::setMax($form, $maj);
        //
        $max_int_size = 9;
        // ajout des champs de l'exploitant
        $max = array(
            'civilite' => 11,
            'nom' => 100,
            'prenom' => 100,
            'adresse_numero' => $max_int_size,
            'adresse_numero2' => 10,
            'adresse_voie' => 255,
            'adresse_complement' => 40,
            'adresse_cp' => 6,
            'adresse_ville' => 40,
            'lieu_dit' => 39,
            'boite_postale' => 5,
            'cedex' => 5,
            'pays' => 40,
        );
        foreach ($max as $key => $value) {
            $form->setMax('exp_' . $key, $value);
        }
        // protection des entiers
        $int_fields = array(
            'adresse_numero',
            'si_effectif_public',
            'si_effectif_personnel',
            'si_periodicite_visites',
            'si_personnel_jour',
            'si_personnel_nuit',
        );
        foreach ($int_fields as $key) {
            $form->setMax($key, $max_int_size);
        }
    }

    /**
     * SETTER_FORM - setSelect.
     *
     * @return void
     */
    function setSelect(&$form, $maj, &$dnu1 = null, $dnu2 = null) {
        parent::setSelect($form, $maj);
        // En mode "ajouter" et "modifier"
        if ($maj == 0 || $maj == 1) {
            //
            $form->setSelect(
                "adresse_voie",
                $this->get_widget_config__adresse_voie__autocomplete()
            );
            // Initialise le select en fonction de la valeur d'un autre champ
            $form->setSelect('adresse_arrondissement', $this->load_select_adresse_arrondissement($form, $maj, "adresse_voie"));

            /* Type SSI */
            $ssi = array();
            $ssi[0][0] = 'NC';
            $ssi[1][0] = 'NC';
            $ssi[0][1] = 'A';
            $ssi[1][1] = 'A';
            $ssi[0][2] = 'B';
            $ssi[1][2] = 'B';
            $ssi[0][3] = 'C';
            $ssi[1][3] = 'C';
            $ssi[0][4] = 'D';
            $ssi[1][4] = 'D';
            $ssi[0][5] = 'E';
            $ssi[1][5] = 'E';
            $form->setSelect("si_type_ssi", $ssi);

            /* Type SSI */
            $alarme = array();
            $alarme[0][0] = 'NC';
            $alarme[1][0] = 'NC';
            $alarme[0][1] = '1';
            $alarme[1][1] = '1';
            $alarme[0][2] = '2a';
            $alarme[1][2] = '2a';
            $alarme[0][3] = '2b';
            $alarme[1][3] = '2b';
            $alarme[0][4] = '3';
            $alarme[1][4] = '3';
            $alarme[0][5] = '4';
            $alarme[1][5] = '4';
            $form->setSelect("si_type_alarme", $alarme);
        }

        // Si formulaire en consultation et sig activé
        if ($this->getParameter('maj') === '3'
            && $this->is_option_sig_enabled() === true) {
            // Paramètre obligatoires pour l'affichage de la liste des
            // propriétaires
            $params = array();
            $params['obj'] = $this->get_absolute_class_name();
            $params['idx'] = $this->getParameter('idx');
            $params['action'] = '33';
            $params['title'] = __('Liste des propriétaires');
            //
            $form->setSelect('references_cadastrales', $params);
        }

        // Types secondaires
        $etablissement_id = 0;
        if (isset($form->val['etablissement']) === true
            && $form->val['etablissement'] != null
            && $form->val['etablissement'] != ''
            && $form->val['etablissement'] != ']') {
            //
            $etablissement_id = $form->val['etablissement'];
        }
        //
        $sql_etablissement_type_secondaire_by_id = str_replace('<idx>', $etablissement_id, $this->get_var_sql_forminc__sql("etablissement_type_secondaire_by_id"));
        $this->init_select($form, $this->f->db, $maj, null, "etablissement_type_secondaire", $this->get_var_sql_forminc__sql("etablissement_type_secondaire"), $sql_etablissement_type_secondaire_by_id, true, true);

        // Transformation des champs de type booléen en select oui/non
        // au lieu de la case à cocher
        // On transforme les checkbox en select pour la gestion du null
        $booleens = array();
        $booleens[0][0] = '';
        $booleens[1][0] = __("NC");
        $booleens[0][1] = 't';
        $booleens[1][1] = __("Oui");
        $booleens[0][2] = 'f';
        $booleens[1][2] = __("Non");
        $form->setSelect("si_conformite_l16", $booleens);
        $form->setSelect("si_alimentation_remplacement", $booleens);
        $form->setSelect("si_service_securite", $booleens);
        
        //Option du référentiel patrimoine
        $option_referentiel_patrimoine = array(
            "option_referentiel_patrimoine" => $this->f->getParameter("option_referentiel_patrimoine")
        );
        // Gestion du bouton  
        $form->setSelect("ref_patrimoine", $option_referentiel_patrimoine);

        //
        $this->init_select($form, $this->f->db, $maj, null, "exp_civilite",
                           $this->get_var_sql_forminc__sql("civilite"), $this->get_var_sql_forminc__sql("civilite_by_id"), false);

        // En mode consulter
        if ($maj == 3) {
            // Paramètres envoyés au type link du champ
            // dossier_coordination_periodique
            $params = array();
            $params['obj'] = "dossier_coordination";
            $params['idx'] = $this->getVal('dossier_coordination_periodique');
            // Instance de la classe dossier_coordination
            $dossier_coordination = $this->f->get_inst__om_dbform(array(
                "obj" => "dossier_coordination",
                "idx" => $params['idx'],
            ));
            $params['libelle'] = $dossier_coordination->getVal("libelle");
            $form->setSelect("dossier_coordination_periodique", $params);
        }
    }

    /**
     * SETTER_FORM - setLib.
     *
     * @return void
     */
    function setLib(&$form, $maj) {
        parent::setLib($form, $maj);
        $form->setLib('etablissement_type', __('type principal'));
        $form->setLib('etablissement_type_secondaire', __('type(s) secondaire(s)'));
        $form->setLib('exp','');
        $form->setLib('exp_blank','');
        $form->setLib('meme_adresse',__("Meme adresse que celle de l'etablissement"));
        $form->setLib('om_validite_debut', __("Date de création de la fiche"));
        $form->setLib('om_validite_fin', __("Date d'archivage"));
        $form->setLib('acc_handicap_auditif', __("acc_handicap_auditif"));
        $form->setLib('acc_handicap_mental', __("acc_handicap_mental"));
        $form->setLib('acc_handicap_physique', __("acc_handicap_physique"));
        $form->setLib('acc_handicap_visuel', __("acc_handicap_visuel"));
        $form->setLib('acc_derogation_scda', __("acc_derogation_scda"));
        $form->setLib('geolocalise', __('Géolocalisé'));
        
        // En modes "ajouter" et "modifier" ainsi que recherche avancée
        if ($maj == 999 || $maj == 0 || $maj == 1) {

            $form->setLib('exp_civilite', __('exp_civilite'));
            $form->setLib('exp_nom', __('exp_nom'));
            $form->setLib('exp_prenom', __('exp_prenom'));
            $form->setLib('exp_adresse_numero', __('exp_adresse_numero'));
            $form->setLib('exp_adresse_numero2', __('exp_adresse_numero2'));
            $form->setLib('exp_adresse_voie', __('exp_adresse_voie'));
            $form->setLib('exp_adresse_complement', __('exp_adresse_complement'));
            $form->setLib('exp_adresse_cp', __('exp_adresse_cp'));
            $form->setLib('exp_adresse_ville', __('exp_adresse_ville'));
            $form->setLib('exp_lieu_dit', __('exp_lieu_dit'));
            $form->setLib('exp_boite_postale', __('exp_boite_postale'));
            $form->setLib('exp_cedex', __('exp_cedex'));
            $form->setLib('exp_pays', __('exp_pays'));
        }

        // En modes "supprimer" et "consulter"
        if ($maj == 2 || $maj == 3) {

            $form->setLib('etablissement_etat','');
            $form->setLib('etablissement_statut_juridique','');
            $form->setLib('adresse_numero','');
            $form->setLib('adresse_numero2','');
            $form->setLib('adresse_voie','');
            $form->setLib('adresse_complement','');
            $form->setLib('adresse_arrondissement','');
            $form->setLib('adresse_cp','');
            $form->setLib('adresse_ville','');
            $form->setLib('lieu_dit','');
            $form->setLib('cedex','');
            $form->setLib('boite_postale','');
            $form->setLib('exp_civilite','');
            $form->setLib('exp_nom','');
            $form->setLib('exp_prenom','');
            $form->setLib('exp_adresse_numero','');
            $form->setLib('exp_adresse_numero2','');
            $form->setLib('exp_adresse_voie','');
            $form->setLib('exp_adresse_complement','');
            $form->setLib('exp_adresse_cp','');
            $form->setLib('exp_adresse_ville','');
            $form->setLib('exp_lieu_dit','');
            $form->setLib('exp_boite_postale','');
            $form->setLib('exp_cedex','');
            $form->setLib('exp_pays','');
            $form->setLib('tot_ua_valid',__("Unites d'accessibilite"));
        }
    }

    /**
     * SETTER_FORM - setOnchange.
     *
     * @return void
     */
    function setOnchange(&$form, $maj) {
        parent::setOnchange($form, $maj);

        //
        $form->setOnchange('adresse_voie',
            'filterSelect(this.value,
            \'adresse_arrondissement\',
            \'adresse_voie\',
            \'etablissement\')');
        
        // ... sur le contrôle du statut
        $form->setOnchange('etablissement_statut_juridique','verifierStatutJuridique(this.value);');

        // ... sur la copie d'adresse
        $form->setOnchange('meme_adresse','copierAdresseEtablissement(this.value);');

        // ... sur le changement de nature
        $form->setOnchange('etablissement_nature','masquerPeriodiciteVisites();');

        // ... sur le changement de type
        $form->setOnchange('etablissement_type','masquerPeriodiciteVisites();');
    }

    /**
     * SETTER_FORM - setLayout.
     *
     * @return void
     */
    function setLayout(&$form, $maj) {
        // CONTAINER GLOBAL - BEGIN
        // Début "Container global"
        $form->setBloc('etablissement', 'D', "", "form-etablissement-action-".$maj);
        
        // DETAILS - BEGIN
        // Début "Détails sécurité et accessibilité"
        $form->setBloc('acc_consignes_om_html', 'D', "", "bloc-section bloc-details-etablissement");
        $form->setFieldset('acc_consignes_om_html', 'D', __('Details'), "startClosed");
        // COL 1 : Accessibilité
        $form->setBloc('acc_consignes_om_html', 'D', "", "col_6");
            $form->setBloc('acc_consignes_om_html', 'D', "", "col_12");
            $form->setFieldset('acc_consignes_om_html', 'D', __('Accessibilite'));
            $form->setFieldset('acc_derogation_scda', 'F', '');
            $form->setBloc('acc_derogation_scda', 'F', '');
        $form->setBloc('acc_derogation_scda', 'F', '');
        // COL 2 : Sécurité
        $form->setBloc('si_consignes_om_html', 'D', "", "col_6");
            $form->setBloc('si_consignes_om_html', 'D', "", "col_12");
            $form->setFieldset('si_consignes_om_html', 'D', __('Securite'));
            $form->setFieldset('si_personnel_nuit', 'F', '');
            $form->setBloc('si_personnel_nuit', 'F', '');
        $form->setBloc('si_personnel_nuit', 'F', '');
        // Fin "Détails sécurité et accessibilité"
        $form->setFieldset('si_personnel_nuit', 'F', '');
        $form->setBloc('si_personnel_nuit', 'F', '');
        // DETAILS - END

        // FICHE ETABLISSEMENT - BEGIN
        // Début "Fiche établissement"
        $form->setBloc('code', 'D', "", "bloc-section bloc-fiche-etablissement");
        $form->setFieldset('code', 'D', __('Fiche etablissement'), "collapsible");
        //
        $form->setBloc('code', 'D', "", "col_6"); // COL 1

        // Fieldset établissement
        $form->setBloc('code', 'D', "", "col_12");
        $form->setFieldset('code', 'D', __('Etablissement'), "");
            if($maj >= 2) {
                $form->setBloc('adresse_numero', 'D', __('Adresse'), '');
                    $form->setBloc('adresse_numero', 'D', '', 'group');
                    $form->setBloc('adresse_voie', 'F');
                    
                    $form->setBloc('adresse_complement', 'DF');
                    
                    $form->setBloc('lieu_dit', 'D', '', 'group');
                    $form->setBloc('boite_postale', 'F');
                    $form->setBloc('adresse_cp', 'D', '', 'group');
                    $form->setBloc('cedex', 'F');
                $form->setBloc('cedex', 'F');

            }
            if ($maj >= 2) {
                $form->setBloc('etablissement_statut_juridique', 'D', __('etablissement_statut_juridique'));
                    // gestion de la mauvaise gestion du groupe s'il est fermé
                    // par un champ caché,  ce qui est le cas
                    // lorsque l'établissement est public
                    if ($this->get_etablissement_statut_juridique_code($this->getVal("etablissement_statut_juridique")) == 'pub') {
                        $form->setBloc('etablissement_statut_juridique', 'D', '', 'group etablissement_tutelle_adm'.$this->form->val["etablissement_tutelle_adm"]);
                        $form->setBloc('etablissement_tutelle_adm', 'F');
                    }
                    
                $form->setBloc('etablissement_tutelle_adm', 'F');
            }
        $form->setFieldset('etablissement_tutelle_adm', 'F', '');
        $form->setBloc('etablissement_tutelle_adm', 'F', '');
        
        // Fieldset exploitant
        $form->setBloc('exp', 'D', "", "col_12");
        $form->setFieldset('exp', 'D', __('Exploitant'), '');
            if($maj >= 2) {
                $form->setBloc('exp_civilite', 'D', __('Identite'));
                    $form->setBloc('exp_civilite',  'D',  '',  'group');
                    $form->setBloc('exp_prenom', 'F');
                $form->setBloc('exp_prenom', 'F');
                $form->setBloc('exp_adresse_numero', 'D', __('Adresse'), '');
                    $form->setBloc('exp_adresse_numero', 'D', '', 'group');
                    $form->setBloc('exp_adresse_voie', 'F');
                    //
                    $form->setBloc('exp_adresse_complement', 'D');
                    $form->setBloc('exp_adresse_complement', 'F');
                    //
                    $form->setBloc('exp_lieu_dit', 'D', '', 'group');
                    $form->setBloc('exp_boite_postale', 'F');
                    //
                    $form->setBloc('exp_adresse_cp', 'D', '', 'group');
                    $form->setBloc('exp_cedex', 'F');
                    //
                    $form->setBloc('exp_pays', 'D');
                    $form->setBloc('exp_pays', 'F');
                $form->setBloc('exp_pays', 'F');
            }
        $form->setFieldset('exp_pays', 'F', '');
        $form->setBloc('exp_pays', 'F');

        // Fieldset localisation
        $form->setBloc('references_cadastrales', 'D', "", "col_12");
        $form->setFieldset('references_cadastrales', 'D', __('Localisation'), '');
        $form->setFieldset('geolocalise', 'F');
        $form->setBloc('geolocalise', 'F');

        $form->setBloc('geolocalise', 'F'); // FIN COL 1

        $form->setBloc('etablissement_type', 'D', "", "col_6"); // COL 2

        // Fieldset statut
        $form->setBloc('etablissement_type', 'D', "", "col_12");
        $form->setFieldset('etablissement_type', 'D', __('Statut'), "");
            if($maj >= 2) {
              $form->setBloc('etablissement_type', 'D', __('statut réglementaire'));
                $form->setBloc('etablissement_type', 'D', '', 'group floatleft etablissement_etat'.$this->form->val["etablissement_etat"]);
                $form->setBloc('etablissement_etat', 'F');
              $form->setBloc('etablissement_etat', 'F');
            }
        $form->setFieldset('om_validite_fin', 'F', '');
        $form->setBloc('om_validite_fin', 'F');

        // Fieldset détail technique
        $form->setBloc('si_effectif_public', 'D', "", "col_12"); 
        $form->setFieldset('si_effectif_public', 'D', __('Detail technique'), "");
            if($maj >= 2) {
              $form->setBloc('si_effectif_public', 'D', __('effectif'), '');
                $form->setBloc('si_effectif_public', 'D', '', 'group floatleft');
                $form->setBloc('si_effectif_personnel', 'F');
              $form->setBloc('si_effectif_personnel', 'F');
            }
        $form->setFieldset('si_visite_duree', 'F', '');
        $form->setBloc('si_visite_duree', 'F');

        // Fieldset suivi des visites
        $form->setBloc('si_autorite_competente_visite', 'D', "", "col_12");
        $form->setFieldset('si_autorite_competente_visite', 'D', __('Suivi des visites'), "");
        // Visites sécurité
        $form->setBloc('si_autorite_competente_visite', 'D', __("Visites securite"), "bloc-visites bloc-visites-securite");
        $form->setBloc('si_periodicite_visites', 'D', __("Périodique"), "sousbloc-visites bloc-periodicite-visites");
        $form->setBloc('dossier_coordination_periodique', 'F');
        $form->setBloc('si_derniere_visite_date', 'D', __("Dernière"), "sousbloc-visites bloc-derniere-visite");
        $form->setBloc('si_derniere_visite_technicien', 'F');
        $form->setBloc('si_prochaine_visite_date', 'D', __("Prochaine"), "sousbloc-visites bloc-prochaine-visite");
        $form->setBloc('si_prochaine_visite_type', 'F');
        $form->setBloc('si_prochaine_visite_type', 'F');
        // Visites accessibilité
        $form->setBloc('acc_derniere_visite_date', 'D', __("Visites accessibilite"), "bloc-visites bloc-visites-accessibilite");
        $form->setBloc('acc_derniere_visite_date', 'D', __("Dernière"), "sousbloc-visites bloc-derniere-visite");
        $form->setBloc('acc_derniere_visite_technicien', 'F');
        $form->setBloc('acc_derniere_visite_technicien', 'F');
        $form->setFieldset('acc_derniere_visite_technicien', 'F', '');
        $form->setBloc('acc_derniere_visite_technicien', 'F');

        // Fieldset suivi des plans
        $form->setBloc('si_autorite_competente_plan', 'D', "", "col_12");
        $form->setFieldset('si_autorite_competente_plan', 'D', __('Suivi des plans'), "");
        // Plans sécurité
        $form->setBloc('si_autorite_competente_plan', 'D', __("Plans sécurité"), "bloc-visites bloc-visites-securite");
        $form->setBloc('si_autorite_competente_plan', 'D', "", "bloc-periodicite-visites");
        $form->setBloc('si_dernier_plan_avis', 'F');
        $form->setBloc('si_dernier_plan_avis', 'F');

        $form->setBloc('si_dernier_plan_avis', 'F'); // FIN COL 2

        // Fin "Fiche établissement"
        $form->setFieldset('si_dernier_plan_avis', 'F');
        $form->setBloc('si_dernier_plan_avis', 'F');
        // FICHE ETABLISSEMENT - END

        // Fin "Container global"
        $form->setBloc(end($this->champs), 'F');
        // CONTAINER GLOBAL - END
    }

    /**
     * SETTER_FORM - setVal (setVal).
     *
     * @return void
     */
    function setVal(&$form, $maj, $validation, &$dnu1 = null, $dnu2 = null) {
        parent::setVal($form, $maj, $validation);
        //
        if ($validation == 0 && $maj == 0) {
                
            $form->setVal("adresse_ville", $this->f->getParameter("ville"));
            $form->setVal("exp_pays", "France");
            $form->setVal("om_validite_debut", date('Y-m-d'));
            // modifier après
            //  type exploitant
            //$this->exploitant->setVal('contact_type', 1);
            // attaché à l'établissement en cours
            //$this->exploitant->setVal('etablissement', $this->getVal('etablissement'));
        }

        if ($maj > 0) {

            // Si un exploitant est attaché à l'établissement
            if ($this->recuperer_ID_exploitant($this->getVal('etablissement')) != "]") {

                $tempBP = $this->exploitant->getVal('boite_postale');
                $tempCEDEX = $this->exploitant->getVal('cedex');

                $form->setVal("exp_civilite", $this->exploitant->getVal('civilite'));
                $form->setVal("exp_nom", $this->exploitant->getVal('nom'));
                $form->setVal("exp_prenom", $this->exploitant->getVal('prenom'));
                $form->setVal("exp_adresse_numero", $this->exploitant->getVal('adresse_numero'));
                $form->setVal("exp_adresse_numero2", $this->exploitant->getVal('adresse_numero2'));
                $form->setVal("exp_adresse_voie", $this->exploitant->getVal('adresse_voie'));
                $form->setVal("exp_adresse_complement", $this->exploitant->getVal('adresse_complement'));
                $form->setVal("exp_adresse_cp", $this->exploitant->getVal('adresse_cp'));
                $form->setVal("exp_adresse_ville", $this->exploitant->getVal('adresse_ville'));
                $form->setVal("exp_lieu_dit", $this->exploitant->getVal('lieu_dit'));
                $form->setVal("exp_pays", $this->exploitant->getVal('pays'));
                
                if ($maj >= 2) {
                    if ($tempCEDEX != '') {
                        $form->setVal('exp_cedex', __('Cedex').' '.$tempCEDEX);
                    }
                    if ($tempBP != '') {
                        $form->setVal('exp_boite_postale', __('BP').' '.$tempBP);
                    }
                }  else {
                    $form->setVal("exp_boite_postale", $tempBP);
                    $form->setVal("exp_cedex", $tempCEDEX);
                }
            }       
        }

        if ($maj >= 2) {
            if (is_array($form->val) === true
                && array_key_exists("cedex", $form->val) === true
                && $form->val['cedex'] != '') {
                //
                $form->setVal('cedex', __('Cedex').' '.$form->val['cedex']);
            }
            if (is_array($form->val) === true
                && array_key_exists("boite_postale", $form->val) === true
                && $form->val['boite_postale'] != '') {
                //
                $form->setVal('boite_postale', __('BP').' '.$form->val['boite_postale']);
            }
        }
        if ($maj == 3) {
            if ($this->is_subject_to_periodic() !== true) {
                $form->setVal('si_periodicite_visites', "NA");
                $form->setVal('si_prochaine_visite_periodique_date_previsionnelle', "NA");
            } else {
                if ($this->getVal('si_periodicite_visites') != "") {
                    $form->setVal('si_periodicite_visites', $this->getVal('si_periodicite_visites')." ans");
                }
            }
        }
    }

    /**
     * SETTER_FORM - set_form_default_values (setVal).
     *
     * @return void
     */
    function set_form_default_values(&$form, $maj, $validation) {
        // Code de l'établissement automatique par défaut
        if ($maj == 0) {
            $form->setVal("code", "AUTO");
        }
        // En mode supprimer et consulter
        if ($maj == 2 || $maj == 3) {
            // Nb total d'UA validées
            $tot_ua_valid = $this->get_tot_ua_valid();
            $form->setVal("tot_ua_valid", $this->get_tot_ua_valid());
            // S'il y en a on calcule ses champs
            if ($tot_ua_valid > 0) {
                $ua = $this->f->get_inst__om_dbform(array(
                    "obj" => "etablissement_unite",
                    "idx" => 0,
                ));
                $champs_acc = array(
                    "acc_handicap_auditif",
                    "acc_handicap_mental",
                    "acc_handicap_physique",
                    "acc_handicap_visuel",
                );
                $id_etablissement = intval($this->getVal($this->clePrimaire));
                foreach ($champs_acc as $champ) {
                    $form->setVal($champ, $ua->get_general_value_switch_field_by_etablissement($id_etablissement, $champ));
                }
                if ($ua->is_any_ua_having_scda($id_etablissement) === true) {
                    $form->setVal('acc_derogation_scda', __('Oui'));
                }
            }
        }
    }

    /**
     * VIEW - view_direct_search.
     *
     * @return void
     */
    function view_direct_search() {
        $this->checkAccessibility();
        // Si le préfixe du code d'établisssement n'est pas renseigné on l'ajoute automatiquement
        // si la valeur postée est numérique
        $etablissement_code_prefixe = trim($this->f->getParameter("etablissement_code_prefixe"));
        $posted_value = trim($this->f->get_submitted_post_value("filter"));
        if ($etablissement_code_prefixe != ""
            && $this->f->starts_with(strtolower($posted_value), strtolower($etablissement_code_prefixe)) !== true
            && is_numeric($posted_value) === true) {
            $posted_value = $etablissement_code_prefixe.$posted_value;
        }
        //
        $obj = $this->get_absolute_class_name();
        //
        $search = array(
            "advanced-search-submit" => "",
        );
        $search["etablissement"] = $posted_value;
        $advs_id = str_replace(array('.',','), '', microtime(true));
        $_SESSION["advs_ids"][$advs_id] = serialize($search);
        //
        $query = sprintf(
            'SELECT etablissement.etablissement
            FROM %1$setablissement
            WHERE lower(etablissement.code)=\'%2$s\'',
            DB_PREFIXE,
            $this->f->db->escapeSimple(strtolower($posted_value))
        );
        $res = $this->f->db->getone($query);
        $this->f->addToLog(__METHOD__."(): db->getone(\"".$query."\");", VERBOSE_MODE);
        $this->f->isDatabaseError($res);
        if ($res !== null) {
            header("Location:".OM_ROUTE_FORM."&obj=".$obj."&action=3&idx=".$res."&advs_id=".$advs_id);
            return;
        }
        //
        header("Location: ".OM_ROUTE_TAB."&obj=".$obj."&advs_id=".$advs_id);
        return;
    }

    /**
     * VIEW - view_om_sig_map.
     *
     * @return void
     */
    function view_om_sig_map() {
        $this->checkAccessibility();
        $this->f->layout->display__form_controls_container__begin(array(
            "controls" => "top",
        ));
        $this->retour();
        $this->f->layout->display__form_controls_container__end();
        //
        $form = $this->f->get_inst__om_formulaire();
        $form->entete();
        $this->form_specific_content_before_portlet_actions(11);
        printf(
            '<iframe style="width:100%%;border: 0 none; height: 780px;" src="%s"></iframe>',
            OM_ROUTE_MAP."&mode=tab_sig&obj=".$this->get_default_om_sig_map()."&idx=".$this->getVal($this->clePrimaire)."&popup=1"
        );
        $form->enpied();
        //
        $this->f->layout->display__form_controls_container__begin(array(
            "controls" => "bottom",
        ));
        $this->retour();
        $this->f->layout->display__form_controls_container__end();
    }

    /**
     * @return void
     */
    function form_specific_content_before_portlet_actions($maj) {
        if ($this->f->is_option_sig_interne_enabled() === true) {
            if ($maj == 11 || $maj == 3) {
                printf(
                    '<div id="switch-geolocaliser-consulter">
                    <div class="switcher__label%1$s"><a class="om-prev-icon om-icon-16 consult-16 right" href="%3$s">%4$s</a></div>
                    <div class="switcher__label%2$s"><a class="om-prev-icon om-icon-16 localiser-sig-interne-16 right" href="%5$s">%6$s</a></div>
                    <div class="switcher__toggle%7$s"></div>
                    </div>',
                    ($maj == 3 ? " selected" : ""),
                    ($maj == 11 ? " selected" : ""),
                    $this->compose_form_url("form", array("maj" => 3, "validation" => 0, )),
                    __("consulter"),
                    $this->compose_form_url("form", array("maj" => 11, "validation" => 0, )),
                    __("géolocaliser"),
                    ($maj == 11 ? " geolocaliser" : "")
                );
            }
        }
    }

    /**
     * @return string
     */
    function get_default_om_sig_map() {
        return "etablissement";
    }

    /**
     * VIEW - view_summary.
     *
     * @return boolean
     */
    function view_summary() {
        $this->checkAccessibility();
        echo $this->get_display_synthesis();
    }

    /**
     * Indique si la redirection vers le lien de retour est activée ou non.
     *
     * L'objectif de cette méthode est de permettre d'activer ou de désactiver
     * la redirection dans certains contextes.
     *
     * @return boolean
     */
    function is_back_link_redirect_activated() {
        // Si l'action 0 est ouvert dans un overlay
        if ($this->getParameter('maj') == 0
            && $this->getParameter('retour') == 'overlay') {
            return false;
        }
        return true;
    }

    /**
     * VIEW - view_ua_tab.
     *
     * @return void
     */
    function view_ua_tab() {
        // Vérification de l'accessibilité sur l'élément
        $this->checkAccessibility();
        //
        $this->f->displaySubTitle("-> ".__("unités d'accessibilité"));
        //
        printf('
<div id="ua-tabs">
<ul>
<li>
    <a id="onglet-etablissement_unite__contexte_etab__ua_valide" href="'.OM_ROUTE_SOUSTAB.'&obj=etablissement_unite__contexte_etab__ua_valide&retourformulaire=etablissement&idxformulaire=%1$s">Validées</a>
</li>
<li>
    <a id="onglet-etablissement_unite__contexte_etab__ua_enprojet" href="'.OM_ROUTE_SOUSTAB.'&obj=etablissement_unite__contexte_etab__ua_enprojet&retourformulaire=etablissement&idxformulaire=%1$s">En projet</a>
</li>
<li>
    <a id="onglet-etablissement_unite__contexte_etab__ua_archive" href="'.OM_ROUTE_SOUSTAB.'&obj=etablissement_unite__contexte_etab__ua_archive&retourformulaire=etablissement&idxformulaire=%1$s">Archivées</a>
</li>
</ul>
</div>
',
            $this->getVal("etablissement")
        );
    }

    /**
     * VIEW - view_lien_referentiel_patrimoine.
     *
     * @return void
     */
    function view_lien_referentiel_patrimoine() {
        // Données
        $references_cadastrale = (isset($_GET['ref_cad']) ? $_GET['ref_cad'] : "" );

        //Si l'identifiant a bien été fourni
        if( isset($references_cadastrale) && $references_cadastrale != "" ) {
            
            //Données à envoyer
            $parcelles = $this->f->parseParcelles($references_cadastrale);
            //
            $parcellesWS = "";
            //Formatage des références cadastrale pour l'envoi
            foreach ($parcelles as $parcelle) {

                $parcellesWS .= "parcelle=";
                //On ajoute les données dans le tableau que si quartier + section + parcelle
                //ont été fournis
                if ($parcelle["quartier"]!==""&&$parcelle["section"]!==""&&$parcelle["parcelle"]!=="") {

                    //On récupère le code impôts de l'arrondissement lié au quartier
                    $sql = "SELECT arrondissement.code_impots 
                            FROM ".DB_PREFIXE."arrondissement
                            LEFT JOIN ".DB_PREFIXE."quartier
                            ON quartier.arrondissement = arrondissement.arrondissement
                            WHERE quartier.code_impots = '".$this->f->db->escapeSimple($parcelle['quartier'])."'";
                    $this->f->addToLog("lien_referentiel_patrimoine.php : ".$sql." execute <br>", EXTRA_VERBOSE_MODE);

                    $res = $this->f->db->getOne($sql);
                    $error = $this->f->isDatabaseError($res, 'true');
                    if ( $error === true ) {
                        $this->f->displayMessage("error", __("Erreur de base de donnees. Contactez votre administrateur."));
                    }
                    //
                    $parcellesWS .= $res.$parcelle["quartier"]."%20".$parcelle["section"].$parcelle["parcelle"]."&";
                }
            }

            if (file_exists("../dyn/services.inc.php")) {
                include "../dyn/services.inc.php";
            }
            $link = "";
            if (isset($URL_REFERENTIEL_PATRIMOINE_EQUIPEMENT)
                && $URL_REFERENTIEL_PATRIMOINE_EQUIPEMENT !== "") {
                $link = $URL_REFERENTIEL_PATRIMOINE_EQUIPEMENT."?".$parcellesWS;
            }

            require_once "../services/outgoing/MessageSenderRest.class.php";
            $messageSenderRest = new MessageSenderRest($link);
            
            //Exécution de la requête
            $refs_patrimoine = $messageSenderRest->execute('GET', 'text/html','');
            
            //Affichage du résultat
            $this->f->setTitle(__("Établissement")." -> ".__("Références patrimoine"));
            $this->f->displayTitle();
            //
            $description = __("Cette page vous permet de lier des références "
                    . "patrimoine à un établissement");
            //
            $this->f->displayDescription($description);
            // Ouverture du formulaire
            printf("<form name=f2>");
            //Bouton retour
            $retour = "<a "
                    . "onclick=\"$('#dialog').dialog('close');\" "
                    . "href=\"#\" "
                    . "class=\"retour\">"
                    . __('Retour')
                    ."</a>";
            //Si la récupération s'est bien passée
            if ($messageSenderRest->getResponseCode() == 200){
                //S'il n'y a pas de référence patrimoine à afficher
                if (count($refs_patrimoine)==0){
                    $this->f->displayMessage("error", __("Aucune référence patrimoine liée."));
                }
                else {
                    printf("<div class=\"formControls\">");
                    // Bouton retour
                    printf($retour);
                    printf("</div>");
                    //
                    printf("<div class=\"formEntete ui-corner-all\">");
                    //Affichage des références patrimoine
                    printf("<div>".__("Liste des références patrimoine trouvées :")."</div>");
                    //
                    $template_patrimoine_liste = '
<table class="table">
    %s
</table>';
                    //
                    $template_patrimoine_elem = '
<tr>
    <td>
        <input type="checkbox" name="ref_patrimoine[]" value="%1$s" />
    </td>
    <td>
       <i title="%6$s">%1$s</i> 
       / <span title="%7$s">%2$s</span> / <span title="%8$s"><b>%3$s</b></span>
       <br/>
       <span title="%9$s">%4$s</span> / <i title="%10$s">%5$s</i>
    </td>
</tr>
                    ';
                    //
                    $patrimoine_liste_content = "";
                    //
                    foreach ($refs_patrimoine as $ref_patrimoine) {
                        //
                        $patrimoine_liste_content .= sprintf(
                            $template_patrimoine_elem,
                            $ref_patrimoine['idEquipement'],
                            $ref_patrimoine['nature'],
                            $ref_patrimoine['nom'],
                            $ref_patrimoine['adresse'],
                            $ref_patrimoine['affectataire'],
                            __("numéro d'équipement"),
                            __("catégorie d'équipement patrimoine"),
                            __("nom de l'équipement"),
                            __("adresse de l'équipement"),
                            __("affectataire")
                        );
                    }
                    //
                    printf(
                        $template_patrimoine_liste,
                        $patrimoine_liste_content
                    );
                    //
                    printf("</div>");
                    // Bouton "Valider"
                    printf("<div class=\"formControls\">");
                    printf("<input "
                            . "id=\"button-lien_referentiel_patrimoine-valider\" "
                            . "type=\"button\" "
                            . "class=\"om-button ui-button ui-widget ui-state-default ui-corner-all\" "
                            . 'onclick="getRefPatrimoine();" '
                            . "value=\"Valider\" "
                            . "role=\"button\" "
                            . "aria-disabled=\"false\">");
                    // Bouton retour
                    printf($retour);
                    printf("</div>");
                    return;
                }
            }
            else {
                $this->addToLog(
                    __METHOD__."() : Erreur de communication avec le référentiel patrimoine (coderet:".$messageSenderRest->getResponseCode().")",
                    DEBUG_MODE
                );
                $this->f->displayMessage("error", __("Une erreur s'est produite lors de la "
                        . "récupération des références patrimoine. Contactez votre "
                        . "administrateur"));
            }
            printf("</div>");
            printf("<div class=\"formControls\">");
            // Bouton retour
            printf($retour);
            printf("</div>");
            // Fermeture du formulaire
            printf("</form>");
        }
        else {
            printf (__("Aucune référence cadastrale fournie"));
        }
    }

    /**
     * VIEW - masquer_si_periodicite_visites.
     * 
     * Retourne en JSON si on doit masquer les champs de périodicité
     * suivant la nature et le type postés.
     *
     * @return void
     */
    function masquer_si_periodicite_visites() {
        //
        $this->f->disableLog();
        //
        $postvar = $this->getParameter("postvar");
        $nature = $postvar['id_nature'];
        $type = $postvar['id_type'];
        // Traitement du type de l'établissement
        $erpr = false;
        if ($nature != 'none') {
            $code_etablissement_nature = $this->get_code_etablissement_nature($nature);
            if ($code_etablissement_nature == strtolower($this->f->getParameter("etablissement_nature_erpr"))) {
                $erpr = true;
            }
        }
        // Traitement de la nature de l'établissement
        $plein_air = false;
        if ($type != 'none') {
            $code_etablissement_type = $this->get_code_etablissement_type($type);
            if ($code_etablissement_type == 'pa') {
                $plein_air = true;
            }
        }
        // si nature ERP différente de référentiel ou si de type plein air
        // alors on masque les champs de périodicité des visites
        if (($erpr == false) || ($plein_air == true)) {

            echo json_encode(array(
                "resultat"=>"masquer"
                )
            );
        } else {

            echo json_encode(array(
                "resultat"=>"afficher"
                )
            );
        }
    }


    /**
     * Retourne le code de la nature de l'établissement passée en paramètre.
     * 
     * @param string $id Identifiant de la nature de l'etablissement.
     * 
     * @return string Code de la nature de l'établissement
     */
    function get_code_etablissement_nature($id) {

        $sql = "SELECT LOWER(code)
                FROM ".DB_PREFIXE."etablissement_nature
                WHERE etablissement_nature = ".$id;
        $this->f->addToLog("get_code_etablissement_nature() db->getOne(\"".$sql."\");",
            VERBOSE_MODE);
        $code = $this->f->db->getOne($sql);
        $this->f->isDatabaseError($code);
        return $code;
    }

    /**
     * Retourne le libellé du type de l'établissement passé en paramètre.
     * 
     * @param string $id Identifiant du type de l'établissement
     * 
     * @return string Libellé du type de l'établissement
     */
    function get_code_etablissement_type($id) {

        $sql = "SELECT LOWER(libelle)
                FROM ".DB_PREFIXE."etablissement_type
                WHERE etablissement_type = ".$id;
        $libelle = $this->f->db->getOne($sql);
        $this->f->addToLog(__METHOD__."(): db->getone(\"".$sql."\")", VERBOSE_MODE);
        $this->f->isDatabaseError($libelle);
        return $libelle;
    }


    /**
     *
     */
    function get_widget_config__adresse_voie__autocomplete() {
        $params = array(
            // Surcharge visée pour l'ajout
            "obj" => "voie",
            // Table de l'objet
            "table" => "voie",
            // Flag d'ajout : on ne souhaite pas pouvoir ajouter de voie depuis
            // cet autocomplete
            "droit_ajout" => false,
            // Critères de recherche
            "criteres" => array(
                "voie.voie" => __("identifiant"),
                "voie.libelle" => __("libelle"),
                "voie.rivoli" => __("rivoli")
            ),
            // Tables liées
            "jointures" => array(),
            // Colonnes ID et libellé du champ
            // (si plusieurs pour le libellé alors une concaténation est faite)
            "identifiant" => "voie.voie",
            "libelle" => array(
                "voie.libelle",
            ),
        );
        //
        $where_tmp = "";
        if ($this->getParameter("maj") == 1) {
            $adresse_voie = $this->getVal("adresse_voie");
            if (!empty($adresse_voie)) {
                $where_tmp = " OR voie.voie = ".intval($adresse_voie)." ";
            }
        }
        $params['where'] = " 
        ((
            (
                voie.om_validite_debut IS NULL 
                AND (
                    voie.om_validite_fin IS NULL 
                    OR voie.om_validite_fin > CURRENT_DATE
                    )
            ) 
            OR (
                voie.om_validite_debut <= CURRENT_DATE 
                AND (
                    voie.om_validite_fin IS NULL 
                    OR voie.om_validite_fin > CURRENT_DATE
                    )
                )
        )".$where_tmp.")
        ";
        return $params;
    }

    /**
     * TRIGGER - triggerajouterapres.
     *
     * - ...
     *
     * @return boolean
     */
    function triggerajouterapres($id, &$dnu1 = null, $val = array(), $dnu2 = null) {
        // Ajout des types secondaires
        $add_type_secondaire = $this->add_lien_etablissement_e_type_in_trigger();
        if ($add_type_secondaire === false) {
            //
            return $this->end_treatment(__METHOD__, false);
        }

        // Ajout des parcelles dans la table etablissement_parcelle
        $this->ajouter_etablissement_parcelle($this->valF[$this->clePrimaire], 
            $val['references_cadastrales']);

        // Si au moins un champ exploitant est renseigné
        if ($this->exploitant_saisi($val) == true) {
            // On le crée
            $ret = $this->ajouter_exploitant($val);
            if ($ret !== true) {
                $this->addToMessage(__("Erreur lors de l'ajout de l'exploitant."));
                return $this->end_treatment(__METHOD__, false);
            }
        }

        if ($this->is_option_sig_enabled() === true) {
            // Tentative de géolocalisation de l'établissement et affichage d'un message à l'utilisateur
            // Si l'établissement n'est pas géocodé il doit quand même pouvoir être créé.
            $this->geocoder();
        }
        //
        return $this->end_treatment(__METHOD__, true);
    }

    /**
     * TRIGGER - triggermodifierapres.
     *
     * - ...
     *
     * @return boolean
     */
    function triggermodifierapres($id, &$dnu1 = null, $val = array(), $dnu2 = null) {
        // Suppression de tous les liens de la table de liaison etab/type sec
        $delete_type_secondaire = $this->delete_lien_etablissement_e_type_in_trigger();
        if ($delete_type_secondaire === false) {
            //
            return $this->end_treatment(__METHOD__, false);
        }

        // Ajout des types secondaires
        $add_type_secondaire = $this->add_lien_etablissement_e_type_in_trigger();
        if ($add_type_secondaire === false) {
            //
            return $this->end_treatment(__METHOD__, false);
        }

        // On supprime toutes les lignes de la table etablissement_parcelle
        // qui font référence à l'établissement en cours de modification
        $this->supprimer_etablissement_parcelle($val['etablissement']);

        // Ajout des parcelles dans la table etablissement_parcelle
        $this->ajouter_etablissement_parcelle($val['etablissement'], 
            $val['references_cadastrales']);

        // Si le flag d'existence du formulaire de l'exploitant dans le formulaire
        // de l'établissement n'est pas défini ou si il est explicitement défini
        // alors on traite la mise à jour de l'exploitant sinon on le laisse
        // en l'état
        if (array_key_exists("flag_form_exploitant", $val) === false
            || $val["flag_form_exploitant"] === true) {
            // S'il existe déjà un exploitant attaché à l'établissement
            if ($this->recuperer_ID_exploitant($val['etablissement']) != "]") {
                // S'il s'agit d'une modification en contexte formulaire
                if (isset($val["exp_nom"])) {
                    // Si au moins un champ exploitant est renseigné
                    if ($this->exploitant_saisi($val) == true) {
                        // On le modifie
                        $ret = $this->modifier_exploitant($val);
                        if ($ret !== true) {
                            $this->addToMessage(__("Erreur lors de la mise à jour de l'exploitant."));
                            return $this->end_treatment(__METHOD__, false);
                        }
                    } else {
                        // Sinon on le supprime
                        $ret = $this->supprimer_exploitant();
                        if ($ret !== true) {
                            $this->addToMessage(__("Erreur lors de la suppression de l'exploitant."));
                            return $this->end_treatment(__METHOD__, false);
                        }
                    }
                }
                // Sinon on ne fait rien (maj données techniques)
            } else {
                // S'il s'agit d'une modification en contexte formulaire
                if (isset($val["exp_nom"])) {
                    // Si au moins un champ exploitant est renseigné
                    if ($this->exploitant_saisi($val) == true) {
                        // On le crée
                        $ret = $this->ajouter_exploitant($val);
                        if ($ret !== true) {
                            $this->addToMessage(__("Erreur lors de l'ajout de l'exploitant."));
                            return $this->end_treatment(__METHOD__, false);
                        }
                    }
                }
            }
        }

        //
        return $this->end_treatment(__METHOD__, true);
    }

    /**
     * TRIGGER - triggersupprimerapres.
     *
     * - ...
     *
     * @return boolean
     */
    function triggersupprimerapres($id, &$dnu1 = null, $val = array(), $dnu2 = null) {
        // Suppression de tous les liens de la table de liaison etab/type sec
        $delete_type_secondaire = $this->delete_lien_etablissement_e_type_in_trigger();
        if ($delete_type_secondaire === false) {
            //
            return $this->end_treatment(__METHOD__, false);
        }

        // On supprime toutes les lignes de la table etablissement_parcelle
        // qui font référence à l'établissement en cours de suppression
        $this->supprimer_etablissement_parcelle($val['etablissement']);
        //
        return $this->end_treatment(__METHOD__, true);
    }


    /**
     * Ajoute les parcelles de l'établissement passé en paramètre.
     * 
     * @param string $etablissement                Identifiant de l'établissement
     * @param string $references_cadastrales Références cadastrales
     */
    function ajouter_etablissement_parcelle($etablissement, $references_cadastrales) {

        // Parse les parcelles
        $list_parcelles = $this->f->parseParcelles($references_cadastrales);

        // A chaque parcelle une nouvelle ligne est créée dans la table
        // etablissement_parcelle
        foreach ($list_parcelles as $parcelle) {

            // Instance de la classe etablissement_parcelle
            $etablissement_parcelle = $this->f->get_inst__om_dbform(array(
                "obj" => "etablissement_parcelle",
                "idx" => "]",
            ));

            // Valeurs à sauvegarder
            $value = array(
                'etablissement_parcelle' => '',
                'etablissement' => $etablissement,
                'ref_cadastre' => $parcelle['quartier']
                                .$parcelle['section']
                                .$parcelle['parcelle']
            );

            // Ajout de la ligne
            $etablissement_parcelle->ajouter($value, $this->f->db, DEBUG);
        }
    }

    /**
     * Supprime les parcelles de l'établissement passé en paramètre.
     * 
     * @param string $etablissement Identifiant de l'etablissement
     */
    function supprimer_etablissement_parcelle($etablissement) {

        // Suppression des parcelles de l'établissement
        $sql = "DELETE FROM ".DB_PREFIXE."etablissement_parcelle
                WHERE etablissement='".$etablissement."'";
        $res = $this->f->db->query($sql);
        $this->addToLog("supprimer_etablissement_parcelle() db->query(\"".$sql."\");",
            VERBOSE_MODE);
        database::isError($res);
    }

    /**
     * Retourne vrai si au moins un champ exploitant a été renseigné.
     */
    function exploitant_saisi($champs) {

        if ($champs["exp_civilite"] != '' ||
            $champs["exp_nom"] != '' ||
            $champs["exp_prenom"] != '' ||
            $champs["exp_adresse_numero"] != '' ||
            $champs["exp_adresse_numero2"] != '' ||
            $champs["exp_adresse_voie"] != '' ||
            $champs["exp_adresse_complement"] != '' ||
            $champs["exp_adresse_cp"] != '' ||
            $champs["exp_adresse_ville"] != '' ||
            $champs["exp_lieu_dit"] != '' ||
            $champs["exp_boite_postale"] != '' ||
            $champs["exp_cedex"] != '') {

            return true;
        } else {

            return false;
        }
    }

    /**
     * Crée un exploitant
     */
    function ajouter_exploitant($champs) {
        // Récupération des champs
        $valF = $this->recuperation_champs_exploitant($champs);
        // Définition de la clé primaire
        $valF['contact'] = "";
        // Définition de la qualité particulier
        $valF['qualite'] = "particulier";
        // Ajout dans la table contact
        $ret = $this->exploitant->ajouter($valF);
        if ($ret !== true) {
            $this->addToMessage($this->exploitant->msg);
        }
        return $ret;
    }

    /**
     * Modifie un exploitant existant
     */
    function modifier_exploitant($champs) {
        // Récupération des champs
        $valF = $this->recuperation_champs_exploitant($champs);
        // Définition de la clé primaire
        $valF['contact'] = $this->recuperer_ID_exploitant($this->valF["etablissement"]);
        // Redéfinition de la qualité particulier
        $valF['qualite'] = "particulier";
        // Modification dans la table contact
        $ret = $this->exploitant->modifier($valF);
        if ($ret !== true) {
            $this->addToMessage($this->exploitant->msg);
        }
        return $ret;
    }

    /**
     * Supprime l'exploitant de l'établissement passé en paramètre.
     */
    function supprimer_exploitant() {
        // Définition de la clé primaire
        $valF['contact'] = $this->recuperer_ID_exploitant($this->valF["etablissement"]);
        $ret = $this->exploitant->supprimer($valF);
        if ($ret !== true) {
            $this->addToMessage($this->exploitant->msg);
        }
        return $ret;
    }

    /**
     * Crée un exploitant
     */
    function recuperation_champs_exploitant($champs) {

        // Rattachement à l'établissement
        $valF['etablissement'] = $this->valF["etablissement"];

        // Définition du type de contact en exploitant
        $valF['contact_type'] = $this->recuperer_ID_type_exploitant();

        // Récupération de la saisie utilisateur
        $valF['civilite'] = $champs["exp_civilite"];
        $valF['nom'] = $champs["exp_nom"];
        $valF['prenom'] = $champs["exp_prenom"];
        $valF['adresse_numero'] = $champs["exp_adresse_numero"];
        $valF['adresse_numero2'] = $champs["exp_adresse_numero2"];
        $valF['adresse_voie'] = $champs["exp_adresse_voie"];
        $valF['adresse_complement'] = $champs["exp_adresse_complement"];
        $valF['adresse_cp'] = $champs["exp_adresse_cp"];
        $valF['adresse_ville'] = $champs["exp_adresse_ville"];
        $valF['lieu_dit'] = $champs["exp_lieu_dit"];
        $valF['boite_postale'] = $champs["exp_boite_postale"];
        $valF['cedex'] = $champs["exp_cedex"];
        $valF['pays'] = $champs["exp_pays"];
        //XXX Nouveaux champs
        $valF['qualite'] = '';
        $valF['denomination'] = '';
        $valF['raison_sociale'] = '';
        $valF['siret'] = '';
        $valF['categorie_juridique'] = '';
        $valF['reception_convocation'] = '';
        $valF['reception_programmation'] = '';
        $valF['reception_commission'] = '';
        $valF['service'] = '';
        
        // Champs non utilisés dans la fiche établissement
        $valF['titre'] = "";
        $valF['telephone'] = "";
        $valF['mobile'] = "";
        $valF['fax'] = "";
        $valF['courriel'] = "";
        $valF['om_validite_debut'] = "";
        $valF['om_validite_fin'] = "";

        // Retourne les champs exploitant
        return $valF;
    }

    // Récupération du code de la nature de l'établissement
    function get_etablissement_nature_code($id) {

        // Initialisation du résultat retourné
        $code = '';

        // Si contact_type n'est pas vide
        if ($id != '') {

            // Requête SQL
            $sql = "
                SELECT LOWER(code)
                FROM ".DB_PREFIXE."etablissement_nature
                WHERE etablissement_nature = ".$id;
            $code = $this->f->db->getOne($sql);
            $this->f->isDatabaseError($code);

        }

        // Résultat retourné
        return $code;
    }

    // Récupération du libellé du type de l'établissement
    function get_etablissement_type_libelle($id) {

        // Initialisation du résultat retourné
        $libelle = '';

        // Si contact_type n'est pas vide
        if ($id != '') {

            // Requête SQL
            $sql = "
                SELECT LOWER(libelle)
                FROM ".DB_PREFIXE."etablissement_type
                WHERE etablissement_type = ".$id;
            $libelle = $this->f->db->getOne($sql);
            $this->f->isDatabaseError($libelle);

        }

        // Résultat retourné
        return $libelle;
    }

    // Récupération du libellé du type de l'établissement
    function get_etablissement_statut_juridique_code($id) {

        // Initialisation du résultat retourné
        $code = '';

        // Si etablissement_statut_juridique n'est pas vide
        if ($id != '') {

            // Requête SQL
            $sql = "
                SELECT LOWER(code)
                FROM ".DB_PREFIXE."etablissement_statut_juridique
                WHERE etablissement_statut_juridique = ".$id;
            $code = $this->f->db->getOne($sql);
            $this->f->isDatabaseError($code);

        }

        // Résultat retourné
        return $code;
    }
    
    /**
     * Mémorisation de l'ID et du libellé d'un nouvel établissement
     * lors d'un ajout dans l'autocomplete des DC
     */
    function formSpecificContent($maj) {

        // Récupération de la validation
        $validation_parameter = $this->getParameter('validation');
        $validation = (isset($validation_parameter)) ? $validation_parameter : 0;

        // Si on est en mode ajout et que le formulaire a été validé
        if ($maj == 0 && $validation > 0) {
            echo "<span id=\"id_new_etablissement_tous\">";
                echo $this->valF[$this->clePrimaire];
            echo "</span>";
            echo "<span id=\"libelle_new_etablissement_tous\">";
                echo $this->valF["libelle"];
            echo "</span>";
        }
    }

    /**
     * VIEW - view_localiser_selection
     * Redirige l'utilisateur vers le SIG externe, avec la vue centrée sur la sélection
     * d'établissements obtenue par la recherche avancée.
     * Si la recherche renvoie une liste vide, l'utilisateur est redirigé vers la couche
     * des établissements sur le SIG.
     *
     * @return void
     */
    function view_localiser_selection() {
        $this->checkAccessibility();

        (isset($_GET['obj']) ? $obj = $this->f->get_submitted_get_value('obj') : $obj = "");
        // Premier enregistrement a afficher
        (isset($_GET['premier']) ? $premier = $this->f->get_submitted_get_value('premier') : $premier = 0);
        // Colonne choisie pour le tri
        (isset($_GET['tricol']) ? $tricol = $this->f->get_submitted_get_value('tricol') : $tricol = "");
        // Id unique de la recherche avancee
        (isset($_GET['advs_id']) ? $advs_id = $this->f->get_submitted_get_value('advs_id') : $advs_id = "");
        // Valilite des objets a afficher
        (isset($_GET['valide']) ? $valide = $this->f->get_submitted_get_value('valide') : $valide = "");

        // Instance geoaria
        $geoaria = $this->f->get_inst_geoaria();
        if($geoaria === false) {
            // L'erreur geoaria est affichée dans la méthode handle_geoaria_exception
            return false;
        }
        // S'il n'y a pas eu de recherche avancée
        if ($advs_id === '') {
            try {
                $url = $geoaria->redirection_web('etablissement', array());
            } catch (geoaria_exception $e) {
                $this->handle_geoaria_exception($e);
                return;
            }
            header("Location: ".$url);
            exit();
        }

        // Ce tableau permet a chaque application de definir des variables
        // supplementaires qui seront passees a l'objet metier dans le constructeur
        // a travers ce tableau
        // Voir le fichier dyn/form.get.specific.inc.php pour plus d'informations
        $extra_parameters = array();
        $f = $this->f;
        // surcharge globale
        if (file_exists('../dyn/tab.inc.php')) {
            require_once '../dyn/tab.inc.php';
        }
        if (file_exists('../dyn/custom.inc.php')) {
            require_once '../dyn/custom.inc.php';
        }
        // *** custom
        if(isset($custom['tab'][$obj]) and file_exists($custom['tab'][$obj])){
            require_once $custom['tab'][$obj];
        } else{
            // surcharge specifique des objets
            if (file_exists("../sql/".OM_DB_PHPTYPE."/".$obj.".inc.php")) {
                require "../sql/".OM_DB_PHPTYPE."/".$obj.".inc.php";
            } else {
                require "../sql/".OM_DB_PHPTYPE."/".$obj.".inc";
            }   
        }
        if (!isset($om_validite) or $om_validite != true) {
            $om_validite = false;
        }
    
        // Redéfinition de l'ordre de tri pour avoir les codes dans un ordre alphabétique
        $tri = "ORDER BY etablissement.code ASC";

        /**
         *
         */
        // Instanciation d'om_table
        $tb = $this->f->get_inst__om_table(array(
            "aff" => OM_ROUTE_TAB,
            "table" => $table,
            "serie" => $serie,
            "champAffiche" => array(
                "etablissement.etablissement as id",
                "etablissement.code as code",
            ),
            "champRecherche" => $champRecherche,
            "tri" => $tri,
            "selection" => $selection,
            "edition" => $edition,
            "options" => $options,
            "advs_id" => $advs_id,
            "om_validite" => $om_validite,
        ));

        /**
         *
         */
        // Affectation des parametres
        $params = array(
            "obj" => $obj,
            "premier" => $premier,
            "tricol" => $tricol,
            "valide" => $valide,
            "advs_id" => $advs_id,
        );
        // Ajout de paramètre spécifique
        $params = array_merge($params, $extra_parameters);
        //
        $tb->setParams($params);
        // Methode permettant de definir si la recherche doit etre faite
        // sur la recherche simple ou avancée
        $tb->composeSearchTab();
        // Generation de la requete de recherche
        $tb->composeQuery();
        // Exécution de la requête
        $res = $this->db->limitquery($tb->sql, $premier, '50');
        $this->addToLog(__METHOD__."() db->limitquery(\"".$tb->sql."\");", VERBOSE_MODE);
        $this->f->isDatabaseError($res);

        $nbligne = $res->numrows();
        // S'il n'y a aucun résultat
        if ($nbligne === 0) {
            try {
                $url = $geoaria->redirection_web('etablissement', array());
            } catch (geoaria_exception $e) {
                $this->handle_geoaria_exception($e);
                return;
            }
            header("Location: ".$url);
            exit();
        }
        // Récupération des identifiants et libellés
        while ($row=& $res->fetchRow()){
            $etablissements_codes[] = $row[1];
        }
        $data = array();
        foreach ($etablissements_codes as $code) {
            $data[] = $code;
        }
        try {
            $url = $geoaria->redirection_web('etablissement', $data);
        } catch (geoaria_exception $e) {
            $this->handle_geoaria_exception($e);
            return;
        }

        //Redirection
        header("Location: ".$url);

    }

    /**
     * Permet d'ajouter les types secondaires de l'établissement dans les
     * méthodes TRIGGER de la classe.
     *
     * @return boolean
     */
    function add_lien_etablissement_e_type_in_trigger() {
        // Récupération des données du select multiple
        $types_secondaires = $this->getPostedValues('etablissement_type_secondaire');
        $type_principal = $this->valF['etablissement_type'];

        //
        $inst_lien_etablissement_e_type = $this->get_inst_lien_etablissement_e_type(']');

        // Ne traite les données que s'il y en a et qu'elles sont correctes
        if (is_array($types_secondaires) === true && count($types_secondaires) > 0) {
            // Boucle sur la liste des types sélectionnés
            foreach ($types_secondaires as $value) {
                // Pas d'ajout si valeur nulle ou type principal sélectionné
                if ($value != "" && $value != null && $value != $type_principal) {
                    // 
                    $data = array(
                        'lien_etablissement_e_type' => "",
                        'etablissement' => $this->valF[$this->clePrimaire],
                        'etablissement_type' => $value
                    );
                    // Stop le traitement si un ajout échoue
                    $add_type_secondaire = $inst_lien_etablissement_e_type->ajouter($data);
                    if ($add_type_secondaire === false) {
                        //
                        return false;
                    }
                }
            }
        }

        //
        return true;
    }

    /**
     * Permet de supprimer les types secondaires de l'établissement dans les
     * méthodes TRIGGER de la classe.
     *
     * @return boolean
     */
    function delete_lien_etablissement_e_type_in_trigger() {
        //
        $inst_lien_etablissement_e_type = $this->get_inst_lien_etablissement_e_type(0);
        $delete_type_secondaire = $inst_lien_etablissement_e_type->delete_by_etablissement(
            $this->getVal($this->clePrimaire)
        );
        if ($delete_type_secondaire === false) {
            //
            return false;
        }

        //
        return true;
    }

    /**
     * Fonction générique permettant de récupérer les données d'un champ postées.
     *
     * @param string $champ Nom du champ
     *
     * @return mixed Valeur posté
     */
    function getPostedValues($champ) {
        // Récupération des demandeurs dans POST
        if (isset($_POST[$champ]) ) {
            //
            return $_POST[$champ];
        }
    }

    /**
     * Charge le select du champ "adresse_arrondissement" par rapport au
     * champ "adresse_voie".
     * 
     * @param  object $form  Formulaire
     * @param  int    $maj   Mode d'insertion
     * @param  string $champ Champ activant le filtre
     * @return array         Contenu du select
     */
    function load_select_adresse_arrondissement(&$form, $maj, $champ) {
        // Contenu du select
        $contenu = array();
        $contenu[0][0] = '';
        $contenu[1][0] = __('choisir')." ".__("adresse_arrondissement");

        // Récupère l'id de la voie
        $adresse_voie = "";
        if (isset($_POST[$champ])) {
            $adresse_voie = $_POST[$champ];
        } elseif($this->getParameter($champ) != "") {
            $adresse_voie = $this->getParameter($champ);
        } elseif(isset($form->val[$champ])) {
            $adresse_voie = $form->val[$champ];
        }

        // S'il y a une voie
        if (!empty($adresse_voie)) {
            // Requête SQL
            $sql_adresse_arrondissement_by_adresse_voie = str_replace('<idx_voie>', $adresse_voie, $this->get_var_sql_forminc__sql("adresse_arrondissement_by_adresse_voie"));
            $res = $this->f->db->query($sql_adresse_arrondissement_by_adresse_voie);
            // Log
            $this->addToLog(__METHOD__."() db->query(\"".$sql_adresse_arrondissement_by_adresse_voie."\");", VERBOSE_MODE);
            $this->f->isDatabaseError($res);
            //Les résultats de la requête sont stocké dans le tableau contenu
            $k=1;
            while ($row=& $res->fetchRow()){
                $contenu[0][$k]=$row[0];
                $contenu[1][$k]=$row[1];
                $k++;
            } 
        }

        // Récupère l'éventuel identifiant d'arrondissement existant sur l'établissement
        // Si l'associaion voie/arrondissement n'existe plus, on ne souhaite pas perdre
        // la valeur actuelle de l'arrondissement pour cet établissement.
        // Si un arrondissement est déjà sélectionné sur l'établissement et qu'il n'est
        // pas déjà dans le select construit juste au dessus alors on le rajoute.
        if (intval($this->getVal("adresse_arrondissement")) > 0
            && in_array($this->getVal("adresse_arrondissement"), $contenu[0]) === false) {
            //
            $this->getSelectOldValue(
                $form,
                $maj,
                $this->f->db,
                $contenu,
                $this->get_var_sql_forminc__sql_adresse_arrondissement_by_id(),
                "adresse_arrondissement",
                $this->getVal("adresse_arrondissement")
            );
        }
        //
        return $contenu;
    }


    /**
     *
     */
    function is_archivable() {
        //
        $om_validite_fin = $this->getVal("om_validite_fin");
        // Vérification de l'état d'archivage
        // S'il y a une date de validité de fin et qu'elle est antérieure ou égale au jour en cours c'est que l'état est archivé
        if ($om_validite_fin != NULL && $om_validite_fin <= date('Y-m-d')) {
            return false;
        } else {
            return true;
        }
    }

    /**
     *
     */
    function is_desarchivable() {
        //
        $om_validite_fin = $this->getVal("om_validite_fin");
        // Vérification de l'état d'archivage
        // S'il y a une date de validité de fin et qu'elle est antérieure ou égale au jour en cours c'est que l'état est archivé
        if ($om_validite_fin != NULL && $om_validite_fin <= date('Y-m-d')) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * TREATMENT - archiver.
     * 
     * Permet de ...
     *
     * @param array $val Valeurs soumises par le formulaire
     *
     * @return boolean
     */
    function archiver($val = array()) {
        // Begin
        $this->begin_treatment(__METHOD__);

        //
        $ret = $this->manage_archiving("archive", $val);
        // Si le traitement ne s'est pas déroulé correctement
        if ($ret !== true) {
            // Return
            return $this->end_treatment(__METHOD__, false);
        }

        // Return
        return $this->end_treatment(__METHOD__, true);
    }

    /**
     * TREATMENT - desarchiver.
     * 
     * Permet de ...
     *
     * @param array $val Valeurs soumises par le formulaire
     *
     * @return boolean
     */
    function desarchiver($val = array()) {
        // Begin
        $this->begin_treatment(__METHOD__);

        //
        $ret = $this->manage_archiving("unarchive", $val);
        // Si le traitement ne s'est pas déroulé correctement
        if ($ret !== true) {
            // Return
            return $this->end_treatment(__METHOD__, false);
        }

        // Return
        return $this->end_treatment(__METHOD__, true);
    }

    /**
     *
     */
    function manage_archiving($mode = null, $val = array()) {
        // Logger
        $this->addToLog(__METHOD__."() - begin", EXTRA_VERBOSE_MODE);
        // Si le mode n'existe pas on retourne false
        if ($mode != "archive" && $mode != "unarchive") {
            return false;
        }
        // Récuperation de la valeur de la cle primaire de l'objet
        $id = $this->getVal($this->clePrimaire);
        // 
        if ($mode == "archive") {
            // Date pour la bdd et pour l'affichage
            $om_validite_fin_bdd = date("Y-m-d");
            $om_validite_fin_aff = date("d/m/Y");
            // Valeurs à modifier
            $valF = array(
                "om_validite_fin" => $om_validite_fin_bdd,
            );
            //
            $valid_message = __("Archivage correctement effectue.");
        } elseif ($mode == "unarchive") {
            // Valeurs à modifier
            $valF = array(
                "om_validite_fin" => null,
            );
            //
            $valid_message = __("Desarchivage correctement effectue.");
        }
        //
        $this->correct = true;
        // Execution de la requête de modification des donnees de l'attribut
        // valF de l'objet dans l'attribut table de l'objet
        $res = $this->f->db->autoExecute(
            DB_PREFIXE.$this->table,
            $valF,
            DB_AUTOQUERY_UPDATE,
            $this->getCle($id)
        );
        // Si une erreur survient
        if (database::isError($res, true)) {
            // Appel de la methode de recuperation des erreurs
            $this->erreur_db($res->getDebugInfo(), $res->getMessage(), '');
            $this->correct = false;
            return false;
        } else {
            //
            $main_res_affected_rows = $this->f->db->affectedRows();
            // Log
            $this->addToLog(__("Requete executee"), VERBOSE_MODE);
            // Log
            $message = __("Enregistrement")."&nbsp;".$id."&nbsp;";
            $message .= __("de la table")."&nbsp;\"".$this->table."\"&nbsp;";
            $message .= "[&nbsp;".$main_res_affected_rows."&nbsp;";
            $message .= __("enregistrement(s) mis a jour")."&nbsp;]";
            $this->addToLog($message, VERBOSE_MODE);
            // Message de validation
            if ($main_res_affected_rows == 0) {
                $this->addToMessage(__("Attention vous n'avez fait aucune modification.")."<br/>");
            } else {
                $this->addToMessage($valid_message."<br/>");
            }
        }
        // Logger
        $this->addToLog(__METHOD__."() - end", EXTRA_VERBOSE_MODE);
        //
        return true;
    }

    /**
     * Récupère le code de la nature de l'établissement par rapport à
     * l'établissement.
     *
     * @param integer $id Identifiant de l'établissement
     *
     * @return string Code de la nature de l'établissement
     */
    function get_etablissement_nature_code_by_etablissement($id) {

        // Initialisation du résultat retourné
        $code = '';

        // Si contact_type n'est pas vide
        if (!empty($id)) {

            // Requête SQL
            $sql = "
                SELECT LOWER(etablissement_nature.code)
                FROM ".DB_PREFIXE."etablissement_nature
                    LEFT JOIN ".DB_PREFIXE."etablissement
                        ON etablissement.etablissement_nature = etablissement_nature.etablissement_nature
                WHERE etablissement.etablissement = ".$id;
            $code = $this->f->db->getOne($sql);
            $this->f->isDatabaseError($code);

        }

        // Résultat retourné
        return $code;
    }

    /**
     * Récupère les données de l'établissement.
     *
     * @param integer $id Identifiant de l'établissement
     *
     * @return array Liste de données
     */
    function get_data($id) {
        // Initialisation de la vatiable de résultat
        $values = array();
        // Si l'identifiant est renseigné
        if (!empty($id)) {
            // Récupère les données nécessaires au type spécifique etablissement
            $values = array(
                "code" => $this->getVal("code"),
                "libelle" => $this->getVal("libelle"),
                "adresse_numero" => $this->getVal("adresse_numero"),
                "adresse_numero2" => $this->getVal("adresse_numero2"),
                "adresse_voie" => $this->getVal("adresse_voie"),
                "adresse_complement" => $this->getVal("adresse_complement"),
                "lieu_dit" => $this->getVal("lieu_dit"),
                "boite_postale" => $this->getVal("boite_postale"),
                "adresse_cp" => $this->getVal("adresse_cp"),
                "adresse_ville" => $this->getVal("adresse_ville"),
                "adresse_arrondissement" => $this->getVal("adresse_arrondissement"),
                "cedex" => $this->getVal("cedex"),
                "etablissement_nature_libelle" => $this->getVal("etablissement_nature"),
                "etablissement_type_libelle" => $this->getVal("etablissement_type"),
                "etablissement_categorie_libelle" => $this->getVal("etablissement_categorie"),
                "si_locaux_sommeil" => ($this->getVal("si_locaux_sommeil")=='t')?__("locaux à sommeil"):"",
                "geolocalise" => $this->getVal("geolocalise"),
            );

            // Récupère le libellé de la voie
            if (!empty($values['adresse_voie'])) {
                $sql_adresse_voie_by_id = str_replace('<idx>', $values['adresse_voie'], $this->get_var_sql_forminc__sql("adresse_voie_by_id"));
                $res = $this->f->db->query($sql_adresse_voie_by_id);
                $this->addToLog("setSelect(): db->query(".$sql_adresse_voie_by_id.");", VERBOSE_MODE);
                $this->f->isDatabaseError($res);
                $row =& $res->fetchRow();
                $values['adresse_voie'] = $row[1];
            }

            // Récupère le libellé de l'arrondissement
            if (!empty($values['adresse_arrondissement'])) {
                $sql_adresse_arrondissement_by_id = str_replace('<idx>', $values['adresse_arrondissement'], $this->get_var_sql_forminc__sql("adresse_arrondissement_by_id"));
                $res = $this->f->db->query($sql_adresse_arrondissement_by_id);
                $this->addToLog("setSelect(): db->query(".$sql_adresse_arrondissement_by_id.");", VERBOSE_MODE);
                $this->f->isDatabaseError($res);
                $row =& $res->fetchRow();
                $values['adresse_arrondissement'] = $row[1];
            }
            
            // Récupère la nature de l'établissement
            if (!empty($values['etablissement_nature_libelle'])) {
                $sql_etablissement_nature_by_id = str_replace('<idx>', $values['etablissement_nature_libelle'], $this->get_var_sql_forminc__sql("etablissement_nature_by_id"));
                $res = $this->f->db->query($sql_etablissement_nature_by_id);
                $this->addToLog("setSelect(): db->query(".$sql_etablissement_nature_by_id.");", VERBOSE_MODE);
                $this->f->isDatabaseError($res);
                $row =& $res->fetchRow();
                $values['etablissement_nature_libelle'] = $row[1];
            }
            
            // Récupère le libellé de le type de l'établissement
            if (!empty($values['etablissement_type_libelle'])) {
                $sql_etablissement_type_by_id = str_replace('<idx>', $values['etablissement_type_libelle'], $this->get_var_sql_forminc__sql("etablissement_type_by_id"));
                $res = $this->f->db->query($sql_etablissement_type_by_id);
                $this->addToLog("setSelect(): db->query(".$sql_etablissement_type_by_id.");", VERBOSE_MODE);
                $this->f->isDatabaseError($res);
                $row =& $res->fetchRow();
                $values['etablissement_type_libelle'] = $row[1];
            }
            
            // Récupère le libellé de la catégorie de l'établissement
            if (!empty($values['etablissement_categorie_libelle'])) {
                $sql_etablissement_categorie_by_id = str_replace('<idx>', $values['etablissement_categorie_libelle'], $this->get_var_sql_forminc__sql("etablissement_categorie_by_id"));
                $res = $this->f->db->query($sql_etablissement_categorie_by_id);
                $this->addToLog("setSelect(): db->query(".$sql_etablissement_categorie_by_id.");", VERBOSE_MODE);
                $this->f->isDatabaseError($res);
                $row =& $res->fetchRow();
                $values['etablissement_categorie_libelle'] = $row[1];
            }
        }

        // Retourne le résultat
        return $values;
    }

    /**
     * Retourne l'ID de l'exploitant de l'établissement passé en paramètre,
     * ou "]" si n'existe pas.
     * 
     * @param string $etablissement Identifiant de l'etablissement
     */
    function recuperer_ID_exploitant($etablissement) {

        // Si l'établissement passé en argument est un enregistrement valide
        if (!empty($etablissement) && $etablissement != "]" && is_numeric($etablissement)) {

            $sql = "SELECT contact.contact
            FROM ".DB_PREFIXE."contact
            WHERE contact.contact_type='".$this->recuperer_ID_type_exploitant()."'
            AND contact.etablissement='".$etablissement."'";
            $res = $this->f->db->query($sql);
            $this->addToLog("recuperer_ID_exploitant() db->query(\"".$sql."\");",
                VERBOSE_MODE);
            $this->f->isDatabaseError($res);
            $row = $res->fetchrow(DB_FETCHMODE_ASSOC);
            // Si un exploitant est attaché à l'établissement
            if ($row) {
                return $row['contact'];
            }
        }
        // Si nouvel établissement ou pas d'exploitant attaché
        return "]";
    }

    /**
     * Retourne l'ID du type de contact exploitant.
     */
    function recuperer_ID_type_exploitant() {

        $sql = "SELECT contact_type.contact_type as idtypeexploitant
        FROM ".DB_PREFIXE."contact_type
        WHERE LOWER(contact_type.code)='expl'";
        $res = $this->f->db->query($sql);
        $this->addToLog("recuperer_ID__type_exploitant() db->query(\"".$sql."\");",
            VERBOSE_MODE);
        $this->f->isDatabaseError($res);
        $row = $res->fetchrow(DB_FETCHMODE_ASSOC);
        return $row['idtypeexploitant'];            
    }

    /**
     * Met à jour le champ "autorite_police_encours"
     *
     * @param boolean $value true ou false
     *
     * @return boolean
     */
    function update_autorite_police_encours($value) {
        // Valeur à mettre àjour
        $valF = array(
            "autorite_police_encours" => $value,
        );

        // Met à jour l'enregistrement
        $update = $this->update_autoexecute($valF);
        //
        if ($update == false) {
            return false;
        }

        //
        return true;
    }

    /**
     * Met à jour l'état de l'établissement.
     *
     * @param integer $value Nouvel état
     *
     * @return boolean
     */
    function update_etat($value) {
        // Valeur à mettre àjour
        $valF = array(
            "etablissement_etat" => $value,
        );

        // Met à jour l'enregistrement
        $update = $this->update_autoexecute($valF);

        // Si la mise à jour échoue
        if ($update == false) {
            // Stop le traitement
            return false;
        }

        // Continue le traitement
        return true;
    }

    /**
     * Met à jour la nature de l'établissement.
     *
     * @param integer $value Nouvelle nature
     *
     * @return boolean
     */
    function update_nature($value) {
        // Valeur à mettre àjour
        $valF = array(
            "etablissement_nature" => $value,
        );

        // Met à jour l'enregistrement
        $update = $this->update_autoexecute($valF);

        // Si la mise à jour échoue
        if ($update == false) {
            // Stop le traitement
            return false;
        }

        // Continue le traitement
        return true;
    }

    /**
     * Met à jour la date d'arrêté d'ouverture de l'établissement.
     *
     * @param string $value Valeur
     *
     * @return boolean
     */
    function update_date_arrete_ouverture($value) {
        // Valeur à mettre àjour
        $valF = array(
            "date_arrete_ouverture" => $value,
        );

        // Met à jour l'enregistrement
        $update = $this->update_autoexecute($valF);

        // Si la mise à jour échoue
        if ($update == false) {
            // Stop le traitement
            return false;
        }

        // Continue le traitement
        return true;
    }

    /**
     * SETTER_TREATMENT - setter_treatment__code (setValF).
     *
     * @return void
     */
    function setter_treatment__code($val = array()) {
        if (array_key_exists("code", $val) !== true) {
            return;
        }
        // Le champ "code" peut être calculé automatiquement
        if ($val["code"] === "AUTO") {
            $prefix_length = strlen($this->get_param__code_prefix());
            $sql = sprintf(
                'SELECT
                    MAX(SUBSTR(code, %2$s, %3$s)::integer)+1
                FROM
                    %1$setablissement',
                DB_PREFIXE,
                // Position du premier chiffre du code
                intval($prefix_length) + 1,
                // Longueur maximale de la partie chiffre du code, le champ
                // fait 25 caractères
                25 - (intval($prefix_length))
            );
            $code_num = $this->f->db->getone($sql);
            $this->addToLog(__METHOD__."() db->getone(\"".$sql."\");", VERBOSE_MODE);
            if ($this->f->isDatabaseError($code_num, true) !== false) {
                $this->correct = false;
                $this->addToMessage(
                    __("Erreur lors de la récupération automatique du code de ".
                    "l'établissement. Contactez votre administrateur.")
                );
                return;
            }
            // Si aucun établissement n'existe dans la table la requête
            // retourne une chaîne vide. Si c'est le cas on initialise le
            // numéro du code à 1.
            $code_num = intval($code_num);
            if ($code_num === 0) {
                $code_num = 1;
            }
            $this->valF["code"] = $this->get_param__code_prefix().$code_num;
            return;
        }
        // On supprime les éventuels 0 entre le préfixe et le numéro de
        // l'établissmeent
        $code_num = str_replace($this->get_param__code_prefix(), "", $val["code"]);
        if (is_numeric($code_num) === true) {
            $this->valF["code"] = $this->get_param__code_prefix().intval($code_num);
        } else {
            // On positionne la valeur telle qu'elle est postée, les méthodes
            // CHECK_TREATMENT (verifier) feront leur travail.
            $this->valF["code"] = $val["code"];
        }
    }

    /**
     * SETTER_TREATMENT - setValFAjout (setValF).
     *
     * @return void
     */
    function setValFAjout($val = array()) {
        parent::setValFAjout($val);
        $this->setter_treatment__code($val);
    }


    /**
     * SETTER_TREATMENT - setValF (setValF).
     *
     * @return void
     */
    function setvalF($val = array()) {
        parent::setvalF($val);
        // Gestion des visites périodiques
        // A chaque modification d'établissement, on met à jour les champs :
        // - si_periodicite_visites
        // - si_prochaine_visite_periodique_date_previsionnelle
        // en fonction des critères postés
        $is_periodic_primary_condition_ok = $this->is_periodic_primary_condition_ok(
            $this->valF['etablissement_nature'],
            $this->valF['etablissement_etat'],
            $this->valF['si_autorite_competente_visite']
        );
        if ($is_periodic_primary_condition_ok === true) {
            $this->valF['si_periodicite_visites'] = $this->get_periodicite_visites(
                $this->valF['etablissement_type'],
                $this->valF['etablissement_categorie'],
                $this->valF['si_locaux_sommeil']
            );
            if ($this->valF['si_derniere_visite_periodique_date'] !== null
                && $this->valF['si_periodicite_visites'] !== null) {
                //
                $this->valF['si_prochaine_visite_periodique_date_previsionnelle'] = $this->sum_date_visite_periodicite(
                    $this->valF['si_derniere_visite_periodique_date'],
                    $this->valF['si_periodicite_visites']
                );
            } else {
                $this->valF['si_prochaine_visite_periodique_date_previsionnelle'] = null;
                $this->valF['dossier_coordination_periodique'] = null;
                if ($this->getVal("dossier_coordination_periodique") != "") {
                    $this->addToMessage(__("Cet établissement vient de perdre les critères le classant comme soumis à visite périodique. Le DC périodique attaché est désormais orphelin."));
                }
            }
        } else {
            $this->valF['si_periodicite_visites'] = null;
            $this->valF['si_prochaine_visite_periodique_date_previsionnelle'] = null;
            $this->valF['dossier_coordination_periodique'] = null;
            if ($this->getVal("dossier_coordination_periodique") != "") {
                $this->addToMessage(__("Cet établissement vient de perdre les critères le classant comme soumis à visite périodique. Le DC périodique attaché est désormais orphelin."));
            }
        }

        // Surcharge des booléens transformés en select pour accepter le null
        switch ($val['si_conformite_l16']) {

            case 1:
            case "t":
            case "Oui":
            case "true":
            $this->valF['si_conformite_l16'] = true;
            break;

            case '':
            case "null":
            case "NULL":
            $this->valF['si_conformite_l16'] = null;
            break;

            default:
            $this->valF['si_conformite_l16'] = false;
            break;
        }
        switch ($val['si_alimentation_remplacement']) {

            case 1:
            case "t":
            case "Oui":
            case "true":
            $this->valF['si_alimentation_remplacement'] = true;
            break;

            case '':
            case "null":
            case "NULL":
            $this->valF['si_alimentation_remplacement'] = null;
            break;

            default:
            $this->valF['si_alimentation_remplacement'] = false;
            break;
        }
        switch ($val['si_service_securite']) {

            case 1:
            case "t":
            case "Oui":
            case "true":
            $this->valF['si_service_securite'] = true;
            break;

            case '':
            case "null":
            case "NULL":
            $this->valF['si_service_securite'] = null;
            break;

            default:
            $this->valF['si_service_securite'] = false;
            break;
        }
    }

    /**
     * CHECK_TREATMENT - verifier.
     *
     * @return void
     */
    function verifier($val = array(), &$dnu1 = null, $dnu2 = null) {
        // On ne fait aucune vérification si les méthodes précédentes
        // SETTER_TREATMENT (setValF) ont déjà rencontrées une erreur.
        if ($this->correct !== true) {
            return;
        }
        // Appel du parent pour gérer les champs requis et les contraintes
        // d'unicité
        parent::verifier($val);
        // Vérification de la conformité du format pour le code de
        // l'établissement renseigné
        $this->check_treatment__code_format($val);
        // Vérification de champs obligatoires en fonction de la nature
        // renseignée
        // Si une nature est renseignée
        if ($this->valF["etablissement_nature"] != "") {
            // On récupère l'identifiant de la nature qui correspond à la nature
            // 'ERP Référentiel' dans le paramétrage
            $sql = sprintf(
                'SELECT
                    etablissement_nature.etablissement_nature
                FROM
                    %1$setablissement_nature 
                WHERE
                    LOWER(etablissement_nature.code) = LOWER(\'%2$s\')',
                DB_PREFIXE,
                $this->f->db->escapesimple($this->f->getParameter("etablissement_nature_erpr"))
            );
            $etablissement_nature_erpr_id = $this->f->db->getone($sql);
            $this->addToLog(__METHOD__."(): db->getone(".$sql.");", VERBOSE_MODE);
            if ($this->f->isDatabaseError($etablissement_nature_erpr_id, true) !== false) {
                $this->correct = false;
                $this->addToMessage(
                    __("Erreur de base de données. Contactez votre ".
                    "administrateur.")
                );
                // On arrête les vérifications
                return;
            }
            // Si la nature 'ERP Référentiel' est la nature renseignée
            if ($etablissement_nature_erpr_id == $this->valF["etablissement_nature"]) {
                // Le type et la catégorie sont obligatoires
                if (is_null($this->valF["etablissement_type"])
                    || $this->valF["etablissement_type"] == ""
                    || is_null($this->valF["etablissement_categorie"])
                    || $this->valF["etablissement_categorie"] == "") {
                    //
                    $this->correct = false;
                    $this->addToMessage(
                        __("Le <span class=\"bold\">type</span> et la <span ".
                        "class=\"bold\">catégorie</span> de l'établissement ".
                        "doivent être renseignés si l'établissement est un ".
                        "ERP Référentiel")
                    );
                }
                // La commission compétente visite SI est obligatoire
                if (is_null($this->valF["si_autorite_competente_visite"])
                    || $this->valF["si_autorite_competente_visite"] == "") {
                    //
                    $this->correct = false;
                    $this->addToMessage(
                        __("La <span class=\"bold\">commission compétente ".
                        "visite SI</span> de l'établissement doit être ".
                        "renseignée si l'établissement est un ERP Référentiel")
                    );
                }
            }
        }
        // Vérification du numéro de SIRET
        // Si un numéro de SIRET est renseigné
        if ($this->valF["siret"] != "") {
            // Le numéro de SIRET doit être valide
            if ($this->f->checkLuhn($this->valF["siret"]) !== true) {
                $this->correct = false;
                $this->addToMessage(__("Le numero de SIRET est invalide."));
            }
        }
    }

    /**
     * Retourne le préfixe du code de l'établissement.
     *
     * C'est le paramètre général de l'application qui est retourné ici et non
     * pas le préfixe effectif du code de l'établissement qui peut être
     * différent.
     *
     * @return string
     */
    function get_param__code_prefix() {
        return trim($this->f->getParameter("etablissement_code_prefixe"));
    }

    /**
     * CHECK_TREATMENT - check_treatment__code_format (verifier).
     *
     * @return void
     */
    function check_treatment__code_format($val = array()) {
        if ($this->valF["code"] != ""
            && preg_match('/^'.$this->get_param__code_prefix().'\d+$/', $this->valF["code"]) !== 1 ) {
            //
            $this->correct = false;
            $this->addToMessage(sprintf(
                _("Le champ <span class=\"bold\">code</span> doit ".
            "commencer par <span class=\"bold\">%s</span> suivi de chiffres"),
                $this->get_param__code_prefix()
            ));
        }
    }

    /**
     * VIEW - view_data_etablissement.
     *
     * Retourne en JSON les informations de l'établissement
     * 
     * @return void
     */
    function view_data_etablissement() {
        //
        $this->f->disableLog();
        //
        $result = $this->get_data_etablissement();
        //
        echo json_encode($result);
    }

    /**
     * Récupère les informations de l'établissement.
     *
     * @return array
     */
    function get_data_etablissement() {

        // Initialisation du tableau des résultats
        $return = array();

        // Identifiant de l'enregistrement
        $id = $this->getVal($this->clePrimaire);

        // Si l'identifiant n'est pas vide
        if (!empty($id)) {

            // Requête SQL
            $sql = "SELECT
                        etablissement.etablissement_type as etablissement_type,
                        etablissement_categorie as etablissement_categorie,
                        si_locaux_sommeil as etablissement_locaux_sommeil,
                        CASE WHEN etablissement_nature.erp IS TRUE 
                            THEN 't'
                            ELSE 'f'
                        END as etablissement_nature,
                        array_to_string(
                            array_agg(lien_etablissement_e_type.etablissement_type),
                        ';') as etablissement_type_secondaire
                    FROM ".DB_PREFIXE."etablissement
                    LEFT JOIN ".DB_PREFIXE."lien_etablissement_e_type
                    ON lien_etablissement_e_type.etablissement = etablissement.etablissement
                    LEFT JOIN ".DB_PREFIXE."etablissement_nature
                        ON etablissement.etablissement_nature=etablissement_nature.etablissement_nature
                    WHERE etablissement.etablissement = ".$id."
                    GROUP BY etablissement.etablissement_type, etablissement.etablissement_categorie,
                    etablissement.si_locaux_sommeil, etablissement_nature.erp";
            $res = $this->f->db->query($sql);
            $this->f->addToLog(__METHOD__."(): db->query(\"".$sql."\");", VERBOSE_MODE);
            $this->f->isDatabaseError($res);

            // Ligne de résultat
            $row = &$res->fetchRow(DB_FETCHMODE_ASSOC);
            $types_sec = array();
            $types_sec = explode(';', $row["etablissement_type_secondaire"]);
            $row["etablissement_type_secondaire"] = $types_sec;

            // Retourne le résultat
            $return = $row;
        }

        // Retourne les résultat
        return $return;
    }

    /**
     * CONDITION - has_dossier_coordination_periodique.
     *
     * Vérifie si l'établissement a un dossier de coordination de visite
     * périodique. C'est-à-dire que le champ 'dossier_coordination_periodique'
     * de l'établissement n'est pas vide, que le DC en question existe ET que
     * son type est paramétré comme périodique (paramètre
     * **dossier_coordination_type_periodique**) ET n'est pas clôturé.
     *
     * @return boolean
     */
    function has_dossier_coordination_periodique() {
        $dossier_coordination_periodique = $this->getVal('dossier_coordination_periodique');
        if (!empty($dossier_coordination_periodique)) {
            $inst__dc = $this->f->get_inst__om_dbform(array(
                "obj" => "dossier_coordination",
                "idx" => $dossier_coordination_periodique,
            ));
            if ($inst__dc->is_periodic() == true
                && $inst__dc->getVal('dossier_cloture') == 'f') {
                $this->addToLog(__METHOD__."() - return TRUE", EXTRA_VERBOSE_MODE);
                return true;
            }
        }
        $this->addToLog(__METHOD__."() - return FALSE", EXTRA_VERBOSE_MODE);
        return false;
    }

    /**
     * Récupère une éventuelle entrée dans la matrice de périodicité des
     * visites par rapport au type, à la catégorie et au caractère locaux
     * à sommeil de l'établissement. Ce sont les critères secondaires indiquant
     * que l'établissement peut être soumis à des visites périodiques.
     *
     * @return boolean|integer false si aucune correspondance dans la matrice
     * de périodicité des visites sinon valeur de la périodicité en
     * années correspondant dans la matrice de périodicité des visites.
     */
    function get_periodicite_visites($type, $categorie, $locaux_sommeil) {
        // Vérifie la présence d'une entrée dans la matrice de périodicité
        // des visites correspond aux critères secondaires de l'établissement :
        // type, catégorie et caractère locaux à sommeil.
        $inst_util__periodicite_visites = $this->f->get_inst__om_dbform(array(
            "obj" => "periodicite_visites",
            "idx" => 0,
        ));
        $ret = $inst_util__periodicite_visites->get_periodicite_by_type_categorie_locaux_sommeil(
            $type,
            $categorie,
            $locaux_sommeil
        );
        // Si il y a une correspondance alors on retourne la périodicité sinon
        // on renvoi null pour indiquer qu'il n'y en a pas.
        if ($ret !== null) {
            $this->addToLog(__METHOD__."() - return ".$ret, EXTRA_VERBOSE_MODE);
            return $ret;
        }
        $this->addToLog(__METHOD__."() - return null", EXTRA_VERBOSE_MODE);
        return null;
    }

    /**
     * Récupère le dossier de coordination qui devrait, d'après son type, être
     * le dossier de visite périodique de l'établissement.
     *
     * @param integer $etablissement_id Identifiant de l'établissement
     *
     * @return mixed                    Identifiant du DC ou false
     */
    function get_supposed_dossier_coordination_periodique($dossier_coordination=null, $etablissement_id=null) {

        // Si l'identifiant n'est pas passé
        if ($etablissement_id == null) {
            // Utilise l'objet en cours
            $etablissement = $this;
        } else {
            // Sinon instancie l'établissement
            $etablissement = $this->f->get_inst__om_dbform(array(
                "obj" => "etablissement",
                "idx" => $etablissement_id,
            ));
        }

        // Récupère la liste des dossiers de coordination de l'établissement
        $list_dossier_coordination_by_etablissement = $etablissement->get_list_dossier_coordination_by_etablissement($etablissement->getVal($etablissement->clePrimaire), $dossier_coordination);

        // Pour chaque dossier de coordination
        foreach ($list_dossier_coordination_by_etablissement as $dossier_coordination_id) {

            // Instance de la classe dossier_coordination
            $dossier_coordination = $this->f->get_inst__om_dbform(array(
                "obj" => "dossier_coordination",
                "idx" => $dossier_coordination_id,
            ));

            // Vérifie si le dossier de coordination est du type périodique
            $is_periodic = $dossier_coordination->is_periodic();

            // Si le type correspond au type des visites périodique
            if ($is_periodic == true) {

                // Si le dossier de coordination n'est pas clôturé
                if ($dossier_coordination->getVal('dossier_cloture') == "f") {

                    //
                    $supposed_dossier_coordination_periodique = $dossier_coordination->getVal($dossier_coordination->clePrimaire);

                    // Retourne l'identifiant du dossier de coordination
                    $this->addToLog(__METHOD__."() - return ".$supposed_dossier_coordination_periodique, EXTRA_VERBOSE_MODE);
                    return $supposed_dossier_coordination_periodique;
                }
            }
        }

        // Aucun dossier de coordination n'est du bon type
        $this->addToLog(__METHOD__."() - return FALSE", EXTRA_VERBOSE_MODE);
        return false;

    }

    /**
     * Récupère la liste des dossier de coordination de l'établissement.
     *
     * @param integer $etablissement Identifiant de l'établissement
     *
     * @return array
     */
    function get_list_dossier_coordination_by_etablissement($etablissement, $dossier_coordination=null) {

        // Initialisation du résultat
        $result = array();

        // S'il y a un établissement
        if (!empty($etablissement)) {

            // Requête SQL
            $sql = "SELECT dossier_coordination
                    FROM ".DB_PREFIXE."dossier_coordination
                    WHERE etablissement = ".intval($etablissement);
            // Si l'identifiant d'un dossier de coordination est passé
            if ($dossier_coordination !== null) {
                // Ne retourne pas le dossier de coordination passé en paramètre
                $sql .= " AND dossier_coordination NOT IN (".intval($dossier_coordination).") ";
            }
            $this->f->addToLog(__METHOD__."() : db->query(\"".$sql."\")", VERBOSE_MODE);
            $res = $this->f->db->query($sql);
            $this->f->isDatabaseError($res);

            // Tant qu'il y a un résultat
            while ($row =& $res->fetchRow(DB_FETCHMODE_ASSOC)) {
                // Ajoute ce résultat au tableau
                $result[] = $row['dossier_coordination'];
            }
        }

        // Retourne le résultat
        return $result;
    }

    /**
     * Ajoute la durée de périodicité à une date.
     *
     * @param string  $date_visite Date
     * @param integer $periodicite Périodicité en années
     *
     * @return string
     */
    function sum_date_visite_periodicite($date_visite, $periodicite = null) {
        // Initialise la date calculée
        $date_periodicite = null;
        // Si la date passé est bien une date
        if ($this->f->check_date($date_visite) == true) {
            // Si l'identifiant de la périodicité n'est pas passé
            if ($periodicite == null) {
                // Récupère la périodicité de visite
                $periodicite = $this->getVal("si_periodicite_visites");
            }
            // Récupère la périodicité
            $periodicite_month = $periodicite * 12;
            // Date calculé
            $date_periodicite = $this->f->mois_date($date_visite, $periodicite_month);
        }
        // Retourne la date calculé
        return $date_periodicite;
    }

    /**
     * Met à jour le champ périodicité de visite de l'établissement.
     *
     * @param integer $periodicite_visites_id Identifiant de la périodicité.
     *
     * @return boolean
     */
    function update_etablissement_periodicite_visites($periodicite_visites) {
        $this->addToLog(__METHOD__."() - begin", EXTRA_VERBOSE_MODE);
        $valF = array(
            "si_periodicite_visites" => $periodicite_visites,
            "si_prochaine_visite_periodique_date_previsionnelle" => null,
        );
        if ($this->getVal("si_derniere_visite_periodique_date") !== null) {
            //
            $valF['si_prochaine_visite_periodique_date_previsionnelle'] = $this->sum_date_visite_periodicite(
                $this->getVal("si_derniere_visite_periodique_date"),
                $periodicite_visites
            );
        }
        $ret = $this->update_autoexecute($valF);
        if ($ret === false) {
            $this->addToLog(__METHOD__."() - end - return false", EXTRA_VERBOSE_MODE);
            return false;
        }
        $this->addToLog(__METHOD__."() - end - return true", EXTRA_VERBOSE_MODE);
        return true;
    }

    /**
     * Récupère le libellé du type de dossier de coordination paramétré
     * comme le type de visites périodiques.
     *
     * @return string
     */
    function get_dossier_coordination_type_periodique_libelle() {

        // Instance de la classe dossier_coordination_type
        $dossier_coordination_type = $this->f->get_inst__om_dbform(array(
            "obj" => "dossier_coordination_type",
            "idx" => 0,
        ));

        // Récupère l'identifiant
        $id = $dossier_coordination_type->get_dossier_coordination_type_by_code($this->f->getParameter("dossier_coordination_type_periodique"));

        // Instance de la classe dossier_coordination_type
        $dossier_coordination_type = $this->f->get_inst__om_dbform(array(
            "obj" => "dossier_coordination_type",
            "idx" => $id,
        ));

        // Récupère le libellé
        $libelle = $dossier_coordination_type->getVal('libelle');

        // Retourne le libellé
        return $libelle;
    }

    /**
     * Récupère la liste des identifiants de tous les établissements.
     *
     * @return array
     */
    function get_list_etablissement() {

        // Initialisation du résultat retourné
        $return = array();

        // Requête SQL
        $sql = "
            SELECT etablissement
            FROM ".DB_PREFIXE."etablissement";
        $this->f->addToLog(__METHOD__."() : db->query(\"".$sql."\")", VERBOSE_MODE);
        $res = $this->f->db->query($sql);
        $this->f->isDatabaseError($res);

        // Tant qu'il y a un résultat
        while ($row =& $res->fetchRow(DB_FETCHMODE_ASSOC)) {
            // Ajoute ce résultat au tableau
            $return[] = $row['etablissement'];
        }

        // Résultat retourné
        return $return;
    }

    /**
     * Ajoute un dossier de coordination périodique.
     *
     * @param string  $date_periodicite Date anniverdaire périodicité
     *
     * @return boolean
     */
    function add_dossier_coordination_periodique($date_periodicite) {
        // Logger
        $this->addToLog(__METHOD__."() - begin", EXTRA_VERBOSE_MODE);

        // Récupération du code du type de dossier de coordination qui
        // correspond à la visite périodique
        $dossier_coordination_type_periodique = $this->f->getParameter("dossier_coordination_type_periodique");

        // Instance de la classe dossier_coordination_type
        $dossier_coordination_type = $this->f->get_inst__om_dbform(array(
            "obj" => "dossier_coordination_type",
            "idx" => 0,
        ));
        // Récupération de l'identifiant du type de dossier de coordination
        $dossier_coordination_type_id = $dossier_coordination_type->get_dossier_coordination_type_by_code($dossier_coordination_type_periodique);

        // Intancie le type de dossier de coordination
        $dossier_coordination_type = $this->f->get_inst__om_dbform(array(
            "obj" => "dossier_coordination_type",
            "idx" => $dossier_coordination_type_id,
        ));
        // Récupère la liste des paramètres par défaut du dossier de
        // coordination
        $list_dossier_coordination_params = $dossier_coordination_type->get_dossier_coordination_type_param();

        // Récupère les informations de l'établissement nécessaire au dossier de 
        // coordination
        $list_etablissement_data = $this->get_data_etablissement();

        // Valeurs du dossier de coordination périodique
        $valF = array();
        $valF["dossier_coordination"] = "";
        $valF["dossier_coordination_type"] = $dossier_coordination_type_id;
        $valF["date_demande"] = $this->dateDBToForm($date_periodicite);
        $valF["date_butoir"] = $this->dateDBToForm($date_periodicite);
        $valF["etablissement"] = $this->getVal($this->clePrimaire);
        $valF["a_qualifier"] = $list_dossier_coordination_params['a_qualifier'];
        $valF["dossier_instruction_secu"] = $list_dossier_coordination_params['dossier_instruction_secu'];
        $valF["dossier_instruction_acc"] = $list_dossier_coordination_params['dossier_instruction_acc'];
        $valF["erp"] = $list_etablissement_data['etablissement_nature'];
        $valF["etablissement_locaux_sommeil"] = $list_etablissement_data['etablissement_locaux_sommeil'];
        $valF["etablissement_type"] = $list_etablissement_data['etablissement_type'];
        $valF["etablissement_categorie"] = $list_etablissement_data['etablissement_categorie'];

        // Ajoute le dossier de coordination
        $add_dossier_coordination = $this->add_dossier_coordination($valF);
        $this->addToLog(__METHOD__."() - return ".$add_dossier_coordination, EXTRA_VERBOSE_MODE);

        // Si l'ajout échoue
        if ($add_dossier_coordination === false) {
            // Stop le traitement
            return false;
        }

        // Logger
        $this->addToLog(__METHOD__."() - end", EXTRA_VERBOSE_MODE);

        // Continue le traitement
        return $add_dossier_coordination;
    }

    /**
     * Ajoute un dossier de coordination.
     *
     * @param array $valF Données de l'enregistrement
     *
     * @return integer    Identifiant du nouvel enregistrement
     */
    function add_dossier_coordination($valF) {
        // Logger
        $this->addToLog(__METHOD__."() - begin", EXTRA_VERBOSE_MODE);

        // Instance de la classe dossier_coordination
        $dossier_coordination = $this->f->get_inst__om_dbform(array(
            "obj" => "dossier_coordination",
            "idx" => "]",
        ));

        // Initialise à vide tous les champs de l'enregistrement
        foreach ($dossier_coordination->champs as $champ) {
            $val[$champ] = "";
        }

        // Fusionne les tableaux de données
        $data = array_merge($val, $valF);

        // Ajoute le dossier de coordination
        $add = $dossier_coordination->ajouter($data);

        // Si l'ajout échoue
        if ($add === false) {
            // Stop le traitement
            return false;
        }

        // Identifiant du nouvel enregistrement
        $id = $dossier_coordination->valF[$dossier_coordination->clePrimaire];

        // Logger
        $this->addToLog(__METHOD__."() - return ".$id, EXTRA_VERBOSE_MODE);

        // Logger
        $this->addToLog(__METHOD__."() - end", EXTRA_VERBOSE_MODE);

        // Continue le traitement et retourne l'identifiant
        return $id;
    }

    /**
     * Met à jour le champ dossier_coordination_periodique de l'établissement.
     *
     * @param integer $dossier_coordination_periodique Identifiant du DC
     * @param integer $etablissement_id                Identifiant de l'étab
     *
     * @return boolean
     */
    function update_etablissement_dossier_coordination_periodique($dossier_coordination_periodique, $etablissement_id=null) {
        // Logger
        $this->addToLog(__METHOD__."() - begin", EXTRA_VERBOSE_MODE);

        // Si l'identifiant n'est pas passé
        if ($etablissement_id == null) {
            // Utilise l'objet en cours
            $etablissement = $this;
        } else {
            // Sinon instancie l'établissement
            $etablissement = $this->f->get_inst__om_dbform(array(
                "obj" => "etablissement",
                "idx" => $etablissement_id,
            ));
        }

        // Met à jour l'établissement
        $update_etablissement_values = array(
            "dossier_coordination_periodique" => $dossier_coordination_periodique,
        );
        $update_etablissement = $etablissement->update_autoexecute($update_etablissement_values);

        // Logger
        $this->addToLog(__METHOD__."() - return ".$update_etablissement, EXTRA_VERBOSE_MODE);

        // Si la mise à jour a échouée
        if ($update_etablissement === false) {
            // Stop le traitement
            return false;
        }

        // Logger
        $this->addToLog(__METHOD__."() - end", EXTRA_VERBOSE_MODE);

        // Continue le traitement
        return true;
    }

    /**
     * Met à jour le champ si_derniere_visite_periodique_date de
     * l'établissement.
     *
     * @param string  $derniere_visite_periodique Date de mise à jour
     *
     * @return boolean
     */
    function update_etablissement_derniere_visite_periodique($derniere_visite_periodique) {
        // Logger
        $this->addToLog(__METHOD__."() - begin", EXTRA_VERBOSE_MODE);
        // Met à jour l'établissement
        $update_etablissement_values = array(
            "si_derniere_visite_periodique_date" => $derniere_visite_periodique,
        );
        $update_etablissement = $this->update_autoexecute($update_etablissement_values);
        // Logger
        $this->addToLog(__METHOD__."() - return ".$update_etablissement, EXTRA_VERBOSE_MODE);
        // Si la mise à jour a échouée
        if ($update_etablissement === false) {
            // Stop le traitement
            return false;
        }
        // Logger
        $this->addToLog(__METHOD__."() - end", EXTRA_VERBOSE_MODE);
        // Continue le traitement
        return true;
    }

    /**
     * Met à jour le champ si_prochaine_visite_periodique_date_previsionnelle
     * de l'établissement.
     *
     * @param string  $prochaine_visite_periodique Date de mise à jour
     *
     * @return boolean
     */
    function update_etablissement_prochaine_visite_periodique($prochaine_visite_periodique) {
        // Logger
        $this->addToLog(__METHOD__."() - begin", EXTRA_VERBOSE_MODE);
        // Met à jour l'établissement
        $update_etablissement_values = array(
            "si_prochaine_visite_periodique_date_previsionnelle" => $prochaine_visite_periodique,
        );
        $update_etablissement = $this->update_autoexecute($update_etablissement_values);
        // Logger
        $this->addToLog(__METHOD__."() - return ".$update_etablissement, EXTRA_VERBOSE_MODE);
        // Si la mise à jour a échouée
        if ($update_etablissement === false) {
            // Stop le traitement
            return false;
        }
        // Logger
        $this->addToLog(__METHOD__."() - end", EXTRA_VERBOSE_MODE);
        // Continue le traitement
        return true;
    }

    /**
     * Met à jour la date butoir du dossier de coordination périodique de
     * l'établissement.
     *
     * @param string  $date_butoir Nouvelle date butoir
     *
     * @return boolean
     */
    function update_dossier_coordination_periodique_date_butoir($date_butoir) {
        // Logger
        $this->addToLog(__METHOD__."() - begin", EXTRA_VERBOSE_MODE);
        // Identifiant du dossier de coordination périodique
        $dossier_coordination_periodique_id = $this->getVal('dossier_coordination_periodique');
        // Instance de la classe dossier_coordination
        $dossier_coordination = $this->f->get_inst__om_dbform(array(
            "obj" => "dossier_coordination",
            "idx" => $dossier_coordination_periodique_id,
        ));
        // Valeurs à mettre à jour
        $values = array(
            'date_butoir' => $this->dateDBToForm($date_butoir),
        );
        // Met à jour le dossier de coordination
        $update = $dossier_coordination->update_autoexecute($values);
        // Logger
        $this->addToLog(__METHOD__."() - return ".$update, EXTRA_VERBOSE_MODE);
        // Si la mise à jour a échouée
        if ($update === false) {
            // Stop le traitement
            return false;
        }
        // Logger
        $this->addToLog(__METHOD__."() - end", EXTRA_VERBOSE_MODE);
        // Continue le traitement
        return true;
    }

    /**
     * Gestion du dossier de coordination périodique de l'établissement.
     *
     * @param string  $mode Mode d'utilisation de la fonction
     * @param array   $val  Valeurs
     *
     * @return boolean
     */
    function handle_dossier_coordination_periodique($mode = null, $val = array()) {
        // Vérification des paramètres
        if (in_array($mode, array("update", "lock", "all")) !== true) {
            // Stop le traitement
            return false;
        }
        // Pour être soumis à des visites périodiques, l'établissement doit posséder
        // les bons critères primaires (nature et état correpondant au paramétrage)
        // ainsi que les bons critères secondaires (type, catégorie et caractère
        // locaux à sommeil correspondant dans la matrice de périodicité des visites).
        $is_primary_ok = $this->is_periodic_primary_condition_ok(
            $this->getVal("etablissement_nature"),
            $this->getVal("etablissement_etat"),
            $this->getVal("si_autorite_competente_visite")
        );
        $periodicite_visites = $this->get_periodicite_visites(
            $this->getVal('etablissement_type'),
            $this->getVal('etablissement_categorie'),
            ($this->getVal('si_locaux_sommeil') == 't' ? true : false)
        );
        $is_secondary_ok = ($periodicite_visites === null ? false : true);
        if ($is_primary_ok !== true
            || $is_secondary_ok !== true) {
            $this->addToMessage(sprintf(
                __("L'établissement %s ne possède pas les critères pour être soumis à des visites périodiques :"),
                '<span class="bold">'.$this->getVal('libelle').'</span>'
            ));
            if ($is_primary_ok !== true) {
                $this->addToMessage(
                    "- ".__("L'état et/ou la nature de l'établissement ne correspondent pas aux critères pour être soumis à des visites périodiques définit dans les paramètres.")
                );
            }
            if ($is_secondary_ok !== true) {
                $this->addToMessage(
                    "- ".__("Le type, la catégorie et la caractère locaux à sommeil de l'etablissement n'ont aucune correspondance dans la table de périodicité des visites.")
                );
            }
            if ($this->is_marked_as_periodic() === true) {
                $ret = $this->unmark_as_periodic();
                if ($ret !== true) {
                    return false;
                }
            }
            if ($this->getVal("dossier_coordination_periodique") !== null
                && $this->getVal("dossier_coordination_periodique") !== '') {
                $ret = $this->detach_dc_periodic();
                if ($ret !== true) {
                    return false;
                }
            }
            // Stop le traitement
            return false;
        }
        // Met à jour la périodicité de visite de l'établissement avec la périodicité
        // récupérée plus haut
        $ret = $this->update_etablissement_periodicite_visites($periodicite_visites);
        if ($ret === false) {
            // Stop le traitement
            return false;
        }
        // Vérifie si l'établissement a un dossier de coordination de visite
        // périodique. C'est-à-dire que le champ 'dossier_coordination_periodique'
        // de l'établissement n'est pas vide, que le DC en question existe ET que
        // son type est paramétré comme périodique (paramètre
        // **dossier_coordination_type_periodique**) ET n'est pas clôturé.
        $has_dossier_coordination_periodique = $this->has_dossier_coordination_periodique();

        // En mode "update"
        // Utilisé à l'ajout ou à la modification d'un dossier de coordination
        if ($mode == "update") {
            // Récupère le dossier de coordination passé en paramètre
            $dossier_coordination_periodique = isset($val['dossier_coordination_periodique']) ? $val['dossier_coordination_periodique'] : null;
            // Si l'identifiant du dossier de coordination n'est pas passé en
            // paramètre
            if ($dossier_coordination_periodique == null) {
                // Stop le traitement
                return false;
            }
            // Vérifie si l'établissement possède déjà un dossier de coordination
            // périodique
            if ($has_dossier_coordination_periodique == true
                && $this->getVal("dossier_coordination_periodique") != $dossier_coordination_periodique) {
                // On vide le message de validation de l'update fait en amont
                $this->msg = "";
                // Message d'erreur
                $msg_error = sprintf(__("L'etablissement %s possede deja un dossier de coordination de type %s."), ' <span class="bold">'.$this->getVal('libelle').'</span>', ' <span class="bold">'.$this->get_dossier_coordination_type_periodique_libelle().'</span>');
                $this->addToMessage($msg_error);
                // Stop le traitement
                return false;
            }

            // Recherche dans les dossiers de coordination existant
            $supposed_dossier_coordination_periodique = $this->get_supposed_dossier_coordination_periodique($dossier_coordination_periodique);

            // Si un autre dossier de coordination que celui demandé à lié est
            // trouvé
            if ($supposed_dossier_coordination_periodique != false) {
                // On vide le message de validation de l'update fait en amont
                $this->msg = "";
                // Message d'erreur
                $msg_error = sprintf(__("L'etablissement %s possede deja un dossier de coordination de type %s, mais celui-ci n'est pas correctement identifie."), ' <span class="bold">'.$this->getVal('libelle').'</span>', ' <span class="bold">'.$this->get_dossier_coordination_type_periodique_libelle().'</span>');
                $this->addToMessage($msg_error);
                $this->addToMessage(__("Veuillez contacter votre administrateur."));
                // Stop le traitement
                return false;
            }

            // Met à jour le champ dossier_coordination_periodique de
            // l'établissement
            $ret = $this->update_etablissement_dossier_coordination_periodique($dossier_coordination_periodique);
            if ($ret === false) {
                // Stop le traitement
                return false;
            }

            // Si l'établissement possède une date de dernière visite périodique
            // alors on la récupère pour calculer la date butoir du DC
            // sinon on positionne cette date butoir à la date du jour
            $etablissement_si_derniere_visite_periodique_date = $this->getVal('si_derniere_visite_periodique_date');
            if (!empty($etablissement_si_derniere_visite_periodique_date)) {
                $date_visite = $etablissement_si_derniere_visite_periodique_date;
                $date_visite_periodicite = $this->sum_date_visite_periodicite($date_visite, $periodicite_visites);
            } else {
                $date_visite_periodicite = date("Y-m-d");
            }

            // Met à jour la date butoir du dossier de coordination
            $ret = $this->update_dossier_coordination_periodique_date_butoir(
                $date_visite_periodicite
            );
            if ($ret === false) {
                // Stop le traitement
                return false;
            }

            // Continue le traitement
            return true;
        }

        // En mode "lock"
        // Utilisé à la clôture d'un dossier de coordination
        if ($mode == "lock") {
            // Récupère la date de la dernière visite périodique en paramètre
            $date_visite = isset($val['date_visite']) ? $val['date_visite'] : null;
            if ($date_visite == null) {
                // Stop le traitement
                return false;
            }
            // Met à jour la date de la dernière visite périodique sur l'établissement
            $ret = $this->update_etablissement_derniere_visite_periodique($date_visite);
            if ($ret === false) {
                // Stop le traitement
                return false;
            }
            // Récupère la date de la prochaine visite périodique
            // -> Date de dernière visite + périodicité
            // Met à jour la date de la prochaine visite périodique sur l'établissement
            $date_visite_periodicite = $this->sum_date_visite_periodicite($date_visite);
            $ret = $this->update_etablissement_prochaine_visite_periodique($date_visite_periodicite);
            if ($ret === false) {
                // Stop le traitement
                return false;
            }
            // Crée le nouveau dossier de coordination périodique
            $ret = $this->add_dossier_coordination_periodique($date_visite_periodicite);
            if ($ret === false) {
                // Stop le traitement
                return false;
            }

            // Continue le traitement
            return true;
        }

        // En mode "all"
        // Utilisé pour mettre à jour tous les établissements
        if ($mode == "all") {

            // Vérifie si l'établissement possède déjà un dossier de coordination
            // périodique
            if ($has_dossier_coordination_periodique == true) {
                // On vide le message de validation de l'update fait en amont
                $this->msg = "";
                // Message d'erreur
                $msg_error = sprintf(__("L'etablissement %s possede deja un dossier de coordination de type %s."), ' <span class="bold">'.$this->getVal('libelle').'</span>', ' <span class="bold">'.$this->get_dossier_coordination_type_periodique_libelle().'</span>');
                $this->addToMessage($msg_error);
                // Stop le traitement
                return false;
            }

            // Recherche dans les dossiers de coordination existant
            $supposed_dossier_coordination_periodique = $this->get_supposed_dossier_coordination_periodique();

            // Si un dossier de coordination est trouvé
            if ($supposed_dossier_coordination_periodique != false) {
                // Utilise le mode "update" de la même fonction
                $values = array(
                    "dossier_coordination_periodique" => $supposed_dossier_coordination_periodique,
                );
                $ret = $this->handle_dossier_coordination_periodique("update", $values);
                if ($ret === false) {
                    // Stop le traitement
                    return false;
                }

                // Continue le traitement
                return true;
            }

            // Si l'établissement possède une date de dernière visite périodique
            // alors on la récupère pour calculer la date butoir du DC
            // sinon on la positionne à la date du jour
            $etablissement_si_derniere_visite_periodique_date = $this->getVal('si_derniere_visite_periodique_date');
            if (!empty($etablissement_si_derniere_visite_periodique_date)) {
                $date_visite = $etablissement_si_derniere_visite_periodique_date;
                $date_visite_periodicite = $this->sum_date_visite_periodicite($date_visite, $periodicite_visites);
            } else {
                $date_visite_periodicite = date("Y-m-d");
            }

            // Ajoute le nouveau dossier de coordination périodique
            $ret = $this->add_dossier_coordination_periodique($date_visite_periodicite);
            if ($ret === false) {
                // Stop le traitement
                return false;
            }

            // Continue le traitement
            return true;
        }
    }

    /**
     * Indique si l'établissement possède les critères primaires pour être
     * soumis à des visites périodiques. Ces critères primaires sont : la
     * nature, l'état et l'autorité compétente (visite SI) de l'établissement.
     * Par exemple : la périodicité des visites s'applique sur les 
     * établissements dont la nature est 'ERP Référentiel' (paramètre 
     * **etablissement_nature_periodique** correpondant au code de la nature
     * 'ERPR') et dans l'état 'Ouvert' (**etablissement_etat_periodique** :
     * paramètre correpondant au code de l'état 'OUVE') et dont l'autorité
     * compétente (visite SI) est 'Commission Communale de Sécurité'
     * (**etablissement_autorite_competente_periodique** : paramètre
     * correspondant au code de l'autorité compétente 'CCS').
     *
     * @return boolean
     */
    function is_periodic_primary_condition_ok($nature, $etat, $si_autorite_competente_visite) {
        // Récupère l'identifiant de la nature des établissements étant soumis
        // à visites périodiques en fonction du paramétrage
        $inst_util__nature = $this->f->get_inst__om_dbform(array(
            "obj" => "etablissement_nature",
            "idx" => 0,
        ));
        $nature_periodique = $inst_util__nature->get_etablissement_nature_by_code(
            $this->f->getParameter("etablissement_nature_periodique")
        );
        $inst_util__nature->__destruct();
        // Récupère l'identifiant de l'état des établissements étant soumis
        // à visites périodiques en fonction du paramétrage
        $inst_util__etat = $this->f->get_inst__om_dbform(array(
            "obj" => "etablissement_etat",
            "idx" => 0,
        ));
        $etat_periodique = $inst_util__etat->get_etablissement_etat_by_code(
            $this->f->getParameter("etablissement_etat_periodique")
        );
        $inst_util__etat->__destruct();
        // Récupère le ou les identifiants des autorités compétentes étant soumis
        // à visites périodiques en fonction du paramétrage
        $param__etablissement_autorite_competente_periodique = $this->f->get_param__etablissement_autorite_competente_periodique();
        // Si l'établissement a la bonne nature et le bon état et la bonne
        // autorité compétente (visite SI) pour avoir des visites périodiques
        if ($nature == $nature_periodique
            && $etat == $etat_periodique
            && (count($param__etablissement_autorite_competente_periodique["ids"]) === 0
                || in_array($si_autorite_competente_visite, $param__etablissement_autorite_competente_periodique["ids"]) === true)) {
            //
            return true;
        }
        return false;
    }

    /**
     * TREATMENT - unmark_as_periodic.
     *
     * Permet de démarquer un établissement comme soumis à périodique.
     *
     * @return boolean
     */
    function unmark_as_periodic() {
        $ret = $this->update_autoexecute(array(
            "si_periodicite_visites" => null,
            "si_prochaine_visite_periodique_date_previsionnelle" => null,
        ));
        if ($ret !== true) {
            return false;
        }
        return true;
    }
    /**
     * TREATMENT - detach_dc_periodic.
     *
     * Permet de délier un éventuel DC périodique.
     *
     * @return boolean
     */
    function detach_dc_periodic() {
        $ret = $this->update_autoexecute(array(
            "dossier_coordination_periodique" => null,
        ));
        if ($ret !== true) {
            return false;
        }
        return true;
    }

    /**
     * CONDITION - is_marked_as_periodic.
     *
     * Permet d'indiquer si les informations de l'établissement indique qu'il est actuellement
     * soumis à visite périodique avec une périodicité.
     *
     * @return boolean
     */
    function is_marked_as_periodic() {
        if ($this->getVal("si_periodicite_visites") === ''
            && $this->getVal("si_prochaine_visite_periodique_date_previsionnelle") === '') {
            //
            return false;
        }
        return true;
    }

    /**
     * VIEW - view_synthesis.
     *
     * @return void
     */
    function view_synthesis() {
        // Logger
        $this->addToLog(__METHOD__."() - begin", EXTRA_VERBOSE_MODE);
        // Désactive les logs
        $this->f->disableLog();
        // Vérification de l'accessibilité sur l'élément
        $this->checkAccessibility();
        // Affichage de la synthèse
        $this->display_synthesis();
        // Logger
        $this->addToLog(__METHOD__."() - end", EXTRA_VERBOSE_MODE);
    }

    /**
     * 
     *
     * @return void
     */
    function display_synthesis() {
        // Logger
        $this->addToLog(__METHOD__."() - begin", EXTRA_VERBOSE_MODE);
        //
        echo $this->get_display_synthesis();
        // Logger
        $this->addToLog(__METHOD__."() - end", EXTRA_VERBOSE_MODE);
    }

    /**
     *
     * @return string
     */
    function get_display_synthesis() {
        //
        $output = "";

        /**
         * Données de l'établissement nécessaires
         */
        //
        $etablissement_id = $this->getVal($this->clePrimaire);
        //
        $etablissement_code = $this->getVal("code");
        $etablissement_libelle = $this->getVal("libelle");
        //
        $etablissement_etablissement_nature = $this->getVal("etablissement_nature");
        $etablissement_etablissement_type = $this->getVal("etablissement_type");
        $etablissement_etablissement_categorie = $this->getVal("etablissement_categorie");
        $etablissement_si_locaux_sommeil = $this->getVal("si_locaux_sommeil");
        //
        $etablissement_boite_postale = $this->getVal("boite_postale");
        $etablissement_cedex = $this->getVal("cedex");
        $etablissement_adresse_numero = $this->getVal("adresse_numero");
        $etablissement_adresse_numero2 = $this->getVal("adresse_numero2");
        $etablissement_adresse_voie = $this->getVal("adresse_voie");
        $etablissement_lieu_dit = $this->getVal("lieu_dit");
        $etablissement_adresse_complement = $this->getVal("adresse_complement");
        $etablissement_adresse_cp = $this->getVal("adresse_cp");
        $etablissement_adresse_ville = $this->getVal("adresse_ville");
        $etablissement_adresse_arrondissement = $this->getVal("adresse_arrondissement");

        /**
         * Formatage des données pour l'affichage
         */
        //// ETABLISSEMENT

        //// ADRESSE
        //
        $libelle_adresse_boite_postale = "";
        if (!empty($etablissement_boite_postale)) {
            $libelle_adresse_boite_postale = __("BP")." ".$etablissement_boite_postale;
        }
        //
        $libelle_adresse_cedex = "";
        if (!empty($etablissement_cedex)) {
            $libelle_adresse_cedex = __("Cedex")." ".$etablissement_cedex;
        }
        //
        $libelle_adresse_voie = $this->get_field_from_table_by_id(
            $etablissement_adresse_voie, 
            "libelle", 
            "voie"
        );
        //
        $libelle_adresse_arrondissement = $this->get_field_from_table_by_id(
            $etablissement_adresse_arrondissement, 
            "libelle", 
            "arrondissement"
        );
        // Découpage de l'adresse par ligne
        $adr = array();
        // Ligne 1
        $adr[] = array(
            $etablissement_adresse_numero,
            $etablissement_adresse_numero2,
            $libelle_adresse_voie,
        );
        // Ligne 2
        $adr[] = array(
            $etablissement_adresse_complement,
        );
        // Ligne 3
        $adr[] = array(
            $etablissement_lieu_dit,
            $libelle_adresse_boite_postale,
        );
        // Ligne 4
        $adr[] = array(
            $etablissement_adresse_cp,
            $etablissement_adresse_ville,
            $etablissement_adresse_arrondissement,
            $libelle_adresse_cedex,
        );
        //// NATURE
        //
        $libelle_nature = $this->get_field_from_table_by_id(
            $etablissement_etablissement_nature, 
            "nature", 
            "etablissement_nature"
        );
        //// TYPE, CATEGORIE ET LOCAUX A SOMMEIL
        //
        $libelle_type = "";
        if ($etablissement_etablissement_type != "") {
            $libelle_type = $this->get_field_from_table_by_id(
                $etablissement_etablissement_type, 
                "libelle", 
                "etablissement_type"
            );
        }
        //
        $libelle_categorie = "";
        if ($etablissement_etablissement_categorie != "") {
            $libelle_categorie = $this->get_field_from_table_by_id(
                $etablissement_etablissement_categorie, 
                "libelle", 
                "etablissement_categorie"
            );
        }
        //
        $libelle_locaux_sommeil = "";
        if ($etablissement_si_locaux_sommeil == "t") {
            $libelle_locaux_sommeil = __("locaux à sommeil");
        }



        //
        $adresse = "";
        // Compteur de ligne
        $i = 1;
        // Pour chaque ligne
        foreach ($adr as $ligne) {
            // Concat des données
            $value = $this->f->concat_text($ligne, " ");
            // Si la ligne n'est pas vide
            if (!empty($value)) {
                // Texte d'une ligne à afficher
                $text = '<span id ="etablissement_adresse_ligne'.$i.'" class="field_value">';
                $text .= $value;
                $text .= "</span>";
                // Affichage de la ligne
                $adresse .= $text."<br/>";
                // Incrémente le compteur
                $i++;
            }
        }

        /**
         *
         */
        $output = sprintf('
<div class="etablissement-synthesis" id="etablissement-synthesis-%1$s">
    <div class="legend_synthesis_etablissement">
        <a 
            id="link_etablissement" 
            class="lienFormulaire" 
            href="'.OM_ROUTE_FORM.'&obj=%11$s&amp;action=3&amp;idx=%1$s"
        >
            %2$s - %3$s
        </a>
    </div>
    %4$s
    <span id="etablissement-synthesis-%1$s-nature" class="etablissement-synthesis-nature">%5$s</span><br/>
    <span id="etablissement-synthesis-%1$s-type" class="etablissement-synthesis-type">%6$s %7$s</span>
    <span id="etablissement-synthesis-%1$s-categorie" class="etablissement-synthesis-categorie">%8$s %9$s</span>
    <span id="etablissement-synthesis-%1$s-locaux_sommeil" class="etablissement-synthesis-locaux_sommeil">%10$s</span>
</div>
',
            $etablissement_id,
            $etablissement_code,
            $etablissement_libelle,
            $adresse,
            $libelle_nature,
            ("type"),
            $libelle_type,
            __("categorie"),
            $libelle_categorie,
            $libelle_locaux_sommeil,
            $this->get_absolute_class_name()
        );
        //
        return $output;
    }

    /**
     * TREATMENT - update_tracking_visit_dates.
     *
     * Met à jour les informations de suivi de visites sur l'établissement : la
     * dernière visite SI (si_derniere_visite_date, si_derniere_visite_avis,
     * si_derniere_visite_technicien), la prochaine visite SI
     * (si_prochaine_visite_date, si_prochaine_visite_type), et la dernière
     * visite ACC (acc_derniere_visite_date, acc_derniere_visite_avis,
     * acc_derniere_visite_technicien).
     *
     * @return boolean
     */
    function update_tracking_visit_dates($val = array()) {
        $this->begin_treatment(__METHOD__);
        $valF = array();
        // Récupère la prochaine visite SI
        $si_next_visit = $this->get_si_next_visit();
        if ($si_next_visit != null) {
            $valF["si_prochaine_visite_date"] = $si_next_visit->getVal("date_visite");
            $valF["si_prochaine_visite_type"] = $si_next_visit->get_analyses_type();
        } else {
            $valF["si_prochaine_visite_date"] = null;
            $valF["si_prochaine_visite_type"] = null;
        }
        // Récupère la dernière visite SI
        $si_last_visit = $this->get_si_last_visit();
        if ($si_last_visit != null) {
            $valF["si_derniere_visite_date"] = $si_last_visit->getVal("date_visite");
            $valF["si_derniere_visite_avis"] = $si_last_visit->get_dossier_instruction_reunion_avis();
            $valF["si_derniere_visite_technicien"] = $si_last_visit->getVal("acteur");
        }
        // Récupère la dernière visite ACC
        $acc_last_visit = $this->get_acc_last_visit();
        if ($acc_last_visit != null) {
            // Valeurs de mise à jour
            $valF["acc_derniere_visite_date"] = $acc_last_visit->getVal("date_visite");
            $valF["acc_derniere_visite_avis"] = $acc_last_visit->get_dossier_instruction_reunion_avis();
            $valF["acc_derniere_visite_technicien"] = $acc_last_visit->getVal("acteur");
        }
        return $this->end_treatment(
            __METHOD__,
            $this->update_autoexecute($valF)
        );
    }

    /**
     * Récupère la dernière visite SI de l'établissement.
     *
     * @return resource|null Instance de la visite ou null si aucune visite.
     */
    function get_si_last_visit() {
        $sql = sprintf(
            'SELECT
                visite.visite
            FROM
                %1$svisite
                    LEFT JOIN %1$svisite_etat
                        ON visite.visite_etat=visite_etat.visite_etat
                    LEFT JOIN %1$sdossier_instruction
                        ON visite.dossier_instruction = dossier_instruction.dossier_instruction
                    LEFT JOIN %1$sdossier_coordination
                        ON dossier_instruction.dossier_coordination = dossier_coordination.dossier_coordination
                    LEFT JOIN %1$sdossier_instruction_reunion
                        ON dossier_instruction.dossier_instruction = dossier_instruction_reunion.dossier_instruction
                    LEFT JOIN %1$sreunion
                        ON dossier_instruction_reunion.reunion = reunion.reunion
                    LEFT JOIN %1$sservice
                        ON dossier_instruction.service = service.service
            WHERE
                visite.date_visite <= NOW()
                AND LOWER(visite_etat.code) = \'pla\'
                AND reunion.reunion_cloture IS TRUE
                AND LOWER(service.code) = \'si\'
                AND dossier_coordination.etablissement = %2$s
            ORDER BY 
                visite.date_visite DESC
            LIMIT 1',
            DB_PREFIXE,
            intval($this->getVal($this->clePrimaire))
        );
        $this->f->addToLog(__METHOD__."() db->getone(\"".$sql."\");", VERBOSE_MODE);
        $visite = $this->f->db->getone($sql);
        $this->f->isDatabaseError($visite);
        if (!empty($visite)) {
            return $this->f->get_inst__om_dbform(array(
                "obj" => "visite",
                "idx" => $visite,
            ));
        }
        return null;
    }

    /**
     * Récupère la dernière visite ACC de l'établissement.
     *
     * @return resource|null Instance de la visite ou null si aucune visite.
     */
    function get_acc_last_visit() {
        $sql = sprintf(
            'SELECT
                visite.visite
            FROM
                %1$svisite
                    LEFT JOIN %1$svisite_etat
                        ON visite.visite_etat=visite_etat.visite_etat
                    LEFT JOIN %1$sdossier_instruction
                        ON visite.dossier_instruction = dossier_instruction.dossier_instruction
                    LEFT JOIN %1$sdossier_coordination
                        ON dossier_instruction.dossier_coordination = dossier_coordination.dossier_coordination
                    LEFT JOIN %1$sdossier_instruction_reunion
                        ON dossier_instruction.dossier_instruction = dossier_instruction_reunion.dossier_instruction
                    LEFT JOIN %1$sreunion
                        ON dossier_instruction_reunion.reunion = reunion.reunion
                    LEFT JOIN %1$sservice
                        ON dossier_instruction.service = service.service
            WHERE
                visite.date_visite <= NOW()
                AND LOWER(visite_etat.code) = \'pla\'
                AND reunion.reunion_cloture IS TRUE
                AND LOWER(service.code) = \'acc\'
                AND dossier_coordination.etablissement = %2$s
            ORDER BY 
                visite.date_visite DESC
            LIMIT 1',
            DB_PREFIXE,
            intval($this->getVal($this->clePrimaire))
        );
        $this->f->addToLog(__METHOD__."() db->getone(\"".$sql."\");", VERBOSE_MODE);
        $visite = $this->f->db->getone($sql);
        $this->f->isDatabaseError($visite);
        if (!empty($visite)) {
            return $this->f->get_inst__om_dbform(array(
                "obj" => "visite",
                "idx" => $visite,
            ));
        }
        return null;
    }

    /**
     * Récupère la prochaine visite SI de l'établissement.
     *
     * @return resource|null Instance de la visite ou null si aucune visite.
     */
    function get_si_next_visit() {
        // Cas n°1 - on prend la prochaine visite dans le futur
        $sql = sprintf(
            'SELECT
                visite.visite
            FROM
                %1$svisite
                    LEFT JOIN %1$svisite_etat
                        ON visite.visite_etat=visite_etat.visite_etat
                    LEFT JOIN %1$sdossier_instruction
                        ON visite.dossier_instruction = dossier_instruction.dossier_instruction
                    LEFT JOIN %1$sdossier_coordination
                        ON dossier_instruction.dossier_coordination = dossier_coordination.dossier_coordination
                    LEFT JOIN %1$sservice
                        ON dossier_instruction.service = service.service
            WHERE
                visite.date_visite > NOW()
                AND LOWER(service.code) = LOWER(\'SI\')
                AND LOWER(visite_etat.code) = \'pla\'
                AND dossier_coordination.etablissement = %2$s
            ORDER BY
                visite.date_visite ASC
            LIMIT 1',
            DB_PREFIXE,
            intval($this->getVal($this->clePrimaire))
        );
        $this->f->addToLog(__METHOD__."() db->getone(\"".$sql."\");", VERBOSE_MODE);
        $visite = $this->f->db->getone($sql);
        $this->f->isDatabaseError($visite);
        if (!empty($visite)) {
            return $this->f->get_inst__om_dbform(array(
                "obj" => "visite",
                "idx" => $visite,
            ));
        }
        // Cas n°2 - il n'existe aucune visite dans le futur, alors on prend
        // la plus proche visite dans le passé si ce n'est pas la même que
        // celle qui correspond à la dernière visite officielle (avec réunion
        // clôturée)
        $sql = sprintf(
            'SELECT
                visite.visite
            FROM
                %1$svisite
                    LEFT JOIN %1$svisite_etat
                        ON visite.visite_etat=visite_etat.visite_etat
                    LEFT JOIN %1$sdossier_instruction
                        ON visite.dossier_instruction = dossier_instruction.dossier_instruction
                    LEFT JOIN %1$sdossier_coordination
                        ON dossier_instruction.dossier_coordination = dossier_coordination.dossier_coordination
                    LEFT JOIN %1$sservice
                        ON dossier_instruction.service = service.service
            WHERE
                visite.date_visite <= NOW()
                AND LOWER(service.code) = LOWER(\'SI\')
                AND LOWER(visite_etat.code) = \'pla\'
                AND dossier_coordination.etablissement = %2$s
            ORDER BY
                visite.date_visite DESC
            LIMIT 1',
            DB_PREFIXE,
            intval($this->getVal($this->clePrimaire))
        );
        $this->f->addToLog(__METHOD__."() db->getone(\"".$sql."\");", VERBOSE_MODE);
        $visite = $this->f->db->getone($sql);
        $this->f->isDatabaseError($visite);
        if (!empty($visite)) {
            $si_last_visit = $this->get_si_last_visit();
            if ($si_last_visit === null
                || $si_last_visit->getVal($si_last_visit->clePrimaire) != intval($visite)) {
                //
                return $this->f->get_inst__om_dbform(array(
                    "obj" => "visite",
                    "idx" => $visite,
                ));
            }
        }
        // Cas n°3 - aucune visite sur l'établissement
        return null;
    }

    /**
     * CONDITION - is_subject_to_periodic.
     *
     * @return boolean
     */
    function is_subject_to_periodic() {
        //
        $is_primary_ok = $this->is_periodic_primary_condition_ok(
            $this->getVal("etablissement_nature"),
            $this->getVal("etablissement_etat"),
            $this->getVal("si_autorite_competente_visite")
        );
        //
        $periodicite_visites = $this->get_periodicite_visites(
            $this->getVal('etablissement_type'),
            $this->getVal('etablissement_categorie'),
            ($this->getVal('si_locaux_sommeil') == 't' ? true : false)
        );
        $is_secondary_ok = ($periodicite_visites === null ? false : true);
        //
        if ($is_primary_ok !== true
            || $is_secondary_ok !== true) {
            return false;
        }
        return true;
    }

    function get_tot_ua_valid($id_etablissement = null) {
        // Si déjà connue on la retourne directement
        if ($this->tot_ua_valid !== null) {
            return $this->tot_ua_valid;
        }
        // Sinon on la calcule puis on la stocke et enfin on la retourne
        $ua = $this->f->get_inst__om_dbform(array(
            "obj" => "etablissement_unite",
            "idx" => 0,
        ));
        if ($id_etablissement == null) {
            $id_etablissement = intval($this->getVal($this->clePrimaire));
        }
        $tot_ua_valid = count($ua->get_list_for_etablissement($id_etablissement));
        $this->tot_ua_valid = $tot_ua_valid;
        return $tot_ua_valid;
    }

    /**
     * Vérifie si l'établissement à des unités d'accessibilité.
     *
     * @param integer $etablissement_id Identifiant de l'établissement
     *
     * @return boolean
     */
    function has_validated_unite_accessibilite($etablissement_id = null) {

        // Si le nombre d'unité d'accessibilité est différent de 0
        if ($this->get_tot_ua_valid($etablissement_id) > 0) {
            //
            return true;
        }
        //
        return false;
    }

    // {{{ BEGIN - CONDITION

    /**
     * CONDITION - is_referentiel.
     *
     * L'établissement est dit référentiel lorsque sa nature est de type
     * 'ERP Référentiel' dont le code dans la table 'etablissement_nature'
     * est 'ERPR'.
     *
     * @return boolean
     */
    function is_referentiel() {
        //
        if (count($this->val) === 0) {
            return null;
        }
        //
        $inst_etablissement_nature = $this->get_inst_etablissement_nature();
        if (strtolower($inst_etablissement_nature->getVal("code")) === strtolower($this->f->getParameter("etablissement_nature_erpr"))) {
            return true;
        }
        //
        return false;
    }

    // }}} END - CONDITION

    // {{{ BEGIN - GET INST

    /**
     *
     */
    function get_inst_etablissement_nature($etablissement_nature = null) {
        return $this->get_inst_common(
            "etablissement_nature",
            $etablissement_nature
        );
    }

    /**
     *
     */
    function get_inst_arrondissement($arrondissement = null) {
        return $this->get_inst_common(
            "arrondissement",
            $arrondissement,
            "adresse_arrondissement"
        );
    }

    /**
     * Récupère l'instance de la classe lien_etablissement_e_type.
     *
     * @param integer $lien_etablissement_e_type Identifiant de l'objet
     *
     * @return object Instance de classe
     */
    function get_inst_lien_etablissement_e_type($lien_etablissement_e_type) {
        //
        return $this->get_inst_common('lien_etablissement_e_type', $lien_etablissement_e_type);
    }

    // }}} END - GET INST


    /**
     * Permet de savoir si l'établissement a déjà été géolocalisé, et existe sur
     * le SIG.
     *
     * @return boolean true si l'établissement n'est pas géolocalisé, sinon true.
     */
    function is_not_geolocalised() {

        $is_geolocalised = $this->getVal('geolocalise');
        //
        if ($is_geolocalised === 't') {
            //
            return false;
        }
        return true;
    }

    /**
     * VIEW - view_localiser
     * Redirige l'utilisateur vers le SIG externe.
     *
     * @return void
     */
    public function view_localiser() {
        $this->checkAccessibility();
        // Code de l'établissement
        $code = $this->getVal('code');
        // Instance geoaria
        $geoaria = $this->f->get_inst_geoaria();
        if($geoaria === false) {
            // L'erreur geoaria est affichée dans la méthode handle_geoaria_exception
            return false;
        }

        // Si la redirection est sur la base des parcelles de l'établissement
        if ($this->f->get_submitted_get_value('option') === 'parcelles' AND $this->getVal('references_cadastrales') != '') {
            // Formattage des parcelles dans un tableau avant l'envoi au connecteur
            $tab_parcelles = $this->f->parseParcelles($this->getVal('references_cadastrales'));
            // Récupération du lien de redirection vers le SIG
            try {
                $url = $geoaria->redirection_web('parcelle', $tab_parcelles);
            } catch (geoaria_exception $e) {
                $this->handle_geoaria_exception($e);
                return;
            }
        }
        elseif ($this->is_not_geolocalised() === false){
            try {
                $url = $geoaria->redirection_web('etablissement', array($code));
            } catch (geoaria_exception $e) {
                $this->handle_geoaria_exception($e);
                return;
            }
        }
        // Sinon redirection vers le sig sans argument
        else {
            try {
                $url = $geoaria->redirection_web();
            } catch (geoaria_exception $e) {
                $this->handle_geoaria_exception($e);
                return;
            }
        }
        // Redirection
        header("Location: ".$url);

    }

    /**
     * VIEW - contrainte.
     *
     * Vue des contraintes de l'établissement
     *
     * Cette vue permet de gérer le contenu de l'onglet "Contraintes" sur un 
     * étblissement. Cette vue spécifique est nécessaire car l'ergonomie standard du
     * framework ne prend pas en charge ce cas.
     * C'est ici la vue spécifique des contraintes liées à l'établissement qui est
     * affichée directement au clic de l'onglet au lieu du soustab.
     * 
     * L'idée est donc de simuler l'ergonomie standard en créant un container 
     * et d'appeler la méthode javascript 'ajaxit' pour charger le contenu 
     * de la vue visualisation de l'objet lié.
     * 
     * @return void
     */
    function view_contrainte() {
        // Vérification de l'accessibilité sur l'élément
        $this->checkAccessibility();

        // Récupération des variables GET
        ($this->f->get_submitted_get_value('idxformulaire')!==null ? $idxformulaire = 
            $this->f->get_submitted_get_value('idxformulaire') : $idxformulaire = "");
        ($this->f->get_submitted_get_value('retourformulaire')!==null ? $retourformulaire = 
            $this->f->get_submitted_get_value('retourformulaire') : $retourformulaire = "");

        // Objet à charger
        $obj = "lien_contrainte_etablissement";
        // Construction de l'url de sousformulaire à appeler
        $url = OM_ROUTE_SOUSFORM."&obj=$obj";
        $url .= "&idx=0";
        $url .= "&action=4";
        $url .= "&retourformulaire=$retourformulaire";
        $url .= "&idxformulaire=$idxformulaire";
        $url .= "&retour=form";
        // Affichage du container permettant le raffraichissement du contenu
        // dans le cas des action-direct.
        printf('
            <div id="sousform-href" data-href="%s">
            </div>',
            $url
        );
        // Affichage du container permettant de charger le retour de la requête
        // ajax récupérant le sous formulaire.
        printf('
            <div id="sousform-%s">
            </div>
            <script>
            ajaxIt(\'%s\', \'%s\');
            </script>',
            $obj,
            $obj,
            $url
        );
    }

    /**
     * VIEW - view_numeroter.
     *
     * Formulaire de renumérotation d'un établissement.
     *
     * @return void
     */
    function view_numeroter() {
        $this->checkAccessibility();

        // Ajout de l'avertissement éventuel
        $nb_doc = $this->get_nombre_document_genere();
        if ( $nb_doc > 0 ) {
            $msg_nb_doc_gen = _("Attention")." : ".$nb_doc." "._("documents ont été générés pour cet établissement, contenant potentiellement le code actuel.");
            $this->addToMessage($msg_nb_doc_gen);
        }

        /**
         * TREATMENT
         */
        $postvar = $this->getParameter("postvar");
        if ($postvar != null) {
            $code = trim($postvar['code']);
            $ret = $this->numeroter(array("code" => $code, ));
            if ($ret !== false) {
                $this->redirect_to_back_link("formulaire");
                return;
            }
        }

        /**
         * FORM
         */
        // Affichage du message avant d'afficher le formulaire
        $this->message();
        //
        $this->f->layout->display__form_container__begin(array(
            "action" => $this->getDataSubmit(),
            "id" => "etablissement_numeroter_form",
        ));
        //
        $champs = array("etablissement", "code_courant", "code");
        //
        $form = $this->f->get_inst__om_formulaire(array(
            "champs" => $champs,
            "validation" => 0,
            "maj" => 1,
        ));
        //
        $form->setType("etablissement", "hidden");
        $form->setVal("etablissement", $this->getVal($this->clePrimaire));
        //
        $form->setType("code_courant", "static");
        $form->setLib("code_courant", __("Code actuel"));
        $form->setVal("code_courant", $this->getVal("code"));
        //
        $form->setType("code", "text");
        $form->setLib("code", __("Nouveau code")." *");
        $form->setMax("code", 25);
        $form->setTaille("code", 40);
        $form->setVal("code", '');
        //
        $form->entete();
        $form->afficher($champs, 0, false, false);
        $form->enpied();
        //
        $this->f->layout->display__form_controls_container__begin(array(
            "controls" => "bottom",
        ));
        $this->f->layout->display__form_input_submit(array(
            "name" => "validation",
            "value" => __("Renuméroter"),
        ));
        $this->retour();
        $this->f->layout->display__form_controls_container__end();
        $this->f->layout->display__form_container__end();
    }    

    /**
     * TREATMENT - numeroter.
     * 
     * @return boolean
     */
    function numeroter($val = array()) {
        $this->begin_treatment(__METHOD__);
        // setValF
        $this->valF = array(
            "etablissement" => $this->getVal($this->clePrimaire),
            "code" => $val["code"],
        );
        $this->setter_treatment__code(array("code" => $val["code"], ));
        // verifier
        if ($this->correct === true
            && $this->valF["code"] === "") {
            //
            $this->correct = false;
            $this->addToMessage(__("Le champ <b>code</b> est obligatoire."));
        }
        if ($this->correct === true
            && $this->isUnique("code", $this->valF["code"]) !== true) {
            //
            $this->correct = false;
            $this->addToMessage( __("La valeur saisie dans le champ")." <span class=\"bold\">".$this->getLibFromField("code")."</span> ".__("existe deja, veuillez saisir une nouvelle valeur."));
        }
        $this->check_treatment__code_format(array("code" => $val["code"], ));
        if ($this->correct !== true) {
            $this->addToMessage();
            $this->addToMessage(__("SAISIE NON ENREGISTREE"));
            return false;
        }
        //
        $ret = $this->update_autoexecute($this->valF);
        if ($ret !== true) {
            $this->addToLog(
                __METHOD__ . "(): Erreur lors de la mise à jour de l'enregistrement en base de données",
                DEBUG_MODE
            );
            return $this->end_treatment(__METHOD__, false);
        }
        $this->addToMessage(sprintf(
            __("Etablissement %s renuméroté %s"),
            $this->getVal("code"),
            $this->valF["code"]
        ));       
        return $this->end_treatment(__METHOD__, true);
   }

    /**
     * Retourne toutes les parcelles de l'établissement
     *
     * @return array
     */
    public function get_formated_parcelles() {
        // Résultat retourné
        return $this->f->parseParcelles($this->getVal('references_cadastrales'));
    }


    /**
     * Méthode de géocodage directement appellée par une action.
     * Cette méthode gère les exceptions et les messages d'erreur.
     *
     * @return boolean
     */
    function geocoder() {

        // Begin
        $this->begin_treatment(__METHOD__);

        // On considère que le traitement est toujours valide
        // afin que l'action ne soit jamais en erreur
        $this->geocoder_treatment();
        //
        return $this->end_treatment(__METHOD__, true);
    }

    /**
     * TREATMENT - Méthode de géocodage de l'établissement sur le SIG.
     * 
     * Cette méthode peut seulement être appelée sur un établissement qui n'a pas encore
     * été géolocalisé. Elle ne doit pas faire obstacle à sa création.
     * 
     * Elle renvoie FAUX si l'établissement n'est pas géocodé, du fait d'une nécessité
     * d'intervention manuelle ou d'une erreur BBD/SIG.
     *
     * @throws  void
     * @return  boolean
     */
    function geocoder_treatment() {

        // Récupération des valeurs du formulaire dans le contexte de création d'un nouvel étab
        if ($this->get_action_crud() === 'create') {
            $code_etablissement = $this->valF['code'];
            $references_cadastrales = $this->valF['references_cadastrales'];
            $numero_voie = isset($this->valF['adresse_numero']) ? $this->valF['adresse_numero'] : '';
            $id_etablissement = $this->valF[$this->clePrimaire];
        }
        else {
            $code_etablissement = $this->getVal("code");
            $references_cadastrales = $this->getVal("references_cadastrales");
            $numero_voie = $this->getVal("adresse_numero");
            $id_etablissement = $this->getVal($this->clePrimaire);
        }
        $tab_parcelles = $this->f->parseParcelles($references_cadastrales);

        // Tableau associatif de paramètres passés au SIG
        $params = array (
             'parcelles' => $tab_parcelles,
             'numero' => $numero_voie,
             'ref_voie' => $this->get_id_voie_ref_by_etablissement_code($code_etablissement),
             'dossier_ads' => '',
        );
        // Interrogation du web service du SIG
        $precision_geocodage = '';
        //
        try {
            //
            $geoaria = $this->f->get_inst_geoaria();
            $ret_geocode = $geoaria->geocoder_objet('etablissement', $code_etablissement, $params);
            if (is_array($ret_geocode) === true
                && array_key_exists("precision", $ret_geocode) === true) {
                $precision_geocodage = $ret_geocode["precision"];
            }
        } catch (geoaria_connector_4XX_exception $e) {

            // Si la géolocalisation de cet établissement à déjà été faite
            if ($e->get_typeException() === 'geocoder_objet_already_exist') {
                // Met à jour le flag de géolocalisation
                $update = $this->update_geolocalise(
                    array(
                        "geolocalise" => "t",
                    ),
                    $id_etablissement
                );

                // Si la mise à jour échoue
                if ($update === false) {
                    // Stop le traitement
                    return false;
                }

                //
                $this->addToMessage(__("L'établissement est déjà géolocalisé."));
                //
                return false;
            }

            // Si la géolocalisation de cet établissement à déjà été faite
            if ($e->get_typeException() === 'geocoder_objet_cant_geocode') {
                // Template du lien vers le SIG
                $template_link_redirection_web_emprise = "<br/><a id='link_dessin_emprise_sig' title=\"%s\" class='lien' target='_blank' href='%s'><span class='om-icon om-icon-16 om-icon-fix sig-16'></span>%s</a>";
                //
                try {
                    //
                    $url_redirection_web_emprise = $this->redirection_web_emprise();

                } catch (geoaria_exception $e) {
                    //
                    $this->handle_geoaria_exception($e);
                    return $this->end_treatment(__METHOD__, false);
                }
                //
                $this->addToMessage(__("L'établissement n'a pas pu être créé automatiquement sur le SIG."));
                // Affichage du lien seulement s'il n'est pas vide
                if ($url_redirection_web_emprise != ''){
                    $link_redirection_web_emprise = sprintf(
                        $template_link_redirection_web_emprise,
                        __("Dessiner l'établissement sur le SIG"),
                        $url_redirection_web_emprise,
                        __("Cliquez ici pour le dessiner.")
                    );
                    $this->addToMessage(__($link_redirection_web_emprise."<br/>"));
                }
            }

            //
            return false;

        } catch (geoaria_exception $e) {
            //
            $this->addToMessage($this->handle_geoaria_exception($e));
            //
            return false;
        }

        // Traitement de la réponse du web service du SIG
        if ($precision_geocodage !== false AND $precision_geocodage !== '') {
            // Met à jour le flag de géolocalisation
            if (is_array($ret_geocode) === true
                && array_key_exists("x", $ret_geocode) === true
                && array_key_exists("y", $ret_geocode) === true) {
                //
                $update = $this->update_geolocalise(
                    array(
                        "geolocalise" => "t",
                        "x" => $ret_geocode["x"],
                        "y" => $ret_geocode["y"],
                    ),
                    $id_etablissement
                );
            } else {
                $update = $this->update_geolocalise(
                    array(
                        "geolocalise" => "t",
                    ),
                    $id_etablissement
                );
            }

            // Si la mise à jour échoue
            if ($update === false) {
                // Stop le traitement
                return false;
            }

            //
            $this->addToMessage(
                sprintf(
                    __("L'établissement a été géolocalisé avec une précision de %sm."),
                    $precision_geocodage
                )
            );
            //
            return true;
        }

        //
        $this->addToMessage(__("L'établissement n'a pas pu être créé sur le SIG."));
        //
        return false;
    }


    /**
     * Met à jour le flag de géolocalisation de l'établissement.
     *
     * @param string $value Valeur du champ.
     *
     * @return boolean
     */
    protected function update_geolocalise($value, $etablissement) {

        // Valeur à mettre à jour
        $valF = array();
        if (array_key_exists("geolocalise", $value) === true) {
            $valF["geolocalise"] = $value["geolocalise"];
        }
        if (array_key_exists("x", $value) === true
            && array_key_exists("y", $value) === true) {
            //
            $coord = $value['x']." ".$value['y'];
            // Vérifie que les coordonées ne sont pas vides
            if (trim($coord) === '') {
                return false;
            }

            // Met à jour le centroide dans le dossier
            $sql = sprintf(
                'UPDATE
                    %1$setablissement
                SET
                    geom_point = public.ST_GeomFromText(\'POINT(%2$s)\', %3$s)
                WHERE
                    etablissement = %4$s',
                DB_PREFIXE,
                $coord,
                2154,
                intval($etablissement)
            );
            $res = $this->f->db->query($sql);
            $this->f->addToLog(__METHOD__.": db->query(\"".$sql."\"", VERBOSE_MODE);
            if ($this->f->isDatabaseError($res, true) !== false) {
                return false;
            }
        }
        // Met à jour l'enregistrement
        $update = $this->update_autoexecute($valF, $etablissement, false);

        // Si la mise à jour échoue
        if ($update == false) {
            // Stop le traitement
            return false;
        }

        // Continue le traitement
        return true;
    }


    /**
     * Surcharge de la méthode du core afin de faire appel à la fonction JS
     * getDataFieldReferenceCadastrale().
     *
     * @param integer $maj Mode de mise a jour
     * @return void
     */
    function bouton($maj) {
        if (!$this->correct
            && $this->checkActionAvailability() == true) {
            // Ancienne gestion des actions
            if ($this->is_option_class_action_activated() == false) {
                switch($maj) {
                    case 0 :
                        $bouton = __("Ajouter");
                        break;
                    case 1 :
                        $bouton = __("Modifier");
                        break;
                    case 2 :
                        $bouton = __("Supprimer");
                        break;
                    case 999 :
                        $bouton = __("Rechercher");
                        break;
                    default :
                        $bouton = __("Valider");
                        break;
                }
            }
            // Nouvelle gestions des actions
            if ($this->is_option_class_action_activated() == true) {
                // Actions SCRUD ou indéfinies
                if ($this->get_action_param($maj, "button") == null) {
                    // Récupération du mode de l'action
                    $crud = $this->get_action_crud($maj);
                    switch($crud) {
                        case 'create' :
                            $bouton = __("Ajouter");
                            break;
                        case 'update' :
                            $bouton = __("Modifier");
                            break;
                        case 'delete' :
                            $bouton = __("Supprimer");
                            break;
                        case 'search' :
                            $bouton = __("Rechercher");
                            break;
                        default :
                            $bouton = __("Valider");
                            break;
                    }
                }
                // Actions spécifiques
                else {
                    //
                    $bouton = $this->get_action_param($maj, "button");
                    
                }
            }
            //
            $params = array(
                "value" => $bouton,
                "name" => "submit",
                "onclick" => "return getDataFieldReferenceCadastrale();"
            );
            //
            $this->f->layout->display_form_button($params);
        }
    }

    /**
     * Méthode renvoyant l'URL permettant de dessiner manuellement l'élément sur le SIG.
     *
     * @return string $url_redirection_web_emprise
     */
    private function redirection_web_emprise() {

        // Instance geoaria
        $geoaria = $this->f->get_inst_geoaria();

        if ($this->get_action_crud() === 'create') {
            $etablissement_code = $this->valF['code'];
        }
        else {
            $etablissement_code = $this->getVal('code');
        }
        //
        $ref_voie = $this->get_id_voie_ref_by_etablissement_code($etablissement_code);

        //
        $data = array (
            'id' => $etablissement_code,
            'ref_voie' => $ref_voie,
        );
        // Interrogation du web service du SIG
        try {
            $url_redirection_web_emprise = $geoaria->redirection_web_emprise('etablissement', $data);
        } catch (geoaria_exception $e) {
            $this->handle_geoaria_exception($e);
            return '';
        }
        return $url_redirection_web_emprise;
    }


    /**
     * 
     * Retourne l'identifiant de la voie du référentiel lié à l'établissement
     * passé en paramètre.
     *
     * @param string $etablissement_code Code de l'établissement.
     *
     * @return mixed false si erreur sinon l'identifiant de la voie.
     */
    function get_id_voie_ref_by_etablissement_code($etablissement_code) {
        //
        $sql = "SELECT
                    voie.id_voie_ref
                FROM
                    ".DB_PREFIXE."etablissement
                LEFT JOIN
                    ".DB_PREFIXE."voie
                    ON etablissement.adresse_voie = voie.voie
                WHERE
                    etablissement.code = '".$etablissement_code."'";
        $id_voie_ref = $this->f->db->getOne($sql);
        if ($id_voie_ref === null) {
            $id_voie_ref = "";
        }
        $this->addToLog("get_id_voie_ref_by_etablissement_code(): db->getone(\"".$sql."\");", VERBOSE_MODE);
        $this->f->isDatabaseError($id_voie_ref);

        return $id_voie_ref;
    }

    /**
     * [construct_contraintes description]
     * @param  [type] $contraintes_params [description]
     * @return [type]                     [description]
     */
    public function construct_contraintes($contraintes_params = null) {
        // Variable à afficher à la place de "&contraintes"
        $contraintes = "";
        // Tableau associatif à 2 dimensions, qui contient l'ensemble des paramètres fournis à 
        // &contraintes explosés, une ligne du tableau contient un nom de groupe, sous-groupe
        // ou une valeur. 
        $conditions = array();

        // SELECT
        $selectContraintes = "SELECT 
            lien_contrainte_etablissement.texte_complete as lien_contrainte_etablissement_texte,
            lower(contrainte.groupe) as contrainte_groupe,
            lower(contrainte.sousgroupe) as contrainte_sousgroupe ";

        // FROM
        $fromContraintes = " FROM ".DB_PREFIXE."contrainte 
            LEFT JOIN ".DB_PREFIXE."lien_contrainte_etablissement
                ON  lien_contrainte_etablissement.contrainte = contrainte.contrainte ";

        // WHERE
        $whereContraintes = sprintf(
            " WHERE lien_contrainte_etablissement.etablissement = %s ",
            $this->getVal($this->clePrimaire)
        );

        $triContraintes = " ORDER BY contrainte_groupe DESC, 
            contrainte_sousgroupe, 
            contrainte.ordre_d_affichage, 
            contrainte.libelle ";

        // S'il y a des paramètres
        if ($contraintes_params !== null) {
            // Explose les paramètres et valeurs récupérées dans un tableau
            $conditions = $this->f->explode_condition_contrainte($contraintes_params[1]);
            // Récupération des conditions à ajouter au WHERE de la requête SQL
            $whereContraintes .= $this->f->treatment_condition_contrainte(NULL, $conditions);
        }

        // Tri différent sur les contraintes si l'affichage est sans arborescence
        if (isset($conditions['affichage_sans_arborescence']) && $conditions['affichage_sans_arborescence'] == 't') {
            $triContraintes = " ORDER BY contrainte.ordre_d_affichage, contrainte.libelle ";
        }

        // Requête
        $sqlContraintes = $selectContraintes.$fromContraintes.$whereContraintes.$triContraintes;
        $resContraintes = $this->f->db->query($sqlContraintes);
        $this->f->addToLog(__METHOD__." : db->query(\"".$sqlContraintes."\");", 
            VERBOSE_MODE);
        // Étant donné le contexte d'édition PDF,
        // si erreur BDD alors on stoppe le traitement et on affiche le message
        $this->f->isDatabaseError($resContraintes);

        // S'il y a un résultat
        if ($resContraintes->numRows() != 0) {
            
            // Sauvegarde des données pour les comparer
            $lastRowContrainte = array();
            $lastRowContrainte['contrainte_groupe'] = 'empty';
            $lastRowContrainte['contrainte_sousgroupe'] = 'empty';
            $contraintes .= "<table width=\"auto\" >";
            // Tant qu'il y a un résultat
            while ($rowContrainte =& $resContraintes->fetchRow(DB_FETCHMODE_ASSOC)) {
                // Si l'identifiant du groupe de la contrainte présente et celui d'avant sont
                // différents, et si l'option affichage_sans_arborescence est désactivée
                if ($rowContrainte['contrainte_groupe'] != $lastRowContrainte['contrainte_groupe']
                    && (!isset($conditions['affichage_sans_arborescence'])
                    || $conditions['affichage_sans_arborescence'] != 't')) {
                    $contraintes .= 
                        "<tr><td style=\"width:10px; text-align:right;\"> - </td>
                        <td style=\"width:5px;\"></td><td>".
                        mb_strtoupper($rowContrainte['contrainte_groupe'], 'UTF-8')."</td></tr>";
                    
                }

                // Si l'identifiant du sousgroupe de la contrainte présente et celui d'avant sont
                // différents, ou s'ils sont identiques mais n'appartiennent pas au même groupe
                // Et si l'option affichage_sans_arborescence est désactivée
                if (($rowContrainte['contrainte_sousgroupe'] != $lastRowContrainte['contrainte_sousgroupe']
                    || $rowContrainte['contrainte_groupe'] != $lastRowContrainte['contrainte_groupe'])
                    &&  $rowContrainte['contrainte_sousgroupe'] != "" && (!isset($conditions['affichage_sans_arborescence'])
                    || $conditions['affichage_sans_arborescence'] != 't')) {
                        $contraintes .=
                        "<tr><td style=\"width:30px; text-align:right;\"> - </td>
                        <td style=\"width:5px;\"></td><td>".
                        mb_strtoupper($rowContrainte['contrainte_sousgroupe'], 'UTF-8')."</td></tr>";

                }
                // Si l'option d'affichage sans arborescence n'est pas activée, on ajoute les
                // contraintes avec alinéas avec tirets.
                // Sinon on affiche les contraintes sans tirets et alinéas.
                if (!isset($conditions['affichage_sans_arborescence']) || $conditions['affichage_sans_arborescence'] != 't') {
                    $contraintes .= 
                    "<tr><td style=\"width:50px; text-align:right;\"> - </td>
                    <td style=\"width:5px;\"></td><td>".
                    ucfirst($rowContrainte['lien_contrainte_etablissement_texte'])."</td></tr>";
                }
                else {
                    $contraintes .= 
                    "<tr><td>".ucfirst($rowContrainte['lien_contrainte_etablissement_texte'])."</td></tr>";
                }
                // sauvegarde des valeurs avant nouvelle itération
                $lastRowContrainte=$rowContrainte;
            }
            $contraintes .= "</table>";
        }
        //
        return $contraintes;
    }

    /**
     * Récupère le nombre de documents générés pour l'établissement.
     *
     * @return integer Nombre de documents
     */
    function get_nombre_document_genere() {
        $sql = sprintf(
            'SELECT COUNT(1) FROM %1$scourrier WHERE etablissement=%2$s',
            DB_PREFIXE,
            intval($this->getVal($this->clePrimaire))
        );
        $nb = $this->f->db->getOne($sql);
        $this->addToLog(__METHOD__."(): db->getone(".$sql.");", VERBOSE_MODE);
        $this->f->isDatabaseError($nb);
        return intval($nb);
    }

    /**
     * CONDITION - is_option_etablissement_code_manual_enabled.
     *
     * @return boolean
     */
    function is_option_etablissement_code_manual_enabled() {
        if (trim($this->f->getParameter("option_etablissement_code")) === "manual") {
            return true;
        }
        return false;
    }
}

