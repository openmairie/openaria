<?php
/**
 * Ce script définit la classe 'dossier_instruction_reunion_contexte_di'.
 *
 * @package openaria
 * @version SVN : $Id$
 */

require_once "../obj/dossier_instruction_reunion.class.php";

/**
 * Définition de la classe 'dossier_instruction_reunion_contexte_di' (om_dbform).
 *
 * Surcharge de la classe 'dossier_instruction_reunion'.
 */
class dossier_instruction_reunion_contexte_di extends dossier_instruction_reunion {

    /**
     *
     */
    protected $_absolute_class_name = "dossier_instruction_reunion_contexte_di";

}

