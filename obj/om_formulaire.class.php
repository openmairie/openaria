<?php
/**
 * Ce script définit la classe 'om_formulaire'.
 *
 * @package openaria
 * @version SVN : $Id$
 */

require_once PATH_OPENMAIRIE."om_formulaire.class.php";

/**
 * Définition de la classe 'om_formulaire' (om_formulaire).
 *
 * Cette classe est destinée à permettre la surcharge de certaines méthodes de
 * la classe 'om_formulaire' du framework pour des besoins spécifiques de
 * l'application.
 */
class om_formulaire extends formulaire {

    function selecthidden($champ, $validation, $DEBUG = false) {
        $this->select($champ, $validation, $DEBUG);
    }

    /**
     * Widget - Saisie des références cadastrales.
     *
     * Ce type permet d'afficher un widget de formualire avec une aide à la 
     * saisie pour la saisie de références cadastrales.
     *
     * @param string  $champ      Nom du champ.
     * @param integer $validation Etat de la validation du formulaire.
     * @param boolean $DEBUG      Parametre inutilise.
     *
     * @return void
     */
    function referencescadastrales($champ, $validation, $DEBUG = false) {
        //
        $this->textarea($champ, $validation, $DEBUG);
    }


    /**
     * La valeur du champ est passe par le controle hidden
     *
     * @param string $champ Nom du champ
     * @param integer $validation
     * @param boolean $DEBUG Parametre inutilise
     */
    function referencescadastralesstatic($champ, $validation, $DEBUG = false) {

        //
        if ($this->val[$champ] != '') {
            $this->link_geolocalisation($champ, $validation, $DEBUG);
        }
        // Affiche le bouton pour la récupération des propriétaires des
        // parcelles
        if ($this->val[$champ] != '') {
            //
            $this->plot_owner($champ, $validation, $DEBUG);
        }
        //
        foreach (explode(';', $this->val[$champ]) as $key => $ref) {

            echo "<span class=\"reference-cadastrale-".$key."\">";
            echo $ref;
            echo "</span>&nbsp";
            
        }
    }

    /**
     * La balise textarea et son libellé sont cachés grâce au CSS, cela permet
     * un traitement en javascript pour afficher/masquer le champ
     *
     * @param string $champ Nom du champ
     * @param integer $validation
     * @param boolean $DEBUG Parametre inutilise
     */
    function textareahidden($champ, $validation, $DEBUG = false) {
        $this->textarea($champ, $validation, $DEBUG);
    }
    
    /**
     * Affiche un textarea avec un bouton
     *
     * @param string $champ Nom du champ
     * @param integer $validation
     * @param boolean $DEBUG Parametre inutilise
     */
    function referentielpatrimoine($champ, $validation, $DEBUG = false){
        //
        echo "<textarea";
        echo " name=\"".$champ."\"";
        echo " id=\"".$champ."\" ";
        echo " cols=\"".$this->taille[$champ]."\"";
        echo " rows=\"".$this->max[$champ]."\"";
        if(!isset($this->select[$champ]['class'])) {
            $this->select[$champ]['class'] = "";
        }
        echo " class=\"champFormulaire noautoresize ".$this->select[$champ]['class']."\"";
        if (!$this->correct) {
            if (isset($this->onchange) and $this->onchange[$champ] != "") {
                echo " onchange=\"".$this->onchange[$champ]."\"";
            }
            if (isset($this->onkeyup) and $this->onkeyup[$champ] != "") {
                echo " onkeyup=\"".$this->onkeyup[$champ]."\"";
            }
            if (isset($this->onclick) and $this->onclick[$champ] != "") {
                echo " onclick=\"".$this->onclick[$champ]."\"";
            }
        } else {
            echo " disabled=\"disabled\"";
        }
        echo ">\n";
        echo $this->val[$champ];
        echo "</textarea>\n";
        
        //Ajout du bouton si le paramètre option_referentiel_patrimoine est à true
        if ($this->select[$champ]['option_referentiel_patrimoine']=='true'){
            
            echo "<input";
            echo " id=\"lien_referentiel_patrimoine\"";
            echo " class=\"champFormulaire ui-button ui-widget ui-state-default ui-corner-all \"";
            echo " type=\"button\"";
            echo " onclick=\"getReferencesPatrimoine(this);\"";
            echo " value=\"".__("Récupérer le(s) référence(s) patrimoine")."\"";
            echo " />\n";
        }
    }
    
    /**
     * La balise textarea et son libellé sont cachés grâce au CSS, cela permet
     * un traitement en javascript pour afficher/masquer le champ
     *
     * @param string $champ Nom du champ
     * @param integer $validation
     * @param boolean $DEBUG Parametre inutilise
     */
    function referentielpatrimoinehidden($champ, $validation, $DEBUG = false){
        $this->referentielpatrimoine($champ, $validation, $DEBUG);
    }
    
    /**
     * La balise textarea et son libellé sont cachés grâce au CSS, cela permet
     * un traitement en javascript pour afficher/masquer le champ
     *
     * @param string $champ Nom du champ
     * @param integer $validation
     * @param boolean $DEBUG Parametre inutilise
     */
    function referentielpatrimoinehiddenstatic($champ, $validation, $DEBUG = false){
        //
        echo "<input";
        echo " type=\"hidden\"";
        echo " id=\"".$champ."\"";
        echo " name=\"".$champ."\"";
        echo " value=\"".$this->val[$champ]."\"";
        echo " class=\"champFormulaire\"";
        echo " />\n";
        echo $this->val[$champ]."\n";
    }
    
    /**
     * La balise textarea et son libellé sont cachés grâce au CSS, cela permet
     * un traitement en javascript pour afficher/masquer le champ
     *
     * @param string $champ Nom du champ
     * @param integer $validation
     * @param boolean $DEBUG Parametre inutilise
     */
    function referentielpatrimoinestatic($champ, $validation, $DEBUG = false){
        //
        echo "<span id=\"".$champ."\" class=\"field_value\">";
        echo $this->val[$champ];
        echo "</span>";
    }

    /**
     * WIDGET_FORM - link.
     * 
     * Champ lien vers un enregistrement lié.
     *
     * Configuration du widget via setSelect : array(
     *     "obj" => "objet du formulaire lié",
     *     "idx" => (optionnel) "Identifiant de l'enregistrement lié (si aucune
     *              valeur n'est fournie c'est la valeur du champ dans le
     *              formulaire qui est utilisée)",
     *     "libelle" => (optionnel) "Libellé de la valeur du champ (si aucune
     *                  valeur n'est fourni c'est idx qui est utilisé)",
     *     "right" => (optionnel) "Permissions pour avoir accès au lien",
     * );
     *
     * @param string $champ Nom du champ
     * @param integer $validation
     * @param boolean $DEBUG Parametre inutilise
     */
    function link($champ, $validation, $DEBUG = false) {
        /**
         * Configuration
         */
        $obj = "";
        $idx = "";
        $libelle = "";
        $right = null;
        // Récupération de la configuration du setSelect
        if (is_array($this->select) === true
            && array_key_exists($champ, $this->select) === true
            && is_array($this->select[$champ]) === true) {
            //
            if (array_key_exists("obj", $this->select[$champ]) === true) {
                $obj = $this->select[$champ]["obj"];
            }
            if (array_key_exists("idx", $this->select[$champ]) === true) {
                $idx = $this->select[$champ]["idx"];
            }
            if (array_key_exists("libelle", $this->select[$champ]) === true) {
                $libelle = $this->select[$champ]["libelle"];
            }
            if (array_key_exists("right", $this->select[$champ]) === true) {
                $right = $this->select[$champ]["right"];
            }
        }
        // Aucun identifiant fourni dans le setSelect donc on ressaye de
        // récupérer la valeur du champ présente dans le formulaire
        if ($idx === ""
            && is_array($this->val) === true
            && array_key_exists($champ, $this->val) === true) {
            //
            $idx = $this->val[$champ];
        }
        // Aucun libellé fourni dans le setSelect donc on y affecte la
        // valeur de l'identifiant de l'élément lié
        if ($libelle === "") {
            $libelle = $idx;
        }
        /**
         * Affiche la value dans un champ hidden pour être postée par le form
         */
        $this->setType($champ, "hidden");
        $this->hidden($champ, $validation, $DEBUG);
        /**
         * Affichage soit du lien soit du libellé soit rien :
         * - le lien est affiché si tous les paramètres sont correctement
         *   configurés et que l'utilisateur a les permissions,
         * - sinon si un libellé est configuré c'est le libellé qui est affiché,
         * - sinon on affiche rien.
         */
        if ($idx !== ""
            && $obj !== ""
            && $libelle !== ""
            && ($right == null
                || $this->f->isAccredited($right) === true)) {
            //
            printf(
                '<a id="link_%1$s" class="lienFormulaire" href="%2$s">%3$s</a>',
                $champ,
                sprintf(
                    '%1$s&obj=%2$s&action=3&idx=%3$s',
                    OM_ROUTE_FORM,
                    $obj,
                    $idx
                ),
                $libelle
            );
        } elseif ($libelle !== "") {
            printf(
                '<span class="field_value">%1$s</span>',
                $libelle
            );
        }
    }

    /**
     * Regroupe toutes les informations d'une adresse.
     *
     * @param string  $champ          Nom du champ
     * @param integer $validation     Validation du formulaire
     * @param boolean $DEBUG          Paramètre inutilisé
     * @param boolean $arrondissement Adresse composée d'un arrondissement
     *
     * @return void
     */
    function adresse($champ, $validation, $DEBUG = false, $arrondissement = false) {

        // Si des valeurs sont envoyées
        if (!empty($this->select[$champ]["values"])) {

            // Ajoute "BP" devant la boite postale si celle-ci est renseignée
            if (!empty($this->select[$champ]["values"]["boite_postale"])) {
                $this->select[$champ]["values"]["boite_postale"] = __("BP")." ".$this->select[$champ]["values"]["boite_postale"];
            }

            // Ajoute "Cedex" devant le cedex si celui-ci est renseigné
            if (!empty($this->select[$champ]["values"]["cedex"])) {
                $this->select[$champ]["values"]["cedex"] = __("Cedex")." ".$this->select[$champ]["values"]["cedex"];
            }

            // Découpage de l'adresse par ligne
            $adr = array();
            // Ligne 1
            $adr[] = array(
                $this->select[$champ]["values"]["adresse_numero"],
                $this->select[$champ]["values"]["adresse_numero2"],
                $this->select[$champ]["values"]["adresse_voie"],
            );
            // Ligne 2
            $adr[] = array(
                $this->select[$champ]["values"]["adresse_complement"],
            );
            // Ligne 3
            $adr[] = array(
                $this->select[$champ]["values"]["lieu_dit"],
                $this->select[$champ]["values"]["boite_postale"],
            );
            // Ligne 4
            // Vérifie si l'adresse est composée d'un arrondissement
            $adresse_arrondissement = "";
            if ($arrondissement == true) {
                $adresse_arrondissement = $this->select[$champ]["values"]["adresse_arrondissement"];
            }
            $adr[] = array(
                $this->select[$champ]["values"]["adresse_cp"],
                $this->select[$champ]["values"]["adresse_ville"],
                $adresse_arrondissement,
                $this->select[$champ]["values"]["cedex"],
            );

            // Compteur de ligne
            $i = 1;
            // Pour chaque ligne
            foreach ($adr as $ligne) {

                // Concat des données
                $value = $this->f->concat_text($ligne, " ");
                // Si la ligne n'est pas vide
                if (!empty($value)) {

                    // Texte d'une ligne à afficher
                    $text = '<span id ="'.$champ.'_adresse_ligne'.$i.'" class="field_value">';
                    $text .= $value;
                    $text .= "</span>";

                    // Affichage de la ligne
                    printf("<br>".$text);

                    // Incrémente le compteur
                    $i++;
                }
            }
        }
    }

    /**
     * Champ spécifique à la table etablissement, permet en mode consulter 
     * d'afficher en valeur de champ, toute les informations utiles de 
     * l'établissement.
     *
     * @param string   $champ      Nom du champ
     * @param integer  $validation 
     * @param boolean  $DEBUG      Paramètre inutilisé
     */
    function etablissement($champ, $validation, $DEBUG = false) {

        // Affiche le lien de redirection vers le SIG si l'établissement lié est géolocalisé
        if (isset($this->select[$champ]["values"]["geolocalise"]) && $this->select[$champ]["values"]["geolocalise"] !== "f") {
            $this->link_geolocalisation($champ, $validation, $DEBUG);
        }
        // Affiche le libellé avec le type link
        $this->link($champ, $validation, $DEBUG);

        // Affiche l'adresse
        $this->adresse($champ, $validation, $DEBUG, true);

        // Si des valeurs sont envoyées
        if (!empty($this->select[$champ]["values"])) {
            //
            if($this->select[$champ]["values"]["etablissement_nature_libelle"]!=""){
                echo "<br/>";
                $this->champstatic("etablissement_nature_libelle", $this->select[$champ]["values"]["etablissement_nature_libelle"], $DEBUG);
            }
            //
            if ($this->select[$champ]["values"]["etablissement_type_libelle"]!=""||$this->select[$champ]["values"]["etablissement_categorie_libelle"]!=""){
                echo "<br/>";
                if ($this->select[$champ]["values"]["etablissement_type_libelle"]!=""){
                    $this->champstatic("etablissement_type_libelle", __("type")."&nbsp;".$this->select[$champ]["values"]["etablissement_type_libelle"], $DEBUG);
                }
                
                if ($this->select[$champ]["values"]["etablissement_categorie_libelle"]!=""){
                    echo "&nbsp;";
                    $this->champstatic("etablissement_categorie_libelle", __("categorie")."&nbsp;".$this->select[$champ]["values"]["etablissement_categorie_libelle"], $DEBUG);
                }
            }
            //
            if($this->select[$champ]["values"]["si_locaux_sommeil"]!=""){
                echo "<br/>";
                $this->champstatic("si_locaux_sommeil", $this->select[$champ]["values"]["si_locaux_sommeil"], $DEBUG);
            }
        }
    }

    /**
     * [contact_exploitant description]
     *
     * @param [type]  $champ      [description]
     * @param [type]  $validation [description]
     * @param boolean $DEBUG      [description]
     *
     * @return [type]  [description]
     */
    function contact_exploitant($champ, $validation, $DEBUG = false) {

        //
        if ($this->select[$champ]["values"] != "") {
            //
            $identite = array(
                $this->select[$champ]["values"]["civilite"],
                $this->select[$champ]["values"]["nom"],
                $this->select[$champ]["values"]["prenom"],
            );
            //
            $value = $this->f->concat_text($identite, " ");
            $text = '<span id ="'.$champ.'_identite" class="field_value">';
            $text .= $value;
            $text .= "</span>";
            //
            printf($text);
        }

        //
        $this->adresse($champ, $validation, $DEBUG);
    }

    /**
     * Affichage des données
     *
     * @param string   $champ      Nom du champ
     * @param integer  $validation 
     * @param boolean  $DEBUG      Paramètre inutilisé
     */
    function champstatic($champ, $value, $DEBUG = false){
        //
        printf('<span id="%s"/>%s</span>',
            $champ,
            $value
        );
    }
    
    /**
     * Affichage des données concernant le dossier de coordination
     *
     * @param string   $champ      Nom du champ
     * @param integer  $validation 
     * @param boolean  $DEBUG      Paramètre inutilisé
     */
    function dossier_coordination_data($champ, $validation, $DEBUG = false) {
        
        // Affiche les données
        $this->champstatic($champ, $this->select[$champ]["value"], $DEBUG);
    }
    
    /**
     * Affichage des données concernant l'établissement
     *
     * @param string   $champ      Nom du champ
     * @param integer  $validation 
     * @param boolean  $DEBUG      Paramètre inutilisé
     */
    function etablissement_data($champ, $validation, $DEBUG = false) {
        
        // Affiche les données
        $this->champstatic($champ, $this->select[$champ]["libelle"], $DEBUG);
    }
    
    /**
     * Champ spécifique à la table dossier_coordination, permet  
     * d'afficher un lien vers le dossier de coordination lié
     *
     * @param string   $champ      Nom du champ
     * @param integer  $validation 
     * @param boolean  $DEBUG      Paramètre inutilisé
     */
    function dossier_coordination_link($champ, $validation, $DEBUG = false) {
        
        // Affiche les données
        echo "<input";
        echo " type=\"hidden\"";
        echo " id=\"".$champ."\"";
        echo " name=\"".$champ."\"";
        echo " value=\"".$this->select[$champ]['idx']."\"";
        echo " class=\"champFormulaire\"";
        echo " />\n";
        // Affiche le lien de redirection vers le SIG si le dossier de coordination lié est géolocalisé
        if (isset($this->select[$champ]["geolocalise"]) && $this->select[$champ]["geolocalise"] != "f") {
            $this->link_geolocalisation($champ, $validation, $DEBUG);
        }
        // Affiche le libellé avec le type link
        $this->link($champ, $validation, $DEBUG);
    }

    /**
     * Choix de l'heure et de la minute dans des listes à choix.
     *
     * @param string  $champ      Nom du champ
     * @param integer $validation Validation du form
     * @param boolean $DEBUG      Parametre inutilise
     */
    function heure_minute($champ, $validation, $DEBUG = false) {
        //
        $text_onchange = "";
        if (isset($this->onchange) and $this->onchange[$champ] != "") {
            $text_onchange = $this->onchange[$champ];
        }

        // Découpe la valeur du champ
        $explode_value = explode(":", $this->val[$champ]);
        // Récupère la valeur du select des heures
        $value_heure = (isset($explode_value[0])) ? $explode_value[0] : "";
        // Récupère la valeur du select des minutes
        $value_minute = (isset($explode_value[1])) ? $explode_value[1] : "";

        // Initialise le select des heures
        $this->setVal($champ."_heure", $value_heure);
        $contenu = array();
        $contenu[0][0] = "";
        $contenu[1][0] = __('choisir');
        for ($i=0; $i < 24; $i++) {
            $j = str_pad($i, 2, "0", STR_PAD_LEFT);
            $contenu[0][$i+1] = $j;
            $contenu[1][$i+1] = $j;
        }
        $this->setSelect($champ."_heure", $contenu);

        // Initialise le onChange du select des heures
        $this->setOnchange($champ."_heure", "heure_minute('".$champ."', 'heure');".$text_onchange);
        // Affiche le champ select des heures
        $this->select($champ."_heure", $validation, $DEBUG);

        // Affiche ":" entre les deux select
        printf("<span> : </span>");

        // Initialise le select des minutes
        $this->setVal($champ."_minute", $value_minute);
        $contenu = array();
        $contenu[0][0] = "";
        $contenu[0][1] = "00";
        $contenu[0][2] = "15";
        $contenu[0][3] = "30";
        $contenu[0][4] = "45";
        $contenu[1][0] = __('choisir');
        $contenu[1][1] = "00";
        $contenu[1][2] = "15";
        $contenu[1][3] = "30";
        $contenu[1][4] = "45";
        $this->setSelect($champ."_minute", $contenu);
        // Initialise le onChange du select des minutes
        $this->setOnchange($champ."_minute", "heure_minute('".$champ."', 'minute');".$text_onchange);
        // Affiche le champ select des minutes
        $this->select($champ."_minute", $validation, $DEBUG);

        // Valeur mise à jour par javascript
        // Affiche la value dans un champ hidden pour être postée par le form
        $this->setType($champ, "hidden");
        $this->hidden($champ, $validation, $DEBUG);
    }

    /**
     * Création d'un input pour choisir une couleur
     *
     * @param string   $champ      Nom du champ
     * @param integer  $validation 
     * @param boolean  $DEBUG      Paramètre inutilisé
     */
    function color($champ, $validation, $DEBUG = false) {

        // Inclusion de JSColor
        $this->f->addHTMLHeadJs("../app/lib/jscolor/jscolor.js");
        // Affiche l'input
        echo "<input";
        echo " type=\"text\"";
        echo " id=\"".$champ."\"";
        echo " size=\"6\"";
        echo " maxlength=\"6\"";
        echo " name=\"".$champ."\"";
        echo " value=\"".$this->val[$champ]."\"";
        echo " class=\"champFormulaire color\"";
        echo " />\n";  
    }

    /**
     * Création d'un tableau pour choisir un ou plusieurs essais réalisés
     *
     * @param string   $champ      Nom du champ
     * @param integer  $validation 
     * @param boolean  $DEBUG      Paramètre inutilisé
     */
    function essai_realise($champ, $validation, $DEBUG = false) {
        // Récupération de tous les essais réalisés
        // avec des informations supplémentaires pour ceux liés à l'analyse
        $essais = $this->val[$champ];
        // Ouverture du tableau
        $tableau = sprintf('<table class="tab-tab">');
        // Entête de tableau
        $template_tab_header = '
            <thead><tr class="ui-tabs-nav ui-accordion ui-state-default tab-title">
                <th class="title col-0 marge_essai_droite">
                </th>
                <th class="title col-1 marge_essai_droite">
                </th>
                <th class="title col-2  center_th_essai_realise marge_essai_droite">
                    <span>%s</span>
                </th>
                <th class="title col-3 center_th_essai_realise">
                    <span>%s</span>
                </th>
                <th class="title col-4  center_th_essai_realise lastcol marge_essai_gauche">
                    <span>%s</span>
                </th>
            </tr></thead>';
        $tableau .= sprintf(
            $template_tab_header,
            __('essai'),
            __("concluant ?"),
            __('complement')
        );
        // Corps du tableau
        $tableau .= sprintf('<tbody>');
        $i = 0;
        // Affiche chacun des essais
        foreach ($essais as $essai => $valeurs) {
            $i++;
            ($i % 2 == 0) ? $odd = "" : $odd = "odd";
            // initialisation des attributs checked et disabled
            $disabled = '';
            $essai_checked = 'checked="checked"';
            $concluant_checked = 'checked="checked"';
            // récupération des attributs checked et disabled
            if ($valeurs["lien"] == 'f') {
                $essai_checked = "";
                $disabled = 'disabled="disabled"';
            }
            if ($valeurs["conc"] == 'f') {
                $concluant_checked = "";
            }
            // action JS à la (dé)sélection d'un essai réalisé
            $disable = ".removeAttr('disabled')";
            $enable = ".attr('disabled', 'disabled')";
            $select = sprintf("if (this.checked) {
                    this.value='t';
                    $('#conc_%s')%s;
                    $('#comp_%s')%s;
                } else {
                    this.value='f';
                    $('#conc_%s')%s;
                    $('#comp_%s')%s;
                }",
                $valeurs["id"],
                $disable,
                $valeurs["id"],
                $disable,
                $valeurs["id"],
                $enable,
                $valeurs["id"],
                $enable
            );
            // action JS lorsque l'on (dé)coche qu'un essai a été concluant
            $coche = "if (this.checked) this.value='t'; else this.value='f';";
            // ouverture de la ligne
            $tableau .= sprintf('<tr class="tab-data %s">',$odd);
            // checkbox de sélection
            $tableau .= sprintf('<td class="col-0 icons marge_essai_droite">
                <input id="essai_%s" type="checkbox" %s value="%s"
                    onclick="%s" name="essai[%s]" /></td>',
                $valeurs["id"],
                $essai_checked,
                $valeurs["lien"],
                $select,
                $valeurs["id"]
            );
            // Infobulle de l'essai s'il y a une description
            if ($valeurs["desc"] != '') {
                $tableau .= sprintf('<td class="col-1 icons marge_essai_droite">
                    <span title="%s" class="info-16"></span></td>',
                    $valeurs["desc"]
                );
            } // Sinon colonne vide
            else {
                $tableau .= sprintf('<td class="col-1 icons marge_essai_droite"></td>');
            }
            // Libellé de l'essai réalisé
            $tableau .= sprintf('<td class="col-2 marge_essai_droite">%s</td>',
                $valeurs['lib']
            );
            // Checkbox d'essai concluant ou non
            $tableau .= sprintf('<td class="col-3 icons">
                <input id="conc_%s" type="checkbox" %s value="%s"
                    onclick="%s" name="conc[%s]" /></td>',
                $valeurs["id"],
                $disabled.$concluant_checked,
                $valeurs["conc"],
                $coche,
                $valeurs["id"]
            );
            // Eventuel complément
            $tableau .= sprintf('<td class="col-4 lastcol marge_essai_gauche">
                <textarea id="comp_%s" %s col="75" name="comp[%s]">%s</textarea></td>',
                $valeurs["id"],
                $disabled,
                $valeurs["id"],
                $valeurs["comp"]
            );
            // fermeture de la ligne
            $tableau .= sprintf('</tr>');
        }
        // fermeture du tableau
        $tableau .= sprintf('</tbody></table>');
        if ($i == 0) {
            printf('%s<br/>%s',
                __("Aucun essai realise disponible."),
                __("Veuillez configurer le parametrage.")
            );
        } else {
            printf($tableau);
        }
    }

    /**
     * Création d'un tableau pour choisir une ou plusieurs prescriptions
     *
     * @param string   $champ      Nom du champ
     * @param integer  $validation 
     * @param boolean  $DEBUG      Paramètre inutilisé
     */
    function prescription($champ, $validation, $DEBUG = false) {
        // Récupération de toutes les prescriptions
        $essais = $this->val[$champ];
        // Création du bouton d'ajout d'une prescription
        $btn_add = sprintf('<span title="%s"
            class="boutons_prescription ui-icon ui-icon-plusthick"></span>',
            __("Ajouter une prescription")
        );
        // Ouverture du tableau
        printf('<table class="tab-tab">');
        // Entête de tableau
        $template_tab_header = '
            <thead><tr class="ui-tabs-nav ui-accordion ui-state-default tab-title">
                <th class="title col-0 marge_essai_droite">%s</th>
                <th class="title col-1 marge_essai_droite">
                    <span class="name">%s</span>
                </th>
                <th class="title col-2 lastcol">
                    <span class="name">%s</span>
                </th>
            </tr></thead>';
        printf(
            $template_tab_header,
            $btn_add,
            __('Prescription reglementaire'),
            __('Prescriptions specifiques')
        );
        // Corps du tableau
        printf('<tbody>');
        $i = 0;
        // Affiche chacun des essais
        foreach ($essais as $essai => $valeurs) {
            $i++;
            ($i % 2 == 0) ? $odd = "" : $odd = "odd";
            // action JS lors de la demande de suppression de la prescription
            $js_delete = sprintf("delete_prescription(this);");
            // action JS lors de la demande de modification de la prescription
            $js_edit = sprintf("",
                $valeurs["id"]
            );
            // action JS lors de la demande de déplacement vers le bas
            $js_bas = sprintf("",
                $valeurs["ord"]
            );
            // action JS lors de la demande de déplacement vers le haut
            $js_haut = sprintf("",
                $valeurs["ord"]
            );
            // ouverture de la ligne
            printf('<tr id="prescription_%s" class="tab-data %s une_prescription">',
                $valeurs["ord"],
                $odd
            );
            // boutons d'actions JS
            printf('<td class="col-0 icons marge_essai_droite">
                <span id="delete_prescription_%s" title="%s" onclick="%s"
                class="boutons_prescription ui-icon ui-icon-closethick"></span>
                <span title="%s" onclick="%s"
                class="boutons_prescription ui-icon ui-icon-pencil"></span>
                <span title="%s" onclick="%s"
                class="boutons_prescription ui-icon ui-icon-arrowthick-1-s"></span>
                <span title="%s" onclick="%s"
                class="boutons_prescription ui-icon ui-icon-arrowthick-1-n"></span>
                </td>',
                $valeurs["ord"],
                __("Supprimer cette prescription"),
                $js_delete,
                __("Modifier cette prescription"),
                $js_edit,
                __("Deplacer cette prescription vers le bas"),
                $js_bas,
                __("Deplacer cette prescription vers le haut"),
                $js_haut
            );
            // Description de la prescription réglementaire
            printf('<td class="col-1 marge_essai_droite">%s</td>',
                ($valeurs["pr_id"] == '') ? "-" : $valeurs["pr_desc"]
            );
            // Description de la prescription spécifique
            printf('<td class="col-2 lastcol">%s</td>',
                $valeurs['ps_desc']
            );
            // fermeture de la ligne
            printf('</tr>');
        }
        // fermeture du tableau
        printf('</tbody></table>');
    }

    /**
     * httpclick - lien http en formulaire - passage d argument sur une
     * application tierce
     *
     * @param string $champ Nom du champ
     * @param integer $validation
     * @param boolean $DEBUG Parametre inutilise
     */
    function httpclick($champ, $validation, $DEBUG = false) {

        //
        if (isset($this->select[$champ][0])) {
            $aff = $this->select[$champ][0];
        } else {
            $aff = $champ;
        }
        //
        echo "<a id='".$champ."' href='#' onclick=\"".$this->val[$champ]."; return false;\" class='insert' >";
        echo $aff;
        echo "</a>\n";

    }

    /**
     * Affiche une liste de courrier.
     *
     * @param string   $champ      Nom du champ
     * @param integer  $validation 
     * @param boolean $DEBUG      [description]
     *
     * @return void
     */
    function listecourrierstatic($champ, $validation, $DEBUG = false) {
        // Affiche les données
        echo "<div id=\"list_courrier_".$champ."\">";
        echo "<ul>";
        foreach($this->select[$champ] as $id => $infos) {
            echo "<li>".$infos["contact"]."</li>";
        }
        echo "</ul>";
        echo "</div>";
    }

    /**
     * Affiche une liste de courrier avec lien de téléchargement.
     *
     * @param string   $champ      Nom du champ
     * @param integer  $validation 
     * @param boolean $DEBUG      [description]
     *
     * @return void
     */
    function listecourrier($champ, $validation, $DEBUG = false) {
        if(empty($this->select[$champ])) {
            return;
        }
        if ($this->f->storage == null) {
            // Message d'erreur
            echo __("Le syteme de stockage n'est pas accessible. Erreur de ".
                   "parametrage. Contactez votre administrateur.");
            echo "</div>";
            // On sort de la méthode
            return -1;
        }
        // Affiche les données
        echo "<div id=\"list_courrier_".$champ."\" class=\"listecourrierFormulaire\">";
        echo "<ul>";
        foreach ($this->select[$champ] as $id => $infos) {
            //
            if ($infos["om_fichier_finalise_courrier"] != "") {
                echo "<li>";
                //
                $filename = $this->f->storage->getFilename($infos["om_fichier_finalise_courrier"]);
                //
                if ($filename != ""
                    && $filename != 'OP_FAILURE') {
                    //
                    echo "<span class=\"om-prev-icon reqmo-16\" title=\"".__("Enregistrer le fichier")."\">";
                    echo "<a href=\"".OM_ROUTE_FORM."&snippet=file&obj=".$infos["obj"]."&amp;champ=".$infos["champ"].
                            "&amp;id=".$id."\" target=\"_blank\" class=\"lienFormulaire\">";
                    //
                    echo $filename;
                    echo " - ".__("Destinataire")." : ";
                    echo $infos["contact"];
                    echo "</a>";
                    echo "</span>";
                } else {
                    //
                    echo __("Le fichier n'existe pas ou n'est pas accessible.");
                }
                echo "</li>";
            }
        }
        echo "</ul>";
        echo "</div>";
    }

    /**
     * Permet d'afficher la liste des valeurs d'un select_multiple avec les
     * valeurs passaient dans le formulaire.
     *
     * @param string  $champ      Nom du champ
     * @param integer $validation Validation du formulaire
     * @param boolean $DEBUG      Parametre inutilise
     *
     * @return void
     */
    function hidden_static_select_multiple($champ, $validation, $DEBUG = false) {
        //
        $this->select_multiple_static($champ, $validation, $DEBUG);
        //
        $this->setType($champ, "hidden");
        $this->hidden($champ, $validation, $DEBUG);
    }


    /**
     * Crée un input hidden d'un contact
     *
     * @param string $champ Nom du champ
     * @param integer $validation
     * @param boolean $DEBUG Parametre inutilise
     */
    function hidden_contact($champ, $validation, $DEBUG = false) {
        echo "<input";
        echo " type=\"hidden\"";
        echo " name=\"contact[]\"";
        echo " id=\"".$champ."\" ";
        echo " value=\"".$this->val[$champ]."\"";
        echo " />\n";
    }

    /**
     * La valeur du champ est passe par le controle hidden
     *
     * @param string $champ Nom du champ
     * @param integer $validation
     * @param boolean $DEBUG Parametre inutilise
     */
    function hiddenstatic($champ, $validation, $DEBUG = false) {

        //
        echo "<input";
        echo " type=\"hidden\"";
        echo " id=\"".$champ."\"";
        echo " name=\"".$champ."\"";
        echo " value=\"".$this->val[$champ]."\"";
        echo " class=\"champFormulaire\"";
        echo " />\n";
        echo "<span id=\"".$champ."_value\" class=\"field_value\">";
        echo $this->val[$champ]."\n";
        echo "</span>";

    }

    /**
     * WIDGET_FORM - select_multiple_static.
     *
     * Ce widget permet d'afficher une liste statique (html) des valeurs 
     * d'un champ. Cette liste de valeurs provient de la combinaison entre les 
     * valeurs et libellés disponibles dans le paramétrage select de ce champ
     * et entre les valeurs du champ représentées de manière linéaire. 
     * 
     * Deux contraintes sont présentes ici : 
     *  - $this->val[$champ] correspond aux valeurs sélectionnées. Le format 
     *    attendu ici dans la valeur du champ est une chaine de caractère 
     *    représentant la liste des valeurs sélectionnées séparées par des ; 
     *    (points virgules). 
     *    Exemple : $this->val[$champ] = string(5) "4;2;3";
     *  - $this->select[$champ] correspond aux libellés de toutes les valeurs 
     *    disponibles dans cette liste lors de la modification de l'élément.
     *    Exemple : $this->select[$champ] = array(2) {
     *         [0] => array(3) {
     *           [0] => string(1) "2"
     *           [1] => string(1) "3"
     *           [2] => string(1) "4"
     *         }
     *         [1] => array(3) {
     *           [0] => string(5) "Plans"
     *           [1] => string(7) "Visites"
     *           [2] => string(18) "Dossiers à enjeux"
     *         }
     *       }
     *
     * @param string $champ Nom du champ
     * @param integer $validation
     * @param boolean $DEBUG Parametre inutilise
     *
     * @return void
     */
    function select_multiple_static($champ, $validation, $DEBUG = false) {
        // Si aucune valeur n'est sélectionnée alors on affiche rien
        if ($this->val[$champ] == "") {
            return;
        }
        // On transforme la chaine de caractère en tableau grâce au
        // séparateur ;
        $selected_values = explode(";", $this->val[$champ]);
        // On affiche la liste
        echo "<ul id='".$champ."'>";
        // On boucle sur la liste de valeurs sélectionnées
        foreach ($selected_values as $value) {
            //
            echo "<li>";
            // On affiche le libellé correspondant à la valeur
            echo $this->select[$champ][1][array_search($value, $this->select[$champ][0])];
            //
            echo "</li>";
        }
        //
        echo "</ul>";
    }

    /**
     * WIDGET_FORM - file.
     *
     * Surcharge pour utiliser un champ différent de celui de base.
     *
     * @param string $champ Nom du champ
     * @param integer $validation
     * @param boolean $DEBUG Parametre inutilise
     *
     * @return void
     */
    function file($champ, $validation, $DEBUG = false) {
        // Récupération du paramétrage si renseigné
        $obj = (isset($this->select[$champ]['obj'])) ? $this->select[$champ]['obj'] : $this->getParameter("obj");
        $idx = (isset($this->select[$champ]['idx'])) ? $this->select[$champ]['idx'] : $this->getParameter("idx");
        $val = (isset($this->select[$champ]['val'])) ? $this->select[$champ]['val'] : $this->val[$champ];
        $field = (isset($this->select[$champ]['champ'])) ? $this->select[$champ]['champ'] : $champ;
        // Si le storage n'est pas configuré, alors on affiche un message
        // d'erreur clair pour l'utilisateur
        echo "<div id=\"".$champ."\">";
        if ($this->f->storage == null) {
            // Message d'erreur
            echo __("Le syteme de stockage n'est pas accessible. Erreur de ".
                   "parametrage. Contactez votre administrateur.");
            echo "</div>";
            // On sort de la méthode
            return -1;
        }
        //
        if ($val != "") {
            //
            $filename = $this->f->storage->getFilename($val);
            //
            if ($filename != ""
                && $filename != 'OP_FAILURE') {
                //
                echo $filename;
                //
                $link = OM_ROUTE_FORM."&snippet=voir&obj=".$obj."&amp;champ=".$field."&amp;id=".$idx;
                //
                echo "<span class=\"om-prev-icon reqmo-16\" title=\"".__("Enregistrer le fichier")."\">";
                echo "<a href=\"".OM_ROUTE_FORM."&snippet=file&obj=".$obj."&amp;champ=".$field.
                        "&amp;id=".$idx."\" target=\"_blank\">";
                echo __("Telecharger");
                echo "</a>";
                echo "</span>";
            } else {
                echo __("Le fichier n'existe pas ou n'est pas accessible.");
            }
        }
        echo "</div>";
    }

    /**
     * WIDGET_FORM - filehiddenstatic.
     *
     * Affichage du nom du fichier ou d'une erreur si le fichier est
     * inaccessible avec la valeur passée dans le formulaire.
     *
     * @param string  $champ      Nom du champ
     * @param integer $validation Validation du formulaire
     * @param boolean $DEBUG      Parametre inutilise
     *
     * @return void
     */
    function filehiddenstatic($champ, $validation, $DEBUG=false) {
        //
        $this->filestatic($champ, $validation, $DEBUG);
        //
        $this->setType($champ, "hidden");
        $this->hidden($champ, $validation, $DEBUG);
    }

    /**
     * WIDGET_FORM - filestatic.
     *
     * Surcharge pour utiliser un champ différent de celui de base.
     *
     * @param string $champ Nom du champ
     * @param integer $validation
     * @param boolean $DEBUG Parametre inutilise
     *
     * @return void
     */
    function filestatic($champ, $validation, $DEBUG = false) {
        // Récupération du paramétrage si renseigné
        $val = (isset($this->select[$champ]['val'])) ? $this->select[$champ]['val'] : $this->val[$champ];
        // Si le storage n'est pas configuré, alors on affiche un message
        // d'erreur clair pour l'utilisateur
        echo "<div id=\"".$champ."\">";
        if ($this->f->storage == null) {
            // Message d'erreur
            echo __("Le syteme de stockage n'est pas accessible. Erreur de ".
                   "parametrage. Contactez votre administrateur.");
            echo "</div>";
            // On sort de la méthode
            return -1;
        }
        if ($val != "") {
            $filename = $this->f->storage->getFilename($val);
            if ($filename != ""
                && $filename != "OP_FAILURE") {
                //
                echo $filename;
            } else {
                echo __("Le fichier n'existe pas ou n'est pas accessible.");
            }
        }
        echo "</div>";
    }

    /**
     *
     * Créée un lien de redirection vers l'objet dans le référentiel ADS.
     *
     * @param string $champ Nom du champ
     * @param integer $validation
     * @param boolean $DEBUG Parametre inutilise
     *
     * @return void
     */
    function link_dossier_autorisation_ads($champ, $validation, $DEBUG = false) {
        //
        $this->hiddenstatic($champ, $validation, $DEBUG);
        //
        if (isset($this->select[$champ]["link"])
            && isset($this->select[$champ]["force"])
            && $this->select[$champ]["force"] != "t"
            && $this->val[$champ] != "") {
            printf(
                '
<a id="%s_link_openads" target="blank" href="%s">
<span class="om-icon om-icon-16 om-icon-fix external-link-16" title="%s">
%s
</span>
</a>
',
                $champ,
                $this->select[$champ]["link"],
                __('Voir dans openADS'),
                __('Voir dans openADS')
            );
        }
    }

    /**
     *
     * Créée un lien de redirection vers l'objet dans le SIG.
     *
     * @param string $champ Nom du champ
     * @param integer $validation
     * @param boolean $DEBUG Parametre inutilise
     *
     * @return void
     */
    function link_geolocalisation($champ, $validation, $DEBUG = false) {
        //
        if ($this->f->is_option_sig_enabled() === false) {
            return;
        }
        // Si le lien est affiché dans un champ de références cadastrales ou le champ
        // geolocalise, on utilise l'objet et l'idx courant
        if ($champ === 'references_cadastrales' OR $champ === 'geolocalise') {
            $obj = $this->getParameter('obj');
            $idx = $this->getParameter('idx');
        }
        else {
            // Récupère l'objet du champ
            $obj = (isset($this->select[$champ]['obj'])) ? $this->select[$champ]['obj'] : '';
            $idx = (isset($this->select[$champ]['idx'])) ? $this->select[$champ]['idx'] : '';
            if ($idx == '') {
                $idx = (isset($this->val[$champ])) ? $this->val[$champ] : '';
            }
        }

        if ($obj != '' AND $idx != '') {
            echo "<a id=\"".$champ."_localiser_sig_externe\" target=\"blank\"".
                 " href=\"".OM_ROUTE_FORM."&obj=".$obj."&amp;action=30&amp;idx=".$idx;
            if ($champ === 'references_cadastrales') {
                echo "&amp;option=parcelles";
            }
            echo "\">";
            echo "<span class=\"om-icon om-icon-16 om-icon-fix sig-16\" title=\"Localiser\">".__('Localiser')."</span>";
            echo "</a>";
        }
    }


    /**
     * WIDGET_FORM - enjeu_ads
     *
     * Cette méthode permet d'ajouter un élément dans le widget de enjeu ADS.
     * Elle affiche un triangle jaune si le champ est a 1 't' ou 'Oui'.
     *
     * @param string  $champ      Nom du champ
     * @param integer $validation Validation
     * @param boolean $DEBUG      Parametre inutilise
     *
     * @return void
     */
    function enjeu_ads($champ, $validation, $DEBUG = false){
        if ($this->val[$champ] == 1 || $this->val[$champ] == "t"
            || $this->val[$champ] == "Oui") {
            $value = "Oui";
        } else {
            $value = "Non";
        }
        printf(
            '<span id="%s" class="om-icon om-icon-16 om-icon-fix enjeu-ads-16 field_value">%s</span>',
            $champ,
            $value
        );
    }


    /**
     * WIDGET_FORM - enjeu_erp
     *
     * Cette méthode permet d'ajouter un élément dans le widget de enjeu ERP.
     * Elle affiche un triangle rose si le champ est a 1 't' ou 'Oui'.
     *
     * @param string  $champ      Nom du champ
     * @param integer $validation Validation
     * @param boolean $DEBUG      Parametre inutilise
     *
     * @return void
     */
    function enjeu_erp($champ, $validation, $DEBUG = false){
        if ($this->val[$champ] == 1 || $this->val[$champ] == "t"
            || $this->val[$champ] == "Oui") {
            $value = "Oui";
        } else {
            $value = "Non";
        }
        printf(
            '<span id="%s" class="om-icon om-icon-16 om-icon-fix enjeu-erp-16 field_value">%s</span>',
            $champ,
            $value
        );
    }

    /**
     * WIDGET_FORM - autocomplete_link_selection
     *
     * Cette méthode permet d'ajouter un élément dans le widget de formulaire autocomplete.
     * Elle est à surcharger dans chaque application selon les besoins.
     *
     * @param string  $champ      Nom du champ
     * @param integer $validation Validation
     * @param boolean $DEBUG      Parametre inutilise
     *
     * @return void
     */
    function autocomplete_link_selection($champ, $validation, $DEBUG = false) {
        //
        if ($this->f->is_option_sig_enabled() === false
            OR isset($this->select[$champ]['obj']) === false
            OR ($this->select[$champ]['obj'] != 'etablissement_tous')) {
            return;
        }
        $obj = $this->select[$champ]['obj'];
        echo "<a id=\"autocomplete-".$obj."-link-selection\"  class=\"autocomplete autocomplete-empty ui-state-default ui-corner-all\"".
             " onclick=\"overlay_etablissements_proches_qualification()\">";
        echo "<span title=\"".__("Liste d'établissements proches")."\" class=\"om-icon om-icon-16 localiser-16\">";
        echo __("Liste d'établissements proches");
        echo "</span>";
        echo "</a>\n";
    }

    /**
     * WIDGET_FORM - button
     *
     * Cette méthode permet de créer un input type button
     * 
     * @param   string   $champ       Nom du champ
     * @param   integer  $validation  Validation
     * @param   boolean  $DEBUG       Parametre inutilise
     * @return  void
     */
    function button($champ, $validation, $DEBUG = false) {
        // PARAMS
        $name = '';
        $text_onchange="";
        $text_onkeyup="";
        $text_onclick="";
        if (isset($this->select[$champ]) === true
            && isset($this->select[$champ]['name']) === true) {
            $name = $this->select[$champ]['name'];
        }
        if (!$this->correct) {
            if (isset($this->onchange) and $this->onchange[$champ] != "") {
                $text_onchange=" onchange=\"".$this->onchange[$champ]."\"";
            }
            if (isset($this->onkeyup) and $this->onkeyup[$champ] != "") {
                $text_onkeyup= " onkeyup=\"".$this->onkeyup[$champ]."\"";
            }
            if (isset($this->onclick) and $this->onclick[$champ] != "") {
                $text_onclick= " onclick=\"".$this->onclick[$champ]."\"";
            }
        } 
        // Affichage bouton
        echo "<input";
        echo " type=\"button\"";
        echo " name=\"".$champ."\"";
        echo " id=\"".$champ."\" ";
        echo " value=\"".$name."\"";
        if ($text_onchange !== '') {
            echo $text_onchange;
        }
        if ( $text_onkeyup !== '') {
            echo $text_onkeyup;
        }
        if ($text_onclick !== '') {
            echo  $text_onclick;
        }
       //
        echo " />\n";
       //
    }

    /**
     * WIDGET_FORM - plot_owner
     *
     * Bouton de récupération des propriétaires de parcelles sur un formulaire
     * en consultation. Utilisable seulement dans le widget de formulaire
     * 'referencescadastralesstatic'.
     * L'objet, l'identifiant, l'action et le titre de l'overlay doivent être
     * passés par le setSelect de l'objet
     *
     * @param string  $champ      Nom du champ.
     * @param integer $validation Niveau de validation.
     * @param boolean $DEBUG      Parametre inutilisé.
     *
     * @return void
     */
    public function plot_owner($champ, $validation, $DEBUG = false) {

        // Vérifie l'option SIG
        if ($this->f->is_option_sig_enabled() === false) {
            return;
        }

        // Récupère les paramètres obligatoire passés par le setSelect
        if (isset($this->select[$champ]['obj']) === false) {
            return;
        }
        if (isset($this->select[$champ]['idx']) === false) {
            return;
        }
        if (isset($this->select[$champ]['action']) === false) {
            return;
        }
        if (isset($this->select[$champ]['title']) === false) {
            return;
        }
        $obj = $this->select[$champ]['obj'];
        $idx = $this->select[$champ]['idx'];
        $action = $this->select[$champ]['action'];
        $title = $this->select[$champ]['title'];

        // Construit l'url pour le traitement ajax
        $link = OM_ROUTE_FORM.'&obj='.$obj.'&action='.$action.'&idx='.$idx;

        // Message à afficher en cas d'erreur du traitement ajax
        $msg_error = __("Une erreur s'est produite lors de la récupération des propriétaires.") . ' ' . __("Veuillez contacter votre administrateur.");
        // Si le message d'erreur est définit dans le setSelect de l'objet
        // alors on affiche celui-ci
        if (isset($this->select[$champ]['msg_error']) === true) {
            //
            $msg_error = $this->select[$champ]['msg_error'];
        }
        //
        $msg_error = addslashes($msg_error);

        // Affichage du bouton
        if ($obj !== '' && $obj !== null
            && $idx !== '' && $idx !== null
            && $action !== '' && $action !== null
            && $title !== '' && $title !== null
            && $msg_error !== '' && $msg_error !== null) {
            //
            $text = __('Récupération des propriétaires des parcelles');
            //
            $template = '<a id="%1$s_get_plot_owner" href="javascript:overlay_load_form(\'%2$s\', \'%3$s\', \'%4$s\', \'%5$s\', \'%6$s\', \'%7$s\', \'%8$s\')">
                <span class="om-icon om-icon-16 om-icon-fix contact-16" title="%9$s">
                    %9$s
                </span>
            </a>';

            //
            printf(
                $template,
                $champ,
                $title,
                $link,
                $msg_error,
                'auto',
                'auto',
                'left top',
                false,
                $text
            );
        }
    }

    /**
     * WIDGET_FORM - datetimehiddenstatic.
     *
     * Affiche les champs au format datetime 'Y-m-d H:i:s' au format
     * 'd/m/Y H:i:s' en mode hiddenstatic.
     *
     * @param string $champ Nom du champ
     * @param integer $validation
     * @param boolean $DEBUG Parametre inutilise
     *
     * @return void
     */
    function datetimehiddenstatic($champ, $validation, $DEBUG = false) {
        //
        $datetime_aff = "";
        if ($this->val[$champ] != "") {
            if (DateTime::createFromFormat('Y-m-d H:i:s', $this->val[$champ]) !== false) {
                $dateFormat = new DateTime($this->val[$champ]);
                $datetime_aff = $dateFormat->format('d/m/Y H:i:s');
            }
        }
        //
        if (!$this->correct) {
            echo "<input type='hidden' ";
            echo "name='".$champ."' ";
            echo "id=\"".$champ."\" ";
            echo "value=\"".$this->val[$champ]."\" ";
            echo "class='champFormulaire' />\n";
        }
        echo $datetime_aff."";
    }

    /**
     *
     */
    protected function snippet__filterselect() {
        //
        $this->f->disableLog();
        // Données pour le champ visé
        (isset($_GET['idx']) ? $idx = $this->f->get_submitted_get_value("idx") : $idx = "");
        // Table visée pour la requête
        (isset($_GET['tableName']) ? $tableName = $this->f->get_submitted_get_value("tableName") : $tableName = "");
        // Champs visé pour le tri
        (isset($_GET['linkedField']) ? $linkedField = $this->f->get_submitted_get_value("linkedField") : $linkedField = "");
        // Formulaire visé
        (isset($_GET['formCible']) ? $formCible = $this->f->get_submitted_get_value("formCible") : $formCible = "");
        // Le formulaire visé doit être renseigné
        if ($formCible != '') {
            //
            $champ = array($linkedField);
            //
            $form = $this->f->get_inst__om_formulaire(array(
                "validation" => 0,
                "maj" => 0,
                "champs" => $champ,
            ));
            // Creation d'un objet vide pour pouvoir créer facilement les champs de
            // type select 
            $object = $this->f->get_inst__om_dbform(array(
                "obj" => $formCible,
                "idx" => "]",
            ));
            $object->setParameter($linkedField, $idx);
            $object->setSelect($form, 0, $this->f->db, false);
            //
            echo json_encode($form->select[$tableName]);
        }
    }

    /**
     * SNIPPET_FORM - upload.
     *
     * Ce script permet d'afficher un formulaire pour gérer l'upload de fichier
     * dans le répertoire de storage.
     *
     * @return void
     */
    protected function snippet__upload() {
        $f = $this->f;

        // Initialisation des paramètres
        $params = array(
            "origine" => array(
                "default_value" => "",
            ),
            "taille_max" => array(
                "default_value" => "",
            ),
            "extension" => array(
                "default_value" => "",
            ),
            "form" => array(
                "default_value" => "f1",
            ),
        );
        foreach ($this->f->get_initialized_parameters($params) as $key => $value) {
            ${$key} = $value;
        }

        /**
         * Verification des parametres
         */
        if ($origine == "") {
            //
            if ($f->isAjaxRequest() == false) {
                $f->setFlag(NULL);
                $f->display();
            }
            $class = "error";
            $message = __("L'objet est invalide.");
            $f->displayMessage($class, $message);
            die();
        }

        /**
         * Affichage de la structure HTML
         */
        if ($f->isAjaxRequest()) {
            header("Content-type: text/html; charset=".HTTPCHARSET."");
        } else {
            //
            $f->setFlag("htmlonly");
            $f->display();
        }
        //
        $f->displayStartContent();
        //
        $f->setTitle(__("Upload"));
        $f->displayTitle();
        //
        $description = __("Cliquer sur 'Parcourir' pour selectionner le fichier a ".
                         "telecharger depuis votre poste de travail puis cliquer sur ".
                         "le bouton 'Envoyer' pour valider votre telechargement.");
        $f->displayDescription($description);

        /**
         *
         */
        //
        (defined("PATH_OPENMAIRIE") ? "" : define("PATH_OPENMAIRIE", ""));
        require_once PATH_OPENMAIRIE."upload.class.php";
        //
        $Upload = new Upload($f);

        /**
         * Gestion des erreurs
         */
        //
        $error = false;
        // Verification du post vide
        if (isset($_POST['submited'])
            and (!isset($_FILES['userfile'])
                 or $_FILES['userfile']['name'][0] == "")) {
            //
            $error = true;
            $f->displayMessage("error", __("Vous devez selectionner un fichier."));
        }

        /**
         * Formulaire soumis et valide
         */
        if (isset($_POST['submited']) and $error == false) {

            // Gestion des extensions de fichier
            if ($origine !== "") {
                $tmp = $origine.'_extension';
            }
            if (isset(${$tmp})) {
                $Upload->Extension = ${$tmp};
            } else {
                if ($extension != ""
                    && isset($f->config['upload_extension'])) {
                    //
                    $Upload->Extension = $extension;

                    //Liste des extensions génériques possibles
                    $extensionPossibleGen = explode(';', $f->config['upload_extension']);
                    array_pop($extensionPossibleGen);
                    //Liste des extensions spécifiques possibles
                    $extensionPossibleSpe = explode(';', $extension);

                    foreach ($extensionPossibleSpe as $value) {

                        // Si une seule des extensions spécifiques n'est pas une des
                        // extensions génériques possibles, on utilise la configuration
                        // générique
                        if ( !in_array($value, $extensionPossibleGen)){
                            $Upload->Extension = $f->config['upload_extension'];
                            break;
                        }
                    }
                } elseif ($extension != "") {
                    $Upload->Extension = $extension;
                }elseif (isset($f->config['upload_extension'])) {
                    $Upload->Extension = $f->config['upload_extension'];
                } else {
                    $Upload->Extension = '.gif;.jpg;.jpeg;.png;.txt;.pdf;.csv';
                }
            }

            // On lance la procedure d'upload
            $Upload->Execute();

            // Gestion erreur / succes
            if ($UploadError) {
                $error = true;
                // (XXX - Le foreach est inutile on traite sur un seul champ fichier)
                foreach ($Upload->GetError() as $elem) {
                    foreach($elem as $key => $elem1) {
                        $f->displayMessage("error", $elem1);
                    }
                }
            } else {
                // (XXX - Le foreach est inutile on traite sur un seul champ fichier)
                foreach ($Upload->GetSummary() as $elem) {
                    $nom = $elem['nom'];
                    $filename = $elem['nom_originel'];
                    // Controle de la longueur du nom de fichier
                    if (strlen($filename) > 250) {
                        $error = true;
                        $f->displayMessage("error", $filename." ".__("contient trop de caracteres.")." ".__("Autorise(s) : 250 caractere(s)."));
                        continue;
                    }
                    //
                    if ($f->isAjaxRequest()) {
                        echo "<script type=\"text/javascript\">";
                        echo "upload_return('".$form."', '".$origine."', 'tmp|".$nom."', '".addslashes($filename)."')";
                        echo "</script>";
                    } else {
                        sprintf(
                            '
                    <script type="text/javascript">
                        parent.opener.document.%1$s.%2$s.value=\'tmp|%3$s\';
                        parent.opener.document.%1$s.%2$s_upload.value=\'%4$s\';
                        parent.close();
                    </script>
                            ',
                            $form,
                            $origine,
                            $nom,
                            $filename
                        );
                    }
                }
            }
        }

        /**
         * Formulaire non soumis ou non valide
         */
        if (!isset($_POST['submited']) or $error == true) {
            // Pour limiter la taille d'un fichier (exprimee en ko)
            if ($taille_max != ""
                && isset($f->config['upload_taille_max'])
                && $taille_max > $f->config['upload_taille_max']) {
                $Upload->MaxFilesize = $f->config['upload_taille_max'];
            } elseif ($taille_max != "") {
                $Upload->MaxFilesize = $taille_max * 1024 ;
            }elseif (isset($f->config['upload_taille_max'])) {
                $Upload->MaxFilesize = $f->config['upload_taille_max'];
            } else {
                $Upload->MaxFilesize = '10000';
            }

            // Pour ajouter des attributs aux champs de type file
            $Upload->FieldOptions = 'class="champFormulaire"';
            // Pour indiquer le nombre de champs desire
            $Upload->Fields = 2;
            // Initialisation du formulaire
            $Upload->InitForm();
            // Ouverture de la balise form
            //
            $this->f->layout->display__form_container__begin(array(
                "action" => "".OM_ROUTE_FORM."&snippet=upload&origine=".$origine.
                "&amp;form=".$form."&amp;taille_max=".$taille_max.
                "&amp;extension=".$extension,
                "name" => "upload-form",
                "id" => "upload-form",
                "enctype" => "multipart/form-data",
            ));
            // Affichage du champ MAX_FILE_SIZE
            print $Upload->Field[0];
            // Affichage du premier champ de type FILE
            print $Upload->Field[1];
            //
            echo "<br/>\n";
            echo "<br/>\n";
            //
            echo "<input type=\"hidden\" value=\"1\" name=\"submited\" />\n";
            $this->f->layout->display__form_input_submit(array(
                "name" => "submit",
                "value" => __("Envoyer"),
            ));
            //
            $f->displayLinkJsCloseWindow();
            // Fermeture de la balise form
            $this->f->layout->display__form_container__end();
        }

        /**
         * Affichage de la structure HTML
         */
        //
        $f->displayEndContent();
    }
}

