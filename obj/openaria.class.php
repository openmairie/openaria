<?php
/**
 * Ce script définit la classe 'utils'.
 *
 * @package openaria
 * @version SVN : $Id$
 */

/**
 *
 */
if (file_exists("../dyn/locales.inc.php") === true) {
    require_once "../dyn/locales.inc.php";
}

/**
 *
 */
if (file_exists("../dyn/include.inc.php") === true) {
    require_once "../dyn/include.inc.php";
} else {
    /**
     * Définition de la constante représentant le chemin d'accès au framework
     */
    define("PATH_OPENMAIRIE", getcwd()."/../core/");

    /**
     * TCPDF specific config
     */
    define('K_TCPDF_EXTERNAL_CONFIG', true);
    define('K_TCPDF_CALLS_IN_HTML', true);

    /**
     * Dépendances PHP du framework
     * On modifie la valeur de la directive de configuration include_path en
     * fonction pour y ajouter les chemins vers les librairies dont le framework
     * dépend.
     */
    set_include_path(
        get_include_path().PATH_SEPARATOR.implode(
            PATH_SEPARATOR,
            array(
                getcwd()."/../php/pear",
                getcwd()."/../php/db",
                getcwd()."/../php/fpdf",
                getcwd()."/../php/phpmailer",
                getcwd()."/../php/tcpdf",
            )
        )
    );
}

/**
 *
 */
if (file_exists("../dyn/debug.inc.php") === true) {
    require_once "../dyn/debug.inc.php";
}

/**
 *
 */
(defined("PATH_OPENMAIRIE") ? "" : define("PATH_OPENMAIRIE", ""));

/**
 *
 */
require_once PATH_OPENMAIRIE."om_application.class.php";

/**
 * Définition de la classe 'openaria' (om_application).
 *
 * Cette classe est destinée à permettre la surcharge de certaines méthodes de
 * la classe om_application pour des besoins spécifiques de l'application.
 */
class openaria extends application {

    /**
     * Gestion du nom de l'application.
     *
     * @var mixed Configuration niveau application.
     */
    protected $_application_name = "openARIA";

    /**
     * Titre HTML.
     *
     * @var mixed Configuration niveau application.
     */
    protected $html_head_title = ":: openMairie :: openARIA - Analyse du Risque Incendie et de l'Accessibilité pour les ERP";

    /**
     * Gestion du nom de la session.
     *
     * @var mixed Configuration niveau application.
     */
    protected $_session_name = "1bb484de79f96a6d0b00aaf4463c18f8bf";

    /**
     * Gestion du nombre de colonnes du tableau de bord.
     *
     * @var mixed Configuration niveau application.
     */
    protected $config__dashboard_nb_column = 2;

    /**
     * Gestion du mode de gestion des permissions.
     *
     * @var mixed Configuration niveau framework.
     */
    protected $config__permission_by_hierarchical_profile = false;

    /**
     *
     */
    function setDefaultValues() {
        $this->addHTMLHeadCss(
            array(
                "../app/lib/jquery-ui-theme/jquery-ui.custom.css",
                "../app/lib/chosen/chosen.min.css",
                "../app/css/om.css",
            ),
            21
        );
        $this->addHTMLHeadJs(
            array(
                "../app/lib/chosen/chosen.jquery.min.js",
            ),
            21
        );
    }

    /**
     * Instanciation de la classe 'edition'.
     *
     * @param array $args Arguments à passer au constructeur.
     * @return edition
     */
    function get_inst__om_edition($args = array()) {
        require_once "../obj/om_edition.class.php";
        return new app_om_edition();
    }

    // {{{
    
    /**
     * 
     */
    function isAccredited($obj = NULL, $operator = "AND") {
        // Les deux rubriques de menu sont affichées dans un écran spécifique
        // administration & paramétrage. Elles ne doivent pas apparaître dans
        // le menu
        if ($obj == "menu_administration"
            || $obj == "menu_parametrage") {
            return false;
        }
        // Fonctionnement standard
        return parent::isAccredited($obj, $operator);
    }

    /**
     * PARAM - 
     *
     * Le paramètre *etablissement_autorite_competente_periodique* n'est pas utilisable
     * en l'état, il est nécessaire de vérifier l'existence des codes fournis. Cet accesseur
     * permet de s'assurer que les valeurs récupérées existent ainsi que de s'assurer de
     * leur format.
     * 
     * La valeur de retour est un tableau :
     * - *codes* (array) : liste des codes paramétrés existants
     * - *ids* (array) : liste des identifiants paramétrés existants
     * - *sql_codes* (string) : 
     * - *query_where_codes* (string) : 
     *
     * @return array
     */
    function get_param__etablissement_autorite_competente_periodique() {
        //
        $ret = array(
            "codes" => array(),
            "ids" => array(),
            "sql_codes" => "",
            "query_where_codes" => "",
        );
        //
        $input_codes = $this->getParameter("etablissement_autorite_competente_periodique");
        //
        if ($input_codes === null
            || is_string($input_codes) !== true) {
            //
            return $ret;
        }
        //
        $autorite_competente_codes = explode(";", $input_codes);
        $autorite_competente_codes_sql = "";
        foreach ($autorite_competente_codes as $autorite_competente_code) {
            if (trim($autorite_competente_code) === "") {
                continue;
            }
            $ret["sql_codes"] .= sprintf(
                '\', \'%1$s',
                $this->db->escapesimple(trim($autorite_competente_code))
            );
        }
        $sql = sprintf(
            'SELECT
                autorite_competente.autorite_competente as id,
                autorite_competente.code as code
            FROM
                %1$sautorite_competente
            WHERE
                autorite_competente.code IN (\'%2$s\')
            ORDER BY
                autorite_competente.code',
            DB_PREFIXE,
            $ret["sql_codes"]
        );
        $res = $this->db->query($sql);    
        $this->addToLog(__METHOD__."() db->query(\"".$sql."\");", VERBOSE_MODE);
        $this->isDatabaseError($res);
        while ($row =& $res->fetchrow(DB_FETCHMODE_ASSOC)) {
            $ret["ids"][] = $row["id"];
            $ret["codes"][] = $row["code"];
        }
        if (count($ret["codes"]) > 0) {
            $ret["query_where_codes"] = sprintf(
                ' AND autorite_competente.code IN (\'%1$s\') ',
                $ret["sql_codes"]
            );
        }
        return $ret;
    }

    /**
     * Permet de calculer la liste des parcelles à partir de la chaîne passée en paramètre
     * et la retourner sous forme d'un tableau associatif
     * 
     * @param  string $strParcelles chaîne de la parcelles 
     * @return array (array(quartier, section, parcelle), ...)
     */
    function parseParcelles($strParcelles) {
        
        // Séparation des lignes
        $references = explode(";", $strParcelles);
        $liste_parcelles = array();
        
        // On boucle sur chaque ligne pour ajouter la liste des parcelles de chaque ligne
        foreach ($references as $parcelles) {
            
            // On transforme la chaîne de la ligne de parcelles en tableau
            $ref = str_split($parcelles);
            // Les 1er caractères sont numériques
            $num = true;
            
            // Tableau des champs de la ligne de références cadastrales
            $reference_tab = array();
            $temp = "";
            foreach ($ref as $carac) {
                
                // Permet de tester si le caractère courant est de même type que le précédent
                if(is_numeric($carac) === $num) {
                    $temp .= $carac;
                } else {
                    // Bascule
                    $num = !$num;
                    // On stock le champ
                    $reference_tab[] = $temp;
                    // re-init de la valeur temporaire pour le champ suivant
                    $temp = $carac;
                }
            }
            // Stockage du dernier champ sur lequel il n'y a pas eu de bascule
            $reference_tab[] = $temp;
            // Calcul des parcelles
            $quartier = $reference_tab[0];
            $sect = $reference_tab[1];

            $ancien_ref_parc = "";
            for ($i = 2; $i < count($reference_tab); $i += 2) {
                $parc["prefixe"] = $this->get_arrondissement_code_impot($quartier);
                $parc["quartier"] = $quartier;
                // Met en majuscule si besoin
                $parc["section"] = strtoupper($sect);
                if ($ancien_ref_parc == "" OR $reference_tab[$i - 1] == "/") {
                    // 1ere parcelle ou parcelle individuelle
                    // Compléte par des "0" le début de la chaîne si besoin
                    $parc["parcelle"] = str_pad($reference_tab[$i], 4, "0", STR_PAD_LEFT);
                    // Ajout d'une parcelle à la liste
                    $liste_parcelles[] = $parc;
                } elseif ($reference_tab[$i - 1] == "A") {
                    // Interval de parcelles
                    for ($j = $ancien_ref_parc + 1; $j <= $reference_tab[$i]; $j++) {
                        // Compléte par des "0" le début de la chaîne si besoin
                        $parc["parcelle"] = str_pad($j, 4, "0", STR_PAD_LEFT);
                        // Ajout d'une parcelle à la liste
                        $liste_parcelles[] = $parc;
                    }
                }
                //Gestion des erreurs
                else{
                    
                    echo "<div class=\"message ui-widget ui-corner-all ui-state-highlight ui-state-valid\">";
                    echo "<p><span class=\"ui-icon ui-icon-info\"></span>";
                    echo "<span class=\"text\">";
                    echo __("Un mauvais formatage de la reference cadastrale a ete detecte : ");
                    echo __("le separateur doit etre 'A' ou '/' et non '").$reference_tab[$i - 1]."'.";
                    echo "</span></p></div>";
                }
                // Sauvegarde de la référence courante de parcelle
                $ancien_ref_parc = $reference_tab[$i];
            }
        }

        return $liste_parcelles;
    }


    /**
     * Récupère le code impôt par rapport au quartier.
     * 
     * @param string $quartier Numéro de quartier.
     * @return string Code impôts.
     */
    protected function get_arrondissement_code_impot($quartier) {
        // Initialisation
        $code_impots = "";
        // Si le quartier fournis est correct
        if ($quartier != "") {
            // Requête SQL
            $sql = "SELECT
                        arrondissement.code_impots
                    FROM
                        ".DB_PREFIXE."arrondissement
                    LEFT JOIN
                        ".DB_PREFIXE."quartier
                        ON
                            quartier.arrondissement = arrondissement.arrondissement 
                    WHERE
                        quartier.code_impots = '".$quartier."'";

        }
        $code_impots = $this->db->getOne($sql);
        if ($code_impots === null) {
            $code_impots = "";
        }
        $this->isDatabaseError($code_impots); 
        // Retour
        return $code_impots;
    }


    /**
     * Vérification du n° de SIRET avec la méthode de Luhn.
     * 
     * @param string $val Numéro de SIRET
     * 
     * @return bool Vrai si valide
     */
    function checkLuhn($val) {

        $len = strlen($val);
        $total = 0;
        for ($i = 1; $i <= $len; $i++) {

            $chiffre = intval(substr($val, -$i, 1));
            if($i % 2 == 0) {
                
                $total += 2 * $chiffre;
                if((2 * $chiffre) >= 10) {
                    $total -= 9;
                }
            }
            else {

                $total += $chiffre;
            }
        }
        if ($total % 10 == 0) {

            return true;
        } else {

            return false;
        }
    }

    /**
     *
     */
    function is_user_with_role_and_service($role, $service) {
        //
        if (in_array($role, array("technicien", "cadre", "secretaire", )) !== true) {
            return false;
        }
        //
        if (in_array($service, array("si", "acc", )) !== true) {
            return false;
        }
        //
        $sql = "
        SELECT *
        FROM ".DB_PREFIXE."acteur 
            LEFT JOIN ".DB_PREFIXE."om_utilisateur
                ON  acteur.om_utilisateur=om_utilisateur.om_utilisateur
        WHERE om_utilisateur.login='".$_SESSION["login"]."'
        ";
        $res = $this->db->query($sql);
        $this->addToLog(__METHOD__."(): db->query(\"".$sql."\")", VERBOSE_MODE);
        if ($this->isDatabaseError($res, true) !== false) {
            return false;
        }
        //
        if ($res->numrows() === 0) {
            return false;
        }
        $row = $res->fetchRow(DB_FETCHMODE_ASSOC);
        if ($role != $row["role"]) {
            return false;
        }
        if ($service != $this->get_service_code($row["service"])) {
            return false;
        }
        return true;
    }


    /**
     * Teste si un acteur a un rôle donné
     * 
     * @param string $nom_prenom Le nom concaténé au prénom, de l'acteur
     * @param string $role Le rôle de l'acteur
     * 
     * @return mixed true si l'acteur a le rôle donné,
     *               false si l'acteur n'a pas le rôle donné
     *               -1 si une erreur s'est produite
     */
    function hasActeurRole($nom_prenom, $role) {
                
        //On vérifie les données passées en paramètre
        if (is_string($nom_prenom) && $nom_prenom !== "" &&
            is_string($role) && $role !== "") {
            
            //Création de la requête
            $sql = "SELECT role "
                    . "FROM ".DB_PREFIXE."acteur "
                    . "WHERE nom_prenom = '".$nom_prenom."'";
            //Exécution de la requête
            $roleActeurBdd = $this->db->getOne($sql);
            $this->addToLog("hasActeurRole(): db->getone(\"".$sql."\")", VERBOSE_MODE);
            $this->isDatabaseError($roleActeurBdd);
            
            if (!is_string($roleActeurBdd)||$roleActeurBdd===""){
                $this->addToLog("hasActeurRole(): ".__("Aucun rôle associé à cet acteur"), VERBOSE_MODE);
                return -1;
            }
            
            //On compare le rôle récupéré en base de données et le rôle passé en
            //paramètre de la fonction
            if (strcmp($role, $roleActeurBdd)===0){
                return true;
            }
            else {
                $this->addToLog("hasActeurRole(): ".
                    __("L'acteur n'a pas le rôle '").$role.__("' mais le rôle '").
                    $roleActeurBdd."'", VERBOSE_MODE);
                return false;
            }
        }
        $this->addToLog("hasActeurRole(): ".
            __("Le format des données fournies en entrées est incorrect"), VERBOSE_MODE);
        return -1;
    }

    /**
     * Récupère l'identifiant de l'acteur de l'utilisateur courant.
     *
     * @return integer Identifiant de l'acteur
     */
    function get_acteur_by_session_login() {
        //
        $acteur = "";

        //
        if (isset($_SESSION['login']) && !empty($_SESSION['login'])) {
            //
            $sql = "SELECT acteur
                    FROM ".DB_PREFIXE."acteur
                    LEFT JOIN ".DB_PREFIXE."om_utilisateur
                        ON acteur.om_utilisateur = om_utilisateur.om_utilisateur
                    WHERE login = '".$_SESSION['login']."'";
            $acteur = $this->db->getOne($sql);
            $this->addToLog(__METHOD__."(): db->getone(\"".$sql."\")", VERBOSE_MODE);
            $this->isDatabaseError($acteur);
        }

        //
        return $acteur;
    }

    /**
     * Permet de concaténer les cellules d'un tableau en un string 
     * en supprimant les vides.
     *
     * @param array  $texts Tableau à transformer en string
     * @param string $glue  Caractère de séparation des cellules
     *
     * @return string
     */
    function concat_text($texts = array(), $glue = "") {
        //
        $texts_without_blank = array();
        //
        foreach ($texts as $key => $value) {
            if (!empty($value)) {
                $texts_without_blank[$key] = $texts[$key];
            }
        }
        //
        return implode($glue, $texts_without_blank);
    }

    // {{{

    /**
     *
     */
    function triggerAfterLogin($utilisateur = NULL) {
        //
        parent::triggerAfterLogin($utilisateur);
        //
        $sql = "select *
        from ".DB_PREFIXE."om_utilisateur 
            left join ".DB_PREFIXE."acteur 
            on om_utilisateur.om_utilisateur=acteur.om_utilisateur
        where om_utilisateur.om_utilisateur=".$utilisateur["om_utilisateur"];
        $res = $this->db->query($sql);
        // Logger
        $this->addToLog("triggerAfterLogin(): db->query(\"".$sql."\");", VERBOSE_MODE);
        $this->isDatabaseError($res);
        if ($res->numRows() == 0) {
            $_SESSION["service"] = null;
        } else {
            $row =& $res->fetchRow(DB_FETCHMODE_ASSOC);
            if ($row["service"] != "") {
                $_SESSION["service"] = $row["service"];
            } else {
                $_SESSION["service"] = null;
            }
        }
        $res->free();
    }

    // }}}

    /**
     * Permet de transformer la valeur retournée de la bdd en booléen.
     *
     * @param string $value valeur du champ
     *
     * @return mixed booléen ou valeur du champ
     */
    function get_val_boolean($value) {
        //
        if ($value == 't') {
            return true;
        }
        //
        if ($value == 'f') {
            return false;
        }
        //
        return $value;
    }
    
    /**
     * Retourne le code du service
     * 
     * @param   integer $id_service L'identifiant du service
     * @return string Code du service
     */
    function get_service_code($id_service) {
        
        $code = "";
        //Si l'identifiant du service a été fourni
        if($id_service!=''){
            // Requête 
            $sql = "SELECT LOWER(code)
                FROM ".DB_PREFIXE."service
                WHERE service = ".intval($id_service);
            $this->addToLog("get_service_code() db->getOne(\"".$sql."\");",
                VERBOSE_MODE);
            $code = $this->db->getOne($sql);
            $this->isDatabaseError($code);
        }
        
        return $code;
    }

    /**
     * Récupère la valeur d'un champ d'une table par l'identifiant de 
     * l'enregistrement.
     *
     * @param mixed  $id    Identifiant de l'enregistrement
     * @param string $field Nom du champ
     * @param string $table Nom de la table
     *
     * @return mixed Valeur du champ
     */
    function get_field_from_table_by_id($id, $field, $table) {
        // Instance de l'objet
        $item = $this->get_inst__om_dbform(array(
            "obj" => $table,
            "idx" => $id,
        ));
        // Rretourne la valeur du champ
        return $item->getVal($field);
    }


    /**
     * Récupère l'identifiant de l'enregistrement par la valeur d'un de ses
     * champs.
     *
     * @param mixed  $value Valeur du champ de l'enregistrement.
     * @param string $field Nom du champ.
     * @param string $table Nom de la table.
     *
     * @return mixed Identifiant de l'enregistrement ou null.
     */
    public function get_id_from_table_by_field($value, $field, $table) {
        //
        if ($value === null || $value === ''
            || $field === null || $field === ''
            || $table === null || $table === '') {
            //
            return null;
        }

        // Requête SQL
        $sql = 'SELECT %1$s
                FROM '.DB_PREFIXE.'%1$s
                WHERE %2$s = \'%3$s\'';
        $sql = sprintf($sql, $table, $field, $value);
        // log
        $this->addToLog(__METHOD__."() db->query(\"".$sql."\");",
                VERBOSE_MODE);
        // Utilise 'query' car il pourrait y avoir plusieurs résultat
        $res = $this->db->query($sql);
        //
        $this->isDatabaseError($res);

        // S'il y a plusieurs résultats
        if ($res->numRows() > 1) {
            //
            return null;
        }

        //
        $row = &$res->fetchRow();
        return $row[0];
    }


    /**
     * Retourne l'identifiant du service.
     *
     * @param string $code_service code de service
     *
     * @return mixed false si erreur sinon identifiant du service
     */
    function get_service_id($code_service) {
        
        $id_service = "";
        //Si l'identifiant du service a été fourni
        if($code_service !=' '){
            // Requête 
            $sql = "SELECT service
                FROM ".DB_PREFIXE."service
                WHERE code = '".$code_service."'";
            $this->addToLog("get_service_id() db->getOne(\"".$sql."\");",
                VERBOSE_MODE);
            $id_service = $this->db->getOne($sql);
            $this->isDatabaseError($id_service);
        }
        
        return $id_service;
    }

    /**
     * Permet de générer un code barres.
     *
     * @param string  $prefix Préfix du code barres
     * @param integer $id     Identifiant de l'enregistrement
     *
     * @return string         Code barres
     */
    function generate_code_barres($prefix, $id) {
        // Met l'identifiant de l'enregistrement sur 10 caractères
        $result = str_pad($id, 10, "0", STR_PAD_LEFT);
        // Retourne la concaténation du préfix avec l'identifiant
        return $prefix.$result;
    }
    
    /**
     * Retourne la semaine qui suit la dernière existante
     * 
     * @return array tableau associatif de l'année et la semaine
     */
    function define_next_week($annee_max, $semaine_max) {
        
    	// Calcul de la prochaine semaine
        // si aucune semaine récupérée on ne continue pas le traitement
        if ($annee_max == '' || $semaine_max == '') {
        	return false;
        }
        // on définit une date au lundi de la semaine récupérée
        $date_recuperee = new DateTime();
        $date_recuperee->setISODate($annee_max,$semaine_max,1);
        // on l'incrémente d'une semaine
        $date_recuperee->add(new DateInterval('P7D'));
        // on récupère les valeurs
        $annee = $date_recuperee->format('o');
        $semaine = $date_recuperee->format('W');
        $prochaine_semaine = array(
        	"annee" => $annee,
        	"semaine" => $semaine,
        );
        return $prochaine_semaine;
    }

    /**
     * Retourne la semaine précédente la semaine fournit en paramètre dans le calendrier.
     * 
     * @param integer $annee 
     * @param integer $semaine 
     *
     * @return array Tableau associatif de l'année et la semaine
     */
    function define_prev_week($annee, $semaine) {
        // si aucune semaine récupérée on ne continue pas le traitement
        if ($annee == '' || $semaine == '') {
            return false;
        }
        // on définit une date au lundi de la semaine récupérée
        $date_recuperee = new DateTime();
        $date_recuperee->setISODate($annee, $semaine, 1);
        // on la décrémente d'une semaine
        $date_recuperee->sub(new DateInterval('P7D'));
        // on récupère les valeurs
        $annee = $date_recuperee->format('o');
        $semaine = $date_recuperee->format('W');
        $precedente_semaine = array(
            "annee" => $annee,
            "semaine" => $semaine,
        );
        return $precedente_semaine;
    }
    
    /**
     * Vérification des paramètres
     */
    function checkParams() {
        //
        parent::checkParams();
        // Dossier des numérisations
        (isset($this->config['digitalization_folder']) ? "" : $this->config['digitalization_folder'] = '../var/digitalization/');
    }

    /**
     * Calcule une date par l'ajout ou la soustraction de mois
     * @param date    $date     Date de base
     * @param integer $delay    Délais à ajouter (en mois)
     * @param string  $operator Opérateur pour le calcul ("-" ou "+")
     * 
     * @return date             Date calculée
     */
    function mois_date($date, $delay, $operator = "+") {
            
        // Si aucune date n'a été fournie ou si ce n'est pas une date correctement formatée
        if ( is_null($date) || $date == "" ||
            preg_match('/[0-9]{4}-[0-9]{2}-[0-9]{2}/', $date) == 0 ){
            return null;
        }

        // Découpage de la date
        $temp = explode("-", $date);
        $day = (int) $temp[2];
        $month = (int) $temp[1];
        $year = (int) $temp[0];

        // Si c'est une addition
        if ($operator == '+') {
            // Année à ajouter
            $year += floor($delay / 12);
            // Mois restant
            $nb_month = ($delay % 12);
            // S'il y a des mois restant
            if ($nb_month != 0) {
                // Ajout des mois restant
                $month += $nb_month;
                // Si ça dépasse le mois 12 (décembre)
                if ($month > 12) {
                    // Soustrait 12 au mois
                    $month -= 12;
                    // Ajoute 1 à l'année
                    $year += 1;
                }
            }
        }

        // Si c'est une soustraction
        if ($operator == "-") {
            // Année à soustraire
            $year -= floor($delay / 12);
            // Mois restant
            $nb_month = ($delay % 12);
            // S'il y a des mois restant
            if ($nb_month != 0) {
                // Soustrait le délais
                $month -= $nb_month;
                // Si ça dépasse le mois 1 (janvier)
                if ($month < 1) {
                    // Soustrait 12 au mois
                    $month += 12;
                    // Ajoute 1 à l'année
                    $year -= 1;
                }
            }
        }

        // Calcul du nombre de jours dans le mois sélectionné
        switch($month) {
            // Mois de février
            case "2":
                if ($year % 4 == 0 && $year % 100 != 0 || $year % 400 == 0) {
                    $day_max = 29;
                } else {
                    $day_max = 28;
                }
            break;
            // Mois d'avril, juin, septembre et novembre
            case "4":
            case "6":
            case "9":
            case "11":
                $day_max = 30;
            break;
            // Mois de janvier, mars, mai, juillet, août, octobre et décembre 
            default:
                $day_max = 31;
        }

        // Si le jour est supérieur au jour maximum du mois
        if ($day > $day_max) {
            // Le jour devient le jour maximum
            $day = $day_max;
        }

        // Compléte le mois et le jour par un 0 à gauche si c'est un chiffre
        $month = str_pad($month, 2, "0", STR_PAD_LEFT);
        $day = str_pad($day, 2, "0", STR_PAD_LEFT);

        // Retourne la date calculée
        return $year."-".$month."-".$day ;
    }

    /**
     * Vérifie la valididité d'une date.
     * 
     * @param string $pDate Date à vérifier
     * 
     * @return boolean
     */
    function check_date($pDate) {

        // Vérifie si c'est une date valide
        if (preg_match("/^([0-9]{4})-([0-9]{2})-([0-9]{2})$/", $pDate, $date) 
            && checkdate($date[2], $date[3], $date[1]) 
            && $date[1] >= 1900) {
            //
            return true;
        }

        //
        return false;
    }

    /**
     * Récupère le type du courrier depuis le modèle d'édition.
     *
     * @param integer $modele_edition_id Identifiant du modèle d'édition
     *
     * @return integer
     */
    function get_courrier_type_by_modele_edition($modele_edition_id) {
        // Initialisation de la variable de résultat
        $courrier_type = "";
        //
        if (!empty($modele_edition_id)) {
            // Instance de la classe modele_edition
            $modele_edition = $this->get_inst__om_dbform(array(
                "obj" => "modele_edition",
                "idx" => $modele_edition_id,
            ));

            // Récupère courrier_type
            $courrier_type = $modele_edition->getVal('courrier_type');
        }

        // Retourne le résultat
        return $courrier_type;
    }

    /**
     * Permet d'obtenir le code/libellé d'un select
     * 
     * @param  [string]   $class  classe principale
     * @param  [string]   $field  nom de la clé étrangère
     * @param  [integer]  $value  valeur de la clé étrangère
     * @return [string]           code ou libellé de la clé étrangère
     */
    function get_label_of_foreign_key($class, $field, $value = "") {
        // retour chaîne vide si pas de valeur passée en paramètre
        if ($value == "") {
            return "";
        }
        // 
        $inst = $this->get_inst__om_dbform(array(
            "obj" => $class,
        ));
        if ($inst === null) {
            return "";
        }
        //
        $sql = $inst->get_var_sql_forminc__sql($field."_by_id");
        if ($sql == "") {
            return "";
        }
        // remplacement de l'id par sa valeur dans la condition
        $sql = str_replace('<idx>', $value, $sql);
        // exécution requete
        $res = $this->db->query($sql);
        $this->addToLog(__METHOD__."(): db->query(\"".$sql."\");", VERBOSE_MODE);
        // Si la récupération du code/libellé  échoue
        if ($this->isDatabaseError($res, true)) {
            $this->addToLog(__METHOD__."(): ERROR");
            return "";
        }
        $row = &$res->fetchRow();
        // retour de la deuxième colonne sélectionnée
        // (considérée comme le code/libellé)
        return $row[1];
    }

    function view_main() {
        if ($this->get_submitted_get_value("module") === "pilotage") {
            $this->view_module_pilotage();
            return;
        }
        if ($this->get_submitted_get_value("module") === "settings") {
            $this->view_module_settings();
            return;
        }
        parent::view_main();
    }

    /**
     *
     */
    function view_module_pilotage() {
        //
        require_once "../obj/pilotage.class.php";
        $p = new pilotage();
        $p->view_main();
    }

    /**
     *
     */
    function view_module_settings() {
        //
        require_once "../obj/settings.class.php";
        $settings = new settings();
        $settings->view_main();
    }

    // {{{ BEGIN - SWROD
    // 

    /**
     *
     */
    var $inst_swrod = null;

    /**
     *
     */
    function is_swrod_enabled() {
        //
        if ($this->getParameter("swrod") === "true") {
            return true;
        }
        //
        return false;
    }

    /**
     *
     */
    function is_swrod_available() {
        //
        $inst_swrod = $this->get_inst_swrod();
        //
        if ($inst_swrod !== false
            && isset($inst_swrod->swrod)
            && $inst_swrod->swrod != null) {
            return true;
        }
        //
        return false;
    }

    /**
     *
     */
    function get_inst_pilotage() {
        if (isset($this->_inst_pilotage) === true) {
            return $this->_inst_pilotage;
        }
        require_once "../obj/pilotage.class.php";
        $this->_inst_pilotage = new pilotage();
        return $this->_inst_pilotage;
    }

    /**
     *
     */
    function get_inst_swrod() {
        //
        if ($this->inst_swrod !== null) {
            return $this->inst_swrod;
        }
        //
        if (!file_exists("../dyn/swrod.inc.php")) {
            $this->inst_swrod = false;
            return $this->inst_swrod;
        }
        require "../dyn/swrod.inc.php";
        if (!isset($swrod)) {
            $this->inst_swrod = false;
            return $this->inst_swrod;
        }
        //
        require_once "../obj/swrod.class.php";
        $this->inst_swrod = new swrod($swrod);
        return $this->inst_swrod;
    }

    // }}} END - SWROD

    /**
     * Indique si l'option contrainte est activée ou non.
     *
     * @return boolean
     */
    public function is_option_contrainte_enabled() {
        if ($this->getParameter('option_contrainte') === "false") {
            return false;
        }
        return true;
    }

    /**
     * Indique si l'option unité d'accessibilité est activée ou non.
     *
     * @return boolean
     */
    public function is_option_unite_accessibilite_enabled() {
        if ($this->getParameter('option_unite_accessibilite') === "false") {
            return false;
        }
        return true;
    }

    /**
     * Vérifie que l'état de l'option du SIG
     *
     * @return boolean true si activée
     */
    public function is_option_sig_enabled() {
        //
        $option_sig = $this->getParameter('option_sig');
        if ($option_sig === 'sig_externe') {
            //
            return true;
        }
        //
        return false;
    }

    /**
     * Vérifie que l'état de l'option du SIG interne
     *
     * @return boolean true si activée
     */
    public function is_option_sig_interne_enabled() {
        if ($this->getParameter("option_localisation") !== "sig_interne") {
            return false;
        }
        return true;
    }

    /**
     * Instance de l'abstracteur de géolocalisation.
     *
     * @var object
     */
    var $inst_geoaria = null;

    /**
     * Retourne une instance du connecteur geoaria
     * et la crée si elle n'existait pas.
     *
     * @return inst_geoaria Instance du connecteur
     */
    public function get_inst_geoaria() {
        //
        if (isset($this->inst_geoaria)) {
            return $this->inst_geoaria;
        }
        // Instanciation de l'abstracteur geoaria
        // On récupère les informations de la collectivité de l'utilisateur
        $param_coll = $this->getCollectivite();
        require_once "../obj/geoaria.class.php";
        $this->inst_geoaria = new geoaria($param_coll);
        //
        return $this->inst_geoaria;
    }

    /**
     * SURCHARGE DE LA CLASSE OM_APPLICATION.
     *
     * @see Documentation sur la méthode parent 'om_application:getCollectivite'.
     */
    function getCollectivite($om_collectivite_idx = null) {
        // On vérifie si une valeur a été passée en paramètre ou non.
        if ($om_collectivite_idx === null) {
            // Cas d'utilisation n°1 : nous sommes dans le cas où on 
            // veut récupérer les informations de la collectivité de
            // l'utilisateur et on stocke l'info dans un flag.
            $is_get_collectivite_from_user = true;
            // On initialise l'identifiant de la collectivité
            // à partir de la variable de session de l'utilisateur.
            $om_collectivite_idx = $_SESSION['collectivite'];
        } else {
            // Cas d'utilisation n°2 : nous sommes dans le cas où on
            // veut récupérer les informations de la collectivité 
            // passée en paramètre et on stocke l'info dans le flag.
            $is_get_collectivite_from_user = false;
        }
        //
        $collectivite_parameters = parent::getCollectivite($om_collectivite_idx);

        //// BEGIN - SURCHARGE OPENADS

        // Ajout du paramétrage du sig pour la collectivité
        if (file_exists("../dyn/sig.inc.php")) {
            include "../dyn/sig.inc.php";
        }
        $sig_externe = $this->get_database_extra_parameters('sig');
        if (!isset($sig_externe)) {
            $sig_externe = "sig-default";
        }

        if (isset($collectivite_parameters['om_collectivite_idx'])
            && isset($conf[$sig_externe][$collectivite_parameters['om_collectivite_idx']])
            && isset($collectivite_parameters["option_sig"]) 
            && $collectivite_parameters["option_sig"] == "sig_externe"
            ) {

            // Cas numéro 1 : conf sig définie sur la collectivité et option sig active
            $collectivite_parameters["sig"] = $conf[$sig_externe][$collectivite_parameters['om_collectivite_idx']];
        }
        //// END - SURCHARGE OPENADS

        // Si on se trouve dans le cas d'utilisation n°1
        if ($is_get_collectivite_from_user === true) {
            // Alors on stocke dans l'attribut collectivite le tableau de 
            // paramètres pour utilisation depuis la méthode 'getParameter'.
            $this->collectivite = $collectivite_parameters;
        }
        // On retourne le tableau de paramètres.
        return $collectivite_parameters;
    }


    /**
     * Retourne le caractère 's' si nécessaire
     *
     * @param  integer $nombre
     * @return string
     */
    public function pluriel($nombre) {
        // Spécifique au français
        if (LOCALE !== 'fr_FR') {
            return '';
        }
        if (intval($nombre) > 1) {
            return 's';
        }
        return '';
    }

    /**
     * Cette fonction prend en entrée le ou les paramètres du &contrainte qui sont entre
     * parenthèses (un ensemble de paramètres séparés par des points-virgules). Elle
     * sépare les paramètres et leurs valeurs puis construit et retourne un tableau 
     * associatif qui contient pour les groupes et sous-groupes :
     * - un tableau de valeurs, avec un nom de groupe ou sous-groupe par ligne
     * pour les autres options :
     * - la valeur de l'option
     * 
     * @param  string  $contraintes_param  Chaîne contenant tous les paramètres
     * 
     * @return array   Tableau associatif avec paramètres et valeurs séparés
     */
    public function explode_condition_contrainte($contraintes_param) {

        // Initialisation des variables
        $return = array();
        $listGroupes = "";
        $listSousgroupes = "";
        $affichage_sans_arborescence = "";

        // Sépare toutes les conditions avec leurs valeurs et les met dans un tableau
        $contraintes_params = explode(";", $contraintes_param);
        
        // Pour chaque paramètre de &contraintes
        foreach ($contraintes_params as $value) {
            // Récupère le mot-clé "liste_groupe" et les valeurs du paramètre
            if (strstr($value, "liste_groupe=")) { 
                // On enlève le mots-clé "liste_groupe=", on garde les valeurs
                $listGroupes = str_replace("liste_groupe=", "", $value);
            }
            // Récupère le mot-clé "liste_ssgroupe" et les valeurs du paramètre
            if (strstr($value, "liste_ssgroupe=")) { 
                // On enlève le mots-clé "liste_ssgroupe=", on garde les valeurs
                $listSousgroupes = str_replace("liste_ssgroupe=", "", $value);
            }
            // Récupère le mot-clé "affichage_sans_arborescence" et la valeur du
            // paramètre
            if (strstr($value, "affichage_sans_arborescence=")) { 
                // On enlève le mots-clé "affichage_sans_arborescence=", on garde la valeur
                $affichage_sans_arborescence = str_replace("affichage_sans_arborescence=", "", $value);
            }
        }

        // Récupère dans des tableaux la liste des groupes et sous-groupes qui  
        // doivent être utilisés lors du traitement de la condition
        if ($listGroupes != "") {
            $listGroupes = array_map('trim', explode(",", $listGroupes));
        }
        if ($listSousgroupes != "") {
            $listSousgroupes = array_map('trim', explode(",", $listSousgroupes));
        }

        // Tableau à retourner
        $return['groupes'] = $listGroupes;
        $return['sousgroupes'] = $listSousgroupes;
        $return['affichage_sans_arborescence'] = $affichage_sans_arborescence;
        return $return;
    }

    /**
     * Méthode qui complète la clause WHERE de la requête SQL de récupération des
     * contraintes, selon les paramètres fournis. Elle permet d'ajouter une condition sur
     * les groupes, sous-groupes et les services consultés.
     * 
     * @param  string  $part  Contient tous les paramètres fournis à &contraintes séparés
     *                 par des points-virgules, tel que définis dans l'état.
     * @param  array   $conditions  Paramètre optionnel, contient les conditions déjà
     *                 explosées par la fonction explode_condition_contrainte()
     * 
     * @return string  Contient les clauses WHERE à ajouter à la requête SQL principale.
     */
    public function treatment_condition_contrainte($part, $conditions = NULL) {

        // Initialisation de la condition
        $whereContraintes = "";
        // Lorsqu'on a déjà les conditions explosées dans le paramètre $conditions, on
        // utilise ces données. Sinon, on appelle la méthode qui explose la chaîne de 
        // caractères contenant l'ensemble des paramètres.
        if (is_array($conditions) === false){
            $conditions = $this->explode_condition_contrainte($part);
        }
        // Récupère les groupes et sous-groupes pour la condition
        $groupes = $conditions['groupes'];
        $sousgroupes = $conditions['sousgroupes'];

        // Pour chaque groupe
        if ($groupes != "") {
            foreach ($groupes as $key => $groupe) {
                // Si le groupe n'est pas vide
                if (!empty($groupe)) {
                    // Choisit l'opérateur logique
                    $op_logique = $key > 0 ? 'OR' : 'AND (';
                    // Ajoute la condition
                    $whereContraintes .= " ".$op_logique." lower(trim(both E'\n\r\t' from contrainte.groupe)) = lower('"
                        .pg_escape_string($groupe)."')";
                }
            }
            // S'il y a des valeurs dans groupe
            if (count($groupes) > 0) {
                // Ferme la parenthèse
                $whereContraintes .= " ) ";
            }
        }

        // Pour chaque sous-groupe
        if ($sousgroupes != "") {
            foreach ($sousgroupes as $key => $sousgroupe) {
                // Si le sous-groupe n'est pas vide
                if (!empty($sousgroupe)) {
                    // Choisit l'opérateur logique
                    $op_logique = $key > 0 ? 'OR' : 'AND (';
                    // Ajoute la condition
                    $whereContraintes .= " ".$op_logique." lower(trim(both E'\n\r\t' from contrainte.sousgroupe)) = lower('"
                        .pg_escape_string($sousgroupe)."')";
                }
            }
            // S'il y a des valeurs dans sous-groupe
            if (count($sousgroupes) > 0) {
                // Ferme la parenthèse
                $whereContraintes .= " ) ";
            }
        }

        // Condition retournée
        return $whereContraintes;
    }

    /**
     * [replace_contrainte description]
     * @param  [type] $objet     [description]
     * @param  [type] $objet_idx [description]
     * @param  [type] &$titre    [description]
     * @param  [type] &$corps    [description]
     * @return [type]            [description]
     */
    public function replace_contrainte($objet, $objet_idx, &$titre, &$corps) {

        switch ($objet) {
            case 'dc':
                $inst = $this->get_inst__om_dbform(array(
                    "obj" => "dossier_coordination",
                    "idx" => $objet_idx,
                ));
                break;
            case 'etab':
                $inst = $this->get_inst__om_dbform(array(
                    "obj" => "etablissement",
                    "idx" => $objet_idx,
                ));
                break;
            default:
                return;
        }

        // Cas 1.1 : contraintes avec paramètres dans le titre
        preg_match_all("/&contraintes_".$objet."\((.*)\)/", $titre, $matches_contraintes_params);
        //
        foreach ($matches_contraintes_params[0] as $key => $value) {
            //
            $contraintes_params[0] = $value;
            $contraintes_params[1] = $matches_contraintes_params[1][$key];
            //
            $titre = str_ireplace(
                $contraintes_params[0],
                $inst->construct_contraintes($contraintes_params),
                $titre
            );
        }

        // Cas 1.2 : contraintes sans paramètre dans le titre
        preg_match_all("/&contraintes_".$objet."(?!\(.*\))/", $titre, $matches_contraintes_titre);
        //
        foreach ($matches_contraintes_titre[0] as $key => $value) {
            //
            $titre = str_ireplace(
                $value,
                $inst->construct_contraintes(),
                $titre
            );
        }

        // Cas 2.1 : contraintes avec paramètres dans le corps
        preg_match_all("/&contraintes_".$objet."\((.*)\)/", $corps, $matches_contraintes_params);
        //
        foreach ($matches_contraintes_params[0] as $key => $value) {
            //
            $contraintes_params[0] = $value;
            $contraintes_params[1] = $matches_contraintes_params[1][$key];
            //
            $corps = str_ireplace(
                $contraintes_params[0],
                $inst->construct_contraintes($contraintes_params),
                $corps
            );
        }

        // Cas 2.2 : contraintes sans paramètre dans le corps
        preg_match_all("/&contraintes_".$objet."(?!\(.*\))/", $corps, $matches_contraintes_corps);
        //
        foreach ($matches_contraintes_corps[0] as $key => $value) {
            //
            $corps = str_ireplace(
                $value,
                $inst->construct_contraintes(),
                $corps
            );
        }
    }

    public function empty_contrainte($objet, &$titre, &$corps) {
        // Cas 1.1 : contraintes avec paramètres dans le titre
        preg_match_all("/&contraintes_".$objet."\((.*)\)/", $titre, $matches_contraintes_params);
        //
        foreach ($matches_contraintes_params[0] as $key => $value) {
            //
            $contraintes_params[0] = $value;
            $contraintes_params[1] = $matches_contraintes_params[1][$key];
            //
            $titre = str_ireplace(
                $contraintes_params[0],
                '',
                $titre
            );
        }

        // Cas 1.2 : contraintes sans paramètre dans le titre
        preg_match_all("/&contraintes_".$objet."(?!\(.*\))/", $titre, $matches_contraintes_titre);
        //
        foreach ($matches_contraintes_titre[0] as $key => $value) {
            //
            $titre = str_ireplace(
                $value,
                '',
                $titre
            );
        }

        // Cas 2.1 : contraintes avec paramètres dans le corps
        preg_match_all("/&contraintes_".$objet."\((.*)\)/", $corps, $matches_contraintes_params);
        //
        foreach ($matches_contraintes_params[0] as $key => $value) {
            //
            $contraintes_params[0] = $value;
            $contraintes_params[1] = $matches_contraintes_params[1][$key];
            //
            $corps = str_ireplace(
                $contraintes_params[0],
                '',
                $corps
            );
        }

        // Cas 2.2 : contraintes sans paramètre dans le corps
        preg_match_all("/&contraintes_".$objet."(?!\(.*\))/", $corps, $matches_contraintes_corps);
        //
        foreach ($matches_contraintes_corps[0] as $key => $value) {
            //
            $corps = str_ireplace(
                $value,
                '',
                $corps
            );
        }
    }

    /**
     * Transforme une heure en numérique.
     *
     * @param string Heure au format HH:MM
     *
     * @return float
     */
    function hours_tofloat($val) {
        if (empty($val)) {
            return 0;
        }
        $parts = explode(':', $val);
        return $parts[0] + floor(($parts[1]/60)*100) / 100;
    }

    // {{{

    /**
     * CONDITION - is_option_display_ads_field_in_tables_enabled.
     *
     * Indique si l'option permettant d'afficher la colonne 'Dossier ADS' est
     * activée ou non.
     *
     * @return boolean
     */
    function is_option_display_ads_field_in_tables_enabled() {
        if ($this->is_option_referentiel_ads_enabled() !== true) {
            return false;
        }
        return true;
    }

    /**
     *
     */
    function is_option_referentiel_ads_enabled() {
        //
        if ($this->getParameter('option_referentiel_ads') !== 'true') {
            return false;
        }
        //
        return true;
    }

    /**
     *
     */
    function get_inst_referentiel_ads() {
        require_once "../obj/interface_referentiel_ads.class.php";
        return new interface_referentiel_ads();
    }

    /**
     * Interface avec le référentiel ADS.
     */
    function execute_action_to_referentiel_ads($code, $infos) {
        $interface_referentiel_ads = $this->get_inst_referentiel_ads();
        return $interface_referentiel_ads->execute_action($code, $infos);
    }

    // }}}

    /**
     * Surcharge - getParameter().
     *
     * Force les paramètres suivants aux valeurs suivantes :
     * - is_settings_view_enabled = true
     *
     * @return mixed
     */
    function getParameter($param = null) {
        // openARIA est fait pour fonctionner avec la view 'settings'
        // pour gérer le menu 'Administration & Paramétrage'. On force donc le
        // paramètre.
        if ($param == "is_settings_view_enabled") {
            return true;
        }
        //
        if ($param == "is_option_contrainte_enabled") {
            return $this->is_option_contrainte_enabled();
        }
        //
        if ($param == "is_option_unite_accessibilite_enabled") {
            return $this->is_option_unite_accessibilite_enabled();
        }
        //
        if ($param == "is_option_referentiel_ads_enabled") {
            return $this->is_option_referentiel_ads_enabled();
        }
        return parent::getParameter($param);
    }

    /**
     * Surcharge - set_config__shortlinks().
     *
     * Ajout du lien vers la view 'settings' pour le menu 'Administration & Paramétrage'.
     *
     * @return void
     */
    protected function set_config__shortlinks() {
        parent::set_config__shortlinks();
        //
        $shortlinks = array();
        $shortlinks[] = array(
            "title" => __("Administration & Paramétrage"),
            "description" => __("Menu de tous les écrans d'administration et de paramétrage"),
            "href" => "../app/index.php?module=settings",
            "class" => "shortlinks-dashboard",
            "right" => "settings",
            "parameters" => array("is_settings_view_enabled" => true, ),
        );
        //
        $this->config__shortlinks = array_merge(
            $shortlinks,
            $this->config__shortlinks
        );
    }

    /**
     * Surcharge - set_config__footer().
     *
     * @return void
     */
    protected function set_config__footer() {
        $footer = array();
        // Documentation du site
        $footer[] = array(
            "title" => __("Documentation"),
            "description" => __("Acceder a l'espace documentation de l'application"),
            "href" => "http://docs.openmairie.org/index.php?project=openaria&version=1.13&format=html&path=manuel_utilisateur",
            "target" => "_blank",
            "class" => "footer-documentation",
        );

        // Forum openMairie
        $footer[] = array(
            "title" => __("Forum"),
            "description" => __("Espace d'échange ouvert du projet openMairie"),
            "href" => "https://communaute.openmairie.org/c/openaria",
            "target" => "_blank",
            "class" => "footer-forum",
        );

        // Portail openMairie
        $footer[] = array(
            "title" => __("openMairie.org"),
            "description" => __("Site officiel du projet openMairie"),
            "href" => "http://www.openmairie.org/catalogue/openaria/",
            "target" => "_blank",
            "class" => "footer-openmairie",
        );
        //
        $this->config__footer = $footer;
    }

    /**
     * Surcharge - set_config__menu().
     *
     * @return void
     */
    protected function set_config__menu() {
        parent::set_config__menu();
        $parent_menu = $this->config__menu;
        //
        $menu = array();
        // {{{ Rubrique ETABLISSEMENTS
        $rubrik = array(
            "title" => __("etablissements"),
            "class" => "etablissements",
            "right" => "menu_etablissement",
        );
        //
        $links = array();
        $links[] = array(
            "href" => OM_ROUTE_TAB."&obj=etablissement_referentiel_erp",
            "class" => "etablissement_referentiel_erp",
            "title" => __("Référentiel ERP"),
            "right" => array(
                "etablissement_referentiel_erp",
                "etablissement_referentiel_erp_tab",
            ),
            "open" => array(
                "index.php|etablissement_referentiel_erp[module=tab]",
                "index.php|etablissement_referentiel_erp[module=form]",
            ),
        );
        $links[] = array(
            "href" => OM_ROUTE_TAB."&obj=etablissement_tous",
            "class" => "etablissement_tous",
            "title" => __("Tous les établissements"),
            "right" => array(
                "etablissement_tous",
                "etablissement_tous_tab",
            ),
            "open" => array(
                "index.php|etablissement_tous[module=tab]",
                "index.php|etablissement_tous[module=form]",
            ),
        );
        $links[] = array(
            "title" => "<hr/>",
            "right" => array(
                "etablissement_referentiel_erp",
                "etablissement_referentiel_erp_tab",
                "etablissement_tous",
                "etablissement_tous_tab",
                "etablissement_unite",
                "etablissement_unite_tab",
            ),
            "parameters" => array(
                "is_option_unite_accessibilite_enabled" => true,
            ),
        );
        $links[] = array(
            "href" => OM_ROUTE_TAB."&obj=etablissement_unite",
            "class" => "etablissement_unite",
            "title" => __("unités d'accessibilité"),
            "right" => array(
                "etablissement_unite",
                "etablissement_unite_tab",
            ),
            "open" => array(
                "index.php|etablissement_unite[module=tab]",
                "index.php|etablissement_unite[module=form]",
            ),
            "parameters" => array(
                "is_option_unite_accessibilite_enabled" => true,
            ),
        );
        //
        $rubrik['links'] = $links;
        $menu[] = $rubrik;
        // }}}
        // {{{ Rubrique DOSSIER
        $rubrik = array(
            "title" => __("dossiers"),
            "class" => "dossiers",
            "right" => "menu_dossier_instruction",
        );
        //
        $links = array();
        $links[] = array(
            "class" => "category",
            "title" => __("DC (coordination)"),
            "right" => array(
                "dossier_coordination_nouveau_ajouter",
                "dossier_coordination_a_qualifier",
                "dossier_coordination_a_qualifier_tab",
                "dossier_coordination_a_cloturer",
                "dossier_coordination_a_cloturer_tab",
                "dossier_coordination",
                "dossier_coordination_tab",
            ),
        );
        $links[] = array(
            "href" => OM_ROUTE_FORM."&obj=dossier_coordination_nouveau&amp;action=0&amp;advs_id=&amp;tricol=&amp;valide=&amp;retour=form",
            "class" => "dossier_coordination_nouveau",
            "title" => __("Nouveau dossier"),
            "right" => array(
                "dossier_coordination_nouveau_ajouter",
            ),
            "open" => array(
                "index.php|dossier_coordination_nouveau[module=form]",
            ),
        );
        $links[] = array(
            "title" => "<hr/>",
            "right" => array(
                "dossier_coordination_nouveau_ajouter",
                "dossier_coordination_a_qualifier",
                "dossier_coordination_a_qualifier_tab",
            ),
        );
        $links[] = array(
            "href" => OM_ROUTE_TAB."&obj=dossier_coordination_a_qualifier",
            "class" => "dossier_coordination_a_qualifier",
            "title" => __("Dossiers à qualifier"),
            "right" => array(
                "dossier_coordination_a_qualifier",
                "dossier_coordination_a_qualifier_tab",
            ),
            "open" => array(
                "index.php|dossier_coordination_a_qualifier[module=tab]",
                "index.php|dossier_coordination_a_qualifier[module=form]",
            ),
        );
        $links[] = array(
            "title" => "<hr/>",
            "right" => array(
                "dossier_coordination_a_qualifier",
                "dossier_coordination_a_qualifier_tab",
                "dossier_coordination_a_cloturer",
                "dossier_coordination_a_cloturer_tab",
            ),
        );
        $links[] = array(
            "href" => OM_ROUTE_TAB."&obj=dossier_coordination_a_cloturer",
            "class" => "dossier_coordination_a_cloturer",
            "title" => __("Dossiers à cloturer"),
            "right" => array(
                "dossier_coordination_a_cloturer",
                "dossier_coordination_a_cloturer_tab",
            ),
            "open" => array(
                "index.php|dossier_coordination_a_cloturer[module=tab]",
                "index.php|dossier_coordination_a_cloturer[module=form]",
            ),
        );
        $links[] = array(
            "title" => "<hr/>",
            "right" => array(
                "dossier_coordination_a_cloturer",
                "dossier_coordination_a_cloturer",
                "dossier_coordination",
                "dossier_coordination_tab",
            ),
        );
        $links[] = array(
            "href" => OM_ROUTE_TAB."&obj=dossier_coordination",
            "class" => "dossier_coordination",
            "title" => __("Tous les dossiers"),
            "right" => array(
                "dossier_coordination",
                "dossier_coordination_tab",
            ),
            "open" => array(
                "index.php|dossier_coordination[module=tab]",
                "index.php|dossier_coordination[module=form]",
            ),
        );
        $links[] = array(
            "class" => "category",
            "title" => __("DI (instruction)"),
            "right" => array(
                "menu_dossier_instruction",
            ),
        );
        $links[] = array(
            "href" => OM_ROUTE_TAB."&obj=dossier_instruction_a_affecter",
            "class" => "dossier_instruction_a_affecter",
            "title" => __("Dossiers à affecter"),
            "right" => array(
                "dossier_instruction_a_affecter",
                "dossier_instruction_a_affecter_tab",
            ),
            "open" => array(
                "index.php|dossier_instruction_a_affecter[module=tab]",
                "index.php|dossier_instruction_a_affecter[module=form]",
            ),
        );
        $links[] = array(
            "href" => OM_ROUTE_TAB."&obj=dossier_instruction_a_qualifier",
            "class" => "dossier_instruction_a_qualifier",
            "title" => __("Dossiers à qualifier"),
            "right" => array(
                "dossier_instruction_a_qualifier",
                "dossier_instruction_a_qualifier_tab",
            ),
            "open" => array(
                "index.php|dossier_instruction_a_qualifier[module=tab]",
                "index.php|dossier_instruction_a_qualifier[module=form]",
            ),
        );
        $links[] = array(
            "title" => "<hr/>",
            "right" => array(
                "dossier_instruction_mes_plans",
                "dossier_instruction_mes_plans_tab",
                "dossier_instruction_tous_plans",
                "dossier_instruction_tous_plans_tab",
            ),
        );
        $links[] = array(
            "href" => OM_ROUTE_TAB."&obj=dossier_instruction_mes_plans",
            "class" => "dossier_instruction_mes_plans",
            "title" => __("Mes plans"),
            "right" => array(
                "dossier_instruction_mes_plans",
                "dossier_instruction_mes_plans_tab",
            ),
            "open" => array(
                "index.php|dossier_instruction_mes_plans[module=tab]",
                "index.php|dossier_instruction_mes_plans[module=form]",
            ),
        );
        $links[] = array(
            "href" => OM_ROUTE_TAB."&obj=dossier_instruction_tous_plans",
            "class" => "dossier_instruction_tous_plans",
            "title" => __("Tous les plans"),
            "right" => array(
                "dossier_instruction_tous_plans",
                "dossier_instruction_tous_plans_tab",
            ),
            "open" => array(
                "index.php|dossier_instruction_tous_plans[module=tab]",
                "index.php|dossier_instruction_tous_plans[module=form]",
            ),
        );
        $links[] = array(
            "title" => "<hr/>",
            "right" => array(
                "dossier_instruction_mes_visites",
                "dossier_instruction_mes_visites_tab",
                "dossier_instruction_tous_visites",
                "dossier_instruction_tous_visites_tab",
            ),
        );
        $links[] = array(
            "href" => OM_ROUTE_TAB."&obj=dossier_instruction_mes_visites",
            "class" => "dossier_instruction_mes_visites",
            "title" => __("Mes visites"),
            "right" => array(
                "dossier_instruction_mes_visites",
                "dossier_instruction_mes_visites_tab",
            ),
            "open" => array(
                "index.php|dossier_instruction_mes_visites[module=tab]",
                "index.php|dossier_instruction_mes_visites[module=form]",
            ),
        );
        $links[] = array(
            "href" => OM_ROUTE_TAB."&obj=dossier_instruction_tous_visites",
            "class" => "dossier_instruction_tous_visites",
            "title" => __("Toutes les visites"),
            "right" => array(
                "dossier_instruction_tous_visites",
                "dossier_instruction_tous_visites_tab",
            ),
            "open" => array(
                "index.php|dossier_instruction_tous_visites[module=tab]",
                "index.php|dossier_instruction_tous_visites[module=form]",
            ),
        );
        $links[] = array(
            "title" => "<hr/>",
            "right" => array(
                "dossier_instruction",
                "dossier_instruction_tab",
            ),
        );
        $links[] = array(
            "href" => OM_ROUTE_TAB."&obj=dossier_instruction",
            "class" => "dossier_instruction_tous",
            "title" => __("Tous les dossiers"),
            "right" => array(
                "dossier_instruction",
                "dossier_instruction_tab",
            ),
            "open" => array(
                "index.php|dossier_instruction[module=tab]",
                "index.php|dossier_instruction[module=form]",
            ),
        );
        $links[] = array(
            "class" => "category",
            "title" => __("Visites"),
            "right" => array(
                "visite_mes_visites_a_realiser",
                "visite_mes_visites_a_realiser_tab",
            ),
        );
        $links[] = array(
            "href" => OM_ROUTE_TAB."&obj=visite_mes_visites_a_realiser",
            "class" => "visite_mes_visites_a_realiser",
            "title" => __("Mes visites à réaliser"),
            "right" => array(
                "visite_mes_visites_a_realiser",
                "visite_mes_visites_a_realiser_tab",
            ),
            "open" => array(
                "index.php|visite_mes_visites_a_realiser[module=tab]",
                "index.php|visite_mes_visites_a_realiser[module=form]",
            ),
        );
        $links[] = array(
            "class" => "category",
            "title" => __("Messages"),
            "right" => array(
                "dossier_coordination_message_mes_non_lu",
                "dossier_coordination_message_mes_non_lu_tab",
                "dossier_coordination_message_tous",
                "dossier_coordination_message_tous_tab",
            ),
            "parameters" => array(
                "is_option_referentiel_ads_enabled" => true,
            ),
        );
        $links[] = array(
            "href" => OM_ROUTE_TAB."&obj=dossier_coordination_message_mes_non_lu",
            "class" => "dossier_coordination_message_mes_non_lu",
            "title" => __("Mes non lus"),
            "right" => array(
                "dossier_coordination_message_mes_non_lu",
                "dossier_coordination_message_mes_non_lu_tab",
            ),
            "open" => array(
                "index.php|dossier_coordination_message_mes_non_lu[module=tab]",
                "index.php|dossier_coordination_message_mes_non_lu[module=form]",
            ),
            "parameters" => array(
                "is_option_referentiel_ads_enabled" => true,
            ),
        );
        $links[] = array(
            "href" => OM_ROUTE_TAB."&obj=dossier_coordination_message_tous",
            "class" => "dossier_coordination_message_tous",
            "title" => __("Tous les messages"),
            "right" => array(
                "dossier_coordination_message_tous",
                "dossier_coordination_message_tous_tab",
            ),
            "open" => array(
                "index.php|dossier_coordination_message_tous[module=tab]",
                "index.php|dossier_coordination_message_tous[module=form]",
            ),
            "parameters" => array(
                "is_option_referentiel_ads_enabled" => true,
            ),
        );
        $links[] = array(
            "class" => "category",
            "title" => __("Documents entrants"),
            "right" => array(
                "piece_non_lu",
                "piece_non_lu_tab",
            ),
        );
        $links[] = array(
            "href" => OM_ROUTE_TAB."&obj=piece_non_lu",
            "class" => "piece_non_lu",
            "title" => __("mes non lus"),
            "right" => array(
                "piece_non_lu",
                "piece_non_lu_tab",
            ),
            "open" => array(
                "index.php|piece_non_lu[module=tab]",
                "index.php|piece_non_lu[module=form]",
            ),
        );
        $links[] = array(
            "class" => "category",
            "title" => __("Autorités de police"),
            "right" => array(
                "autorite_police",
                "autorite_police_tab",
                "autorite_police_non_notifiee_executee",
                "autorite_police_non_notifiee_executee_tab",
            ),
        );
        $links[] = array(
            "href" => OM_ROUTE_TAB."&obj=autorite_police_non_notifiee_executee",
            "class" => "autorite_police_non_notifiee_executee",
            "title" => __("AP non notif. ou exec."),
            "right" => array(
                "autorite_police_non_notifiee_executee",
                "autorite_police_non_notifiee_executee_tab",
            ),
            "open" => array(
                "index.php|autorite_police_non_notifiee_executee[module=tab]",
                "index.php|autorite_police_non_notifiee_executee[module=form]",
            ),
        );
        $links[] = array(
            "href" => OM_ROUTE_TAB."&obj=autorite_police",
            "class" => "autorite_police",
            "title" => __("toutes les AP"),
            "right" => array(
                "autorite_police",
                "autorite_police_tab",
            ),
            "open" => array(
                "index.php|autorite_police[module=tab]",
                "index.php|autorite_police[module=form]",
            ),
        );
        //
        $rubrik['links'] = $links;
        $menu[] = $rubrik;
        // }}}
        // {{{ Rubrique SUIVI
        $rubrik = array(
            "title" => __("suivi"),
            "class" => "suivi",
            "right" => array(
                "menu_suivi" ),
        );
        //
        $links = array();
        $links[] = array(
            "class" => "category",
            "title" => __("documents entrants"),
            "right" => array(
                "piece_bannette",
                "piece_bannette_tab",
                "piece_suivi",
                "piece_suivi_tab",
                "piece_a_valider",
                "piece_a_valider_tab",
            ),
        );
        $links[] = array(
            "href" => OM_ROUTE_TAB."&obj=piece_bannette",
            "class" => "piece_bannette",
            "title" => __("bannette"),
            "right" => array(
                "piece_bannette",
                "piece_bannette_tab",
            ),
            "open" => array(
                "index.php|piece_bannette[module=tab]",
                "index.php|piece_bannette[module=form]",
            ),
        );
        $links[] = array(
            "title" => "<hr/>",
            "right" => array(
                "piece_bannette",
                "piece_bannette_tab",
                "piece_suivi",
                "piece_suivi_tab",
                "piece_a_valider",
                "piece_a_valider_tab",
                "piece_non_lu",
                "piece_non_lu_tab",
            ),
        );
        $links[] = array(
            "href" => OM_ROUTE_TAB."&obj=piece_a_valider",
            "class" => "piece_a_valider",
            "title" => __("a valider"),
            "right" => array(
                "piece_a_valider",
                "piece_a_valider_tab",
            ),
            "open" => array(
                "index.php|piece_a_valider[module=tab]",
                "index.php|piece_a_valider[module=form]",
            ),
        );
        $links[] = array(
            "href" => OM_ROUTE_TAB."&obj=piece_suivi",
            "class" => "piece_suivi",
            "title" => __("suivi"),
            "right" => array(
                "piece_suivi",
                "piece_suivi_tab",
            ),
            "open" => array(
                "index.php|piece_suivi[module=tab]",
                "index.php|piece_suivi[module=form]",
            ),
        );
        $links[] = array(
            "title" => "<hr/>",
            "right" => array(
                "piece",
                "piece_tab",
            ),
        );
        $links[] = array(
            "href" => OM_ROUTE_TAB."&obj=piece",
            "class" => "piece",
            "title" => __("tous les documents"),
            "right" => array(
                "piece_tous",
            ),
            "open" => array(
                "index.php|piece[module=tab]",
                "index.php|piece[module=form]",
            ),
        );
        $links[] = array(
            "class" => "category",
            "title" => __("documents generes"),
            "right" => array(
                "courrier",
                "courrier_tab",
                "courrier_suivi_form_suivi",
                "courrier_rar_form_rar",
                "contact_institutionnel",
                "contact_institutionnel_tab",
            ),
        );
        $links[] = array(
            "href" => OM_ROUTE_TAB."&obj=courrier",
            "class" => "courrier",
            "title" => __("gestion"),
            "right" => array(
                "courrier",
                "courrier_tab",
            ),
            "open" => array(
                "index.php|courrier[module=tab]",
                "index.php|courrier[module=form]",
            ),
        );
        $links[] = array(
            "href" => OM_ROUTE_TAB."&obj=courrier_a_editer",
            "class" => "courrier_a_editer",
            "title" => __("à éditer"),
            "right" => array(
                "courrier_a_editer",
                "courrier_a_editer_tab",
            ),
            "open" => array(
                "index.php|courrier_a_editer[module=tab]",
                "index.php|courrier_a_editer[module=form]",
            ),
        );
        $links[] = array(
            "href" => OM_ROUTE_TAB."&obj=courrier_attente_signature",
            "class" => "courrier_attente_signature",
            "title" => __("attente signature"),
            "right" => array(
                "courrier_attente_signature",
                "courrier_attente_signature_tab",
            ),
            "open" => array(
                "index.php|courrier_attente_signature[module=tab]",
                "index.php|courrier_attente_signature[module=form]",
            ),
        );
        $links[] = array(
            "href" => OM_ROUTE_TAB."&obj=courrier_attente_retour_ar",
            "class" => "courrier_attente_retour_ar",
            "title" => __("attente retour AR"),
            "right" => array(
                "courrier_attente_retour_ar",
                "courrier_attente_retour_ar_tab",
            ),
            "open" => array(
                "index.php|courrier_attente_retour_ar[module=tab]",
                "index.php|courrier_attente_retour_ar[module=form]",
            ),
        );
        $links[] = array(
            "title" => "<hr/>",
            "right" => array(
                "courrier",
                "courrier_tab",
                "courrier_suivi_form_suivi",
                "courrier_rar_form_rar",
                "contact_institutionnel",
                "contact_institutionnel_tab",
            ),
        );
        $links[] = array(
            "href" => OM_ROUTE_FORM."&obj=courrier_suivi&amp;action=7&amp;idx=0",
            "class" => "courrier_suivi",
            "title" => __("Suivi par code barres"),
            "right" => array(
                "courrier_suivi_form_suivi",
            ),
            "open" => array(
                "index.php|courrier_suivi[module=form]",
            ),
        );
        $links[] = array(
            "href" => OM_ROUTE_FORM."&obj=courrier_rar&amp;action=8&amp;idx=0",
            "class" => "courrier_rar",
            "title" => __("edition RAR"),
            "right" => array(
                "courrier_rar_form_rar",
            ),
            "open" => array(
                "index.php|courrier_rar[module=form]",
            ),
        );
        $links[] = array(
            "href" => OM_ROUTE_TAB."&obj=contact_institutionnel",
            "class" => "contact",
            "title" => __("contact_institutionnel"),
            "right" => array(
                "contact_institutionnel",
                "contact_institutionnel_tab",
            ),
            "open" => array(
                "index.php|contact_institutionnel[module=tab]",
                "index.php|contact_institutionnel[module=form]",
            ),
        );
        $links[] = array(
            "class" => "category",
            "title" => __("programmations"),
            "right" => array(
                "programmation",
                "programmation_tab",
            ),
        );
        $links[] = array(
            "href" => OM_ROUTE_TAB."&obj=programmation",
            "class" => "programmation",
            "title" => __("gestion"),
            "right" => array(
                "programmation",
                "programmation_tab",
            ),
            "open" => array(
                "index.php|programmation[module=tab]",
                "index.php|programmation[module=form]",
            ),
        );
        $links[] = array(
            "href" => OM_ROUTE_TAB."&obj=visite",
            "class" => "visite",
            "title" => __("visites"),
            "right" => array(
                "visite",
                "visite_tab",
            ),
            "open" => array(
                "index.php|visite[module=tab]",
                "index.php|visite[module=form]",
            ),
        );
        $links[] = array(
            "class" => "category",
            "title" => __("reunions"),
            "right" => array(
                "reunion",
                "reunion_tab",
            ),
        );
        $links[] = array(
            "href" => OM_ROUTE_TAB."&obj=reunion",
            "class" => "reunion",
            "title" => __("gestion"),
            "right" => array(
                "reunion",
                "reunion_tab",
            ),
            "open" => array(
                "index.php|reunion[module=tab]",
                "index.php|reunion[module=form]",
            ),
        );
        $links[] = array(
            "class" => "category",
            "title" => __("pilotage"),
            "right" => array(
                "statistiques",
                "statistiques_tab",
                "reqmo",
            ),
        );
        $links[] = array(
            "href" => "../app/index.php?module=pilotage",
            "class" => "statistiques",
            "title" => __("statistiques"),
            "right" => array(
                "statistiques",
                "statistiques_tab",
            ),
            "open" => array(
                "index.php|[module=pilotage]",
            ),
        );
        $links[] = array(
            "href" => OM_ROUTE_MODULE_REQMO,
            "class" => "reqmo",
            "title" => __("requêtes mémorisées"),
            "right" => "reqmo",
            "open" => array(
                "index.php|[module=reqmo]",
            ),
        );
        //
        $rubrik['links'] = $links;
        $menu[] = $rubrik;
        // }}}

        // {{{ Rubrique PARAMETRAGE
        $rubrik = array(
            "title" => __("parametrage métier"),
            "class" => "parametrage",
            "right" => "menu_parametrage",
            "parameters" => array("is_settings_view_enabled" => false, ),
        );
        //
        $links = array();
        $links[] = array(
            "class" => "category",
            "title" => __("etablissements"),
            "right" => array(
                "etablissement_type",
                "etablissement_type_tab",
                "etablissement_categorie",
                "etablissement_categorie_tab",
                "etablissement_nature",
                "etablissement_nature_tab",
                "etablissement_etat",
                "etablissement_etat_tab",
                "etablissement_statut_juridique",
                "etablissement_statut_juridique_tab",
                "etablissement_tutelle_adm",
                "etablissement_tutelle_adm_tab",
                "periodicite_visites",
                "periodicite_visites_tab",
            ),
        );
        $links[] = array(
            "href" => OM_ROUTE_TAB."&obj=etablissement_type",
            "class" => "etablissement_type",
            "title" => __("types"),
            "description" => __("Typologie d'un établissement représentant son activité."),
            "right" => array(
                "etablissement_type",
                "etablissement_type_tab",
            ),
            "open" => array(
                "index.php|etablissement_type[module=tab]",
                "index.php|etablissement_type[module=form]",
            ),
        );
        $links[] = array(
            "href" => OM_ROUTE_TAB."&obj=etablissement_categorie",
            "class" => "etablissement_categorie",
            "title" => __("categories"),
            "description" => __("Liste des catégories d'un établissement représentant sa capacité."),
            "right" => array(
                "etablissement_categorie",
                "etablissement_categorie_tab",
            ),
            "open" => array(
                "index.php|etablissement_categorie[module=tab]",
                "index.php|etablissement_categorie[module=form]",
            ),
        );
        $links[] = array(
            "href" => OM_ROUTE_TAB."&obj=etablissement_nature",
            "class" => "etablissement_nature",
            "title" => __("natures"),
            "description" => __("Liste des natures d'un établissement (ERP Référentiel, ERP non référentiel, Bâtiment non ERP, ...)."),
            "right" => array(
                "etablissement_nature",
                "etablissement_nature_tab",
            ),
            "open" => array(
                "index.php|etablissement_nature[module=tab]",
                "index.php|etablissement_nature[module=form]",
            ),
        );
        $links[] = array(
            "href" => OM_ROUTE_TAB."&obj=etablissement_etat",
            "class" => "etablissement_etat",
            "title" => __("etats"),
            "description" => __("Liste des etats d'un établissement (Ouvert, Fermé, Non suivi, ...)."),
            "right" => array(
                "etablissement_etat",
                "etablissement_etat_tab",
            ),
            "open" => array(
                "index.php|etablissement_etat[module=tab]",
                "index.php|etablissement_etat[module=form]",
            ),
        );
        $links[] = array(
            "href" => OM_ROUTE_TAB."&obj=etablissement_statut_juridique",
            "class" => "etablissement_statut_juridique",
            "title" => __("statuts juridiques"),
            "description" => __("Liste des statuts juridiques d'un établissement (ville, public, privé, ...)."),
            "right" => array(
                "etablissement_statut_juridique",
                "etablissement_statut_juridique_tab",
            ),
            "open" => array(
                "index.php|etablissement_statut_juridique[module=tab]",
                "index.php|etablissement_statut_juridique[module=form]",
            ),
        );
        $links[] = array(
            "href" => OM_ROUTE_TAB."&obj=etablissement_tutelle_adm",
            "class" => "etablissement_tutelle_adm",
            "title" => __("tutelles administratives"),
            "description" => __("Liste des tutelles administratives d'un établissement lorsque son statut juridique est 'public'."),
            "right" => array(
                "etablissement_tutelle_adm",
                "etablissement_tutelle_adm_tab",
            ),
            "open" => array(
                "index.php|etablissement_tutelle_adm[module=tab]",
                "index.php|etablissement_tutelle_adm[module=form]",
            ),
        );
        $links[] = array(
            "href" => OM_ROUTE_TAB."&obj=periodicite_visites",
            "class" => "periodicite_visites",
            "title" => __("periodicites de visites"),
            "description" => __("Paramétrage de la périodicité des visites obligatoires à réaliser sur les établissements de nature 'ERP référentiel'."),
            "right" => array(
                "periodicite_visites",
                "periodicite_visites_tab",
            ),
            "open" => array(
                "index.php|periodicite_visites[module=tab]",
                "index.php|periodicite_visites[module=form]",
            ),
        );
        $links[] = array(
            "class" => "category",
            "title" => __("adresses"),
            "right" => array(
                "voie",
                "voie_tab",
                "arrondissement",
                "arrondissement_tab",
            ),
        );
        $links[] = array(
            "href" => OM_ROUTE_TAB."&obj=voie",
            "class" => "voie",
            "title" => __("voies"),
            "description" => __("Liste des voies auxquelles sont rattachées les établissements."),
            "right" => array(
                "voie",
                "voie_tab",
            ),
            "open" => array(
                "index.php|voie[module=tab]",
                "index.php|voie[module=form]",
            ),
        );
        $links[] = array(
            "href" => OM_ROUTE_TAB."&obj=arrondissement",
            "class" => "arrondissement",
            "title" => __("arrondissements"),
            "description" => __("Liste des arrondissements auxquels sont rattachés les établissements."),
            "right" => array(
                "arrondissement",
                "arrondissement_tab",
            ),
            "open" => array(
                "index.php|arrondissement[module=tab]",
                "index.php|arrondissement[module=form]",
            ),
        );
        $links[] = array(
            "class" => "category",
            "title" => __("contacts"),
            "right" => array(
                "contact_type",
                "contact_type_tab",
                "contact_civilite",
                "contact_civilite_tab",
            ),
        );
        $links[] = array(
            "href" => OM_ROUTE_TAB."&obj=contact_type",
            "class" => "contact_type",
            "title" => __("types"),
            "description" => __("Typologie d'un contact."),
            "right" => array(
                "contact_type",
                "contact_type_tab",
            ),
            "open" => array(
                "index.php|contact_type[module=tab]",
                "index.php|contact_type[module=form]",
            ),
        );
        $links[] = array(
            "href" => OM_ROUTE_TAB."&obj=contact_civilite",
            "class" => "contact_civilite",
            "title" => __("civilites"),
            "description" => __("Liste des civilités d'un contact."),
            "right" => array(
                "contact_civilite",
                "contact_civilite_tab",
            ),
            "open" => array(
                "index.php|contact_civilite[module=tab]",
                "index.php|contact_civilite[module=form]",
            ),
        );
        $links[] = array(
            "class" => "category",
            "title" => __("contraintes"),
            "parameters" => array(
                "is_option_contrainte_enabled" => true,
            ),
            "right" => array(
                "contrainte",
                "contrainte_tab",
                "contrainte_synchronisation",
                "contrainte_synchronisation_synchroniser",
            ),
        );
        $links[] = array(
            "href" => OM_ROUTE_TAB."&obj=contrainte",
            "class" => "contrainte",
            "title" => __("listing"),
            "description" => __("Liste des contraintes paramétrées et applicables aux établissements / dossiers de coordination."),
            "right" => array(
                "contrainte",
                "contrainte_tab",
            ),
            "open" => array(
                "index.php|contrainte[module=tab]",
                "index.php|contrainte[module=form]",
            ),
            "parameters" => array(
                "is_option_contrainte_enabled" => true,
            ),
        );
        $links[] = array(
            "href" => OM_ROUTE_FORM."&obj=contrainte_synchronisation&action=4&idx=0",
            "class" => "contrainte_synchronisation",
            "title" => __("synchronisation"),
            "description" => __("Synchroniser les contraintes paramétrées liées à un référentiel avec celles provenant du SIG."),
            "right" => array(
                "contrainte_synchronisation",
                "contrainte_synchronisation_synchroniser",
            ),
            "open" => array(
                "index.php|contrainte_synchronisation[module=form]",
            ),
            "parameters" => array(
                "option_sig" => "sig_externe",
                "is_option_contrainte_enabled" => true,
            ),
        );
        $links[] = array(
            "class" => "category",
            "title" => __("metiers"),
            "right" => array(
                "service",
                "service_tab",
                "acteur",
                "acteur_tab",
                "reunion_avis",
                "reunion_avis_tab",
                "reunion_instance",
                "reunion_instance_tab",
                "autorite_competente",
                "autorite_competente_tab",
                "derogation_scda",
                "derogation_scda_tab",
            ),
        );
        $links[] = array(
            "href" => OM_ROUTE_TAB."&obj=service",
            "class" => "service",
            "title" => __("services"),
            "description" => __("Liste des services."),
            "right" => array(
                "service",
                "service_tab",
            ),
            "open" => array(
                "index.php|service[module=tab]",
                "index.php|service[module=form]",
            ),
        );
        $links[] = array(
            "href" => OM_ROUTE_TAB."&obj=acteur",
            "class" => "acteur",
            "title" => __("acteurs"),
            "description" => __("Liste des acteurs de l'application représentant les cadres, techniciens et secrétaires affectés à un service. Un acteur peut être rattaché à un utilisateur ou non."),
            "right" => array(
                "acteur",
                "acteur_tab",
            ),
            "open" => array(
                "index.php|acteur[module=tab]",
                "index.php|acteur[module=form]",
            ),
        );
        $links[] = array(
            "href" => OM_ROUTE_TAB."&obj=reunion_avis",
            "class" => "reunion_avis",
            "title" => __("avis"),
            "description" => __("Liste des avis possibles sur un dossier que ce soit en réunion, suite à une visite ou dans une analyse."),
            "right" => array(
                "reunion_avis",
                "reunion_avis_tab",
            ),
            "open" => array(
                "index.php|reunion_avis[module=tab]",
                "index.php|reunion_avis[module=form]",
            ),
        );
        $links[] = array(
            "href" => OM_ROUTE_TAB."&obj=reunion_instance",
            "class" => "reunion_instance",
            "title" => __("instances"),
            "description" => __("Paramétrage des instances convoquées lors des réunions ou lors des visites ainsi que de leurs membres."),
            "right" => array(
                "reunion_instance",
                "reunion_instance_tab",
            ),
            "open" => array(
                "index.php|reunion_instance[module=tab]",
                "index.php|reunion_instance[module=form]",
            ),
        );
        $links[] = array(
            "href" => OM_ROUTE_TAB."&obj=autorite_competente",
            "class" => "autorite_competente",
            "title" => __("autorites competentes"),
            "description" => __("Liste des autorités compétentes d'un dossier d'instruction."),
            "right" => array(
                "autorite_competente",
                "autorite_competente_tab",
            ),
            "open" => array(
                "index.php|autorite_competente[module=tab]",
                "index.php|autorite_competente[module=form]",
            ),
        );
        $links[] = array(
            "href" => OM_ROUTE_TAB."&obj=derogation_scda",
            "class" => "derogation_scda",
            "title" => __("derogations scda"),
            "description" => __("Liste des dérogations SCDA disponibles depuis les données techniques des établissements."),
            "right" => array(
                "derogation_scda",
                "derogation_scda_tab",
            ),
            "open" => array(
                "index.php|derogation_scda[module=tab]",
                "index.php|derogation_scda[module=form]",
            ),
        );
        $links[] = array(
            "class" => "category",
            "title" => __("reunions"),
            "right" => array(
                "reunion_type",
                "reunion_type_tab",
                "reunion_categorie",
                "reunion_categorie_tab",
            ),
        );
        $links[] = array(
            "href" => OM_ROUTE_TAB."&obj=reunion_type",
            "class" => "reunion_type",
            "title" => __("types"),
            "description" => __("Typologie et paramétrage de toutes les informations communes à chaque réunion et qui caractérisent un type de réunion."),
            "right" => array(
                "reunion_type",
                "reunion_type_tab",
            ),
            "open" => array(
                "index.php|reunion_type[module=tab]",
                "index.php|reunion_type[module=form]",
            ),
        );
        $links[] = array(
            "href" => OM_ROUTE_TAB."&obj=reunion_categorie",
            "class" => "reunion_categorie",
            "title" => __("categories"),
            "description" => __("Liste des catégories de dossiers traitées en réunion."),
            "right" => array(
                "reunion_categorie",
                "reunion_categorie_tab",
            ),
            "open" => array(
                "index.php|reunion_categorie[module=tab]",
                "index.php|reunion_categorie[module=form]",
            ),
        );
        $links[] = array(
            "class" => "category",
            "title" => __("autorites de police"),
            "right" => array(
                "autorite_police_decision",
                "autorite_police_decision_tab",
                "autorite_police_motif",
                "autorite_police_motif_tab",
            ),
        );
        $links[] = array(
            "href" => OM_ROUTE_TAB."&obj=autorite_police_decision",
            "class" => "autorite_police_decision",
            "title" => __("types"),
            "description" => __("Typologie et paramétrage d'une décision d'autorité de police notamment les délais."),
            "right" => array(
                "autorite_police_decision",
                "autorite_police_decision_tab",
            ),
            "open" => array(
                "index.php|autorite_police_decision[module=tab]",
                "index.php|autorite_police_decision[module=form]",
            ),
        );
        $links[] = array(
            "href" => OM_ROUTE_TAB."&obj=autorite_police_motif",
            "class" => "autorite_police_motif",
            "title" => __("motifs"),
            "description" => __("Liste des motifs d'une décision d'autorité de police."),
            "right" => array(
                "autorite_police_motif",
                "autorite_police_motif_tab",
            ),
            "open" => array(
                "index.php|autorite_police_motif[module=tab]",
                "index.php|autorite_police_motif[module=form]",
            ),
        );
        $links[] = array(
            "class" => "category",
            "title" => __("analyses"),
            "right" => array(
                "analyses_type",
                "analyses_type_tab",
                "essai_realise",
                "essai_realise_tab",
                "document_presente",
                "document_presente_tab",
                "reglementation_applicable",
                "reglementation_applicable_tab",
                "prescription_reglementaire",
                "prescription_reglementaire_tab",
            ),
        );
        $links[] = array(
            "href" => OM_ROUTE_TAB."&obj=analyses_type",
            "class" => "analyses_type",
            "title" => __("types"),
            "description" => __("Typologie et paramétrage des analyses notamment les modèles d'éditions associés."),
            "right" => array(
                "analyses_type",
                "analyses_type_tab",
            ),
            "open" => array(
                "index.php|analyses_type[module=tab]",
                "index.php|analyses_type[module=form]",
            ),
        );
        $links[] = array(
            "href" => OM_ROUTE_TAB."&obj=essai_realise",
            "class" => "essai_realise",
            "title" => __("essais realises"),
            "description" => __("Textes types disponibles à l'insertion depuis le formulaire de saisie des essais réalisés lors de l'analyse des dossiers d'instruction."),
            "right" => array(
                "essai_realise",
                "essai_realise_tab",
            ),
            "open" => array(
                "index.php|essai_realise[module=tab]",
                "index.php|essai_realise[module=form]",
            ),
        );
        $links[] = array(
            "href" => OM_ROUTE_TAB."&obj=document_presente",
            "class" => "document_presente",
            "title" => __("documents presentes"),
            "description" => __("Textes types disponibles à l'insertion depuis le formulaire de saisie des documents présentés lors de l'analyse des dossiers d'instruction."),
            "right" => array(
                "document_presente",
                "document_presente_tab",
            ),
            "open" => array(
                "index.php|document_presente[module=tab]",
                "index.php|document_presente[module=form]",
            ),
        );
        $links[] = array(
            "href" => OM_ROUTE_TAB."&obj=reglementation_applicable",
            "class" => "reglementation_applicable",
            "title" => __("reglementations applicables"),
            "description" => __("Textes types disponibles à l'insertion depuis le formulaire de saisie des réglementation applicables lors de l'analyse des dossiers d'instruction."),
            "right" => array(
                "reglementation_applicable",
                "reglementation_applicable_tab",
            ),
            "open" => array(
                "index.php|reglementation_applicable[module=tab]",
                "index.php|reglementation_applicable[module=form]",
            ),
        );
        $links[] = array(
            "href" => OM_ROUTE_TAB."&obj=prescription_reglementaire",
            "class" => "prescription_reglementaire",
            "title" => __("prescriptions"),
            "description" => __("Paramétrage des prescriptions réglementaires et spécifiques utilisées dans les analyses des dossiers d'instruction."),
            "right" => array(
                "prescription_reglementaire",
                "prescription_reglementaire_tab",
            ),
            "open" => array(
                "index.php|prescription_reglementaire[module=tab]",
                "index.php|prescription_reglementaire[module=form]",
            ),
        );
        $links[] = array(
            "class" => "category",
            "title" => __("documents entrants"),
            "right" => array(
                "piece_type",
                "piece_type_tab",
            ),
        );
        $links[] = array(
            "href" => OM_ROUTE_TAB."&obj=piece_type",
            "class" => "piece_type",
            "title" => __("types"),
            "description" => __("Typologie d'un document entrant."),
            "right" => array(
                "piece_type",
                "piece_type_tab",
            ),
            "open" => array(
                "index.php|piece_type[module=tab]",
                "index.php|piece_type[module=form]",
            ),
        );
        $links[] = array(
            "class" => "category",
            "title" => __("documents generes"),
            "right" => array(
                "courrier_type",
                "courrier_type_tab",
                "courrier_type_categorie",
                "courrier_type_categorie_tab",
                "modele_edition",
                "modele_edition_tab",
                "courrier_texte_type",
                "courrier_texte_type_tab",
                "signataire_qualite",
                "signataire_qualite_tab",
                "signataire",
                "signataire_tab",
            ),
        );
        $links[] = array(
            "href" => OM_ROUTE_TAB."&obj=courrier_type",
            "class" => "courrier_type",
            "title" => __("types"),
            "description" => __("Typologie et paramétrage d'un modèle d'édition qui permet de filtrer les modèles d'édition disponibles en fonction du contexte des interfaces."),
            "right" => array(
                "courrier_type",
                "courrier_type_tab",
            ),
            "open" => array(
                "index.php|courrier_type[module=tab]",
                "index.php|courrier_type[module=form]",
            ),
        );
        $links[] = array(
            "href" => OM_ROUTE_TAB."&obj=courrier_type_categorie",
            "class" => "courrier_type_categorie",
            "title" => __("categories"),
            "description" => __("Paramétrage des catégories de types de modèles d'édition. Cette catégorisation permet de définir le contexte dans lequel les types de modèles d'édtion rattachés à cette catégorie vont être disponibles."),
            "right" => array(
                "courrier_type_categorie",
                "courrier_type_categorie_tab",
            ),
            "open" => array(
                "index.php|courrier_type_categorie[module=tab]",
                "index.php|courrier_type_categorie[module=form]",
            ),
        );
        $links[] = array(
            "title" => "<hr/>",
            "right" => array(
                "modele_edition",
                "modele_edition_tab",
            ),
        );
        $links[] = array(
            "href" => OM_ROUTE_TAB."&obj=modele_edition",
            "class" => "modele_edition",
            "title" => __("modeles d'edition"),
            "description" => __("Paramétrage des modèles d'édition par la sélection de leur type et de la lettre type utilisée."),
            "right" => array(
                "modele_edition",
                "modele_edition_tab",
            ),
            "open" => array(
                "index.php|modele_edition[module=tab]",
                "index.php|modele_edition[module=form]",
            ),
        );
        $links[] = array(
            "href" => OM_ROUTE_TAB."&obj=courrier_texte_type",
            "class" => "courrier_texte_type",
            "title" => __("complements"),
            "description" => __("Textes types disponibles à l'insertion depuis le formulaire de saisie d'un document généré dans les champs compléments."),
            "right" => array(
                "courrier_texte_type",
                "courrier_texte_type_tab",
            ),
            "open" => array(
                "index.php|courrier_texte_type[module=tab]",
                "index.php|courrier_texte_type[module=form]",
            ),
        );
        $links[] = array(
            "href" => OM_ROUTE_TAB."&obj=signataire_qualite",
            "class" => "signataire_qualite",
            "title" => __("qualites de signataire"),
            "description" => __("Liste des qualités d'un signataire."),
            "right" => array(
                "signataire_qualite",
                "signataire_qualite_tab",
            ),
            "open" => array(
                "index.php|signataire_qualite[module=tab]",
                "index.php|signataire_qualite[module=form]",
            ),
        );
        $links[] = array(
            "href" => OM_ROUTE_TAB."&obj=signataire",
            "class" => "signataire",
            "title" => __("signataires"),
            "description" => __("Paramétrage des signataires disponibles depuis un document généré ou un PV."),
            "right" => array(
                "signataire",
                "signataire_tab",
            ),
            "open" => array(
                "index.php|signataire[module=tab]",
                "index.php|signataire[module=form]",
            ),
        );
        $links[] = array(
            "class" => "category",
            "title" => __("visites"),
            "right" => array(
                "visite_duree",
                "visite_duree_tab",
                "visite_motif_annulation",
                "visite_motif_annulation_tab",
            ),
        );
        $links[] = array(
            "href" => OM_ROUTE_TAB."&obj=visite_duree",
            "class" => "visite_duree",
            "title" => __("durees"),
            "description" => __("Liste des durées de visite."),
            "right" => array(
                "visite_duree",
                "visite_duree_tab",
            ),
            "open" => array(
                "index.php|visite_duree[module=tab]",
                "index.php|visite_duree[module=form]",
            ),
        );
        $links[] = array(
            "href" => OM_ROUTE_TAB."&obj=visite_motif_annulation",
            "class" => "visite_motif_annulation",
            "title" => __("motifs d'annulation"),
            "description" => __("Liste des motifs d'annulation d'une visite."),
            "right" => array(
                "visite_motif_annulation",
                "visite_motif_annulation_tab",
            ),
            "open" => array(
                "index.php|visite_motif_annulation[module=tab]",
                "index.php|visite_motif_annulation[module=form]",
            ),
        );
        $links[] = array(
            "class" => "category",
            "title" => __("dossiers"),
            "right" => array(
                "dossier_type",
                "dossier_type_tab",
                "dossier_coordination_type",
                "dossier_coordination_type_tab",
                "dossier_coordination_geocoder_tous",
                "dossier_coordination_geocoder_tous_geocoder",
            ),
        );
        $links[] = array(
            "href" => OM_ROUTE_TAB."&obj=dossier_type",
            "class" => "dossier_type",
            "title" => __("types"),
            "description" => __("Typologie des types de dossiers de coordination (Visites, Plans, ...)."),
            "right" => array(
                "dossier_type",
                "dossier_type_tab",
            ),
            "open" => array(
                "index.php|dossier_type[module=tab]",
                "index.php|dossier_type[module=form]",
            ),
        );
        $links[] = array(
            "href" => OM_ROUTE_TAB."&obj=dossier_coordination_type",
            "class" => "dossier_coordination_type",
            "title" => __("types de DC"),
            "description" => __("Typologie et paramétrage des dossiers de coordination (AT, PC, Visite périodque, ...)."),
            "right" => array(
                "dossier_coordination_type",
                "dossier_coordination_type_tab",
            ),
            "open" => array(
                "index.php|dossier_coordination_type[module=tab]",
                "index.php|dossier_coordination_type[module=form]",
            ),
        );
        $links[] = array(
            "title" => "<hr/>",
            "right" => array(
                "dossier_coordination_geocoder_tous",
                "dossier_coordination_geocoder_tous_geocoder",
            ),
            "parameters" => array(
                "option_sig" => "sig_externe",
            ),
        );
        $links[] = array(
            "href" => OM_ROUTE_FORM."&obj=dossier_coordination_geocoder_tous&action=260&idx=0",
            "class" => "dossier_coordination_geocoder_tous",
            "title" => __("géolocalisation"),
            "description" => __("Géolocaliser tous les dossiers de coordination et établissements."),
            "right" => array(
                "dossier_coordination_geocoder_tous",
                "dossier_coordination_geocoder_tous_geocoder",
            ),
            "open" => array(
                "index.php|dossier_coordination_geocoder_tous[module=form]",
            ),
            "parameters" => array(
                "option_sig" => "sig_externe",
            ),
        );

        $links[] = array(
            "class" => "category",
            "title" => __("interface ADS"),
            "right" => array(
                "interface_avec_le_referentiel_ads_controlpanel",
                "dossier_coordination_message_controlpanel",
            ),
            "parameters" => array(
                "is_option_referentiel_ads_enabled" => true,
            ),
        );
        $links[] = array(
            "href" => "../app/index.php?module=settings&controlpanel=interface_avec_le_referentiel_ads",
            "class" => "interface_avec_le_referentiel_ads",
            "title" => __("interface avec le réferentiel ADS"),
            "description" => __("Gère les échanges entre les applications openARIA et openADS."),
            "right" => array(
                "interface_avec_le_referentiel_ads_controlpanel",
            ),
            "open" => array(
                "index.php|[module=settings]",
            ),
            "parameters" => array(
                "is_option_referentiel_ads_enabled" => true,
            ),
        );
        $links[] = array(
            "href" => OM_ROUTE_FORM."&obj=dossier_coordination_message&action=99&idx=0",
            "class" => "controlpanel_messages",
            "title" => __("messages"),
            "description" => __("Ce module permet d'accéder à des statistiques sur les messages et de lancer des traitements."),
            "right" => array(
                "dossier_coordination_message_controlpanel",
            ),
            "open" => array(
                "index.php|[module=settings]",
            ),
            "parameters" => array(
                "is_option_referentiel_ads_enabled" => true,
            ),
        );
        //
        $rubrik['links'] = $links;
        $menu[] = $rubrik;
        // }}}
        // On supprime la rubrique "Export" du framework car l'entrée de menu
        // pour les requêtes mémorisées se trouve dans la rubrique "Suivi"
        unset($parent_menu["om-menu-rubrik-export"]);
        //
        $this->config__menu = array_merge(
            $menu,
            $parent_menu
        );
    }
}
