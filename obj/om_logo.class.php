<?php
/**
 * Ce script définit la classe 'om_logo'.
 *
 * @package openaria
 * @version SVN : $Id$
 */

require_once PATH_OPENMAIRIE."obj/om_logo.class.php";

/**
 * Définition de la classe 'om_logo' (om_dbform).
 */
class om_logo extends om_logo_core {


    // {{{ BEGIN - METADATA FILESTORAGE

    /**
     *
     */
    var $metadata = array(
        "uid" => array(
            "titre" => "get_md_titre",
            "description" => "get_md_description",
        ),
    );

    function get_md_titre() { return "Logo ".$this->getVal("libelle"); }
    function get_md_description() { return "logo"; }

    // }}} END - METADATA FILESTORAGE

}

