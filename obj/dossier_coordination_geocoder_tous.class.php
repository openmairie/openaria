<?php
/**
 * Ce script définit la classe 'dossier_coordination_geocoder_tous'.
 *
 * @package openaria
 * @version SVN : $Id$
 */

require_once "../obj/dossier_coordination.class.php";

/**
 * Définition de la classe 'dossier_coordination_geocoder_tous' (om_dbform).
 *
 * Surcharge de la classe 'dossier_coordination'.
 */
class dossier_coordination_geocoder_tous extends dossier_coordination {

    /**
     *
     */
    protected $_absolute_class_name = "dossier_coordination_geocoder_tous";

    //
    // VIEWS
    //

    /**
     * VIEW - view_geocoder_tous.
     *
     * Permet de géolocaliser tous les dossiers de coordination ainsi que
     * tous les établissements d'openARIA qui ne sont pas déjà localisés.
     *
     * @return void
     */
    function view_geocoder_tous() {
        // Vérification de l'accessibilité sur l'élément
        $this->checkAccessibility();

        // Traitement et affichage message si formulaire validé
        if ($this->f->get_submitted_post_value('valider_geolocalisation') === 'oui') {

            // On considère que le traitement est toujours valide
            $this->geocoder_tous();
            $this->display_msg();
        }

        // Formulaire de validation
        $this->f->layout->display__form_container__begin(array(
            "action" => $this->getDataSubmit(),
            "name" => "form-dc-geocoder-tous",
            "id" => "form-dc-geocoder-tous",
        ));
        $champs = array("valider_geolocalisation", );
        $form = $this->f->get_inst__om_formulaire(array(
            "champs" => $champs,
            "validation" => 0,
            "maj" => 0,
        ));
        $form->setType("valider_geolocalisation", "hidden");
        $form->setTaille("valider_geolocalisation", 20);
        $form->setMax("valider_geolocalisation", 20);
        $form->setVal("valider_geolocalisation", "oui");
        $form->entete();
        $this->f->displayDescription(
            __("Cette page permet de géolocaliser tous les dossiers de coordination et établissements qui peuvent l'être automatiquement.").'<br/>'.__("Concernant ceux pour lesquels ce n'est pas possible, vous pouvez filtrer les éléments non localisés depuis leur listing puis les localiser manuellement.")
        );
        $form->afficher($champs, 0, false, false);
        $form->enpied();
        $this->f->layout->display__form_controls_container__begin(array(
            "controls" => "bottom",
        ));
        $this->f->layout->display_form_button(array(
            "value" => __("Géolocaliser"),
            "name" => "btn_geocoder_tous"
        ));
        $this->f->layout->display__form_controls_container__end();
        $this->f->layout->display__form_container__end();
    }

    //
    // CONDITIONS
    //

    /**
     * CONDITION - can_geocoder_tous.
     * 
     * Méthode permettant de tester la condition nécessaire
     * à la géolocalisation.
     * 
     * @return boolean
     */
    function can_geocoder_tous() {
        // L'option SIG doit être activée
        if ($this->is_option_sig_enabled() === true) {
            return true;
        }
        return false;
    }

    //
    // TREATMENTS
    //

    /**
     * TREATMENT - geocoder_tous.
     *
     * Géolocalise tous les DC et établissements
     *
     * @param  array $val Valeurs soumises par le formulaire
     * @throws treatment_exception
     * @return void
     */
    function geocoder_tous($val = array()) {
        $this->begin_treatment(__METHOD__);
        // Récupération de tous les éléments non localisés
        $dc_list = $this->get_elements_non_localises('dossier_coordination');
        $etab_list = $this->get_elements_non_localises('etablissement');
        // Init compteurs
        $tot_dc_ok = 0;
        $tot_etab_ok = 0;
        // Géolocalisation des dossiers de coordination
        foreach ($dc_list as $dc_idx) {
            $dc = $this->f->get_inst__om_dbform(array(
                "obj" => "dossier_coordination",
                "idx" => $dc_idx,
            ));
            // Le DC est susceptible de pas être géocodé, du fait d'une nécessité
            // d'intervention manuelle ou d'une erreur BDD/SIG.
            // Cela ne doit pas stopper le traitement multiple mais non consigne l'échec
            // en n'incrémentant pas le compteur de DC géocodés.
            if ($dc->geocoder_treatment() === true) {
                $tot_dc_ok++;
            }
        }
        // Géolocalisation des établissements
        foreach ($etab_list as $etab_idx) {
            $etab = $this->f->get_inst__om_dbform(array(
                "obj" => "etablissement",
                "idx" => $etab_idx,
            ));
            // Même logique que pour le DC
            if ($etab->geocoder_treatment() === true) {
                $tot_etab_ok++;
            }
        }
        //
        $tot_dc_ko = count($dc_list) - $tot_dc_ok;
        $tot_etab_ko = count($etab_list) - $tot_etab_ok;
        //
        $this->addToMessage($this->contruct_msg_val($tot_dc_ok, $tot_dc_ko, $tot_etab_ok, $tot_etab_ko));
        return $this->end_treatment(__METHOD__, true);
    }

    //
    // REQUÊTES SQL
    //

    /**
     * Récupération des éléments non localisés pour l'objet fourni.
     *
     * @param  string $obj classe métier (dossier_coordination/etablissement)
     * @throws treatment_exception
     * @return array
     */
    private function get_elements_non_localises($obj) {
        //
        $elems = array();
        //
        $sql = "SELECT $obj as idx
            FROM ".DB_PREFIXE."$obj
            WHERE geolocalise = 'f'";
        $this->f->addToLog(__METHOD__."() : db->query(\"".$sql."\")", VERBOSE_MODE);
        $res = $this->f->db->query($sql);
        if ($this->f->isDatabaseError($res, true)) {
            throw new treatment_exception(
                __("Erreur de base de donnees. Contactez votre administrateur."),
                __METHOD__.": ".$res->getMessage()
            );
        }
        //
        while ($row = &$res->fetchRow(DB_FETCHMODE_ASSOC)) {
            $elems[] = $row['idx'];
        }
        //
        return  $elems;
    }

    //
    // HELPERS
    //

    /**
     * Construit le message de validation de la géolocalisation
     * /!\ et affiche en masqué les valeurs
     * 
     * @param  integer $tot_dc_ok   nombre de DC géocodés
     * @param  integer $tot_dc_ko   nombre de DC non géocodés
     * @param  integer $tot_etab_ok nombre d'établissements géocodés
     * @param  integer $tot_etab_ko nombre d'établissements non géocodés
     * @return string
     */
    function contruct_msg_val($tot_dc_ok, $tot_dc_ko, $tot_etab_ok, $tot_etab_ko) {
        //
        $tot_ok = $tot_dc_ok+$tot_etab_ok;
        $tot_ko = $tot_dc_ko+$tot_etab_ko;
        $puce = '&nbsp;&bull;&nbsp;';
        $lf = '<br/>';
        //
        $msg_temp = __('%1$s élément%2$s géolocalisé%2$s dont :').$lf
            .$puce.'<span id="tot_dc_ok">%3$s</span> '.__('dossier%4$s de coordination').$lf
            .$puce.'<span id="tot_etab_ok">%5$s</span> '.__('établissement%6$s').$lf
            .__('%7$s élément%8$s non géolocalisé%8$s dont :').$lf
            .$puce.__('%9$s dossier%10$s de coordination').$lf
            .$puce.__('%11$s établissement%12$s');
        return sprintf($msg_temp,
            $tot_ok, $this->f->pluriel($tot_ok),
            $tot_dc_ok, $this->f->pluriel($tot_dc_ok),
            $tot_etab_ok, $this->f->pluriel($tot_etab_ok),
            $tot_ko, $this->f->pluriel($tot_ko),
            $tot_dc_ko, $this->f->pluriel($tot_dc_ko),
            $tot_etab_ko, $this->f->pluriel($tot_etab_ko)
        );
    }

    //
    // OVERRIDE
    //

    /**
     * Définition des actions disponibles sur la classe.
     *
     * @return void
     */
    function init_class_actions() {

        // On récupère les actions génériques définies dans la méthode 
        // d'initialisation de la classe parente
        parent::init_class_actions();

        // ACTION - 260 - geocoder_tous
        // Interface spécifique de géolocalisation de tous les DC
        // et établissements
        $this->class_actions[260] = array(
            "identifier" => "geocoder_tous",
            "view" => "view_geocoder_tous",
            "permission_suffix" => "geocoder_tous",
            "condition" => "can_geocoder_tous",
        );
    }
}

