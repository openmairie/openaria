<?php
/**
 * Ce script définit la classe 'autorite_police'.
 *
 * @package openaria
 * @version SVN : $Id$
 */

require_once "../gen/obj/autorite_police.class.php";

/**
 * Définition de la classe 'autorite_police' (om_dbform).
 */
class autorite_police extends autorite_police_gen {

    /**
     * Liaison NaN
     */
    var $liaisons_nan = array(
        //
        "lien_autorite_police_courrier" => array(
            "table_l" => "lien_autorite_police_courrier",
            "table_f" => "courrier",
            "field" => "courriers_lies",
        ),
    );


    /**
     *
     */
    function get_var_sql_forminc__tableSelect() {
        return sprintf(
            '%1$s%2$s
            LEFT JOIN %1$slien_autorite_police_courrier
                ON lien_autorite_police_courrier.autorite_police = autorite_police.autorite_police
            LEFT JOIN %1$scourrier
                ON lien_autorite_police_courrier.courrier = courrier.courrier',
            DB_PREFIXE,
            $this->table
        );
    }

    /**
     *
     */
    function get_var_sql_forminc__champs() {
        return array(
            "autorite_police.autorite_police",
            "autorite_police.etablissement",
            "autorite_police.dossier_coordination",
            "autorite_police_decision",
            "date_decision",
            "delai",
            "autorite_police_motif",
            "autorite_police.cloture",
            "date_notification",
            "autorite_police.date_butoir",
            "service",
            "array_to_string(
                array_agg(
                    lien_autorite_police_courrier.courrier
                    ORDER BY courrier.courrier
                ),
            ';') as courriers_lies",
            "dossier_instruction_reunion",
            "dossier_instruction_reunion_prochain",
        );
    }

    /**
     *
     */
    function get_var_sql_forminc__selection() {
        return " GROUP BY autorite_police.autorite_police ";
    }

    /**
     *
     */
    function get_var_sql_forminc__sql_dossier_instruction_reunion() {
        return sprintf(
            'SELECT
                dossier_instruction_reunion.dossier_instruction_reunion,
                CASE WHEN dossier_instruction_reunion.reunion IS NOT NULL THEN CONCAT(
                    \'%2$s\',
                    dossier_instruction_reunion.dossier_instruction_reunion,
                    \' %4$s (\',
                    reunion.code,
                    \')\'
                )
                WHEN dossier_instruction_reunion.dossier_instruction_reunion IS NOT NULL THEN CONCAT(
                    \'%2$s\',
                    dossier_instruction_reunion.dossier_instruction_reunion,
                    \' %3$s \',
                    to_char(dossier_instruction_reunion.date_souhaitee, \'DD/MM/YYYY\')
                )
                ELSE \' \'
                END as lib
            FROM
                %1$sdossier_instruction_reunion
                    LEFT JOIN %1$sreunion ON dossier_instruction_reunion.reunion=reunion.reunion
                    LEFT JOIN %1$sreunion_type On reunion.reunion_type=reunion_type.reunion_type
            ORDER BY lib, dossier_instruction_reunion.dossier_instruction_reunion ASC
            ',
            DB_PREFIXE,
            __("Demande de passage n°"),
            __("souhaitée le"),
            __("planifiée")
        );
    }

    /**
     *
     */
    function get_var_sql_forminc__sql_dossier_instruction_reunion_by_id() {
        return sprintf(
            'SELECT
                dossier_instruction_reunion.dossier_instruction_reunion,
                CASE WHEN dossier_instruction_reunion.reunion IS NOT NULL THEN CONCAT(
                    \'%2$s\',
                    dossier_instruction_reunion.dossier_instruction_reunion,
                    \' %4$s (\',
                    reunion.code,
                    \')\'
                )
                WHEN dossier_instruction_reunion.dossier_instruction_reunion IS NOT NULL THEN CONCAT(
                    \'%2$s\',
                    dossier_instruction_reunion.dossier_instruction_reunion,
                    \' %3$s \',
                    to_char(dossier_instruction_reunion.date_souhaitee, \'DD/MM/YYYY\')
                )
                ELSE \' \'
                END as lib
            FROM
                %1$sdossier_instruction_reunion
                    LEFT JOIN %1$sreunion ON dossier_instruction_reunion.reunion=reunion.reunion
                    LEFT JOIN %1$sreunion_type On reunion.reunion_type=reunion_type.reunion_type
            WHERE
                dossier_instruction_reunion.dossier_instruction_reunion = <idx>
            ',
            DB_PREFIXE,
            __("Demande de passage n°"),
            __("souhaitée le"),
            __("planifiée")
        );
    }

    /**
     *
     */
    function get_var_sql_forminc__sql_dossier_instruction_reunion_prochain() {
        return sprintf(
            'SELECT
                dossier_instruction_reunion.dossier_instruction_reunion,
                CASE WHEN dossier_instruction_reunion.reunion IS NOT NULL THEN CONCAT(
                    \'%2$s\',
                    dossier_instruction_reunion.dossier_instruction_reunion,
                    \' %4$s (\',
                    reunion.code,
                    \')\'
                )
                WHEN dossier_instruction_reunion.dossier_instruction_reunion IS NOT NULL THEN CONCAT(
                    \'%2$s\',
                    dossier_instruction_reunion.dossier_instruction_reunion,
                    \' %3$s \',
                    to_char(dossier_instruction_reunion.date_souhaitee, \'DD/MM/YYYY\')
                )
                ELSE \' \'
                END as lib
            FROM
                %1$sdossier_instruction_reunion
                    LEFT JOIN %1$sreunion ON dossier_instruction_reunion.reunion=reunion.reunion
                    LEFT JOIN %1$sreunion_type On reunion.reunion_type=reunion_type.reunion_type
            ORDER BY lib, dossier_instruction_reunion.dossier_instruction_reunion ASC
            ',
            DB_PREFIXE,
            __("Demande de passage n°"),
            __("souhaitée le"),
            __("planifiée")
        );
    }

    /**
     *
     */
    function get_var_sql_forminc__sql_dossier_instruction_reunion_prochain_by_id() {
        return sprintf(
            'SELECT
                dossier_instruction_reunion.dossier_instruction_reunion,
                CASE WHEN dossier_instruction_reunion.reunion IS NOT NULL THEN CONCAT(
                    \'%2$s\',
                    dossier_instruction_reunion.dossier_instruction_reunion,
                    \' %4$s (\',
                    reunion.code,
                    \')\'
                )
                WHEN dossier_instruction_reunion.dossier_instruction_reunion IS NOT NULL THEN CONCAT(
                    \'%2$s\',
                    dossier_instruction_reunion.dossier_instruction_reunion,
                    \' %3$s \',
                    to_char(dossier_instruction_reunion.date_souhaitee, \'DD/MM/YYYY\')
                )
                ELSE \' \'
                END as lib
            FROM
                %1$sdossier_instruction_reunion
                    LEFT JOIN %1$sreunion ON dossier_instruction_reunion.reunion=reunion.reunion
                    LEFT JOIN %1$sreunion_type On reunion.reunion_type=reunion_type.reunion_type
            WHERE
                dossier_instruction_reunion.dossier_instruction_reunion = <idx>
            ',
            DB_PREFIXE,
            __("Demande de passage n°"),
            __("souhaitée le"),
            __("planifiée")
        );
    }

    /**
     * Retourne le libellé par défaut d'un enregistrement.
     *
     * Principalement utilisé pour le titre de la page. C'est le dernier élément du 
     * titre : Rubrique > Catégorie > Libellé par défaut.
     *
     * @return string
     */
    function get_default_libelle() {
        $inst_autorite_police_decision = $this->get_inst_common(
            "autorite_police_decision",
            $this->getVal("autorite_police_decision")
        );
        return "AP n°".$this->getVal($this->clePrimaire)." ".$inst_autorite_police_decision->getVal("libelle")." du ".$this->f->formatDate($this->getVal("date_decision"), true);
    }

    /**
     * Permet de définir le libellé des champs.
     *
     * @param object  &$form Instance du formulaire
     * @param integer $maj   Mode du formulaire
     */
    function setLib(&$form, $maj) {
        //
        parent::setLib($form, $maj);
        //
        $form->setLib("courriers_lies", __("courriers_lies"));
    }

    /**
     * Permet de définir le type des champs.
     *
     * @param object  &$form Instance du formulaire
     * @param integer $maj   Mode du formulaire
     */
    function setType(&$form, $maj) {
        parent::setType($form, $maj);
        //
        $form->setType("dossier_coordination", "hidden");
        $form->setType("etablissement", "hidden");
        //
        if ($maj==0){ //ajout
            $form->setType('courriers_lies', 'select_multiple');
            $form->setType('service', 'hidden');
            $form->setType('dossier_instruction_reunion_prochain', 'hidden');
            $form->setType('date_notification', 'hidden');
            $form->setType('date_butoir', 'hidden');
            $form->setType('cloture', 'hidden');
        }// fin ajout
        if ($maj==1){ //modifier
            $form->setType('courriers_lies', 'select_multiple');
            $form->setType('service', 'hidden');
            $form->setType('dossier_instruction_reunion', 'selecthiddenstatic');
            $form->setType('dossier_instruction_reunion_prochain', 'hidden');
            $form->setType('date_notification', 'hiddenstaticdate');
            $form->setType('date_butoir', 'hiddenstaticdate');
            $form->setType('cloture', 'checkboxhiddenstatic');
            $form->setType('etablissement', 'etablissement');
            $form->setType('dossier_coordination', 'dossier_coordination_link');
        }// fin modifier
        if ($maj==2){ //supprimer
            $form->setType('courriers_lies', 'select_multiple_static');
            $form->setType('etablissement', 'etablissement');
            $form->setType('dossier_coordination', 'dossier_coordination_link');
        }//fin supprimer
        if ($maj==3){ //consulter
            $form->setType('courriers_lies', 'select_multiple_static');
            $form->setType('etablissement', 'etablissement');
            $form->setType('dossier_coordination', 'dossier_coordination_link');
        }//fin consulter

        // En mode "cloturer" et "decloturer"
        if ($maj == 7 || $maj == 8) {
            foreach ($this->champs as $key => $value) {
                $form->setType($value, 'hidden');
            }
            $form->setType("autorite_police", "hiddenstatic");
        }
    }

    /**
     * Méthode qui effectue les requêtes de configuration des champs.
     *
     * @param object  $form Instance du formulaire.
     * @param integer $maj  Mode du formulaire.
     * @param null    $dnu1 @deprecated Ancienne ressource de base de données.
     * @param null    $dnu2 @deprecated Ancien marqueur de débogage.
     *
     * @return void
     */
    function setSelect(&$form, $maj, &$dnu1 = null, $dnu2 = null) {
        parent::setSelect($form, $maj, $dnu1, $dnu2);

        // Filtre par établissement
        $dossier_coordination = "";
        $champ = "dossier_coordination";
        if (isset($_POST[$champ])) {
            $dossier_coordination = $_POST[$champ];
        } elseif($this->getParameter($champ) != "") {
            $dossier_coordination = $this->getParameter($champ);
        } elseif(isset($form->val[$champ])) {
            $dossier_coordination = $form->val[$champ];
        } elseif($this->getVal($champ) != null) {
            $dossier_coordination = $this->getVal($champ);
        }
        if ($dossier_coordination == "") {
            $dossier_coordination = -1;
        }

        // Liste des champs pour générer le libellé d'un courrier
        $list_fields_libelle = array(
            "courrier.courrier",
            "to_char(courrier.om_date_creation ,'DD/MM/YYYY')",
            "CASE 
                WHEN dossier_instruction.libelle IS NOT NULL THEN dossier_instruction.libelle 
                WHEN dossier_coordination.libelle IS NOT NULL THEN dossier_coordination.libelle 
                WHEN etablissement.libelle IS NOT NULL THEN etablissement.libelle 
            END",
            "courrier_type.code",
            "modele_edition.code",
            "CONCAT (contact_type.libelle, ' ', contact_civilite.libelle, ' ', contact.nom, ' ', contact.prenom)"
        );
        // Liste des jointures pour le libellé
        $list_joint_libelle = array(
            "lien_courrier_contact ON lien_courrier_contact.courrier = courrier.courrier",
            "contact ON lien_courrier_contact.contact = contact.contact",
            "etablissement ON courrier.etablissement = etablissement.etablissement",
            "dossier_coordination ON courrier.dossier_coordination = dossier_coordination.dossier_coordination",
            "dossier_instruction ON courrier.dossier_instruction = dossier_instruction.dossier_instruction",
            "modele_edition ON courrier.modele_edition = modele_edition.modele_edition",
            "courrier_type ON courrier.courrier_type = courrier_type.courrier_type",
            "contact_type ON contact.contact_type = contact_type.contact_type",
            "contact_civilite ON contact.civilite = contact_civilite.contact_civilite"
        );
        // Liaison NaN - autorite_police/courrier
        // SELECT
        $sql_courriers_lies_select = "SELECT courrier.courrier, CONCAT(%s) as lib ";
        // FROM
        $sql_courriers_lies_from = " FROM ".DB_PREFIXE."courrier %s ";
        //
        $sql_courriers_lies_where = " 
        WHERE courrier.mailing IS FALSE 
            AND courrier.finalise IS TRUE
            AND (LOWER(courrier_type.code) = LOWER('DEC') OR LOWER(courrier_type.code) = LOWER('NOTIFAP'))
            AND dossier_coordination.dossier_coordination = <idx_dc>
        ORDER BY courrier.courrier";
        //
        $sql_courriers_lies_by_id_where = " 
        WHERE courrier.courrier IN (<idx>)
        ORDER BY courrier.courrier";

        //
        $sql_courriers_lies_select = sprintf($sql_courriers_lies_select, implode(",' - ', ", $list_fields_libelle));
        $sql_courriers_lies_from = sprintf($sql_courriers_lies_from, " LEFT JOIN ".DB_PREFIXE.implode(" LEFT JOIN ".DB_PREFIXE, $list_joint_libelle));
        $sql_courriers_lies_where = str_replace('<idx_dc>', $dossier_coordination, $sql_courriers_lies_where);

        //
        $sql_courriers_lies = $sql_courriers_lies_select.$sql_courriers_lies_from.$sql_courriers_lies_where;
        $sql_courriers_lies_by_id = $sql_courriers_lies_select.$sql_courriers_lies_from.$sql_courriers_lies_by_id_where;
        //
        $this->init_select($form, $dnu1, $maj, $dnu2, "courriers_lies", $sql_courriers_lies, $sql_courriers_lies_by_id, false, true);
        
        if ($maj == 1 || $maj == 2 || $maj == 3) { // Modification, suppression, consultation
            // Paramétres envoyés au type link dossier_coordination
            $dossier_coordinationId = $this->getVal('dossier_coordination');
            if ($dossier_coordinationId != '' &&
                is_numeric($dossier_coordinationId)) {
                $dossier_coordination = $this->f->get_inst__om_dbform(array(
                    "obj" => "dossier_coordination",
                    "idx" => $dossier_coordinationId,
                ));
                
                $valueObj = "dossier_coordination";
                $valuelibelle = $dossier_coordination->getVal('libelle');
                $valueIdx = $dossier_coordination->getVal($dossier_coordination->clePrimaire);
                $valueGeolocalise = $dossier_coordination->getVal('geolocalise');
                
                $params = array();
                $params['obj'] = $valueObj;
                $params['libelle'] = $valuelibelle;
                $params['idx'] = $valueIdx;
                $params['geolocalise'] = $valueGeolocalise;
                $form->setSelect("dossier_coordination", $params);
            }

            // Paramétres envoyés au type établissement
            $etablissementId = $this->getVal('etablissement');
            // Si le champ etablissement n'est pas vide
            if ($etablissementId != '' &&
                is_numeric($etablissementId)) {
                // Initialisation de l'objet
                $obj = "etablissement_tous";

                // Instance de la classe etablissement
                $etablissement = $this->f->get_inst__om_dbform(array(
                    "obj" => "etablissement",
                    "idx" => $etablissementId,
                ));

                // Récupère toutes les données nécessaires de l'établissement
                $values_etablissement = $etablissement->get_data($etablissementId);
                
                // Paramétres envoyés au type etablissement
                $params = array();
                $params['obj'] = $obj;
                $params['libelle'] = $values_etablissement['code'].' - '.$values_etablissement['libelle'];
                $params['values'] = $values_etablissement;
                $form->setSelect("etablissement", $params);

            }

        }
        
    }

    /**
     * Permet de définir l’attribut “onchange” sur chaque champ.
     * 
     * @param object  $form Formumaire
     * @param integer $maj  Mode d'insertion
     */
    function setOnChange(&$form, $maj) {
        parent::setOnChange($form, $maj);
        //
        $form->setOnChange('autorite_police_decision',
            'get_autorite_police_decision_params(this.value);');
    }

    /**
     * Permet de définir les valeurs des champs en sous-formualire.
     *
     * @param object  $form             Instance du formulaire.
     * @param integer $maj              Mode du formulaire.
     * @param integer $validation       Validation du form.
     * @param integer $idxformulaire    Identifiant de l'objet parent.
     * @param string  $retourformulaire Objet du formulaire.
     * @param mixed   $typeformulaire   Type du formulaire.
     * @param null    $dnu1 @deprecated Ancienne ressource de base de données.
     * @param null    $dnu2 @deprecated Ancien marqueur de débogage.
     */
    function setValsousformulaire(&$form, $maj, $validation, $idxformulaire, $retourformulaire, $typeformulaire, &$dnu1 = null, $dnu2 = null) {
        //
        parent::setValsousformulaire($form, $maj, $validation, $idxformulaire, $retourformulaire, $typeformulaire, $dnu1, $dnu2);
        //
        if($validation == 0 & $maj == 0) {
            // Instance de la classe dossier_instruction_reunion
            $dossier_instruction_reunion = $this->f->get_inst__om_dbform(array(
                "obj" => "dossier_instruction_reunion",
                "idx" => $idxformulaire,
            ));
            //
            $form->setVal('date_decision', $dossier_instruction_reunion->get_reunion_date_reunion());
            //
            $form->setVal('dossier_coordination', $dossier_instruction_reunion->get_dossier_instruction_dossier_coordination());
            //
            $form->setVal('etablissement', $dossier_instruction_reunion->get_dossier_instruction_dossier_coordination_etablissement());
            //
            $form->setVal('service', $dossier_instruction_reunion->get_dossier_instruction_service($dossier_instruction_reunion->getVal('dossier_instruction')));
            // Le générateur positionne par défaut une valeur dans ce champ
            if($this->is_in_context_of_foreign_key('dossier_instruction_reunion', $this->retourformulaire))
                $form->setVal('dossier_instruction_reunion_prochain', '');
        }// fin validation
    }

    /**
     * Initialisation des actions supplémentaires de la classe.
     */
    function init_class_actions() {
        parent::init_class_actions();
        // ACTION - 004 - autorite_police_decision_params
        // Récupère les valeurs de la décision de l'autorité de police
        $this->class_actions[4] = array(
            "identifier" => "autorite_police_decision_params",
            "view" => "get_autorite_police_decision_params",
            "permission_suffix" => "json",
        );
        // ACTION - 005 - display_synthesis
        // Affiche une synthése de l'enregistrement
        $this->class_actions[5] = array(
            "identifier" => "display_synthesis",
            "view" => "view_synthesis",
            "permission_suffix" => "consulter_resume",
        );
        // ACTION - 006 - delete_autorite_police_on_dossier_instruction_reunion
        // Supprime l'enregistrement et renvoie un booléen
        $this->class_actions[6] = array(
            "identifier" => "delete_autorite_police_on_dossier_instruction_reunion",
            "view" => "delete_autorite_police_on_dossier_instruction_reunion",
        );
        // ACTION - 007 - cloturer
        // Clôture l'enregistrement et supprime les demandes de passage en
        // réunion liées qui ne sont pas planifiées
        $this->class_actions[7] = array(
            "identifier" => "cloturer",
            "portlet" => array(
                "type" => "action-direct",
                "libelle" => __("cloturer"),
                "order" => 100,
                "class" => "lock-16",
            ),
            "view" => "formulaire",
            "method" => "lock",
            "button" => "cloturer",
            "permission_suffix" => "cloturer",
            "condition" => "is_lockable",
        );
        // ACTION - 008 - decloturer
        // Réouvre l'enregistrement
        $this->class_actions[8] = array(
            "identifier" => "decloturer",
            "portlet" => array(
                "type" => "action-direct",
                "libelle" => __("reouvrir"),
                "order" => 110,
                "class" => "unlock-16",
            ),
            "view" => "formulaire",
            "method" => "unlock",
            "button" => "decloturer",
            "permission_suffix" => "decloturer",
            "condition" => "is_unlockable",
        );
    }

    /**
     * Indique si la redirection vers le lien de retour est activée ou non.
     *
     * L'objectif de cette méthode est de permettre d'activer ou de désactiver
     * la redirection dans certains contextes.
     *
     * @return boolean
     */
    function is_back_link_redirect_activated() {
        return false;
    }

    /**
     * CONDITION - is_lockable.
     * 
     * Condition pour afficher le bouton cloturer
     *
     * @return boolean
     */
    function is_lockable() {
        // Récupère la valeur du champ cloture
        $cloture = $this->getVal('cloture');

        // Si cloture est false
        if ($cloture == 'f') {
            // Continue le traitement
            return true;
        }

        // Stop le traitement
        return false;
    }

    /**
     * CONDITION - is_unlockable.
     * 
     * Condition pour afficher le bouton decloturer
     *
     * @return boolean
     */
    function is_unlockable() {
        // Récupère la valeur du champ cloture
        $cloture = $this->getVal('cloture');

        // Si cloture est true
        if ($cloture == 't') {
            // Continue le traitement
            return true;
        }

        // Stop le traitement
        return false;
    }

    /**
     * TREATMENT - lock.
     *
     * Clôture l'autorité de police.
     *
     * @param array $val Valeurs soumises par le formulaire
     *
     * @return boolean
     */
    function lock($val = array()) {
        // Begin
        $this->begin_treatment(__METHOD__);

        // On clôture l'autorité de police
        $ret = $this->manage_locking("lock", $val);
        // Si le traitement ne s'est pas déroulé correctement
        if ($ret !== true) {
            // Return
            return $this->end_treatment(__METHOD__, false);
        }

        // On supprime les demandes de passage en réunion liées qui ne sont
        // pas planifiées
        $ret = $this->delete_dossier_instruction_reunion_not_planned();
        // Si le traitement ne s'est pas déroulé correctement
        if ($ret !== true) {
            // Return
            return $this->end_treatment(__METHOD__, false);
        }

        // Return
        return $this->end_treatment(__METHOD__, true);
    }

    /**
     * TREATMENT - unlock.
     *
     * Réouvre l'autorité de police.
     *
     * @param array $val Valeurs soumises par le formulaire
     *
     * @return boolean
     */
    function unlock($val = array()) {
        // Begin
        $this->begin_treatment(__METHOD__);

        // On réouvre l'autorité de police.
        $ret = $this->manage_locking("unlock", $val);
        // Si le traitement ne s'est pas déroulé correctement
        if ($ret !== true) {
            // Return
            return $this->end_treatment(__METHOD__, false);
        }

        // Return
        return $this->end_treatment(__METHOD__, true);
    }

    /**
     * Effectue le traitement de clôturation et de réouverture.
     *
     * @param string $mode lock/unlock
     * @param array  $val  valeurs du formulaire
     *
     * @return boolean
     */
    function manage_locking($mode = null, $val = array()) {
        // Logger
        $this->addToLog(__METHOD__."() - begin", EXTRA_VERBOSE_MODE);
        // Si le mode n'existe pas on retourne false
        if ($mode != "lock" && $mode != "unlock") {
            return false;
        }
        // Récuperation de la valeur de la cle primaire de l'objet
        $id = $this->getVal($this->clePrimaire);
        // 
        if ($mode == "lock") {
            $valF = array(
                "cloture" => 't',
            );
            $valid_message = __("L'%s a ete correctement cloturee.");
        }
        if ($mode == "unlock") {
            $valF = array(
                "cloture" => 'f',
            );
            $valid_message = __("L'%s a ete correctement reouverte.");
        }
        //
        $this->correct = true;
        // Execution de la requête de modification des donnees de l'attribut
        // valF de l'objet dans l'attribut table de l'objet
        $res = $this->f->db->autoExecute(
            DB_PREFIXE.$this->table,
            $valF,
            DB_AUTOQUERY_UPDATE,
            $this->getCle($id)
        );
        // Si une erreur survient
        if (database::isError($res, true)) {
            // Appel de la methode de recuperation des erreurs
            $this->erreur_db($res->getDebugInfo(), $res->getMessage(), '');
            $this->correct = false;
            return false;
        } else {
            //
            $valid_message = sprintf($valid_message, __("autorite_police"));
            //
            $main_res_affected_rows = $this->f->db->affectedRows();
            // Log
            $this->addToLog(__("Requete executee"), VERBOSE_MODE);
            // Log
            $message = __("Enregistrement")."&nbsp;".$id."&nbsp;";
            $message .= __("de la table")."&nbsp;\"".$this->table."\"&nbsp;";
            $message .= "[&nbsp;".$main_res_affected_rows."&nbsp;";
            $message .= __("enregistrement(s) mis a jour")."&nbsp;]";
            $this->addToLog($valid_message, VERBOSE_MODE);
            // Message de validation
            if ($main_res_affected_rows == 0) {
                $this->addToMessage(__("Attention vous n'avez fait aucune modification.")."<br/>");
            } else {
                $this->addToMessage($valid_message."<br/>");
            }
        }
        // Logger
        $this->addToLog(__METHOD__."() - end", EXTRA_VERBOSE_MODE);
        //
        return true;
    }

    /**
     * VIEW - get_autorite_police_decision_params.
     *
     * Permet de récupérer et d'afficher les données de paramétrage
     * du type du dossier de coordination.
     *
     * return void
     */
    function get_autorite_police_decision_params() {
        // Valeurs postées
        $postvar = $this->getParameter("postvar");
        // Désactive les logs
        $this->f->disableLog();
        // Initialisation de la variable de retour
        $return = "";
        // Si la valeur autorite_police_decision_selected est envoyé
        if (!empty($postvar['autorite_police_decision_selected'])) {
            // Instance de la classe autorite_police_decision
            $autorite_police_decision = $this->f->get_inst__om_dbform(array(
                "obj" => "autorite_police_decision",
                "idx" => $postvar['autorite_police_decision_selected'],
            ));
            // Récupère la valeur du champ "delai"
            $return = $autorite_police_decision->getVal('delai');
        }
        // Affiche tableau json
        echo json_encode($return);
    }

    /**
     * VIEW - delete_autorite_police_on_dossier_instruction_reunion.
     *
     * Supprime l'enregistrement est envoie en json le résultat.
     *
     * @return void
     */
    function delete_autorite_police_on_dossier_instruction_reunion() {
        // Désactive les logs
        $this->f->disableLog();
        // Récupère l'identifiant
        $val[$this->clePrimaire] = $this->getVal($this->clePrimaire);
        // Suppression
        $delete = $this->supprimer($val);
        // Retourne le résultat de la suppression
        echo json_encode($delete);
    }

    /**
     * Permet d'ajouter un courrier lié à l'autorisation de police.
     *
     * @param array $data Données de l'enregistrement
     *
     * @return boolean
     */
    function add_courrier_lie($data) {
        // Instance de la classe lien_autorite_police_courrier
        $lien_autorite_police_courrier = $this->f->get_inst__om_dbform(array(
            "obj" => "lien_autorite_police_courrier",
            "idx" => "]",
        ));
        // initialisation de la clé primaire
        $val['lien_autorite_police_courrier'] = "";
        // Si les données sont dans un tableau
        if (is_array($data)) {
            //
            foreach ($data as $key => $value) {
                //
                $val[$key] = $value;
            }
        }
        // Ajoute l'enregistrement
        $add = $lien_autorite_police_courrier->ajouter($val);
        //
        return $add;
    }

    /**
     * Permet de supprimer un courrier lié à l'autorisation de police.
     *
     * @param integer $autorite_police Identifiant de l'autorité de police
     *
     * @return boolean
     */
    function delete_courrier_lie($autorite_police) {
        // Instance de la classe lien_autorite_police_courrier
        $lien_autorite_police_courrier = $this->f->get_inst__om_dbform(array(
            "obj" => "lien_autorite_police_courrier",
            "idx" => 0,
        ));
        // Supprime l'enregistrement
        $delete = $lien_autorite_police_courrier->delete_by_autorite_police($autorite_police);
        //
        return $delete;
    }

    /**
     * Ajouter une liaison NàN entre deux tables.
     *
     * @param string $table_l Table de liaison
     * @param string $table_f Table cible
     * @param string $field   Champ de la table à liée
     *
     * @return integer Nombre de lien crée
     */
    function ajouter_liaisons_table_nan($table_l, $table_f, $field, $values=null) {
        //
        $multiple_values = array();
        // Récupération des données du select multiple
        $postvar = $this->getParameter("postvar");
        if (isset($postvar[$field])
            && is_array($postvar[$field])) {
            $multiple_values = $postvar[$field];
        } elseif ($values != null) {
            //
            $multiple_values = $values;
            // Si ce n'est pas un tableau
            if (!is_array($values)) {
                //
                $multiple_values = explode(";", $multiple_values);
            }
        }

        // Ajout des liaisons
        $nb_liens = 0;
        // Boucle sur la liste des valeurs sélectionnées
        foreach ($multiple_values as $value) {
            // Test si la valeur par défaut est sélectionnée
            if ($value == "") {
                continue;
            }
            // On compose les données de l'enregistrement
            $donnees = array(
                $this->clePrimaire => $this->valF[$this->clePrimaire],
                $table_f => $value,
                $table_l => "",
            );
            // On ajoute l'enregistrement
            $obj_l = $this->f->get_inst__om_dbform(array(
                "obj" => $table_l,
                "idx" => "]",
            ));
            $obj_l->ajouter($donnees);
            // On compte le nombre d'éléments ajoutés
            $nb_liens++;
        }
        //
        return $nb_liens;
    }

    /**
     * [supprimer_liaisons_table_nan description]
     *
     * @param [type] $table [description]
     *
     * @return [type] [description]
     */
    function supprimer_liaisons_table_nan($table) {
        // Suppression de tous les enregistrements correspondants à l'id 
        // de l'objet instancié en cours dans la table NaN
        $sql = "DELETE FROM ".DB_PREFIXE.$table." WHERE ".$this->clePrimaire."=".$this->getVal($this->clePrimaire);
        $res = $this->f->db->query($sql);
        $this->f->addToLog(__METHOD__."(): db->query(\"".$sql."\");", VERBOSE_MODE);
        if (database::isError($res)) {
            die();
        }
    }

    /**
     * Surcharge de la méthode rechercheTable pour éviter de court-circuiter le 
     * générateur en devant surcharger la méthode cleSecondaire afin de supprimer
     * les éléments liés dans les tables NaN.
     *
     * @param [type] $dnu1      [description]
     * @param [type] $table     [description]
     * @param [type] $field     [description]
     * @param [type] $id        [description]
     * @param [type] $dnu2      [description]
     * @param string $selection [description]
     *
     * @return [type] [description]
     */
    function rechercheTable(&$dnu1 = null,  $table = "", $field = "", $id = null, $dnu2 = null, $selection = "") {
        //
        if (in_array($table, array_keys($this->liaisons_nan))) {
            //
            $this->addToLog(__METHOD__."(): On ne vérifie pas la table ".$table." car liaison nan.", EXTRA_VERBOSE_MODE);
            return;
        }
        //
        parent::rechercheTable($this->f->db, $table, $field, $id, null, $selection);
    }

    /**
     * Change la valeur du champ autorite_police_encours de la table 
     * dossier_coordination.
     *
     * @param integer $id    Identifiant du dossier de coordination
     * @param boolean $value Valeur à modifier
     *
     * @return boolean
     */
    function update_dossier_coordination_autorite_police_encours($id, $value) {
        // Instance de la classe dossier_coordination
        $dossier_coordination = $this->f->get_inst__om_dbform(array(
            "obj" => "dossier_coordination",
            "idx" => $id,
        ));
        // Met à jour seulement si la valeur n'est pas identique
        if ($this->f->get_val_boolean($dossier_coordination->getVal('autorite_police_encours')) != $value){
            // Mise à jour de l'autorité de police en cours
            $update = $dossier_coordination->update_autorite_police_encours($value);
            //
            if ($update == false) {
                return false;
            }
        }
        //
        return true;
    }

    /**
     * Met à jour les dates de suivi.
     *
     * @param  integer $id    Identifiant de l'enregistrmeent
     * @param  string  $value Valeur de mise à jour
     *
     * @return boolean
     */
    function update_track_date($id, $value) {
        // Logger
        $this->addToLog(__METHOD__."() - begin", EXTRA_VERBOSE_MODE);

        // Traitement en amont
        $trigger = $this->trigger_update_track_date($id, $value);

        // Si le traitement en amont a échoué
        if ($trigger === false) {
            // Stop le traitement
            return false;
        }

        // Valeurs de mise à jour
        $valF = array(
            "date_notification" => $value,
        );

        // Vérifie que tous les courriers liés sont notifiés
        $check_courriers_notified = $this->has_all_courriers_notified($id);

        // Si tous les courriers liés sont notifiés
        if ($check_courriers_notified == true) {
            // On calule la date_butoir en fonction de la date de notification + délai
            $inst_date_notification = new DateTime($value);
            $inst_date_notification->add(new DateInterval(sprintf('P%sD', intval($this->getVal("delai")))));
            $valF["date_butoir"] = $inst_date_notification->format('Y-m-d');

            // Créé une nouvelle demande de passage en réunion
            $add_dossier_instruction_reunion_prochain = $this->add_dossier_instruction_reunion_prochain($valF["date_butoir"]);

            // Si la mise à jour a échouée
            if ($add_dossier_instruction_reunion_prochain === false) {
                // Stop le traitement
                return false;
            }

            // Met à jour la demande de passage suivante
            $valF["dossier_instruction_reunion_prochain"] = $add_dossier_instruction_reunion_prochain;
        }

        // Met à jour les données
        $update = $this->update_autoexecute($valF, $id);

        // Si la mise à jour a échouée
        if ($update === false) {
            // Stop le traitement
            return false;
        }

        // Traitement en aval
        $trigger_after = $this->trigger_after_update_track_date($id, $value);

        // Si le traitement en aval a échoué
        if ($trigger_after === false) {
            // Stop le traitement
            return false;
        }

        // Logger
        $this->addToLog(__METHOD__."() - end", EXTRA_VERBOSE_MODE);

        // Continue le traitement
        return true;
    }

    /**
     * Permet d’effectuer des actions avant le suivi des dates.
     *
     * @param integer $id    Identifiant de l'AP
     * @param string  $value Valeur de la date butoir
     *
     * @return boolean
     */
    function trigger_update_track_date($id, $value) {
        //
        return true;
    }

    /**
     * Permet d’effectuer des actions après le suivi des dates.
     *
     * @param integer $id    Identifiant de l'AP
     * @param string  $value Valeur de la date butoir
     *
     * @return boolean
     */
    function trigger_after_update_track_date($id, $value) {
        //
        return true;
    }

    /**
     * Ajoute la prochaine demande de passage de réunion.
     *
     * @param string $value Date souhaitée
     *
     * @return mixed False en cas d'erreur ou l'identifiant de l'ajout
     */
    function add_dossier_instruction_reunion_prochain($value) {
        // Logger
        $this->addToLog(__METHOD__."() - begin", EXTRA_VERBOSE_MODE);
        // Instance de la demande de passage
        $dossier_instruction_reunion = $this->f->get_inst__om_dbform(array(
            "obj" => "dossier_instruction_reunion",
            "idx" => $this->getVal("dossier_instruction_reunion"),
        ));

        // Récupère les données de la demande de passage
        foreach ($dossier_instruction_reunion->champs as $champ) {
            $valF[$champ] = $dossier_instruction_reunion->getVal($champ);
        }
        // Valeurs de la prochaine demande de passage
        $valF["dossier_instruction_reunion"] = "";
        $valF["date_souhaitee"] = $this->dateDBToForm($value);
        $valF["reunion"] = "";
        $valF["ordre"] = "";
        $valF["avis"] = "";
        $valF["avis_complement"] = "";
        $valF["avis_motivation"] = "";

        // On ajoute la prochaine demande de passage en réunion
	    $dossier_instruction_reunion_prochain = $this->f->get_inst__om_dbform(array(
            "obj" => "dossier_instruction_reunion",
            "idx" => "]",
        ));
        $add = $dossier_instruction_reunion_prochain->ajouter($valF);

        // Si l'ajout échoue
        if ($add === false) {
            // Stop le traitement
            return false;
        }

        // Logger
        $this->addToLog(__METHOD__."() - end", EXTRA_VERBOSE_MODE);

        // Continue le traitement
        return $dossier_instruction_reunion_prochain->valF[$dossier_instruction_reunion_prochain->clePrimaire];
    }

    /**
     * Met à jour l'établissement.
     *
     * @param integer $etablissement            Identifiant de l'établissement
     * @param integer $autorite_police_decision Identifiant de l'AP
     *
     * @return boolean
     */
    function update_etablissement($etablissement, $autorite_police_decision) {
        // Logger
        $this->addToLog(__METHOD__."() - begin", EXTRA_VERBOSE_MODE);

        // Instance de la classe autorite_police_decision
        $autorite_police_decision = $this->f->get_inst__om_dbform(array(
            "obj" => "autorite_police_decision",
            "idx" => $autorite_police_decision,
        ));

        // Si l'autorité de police doit modifier l'état de l'établissement
        if ($autorite_police_decision->getVal("etablissement_etat") != "") {
            // Instance de la classe établissement
            $etablissement = $this->f->get_inst__om_dbform(array(
                "obj" => "etablissement",
                "idx" => $etablissement,
            ));

            // Met à jour l'état de l'établissement
            $update_etat = $etablissement->update_etat($autorite_police_decision->getVal("etablissement_etat"));

            // Si la modification de l'état échoue
            if ($update_etat === false) {
                // Stop le traitement
                return false;
            }

            // Vérifie que l'établissement soit un ERP référentiel
            if ($etablissement->get_etablissement_nature_code_by_etablissement($etablissement->getVal($etablissement->clePrimaire)) != strtolower($this->f->getParameter("etablissement_nature_erpr"))) {

                // Instance de la classe établissement
                $etablissement_nature = $this->f->get_inst__om_dbform(array(
                    "obj" => "etablissement_nature",
                    "idx" => 0,
                ));

                // Met à jour la nature de l'établissement en ERP référentiel
                $update_nature = $etablissement->update_nature($etablissement_nature->get_etablissement_nature_by_code(strtolower($this->f->getParameter("etablissement_nature_erpr"))));

                // Si la modification de la nature échoue
                if ($update_nature === false) {
                    // Stop le traitement
                    return false;
                }
            }

            // Instance de la classe établissement
            $etablissement_etat = $this->f->get_inst__om_dbform(array(
                "obj" => "etablissement_etat",
                "idx" => $autorite_police_decision->getVal("etablissement_etat"),
            ));

            // Si l'établissement est ouvert
            if ($etablissement_etat->get_etablissement_etat_code() == 'ouve') {

                // Met à jour la date d'ouverture de l'ERP
                $update_date_arrete_ouverture = $etablissement->update_date_arrete_ouverture(date("Y-m-d"));

                // Si la modification de la date d'ouverture échoue
                if ($update_date_arrete_ouverture === false) {
                    // Stop le traitement
                    return false;
                }
            }
        }

        // Logger
        $this->addToLog(__METHOD__."() - end", EXTRA_VERBOSE_MODE);

        // Continue le traitement
        return true;
    }

    /**
     * Retourne TRUE si l'autorité de police à un suivi de délai sinon FALSE.
     *
     * @param integer $id Identifiant de la décision d'autorité de police
     *
     * @return boolean
     */
    function get_autorite_police_decision_suivi_delai($id) {
        // Instance de la classe autorite_police_decision
        $autorite_police_decision = $this->f->get_inst__om_dbform(array(
            "obj" => "autorite_police_decision",
            "idx" => $id,
        ));
        // Retourne la valeur
        return $this->f->get_val_boolean($autorite_police_decision->getVal('suivi_delai'));
    }

    /**
     * Traitement à réaliser sur les dossiers de coordination.
     *
     * @return boolean
     */
    function trigger_dossier_coordination_autorite_police_encours($id) {
        // Initialisation de la variable de mise à jour du DC
        $update_dc = true;

        // Si l'autorité de police n'est pas clôturée et qu'il y a un suivi de
        // délai
        if ($this->f->get_val_boolean($this->valF['cloture']) == false
            && $this->get_autorite_police_decision_suivi_delai(
                $this->valF['autorite_police_decision']) == true) {
            // Si l'autorité de police est lié à un dossier de coordination
            if (!empty($this->valF['dossier_coordination'])) {
                // Met à jour le champ du dc
                $update_dc = $this->update_dossier_coordination_autorite_police_encours($this->valF['dossier_coordination'], true);
            }
        }

        // Si l'autorité de police est clôturée et qu'aucune autre est encours
        if ($this->f->get_val_boolean($this->valF['cloture']) == true) {
            // Si l'autorité de police est lié à un dossier de coordination
            if (!empty($this->valF['dossier_coordination'])) {
                // Récupère les autres autorités de police du même dossier de 
                // coordination
                $list_autorite_police = $this->get_list_autorite_police_by_dossier_coordination($id, $this->valF['dossier_coordination']);
                // Vérifie les autres autorités de police
                if ($this->is_autorite_police_encours($list_autorite_police) == false) {
                    // Met à jour le champ du dc
                    $update_dc = $this->update_dossier_coordination_autorite_police_encours($this->valF['dossier_coordination'], false);
                }
            }
        }

        // Stop le traitement si la modification échoue
        if ($update_dc == false) {
            return false;
        }

        //
        return true;
    }

    /**
     * Récupère la liste des autorités de police sur un dossier de coordination.
     *
     * @param integer $autorite_police      Identifiant de l'AP
     * @param integer $dossier_coordination Identifiant du DC
     *
     * @return array
     */
    function get_list_autorite_police_by_dossier_coordination($autorite_police, $dossier_coordination) {
        // Initialisation du résultat
        $result = array();
        // S'il y a un établissement
        if (!empty($dossier_coordination)) {
            // Requête SQL
            $sql = "SELECT autorite_police, cloture, autorite_police_decision
                    FROM ".DB_PREFIXE."autorite_police
                    WHERE autorite_police NOT IN (".intval($autorite_police).")
                    AND dossier_coordination = ".intval($dossier_coordination);
            $this->f->addToLog(__METHOD__."() : db->query(\"".$sql."\")", VERBOSE_MODE);
            $res = $this->f->db->query($sql);
            $this->f->isDatabaseError($res);
            //
            while ($row =& $res->fetchRow(DB_FETCHMODE_ASSOC)) {
                //
                $result[] = $row;
            }
        }
        // Retourne le résultat
        return $result;
    }

    /**
     * Récupère la liste des courriers liés.
     *
     * @param integer $id Identifiant de l'AP
     *
     * @return array
     */
    function get_list_courriers_lies($id) {
        // Initialisation de la variable de résultat
        $list_courrier = array();

        // Si l'identifiant n'est pas vide
        if (!empty($id)) {

            // Requête SQL
            $sql = "SELECT courrier
                    FROM ".DB_PREFIXE."lien_autorite_police_courrier
                    WHERE autorite_police = ".intval($id);
            $this->f->addToLog(__METHOD__."() : db->query(\"".$sql."\")", VERBOSE_MODE);
            $res = $this->f->db->query($sql);
            $this->f->isDatabaseError($res);

            // Récupère les résultats dans un tableau
            while ($row =& $res->fetchRow(DB_FETCHMODE_ASSOC)) {
                //
                $list_courrier[] = $row['courrier'];
            }
        }

        // Retourne le résultat
        return $list_courrier;
    }

    /**
     * Vérifie que les dossiers de coordination n'ont pas d'autorité de police
     * en cours.
     *
     * @param array $list_autorite_police Liste des autorités de police
     *
     * @return boolean
     */
    function is_autorite_police_encours($list_autorite_police) {
        // Pour chaque autorité de police
        foreach ($list_autorite_police as $key => $value) {
            // Si le champ "autorite_police_encours" est à true
            if ($this->f->get_val_boolean($value['cloture']) == false
                && $this->get_autorite_police_decision_suivi_delai($value['autorite_police_decision']) == true) {
                //
                return true;
            }
        }
        //
        return false;
    }

    /**
     * Vérifie que tous les courriers liés sont notifiés.
     *
     * @param integer $id Identifiant de l'AP
     *
     * @return boolean
     */
    function has_all_courriers_notified($id) {
        // Logger
        $this->addToLog(__METHOD__."() - begin", EXTRA_VERBOSE_MODE);

        // Récupère la liste des courriers liés
        $list_courriers_lies = $this->get_list_courriers_lies($id);

        // Pour chaque courrier
        foreach ($list_courriers_lies as $courrier_id) {

            // Instance de la classe courrier
            $courrier = $this->f->get_inst__om_dbform(array(
                "obj" => "courrier",
                "idx" => $courrier_id,
            ));

            // Vérifie que la date de retour ar n'est pas vide
            $check_date_retour_rar = $courrier->has_date_retour_rar_not_empty();

            // Si la date de retour ar est vide
            if ($check_date_retour_rar === false) {
                //
                return false;
            }
        }

        // Logger
        $this->addToLog(__METHOD__."() - end", EXTRA_VERBOSE_MODE);

        //
        return true;
    }

    /**
     * Supprime les demandes de passage en réunion liées à un autorité de police
     * qui ne sont pas planifiées.
     *
     * @param mixed $id Identifiant de l'AP
     *
     * @return boolean
     */
    function delete_dossier_instruction_reunion_not_planned($id = null) {
        // 
        $autorite_police = $this;
        // Si l'identifiant n'est pas vide
        if (!empty($id)) {
            $autorite_police = $this->f->get_inst__om_dbform(array(
                "obj" => "autorite_police",
                "idx" => $id,
            ));
        }

        // Récupère la demande de passage principale
        $dossier_instruction_reunion_id = $autorite_police->getVal("dossier_instruction_reunion");

        // Si la demande de passage en réunion n'est pas vide
        if (!empty($dossier_instruction_reunion_id)) {

            // Instance de la demande de passage en réunion
            $dossier_instruction_reunion = $this->f->get_inst__om_dbform(array(
                "obj" => "dossier_instruction_reunion",
                "idx" => $dossier_instruction_reunion_id,
            ));
            // Vérifie que la demande n'est pas planifiée
            if ($dossier_instruction_reunion->is_not_planned()) {
                // Supprime la demande de passage de l'AP
                $valF["dossier_instruction_reunion_"] = null;
                $autorite_police->update_autoexecute($valF, $autorite_police->getVal($autorite_police->clePrimaire));

                // Supprime l'enregistrement de la demande de passage
                $val[$dossier_instruction_reunion->clePrimaire] = $dossier_instruction_reunion->getVal($dossier_instruction_reunion->clePrimaire);
                $delete = $dossier_instruction_reunion->supprimer($val);

                // Si la suppression échoue
                if ($delete == false) {
                    // Stop le traitement
                    return false;
                }
            }
        }

        // Récupère la demande de passage prochaine
        $dossier_instruction_reunion_prochain_id = $autorite_police->getVal("dossier_instruction_reunion_prochain");

        // Si la prochaine demande de passage en réunion n'est pas vide
        if (!empty($dossier_instruction_reunion_prochain_id)) {

            // Instance de la prochaine demande de passage en réunion
            $dossier_instruction_reunion_prochain = $this->f->get_inst__om_dbform(array(
                "obj" => "dossier_instruction_reunion",
                "idx" => $dossier_instruction_reunion_prochain_id,
            ));
            // Vérifie que la demande n'est pas planifiée
            if ($dossier_instruction_reunion_prochain->is_not_planned()) {
                // Supprime la prochaine demande de passage de l'AP
                $valF["dossier_instruction_reunion_prochain"] = null;
                $autorite_police->update_autoexecute($valF, $autorite_police->getVal($autorite_police->clePrimaire));

                // Supprime l'enregistrement de la prochaine demande de passage
                $val[$dossier_instruction_reunion_prochain->clePrimaire] = $dossier_instruction_reunion_prochain->getVal($dossier_instruction_reunion_prochain->clePrimaire);
                $delete = $dossier_instruction_reunion_prochain->supprimer($val);

                // Si la suppression échoue
                if ($delete == false) {
                    // Stop le traitement
                    return false;
                }
            }
        }

        // Continue le traitement
        return true;
    }

    /**
     * Fonction générique permettant de récupérer les données d'un champ postées.
     *
     * @param string $champ Nom du champ
     *
     * @return mixed Valeur posté
     */
    function getPostedValues($champ) {
        // Récupération des demandeurs dans POST
        if (isset($_POST[$champ]) ) {
            //
            return $_POST[$champ];
        }
    }

    /**
     * TRIGGER - triggerajouterapres.
     *
     * - ...
     *
     * @return boolean
     */
    function triggerajouterapres($id, &$dnu1 = null, $val = array(), $dnu2 = null) {
        // Liaison NaN
        foreach ($this->liaisons_nan as $liaison_nan) {
            // Ajout des liaisons table Nan
            $nb_liens = $this->ajouter_liaisons_table_nan(
                $liaison_nan["table_l"], 
                $liaison_nan["table_f"], 
                $liaison_nan["field"],
                $val[$liaison_nan["field"]]
            );
            // Message de confirmation
            if ($nb_liens > 0) {
                if ($nb_liens == 1 ){
                    $this->addToMessage(sprintf(__("Creation d'une nouvelle liaison realisee avec succes.")));
                } else {
                    $this->addToMessage(sprintf(__("Creation de %s nouvelles liaisons realisee avec succes."), $nb_liens));
                }
            }
        }

        // Traitement à réaliser pour les dossiers de coordination
        $trigger_dc_ap_encours = $this->trigger_dossier_coordination_autorite_police_encours($id);
        //
        if ($trigger_dc_ap_encours == false) {
            return false;
        }

        //
        return true;
    }

    /**
     * TRIGGER - triggermodifierapres.
     *
     * - ...
     *
     * @return boolean
     */
    function triggermodifierapres($id, &$dnu1 = null, $val = array(), $dnu2 = null) {
        // Liaisons NaN
        foreach ($this->liaisons_nan as $liaison_nan) {
            // Suppression des liaisons table NaN
            $this->supprimer_liaisons_table_nan($liaison_nan["table_l"]);
            // Ajout des liaisons table Nan
            $nb_liens = $this->ajouter_liaisons_table_nan(
                $liaison_nan["table_l"], 
                $liaison_nan["table_f"], 
                $liaison_nan["field"],
                $val[$liaison_nan["field"]]
            );
            // Message de confirmation
            if ($nb_liens > 0) {
                $this->addToMessage(__("Mise a jour des liaisons realisee avec succes."));
            }
        }

        // Traitement à réaliser pour les dossiers de coordination
        $trigger_dc_ap_encours = $this->trigger_dossier_coordination_autorite_police_encours($id);
        //
        if ($trigger_dc_ap_encours == false) {
            return false;
        }

        //
        return true;
    }

    /**
     * Permet d’effectuer des actions avant la modification des données dans la
     * base.
     *
     * @param integer $id    Identifiant de l'objet
     *
     * @return boolean
     */
    function triggersupprimer($id, &$dnu1 = null, $val = array(), $dnu2 = null) {
        //
        parent::triggersupprimer($id, $dnu1, $val, $dnu2);

        // Liaisons NaN
        foreach ($this->liaisons_nan as $liaison_nan) {
            // Suppression des liaisons table NaN
            $this->supprimer_liaisons_table_nan($liaison_nan["table_l"]);
        }

        //
        return true;
    }

    /**
     * Permet d’effectuer des actions après la modification des données dans la
     * base.
     *
     * @param integer $id Identifiant de l'objet
     *
     * @return boolean
     */
    function triggersupprimerapres($id, &$dnu1 = null, $val = array(), $dnu2 = null) {
        //
        parent::triggersupprimerapres($id, $dnu1, $val, $dnu2);

        // Si l'autorité de police est lié à un dossier de coordination
        $dossier_coordination = $this->getVal('dossier_coordination');
        if (!empty($dossier_coordination)) {
            // Récupère les autres autorités de police du même dossier de 
            // coordination
            $list_autorite_police = $this->get_list_autorite_police_by_dossier_coordination($id, $dossier_coordination);
            // Vérifie les autres autorités de police
            if ($this->is_autorite_police_encours($list_autorite_police) == false) {
                // Met à jour le champ du dc
                $update_dc = $this->update_dossier_coordination_autorite_police_encours($dossier_coordination, false);
                // Stop le traitement si la modification échoue
                if ($update_dc == false) {
                    return false;
                }
            }
        }

        //
        return true;
    }

    /**
     * VIEW - view_synthesis.
     *
     * @return void
     */
    function view_synthesis() {
      // Logger
      $this->addToLog(__METHOD__."() - begin", EXTRA_VERBOSE_MODE);
      // Désactive les logs
      $this->f->disableLog();
      // Vérification de l'accessibilité sur l'élément
      $this->checkAccessibility();
      // Affichage de la synthèse
      $this->display_synthesis();
      // Logger
      $this->addToLog(__METHOD__."() - end", EXTRA_VERBOSE_MODE);
    }

    /**
     * Synthèse de l'autorité de police. Affichage résumé de l'enregistrement.
     *
     * @return void
     */
    function display_synthesis() {
        // Identifiant de l'enregistrement
        $id = $this->getVal($this->clePrimaire);

        // Conteneur de l'autorité de police
        printf("<div class=\"autorite_police col_3\" id=\"autorite_police_".$id."\">\n");
        // Légende
        printf("<div class=\"legend_synthesis_autorite_police\">\n");

        // Bouton supprimer
        if ($this->f->isAccredited(array($this->table."_supprimer", $this->table, ), "OR")) {
            //
            $delete_button = "<a href=\"#\" onclick=\"delete_autorite_police(".$id.", '".__("Confirmation de suppression")."'); return false;\">";
            $delete_button .= "<span class=\"delete_autorite_police om-icon om-icon-16 om-icon-fix delete-16\"";
            $delete_button .= "title=\"".__("Supprimer l'autorite de police")."\">".__("Supprimer l'autorite de police")."</span>";
            $delete_button .= "</a>";
            $delete_button .= "<div id=\"dialog-confirm\">".__("Etes-vous sur de vouloir supprimer cette autorite de police ?")."</div>";

            // Si l'objet sur lequel la synthèse est affiché est une demande de
            // passage dans un contexte de la reunion
            if (isset($_GET['obj']) && isset($_GET['idx'])
                && $_GET['obj'] == 'dossier_instruction_reunion_contexte_reunion') {
                // Instancie la demande de passage
                $dossier_instruction_reunion = $this->f->get_inst__om_dbform(array(
                    "obj" => "dossier_instruction_reunion",
                    "idx" => $_GET['idx'],
                ));

                // Si la reunion est clôturée
                if ($dossier_instruction_reunion->is_reunion_not_closed() == false) {
                    // Efface le bouton de suppression
                    $delete_button = "";
                }
            }

            // Affiche le bouton de suppression
            printf($delete_button);
        }
        // Bouton modifier
        if ($this->f->isAccredited(array($this->table."_modifier", $this->table, ), "OR")) {
            //
            $edit_button = "<a href=\"#\" onclick=\"edit_autorite_police(".$id."); return false;\">";
            $edit_button .= "<span class=\"edit_autorite_police om-icon om-icon-16 om-icon-fix edit-16\"";
            $edit_button .= "title=\"".__("Modifier l'autorite de police")."\">".__("Modifier l'autorite de police")."</span>";
            $edit_button .= "</a>";

            // Si l'objet sur lequel la synthèse est affiché est une demande de
            // passage dans un contexte de la reunion
            if (isset($_GET['obj']) && isset($_GET['idx'])
                && $_GET['obj'] == 'dossier_instruction_reunion_contexte_reunion') {
                // Instancie la demande de passage
                $dossier_instruction_reunion = $this->f->get_inst__om_dbform(array(
                    "obj" => "dossier_instruction_reunion",
                    "idx" => $_GET['idx'],
                ));

                // Si la reunion est clôturée
                if ($dossier_instruction_reunion->is_reunion_not_closed() == false) {
                    // Efface le bouton de suppression
                    $edit_button = "";
                }
            }

            printf($edit_button);
        }

        //
        printf(__("AP n°%s du %s"), $id, $this->dateDBToForm($this->getVal("date_decision")));

        // Ferme la légende
        printf("</div>\n");

        //
        printf("<div class=\"synthese_autorite_police\">\n");

        // Structure pour les champs à afficher
        $field = '<div class="field">';
        $field .= '<div class="form-libelle">';
        $field .= '<label id="lib-%1$s" class="libelle-%1$s" for="synthesis-%1$s">%2$s</label>';
        $field .= '</div>';
        $field .= '<div class="form-content">';
        $field .= '<span id="synthesis-%1$s" class="field_value">%3$s</span>';
        $field .= '</div>';
        $field .= '</div>';

        // Affiche la décision
        printf($field, "autorite_police_decision", __("autorite_police_decision"), $this->get_field_from_table_by_id($this->getVal("autorite_police_decision"), "libelle", "autorite_police_decision"));
        // Affiche le délai
        printf($field, "delai", __("delai"), $this->getVal("delai"));
        // Affiche le motif
        printf($field, "autorite_police_motif", __("autorite_police_motif"), $this->get_field_from_table_by_id($this->getVal("autorite_police_motif"), "libelle", "autorite_police_motif"));
        // Affiche clôture
        printf($field, "cloture", __("cloture"), $this->get_boolean_value($this->getVal("cloture")));

        // Valeur de formulaire à retourner
        printf("<input type=\"hidden\" class=\"autorite_police_id\" value=\"".$id."\" />\n");
        //
        printf("</div>\n");

        // Ferme le conteneur de l'autorité de police
        printf("</div>\n");
    }

    /**
     * Retourne "Oui" ou "Non" par rapport à la valeur d'un booléen
     *
     * @param string $value Valeur du champ booléen
     *
     * @return string Oui/Non
     */
    function get_boolean_value($value) {

        // Retourne "Oui"
        if ($value == "t") {
            return "Oui";
        }

        // Retourne "Non"
        return "Non";
    }

    /**
     * Contenu spécifique sur le formulaire.
     *
     * @param integer $maj Mode du formulaire
     *
     * @return void
     */
    function formSpecificContent($maj) {
        //
        $id = $this->getVal($this->clePrimaire);
        // Affiche l'identifiant de l'enregistrement dans le DOM
        if(isset($this->valF[$this->clePrimaire]) AND !empty($this->valF[$this->clePrimaire])) {
            echo "<input id=\"id_retour\" name=\"id_retour\" type=\"hidden\" value=\"".
                    $this->valF[$this->clePrimaire]."\" />";
        } elseif(isset($id) AND !empty($id) AND $maj == 1) {
            echo "<input id=\"id_retour\" name=\"id_retour\" type=\"hidden\" value=\"".
                    $this->getVal($this->clePrimaire)."\" />";
        }
    }

    /**
     * Contenu spécifique sur le sous-formulaire.
     *
     * @param integer $maj Mode du formulaire
     *
     * @return void
     */
    function sousFormSpecificContent($maj) {
        //
        $this->formSpecificContent($maj);
    }

}
