<?php
/**
 * Ce script définit la classe 'etablissement_tous'.
 *
 * @package openaria
 * @version SVN : $Id$
 */

require_once "../obj/etablissement.class.php";

/**
 * Définition de la classe 'etablissement_tous' (om_dbform).
 *
 * Surcharge de la classe 'etablissement'.
 */
class etablissement_tous extends etablissement {

    /**
     *
     */
    protected $_absolute_class_name = "etablissement_tous";

}

