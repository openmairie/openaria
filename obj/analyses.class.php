<?php
/**
 * Ce script définit la classe 'analyses'.
 *
 * @package openaria
 * @version SVN : $Id$
 */

require_once "../gen/obj/analyses.class.php";

/**
 * Définition de la classe 'analyses' (om_dbform).
 */
class analyses extends analyses_gen {


    /**
     * Définition des actions disponibles sur la classe.
     * A partir de 100 les actions sont réservées à la modification
     * des blocs de données.
     *
     * @return void
     */
    function init_class_actions() {
        
        parent::init_class_actions();

        // ACTION - 001 - modifier
        // Désactivation de l'action modifier
        $this->class_actions[1] = null;

        // ACTION - 002 - supprimer
        // Désactivation de l'action supprimer
        $this->class_actions[2] = null;

        // ACTION - 003 - visualiser
        // Interface spécifique de l'action consulter
        $this->class_actions[3] = array(
            "identifier" => "visualiser",
            "view" => "visualiser",
            "permission_suffix" => "consulter",
        );

        // ACTION - 004 - finaliser
        // Pour qu'un technicien termine l'analyse
        $this->class_actions[4] = array(
            "identifier" => "finaliser",
            "portlet" => array(
                "type" => "action-direct",
                "libelle" => __("Terminer"),
                "order" => 10,
                "class" => "",
            ),
            "view" => "formulaire",
            "method" => "finaliser",
            "button" => "finaliser",
            "permission_suffix" => "finaliser",
            "condition" => array("is_from_good_service", "is_in_progress", ),
        );

        // ACTION - 005 - valider
        // Pour qu'un cadre valide l'analyse
        $this->class_actions[5] = array(
            "identifier" => "valider",
            "portlet" => array(
                "type" => "action-direct",
                "libelle" => __("Valider"),
                "order" => 20,
                "class" => "",
            ),
            "view" => "formulaire",
            "method" => "valider",
            "button" => "valider",
            "permission_suffix" => "valider",
            "condition" => array("is_from_good_service", "is_finished", ),
        );

        // ACTION - 006 - reouvrir_terminee
        // Pour réouvrir une analyse terminée
        $this->class_actions[6] = array(
            "identifier" => "reouvrir_terminee",
            "portlet" => array(
                "type" => "action-direct",
                "libelle" => __("Reouvrir"),
                "order" => 30,
                "class" => "",
            ),
            "view" => "formulaire",
            "method" => "reouvrir",
            "button" => "reouvrir",
            "permission_suffix" => "terminee_reouvrir",
            "condition" => array("is_from_good_service", "is_finished", ),
        );

        // ACTION - 023 - reouvrir_validee
        // Pour réouvrir une analyse validée
        $this->class_actions[23] = array(
            "identifier" => "reouvrir_validee",
            "portlet" => array(
                "type" => "action-direct",
                "libelle" => __("Reouvrir"),
                "order" => 31,
                "class" => "",
            ),
            "view" => "formulaire",
            "method" => "reouvrir",
            "button" => "reouvrir",
            "permission_suffix" => "validee_reouvrir",
            "condition" => array("is_from_good_service", "is_validated", ),
        );

        // ACTION - 024 - reouvrir_actee
        // Pour réouvrir une analyse actée
        $this->class_actions[24] = array(
            "identifier" => "reouvrir_actee",
            "portlet" => array(
                "type" => "action-direct",
                "libelle" => __("Reouvrir"),
                "order" => 32,
                "class" => "",
            ),
            "view" => "formulaire",
            "method" => "reouvrir",
            "button" => "reouvrir",
            "permission_suffix" => "actee_reouvrir",
            "condition" => array("is_from_good_service", "is_acted", ),
        );

        // ACTION - 021 - overlay_reglementation_applicable_type
        // Propositions d'ajout de réglementations applicables-types
        $this->class_actions[21] = array(
            "identifier" => "overlay_reglementation_applicable_type",
            "view" => "view_overlay_reglementation_applicable_type",
            "condition" => array("is_from_good_service", ),
            // Permission consulter car il s'agit d'une aide à la saisie :
            // aucun modification dans la base de données.
            "permission_suffix" => "consulter",
        );

        // ACTION - 030 - overlay_document_presente_type
        // Propositions d'ajout de documents-types
        $this->class_actions[30] = array(
            "identifier" => "overlay_document_presente_type",
            "view" => "view_overlay_document_presente_type",
            "condition" => array("is_from_good_service", ),
            // Permission consulter car il s'agit d'une aide à la saisie :
            // aucun modification dans la base de données.
            "permission_suffix" => "consulter",
        );

        // ACTION - 060 - overlay_prescription_specifique_type
        // Propositions d'ajout de prescriptions spécifiques-types
        $this->class_actions[60] = array(
            "identifier" => "overlay_prescription_specifique_type",
            "view" => "view_overlay_prescription_specifique_type",
            // Permission consulter car il s'agit d'une aide à la saisie :
            // aucun modification dans la base de données.
            "permission_suffix" => "consulter",
        );

        // ACTION - 040 - overlay_add_prescription
        // Formulaire d'ajout d'une prescription
        $this->class_actions[40] = array(
            "identifier" => "overlay_add_prescription",
            "view" => "view_overlay_add_prescription",
            "condition" => array("is_from_good_service", ),
            // Permission consulter car il s'agit d'une aide à la saisie :
            // aucun modification dans la base de données.
            "permission_suffix" => "consulter",
        );

        // ACTION - 050 - overlay_edit_prescription
        // Formulaire d'édition d'une prescription
        $this->class_actions[50] = array(
            "identifier" => "overlay_edit_prescription",
            "view" => "view_overlay_edit_prescription",
            "condition" => array("is_from_good_service", ),
            // Permission consulter car il s'agit d'une aide à la saisie :
            // aucun modification dans la base de données.
            "permission_suffix" => "consulter",
        );

        // ACTION - 070 - rapport_analyse
        // Génération du rapport d'analyse
        $this->class_actions[70] = array(
            "identifier" => "rapport_analyse",
            "portlet" => array(
                "type" => "action-blank",
                "libelle" => __("Rapport"),
                "order" => 70,
                "class" => "pdf-16",
            ),
            "view" => "view_edition",
            "permission_suffix" => "rapport_analyse",
        );

        // ACTION - 071 - compte_rendu_analyse
        // Génération du compte-rendu d'analyse
        $this->class_actions[71] = array(
            "identifier" => "compte_rendu_analyse",
            "portlet" => array(
                "type" => "action-blank",
                "libelle" => __("Compte-rendu"),
                "order" => 71,
                "class" => "pdf-16",
            ),
            "view" => "view_edition",
            "permission_suffix" => "compte_rendu_analyse",
        );

        // ACTION - 072 - proces_verbal
        // Génération du PV
        $this->class_actions[72] = array(
            "identifier" => "proces_verbal",
            "portlet" => array(
                "type" => "action-blank",
                "libelle" => __("PV (Prévisualisation)"),
                "order" => 72,
                "class" => "pdf-16",
            ),
            "view" => "view_edition",
            "permission_suffix" => "proces_verbal",
        );

        /*
         *  ACTIONS DE MODIFICATION DES BLOCS DE DONNÉES
         *  SUIVANT L'ÉTAT DE L'ANALYSE
         */

        // États de l'analyse et leurs conditions
        $etats_analyse = array(
            'en_cours' => 'is_in_progress',
            'termine' => 'is_finished',
        );
        // Blocs de données
        $blocs_donnees = array(
            'analyses_type',
            'objet',
            'descriptif_etablissement',
            'classification_etablissement',
            'donnees_techniques',
            'reglementation_applicable',
            'prescriptions',
            'documents_presentes',
            'essais_realises',
            'compte_rendu',
            'observation',
            'avis_propose',
            'proposition_decision_ap',
            'modeles_edition',
        );
        $key_action = 100;
        // Pour chaque bloc de données défini
        foreach ($blocs_donnees as $bloc) {
            // Pour chaque état de l'analyse défini
            foreach ($etats_analyse as $etat => $condition) {
                // Création de l'action pour ce couple bloc/état
                $this->class_actions[$key_action] = array(
                    "identifier" => "edit_".$etat."_".$bloc,
                    "view" => "view_edit_".$bloc,
                    "permission_suffix" => $etat."_modifier_".$bloc,
                    "condition" => array("is_from_good_service", $condition, ),
                    // marqueurs
                    "etat_analyse" => $etat,
                    "bloc_donnees" => $bloc,
                );
                $key_action = $key_action + 1;
            }
        }
    }

    /**
     *
     */
    function get_var_sql_forminc__champs() {
        return array(
            "analyses.analyses",
            "analyses.service",
            "analyses_etat",
            "analyses_type",
            "objet",
            "descriptif_etablissement_om_html",
            "reglementation_applicable_om_html",
            "compte_rendu_om_html",
            "document_presente_pendant_om_html",
            "document_presente_apres_om_html",
            "observation_om_html",
            "reunion_avis",
            "avis_complement",
            "array_to_string(
                array_agg(
                    lien_dossier_coordination_etablissement_type.etablissement_type
                    ORDER BY etablissement_type.libelle),
            ';') as etablissement_type_secondaire",
            "si_effectif_public",
            "si_effectif_personnel",
            "si_type_ssi",
            "si_type_alarme",
            "si_conformite_l16",
            "si_alimentation_remplacement",
            "si_service_securite",
            "si_personnel_jour",
            "si_personnel_nuit",
            "analyses.dossier_instruction",
            "analyses.modele_edition_rapport",
            "analyses.modele_edition_compte_rendu",
            "analyses.modele_edition_proces_verbal",
            "modifiee_sans_gen",
            "dernier_pv",
            "dec1",
            "delai1",
            "dec2",
            "delai2",
        );
    }

    /**
     *
     */
    function get_var_sql_forminc__tableSelect() {
        return sprintf(
            '%1$s%2$s
            LEFT JOIN %1$sdossier_instruction
                ON dossier_instruction.dossier_instruction = analyses.dossier_instruction
            LEFT JOIN %1$sdossier_coordination
                ON dossier_coordination.dossier_coordination = dossier_instruction.dossier_coordination
            LEFT JOIN %1$slien_dossier_coordination_etablissement_type
                ON lien_dossier_coordination_etablissement_type.dossier_coordination = dossier_coordination.dossier_coordination
            LEFT JOIN %1$setablissement_type
                ON etablissement_type.etablissement_type = lien_dossier_coordination_etablissement_type.etablissement_type',
            DB_PREFIXE,
            $this->table
        );
    }

    /**
     *
     */
    function get_var_sql_forminc__selection() {
        return " GROUP BY analyses.analyses ";
    }

    /**
     * filtre les modèles d'édition par type de courrier et valide ou déjà sélectionné
     */
    function get_var_sql_forminc__sql_modele_edition() {
        return "SELECT modele_edition.modele_edition, modele_edition.libelle 
        FROM ".DB_PREFIXE."modele_edition 
        LEFT JOIN ".DB_PREFIXE."courrier_type ON courrier_type.courrier_type = modele_edition.courrier_type
        WHERE LOWER(courrier_type.code) = LOWER('<courrier_type_code>')
            AND (
                ((modele_edition.om_validite_debut IS NULL
                        AND (modele_edition.om_validite_fin IS NULL
                        OR modele_edition.om_validite_fin > CURRENT_DATE))
                        OR (modele_edition.om_validite_debut <= CURRENT_DATE
                        AND (modele_edition.om_validite_fin IS NULL
                        OR modele_edition.om_validite_fin > CURRENT_DATE)))
                <where_modele>)
        ORDER BY modele_edition.libelle ASC";
    }

    /**
     * Indique si la redirection vers le lien de retour est activée ou non.
     *
     * L'objectif de cette méthode est de permettre d'activer ou de désactiver
     * la redirection dans certains contextes.
     *
     * @return boolean
     */
    function is_back_link_redirect_activated() {
        return false;
    }

    /**
     * SETTER_FORM - setType.
     */
    function setType(&$form, $maj) {
        parent::setType($form, $maj);
        // Pour les actions appelée en POST en Ajax, il est nécessaire de
        // qualifier le type de chaque champs (Si le champ n'est pas défini un
        // PHP Notice:  Undefined index dans core/om_formulaire.class.php est
        // levé). On sélectionne donc les actions de portlet de type
        // action-direct ou assimilé et les actions spécifiques avec le même
        // comportement.
        if ($this->get_action_param($maj, "portlet_type") == "action-direct"
            || $this->get_action_param($maj, "portlet_type") == "action-direct-with-confirmation") {
            //
            foreach ($this->champs as $key => $value) {
                $form->setType($value, 'hidden');
            }
            $form->setType($this->clePrimaire, "hiddenstatic");
        }
    }

    /**
     * VIEW - visualiser
     * 
     * Surcharge de la consultation de l'analyse.
     *
     * @return void
     */
    function visualiser() {
        // Vérification de l'accessibilité sur l'élément
        $this->checkAccessibility();
        // Récupération du code du service
        $code_service = $this->f->get_service_code($this->getVal("service"));
        // Instanciation du dossier de coordination
        $di_id = $this->getVal("dossier_instruction");
        $di = $this->f->get_inst__om_dbform(array(
            "obj" => "dossier_instruction",
            "idx" => $di_id,
        ));
        $dc_id = $di->getVal("dossier_coordination");
        $dc = $this->f->get_inst__om_dbform(array(
            "obj" => "dossier_coordination",
            "idx" => $dc_id,
        ));
        // Récupération de l'état de l'analyse
        $etat_valeur = $this->getVal("analyses_etat");
        $etat_libelle = __("analyse");
        switch ($etat_valeur) {
            case 'en_cours':
                $etat_libelle = __("en cours de redaction");
                break;
            case 'termine':
                $etat_libelle = __("terminee");
                break;
            case 'valide':
                $etat_libelle = __("validee");
                break;
            case 'acte':
                $etat_libelle = __("actee");
                break;
        }
        //Création du formulaire
        $form = $this->f->get_inst__om_formulaire(array(
            "validation" => 0,
            "maj" => $this->getParameter("maj"),
            "champs" => $this->champs,
        ));
        // Composition du tableau d'action à afficher dans le portlet
        $this->compose_portlet_actions();
        // Affichage du portlet d'actions s'il existe des actions
        if (!empty($this->user_actions)) {
            $form->afficher_portlet(
                $this->getParameter("idx"),
                $this->user_actions
            );
        }
        // Affichage de l'état en titre
        printf('<div id="titre_etat_analyse">');
        $this->display_analyse_state($etat_libelle);
        printf('</div>');
        printf("<div class=\"visualClear\"></div>");
        // Ouverture du conteneur des données
        printf('<div>');

        // Récupération des prescriptions
        $prescriptions = $this->get_prescription($this->getVal("analyses"));
        (count($prescriptions) > 0) ? $nb_prescriptions = " (".count($prescriptions).")" : $nb_prescriptions = "";

        // Liste des blocs de données avec leurs titres et données
        $blocs_donnees = array(
            // Type d'analyse
            'analyses_type' => array(
                "libelle" => __("analyse_type"),
                "valeur" => $this->get_libelle("analyses_type", "analyses_type", "analyses_type", null, null, $this->getVal("analyses_type")),
            ),
            // Objet
            'objet' => array(
                "libelle" => __("objet"),
                "valeur" => nl2br($this->getVal("objet")),
            ),
            // Descriptif établissement
            'descriptif_etablissement' => array(
                "libelle" => __("descriptif_etablissement_om_html"),
                "valeur" => $this->getVal("descriptif_etablissement_om_html"),
            ),
            // Classification établissement
            'classification_etablissement' => array(
                "libelle" => __("classification de l'etablissement"),
                "valeur" => $dc->get_classification_etablissement(),
            ),
            // Données techniques
            'donnees_techniques' => array(
                "libelle" => __("Donnees techniques").($code_service == 'acc' ? " / ".__("UA") : ""),
                "valeur" => $this->get_donnees_techniques(),
            ),
            // Réglementation applicable
            'reglementation_applicable' => array(
                "libelle" => __("reglementation_applicable_om_html"),
                "valeur" => $this->getVal("reglementation_applicable_om_html"),
            ),
            // Prescriptions
            'prescriptions' => array(
                "libelle" => __("prescriptions").$nb_prescriptions,
                "valeur" => $this->widget_view_prescription($prescriptions),
            ),
            // Documents présentés
            'documents_presentes' => array(
                "libelle" => __("documents presentes"),
                "valeur" => $this->get_document_presente($this->getVal("document_presente_pendant_om_html"),$this->getVal("document_presente_apres_om_html")),
            ),
            // Essais réalisés
            'essais_realises' => array(
                "libelle" => __("essais realises"),
                "valeur" => $this->widget_n_n($this->get_essai_realise($this->getVal("analyses"))),
            ),
            // Compte rendu de l'analyse
            'compte_rendu' => array(
                "libelle" => __("compte_rendu_analyse"),
                "valeur" => $this->getVal("compte_rendu_om_html"),
            ),
            // Observation
            'observation' => array(
                "libelle" => __("observation"),
                "valeur" => $this->getVal("observation_om_html"),
            ),
            // Avis proposé
            'avis_propose' => array(
                "libelle" => __("avis propose"),
                "valeur" => $this->get_avis_propose($this->getVal("reunion_avis"),nl2br($this->getVal("avis_complement"))),
            ),
            // Proposition de décision autorité de police
            'proposition_decision_ap' => array(
                "libelle" => __("proposition de decision autorite de police"),
                "valeur" => $this->get_proposition_decision_ap(),
            ),
            // Modèles d'édition
            'modeles_edition' => array(
                "libelle" => __("modeles d'edition"),
                "valeur" => $this->get_modeles_edition(),
            ),
        );

        // Pour toutes les actions de modification définies pour l'état actuel
        // de l'analyse on récupère leur numéro et leur permission
        foreach ($this->class_actions as $key => $action) {
            // S'il s'agit d'une modification de bloc de données
            if (isset($action["etat_analyse"]) && isset($action["bloc_donnees"])) {
                // Si l'état de l'analyse est géré par l'action
                if ($action["etat_analyse"] == $etat_valeur) {
                    // On récupère l'identifiant du bloc de données
                    $bloc = $action["bloc_donnees"];
                    // On lui associe le numéro d'action
                    $blocs_donnees[$bloc]["ordre"] = $key;
                    // On lui associe la permission de modification
                    $blocs_donnees[$bloc]["right_edit"] = "analyses_".$action["permission_suffix"];
                }
            }

        }

        if ($code_service == 'acc'
            && $this->f->is_option_unite_accessibilite_enabled() !== true) {
            unset($blocs_donnees["donnees_techniques"]);
        }
        // Si service de l'analyse différent de sécurité incendie
        if ($code_service != 'si') {
            // Pas de bloc "classification de l'établissement"
            unset($blocs_donnees["classification_etablissement"]);
        }

        // On affiche les widgets pour chaque bloc récupéré
        foreach ($blocs_donnees as $bloc => $val) {
            // Titre du bloc
            $title = $val["libelle"];
            // Données du bloc
            $value = $val["valeur"];
            // Permission de consultation du bloc
            $view = "analyses_consulter_".$bloc;
            // Éventuel numéro d'action de modification
            $ord = null;
            if (isset($val["ordre"]) && !empty($val["ordre"])) {
                $ord = $val["ordre"];
            }
            // Affichage du widget
            $this->widget_analyses($ord, $title, $value, $view);
        }

        // Fin de la vue
        printf('</div>');
        printf("<div class=\"visualClear\"></div>");
    }

    /**
     * VIEW - view_edit_analyses_type.
     *
     * Appelle le formulaire générique d'édition pour le champ analyses_type
     * et traite ses valeurs postées.
     * 
     * @return void
     */
    function view_edit_analyses_type() {
        // Vérification de l'accessibilité sur l'élément
        $this->checkAccessibility();
        /*
            Initilisation des variables
         */
        // Légende du fieldset
        $title = __("analyses_type");
        // Avant les vérifications on considère le formulaire valide
        $correct = true;
        // Pour la même raison on crée un message d'erreur vide
        $message = "";
        // Définition de la table mise à jour
        $table = $this->table;
        // Récupération de l'ID de l'analyse
        $id = $this->getVal($this->clePrimaire);
        // Récupération du code du service et construction du suffixe
        $code_service = $this->f->get_service_code($this->getVal("service"));
        $suffixe_service = "_".strtolower($code_service);
        // Récupération de l'ID du DC
        $di = $this->f->get_inst__om_dbform(array(
            "obj" => "dossier_instruction",
            "idx" => $this->getVal("dossier_instruction"),
        ));
        $id_dc = $di->getVal("dossier_coordination");
        // Récupération du type du DC
        $dc = $this->f->get_inst__om_dbform(array(
            "obj" => "dossier_coordination",
            "idx" => $id_dc,
        ));
        $type_dc = $dc->getVal("dossier_coordination_type");
        // Construction du select
        // 1/2 - Ajout du type principal
        $sql_select = "SELECT analyses_type.analyses_type, analyses_type.libelle
            FROM ".DB_PREFIXE."analyses_type
            LEFT JOIN ".DB_PREFIXE."dossier_coordination_type
                ON dossier_coordination_type.analyses_type".$suffixe_service." = analyses_type.analyses_type
            LEFT JOIN ".DB_PREFIXE."dossier_coordination
                 ON dossier_coordination.dossier_coordination_type = dossier_coordination_type.dossier_coordination_type
            WHERE dossier_coordination.dossier_coordination = ".$id_dc."
            ORDER BY analyses_type.libelle ASC";
        // On crée le contenu du select
        $res = $this->f->db->query($sql_select);
        $this->f->addToLog(__METHOD__."(): db->query(\"".$sql_select."\");", VERBOSE_MODE);
        // Si la construction du select échoue
        if ($this->f->isDatabaseError($res, true)) {
            // Appel de la methode de recuperation des erreurs
            $this->erreur_db($res->getDebugInfo(), $res->getMessage(), '');
            $this->correct = false;
            return false;
        }
        $k=1;
        $contenu = array();
        $contenu[0][0] = '';
        $contenu[1][0] = __("choisir")." ".__("analyses_type");
        while($row = &$res->fetchRow()){
            $contenu[0][$k] = $row[0];
            $contenu[1][$k] = $row[1];
            $k++;
        }
        // 2/2 - Ajout des types secondaires si valide ou déjà sélectionné
        $sql_select_bis = "SELECT analyses_type.analyses_type, analyses_type.libelle
            FROM ".DB_PREFIXE."analyses_type
            LEFT JOIN ".DB_PREFIXE."lien_dossier_coordination_type_analyses_type lien
                ON lien.analyses_type = analyses_type.analyses_type
            WHERE lien.dossier_coordination_type = ".$type_dc."
            AND lien.service = ".$this->getVal("service")."
            AND (
                ((analyses_type.om_validite_debut IS NULL AND (analyses_type.om_validite_fin IS NULL OR analyses_type.om_validite_fin > CURRENT_DATE)) OR (analyses_type.om_validite_debut <= CURRENT_DATE AND (analyses_type.om_validite_fin IS NULL OR analyses_type.om_validite_fin > CURRENT_DATE)))
                OR analyses_type.analyses_type = ".$this->getVal("analyses_type").")
            ORDER BY analyses_type.libelle ASC";
        // On ajoute le contenu du select à celui existant
        $res2 = $this->f->db->query($sql_select_bis);
        $this->f->addToLog(__METHOD__."(): db->query(\"".$sql_select_bis."\");", VERBOSE_MODE);
        // Si la construction du select échoue
        if ($this->f->isDatabaseError($res2, true)) {
            // Appel de la methode de recuperation des erreurs
            $this->erreur_db($res2->getDebugInfo(), $res2->getMessage(), '');
            $this->correct = false;
            return false;
        }
        while($row = &$res2->fetchRow()){
            // Si la valeur existe déjà dans la liste (type principal) alors on
            // passe à l'itération suivante pour ne pas avoir de doublons.
            if (in_array($row[0], $contenu[0]) === true) {
                continue;
            }
            //
            $contenu[0][$k] = $row[0];
            $contenu[1][$k] = $row[1];
            $k++;
        }
        // Champ de la table à mettre à jour
        $champs = array(
            "analyses_type" => array(
                "colonne" => "analyses_type",
                "libelle" => "",
                "type" => "select",
                "contenu" => $contenu,
                "valeur" => $this->getVal("analyses_type"),
            ),
        );
        /*
            Traitement d'éventuelles valeurs postées
         */
        // S'il y a des valeurs postées on les vérifie, puis le cas échéant
        // on met à jour la base de données ou on rappelle le formulaire
        if ($this->getParameter("postvar") != null) {
            // Si la valeur est incohérente
            // alors la validation du formulaire a échoué
            // sinon on l'échappe s'il s'agit d'une chaîne de caractères
            // 
            // Cas analyses_type
            $analyses_type = $this->encode_and_escape("analyses_type");
            if ((is_numeric($analyses_type)) == false || ($analyses_type == 0)) {
                // alors la validation du formulaire a échoué
                $correct = false;
                $message .= __("Le type d'analyse renseigne est invalide.");
                // on mémorise la valeur postée,
                // ici on l'efface vu qu'il s'agit d'un select
                $champs["analyses_type"]["valeur"] = "";
            } else {
                // sinon on récupère la valeur postée
                $champs["analyses_type"]["valeur"] = intval($analyses_type);
            }
            // On met à jour la table
            // Si le formulaire est valide
            if ($correct == true) {
                // On met à jour la table
                if ($this->update_table($table, $champs, $id) != false) {
                    // On affiche un message de validation
                    $this->display_success($id);
                } else {
                    // On affiche un message d'erreur
                    $this->display_success($id, false);
                }
            } else {
                // Sinon on réaffiche le formulaire
                // avec le message d'erreur et les valeurs postées
                $this->display_generic_edit_form($champs, $id, $title, $message);
            }
        } else {
            // sinon (aucune valeur postée) on affiche le formulaire sans message
            $this->display_generic_edit_form($champs, $id, $title);
        }
    }

    /**
     * VIEW - view_edit_modeles_edition.
     *
     * Appelle le formulaire générique d'édition pour les champs
     * modèles edition rapport, compte-rendu et PV.
     * 
     * @return void
     */
    function view_edit_modeles_edition() {
        // Vérification de l'accessibilité sur l'élément
        $this->checkAccessibility();
        /*
            Initilisation des variables
         */
        // Légende du fieldset
        $title = __("modeles d'edition");
        // Avant les vérifications on considère le formulaire valide
        $correct = true;
        // Pour la même raison on crée un message d'erreur vide
        $message = "";
        // Récupération de l'ID de l'analyse
        $id = $this->getVal($this->clePrimaire);
        // Récupération du code du service
        $code_service = $this->f->get_service_code($this->getVal("service"));
        // Requête compte-rendu
        // Inclusion du modèle précédemment sélectionné s'il existe
        $where_modele = "";
        $id_modele = $this->getVal("modele_edition_compte_rendu");
        if ($id_modele != "") {
            $where_modele = "OR modele_edition.modele_edition = ".$id_modele;
        }
        $sql_modele_edition_crd = str_replace('<where_modele>', $where_modele, $this->get_var_sql_forminc__sql("modele_edition"));
        $sql_modele_edition_crd = str_replace('<courrier_type_code>', 'ANL-'.$code_service.'-CRD', $sql_modele_edition_crd);
        $res_crd = $this->f->db->query($sql_modele_edition_crd);
        $this->f->addToLog(__METHOD__."(): db->query(\"".$sql_modele_edition_crd."\");", VERBOSE_MODE);
        if ($this->f->isDatabaseError($res_crd, true)) {
            $this->erreur_db($res_crd->getDebugInfo(), $res_crd->getMessage(), '');
            $this->correct = false;
            return false;
        }
        // Requête rapport
        // Inclusion du modèle précédemment sélectionné s'il existe
        $where_modele = "";
        $id_modele = $this->getVal("modele_edition_rapport");
        if ($id_modele != "") {
            $where_modele = "OR modele_edition.modele_edition = ".$id_modele;
        }
        $sql_modele_edition_rpt = str_replace('<where_modele>', $where_modele, $this->get_var_sql_forminc__sql("modele_edition"));
        $sql_modele_edition_rpt = str_replace('<courrier_type_code>', 'ANL-'.$code_service.'-RPT', $sql_modele_edition_rpt);
        $res_rpt = $this->f->db->query($sql_modele_edition_rpt);
        $this->f->addToLog(__METHOD__."(): db->query(\"".$sql_modele_edition_rpt."\");", VERBOSE_MODE);
        if ($this->f->isDatabaseError($res_rpt, true)) {
            $this->erreur_db($res_rpt->getDebugInfo(), $res_rpt->getMessage(), '');
            $this->correct = false;
            return false;
        }
        // Requête PV
        // Inclusion du modèle précédemment sélectionné s'il existe
        $where_modele = "";
        $id_modele = $this->getVal("modele_edition_proces_verbal");
        if ($id_modele != "") {
            $where_modele = "OR modele_edition.modele_edition = ".$id_modele;
        }
        $sql_modele_edition_pv = str_replace('<where_modele>', $where_modele, $this->get_var_sql_forminc__sql("modele_edition"));
        $sql_modele_edition_pv = str_replace('<courrier_type_code>', 'ANL-'.$code_service.'-PV', $sql_modele_edition_pv);
        $res_pv = $this->f->db->query($sql_modele_edition_pv);
        $this->f->addToLog(__METHOD__."(): db->query(\"".$sql_modele_edition_pv."\");", VERBOSE_MODE);
        if ($this->f->isDatabaseError($res_pv, true)) {
            $this->erreur_db($res_pv->getDebugInfo(), $res_pv->getMessage(), '');
            $this->correct = false;
            return false;
        }
        // select compte-rendu
        $k=1;
        $select_cr = array();
        $select_cr[0][0] = '';
        $select_cr[1][0] = __("choisir")." ".__("modele_edition_compte_rendu");
        while ($row_crd = &$res_crd->fetchRow()){
            $select_cr[0][$k] = $row_crd[0];
            $select_cr[1][$k] = $row_crd[1];
            $k++;
        }
        // select rapport
        $k=1;
        $select_rpt = array();
        $select_rpt[0][0] = '';
        $select_rpt[1][0] = __("choisir")." ".__("modele_edition_rapport");
        while ($row_rpt = &$res_rpt->fetchRow()){
            $select_rpt[0][$k] = $row_rpt[0];
            $select_rpt[1][$k] = $row_rpt[1];
            $k++;
        }
        // select pv
        $k=1;
        $select_pv = array();
        $select_pv[0][0] = '';
        $select_pv[1][0] = __("choisir")." ".__("modele_edition_proces_verbal");
        while ($row_pv = &$res_pv->fetchRow()){
            $select_pv[0][$k] = $row_pv[0];
            $select_pv[1][$k] = $row_pv[1];
            $k++;
        }
        // Champ de la table à mettre à jour
        $champs = array(
            "modele_edition_rapport" => array(
                "colonne" => "modele_edition_rapport",
                "libelle" => __("modele_edition_rapport"),
                "type" => "select",
                "contenu" => $select_rpt,
                "valeur" => $this->getVal("modele_edition_rapport"),
            ),
            "modele_edition_compte_rendu" => array(
                "colonne" => "modele_edition_compte_rendu",
                "libelle" => __("modele_edition_compte_rendu"),
                "type" => "select",
                "contenu" => $select_cr,
                "valeur" => $this->getVal("modele_edition_compte_rendu"),
            ),
            "modele_edition_proces_verbal" => array(
                "colonne" => "modele_edition_proces_verbal",
                "libelle" => __("modele_edition_proces_verbal"),
                "type" => "select",
                "contenu" => $select_pv,
                "valeur" => $this->getVal("modele_edition_proces_verbal"),
            ),
        );
        /*
            Traitement d'éventuelles valeurs postées
         */
        // S'il y a des valeurs postées on les vérifie, puis le cas échéant
        // on met à jour la base de données ou on rappelle le formulaire
        if ($this->getParameter("postvar") != null) {
            // Si la valeur du select est incohérente
            // alors la validation du formulaire a échoué
            // sinon on la convertit en integer
            // 
            // Cas modele_edition_rapport
            $modele_edition_rapport = $this->encode_and_escape("modele_edition_rapport");
            if ((is_numeric($modele_edition_rapport)) == false || ($modele_edition_rapport == 0)) {
                // alors la validation du formulaire a échoué
                $correct = false;
                $message .= __("Le modele de rapport renseigne est invalide.");
                // on mémorise la valeur postée,
                // ici on l'efface vu qu'il s'agit d'un select
                $champs["modele_edition_rapport"]["valeur"] = "";
            } else {
                // sinon on récupère la valeur postée
                $champs["modele_edition_rapport"]["valeur"] = intval($modele_edition_rapport);
            }
            // Cas modele_edition_compte_rendu
            $modele_edition_compte_rendu = $this->encode_and_escape("modele_edition_compte_rendu");
            if ((is_numeric($modele_edition_compte_rendu)) == false || ($modele_edition_compte_rendu == 0)) {
                // alors la validation du formulaire a échoué
                $correct = false;
                $message .= __("Le modele de compte-rendu renseigne est invalide.");
                // on mémorise la valeur postée,
                // ici on l'efface vu qu'il s'agit d'un select
                $champs["modele_edition_compte_rendu"]["valeur"] = "";
            } else {
                // sinon on récupère la valeur postée
                $champs["modele_edition_compte_rendu"]["valeur"] = intval($modele_edition_compte_rendu);
            }
            // Cas modele_edition_proces_verbal
            $modele_edition_proces_verbal = $this->encode_and_escape("modele_edition_proces_verbal");
            if ((is_numeric($modele_edition_proces_verbal)) == false || ($modele_edition_proces_verbal == 0)) {
                // alors la validation du formulaire a échoué
                $correct = false;
                $message .= __("Le modele de proces-verbal renseigne est invalide.");
                // on mémorise la valeur postée,
                // ici on l'efface vu qu'il s'agit d'un select
                $champs["modele_edition_proces_verbal"]["valeur"] = "";
            } else {
                // sinon on récupère la valeur postée
                $champs["modele_edition_proces_verbal"]["valeur"] = intval($modele_edition_proces_verbal);
            }
            // On met à jour la table
            // Si le formulaire est valide
            if ($correct == true) {
                // On met à jour la table
                if ($this->update_table($this->table, $champs, $id) != false) {
                    // On affiche un message de validation
                    $this->display_success($id);
                } else {
                    // On affiche un message d'erreur
                    $this->display_success($id, false);
                }
            } else {
                // Sinon on réaffiche le formulaire
                // avec le message d'erreur et les valeurs postées
                $this->display_generic_edit_form($champs, $id, $title, $message);
            }
        } else {
            // sinon (aucune valeur postée) on affiche le formulaire sans message
            $this->display_generic_edit_form($champs, $id, $title);
        }
    }

    /**
     * VIEW - view_edit_objet.
     *
     * Appelle le formulaire générique d'édition pour le champ objet
     * et traite ses valeurs postées.
     * 
     * @return void
     */
    function view_edit_objet() {
        // Vérification de l'accessibilité sur l'élément
        $this->checkAccessibility();
        // Légende du fieldset
        $title = __("Objet");
        // Définition de la table mise à jour
        $table = $this->table;
        // Récupération de l'ID de l'analyse
        $id = $this->getVal($this->clePrimaire);
        // Champ de la table à mettre à jour
        $champs = array(
            "objet" => array(
                "colonne" => "objet",
                "libelle" => "",
                "type" => "textarea",
                "valeur" => $this->getVal("objet"),
                "taille" => 80,
                "max" => 6,
            ),
        );
        // S'il y a des valeurs postées on les échappe puis
        // on met à jour la base de données ou on rappelle le formulaire
        if ($this->getParameter("postvar") != null) {
            $champs["objet"]["valeur"] = $this->encode_and_escape("objet");
            // On met à jour la table
            if ($this->update_table($table, $champs, $id) != false) {
                // On affiche un message de validation
                $this->display_success($id);
            } else {
                // On affiche un message d'erreur
                $this->display_success($id, false);
            }
        } else {
            // sinon (aucune valeur postée) on affiche le formulaire sans message
            $this->display_generic_edit_form($champs, $id, $title);
        }
    }

    /**
     * VIEW - view_edit_proposition_decision_ap.
     *
     * Appelle le formulaire générique d'édition pour le bloc de données
     * proposition de décision autorité de police composé des champs
     * décision 1, délai 1, décision 2 et délai 2.
     * 
     * @return void
     */
    function view_edit_proposition_decision_ap() {
        // Vérification de l'accessibilité sur l'élément
        $this->checkAccessibility();
        // Légende du fieldset
        $title = __("proposition de decision autorite de police");
        // Définition de la table mise à jour
        $table = $this->table;
        // Récupération de l'ID de l'analyse
        $id = $this->getVal($this->clePrimaire);
        // Champ de la table à mettre à jour
        $champs = array(
            "dec1" => array(
                "colonne" => "dec1",
                "libelle" => __("Decision"),
                "type" => "text",
                "valeur" => $this->getVal("dec1"),
                "taille" => 100,
                "max" => 250,
            ),
            "delai1" => array(
                "colonne" => "delai1",
                "libelle" => __("Delai"),
                "type" => "text",
                "valeur" => $this->getVal("delai1"),
                "taille" => 100,
                "max" => 250,
            ),
            "dec2" => array(
                "colonne" => "dec2",
                "libelle" => __("Decision"),
                "type" => "text",
                "valeur" => $this->getVal("dec2"),
                "taille" => 100,
                "max" => 250,
            ),
            "delai2" => array(
                "colonne" => "delai2",
                "libelle" => __("Delai"),
                "type" => "text",
                "valeur" => $this->getVal("delai2"),
                "taille" => 100,
                "max" => 250,
            ),
        );
        // S'il y a des valeurs postées on les échappe puis
        // on met à jour la base de données ou on rappelle le formulaire
        if ($this->getParameter("postvar") != null) {
            $champs["dec1"]["valeur"] = $this->encode_and_escape("dec1");
            $champs["delai1"]["valeur"] = $this->encode_and_escape("delai1");
            $champs["dec2"]["valeur"] = $this->encode_and_escape("dec2");
            $champs["delai2"]["valeur"] = $this->encode_and_escape("delai2");
            // On met à jour la table
            if ($this->update_table($table, $champs, $id) != false) {
                // On affiche un message de validation
                $this->display_success($id);
            } else {
                // On affiche un message d'erreur
                $this->display_success($id, false);
            }
        }
        // sinon (aucune valeur postée) on affiche le formulaire sans message
        else {
            // Création du formulaire
            $liste_champs = array();
            foreach ($champs as $champ => $values) {
                $liste_champs[] = $champ;
            }
            $form = $this->f->get_inst__om_formulaire(array(
                "validation" => 0,
                "maj" => 1,
                "champs" => $liste_champs,
            ));
            // Formatage des champs
            foreach ($champs as $champ => $values) {
                // libellé du champ
                $form->setLib($champ, $values["libelle"]);
                // widget formulaire du champ
                $form->setType($champ, $values["type"]);
                // si une taille a été définie
                if (isset($values["taille"])) {
                    $form->setTaille($champ, $values["taille"]);
                }
                // si un max a été défini
                if (isset($values["max"])) {
                    $form->setMax($champ, $values["max"]);
                }
                // valeur du champ
                $form->setVal($champ, $values["valeur"]);
            }
            // Bouton retour du haut
            $this->btn_retour();
            // Ouverture de la balise form
            $this->open_form_tag();
            // Entete
            $form->entete();
            // Champs
            $form->setBloc("dec1", "D", "", "col_12");
            $form->setFieldset("dec1", "D", $title);
            $form->setBloc("dec1", "D", __("Proposition 1"), "col_12");
            $form->setBloc("delai1", "F");
            $form->setBloc("dec2", "D", __("Proposition 2"), "col_12");
            $form->setBloc("delai2", "F");
            $form->setFieldset("delai2", "F");
            $form->setBloc("delai2", "F");
            $form->afficher($liste_champs, 0, false, false);
            // Boutons
            $form->enpied();
            $this->form_controls();
            // Fermeture du formulaire
            printf("</form>\n");
        }
    }

    /**
     * VIEW - view_edit_descriptif_etablissement.
     *
     * Appelle le formulaire générique d'édition pour le champ
     * descriptif_etablissement_om_html et traite ses valeurs postées.
     * 
     * @return void
     */
    function view_edit_descriptif_etablissement() {
        // Vérification de l'accessibilité sur l'élément
        $this->checkAccessibility();
        $title = __("Descriptif de l'etablissement");
        // Définition de la table mise à jour
        $table = $this->table;
        // Récupération de l'ID de l'analyse
        $id = $this->getVal($this->clePrimaire);
        // Champ de la table à mettre à jour
        $champs = array(
            "descriptif_etablissement_om_html" => array(
                "colonne" => "descriptif_etablissement_om_html",
                "libelle" => "",
                "type" => "html",
                "valeur" => $this->getVal("descriptif_etablissement_om_html"),
                "taille" => "",
                "max" => "",
            ),
        );
        // S'il y a des valeurs postées on les échappe puis
        // on met à jour la base de données ou on rappelle le formulaire
        if ($this->getParameter("postvar") != null) {
            $champs["descriptif_etablissement_om_html"]["valeur"] = $this->encode_and_escape("descriptif_etablissement_om_html");
            // On met à jour la table
            if ($this->update_table($table, $champs, $id) != false) {
                // On affiche un message de validation
                $this->display_success($id);
            } else {
                // On affiche un message d'erreur
                $this->display_success($id, false);
            }
        } else {
            // sinon (aucune valeur postée) on affiche le formulaire sans message
            $this->display_generic_edit_form($champs, $id, $title);
        }
    }

    /**
     * VIEW - edit_classification_etablissement.
     *
     * Appelle le formulaire générique d'édition pour les champs d'établissement
     * type, types secondaires, catégorie et locaux à sommeil.
     * 
     * @return void
     */
    function view_edit_classification_etablissement() {
        // Vérification de l'accessibilité sur l'élément
        $this->checkAccessibility();
        /*
            Initilisation des variables
         */
        // Légende du fieldset
        $title = __("Classification de l'etablissement");
        // Instanciation du DC
        $di_id = $this->getVal("dossier_instruction");
        $di = $this->f->get_inst__om_dbform(array(
            "obj" => "dossier_instruction",
            "idx" => $di_id,
        ));
        $dc_id = $di->getVal("dossier_coordination");
        $dc = $this->f->get_inst__om_dbform(array(
            "obj" => "dossier_coordination",
            "idx" => $dc_id,
        ));
        // Récupération de l'ID de l'analyse
        $id = $this->getVal($this->clePrimaire);
        // Cas du select "type d'établissement"
        $sql_select = "SELECT etablissement_type.etablissement_type,
            concat('[',etablissement_type.libelle, '] ',
                etablissement_type.description)
            FROM ".DB_PREFIXE."etablissement_type
            ORDER BY etablissement_type.libelle ASC";
        $res = $this->f->db->query($sql_select);
        $this->f->addToLog(__METHOD__."(): db->query(\"".$sql_select."\");", VERBOSE_MODE);
        if ($this->f->isDatabaseError($res, true)) {
            $this->erreur_db($res->getDebugInfo(), $res->getMessage(), '');
            $this->correct = false;
            return false;
        }
        $k=1;
        $select_type = array();
        $select_type[0][0] = '';
        $select_type[1][0] = __("NC");
        while($row = &$res->fetchRow()){
            $select_type[0][$k] = $row[0];
            $select_type[1][$k] = $row[1];
            $k++;
        }
        // Cas du select "catégorie d'établissement"
        $sql_select = "SELECT etablissement_categorie.etablissement_categorie,
            concat('[',etablissement_categorie.libelle, '] ',
                etablissement_categorie.description)
            FROM ".DB_PREFIXE."etablissement_categorie
            ORDER BY etablissement_categorie.libelle ASC";
        $res = $this->f->db->query($sql_select);
        $this->f->addToLog(__METHOD__."(): db->query(\"".$sql_select."\");", VERBOSE_MODE);
        if ($this->f->isDatabaseError($res, true)) {
            $this->erreur_db($res->getDebugInfo(), $res->getMessage(), '');
            $this->correct = false;
            return false;
        }
        $k=1;
        $select_categorie = array();
        $select_categorie[0][0] = '';
        $select_categorie[1][0] = __("NC");
        while ($row = &$res->fetchRow()){
            $select_categorie[0][$k] = $row[0];
            $select_categorie[1][$k] = $row[1];
            $k++;
        }
        // Champs du formulaire
        $champs = array(
            "erp" => array(
                "colonne" => "erp",
                "libelle" => __("erp"),
                "type" => "checkbox",
                "taille" => 20,
                "max" => "",
                "valeur" => $dc->getVal("erp"),
            ),
            "etablissement_locaux_sommeil" => array(
                "colonne" => "etablissement_locaux_sommeil",
                "libelle" => __("etablissement_locaux_sommeil"),
                "type" => "checkbox",
                "taille" => 20,
                "max" => "",
                "valeur" => $dc->getVal("etablissement_locaux_sommeil"),
            ),
            "etablissement_type" => array(
                "colonne" => "etablissement_type",
                "libelle" => __("type"),
                "type" => "select",
                "taille" => 20,
                "contenu" => $select_type,
                "valeur" => $dc->getVal("etablissement_type"),
            ),
            "etablissement_type_secondaire" => array(
                "colonne" => "etablissement_type_secondaire",
                "libelle" => __("type(s) secondaire(s)"),
                "type" => "select_multiple",
                "taille" => 20,
                "contenu" => $select_type,
                "valeur" => $dc->getVal("etablissement_type_secondaire"),
            ),
            "etablissement_categorie" => array(
                "colonne" => "etablissement_categorie",
                "libelle" => __("categorie"),
                "type" => "select",
                "taille" => 20,
                "contenu" => $select_categorie,
                "valeur" => $dc->getVal("etablissement_categorie"),
            ),
        );
        // Ajout en hidden des contacts s'il en existe
        $dc->listeContact($dc_id);
        if (isset ($dc->valIdContact) AND
            !empty($dc->valIdContact)) {
            $i = 0;
            foreach ($dc->valIdContact as $contact) {
                $champs["contact_".$i] = array(
                    "colonne" => "contact",
                    "libelle" => "",
                    "type" => "hidden_contact",
                    "valeur" => $contact,
                );
                $i++;
            }
        }
        // S'il y a des valeurs postées on met à jour la base de données
        if ($this->getParameter("postvar") != null) {
            $dc->correct = true;
            // Récupération des champs déjà renseignés
            $dc->setValFFromVal();
            // Pour éviter une erreur de libellé du DC unique
            unset($dc->unique_key);
            // Suppression des champs virtuels
            unset($dc->valF["dossier_instruction_secu_lien"]);
            unset($dc->valF["dossier_instruction_acc_lien"]);
            unset($dc->valF["etablissement_type_secondaire"]);
            unset($dc->valF["exploitant"]);
            // Récupération des champs postés
            $postedValues = $this->getParameter("postvar");
            $dc->valF["erp"] = $postedValues["erp"];
            $dc->valF["etablissement_locaux_sommeil"] = $postedValues["etablissement_locaux_sommeil"];
            $dc->valF["etablissement_type"] = $postedValues["etablissement_type"];
            $dc->valF["etablissement_categorie"] = $postedValues["etablissement_categorie"];
            // Reformatage des dates EN en FR
            foreach ($dc->champs as $key => $field) {
                if ($dc->type[$key] == 'date') {
                    $dc->valF[$field] = $dc->dateDBToForm($dc->valF[$field]);
                }
            }
            // On met à jour le dossier de coordination
            if ($dc->modifier($dc->valF) != false) {
                // On affiche un message de validation
                $this->display_success($id);
            } else {
                // Récupération des champs postés
                $champs["erp"]["valeur"] = $dc->valF["erp"];
                $champs["etablissement_locaux_sommeil"]["valeur"] = $dc->valF["etablissement_locaux_sommeil"];
                $champs["etablissement_type"]["valeur"] = $dc->valF["etablissement_type"];
                $champs["etablissement_type_secondaire"]["valeur"] = implode(';', $postedValues["etablissement_type_secondaire"]);
                $champs["etablissement_categorie"]["valeur"] = $dc->valF["etablissement_categorie"];
                // On affiche un message d'erreur
                $this->f->displayMessage("error", $dc->msg);
                $this->display_generic_edit_form($champs, $id, $title);
            }
        } else {
            // sinon (aucune valeur postée) on affiche le formulaire
            $this->display_generic_edit_form($champs, $id, $title);
        }
    }


    /**
     * VIEW - view_edit_donnees_techniques_si.
     *
     * Appelle le formulaire spécifique de modification pour les données 
     * techniques SI.
     *
     * @return void
     */
    function view_edit_donnees_techniques_si() {
        /*
            Initilisation des variables
         */
        // Légende du fieldset
        $title = __("donnees techniques");
        // Définition de la table mise à jour
        $table = $this->table;
        // Récupération de l'ID de l'analyse
        $id = $this->getVal($this->clePrimaire);
        // Récupération du code du service
        $code_service = $this->f->get_service_code($this->getVal("service"));

        // Construction des select
        // Type SSI
        $ssi = array();
        $ssi[0][0] = 'NC';
        $ssi[1][0] = __("NC");
        $ssi[0][1] = 'A';
        $ssi[1][1] = 'A';
        $ssi[0][2] = 'B';
        $ssi[1][2] = 'B';
        $ssi[0][3] = 'C';
        $ssi[1][3] = 'C';
        $ssi[0][4] = 'D';
        $ssi[1][4] = 'D';
        $ssi[0][5] = 'E';
        $ssi[1][5] = 'E';
        // Type alarme
        $alarme = array();
        $alarme[0][0] = 'NC';
        $alarme[1][0] = __("NC");
        $alarme[0][1] = '1';
        $alarme[1][1] = '1';
        $alarme[0][2] = '2a';
        $alarme[1][2] = '2a';
        $alarme[0][3] = '2b';
        $alarme[1][3] = '2b';
        $alarme[0][4] = '3';
        $alarme[1][4] = '3';
        $alarme[0][5] = '4';
        $alarme[1][5] = '4';
        // Booléens (pour la gestion du null)
        $booleens = array();
        $booleens[0][0] = '';
        $booleens[1][0] = __("NC");
        $booleens[0][1] = 't';
        $booleens[1][1] = __("Oui");
        $booleens[0][2] = 'f';
        $booleens[1][2] = __("Non");

        // Champ SI de la table à mettre à jour
        $champs_si = array(
            "si_conformite_l16" => array(
                "colonne" => "si_conformite_l16",
                "libelle" => __("si_conformite_l16"),
                "type" => "select",
                "contenu" => $booleens,
                "valeur" => $this->getVal("si_conformite_l16"),
            ),
            "si_alimentation_remplacement" => array(
                "colonne" => "si_alimentation_remplacement",
                "libelle" => __("si_alimentation_remplacement"),
                "type" => "select",
                "contenu" => $booleens,
                "valeur" => $this->getVal("si_alimentation_remplacement"),
            ),
            "si_service_securite" => array(
                "colonne" => "si_service_securite",
                "libelle" => __("si_service_securite"),
                "type" => "select",
                "contenu" => $booleens,
                "valeur" => $this->getVal("si_service_securite"),
            ),
            "si_type_ssi" => array(
                "colonne" => "si_type_ssi",
                "libelle" => __("si_type_ssi"),
                "type" => "select",
                "contenu" => $ssi,
                "valeur" => $this->getVal("si_type_ssi"),
            ),
            "si_type_alarme" => array(
                "colonne" => "si_type_alarme",
                "libelle" => __("si_type_alarme"),
                "type" => "select",
                "contenu" => $alarme,
                "valeur" => $this->getVal("si_type_alarme"),
            ),
            "si_effectif_public" => array(
                "colonne" => "si_effectif_public",
                "libelle" => __("si_effectif_public"),
                "type" => "text",
                "valeur" => $this->getVal("si_effectif_public"),
                "taille" => "",
                "max" => "",
            ),
            "si_effectif_personnel" => array(
                "colonne" => "si_effectif_personnel",
                "libelle" => __("si_effectif_personnel"),
                "type" => "text",
                "valeur" => $this->getVal("si_effectif_personnel"),
                "taille" => "",
                "max" => "",
            ),
            "si_personnel_jour" => array(
                "colonne" => "si_personnel_jour",
                "libelle" => __("si_personnel_jour"),
                "type" => "text",
                "valeur" => $this->getVal("si_personnel_jour"),
                "taille" => "",
                "max" => "",
            ),
            "si_personnel_nuit" => array(
                "colonne" => "si_personnel_nuit",
                "libelle" => __("si_personnel_nuit"),
                "type" => "text",
                "valeur" => $this->getVal("si_personnel_nuit"),
                "taille" => "",
                "max" => "",
            ),
        );

        // Définition des champs suivant le service
        switch ($code_service) {
            case 'si' :
                $champs = $champs_si;
                break;
        }

        // S'il y a des valeurs postées on met à jour la base de données
        if ($this->getParameter("postvar") != null) {
            switch ($code_service) {
                case 'si' :
                    // Booléens select
                    if ($this->encode_and_escape("si_conformite_l16") == "") {
                        $champs["si_conformite_l16"]["valeur"] = null;
                    } else {
                        $champs["si_conformite_l16"]["valeur"] = $this->encode_and_escape("si_conformite_l16");
                    }
                    if ($this->encode_and_escape("si_alimentation_remplacement") == "") {
                        $champs["si_alimentation_remplacement"]["valeur"] = null;
                    } else {
                        $champs["si_alimentation_remplacement"]["valeur"] = $this->encode_and_escape("si_alimentation_remplacement");
                    }
                    if ($this->encode_and_escape("si_service_securite") == "") {
                        $champs["si_service_securite"]["valeur"] = null;
                    } else {
                        $champs["si_service_securite"]["valeur"] = $this->encode_and_escape("si_service_securite");
                    }
                    // Nombres
                    if ($this->encode_and_escape("si_effectif_public") == "") {
                        $champs["si_effectif_public"]["valeur"] = null;
                    } else {
                        $champs["si_effectif_public"]["valeur"] = intval($this->encode_and_escape("si_effectif_public"));
                    }
                    if ($this->encode_and_escape("si_effectif_personnel") == "") {
                        $champs["si_effectif_personnel"]["valeur"] = null;
                    } else {
                        $champs["si_effectif_personnel"]["valeur"] = intval($this->encode_and_escape("si_effectif_personnel"));
                    }
                    if ($this->encode_and_escape("si_personnel_jour") == "") {
                        $champs["si_personnel_jour"]["valeur"] = null;
                    } else {
                        $champs["si_personnel_jour"]["valeur"] = intval($this->encode_and_escape("si_personnel_jour"));
                    }
                    if ($this->encode_and_escape("si_personnel_nuit") == "") {
                        $champs["si_personnel_nuit"]["valeur"] = null;
                    } else {
                        $champs["si_personnel_nuit"]["valeur"] = intval($this->encode_and_escape("si_personnel_nuit"));
                    }
                    // Select
                    $champs["si_type_ssi"]["valeur"] = $this->encode_and_escape("si_type_ssi");
                    $champs["si_type_alarme"]["valeur"] = $this->encode_and_escape("si_type_alarme");
                    break;
            }
            // On met à jour la table
            if ($this->update_table($table, $champs, $id) != false) {
                // On affiche un message de validation
                $this->display_success($id);
            } else {
                // On affiche un message d'erreur
                $this->display_success($id, false);
            }
        } else {
            // sinon on affiche le formulaire
            $this->display_generic_edit_form($champs, $id, $title);
        }
    }

    /**
     * VIEW - view_edit_donnees_techniques_acc.
     *
     * Appelle le formulaire spécifique de modification pour les données 
     * techniques ACC.
     *
     * @return void
     */
    function view_edit_donnees_techniques_acc() {
        //
        $this->btn_retour();
        //
        $form = $this->f->get_inst__om_formulaire(array(
            "maj" => 0,
            "validation" => 0,
            "champs" => array(),
        ));
        // Entete
        $form->entete();
        //
        $this->f->layout->display_start_fieldset(
            array("legend_content" => __("Données techniques / UA"),)
        );
        printf('
<div id="ua-tabs">
<ul>
<li>
    <a id="onglet-etablissement_unite__contexte_di_analyse__ua_en_analyse" href="'.OM_ROUTE_SOUSTAB.'&obj=etablissement_unite__contexte_di_analyse__ua_en_analyse&retourformulaire=dossier_instruction&idxformulaire=%1$s">UA analysées</a>
</li>
<li>
    <a id="onglet-etablissement_unite__contexte_di_analyse__ua_valide_sur_etab" href="'.OM_ROUTE_SOUSTAB.'&obj=etablissement_unite__contexte_di_analyse__ua_valide_sur_etab&retourformulaire=dossier_instruction&idxformulaire=%1$s">UA validées sur l\'établissement</a>
</li>
</ul>
</div>',
            $this->getVal("dossier_instruction")
        );
        //
        //$form->afficher($array(), 0, false, false);
        $this->f->layout->display_stop_fieldset();
        //
        $form->enpied();
        // Bouton
        $this->btn_retour();
    }

    /**
     * VIEW - view_edit_donnees_techniques.
     *
     * Appelle le formulaire générique de modification pour les données techniques.
     *
     * @return void
     */
    function view_edit_donnees_techniques() {
        // Vérification de l'accessibilité sur l'élément
        $this->checkAccessibility();

        // Récupération du code du service
        $code_service = $this->f->get_service_code($this->getVal("service"));
        // Appel de la bonne vue en fonction du service
        switch ($code_service) {
            case 'si' :
                $this->view_edit_donnees_techniques_si();
                break;
            case 'acc' :
                if ($this->f->is_option_unite_accessibilite_enabled() === true) {
                    $this->view_edit_donnees_techniques_acc();
                }
                break;
        }

    }

    /**
     * VIEW - view_edit_reglementation_applicable.
     *
     * Appelle le formulaire générique d'édition pour le champ
     * reglementation_applicable et traite ses valeurs postées.
     * 
     * @return void
     */
    function view_edit_reglementation_applicable() {
        // Vérification de l'accessibilité sur l'élément
        $this->checkAccessibility();
        /*
            Initilisation des variables
         */
        // Légende du fieldset
        $title = __("Réglementation applicable");
        // Définition de la table mise à jour
        $table = $this->table;
        // Récupération de l'ID de l'analyse
        $id = $this->getVal($this->clePrimaire);
        // Champ de la table à mettre à jour
        $champs = array(
            "texte-type" => array (
                "colonne" => "texte-type",
                "libelle" => "",
                "type" => "httpclick",
                "valeur" => "popupIt('', 'analyses', '".OM_ROUTE_SOUSFORM."&obj=analyses&action=21&objsf=dossier_instruction&retourformulaire=4&idxformulaire=4&retour=form&idx=".$this->getVal($this->clePrimaire)."', 'auto', 'auto', add_description_type, '\'reglementation_applicable_om_html\', \'form-analyses-overlay\', false', '')",
                "contenu" => array(__("Inserer une reglementation-type")),
            ),
            "reglementation_applicable_om_html" => array(
                "colonne" => "reglementation_applicable_om_html",
                "libelle" => "",
                "type" => "html",
                "valeur" => $this->getVal("reglementation_applicable_om_html"),
                "taille" => "",
                "max" => "",
            )
        );
        /*
            Traitement d'éventuelles valeurs postées
         */
        // S'il y a des valeurs postées on les vérifie, puis le cas échéant
        // on met à jour la base de données ou on rappelle le formulaire
        if ($this->getParameter("postvar") != null) {
            // Cas reglementation_applicable_om_html
            $reglementation_applicable = $this->encode_and_escape("reglementation_applicable_om_html");
            // On met à jour la table
            if ($this->update_table($table, array("reglementation_applicable_om_html" => 
                array(
                    "colonne" => "reglementation_applicable_om_html",
                    "valeur" => $reglementation_applicable,),), $id) != false) {
                // On affiche un message de validation
                $this->display_success($id);
            } else {
                // On affiche un message d'erreur
                $this->display_success($id, false);
            }
        } else {
            // sinon (aucune valeur postée) on affiche le formulaire sans message
            $this->display_generic_edit_form($champs, $id, $title);
        }
    }

    /**
     * VIEW - view_edit_prescriptions.
     *
     * Appelle le formulaire générique d'édition pour la liaison
     * entre analyse et prescription, et traite ses valeurs postées.
     * 
     * @return void
     */
    function view_edit_prescriptions() {
        // Vérification de l'accessibilité sur l'élément
        $this->checkAccessibility();
        /*
            Initilisation des variables
         */
        // Légende du fieldset
        $title = __("Prescriptions");
        // Récupération de l'ID de l'analyse
        $id = intval($this->getVal($this->clePrimaire));
        // Récupération de toutes les prescriptions
        $sql_select = "SELECT prescription as id,
            prescription_reglementaire as pr_id,
            pr_description_om_html as pr_desc,
            pr_defavorable as pr_def,
            ps_description_om_html as ps_desc,
            ordre as ord
            FROM ".DB_PREFIXE."prescription
            WHERE analyses = ".$id."
            ORDER BY ordre ASC NULLS LAST";
        $res = $this->f->db->query($sql_select);
        $this->f->addToLog(__METHOD__."(): db->query(\"".$sql_select."\");", VERBOSE_MODE);
        if ($this->f->isDatabaseError($res, true)) {
            $this->erreur_db($res->getDebugInfo(), $res->getMessage(), '');
            $this->correct = false;
            return false;
        }
        $prescriptions = array();
        while($row=& $res->fetchRow(DB_FETCHMODE_ASSOC)){
            $prescription = array(
                "id" => intval($row["id"]),
                "pr_id" => intval($row["pr_id"]),
                "pr_desc" => $row["pr_desc"],
                "pr_def" => $row["pr_def"],
                "ps_desc" => $row["ps_desc"],
                "ord" => $row["ord"]
            );
            $prescriptions[] = $prescription;
        }
        // S'il y a des valeurs postées on met à jour la base de données
        if ($this->getParameter("postvar") != null) {
            $ps_desc = null;
            $prescriptions = array();
            $postedValues = $this->getParameter("postvar");
            // Récupération des prescriptions s'il y en a
            if (isset($postedValues["ps_desc"]) || isset($postedValues["pr_id"])) {
                $pr_id = $postedValues["pr_id"];
                $pr_def = $postedValues["pr_def"];
                $pr_desc = $postedValues["pr_desc"];
                $ps_desc = $postedValues["ps_desc"];
                foreach ($ps_desc as $key => $value) {
                    $prescriptions[] = array (
                        "ord" => $key,
                        "pr_id" => $this->encode_and_escape($pr_id[$key], true),
                        "pr_def" => $this->encode_and_escape($pr_def[$key], true),
                        "pr_desc" => $this->encode_and_escape($pr_desc[$key], true),
                        "ps_desc" => $this->encode_and_escape($ps_desc[$key], true)
                    );
                }
            }
            // on met à jour la table de liaison
            if ($this->update_prescription($prescriptions, $id) != false) {
                // On affiche un message de validation
                $this->display_success($id);
            } else {
                // On affiche un message d'erreur
                $this->display_success($id, false);
            }
        } else {
            // sinon (aucune valeur postée) on affiche le formulaire sans message
            $this->display_edit_prescription_form($prescriptions, $id, $title);
        }
    }

    /**
     * VIEW - view_edit_documents_presentes.
     *
     * Appelle le formulaire générique d'édition pour les champs
     * document_presente__(pendant,apres)_om_html et traite ses valeurs postées.
     * 
     * @return void
     */
    function view_edit_documents_presentes() {
        // Vérification de l'accessibilité sur l'élément
        $this->checkAccessibility();
        // Légende du fieldset
        $title = __("Documents presentes");
        // Définition de la table mise à jour
        $table = $this->table;
        // Récupération de l'ID de l'analyse
        $id = intval($this->getVal($this->clePrimaire));
        // Champ de la table à mettre à jour
        $champ_pendant = "document_presente_pendant_om_html";
        $action_pendant = OM_ROUTE_FORM."&obj=analyses&action=30&idx=".$id.
            "&champ=".$champ_pendant;
        $popup_pendant = sprintf("overlay_document_presente_type('%s', '%s')",
            $action_pendant,
            $champ_pendant
        );
        $champ_apres = "document_presente_apres_om_html";
        $action_apres = OM_ROUTE_FORM."&obj=analyses&action=30&idx=".$id.
            "&champ=".$champ_apres;
        $popup_apres = sprintf("overlay_document_presente_type('%s', '%s')",
            $action_apres,
            $champ_apres
        );
        $champs = array(
            "doc_type_pendant" => array (
                "colonne" => "doc_type_pendant",
                "libelle" => "",
                "type" => "httpclick",
                "valeur" => $popup_pendant,
                "contenu" => array(__("Inserer un document-type")),
            ),
            $champ_pendant => array(
                "colonne" => $champ_pendant,
                "libelle" => "",
                "type" => "html",
                "valeur" => $this->getVal($champ_pendant),
                "taille" => "",
                "max" => "",
            ),
            "doc_type_apres" => array (
                "colonne" => "doc_type_apres",
                "libelle" => "",
                "type" => "httpclick",
                "valeur" => $popup_apres,
                "contenu" => array(__("Inserer un document-type")),
            ),
            $champ_apres => array(
                "colonne" => $champ_apres,
                "libelle" => "",
                "type" => "html",
                "valeur" => $this->getVal($champ_apres),
                "taille" => "",
                "max" => "",
            ),
        );
        // S'il y a des valeurs postées on les échappe puis
        // on met à jour la base de données ou on rappelle le formulaire
        if ($this->getParameter("postvar") != null) {
            // On supprime les champs texte-type
            unset($champs['doc_type_pendant']);
            unset($champs['doc_type_apres']);
            // On récupère les valeurs postées
            $champs[$champ_pendant]["valeur"] = $this->encode_and_escape($champ_pendant);
            $champs[$champ_apres]["valeur"] = $this->encode_and_escape($champ_apres);
            // On met à jour la table
            if ($this->update_table($table, $champs, $id) != false) {
                // On affiche un message de validation
                $this->display_success($id);
            } else {
                // On affiche un message d'erreur
                $this->display_success($id, false);
            }
        } else { // sinon (aucune valeur postée) on affiche le formulaire
            // Initialisation du compteur validation
            $validation = 0;
            // Création du formulaire
            $liste_champs = array(
                "doc_type_pendant",
                $champ_pendant,
                "doc_type_apres",
                $champ_apres
            );
            $form = $this->f->get_inst__om_formulaire(array(
                "maj" => 1,
                "validation" => $validation,
                "champs" => $liste_champs,
            ));
            // Formatage des champs
            foreach ($champs as $champ => $values) {
                // libellé du champ
                $form->setLib($champ, $values["libelle"]);
                // widget formulaire du champ
                $form->setType($champ, $values["type"]);
                // cas des texte-type
                if ($values["type"] == "httpclick") {
                    $form->setSelect($champ, $values["contenu"]);
                }
                // si une taille a été définie
                if (isset($values["taille"])) {
                    $form->setTaille($champ, $values["taille"]);
                }
                // si un max a été défini
                if (isset($values["max"])) {
                    $form->setMax($champ, $values["max"]);
                }
                // valeur du champ
                $form->setVal($champ, $values["valeur"]);
            }
            // Bouton retour du haut
            $this->btn_retour();
            // Ouverture de la balise form
            $this->open_form_tag();
            // Entete
            $form->entete();
            // Champs
            $form->setBloc("doc_type_pendant", "D", "", "col_12");
            $form->setFieldset("doc_type_pendant", "D", $title);
            $form->setFieldset("doc_type_pendant", "D", __("Pendant"), "fieldset_pendant");
            $form->setFieldset("document_presente_pendant_om_html", "F");
            $form->setFieldset("doc_type_apres", "D", __("Apres"), "fieldset_apres");
            $form->setFieldset("document_presente_apres_om_html", "F");
            $form->setFieldset("document_presente_apres_om_html", "F");
            $form->setBloc("document_presente_apres_om_html", "F");
            $form->afficher($liste_champs, 0, false, false);
            // Boutons
            $form->enpied();
            $this->form_controls();
            // Fermeture du formulaire
            $this->close_form_tag();
        }
    }

    /**
     * VIEW - view_edit_essais_realises.
     *
     * Appelle le formulaire générique d'édition pour la liaison
     * entre analyse et essais réalisés, et traite ses valeurs postées.
     * 
     * @return void
     */
    function view_edit_essais_realises() {
        // Vérification de l'accessibilité sur l'élément
        $this->checkAccessibility();
        /*
            Initilisation des variables
         */
        // Légende du fieldset
        $title = __("Essais realises");
        // Récupération de l'ID de l'analyse
        $id = intval($this->getVal($this->clePrimaire));
        // Récupération de tous les essais réalisés avec des informations
        // supplémentaires pour ceux liés à l'analyse.
        // Filtre : sont récupérés les essais réalisés
        // - ayant le même service que l'analyse en cours
        // - ayant soit une date de validité correcte soit une liaison avec analyse
        $sql_select = "SELECT essai_realise.essai_realise as id,
            essai_realise.description as desc,
            CASE WHEN lien_essai_realise_analyses.lien_essai_realise_analyses IS NULL
                THEN 'f'
                ELSE 't'
            END as lien,
            essai_realise.libelle as lib,
            CASE WHEN lien_essai_realise_analyses.concluant IS NULL
                OR lien_essai_realise_analyses.concluant = 'f'
                THEN 'f'
                ELSE 't'
            END as conc,
            CASE WHEN lien_essai_realise_analyses.complement IS NULL
                THEN ''
                ELSE lien_essai_realise_analyses.complement
            END as comp
            FROM ".DB_PREFIXE."essai_realise
            LEFT JOIN ".DB_PREFIXE."lien_essai_realise_analyses
                ON lien_essai_realise_analyses.essai_realise = essai_realise.essai_realise
            AND lien_essai_realise_analyses.analyses = ".$id."
            WHERE essai_realise.service = ".$this->getVal("service")."
            AND (
                ((essai_realise.om_validite_debut IS NULL AND (essai_realise.om_validite_fin IS NULL OR essai_realise.om_validite_fin > CURRENT_DATE)) OR (essai_realise.om_validite_debut <= CURRENT_DATE AND (essai_realise.om_validite_fin IS NULL OR essai_realise.om_validite_fin > CURRENT_DATE)))
                OR essai_realise.essai_realise = lien_essai_realise_analyses.essai_realise)
            ORDER BY essai_realise.libelle ASC";
        $res = $this->f->db->query($sql_select);
        $this->f->addToLog(__METHOD__."(): db->query(\"".$sql_select."\");", VERBOSE_MODE);
        if ($this->f->isDatabaseError($res, true)) {
            $this->erreur_db($res->getDebugInfo(), $res->getMessage(), '');
            $this->correct = false;
            return false;
        }
        $essais_realises = array();
        while($row=& $res->fetchRow(DB_FETCHMODE_ASSOC)){
            $essai_realise = array(
                "id" => intval($row["id"]),
                "lien" => $row["lien"],
                "lib" => $row["lib"],
                "conc" => $row["conc"],
                "comp" => $row["comp"],
                "desc" => $row["desc"],
            );
            $essais_realises[] = $essai_realise;
        }
        // Champ de la table à mettre à jour
        $champs = array(
            "essai_realise" => array(
                "colonne" => "essai_realise",
                "libelle" => "",
                "type" => "essai_realise",
                "valeur" => $essais_realises,
            ),
        );
        // S'il y a des valeurs postées on met à jour la base de données
        if ($this->getParameter("postvar") != null) {
            $essais = null;
            $essais_realises = array();
            $postedValues = $this->getParameter("postvar");
            // Récupération des essais réalisés s'il y en a
            if (isset($postedValues["essai"])) {
                $essais = $postedValues["essai"];
                $conc = $postedValues["conc"];
                $comp = $postedValues["comp"];
                foreach ($essais as $key => $value) {
                    if ($value == 't') {
                        $essais_realises[] = array (
                            "id" => $key,
                            "conc" => $this->encode_and_escape($conc[$key], true),
                            "comp" => $this->encode_and_escape($comp[$key], true)
                        );
                    }
                }
            }
            // on met à jour la table de liaison
            if ($this->update_lien_essai_realise_analyses($essais_realises, $id) != false) {
                // On affiche un message de validation
                $this->display_success($id);
            } else {
                // On affiche un message d'erreur
                $this->display_success($id, false);
            }
        } else {
            // sinon (aucune valeur postée) on affiche le formulaire sans message
            $this->display_generic_edit_form($champs, $id, $title);
        }
    }

    /**
     * VIEW - view_edit_compte_rendu.
     *
     * Appelle le formulaire générique d'édition pour le champ
     * compte_rendu_om_html et traite ses valeurs postées.
     * 
     * @return void
     */
    function view_edit_compte_rendu() {
        // Vérification de l'accessibilité sur l'élément
        $this->checkAccessibility();
        // Légende du fieldset
        $title = __("Compte-rendu");
        // Définition de la table mise à jour
        $table = $this->table;
        // Récupération de l'ID de l'analyse
        $id = $this->getVal($this->clePrimaire);
        // Champ de la table à mettre à jour
        $champs = array(
            "compte_rendu_om_html" => array(
                "colonne" => "compte_rendu_om_html",
                "libelle" => "",
                "type" => "html",
                "valeur" => $this->getVal("compte_rendu_om_html"),
                "taille" => "",
                "max" => "",
            ),
        );
        // S'il y a des valeurs postées on les échappe puis
        // on met à jour la base de données ou on rappelle le formulaire
        if ($this->getParameter("postvar") != null) {
            $champs["compte_rendu_om_html"]["valeur"] = $this->encode_and_escape("compte_rendu_om_html");
            // On met à jour la table
            if ($this->update_table($table, $champs, $id) != false) {
                // On affiche un message de validation
                $this->display_success($id);
            } else {
                // On affiche un message d'erreur
                $this->display_success($id, false);
            }
        } else {
            // sinon (aucune valeur postée) on affiche le formulaire sans message
            $this->display_generic_edit_form($champs, $id, $title);
        }
    }

    /**
     * VIEW - view_edit_observation.
     *
     * Appelle le formulaire générique d'édition pour le champ
     * observation_om_html et traite ses valeurs postées.
     * 
     * @return void
     */
    function view_edit_observation() {
        // Vérification de l'accessibilité sur l'élément
        $this->checkAccessibility();
        // Légende du fieldset
        $title = __("Observation");
        // Définition de la table mise à jour
        $table = $this->table;
        // Récupération de l'ID de l'analyse
        $id = $this->getVal($this->clePrimaire);
        // Champ de la table à mettre à jour
        $champs = array(
            "observation_om_html" => array(
                "colonne" => "observation_om_html",
                "libelle" => "",
                "type" => "html",
                "valeur" => $this->getVal("observation_om_html"),
                "taille" => "",
                "max" => "",
            ),
        );
        // S'il y a des valeurs postées on les échappe puis
        // on met à jour la base de données ou on rappelle le formulaire
        if ($this->getParameter("postvar") != null) {
            $champs["observation_om_html"]["valeur"] = $this->encode_and_escape("observation_om_html");
            // On met à jour la table
            if ($this->update_table($table, $champs, $id) != false) {
                // On affiche un message de validation
                $this->display_success($id);
            } else {
                // On affiche un message d'erreur
                $this->display_success($id, false);
            }
        } else {
            // sinon (aucune valeur postée) on affiche le formulaire sans message
            $this->display_generic_edit_form($champs, $id, $title);
        }
    }
    /**
     * VIEW - view_edit_avis_propose.
     *
     * Appelle le formulaire générique d'édition pour les champs analyses_avis
     * et avis_complement, puis traite ses valeurs postées.
     * 
     * @return void
     */
    function view_edit_avis_propose() {
        // Vérification de l'accessibilité sur l'élément
        $this->checkAccessibility();
        /*
            Initilisation des variables
         */
        // Légende du fieldset
        $title = __("Avis propose");
        // Avant les vérifications on considère le formulaire valide
        $correct = true;
        // Pour la même raison on crée un message d'erreur vide
        $message = "";
        // Définition de la table mise à jour
        $table = $this->table;
        // Récupération de l'ID de l'analyse
        $id = $this->getVal($this->clePrimaire);
        // Cas du select
        $id_service = $this->getVal("service");
        $id_avis = $this->getVal("reunion_avis");
        $sql_select = "SELECT reunion_avis.reunion_avis, reunion_avis.libelle
            FROM ".DB_PREFIXE."reunion_avis
            WHERE reunion_avis.service = ".$id_service."
            AND (
                (((reunion_avis.om_validite_debut IS NULL
                AND (reunion_avis.om_validite_fin IS NULL
                OR reunion_avis.om_validite_fin > CURRENT_DATE))
                OR (reunion_avis.om_validite_debut <= CURRENT_DATE
                AND (reunion_avis.om_validite_fin IS NULL
                OR reunion_avis.om_validite_fin > CURRENT_DATE))))";
        if ($id_avis != "") {
            $sql_select .= "OR reunion_avis.reunion_avis =".$id_avis;
        }
        $sql_select .=") 
            ORDER BY reunion_avis.libelle ASC";
        // On crée le contenu du select
        $res = $this->f->db->query($sql_select);
        $this->f->addToLog(__METHOD__."(): db->query(\"".$sql_select."\");", VERBOSE_MODE);
        // Si la construction du select échoue
        if ($this->f->isDatabaseError($res, true)) {
            // Appel de la methode de recuperation des erreurs
            $this->erreur_db($res->getDebugInfo(), $res->getMessage(), '');
            $this->correct = false;
            return false;
        }
        $k=1;
        $contenu = array();
        $contenu[0][0] = '';
        $contenu[1][0] = __("choisir")." ".__("analyses_avis");
        while($row = &$res->fetchRow()){
            $contenu[0][$k] = $row[0];
            $contenu[1][$k] = $row[1];
            $k++;
        }
        // Champ de la table à mettre à jour
        $champs = array(
            "reunion_avis" => array(
                "colonne" => "reunion_avis",
                "libelle" => __("analyses_avis"),
                "type" => "select",
                "contenu" => $contenu,
                "valeur" => $this->getVal("reunion_avis"),
            ),
            "avis_complement" => array(
                "colonne" => "avis_complement",
                "libelle" => __("avis_complement"),
                "type" => "text",
                "valeur" => $this->getVal("avis_complement"),
                "taille" => "",
                "max" => "250",
            ),
        );
        /*
            Traitement d'éventuelles valeurs postées
         */
        // S'il y a des valeurs postées on les vérifie, puis le cas échéant
        // on met à jour la base de données ou on rappelle le formulaire
        if ($this->getParameter("postvar") != null) {
            // Si la valeur est incohérente
            // alors la validation du formulaire a échoué
            // sinon on l'échappe s'il s'agit d'une chaîne de caractères
            // 
            // Cas analyses_avis
            $reunion_avis = $this->encode_and_escape("reunion_avis");
            if (!(is_numeric($reunion_avis)) || ($reunion_avis == 0)) {
                // alors la validation du formulaire a échoué
                $correct = false;
                $message .= __("L'avis d'analyse renseigne est invalide.");
                $champs["reunion_avis"]["valeur"] = "";
            } else {
                // sinon on récupère la valeur postée
                $champs["reunion_avis"]["valeur"] = intval($reunion_avis);
            }
            // Cas avis_complement
            $champs["avis_complement"]["valeur"] = $this->encode_and_escape("avis_complement");
            // On met à jour la table
            // Si le formulaire est valide
            if ($correct == true) {
                // On met à jour la table
                if ($this->update_table($table, $champs, $id) != false) {
                    // On affiche un message de validation
                    $this->display_success($id);
                } else {
                    // On affiche un message d'erreur
                    $this->display_success($id, false);
                }
            } else {
                // Sinon on réaffiche le formulaire
                // avec le message d'erreur et les valeurs postées
                $this->display_generic_edit_form($champs, $id, $title, $message);
            }
        } else {
            // sinon (aucune valeur postée) on affiche le formulaire sans message
            $this->display_generic_edit_form($champs, $id, $title);
        }
    }

    /**
     * VIEW - view_overlay_reglementation_applicable_type.
     *
     * Formulaire de sélection de textes-types.
     * 
     * @return void
     */
    function view_overlay_reglementation_applicable_type(){
        // Vérification de l'accessibilité sur l'élément
        $this->checkAccessibility();

        /**
         *
         */
        // Instanciation du dossier d'instruction
        $di = $this->f->get_inst__om_dbform(array(
            "obj" => "dossier_instruction",
            "idx" => $this->getVal("dossier_instruction"),
        ));
        // Instanciation du dossier de coordination
        $dc = $this->f->get_inst__om_dbform(array(
            "obj" => "dossier_coordination",
            "idx" => $di->getVal("dossier_coordination"),
        ));

        // Formatage des valeurs récupérées
        $dc_categorie = $dc->getVal("etablissement_categorie");
        $dc_type = $dc->getVal("etablissement_type");
        $dc_types = $dc->getVal("etablissement_type_secondaire");
        // Liste des types
        $dc_types = array();
        if ($dc->getVal("etablissement_type_secondaire") != "") {
            // Ajout des types secondaires s'il y en a au moins un
            $dc_types = explode(';', $dc->getVal("etablissement_type_secondaire"));
        }
        if ($dc->getVal("etablissement_type") != '') {
            // Ajout du type principal s'il y en a un
            $dc_types[] = $dc->getVal("etablissement_type");
        }

        // Condition service dans tous les cas
        $where_service = " WHERE reglementation_applicable.service = ".$this->getVal("service");

        // Condition catégorie si le DC en a une
        $where_categorie = "";
        if ($dc_categorie != "") {
            $where_categorie = " AND lien_reglementation_applicable_etablissement_categorie.etablissement_categorie = ".$dc_categorie;
        }

        // Condition type si le DC en a au moins un
        $where_types = "";
        if (!empty($dc_types)) {
            $dc_types = implode(',', $dc_types);
            $where_types = " AND lien_reglementation_applicable_etablissement_type.etablissement_type IN (".$dc_types.")";
        }

        // Condition date de validité
        $where_validite = " AND ((reglementation_applicable.om_validite_debut IS NULL
            AND (reglementation_applicable.om_validite_fin IS NULL
            OR reglementation_applicable.om_validite_fin > CURRENT_DATE))
            OR (reglementation_applicable.om_validite_debut <= CURRENT_DATE
            AND (reglementation_applicable.om_validite_fin IS NULL
            OR reglementation_applicable.om_validite_fin > CURRENT_DATE)))";

        // Requête de récupération des réglementations applicables
        $sql = "SELECT DISTINCT reglementation_applicable.reglementation_applicable as reglementation_applicable, 
                reglementation_applicable.libelle as libelle,
                reglementation_applicable.description as description, 
                reglementation_applicable.annee_debut_application as annee_debut_application, 
                reglementation_applicable.annee_fin_application as annee_fin_application
            FROM ".DB_PREFIXE."reglementation_applicable 
            LEFT JOIN ".DB_PREFIXE."lien_reglementation_applicable_etablissement_categorie
                ON reglementation_applicable.reglementation_applicable = lien_reglementation_applicable_etablissement_categorie.reglementation_applicable
            LEFT JOIN ".DB_PREFIXE."lien_reglementation_applicable_etablissement_type
                ON reglementation_applicable.reglementation_applicable = lien_reglementation_applicable_etablissement_type.reglementation_applicable
            LEFT JOIN ".DB_PREFIXE."etablissement_categorie
                ON lien_reglementation_applicable_etablissement_categorie.etablissement_categorie=etablissement_categorie.etablissement_categorie
            LEFT JOIN ".DB_PREFIXE."etablissement_type 
                ON lien_reglementation_applicable_etablissement_type.etablissement_type=etablissement_type.etablissement_type
            LEFT JOIN ".DB_PREFIXE."dossier_coordination
                ON etablissement_categorie.etablissement_categorie=dossier_coordination.etablissement_categorie
            LEFT JOIN ".DB_PREFIXE."dossier_instruction
                ON dossier_coordination.dossier_coordination=dossier_instruction.dossier_coordination";
        // Condition
        $sql .=  $where_service.$where_categorie.$where_types.$where_validite;
        // Tri
        $sql .= " ORDER BY reglementation_applicable.libelle";

        // Exécution de la requête
        $res = $this->f->db->query($sql);
        // Log
        $this->f->addToLog(__METHOD__."(): db->query(\"".$sql."\");", VERBOSE_MODE);
        if ($this->f->isDatabaseError($res,true)){
            $this->erreur_db($res->getDebugInfo(), $res->getMessage(), '');
            die();
        }

        /**
         *
         */
        // titre de l'overlay
        $this->display_overlay_title(
            __("liste des reglementations-types")
        );

        /**
         *
         */
        //
        if ($res->numrows() == 0) {
            //
            $message_class = "error";
            $message = __("Aucune reglementation-type trouvee.");
            $this->f->displayMessage($message_class, $message);
            //
            printf('
                <div class="formControls">
            ');
            // Bouton fermer
            $this->f->layout->display_form_button(array(
                "value" => __("Revenir a l'edition"),
                "onclick" => "$('#form-analyses-overlay').remove();",
            ));
            // Fermeture de form controls
            printf('
                </div>
            ');
            //
            return "";
        }

        /**
         *
         */
        //
        echo "
            <table id=\"tab-text-type_reglementation_applicable\" >
                <tr class=\"ui-tabs-nav ui-accordion ui-state-default tab-title\">
                    <th>".__("Choisir")."</th>
                    <th>".__("Libelle")."</th>
                    <th class=\"cell_description\">".__("Description")."</th>
                    <th>".__("Debut d'application")."</th>
                    <th>".__("Fin d'application")."</th>
                </tr>";
        //
        $class="odd";
        //Template DOM
        $template= "
            <tr class=\"%1\$s\">
                <td> <input type=\"checkbox\" value=\"%2\$s\" id=\"checkbox_%2\$s\" /> </td>
                <td> <span title=\"%3\$s\" id=\"libelle_\"%2\$s\">%3\$s</span> </td>
                <td class=\"cell_description\"> <span title=\"%4\$s\" id=\"description_%2\$s\">%4\$s</span> </td>
                <td> <span>%5\$s</span> </td>
                <td> <span>%6\$s</span> </td>
            </tr>";
        //Formatage des libellés
         while($reglementation_applicable =& $res->fetchRow(DB_FETCHMODE_ASSOC)){
            // Tous le html ce situe dans template et seulement les arguments reste dans le
            printf ($template, $class, 
                $reglementation_applicable['reglementation_applicable'], 
                htmlentities($reglementation_applicable['libelle']), 
                htmlentities($reglementation_applicable['description']), 
                $reglementation_applicable['annee_debut_application'], 
                $reglementation_applicable['annee_fin_application']
                );
            //
            $class=($class=="even")?"odd":"even";
            
        }

        echo "
            </table>
            <div class=\"formControls\">";
        $this->f->layout->display_form_button(array(
            "value" => __("Valider"),
            "onclick" => "add_description_type('reglementation_applicable_om_html', 'form-analyses-overlay', false); return false;",
         ));
        $this->f->displayLinkJsCloseWindow("$('#form-analyses-overlay').remove();");
        echo "</div>";

    }

    /**
     * VIEW - view_overlay_document_presente_type.
     *
     * Formulaire de sélection de textes-types.
     * 
     * @return void
     */
    function view_overlay_document_presente_type() {
        // Vérification de l'accessibilité sur l'élément
        $this->checkAccessibility();

        // Récupération de l'ID de l'analyse
        $id = intval($this->getVal($this->clePrimaire));
        // Récupération de l'ID CSS du champ qui appelle cette vue
        $id_css_champ = $_GET["champ"];
        // Déclaration de l'ID CSS du conteneur dialog
        $id_css_conteneur = "overlay_document_presente_type";
        // Récupération de tous les documents présentés valides du service adéquat
        $sql = "
        SELECT
            document_presente as id,
            libelle as lib,
            description_om_html as desc
        FROM 
            ".DB_PREFIXE."document_presente
        WHERE 
            service = ".$this->getVal("service")."
            AND ((om_validite_debut IS NULL
                AND (om_validite_fin IS NULL
                OR om_validite_fin > CURRENT_DATE))
                OR (om_validite_debut <= CURRENT_DATE
                AND (om_validite_fin IS NULL
                OR om_validite_fin > CURRENT_DATE)))
        ORDER BY 
            libelle ASC
        ";
        $res = $this->f->db->query($sql);
        $this->f->addToLog(__METHOD__."(): db->query(\"".$sql."\");", VERBOSE_MODE);
        if ($this->f->isDatabaseError($res, true)) {
            $this->erreur_db($res->getDebugInfo(), $res->getMessage(), '');
            $this->correct = false;
            return false;
        }

        $documents = array();
        $liste_champs = array();
        $checkboxs = array();
        $libelles = array();
        $descriptions = array();
        $i = 0;
        while($row=& $res->fetchRow(DB_FETCHMODE_ASSOC)){
            $id_doc = intval($row["id"]);
            $document = array(
                "id" => $id_doc,
                "lib" => $row["lib"],
                "desc" => $row["desc"],
            );
            $documents[] = $document;
            $checkboxs[] = "checkbox_".$id_doc;
            $libelles[] = "libelle_".$id_doc;
            $descriptions[] = "description_".$id_doc;
            $i++;
        }

        /**
         *
         */
        // titre de l'overlay
        $this->display_overlay_title(
            __("liste des documents-types")
        );

        /**
         *
         */
        if ($i == 0) {
            //
            $message_class = "error";
            $message = __("Aucun document-type trouve.");
            $this->f->displayMessage($message_class, $message);
            //
            printf('
                <div class="formControls">
            ');
            // Bouton fermer
            $this->f->layout->display_form_button(array(
                "value" => __("Fermer"),
                "onclick" => "$('#".$id_css_conteneur."').remove();",
            ));
            // Fermeture de form controls
            printf('
                </div>
            ');
            //
            return "";
        }

        /**
         *
         */
        // Ouverture formulaire
        printf('
            <form name="f2" action="" method="post">
        ');
        // Ouverture du tableau
        printf('<table class="tab-tab">');
        // Entête de tableau
        $template_tab_header = '
            <thead><tr class="ui-tabs-nav ui-accordion ui-state-default tab-title">
                <th class="title col-0 marge_essai_droite">
                </th>
                <th class="title col-1">
                    <span class="name">%s</span>
                </th>
                <th class="title col-2 lastcol cell_description">
                </th>
            </tr></thead>';
        printf(
            $template_tab_header,
            __('libelle')
        );
        // Corps du tableau
        printf('<tbody>');
        // Affiche chacun des essais
        foreach ($documents as $document => $valeurs) {
            // action JS lorsque l'on (dé)coche un document
            $coche = "if (this.checked) this.value='t'; else this.value='f';";
            // ouverture de la ligne
            printf('<tr class="tab-data odd">');
            // checkbox de sélection du document
            printf('<td class="col-0 icons marge_essai_droite">
                <input id="checkbox_%s" type="checkbox"
                    onclick="%s" name="checkbox[%s]" /></td>',
                $valeurs["id"],
                $coche,
                $valeurs["id"]
            );
            // libellé du document
            printf('<td class="col-1">
                <span id="libelle_%s">%s</span></td>',
                $valeurs["id"],
                $valeurs['lib']
            );
            // description cachée
            printf('<td class="col-2 lastcol cell_description">
                <span id="description_%s" class="%s">%s</span></td>',
                $valeurs["id"],
                "cell_description",
                $valeurs["desc"]
            );
            // fermeture de la ligne
            printf('</tr>');
        }
        // fermeture du tableau
        printf('</tbody></table>');
        //
        printf('
            <div class="formControls btn_ajouter_overlay">
        ');
        // Bouton ajouter
        $this->f->layout->display_form_button(array(
            "value" => __("Ajouter"),
            "name" => "add",
        ));
        // Bouton annuler
        $this->f->layout->display_form_button(array(
            "value" => __("Annuler"),
            "name" => "cancel",
        ));
        // Fermeture de form controls
        printf('
            </div>
        ');
        // Fermeture du formulaire
        printf('
            </form>
        ');
    }

    /**
     * VIEW - view_overlay_prescription_specifique_type.
     *
     * Formulaire de sélection de textes-types.
     * 
     * @return void
     */
    function view_overlay_prescription_specifique_type() {
        // Vérification de l'accessibilité sur l'élément
        $this->checkAccessibility();

        /**
         *
         */
        // Récupération de l'ID de la prescription réglementaire
        $postedValues = $this->getParameter("postvar");
        $id_pr = $postedValues["id_pr"];
        // ID CSS du champ qui appelle cette vue
        $id_css_champ = "ps_description_om_html";
        // Déclaration de l'ID CSS du conteneur dialog
        $id_css_conteneur = "form_specifiques_types_overlay";

        /**
         *
         */
        //
        $prescription_reglementaire_inst = $this->f->get_inst__om_dbform(array(
            "obj" => "prescription_reglementaire",
            "idx" => $id_pr,
        ));
        $prescriptions_specifiques = $prescription_reglementaire_inst->get_prescriptions_specifiques();

        /**
         *
         */
        // titre de l'overlay
        $this->display_overlay_title(
            __("Liste des prescriptions specifiques-types")
        );

        /**
         *
         */
        if (count($prescriptions_specifiques) == 0) {
            //
            $message_class = "error";
            $message = __("Aucune prescription-type trouvee.");
            $this->f->displayMessage($message_class, $message);
            //
            printf('
                <div class="formControls">
            ');
            // Bouton fermer
            $this->f->layout->display_form_button(array(
                "value" => __("Fermer"),
                "onclick" => "$('#".$id_css_conteneur."').remove();",
            ));
            // Fermeture de form controls
            printf('
                </div>
            ');
            //
            return "";
        }

        /**
         *
         */
        // Ouverture formulaire
        printf('
            <form name="f2" action="" method="post">
        ');
        // Ouverture du tableau
        printf('<table class="tab-tab">');
        // Entête de tableau
        $template_tab_header = '
            <thead><tr class="ui-tabs-nav ui-accordion ui-state-default tab-title">
                <th class="title col-0 marge_essai_droite">
                </th>
                <th class="title col-1">
                    <span class="name">%s</span>
                </th>
                <th class="title col-2 lastcol cell_description">
                </th>
            </tr></thead>';
        printf(
            $template_tab_header,
            __('libelle')
        );
        // Corps du tableau
        printf('<tbody>');
        // Affiche chacun des essais
        foreach ($prescriptions_specifiques as $prescription => $valeurs) {
            // action JS lorsque l'on (dé)coche une prescription
            $coche = "if (this.checked) this.value='t'; else this.value='f';";
            // ouverture de la ligne
            printf('<tr class="tab-data odd">');
            // checkbox de sélection de la prescription
            printf('<td class="col-0 icons marge_essai_droite">
                <input id="checkbox_%s" type="checkbox"
                    onclick="%s" name="checkbox[%s]" /></td>',
                $valeurs["id"],
                $coche,
                $valeurs["id"]
            );
            // libellé de la prescription
            printf('<td class="col-1">
                <span id="libelle_%s">%s</span></td>',
                $valeurs["id"],
                $valeurs['lib']
            );
            // description cachée
            printf('<td class="col-2 lastcol cell_description">
                <span id="description_%s" class="%s">%s</span></td>',
                $valeurs["id"],
                "cell_description",
                $valeurs["desc"]
            );
            // fermeture de la ligne
            printf('</tr>');
        }
        // fermeture du tableau
        printf('</tbody></table>');
        //
        printf('
            <div class="formControls btn_ajouter_overlay">
        ');
        // Bouton ajouter
        $this->f->layout->display_form_button(array(
            "value" => __("Ajouter"),
            "name" => "add",
        ));
        // Bouton annuler
        $this->f->layout->display_form_button(array(
            "value" => __("Annuler"),
            "name" => "cancel",
        ));
        // Fermeture de form controls
        printf('
            </div>
        ');
        // Fermeture du formulaire
        printf('
            </form>
        ');
    }

    /**
     * VIEW - view_overlay_add_prescription.
     *
     * Appelle l'ajout de prescriptions
     * 
     * @return void
     */
    function view_overlay_add_prescription() {
        // Vérification de l'accessibilité sur l'élément
        $this->checkAccessibility();

        // titre de l'overlay
        $this->display_overlay_title(
            __("ajouter une prescription")
        );

        // Récupération du prochain ordre pour la nouvelle prescription
        $postedValues = $this->getParameter("postvar");
        $ordre = $postedValues["ordre"];
        // Récupération du select des prescriptions réglementaires
        $select_pr = $this->get_prescriptions_reglementaires();
        if ($select_pr == false) {
            return false;
        }

        // Lien popup liste des prescriptions spécifiques-types
        $champs = array(
            // 
            "prochain_ordre" => array(
                "colonne" => "prochain_ordre",
                "libelle" => "",
                "type" => "hidden",
                "valeur" => $ordre,
            ),
            //
            "prescription_reglementaire" => array(
                "colonne" => "prescription_reglementaire",
                "libelle" => "",//__("prescription_reglementaire"),
                "type" => "select",
                "valeur" => "",
                "contenu" => $select_pr,
                "onchange" => "get_prescription_reglementaire(this.value)",
            ),
            //
            "pr_description_om_html" => array(
                "colonne" => "pr_description_om_html",
                "libelle" => "",//__("pr_description_om_html"),
                "type" => "textareastatic",
                "valeur" => "",
            ),
            //
            "pr_defavorable" => array(
                "colonne" => "pr_defavorable",
                "libelle" => __("defavorable ?"),
                "type" => "checkbox",
                "valeur" => "",
                "taille" => "",
                "max" => "",
            ),
            //
            "ps_description_om_html" => array(
                "colonne" => "ps_description_om_html",
                "libelle" => "",//__("description specifique"),
                "type" => "html",
                "valeur" => '',
                "taille" => "",
                "max" => "",
            ),
            //
            "specifique-type" => array (
                "colonne" => "texte-type",
                "libelle" => "",
                "type" => "httpclick",
                "valeur" => "overlay_prescription_specifique_type();",
                "contenu" => array(__("Inserer une ou plusieurs prescription(s) specifique(s)-type(s)")),
            ),
        );
        $this->display_overlay_prescription("add", $champs);
    }

    /**
     * VIEW - view_overlay_edit_prescription.
     *
     * Appelle l'ajout de prescriptions
     * 
     * @return void
     */
    function view_overlay_edit_prescription() {
        // Vérification de l'accessibilité sur l'élément
        $this->checkAccessibility();
        // titre de l'overlay
        $this->display_overlay_title(__("modifier une prescription"));
        // Récupération des valeurs de la prescription
        $postedValues = $this->getParameter("postvar");
        $ordre = $postedValues["ordre"];
        $pr_id = $postedValues["pr_id"];
        $pr_def = $postedValues["pr_def"];
        $pr_desc = $postedValues["pr_desc"];
        $ps_desc = $postedValues["ps_desc"];
        // Récupération du select des prescriptions réglementaires
        $select_pr = $this->get_prescriptions_reglementaires($pr_id);
        if ($select_pr == false) {
            return false;
        }
        // Liste des champs du formulaire
        $champs = array(
            "prochain_ordre" => array(
                "colonne" => "prochain_ordre",
                "libelle" => "",
                "type" => "hidden",
                "valeur" => $ordre,
            ),
            "prescription_reglementaire" => array(
                "colonne" => "prescription_reglementaire",
                "libelle" => "",
                "type" => "select",
                "valeur" => $pr_id,
                "contenu" => $select_pr,
                "onchange" => "get_prescription_reglementaire(this.value)",
            ),

            "pr_description_om_html" => array(
                "colonne" => "pr_description_om_html",
                "libelle" => "",//__("pr_description_om_html"),
                "type" => "textareastatic",
                "valeur" => $pr_desc,
            ),
            "pr_defavorable" => array(
                "colonne" => "pr_defavorable",
                "libelle" => __("pr_defavorable"),
                "type" => "checkbox",
                "valeur" => $pr_def,
                "taille" => "",
                "max" => "",
            ),
            //
            "ps_description_om_html" => array(
                "colonne" => "ps_description_om_html",
                "libelle" => "",
                "type" => "html",
                "valeur" => $ps_desc,
                "taille" => "",
                "max" => "",
            ),
            //
            "specifique-type" => array (
                "colonne" => "texte-type",
                "libelle" => "",
                "type" => "httpclick",
                "valeur" => "overlay_prescription_specifique_type();",
                "contenu" => array(__("Inserer une ou plusieurs prescription(s) specifique(s)-type(s)")),
            ),
        );
        $this->display_overlay_prescription("edit", $champs);
    }

    /**
     * METHOD - update_table.
     * 
     * Met à jour une table donnée
     *
     * @param  [string]  $table         identifiant de la table dans la BDD
     * @param  [array]   $champs        tableau des champs (colonne et valeur)
     * @param  [string]  $id            valeur de la clé primaire
     * @return [boolean]                Vrai si table mise à jour avec succès
     */
    function update_table($table, $champs, $id) {
        //Vérification des paramètres
        if ($table!='' && is_array($champs) && count($champs)>0 && $id!='') {
            // Données
            $valF = array();
            foreach ($champs as $champ => $valeurs) {
                $valF[$valeurs["colonne"]]=$valeurs["valeur"];
            }
            //Mise à jour
            $res = $this->f->db->autoExecute(DB_PREFIXE.$table, $valF, DB_AUTOQUERY_UPDATE, $this->getCle($id));
            // Logger
            $this->f->addToLog(__METHOD__."(): db->autoExecute(\"".DB_PREFIXE.$table."\", ".print_r($valF, true).", DB_AUTOQUERY_UPDATE, \"".$this->getCle($id)."\")", VERBOSE_MODE);
            // Si une erreur survient
            if ($this->f->isDatabaseError($res, true)) {
                // Appel de la methode de recuperation des erreurs
                $this->erreur_db($res->getDebugInfo(), $res->getMessage(), '');
                $this->correct = false;
                return false;
            }
            return true;
        }
        return false;
    }

    /**
     * METHOD - update_prescription.
     * 
     * Met à jour les prescriptions d'une analyse
     *
     * @param  [array]   $prescriptions prescriptions à insérer
     * @param  [string]  $id            ID de l'analyse
     * @return [boolean]                Vrai si table mise à jour avec succès
     */
    function update_prescription($prescriptions, $id) {
        // Désactivation de l'autocommit
        $this->f->db->autoCommit(false);
        $correct = true;
        // Définition de la table mise à jour
        $table = "prescription";
        // Suppression de toutes les prescriptions de l'analyse
        $sql_delete = "DELETE FROM ".DB_PREFIXE.$table."
            WHERE analyses = ".$id;
        // Si la suppression échoue
        $res_delete = $this->f->db->query($sql_delete);
        $this->f->addToLog(__METHOD__."(): db->query(\"".$sql_delete."\");", VERBOSE_MODE);
        if ($this->f->isDatabaseError($res_delete, true)) {
            // Appel de la methode de recuperation des erreurs
            $this->erreur_db($res_delete->getDebugInfo(), $res_delete->getMessage(), '');
            $correct = false;
        }
        // Ajout des nouvelles prescriptions s'il y en a
        if (count($prescriptions) > 0) {
            foreach ($prescriptions as $prescription) {
                $pr_id = (
                    $prescription["pr_id"] == "" ?
                    'null' : $prescription["pr_id"]
                );
                $pr_def = (
                    $prescription["pr_id"] == "" ?
                    'null' : "'".$prescription["pr_def"]."'"
                );
                $sql_insert = "INSERT INTO ".DB_PREFIXE.$table."
                    (prescription, analyses, ordre,
                        prescription_reglementaire,
                        pr_description_om_html,
                        pr_defavorable,
                        ps_description_om_html)
                    VALUES (nextval('".DB_PREFIXE.$table."_seq'),
                            ".$id.",
                            ".$prescription["ord"].",
                            ".$pr_id.",
                            '".str_replace("'","''",$prescription["pr_desc"])."',
                            ".$pr_def.",
                            '".str_replace("'","''",$prescription["ps_desc"])."')";
                $res_insert = $this->f->db->query($sql_insert);
                $this->f->addToLog(__METHOD__."(): db->query(\"".$sql_insert."\");", VERBOSE_MODE);
                // Si la mise à jour de la table échoue
                if ($this->f->isDatabaseError($res_insert, true)) {
                    // Appel de la methode de recuperation des erreurs
                    $this->erreur_db($res_insert->getDebugInfo(), $res_insert->getMessage(), '');
                    $correct = false;
                }
            }
        }        
        if ($correct == true) {
            $this->f->db->commit();
            return true;
        } else {
            $this->undoValidation();
            return false;
        }
    }

    /**
     * METHOD - update_lien_essai_realise_analyses.
     * 
     * Met à jour la table de liaison lien_essai_realise_analyses
     *
     * @param  [array]   $liens         liens à insérer
     * @param  [string]  $id            ID de l'analyse
     * @return [boolean]                Vrai si table mise à jour avec succès
     */
    function update_lien_essai_realise_analyses($liens, $id) {
        // Désactivation de l'autocommit
        $this->f->db->autoCommit(false);
        $correct = true;
        // Définition de la table mise à jour
        $table = "lien_essai_realise_analyses";
        // Suppression des liens établis
        $sql_delete = "DELETE FROM ".DB_PREFIXE.$table."
            WHERE analyses = ".$id;
        // Si la suppression échoue
        $res_delete = $this->f->db->query($sql_delete);
        $this->f->addToLog(__METHOD__."(): db->query(\"".$sql_delete."\");", VERBOSE_MODE);
        if ($this->f->isDatabaseError($res_delete, true)) {
            // Appel de la methode de recuperation des erreurs
            $this->erreur_db($res_delete->getDebugInfo(), $res_delete->getMessage(), '');
            $correct = false;
        }
        // Ajout des nouveaux liens s'il y en a
        if (count($liens) > 0) {
            foreach ($liens as $lien) {
                $sql_insert = "INSERT INTO ".DB_PREFIXE.$table."
                    (lien_essai_realise_analyses, essai_realise, analyses,
                        concluant, complement)
                    VALUES (nextval('".DB_PREFIXE.$table."_seq'),
                            ".$lien["id"].",
                            ".$id.",
                            '".$lien["conc"]."',
                            '".str_replace("'","''",$lien["comp"])."')";
                $res_insert = $this->f->db->query($sql_insert);
                $this->f->addToLog(__METHOD__."(): db->query(\"".$sql_insert."\");", VERBOSE_MODE);
                // Si la mise à jour de la table échoue
                if ($this->f->isDatabaseError($res_insert, true)) {
                    // Appel de la methode de recuperation des erreurs
                    $this->erreur_db($res_insert->getDebugInfo(), $res_insert->getMessage(), '');
                    $correct = false;
                }
            }
        }        
        if ($correct == true) {
            $this->f->db->commit();
            return true;
        } else {
            $this->undoValidation();
            return false;
        }
    }

    /**
     * Formulaire générique d'édition de champs de la table métier analyse
     * ou de ceux d'une table de paramétrage.
     * 
     * @param  [array]    $champs      tableau des champs avec leur formatage
     * @param  [integer]  $id_analyse  ID de l'analyse modifiée
     * @param  [string]   $title       légende du fieldset
     * @param  [string]   $message     message d'erreur (facultatif)
     * @return void
     */
    function display_generic_edit_form($champs, $id_analyse, $title, $message = "") {
        // Tableau des widgets form nécessitant du contenu
        $widgets_need_content = array(
            "select",
            "select_multiple",
            "httpclick",
        );
        // Initialisation du compteur validation
        $validation = 0;
        // S'il y a un message d'erreur on l'affiche
        if ($message != "") {
            $validation = 1;
            $this->correct = false;
            $this->f->displayMessage("error", $message);
        }
        // Création du formulaire
        $liste_champs = array();
        foreach ($champs as $champ => $values) {
            $liste_champs[] = $champ;
        }
        $form = $this->f->get_inst__om_formulaire(array(
            "maj" => 1,
            "validation" => $validation,
            "champs" => $liste_champs,
        ));
        // Formatage des champs
        foreach ($champs as $champ => $values) {
            // libellé du champ
            $form->setLib($champ, $values["libelle"]);
            // widget formulaire du champ
            $form->setType($champ, $values["type"]);
            // s'il s'agit d'un widget avec du contenu spécifique
            if (in_array($values["type"],$widgets_need_content)) {
                $form->setSelect($champ, $values["contenu"]);
            }
            // si une taille a été définie
            if (isset($values["taille"])) {
                $form->setTaille($champ, $values["taille"]);
            }
            // si un évènement a été spécifié
            if (isset($values["onchange"])) {
                $form->setOnChange($champ, $values["onchange"]);
            }
            // si un max a été défini
            if (isset($values["max"])) {
                $form->setMax($champ, $values["max"]);
            }
            // valeur du champ
            $form->setVal($champ, $values["valeur"]);
        }
        // Bouton retour du haut
        $this->btn_retour();
        // Ouverture de la balise form
        $this->open_form_tag();
        // Entete
        $form->entete();
        // Champs
        $premier_champ = $liste_champs[0];
        $dernier_champ = $liste_champs[count($liste_champs) - 1];
        $form->setBloc($premier_champ, "D", "", "col_12");
        $form->setFieldset($premier_champ, "D", $title);
        $form->setFieldset($dernier_champ, "F");
        $form->setBloc($dernier_champ, "F");
        $form->afficher($liste_champs, 0, false, false);
        // Boutons
        $form->enpied();
        $this->form_controls();
        // Fermeture du formulaire
        printf("</form>\n");
    }

    /**
     * Formulaire d'ajout ou de modification d'une prescription
     *
     * @param  [string]   $mode     ajout ou modification
     * @param  [array]    $champs   champs du formulaire
     * @return void
     */
    function display_overlay_prescription($mode, $champs) {
        // ID CSS du dialog conteneur de l'overlay
        if ($mode == "add") {
            $id_overlay = "overlay_add_prescription";
            $js_action = sprintf("add_prescription();");
            $lib_action = __("Ajouter");
            $name_action = "btn_ajouter";
            $id_action = "add_the_prescription";
        }
        if ($mode == "edit") {
            $ordre_a_editer = $champs["prochain_ordre"]["valeur"];
            $id_overlay = "overlay_edit_prescription";
            $js_action = sprintf("edit_prescription(%s);",
                $ordre_a_editer
            );
            $lib_action = __("Modifier");
            $name_action = "btn_modifier";
            $id_action = "edit_the_prescription";
        }
        // action JS bouton annuler
        $js_annuler = sprintf("back_from_prescription();");
        // Tableau des widgets form nécessitant du contenu
        $widgets_need_content = array(
            "select",
            "select_multiple",
            "httpclick",
        );
        // Initialisation du compteur validation
        $validation = 0;
        // Création de la liste des,chhamps
        $liste_champs = array();
        foreach ($champs as $champ => $values) {
            $liste_champs[] = $champ;
        }
        // Création du formulaire
        $form = $this->f->get_inst__om_formulaire(array(
            "maj" => 1,
            "validation" => $validation,
            "champs" => $liste_champs,
        ));
        // Formatage des champs
        foreach ($champs as $champ => $values) {
            // libellé du champ
            $form->setLib($champ, $values["libelle"]);
            // widget formulaire du champ
            $form->setType($champ, $values["type"]);
            // s'il s'agit d'un widget avec du contenu spécifique
            if (in_array($values["type"],$widgets_need_content)) {
                $form->setSelect($champ, $values["contenu"]);
            }
            // si une taille a été définie
            if (isset($values["taille"])) {
                $form->setTaille($champ, $values["taille"]);
            }
            // si un évènement a été spécifié
            if (isset($values["onchange"])) {
                $form->setOnChange($champ, $values["onchange"]);
            }
            // si un max a été défini
            if (isset($values["max"])) {
                $form->setMax($champ, $values["max"]);
            }
            // valeur du champ
            $form->setVal($champ, $values["valeur"]);
        }
        // message d'erreur
        printf('<div class="erreur_prescription">');
        $this->f->displayMessage("error", __("Veuillez selectionner une prescription reglementaire."));
        printf('</div>');
        // Ouverture du formulaire
        printf('<form id="form_prescription_overlay" name="f3" action=""
            onsubmit="" method="post">');
        // Champs
        $form->setBloc("prescription_reglementaire", "D", __("reglementaire")." *");
        $form->setBloc("pr_description_om_html", "F");
        $form->setBloc("ps_description_om_html", "D", __("specifique"));
        $form->setBloc("specifique-type", "F");
        //
        $form->afficher($liste_champs, 0, false, false);
        // Boutons
        printf("\t<div class=\"formControls\">\n");
        // Ajouter ou Modifier
        printf('<input class="om-button ui-button ui-widget ui-state-default
            ui-corner-all" type="button" name="%s" id="%s" value="%s"
            role="button" aria-disabled="false" onclick="%s">',
            $name_action,
            $id_action,
            $lib_action,
            $js_action
        );
        // Annuler
        printf('<input class="om-button ui-button ui-widget ui-state-default
            ui-corner-all" type="button" name="btn_annuler" value="%s"
            role="button" aria-disabled="false" onclick="%s">',
            __("Annuler"),
            $js_annuler
        );
        // Fermeture de form controls
        printf("</div>\n");
        // Fermeture du formulaire
        $this->close_form_tag();
    }

    /**
     * Formulaire spécifique de l'édition du tableau de prescrptions.
     * 
     * @param  [array]    $prescriptions      tableau des prescriptions avec leur formatage
     * @param  [integer]  $id_analyse  ID de l'analyse modifiée
     * @param  [string]   $title       légende du fieldset
     * @param  [string]   $message     message d'erreur (facultatif)
     * @return void
     */
    function display_edit_prescription_form($prescriptions, $id_analyse, $title, $message = "") {
        // Initialisation du compteur validation
        $validation = 0;
        // S'il y a un message d'erreur on l'affiche
        if ($message != "") {
            $validation = 1;
            $this->correct = false;
            $this->f->displayMessage("error", $message);
        }
        // Bouton retour du haut
        $this->btn_retour();
        // Ouverture de la balise form
        $this->open_form_tag();
        // Entete
        $form = $this->f->get_inst__om_formulaire(array(
            "maj" => 1,
            "validation" => $validation,
            "champs" => array(),
        ));
        $form->entete();
        // Affichage des prescriptions
        printf('<div class="col_12">
            <fieldset class="cadre ui-corner-all ui-widget-content">
            <legend class="ui-corner-all ui-widget-content ui-state-active">
            %s</legend>',
            $title
        );
        $this->widget_edit_prescription($prescriptions, $id_analyse);
        printf('</fieldset></div><div class="visualClear"></div>');
        // Affichage des boutons du formulaire
        $form->enpied();
        $this->form_controls();
        // Fermeture du formulaire
        printf("</form>\n");
    }

    /**
     * Affiche les boutons modifier et retour du bas
     * dans les vues d'édition de l'analyse.
     * 
     * @return void
     */
    function form_controls() {
        printf("\t<div class=\"formControls\">\n");
        $this->btn_modifier();
        $this->btn_retour();
        printf("</div>\n");
    }

    /**
     * Affiche le bouton retour dans les vues d'édition de l'analyse.
     * 
     * @param  [integer] $id  ID de l'analyse
     * @return void
     */
    function btn_retour() {
        $this->retoursousformulaire();
    }

    /**
     * Affiche le bouton valider dans les vues d'édition de l'analyse.
     * 
     * @param  [integer] $id  ID de l'analyse
     */
    function btn_modifier() {
        $btn_modifier = array(
            "value" => __("Modifier"),
            "name" => "btn_modifier"
        );
        $this->f->layout->display_form_button($btn_modifier);
    }

    /**
     * METHOD - create_analyse.
     * 
     * Crée automatiquement une analyse lors de la création d'un DI.
     *
     * @param  [integer] $di_id Identifiant du nouveau dossier d'instruction
     * @return [boolean]        Vrai si analyse créée avec succès
     */
    function create_analyse($di_id) {
        // On instancie le dossier d'instruction puis récupère ses valeurs
        $di = $this->f->get_inst__om_dbform(array(
            "obj" => "dossier_instruction",
            "idx" => $di_id,
        ));
        $di_service = $di->getVal("service");
        $code_service = $this->f->get_service_code($di_service);
        $di_description = $di->getVal("description");
        $dc_id = $di->getVal("dossier_coordination");

        // On instancie son dossier de coordination puis récupère ses valeurs
        $dc = $this->f->get_inst__om_dbform(array(
            "obj" => "dossier_coordination",
            "idx" => $dc_id,
        ));
        $etablissement_id = $dc->getVal("etablissement");
        $dc_parent_id = $dc->getVal("dossier_coordination_parent");

        // On instancie l'établissement s'il y en a un de lié
        $etablissement = false;
        if ($etablissement_id != '' && $etablissement_id != null) {
            $etablissement = $this->f->get_inst__om_dbform(array(
                "obj" => "etablissement",
                "idx" => $etablissement_id,
            ));
        }

        // On instancie le DC parent s'il y en a un de renseigné,
        // ainsi que son analyse s'il est qualifié
        $dc_parent = false;
        $analyse_parente = false;
        if ($dc_parent_id != '' && $dc_parent_id != null) {
            $dc_parent = $this->f->get_inst__om_dbform(array(
                "obj" => "dossier_coordination",
                "idx" => $dc_parent_id,
            ));
            $di_parent_id = $dc_parent->get_id_dossier_instruction($code_service);
            // S'il y a un di parent
            if ($di_parent_id != "" && $di_parent_id != null) {
                $di_parent = $this->f->get_inst__om_dbform(array(
                    "obj" => "dossier_instruction",
                    "idx" => $di_parent_id,
                ));
                $analyse_parente_id = $di_parent->get_id_analyse();
                // S'il y a une analyse parente
                if ($analyse_parente_id != "" && $analyse_parente_id != null) {
                    $analyse_parente = $this->f->get_inst__om_dbform(array(
                        "obj" => "analyses",
                        "idx" => $analyse_parente_id,
                    ));
                }
            }
        }

        // Récupération du type d'analyse lié au type du dossier de coordination
        $sql = "SELECT dossier_coordination_type.analyses_type_".$code_service."
            FROM ".DB_PREFIXE."dossier_coordination_type
            WHERE dossier_coordination_type.dossier_coordination_type = ".$dc->getVal("dossier_coordination_type");
        $id_type_analyse = $this->f->db->getOne($sql);
        // Si la récupération de l'ID du type d'analyse échoue
        if ($this->f->isDatabaseError($id_type_analyse, true)) {
            // Appel de la methode de recuperation des erreurs
            $this->erreur_db($id_type_analyse->getDebugInfo(), $id_type_analyse->getMessage(), '');
            $this->correct = false;
            return false;
        }
        // On instancie le type d'analyse
        $type_analyse = $this->f->get_inst__om_dbform(array(
            "obj" => "analyses_type",
            "idx" => $id_type_analyse,
        ));
        // On récupère son paramétrage des modèles d'édition
        $mod_rpt = $type_analyse->getVal("modele_edition_rapport");
        $mod_cr = $type_analyse->getVal("modele_edition_compte_rendu");
        $mod_pv = $type_analyse->getVal("modele_edition_proces_verbal");

        // On crée un tableau pour les valeurs de l'analyse
        $analyse_values = array();
        // On affecte tous les champs par défaut en prévision de l'ajout
        foreach ($this->champs as $key => $champ) {
            $analyse_values[$champ] = '';
        }
        // Dossier d'instruction
        $analyse_values['dossier_instruction'] = $di_id;
        // Type d'analyse
        $analyse_values['analyses_type'] = $id_type_analyse;
        // Etat
        $analyse_values['analyses_etat'] = "en_cours";
        // Service
        $analyse_values['service'] = $di_service;
        // Objet
        $analyse_values['objet'] = $di_description;
        // Modèles d'édition
        $analyse_values['modele_edition_rapport'] = $mod_rpt;
        $analyse_values['modele_edition_compte_rendu'] = $mod_cr;
        $analyse_values['modele_edition_proces_verbal'] = $mod_pv;
        // Marqueur de modification sans (re)génération
        $analyse_values['modifiee_sans_gen'] = "Oui";
        // Récupération de données si établissement lié
        if ($etablissement != false) {
            // Si analyse parente on récupère ses valeurs
            if ($analyse_parente != false) {
                // descriptif
                $analyse_values['descriptif_etablissement_om_html']
                    = $analyse_parente->getVal("descriptif_etablissement_om_html");
                // données techniques SI
                if ($code_service == 'si') {
                    $analyse_values = $this->set_donnees_techniques_si($analyse_values, $analyse_parente);
                }
                // type
                $analyse_values['etablissement_type']
                    = $analyse_parente->getVal("etablissement_type");
                // catégorie
                $analyse_values['etablissement_categorie']
                    = $analyse_parente->getVal("etablissement_categorie");
            } else { // sinon celles de l'établissement
                // descriptif
                switch ($code_service) {
                    case 'acc':
                    $analyse_values['descriptif_etablissement_om_html']
                        = $etablissement->getVal("acc_descriptif_om_html");
                    break;
                    case 'si':
                    $analyse_values['descriptif_etablissement_om_html']
                        = $etablissement->getVal("si_descriptif_om_html");
                    // + données techniques SI
                    $analyse_values = $this->set_donnees_techniques_si($analyse_values, $etablissement);
                    break;
                }
                // type
                $analyse_values['etablissement_type']
                    = $etablissement->getVal("etablissement_type");
                // catégorie
                $analyse_values['etablissement_categorie']
                    = $etablissement->getVal("etablissement_categorie");
            }
        }

        // On crée l'analyse et stoppe le traitement s'il y a une erreur
        if ($this->ajouter($analyse_values) === false) {
            return false;
        }

        // Fin du traitement avec succès
        return true;
    }
    
    /**
     * CONDITION - is_from_good_service.
     */
    function is_from_good_service() {
        // Si l'utilisateur a un service associé
        // et que ce service est différent du service de l'élément 
        // sur lequel on se trouve
        if (!is_null($_SESSION["service"]) 
            && $this->getVal("service") != $_SESSION["service"]) {
            // Alors on indique qu'il ne peut pas accéder à l'élément
            $this->addToLog(__METHOD__."(): return false;", EXTRA_VERBOSE_MODE);
            return false;
        } else {
            // Si l'utilisateur n'a pas de service associé
            // Alors on indique q'uil peut accéder à l'élément puisque
            // ce sont les permissions standards qui permettent d'indiquer
            // si il peut accéder ou non à l'élément
            $this->addToLog(__METHOD__."(): return true;", EXTRA_VERBOSE_MODE);
            return true;
        }
    }

    /**
     * Récupère les essais réalisés de l'analyse
     *
     * @param  string $id_analyse ID de l'analyse
     * @return array              Les essais réalisés avec leur complément
     */
    function get_essai_realise($id_analyse){
        $sql = "SELECT essai_realise.libelle as lib,
                lien_essai_realise_analyses.concluant as conc,
                lien_essai_realise_analyses.complement as comp
            FROM ".DB_PREFIXE."lien_essai_realise_analyses
            LEFT JOIN ".DB_PREFIXE."essai_realise
                ON essai_realise.essai_realise = lien_essai_realise_analyses.essai_realise
            WHERE lien_essai_realise_analyses.analyses = ".$id_analyse."
            ORDER BY lien_essai_realise_analyses.concluant DESC,
            essai_realise.libelle ASC
        ";
        $res = $this->f->db->query($sql);
        $this->f->addToLog(__METHOD__."(): db->query(\"".$sql."\");", VERBOSE_MODE);
        if ($this->f->isDatabaseError($res, true)) {
            // Appel de la methode de recuperation des erreurs
            $this->erreur_db($res->getDebugInfo(), $res->getMessage(), '');
            $this->correct = false;
            return false;
        }
        $essais_realises = array();
        $ok =  sprintf('<span title="%s"
            class="icon_essai_realise_check"></span></td>',
            __("concluant")
        );
        $nok =  sprintf('<span title="%s"
            class="icon_essai_realise_cancel"></span></td>',
            __("non concluant")
        );
        while ($row = &$res->fetchRow(DB_FETCHMODE_ASSOC)) {
            ($row["conc"] == 't') ? $conc = $ok : $conc = $nok;
            ($row["comp"] != '') ? $comp = "<br/>".$row["comp"]."<br/>" : $comp = "";
            $essais_realises[] = $conc.$row["lib"].$comp;
        }
        return $essais_realises;
    }

    /**
     * Permet d'afficher les données techniques de l'analyse
     *
     * @return [string] Liste HTML des données techniques
     */
    function get_donnees_techniques_si() {
        // Liste des champs selon le service
        $code_service = $this->f->get_service_code($this->getVal("service"));
        switch ($code_service) {
            case 'si':
            $champs = array(
                "si_effectif_public"=>array(__("si_effectif_public"), __(" personne(s)")), 
                "si_effectif_personnel"=>array(__("si_effectif_personnel"), __(" personne(s)")), 
                "si_type_ssi"=>array(__("si_type_ssi")), 
                "si_type_alarme"=>array(__("si_type_alarme")), 
                "si_conformite_l16"=>array(__("si_conformite_l16"), "boolean"), 
                "si_alimentation_remplacement"=>array(__("si_alimentation_remplacement"), "boolean"), 
                "si_service_securite"=>array(__("si_service_securite"), "boolean"), 
                "si_personnel_jour"=>array(__("si_personnel_jour"), __(" personne(s)")), 
                "si_personnel_nuit"=>array(__("si_personnel_nuit"), __(" personne(s)"))
            );
            break;
        }
        //
        $no_content = true;
        $donnees = sprintf('<ul class="liste_consultation_analyse" id="donnee_technique">');
        foreach($champs as $champ => $valeurs){
            // On n'ajoute une entrée que s'il y a une valeur à afficher
            if ($this->getVal($champ) != '' && $this->getVal($champ) != 'NC') {
                $no_content = false;
                $valeur = "";
                // Si le champ est un booléen
                if (isset($valeurs[1])&&$valeurs[1]=="boolean"){
                    $valeur = ($this->getVal($champ)!='f')?__("Oui"):__("Non");
                }
                // Si une valeur complémentaire doit être ajoutée
                elseif (isset($valeurs[1])&&$valeurs[1]!="boolean"){
                    $valeur = $this->getVal($champ).$valeurs[1];
                }
                // Autres champs
                else {
                    $valeur = $this->getVal($champ);
                }
                // Construction de l'entrée de liste
                $donnees .= sprintf('<li class="maj_first_letter">');
                $donnees .= sprintf('<span>%s</span> : %s',
                    $valeurs[0],
                    $valeur
                );
                $donnees .= sprintf('</li>');
            }
        }
        $donnees .= sprintf('</ul>');
        if ($no_content == true) {
            $donnees = "";
        }
        return $donnees;
    }

    /**
     * Permet d'afficher les données techniques de l'analyse
     *
     * @return [string] Liste HTML des données techniques
     */
    function get_donnees_techniques_acc() {
        //
        $inst_ua = $this->f->get_inst__om_dbform(array(
            "obj" => "etablissement_unite",
            "idx" => 0,
        ));
        $ua_list = $inst_ua->get_list_for_dossier_instruction($this->getVal("dossier_instruction"));
        
        //
        if (count($ua_list) == 0) {
            return $this->no_content();
        }

        //
        $template_list = '
        <ul class="liste_consultation_analyse" id="donnee_technique">
        %s
        </ul>
        ';
        //
        $template_elem = '
        <li><u>%s</u><br/>%s</li>
        ';
        //
        $list_ct = '';
        foreach ($ua_list as $key => $value) {
            //
            if ($key > 9) {
                //
                $list_ct .= sprintf(
                    $template_elem,
                    "...",
                    ""
                );
                break;
            }
            //
            $list_ct .= sprintf(
                $template_elem,
                $value["libelle"],
                $value["description"]
            );
        }
        //
        return sprintf($template_list, $list_ct);
    }

    /**
     * Permet d'afficher les données techniques de l'analyse
     *
     * @return [string] Liste HTML des données techniques
     */
    function get_donnees_techniques() {
        // Liste des champs selon le service
        $code_service = $this->f->get_service_code($this->getVal("service"));
        switch ($code_service) {
            case 'si':
            return $this->get_donnees_techniques_si();
            break;
            case 'acc':
            return $this->get_donnees_techniques_acc();
            break;
        }
        return "";
    }

    /**
     * Récupère les prescriptions de l'analyse
     *
     * @param  string $id_analyse ID de l'analyse
     * @return array              Les essais réalisés avec leur complément
     */
    function get_prescription($id_analyse){
        $sql = "SELECT prescription_reglementaire as pr_id,
                pr_description_om_html as pr_desc,
                pr_defavorable as pr_def,
                ps_description_om_html as ps_desc
            FROM ".DB_PREFIXE."prescription
            WHERE analyses = ".$id_analyse."
            ORDER BY ordre ASC
        ";
        $res = $this->f->db->query($sql);
        $this->f->addToLog(__METHOD__."(): db->query(\"".$sql."\");", VERBOSE_MODE);
        if ($this->f->isDatabaseError($res, true)) {
            // Appel de la methode de recuperation des erreurs
            $this->erreur_db($res->getDebugInfo(), $res->getMessage(), '');
            $this->correct = false;
            return false;
        }
        $prescriptions = array();
        while ($row = &$res->fetchRow(DB_FETCHMODE_ASSOC)) {
            $prescriptions[] = $row;
        }
        return $prescriptions;
    }

    /**
     * FORM WIDGET widget_n_n
     * 
     * Affiche une liste HTML d'une table de liaison n/n
     * 
     * @param  [array]  $enregistrements  liste d'enregistrements
     * @return [string]                   code HTML de la liste
     */
    function widget_n_n($enregistrements) {
        if (empty($enregistrements)) {
            return $this->no_content();
        } else {
            $html = '<ul class="liste_consultation_analyse">';
            foreach ($enregistrements as $enregistrement) {
                
                $html .= '<li>'.$enregistrement.'</li>';
            }
            $html .= '</ul>';
            return $html;
        }
    }

    /**
     * FORM WIDGET widget_view_prescription
     * 
     * Affiche une liste HTML des prescriptions de l'analyse
     * 
     * @param  [array]  $prescriptions  liste de prescriptions
     * @return [string]                 code HTML de la liste
     */
    function widget_view_prescription($prescriptions) {
        $total = count($prescriptions);
        if ($total == 0) {
            return $this->no_content();
        } else {
            // Début bloc prescription
            $html = '<div id="analyse_bloc_prescriptions">';
            $i = 1;
            foreach ($prescriptions as $prescription) {
                // Ordre
                $html .= sprintf('<p>&bull; #%s &bull;</p>',
                    $i
                );
                // Début prescription
                $html .= '<div>';
                // Prescription réglementaire s'il y en a une
                $html .= sprintf('<p>&bull; %s &bull;<p/>',
                    __("Reglementaire")
                );
                // Début de la PR
                $html .= '<div class="view_prescription">';
                ($prescription["pr_def"] == 't') ?
                    $def = __("Oui") : $def = __("Non");
                $def = sprintf('%s : <span style="font-weight: bold;">%s</span>',
                    __("Defavorable"),
                    $def
                );
                $html .= sprintf('<p>%s</p>
                    <p>%s</p>',
                    $def,
                    $prescription["pr_desc"]
                );
                // Fin de la PR
                $html .= '</div>';
                // Prescription spécifique s'il y en a une
                if ($prescription["ps_desc"] != '') {
                    $html .= sprintf('<p>&bull; %s &bull;<p/>',
                        __("Specifiques")
                    );
                    // Début de la PS
                    $html .= '<div class="view_prescription" id="ps_view_'.$i.'">';
                    $html .= $prescription["ps_desc"];
                    // Fin de la PS
                    $html .= '</div>';
                }
                // Fin de la prescription
                $html .= '</div>';
                $i++;
            }
            // Fin bloc prescription
            $html .= '</div>';
            return $html;
        }
    }

    /**
     * FORM WIDGET widget_edit_prescription
     * 
     * Affiche un tableau pour choisir une ou plusieurs prescriptions
     *
     * @param string   $prescriptions Tableau de prescriptions
     * @param integer  $id_analyse    ID de l'analyse 
     */
    function widget_edit_prescription($prescriptions, $id_analyse) {
        // Insertion cachée de l'ID de l'analyse
        printf('<input id="id_analyse" type="hidden" value="%s" name="id_analyse"/>',
            $id_analyse
        );
        // Création du bouton d'ajout d'une prescription
        $btn_add = sprintf('<span id="add_a_prescription" title="%s"
            onclick="overlay_add_prescription(%s);"
            class="om-form-button add-16"></span>',
            __("Ajouter une prescription"),
            $id_analyse
        );
        // Ouverture du tableau
        printf('<table class="tab-tab">');
        // Entête de tableau
        $template_tab_header = '
            <thead><tr class="ui-tabs-nav ui-accordion ui-state-default tab-title">
                <th class="title col-0 actions-max-2">%s</th>
                <th class="title col-1 center_cell_prescription actions-max-3"></th>
                <th class="title col-2 marge_essai_droite">
                    <span class="name">%s</span>
                </th>
                <th class="title col-3 lastcol">
                    <span class="name">%s</span>
                </th>
            </tr></thead>';
        printf(
            $template_tab_header,
            $btn_add,
            __('Reglementaire'),
            __('Specifiques')
        );
        // Corps du tableau
        printf('<tbody id="%s">',
            "tableau_prescriptions"
        );
        $total = count($prescriptions);
        $i = 0;
        // Affiche chacune des prescriptions
        foreach ($prescriptions as $prescription => $valeurs) {
            $i++;
            $cache_haut = '';
            if ($i == 1) {
                $cache_haut = 'cache_fleche_haut_prescription';
            }
            $cache_bas = '';
            if ($i == $total) {
                $cache_bas = 'cache_fleche_bas_prescription';
            }
            ($i % 2 == 0) ? $odd = "even" : $odd = "odd";
            $ordre = intval($valeurs["ord"]);
            // action JS lors de la demande de suppression de la prescription
            $js_delete = sprintf("delete_prescription(this.id);");
            // action JS lors de la demande de modification de la prescription
            $js_edit = sprintf("overlay_edit_prescription(this.id, %s);",
                $id_analyse
            );
            // action JS lors de la demande de déplacement vers le bas
            $js_bas = sprintf("move_down_prescription(this.id);");
            // action JS lors de la demande de déplacement vers le haut
            $js_haut = sprintf("move_up_prescription(this.id);");
            // ouverture de la ligne
            printf('<tr id="prescription_%s" class="tab-data %s une_prescription">',
                $ordre,
                $odd
            );
            // ordre de la description
             printf('<td class="col-0 center_cell_prescription actions-max-2">
                <span class="cercle_ordre" id="numero_%s">%s</span></td>',
                $ordre,
                $ordre
            );
            // boutons d'actions
            printf('<td class="col-1 btns_prescription marge_essai_droite actions-max-3">
                <span id="delete_prescription_%s" title="%s" onclick="%s"
                class="om-form-button delete-16"></span>
                <span id="edit_prescription_%s" title="%s" onclick="%s"
                class="om-form-button edit-16"></span>
                <span id="move_down_prescription_%s" title="%s" onclick="%s"
                class="arrow-down-16 %s"></span>
                <span id="move_up_prescription_%s" title="%s" onclick="%s"
                class="arrow-up-16 %s"></span>
                </td>',
                $ordre,
                __("Supprimer cette prescription"),
                $js_delete,
                $ordre,
                __("Modifier cette prescription"),
                $js_edit,
                $ordre,
                __("Deplacer cette prescription vers le bas"),
                $js_bas,
                $cache_bas,
                $ordre,
                __("Deplacer cette prescription vers le haut"),
                $js_haut,
                $cache_haut
            );
            // Description de la prescription réglementaire
            if ($valeurs["pr_id"] == '') {
                $center = "center_cell_prescription";
                $pr = sprintf('<i>%s</i>',
                    __("Aucune")
                );
                $this->input_prescription($ordre, "pr_id", "");
                $this->input_prescription($ordre, "pr_def", "");
                $this->input_prescription($ordre, "pr_desc", "");
            } else {
                $center = "";
                ($valeurs["pr_def"] == 't') ? $def = __("Oui") : $def = __("Non");
                $def = sprintf('%s : <span style="font-weight: bold;">%s</span>',
                    __("Defavorable"),
                    $def
                );
                $pr = sprintf('<p>%s</p>
                    <p>%s</p>',
                    $def,
                    $valeurs["pr_desc"]
                );
                $this->input_prescription($ordre, "pr_id", $valeurs["pr_id"]);
                $this->input_prescription($ordre, "pr_def", $valeurs["pr_def"]);
                $this->input_prescription($ordre, "pr_desc", $valeurs["pr_desc"]);
            }
            printf('<td id="td_pr_%s" class="col-2 %s marge_essai_droite">%s</td>',
                $ordre,
                $center,
                $pr
            );
            // Description de la prescription spécifique
            $this->input_prescription($ordre, "ps_desc", $valeurs["ps_desc"]);
            printf('<td id="td_ps_%s" class="col-3 lastcol">%s</td>',
                $ordre,
                $valeurs['ps_desc']
            );
            // fermeture de la ligne
            printf('</tr>');
        }
        // fermeture du tableau
        printf('</tbody></table>');
    }

    /**
     * Affiche les documents présentés pendant et après la visite
     * 
     * @param  [string]  $pendant texte riche
     * @param  [string]  $apres   texte riche
     * @return [string]           code HTML de la liste
     */
    function get_document_presente($pendant, $apres) {
        $html = '<ul class="liste_consultation_analyse">';
        $html .= '<li>'.__("Pendant").'</li>';
        if ($pendant == '') {
            $html .= $this->no_content();
        } else {
            $html .= '<div class="documents_presentes">'.$pendant.'</div>';
        }
        $html .= '<li>'.__("Apres").'</li>';
        if ($apres == '') {
            $html .= $this->no_content();
        } else {
            $html .= '<div class="documents_presentes">'.$apres.'</div>';
        }
        $html .= '</ul>';
        return $html;
    }

    /**
     * Affiche la description de l'avis de référence choisi
     * asin qu'un éventuel complément.
     * 
     * @param  [integer]  $id_avis     id de l'avis d'analyse (table de paramétrage)
     * @param  [string]   $complement  complément d'avis (texte libre)
     * @return [string]                code HTML de l'avis et son complément
     */
    function get_avis_propose($id_avis, $complement) {
        // Gestion d'aucun avis
        $aucun_avis = true;
        if ($id_avis != '' AND $id_avis != 0) {
            $aucun_avis = false;
        }        
        $aucun_complement = true;
        if ($complement != '') {
            $aucun_complement = false;
        }
        if ($aucun_avis == true && $aucun_complement == true) {
            $html = $this->no_content();
        } else {
            // ouverture liste
            $html = '<ul class="liste_consultation_analyse">';
            // avis d'analyse
            if ($aucun_avis == false) {
                $id_avis = intval($id_avis);
                $sql_select = "SELECT libelle, description
                    FROM ".DB_PREFIXE."reunion_avis
                    WHERE reunion_avis = ".$id_avis;
                $res = $this->f->db->query($sql_select);
                $this->f->addToLog(__METHOD__."(): db->query(\"".$sql_select."\");", VERBOSE_MODE);
                // Si la récupération de la description de l'avis échoue
                if ($this->f->isDatabaseError($res, true)) {
                    // Appel de la methode de recuperation des erreurs
                    $this->erreur_db($res->getDebugInfo(), $res->getMessage(), '');
                    $this->correct = false;
                    return false;
                }
                $row = &$res->fetchRow();
                // avis
                $html .= sprintf('<li>'.__("Avis").' : %s',
                    $row[0]
                );
                // description
                if ($row[1] != '') {
                    $html .= sprintf('<br/>%s',
                        $row[1]
                    );
                }
                $html .= '</li>';
            }
            // complément
            if ($aucun_complement == false) {
                $html .= sprintf('<li>%s<br/>%s</li>',
                    __("Complement"),
                    $complement
                );
            }
            // fermeture liste
            $html .= '</ul>';
        }
        return $html;
    }

    function get_proposition_decision_ap() {
        // Ouverture liste
        $html = '<ul class="liste_consultation_analyse">';
        // Proposition 1
        $html .= sprintf('<li>%s</li><p>%s : %s<br/>%s : %s</p>',
            __("Proposition 1"),
            __("Decision"), $this->getVal("dec1"),
            __("Delai"), $this->getVal("delai1")
        );
        // Proposition 2
        $html .= sprintf('<li>%s</li><p>%s : %s<br/>%s : %s</p>',
            __("Proposition 2"),
            __("Decision"), $this->getVal("dec2"),
            __("Delai"), $this->getVal("delai2")
        );
        // Fermeture liste
        $html .= '</ul>';
        //
        return $html;
    }
    
    /**
     * FORM WIDGET widget_analyses
     * 
     * Utilisé pour afficher les informations dans l'analyse
     * 
     * @param string  $action      Numéro de l'action
     * @param string  $title       Le titre du bloc à afficher
     * @param string  $valeur      Les données du bloc à afficher
     * @param string  $right_view  Permission de consultation
     */
    function widget_analyses($action, $title, $valeur, $right_view) {
        // Si droit de consulter
        if ($this->f->isAccredited($right_view)) {
            // Bloc titre
            $this->bloc_titre_widget_analyses($title, $action);
            // Bloc contenu
            $this->bloc_contenu_widget_analyses($valeur);
        }
    }
    
    /**
     * Affichage du bloc titre du widget analyses
     *
     * @param type $title       Titre du bloc
     * @param type $action      Numéro de l'action
     *
     * @return void
     */
    function bloc_titre_widget_analyses($title, $action) {
        $obj = "analyses";
        $btn_edit = "";
        // Si utilisateur possède le droit sur cette permission
        if (!empty($action) && $this->is_action_available($action)) {
            // XXX Vérifier que l'url suivante est composée correctement
            //     il n'y a pas de retourformulaire et d'idxformulaire
            //     peut-être qu'il est possible d'utiliser compose_form_url ?
            $url = OM_ROUTE_SOUSFORM."&obj=".$obj;
            $url .= "&idx=".$this->getParameter('idx');
            $url .= "&action=".$action;
            $url .= "&retour=form";
            // On affiche le bouton modifier
            $btn_edit = sprintf('<a id="action_%s"
                onclick="ajaxIt(\'%s\', \'%s\');" href="#">
                <span class="om-prev-icon om-icon-16 edit-16" title="%s"></span>
                </a>',
                $this->get_action_param($action, 'identifier'),
                $obj,
                $url,
                ucfirst($title)
            );
        }
        printf('
            <div class="bloc_titre_widget_analyses">
                %s
                <span class="titre_widget_analyses">%s</span>
            </div>',
            $btn_edit,
            ucfirst($title)
        );
    }

    /**
     * Affichage du contenu du widget analyses
     *
     * @param string  $valeur    Valeur à afficher
     */
    function bloc_contenu_widget_analyses($valeur){
        //Bloc contenu
        printf ("<div class=\"contenu_widget_analyses\">%s</div><br/>",($valeur!='')?$valeur:$this->no_content());
    }
    
    /**
     * Récupère le libellé de l'objet passé en paramètre.
     *
     * @param string $table                 La table de la clause FROM
     * @param string $cle_primaire          La clé primaire pour les jointures
     * @param string $champ_where           La champ de la clause WHERE
     * @param array $tables_lien            Les tables des jointures
     * @param array $cles_primaire_liens    Les clés primaires des jointures
     * @param mixed $idx                    L'identifiant de l'objet
     * @param string $order                 Ordonnancement des résultats
     * @param boolean $square               Affichage d'un carré
     * @return string                       Le libellé
     */
    function get_libelle($table, $cle_primaire, $champ_where, $tables_lien, $cles_primaire_liens, 
            $idx, $order = null, $square=false){
        
        $libelle = "";
        $square = ($square)?"&nbsp;&nbsp;&#9632; ":"";
        //On vérifie que les variables ont été fournies
        if ($table!=''&&$cle_primaire!=''&&$idx!=''&&$champ_where!=''){
            //Requête SQL
            $sql = "SELECT ".$table.".libelle
                FROM ".DB_PREFIXE.$table;
            //S'il y a des jointures
            if (!is_null($tables_lien)&&!is_null($cles_primaire_liens)
                && count($tables_lien)==count($cles_primaire_liens)){
                //On récupère les informations précédentes
                $table_lien_prec = $table;
                $cle_primaire_prec = $cle_primaire;
                
                //Pour chaque jointure
                for ($i=0;$i<count($tables_lien);$i++){
                    //
                    $sql .= " LEFT JOIN ".DB_PREFIXE.$tables_lien[$i]."
                        ON ".$table_lien_prec.".".$cle_primaire_prec."=".$tables_lien[$i].".".$cles_primaire_liens[$i];
                    //
                    $table_lien_prec = $tables_lien[$i];
                    $cle_primaire_prec = $cles_primaire_liens[$i];
                }
            }
            //Ajout de la condition
            $sql .= " WHERE ".$champ_where."=".intval($idx)." ".$order;
            
            //Exécution de la requête
            $res = $this->f->db->query($sql);
            //log
            $this->f->addToLog("get_libelle(): db->query(\"".$sql."\");", VERBOSE_MODE);
            if ($this->f->isDatabaseError($res,true)){
                $this->erreur_db($res->getDebugInfo(), $res->getMessage(), '');
                die();
            }
            //Formatage des libellés
            while($libelles =& $res->fetchRow()){
                $libelle.=$square.$libelles[0]."<br/>";
            }
        }
        
        return $libelle;
    }


    /**
     * TREATMENT - finaliser.
     * 
     * Permet de finaliser une analyse.
     *
     * @param array $val Valeurs soumises par le formulaire.
     *
     * @return boolean
     */
    function finaliser($val = array()) {
        // Begin
        $this->begin_treatment(__METHOD__);

        //
        $ret = $this->manage_analyse_state("finalise");
        // Si le traitement ne s'est pas déroulé correctement
        if ($ret !== true) {
            // Return
            return $this->end_treatment(__METHOD__, false);
        }

        // Return
        return $this->end_treatment(__METHOD__, true);
    }

    /**
     * TREATMENT - reouvrir.
     * 
     * Permet de réouvrir une analyse.
     *
     * @param array $val Valeurs soumises par le formulaire.
     *
     * @return boolean
     */
    function reouvrir($val = array()) {
        // Begin
        $this->begin_treatment(__METHOD__);

        //
        $ret = $this->manage_analyse_state("unfinalise");
        // Si le traitement ne s'est pas déroulé correctement
        if ($ret !== true) {
            // Return
            return $this->end_treatment(__METHOD__, false);
        }

        // Return
        return $this->end_treatment(__METHOD__, true);
    }

    /**
     * TREATMENT - valider.
     * 
     * Permet de valider une analyse
     *
     * @param array $val Valeurs soumises par le formulaire.
     *
     * @return boolean
     */
    function valider($val = array()) {
        // Begin
        $this->begin_treatment(__METHOD__);

        // On récupère le code du service
        $code_service = $this->f->get_service_code($this->getVal("service"));
        // Si accessibilité
        if ($code_service == "acc") {
            // Mise à jour fiche établissement
            $ret = $this->update_etablissement($code_service);
            // Si le traitement ne s'est pas déroulé correctement
            if ($ret !== true) {
                // Return
                return $this->end_treatment(__METHOD__, false);
            }
        }

        // Mise à jour de l'état de l'analyse
        $ret = $this->manage_analyse_state("validate");
        // Si le traitement ne s'est pas déroulé correctement
        if ($ret !== true) {
            // Return
            return $this->end_treatment(__METHOD__, false);
        }

        // Return
        return $this->end_treatment(__METHOD__, true);
    }

    /**
     * METHOD - acter.
     * 
     * Permet d'acter une analyse.
     * 
     * @param  [integer]  $id  identifiant du PV (re)généré
     * @return boolean         true si valide false sinon
     */
    function acter($id) {
         // Définition des valeurs
        $this->setValFFromVal();
        // Si analyse validée
        if ($this->is_validated()) {
            // Logger
            $this->addToLog(__METHOD__."() - begin", EXTRA_VERBOSE_MODE);
            // Changement de l'état
            $this->valF["analyses_etat"] = "acte";
            // Mise à jour du dernier PV généré
            $this->valF["dernier_pv"] = $id;
            // Mise à jour du marqueur "PV (re)généré && analyse inchangée"
            $this->valF["modifiee_sans_gen"] = "f";
            // On libère le champ simulé types secondaires
            unset($this->valF["etablissement_type_secondaire"]);
            // Execution de la requête de modification des donnees de l'attribut
            // valF de l'objet dans l'attribut table de l'objet
            $this->correct = true;
            $this->setParameter("maj", 3);
            //
            return $this->modifier($this->valF);
        }
        return true;
    }


    /**
     * Gestion de la finalisation, validation et réouverture d'une analyse
     *
     * @param string $mode action
     *
     * @return boolean true si valide false sinon
     */
    function manage_analyse_state($mode = null) {
        // Logger
        $this->addToLog(__METHOD__."() - begin", EXTRA_VERBOSE_MODE);
        // Si le mode n'existe pas on retourne false
        if ($mode != "finalise" && $mode != "unfinalise" && $mode != "validate" ) {
            return false;
        }
        $this->setValFFromVal();
        switch ($mode) {
            case "finalise" :
                $this->valF["analyses_etat"] = "termine";
                $valid_message = __("Redaction terminee.");
                break;
            case "unfinalise" :
                $this->valF["analyses_etat"] = "en_cours";
                $this->valF["modifiee_sans_gen"] = "t";
                $valid_message = __("Reouverture de l'analyse.");
                break;
            case "validate" :
                $this->valF["analyses_etat"] = "valide";
                $valid_message = __("Validation de l'analyse.");
                break;
        }
        // On libère le champ simulé types secondaires
        unset($this->valF["etablissement_type_secondaire"]);
        // Execution de la requête de modification des donnees de l'attribut
        // valF de l'objet dans l'attribut table de l'objet
        $res = $this->modifier($this->valF);
        // Si une erreur survient
        if($res == false) {
            $this->correct = false;
            //
            $this->addToMessage(
                __("Erreur lors du changement d'etat. Contactez votre administrateur.")
            );
            return false;
        }
        $this->msg="";
        $this->addToMessage($valid_message);
        // Logger
        $this->addToLog(__METHOD__."() - end", EXTRA_VERBOSE_MODE);
        //
        return $res;
    }

    /**
     * CONDITION - is_in_progress
     * Analyse en cours ?
     *
     * @return boolean true si possible false sinon
     */
    function is_in_progress() {
        if ($this->getVal("analyses_etat") == "en_cours") {
            return true;
        }
        return false;
    }

    /**
     * CONDITION - is_finished
     * Analyse terminée ?
     *
     * @return boolean true si possible false sinon
     */
    function is_finished() {
        if ($this->getVal("analyses_etat") == "termine") {
            return true;
        }
        return false;
    }

    /**
     * CONDITION - is_validated
     * Analyse validée ?
     *
     * @return boolean true si possible false sinon
     */
    function is_validated() {
        if ($this->getVal("analyses_etat") == "valide") {
            return true;
        }
        return false;
    }

    /**
     * CONDITION - is_acted
     * Analyse actée ?
     *
     * @return boolean true si possible false sinon
     */
    function is_acted() {
        if ($this->getVal("analyses_etat") == "acte") {
            return true;
        }
        return false;
    }

    /**
     * CONDITION - is_last_pv_defined
     * Présence d'au moins un PV généré ?
     *
     * @return  [boolean]  vrai si condition réalisée
     */
    function is_last_pv_defined() {
        if ($this->getVal("dernier_pv") == "") {
            return false;
        }
        return true;
    }

    function display_success($id, $success = true) {
        if ($success == true) {
            $style = "valid";
            $message = __("Vos modifications ont bien ete enregistrees.");
        } else {
            $style = "error";
            $message = __("Erreur de base de donnees. Contactez votre administrateur.");
        }
        $this->btn_retour();
        $this->f->displayMessage($style, $message);
        $this->btn_retour();
    }

    /**
     * Affiche le titre de l'overlay
     * 
     * @param  [string] $title Titre de l'overlay
     * @return void
     */
    function display_overlay_title($title) {
        //
        $template_title = '
        <div class="overlay_title ui-state-active ui-corner-all">
            <h2>%s</h2>
        </div>
        ';
        //
        printf(
            $template_title,
            $title
        );
    }

    /**
     * Affiche l'état de l'analyse en titre ainsi que sa valeur codée cachée.
     * 
     * @param  [string] $title Titre de l'analyse (libellé de l'état)
     * 
     * @return void
     */
    function display_analyse_state($title) {
        printf('<div class="overlay_title ui-state-active ui-corner-all">
            <h2 id="analyse_etat">%s</h2>
            </div>',
            $title
        );
    }

    /**
     * Affiche un input caché d'une prescription afin de pouvoir la poster.
     * 
     * @param  [string] $ordre  Ordre de la prescription
     * @param  [string] $champ  Identifiant du champ
     * @param  [string] $valeur Valeur du champ
     * @return void
     */
    function input_prescription($ordre, $champ, $valeur) {
        $valeur = htmlspecialchars($valeur);
        printf('<input id="%s" type="hidden" value="%s" name="%s"/>',
            $champ."_".$ordre,
            $valeur,
            $champ."[".$ordre."]"
            
        );
    }

    /**
     * Retourne sans l'afficher une icône et un texte
     * mentionnant qu'il n'y a aucun contenu.
     * 
     * @return [string] Code HTML
     */
    function no_content() {
        return sprintf('<span title="%s"
            class="icon_no_content"></span><i>%s</i>',
            __("Aucun contenu"),
            __("Aucun contenu")
        );
    }

    /**
     * Ouverture de la balise form du formulaire d'édition
     * 
     * @return void
     */
    function open_form_tag() {
        $datasubmit = $this->getDataSubmitSousForm();
        printf('<!-- ########## START DBFORM ########## -->
            <form id="form_analyses_modifier" method="post" name="f2" action=""
            onsubmit="affichersform(\'%s\', \'%s\', this);return false;">',
            $this->getParameter("objsf"),
            $datasubmit
        );
    }
    
    /**
     * Mise à jour de la fiche établissement en fonction du service concerné
     * @param type $code_service Le code du service
     * @return boolean
     */
    function update_etablissement($code_service){
        // Instanciation du dossier d'instruction
        $di = $this->f->get_inst__om_dbform(array(
            "obj" => "dossier_instruction",
            "idx" => $this->getVal("dossier_instruction"),
        ));
        // Instanciation du dossier de coordination
        $dc = $this->f->get_inst__om_dbform(array(
            "obj" => "dossier_coordination",
            "idx" => $di->getVal("dossier_coordination"),
        ));
        // Récupération de l'ID de l'établissement
        $id_etablissement = $dc->getVal("etablissement");
        // Initialisation de l'établissement s'il existe
        if ($id_etablissement != "") {
            $etablissement = $this->f->get_inst__om_dbform(array(
                "obj" => "etablissement_tous",
                "idx" => $id_etablissement,
            ));
            // Récupération des valeurs existantes
            $values = array();
            foreach($etablissement->champs as $key => $champ) {
                $values[$champ] = $etablissement->val[$key];
                // Si champ de type date
                if ($etablissement->type[$key] == 'date') {
                    // Conversion format EN en FR
                    $values[$champ] = $etablissement->dateDBToForm($etablissement->val[$key]);
                }
            }            
            // Mise à jour du descriptif établissement du service de l'analyse
            switch ($code_service) {
                case 'si':
                    $values["si_descriptif_om_html"]
                        = $this->getVal("descriptif_etablissement_om_html");
                    // + données techniques SI dans la fiche établissement
                    $this->set_donnees_techniques_si($values, $this);
                    break;
                case 'acc':
                    $values["acc_descriptif_om_html"]
                        = $this->getVal("descriptif_etablissement_om_html");
                    break;
            }
            $etablissement->setParameter("maj", 1);
            // On ne souhaite apporter aucune modification à l'exploitant
            $values["flag_form_exploitant"] = false;
            if ($etablissement->modifier($values) === false) {
                $this->correct = false;
                //
                $this->addToMessage(__("Erreur lors de la mise a jour de la fiche etablissement :"));
                $this->addToMessage(str_replace(__("SAISIE NON ENREGISTREE"), "",$etablissement->msg));
                // Suppression des interlignes superflues
                $this->msg = str_replace("<br />", "<br/>",$this->msg);
                $this->msg = preg_replace('#<br/>(\s*<br/>)+#', '', $this->msg);
                return false;
            }
        }
        // continue le traitement de la méthode d'appel
        return true;
    }

    /**
     * Fermeture de la balise form
     * 
     * @return void
     */
    function close_form_tag() {
        printf("</form>\n");
    }

    /**
     * VIEW - view_edition
     * 
     * Edite directement un rapport, un compte-rendu ou un PV.
     *
     * @return null Si l'action est incorrecte
     */
    function view_edition() {
        //
        $this->checkAccessibility();
        // Initialisation de l'objet à éditer
        $obj = "";
        // Tableau des éditions possibles
        $editions = array (
            "rapport_analyse",
            "compte_rendu_analyse",
            "proces_verbal"
        );
        // On vérifie que l'une des éditions est demandée
        foreach ($editions as $edition) {
            // Si oui on la mémorise
            if ($this->get_action_key_for_identifier($edition)
                == $this->getParameter("maj")) {
                $obj = $edition;
            }
        }
        // Sinon on stoppe le traitement
        if ($obj == "") {
            return false;
        }
        // Récupération du modèle d'édition et paramétrage associé
        $extension = ".pdf";
        $params = array();
        $params["specific"] = array();
        switch ($obj) {
            case 'rapport_analyse':
                $id_modele_edition = $this->getVal("modele_edition_rapport");
                $idx = $this->getVal($this->clePrimaire);
                $filename = $obj."_".$idx.$extension;
                break;
            case 'compte_rendu_analyse':
                $id_modele_edition = $this->getVal("modele_edition_compte_rendu");
                $idx = $this->getVal($this->clePrimaire);
                $filename = $obj."_".$idx.$extension;
                break;
            case 'proces_verbal':
                $id_modele_edition = $this->getVal("modele_edition_proces_verbal");
                $idx = "";
                $filename = __("previsualisation")."_".$obj."_".__("analyse")."_".$this->getVal($this->clePrimaire).$extension;
                $params["watermark"] = true;
                $params["specific"] = array(
                    "mode" => "previsualisation",
                    "analyses" => $this->getVal($this->clePrimaire),
                );
                break;
        }
        // Génération du PDF
        $pdfedition = $this->compute_pdf_output("modele_edition", $id_modele_edition, null, $idx, $params);
        // Affichage du PDF
        $this->expose_pdf_output(
            $pdfedition["pdf_output"], 
            $filename
        );
    }

    /**
     * Récupère les données techniques d'un établissement ou d'une analyse parente.
     * 
     * @param [array]   $values       Valeurs déjà fournies
     * @param [object]  $obj          Objet dont il faut récupérer les valeurs
     * @param [array]   $code_service Valeurs plus données techniques
     *
     * @return [array]
     */
    function set_donnees_techniques_si($values, $obj) {
        // Définition des champs suivant le service
        $champs_si = array(
            "si_effectif_public",
            "si_effectif_personnel",
            "si_type_ssi",
            "si_type_alarme",
            "si_conformite_l16",
            "si_alimentation_remplacement",
            "si_service_securite",
            "si_personnel_jour",
            "si_personnel_nuit"
        );
        foreach ($champs_si as $champ) {
            $values[$champ] = $obj->getVal($champ);
        }
        return $values;
    }

    /**
     * Méthode de traitement des données retournées par le formulaire
     *
     * @param array $val
     */
    function setvalF($val = array()) {

        parent::setValF($val);

        switch ($val['si_conformite_l16']) {

            case 1:
            case "t":
            case "Oui":
            case "true":
            $this->valF['si_conformite_l16'] = true;
            break;

            case '':
            case "null":
            case "NULL":
            $this->valF['si_conformite_l16'] = null;
            break;

            default:
            $this->valF['si_conformite_l16'] = false;
            break;
        }
        switch ($val['si_alimentation_remplacement']) {

            case 1:
            case "t":
            case "Oui":
            case "true":
            $this->valF['si_alimentation_remplacement'] = true;
            break;

            case '':
            case "null":
            case "NULL":
            $this->valF['si_alimentation_remplacement'] = null;
            break;

            default:
            $this->valF['si_alimentation_remplacement'] = false;
            break;
        }
        switch ($val['si_service_securite']) {

            case 1:
            case "t":
            case "Oui":
            case "true":
            $this->valF['si_service_securite'] = true;
            break;

            case '':
            case "null":
            case "NULL":
            $this->valF['si_service_securite'] = null;
            break;

            default:
            $this->valF['si_service_securite'] = false;
            break;
        }
    }

    /**
     * Surcharge de la récupération des valeurs des champs de fusion
     * 
     * @return [array]  tableau associatif champ de fusion => valeur
     */
    function get_values_merge_fields() {
        //
        $ua = "";
        $inst_ua = $this->f->get_inst__om_dbform(array(
            "obj" => "etablissement_unite",
            "idx" => 0,
        ));
        $ua = $inst_ua->get_display_for_merge_field(array(
            "obj" => "dossier_instruction", 
            "idx" => $this->getVal("dossier_instruction"),
        ));

        // On récupère les éventuelles prescriptions
        $obj_prescription = $this->f->get_inst__om_dbform(array(
            "obj" => "prescription",
            "idx" => "]",
        ));
        $prescriptions = $obj_prescription->get_html_output__prescriptions_for_analyse($this->getVal($this->clePrimaire));
        $prescriptions_no_bdr = $obj_prescription->get_html_output__prescriptions_for_analyse($this->getVal($this->clePrimaire),  array("border" => false, ));
        $prescriptions_no_bdr_no_bgr = $obj_prescription->get_html_output__prescriptions_for_analyse($this->getVal($this->clePrimaire),  array("border" => false, "background" => false, ));
        $prescriptions_unifiees = $obj_prescription->get_html_output__prescriptions_for_analyse($this->getVal($this->clePrimaire), array("separator" => false, ));
        $prescriptions_unifiees_no_bdr = $obj_prescription->get_html_output__prescriptions_for_analyse($this->getVal($this->clePrimaire),  array("separator" => false, "border" => false, ));
        $prescriptions_unifiees_no_bdr_no_bgr = $obj_prescription->get_html_output__prescriptions_for_analyse($this->getVal($this->clePrimaire),  array("separator" => false, "border" => false, "background" => false, ));
        $prescriptions_defavorables = $obj_prescription->get_prescription_defavorable_from_analyse($this->getVal($this->clePrimaire));

        // On récupère les éventuels essais réalisés
        $lien_essais = $this->f->get_inst__om_dbform(array(
            "obj" => "lien_essai_realise_analyses",
            "idx" => "]",
        ));
        $essais = $lien_essais->get_essai_realise_from_analyse($this->getVal($this->clePrimaire));

        // données techniques vides par défaut
        $dt = array(
            // si
            "dt_eff_pub" => "",
            "dt_eff_pers" => "",
            "dt_ssi" => "",
            "dt_alarme" => "",
            "dt_l16" => "",
            "dt_alim" => "",
            "dt_svc_sec" => "",
            "dt_pers_jour" => "",
            "dt_pers_nuit" => "",
        );
        // données techniques SI
        $dt["dt_eff_pub"] = $this->getVal("si_effectif_public");
        $dt["dt_eff_pers"] = $this->getVal("si_effectif_personnel");
        $dt["dt_ssi"] = $this->getVal("si_type_ssi");
        $dt["dt_alarme"] = $this->getVal("si_type_alarme");
        $dt["dt_l16"] = $this->translate_select_boolean_value_in_string($this->getVal("si_conformite_l16"));
        $dt["dt_alim"] = $this->translate_select_boolean_value_in_string($this->getVal("si_alimentation_remplacement"));
        $dt["dt_svc_sec"] = $this->translate_select_boolean_value_in_string($this->getVal("si_service_securite"));
        $dt["dt_pers_jour"] = $this->getVal("si_personnel_jour");
        $dt["dt_pers_nuit"] = $this->getVal("si_personnel_nuit");

        // retour
        return array(
            // analyse
            "analyse.type" => $this->f->get_label_of_foreign_key("analyses","analyses_type",$this->getVal("analyses_type")),
            "analyse.objet" => $this->getVal("objet"),
            "analyse.descriptif_etablissement" => $this->getVal("descriptif_etablissement_om_html"),
            "analyse.compte_rendu" => $this->getVal("compte_rendu_om_html"),
            "analyse.observations" => $this->getVal("observation_om_html"),
            "analyse.reglementation_applicable" => $this->getVal("reglementation_applicable_om_html"),
            "analyse.documents_presentes_pendant" => $this->getVal("document_presente_pendant_om_html"),
            "analyse.documents_presentes_apres" => $this->getVal("document_presente_apres_om_html"),
            "analyse.essais_realises" => $essais,
            "analyse.prescriptions" => $prescriptions,
            "analyse.prescriptions_no_bdr" => $prescriptions_no_bdr,
            "analyse.prescriptions_no_bdr_no_bgr" => $prescriptions_no_bdr_no_bgr,
            "analyse.prescriptions_unifiees" => $prescriptions_unifiees,
            "analyse.prescriptions_unifiees_no_bdr" => $prescriptions_unifiees_no_bdr,
            "analyse.prescriptions_unifiees_no_bdr_no_bgr" => $prescriptions_unifiees_no_bdr_no_bgr,
            "analyse.prescriptions_defavorables" => $prescriptions_defavorables,
            "analyse.proposition_avis" => $this->f->get_label_of_foreign_key("analyses","reunion_avis",$this->getVal("reunion_avis")),
            "analyse.proposition_avis_complement" => $this->getVal("avis_complement"),
            "analyse.ua_analysees" => $ua,
            "analyse.dec1" => $this->getVal("dec1"),
            "analyse.delai1" => $this->getVal("delai1"),
            "analyse.dec2" => $this->getVal("dec2"),
            "analyse.delai2" => $this->getVal("delai2"),
            // données techniques si
            "analyse.dt_si_effectif_public" => $dt["dt_eff_pub"],
            "analyse.dt_si_effectif_personnel" => $dt["dt_eff_pers"],
            "analyse.dt_si_type_ssi" => $dt["dt_ssi"],
            "analyse.dt_si_type_alarme" => $dt["dt_alarme"],
            "analyse.dt_si_conformite_l16" => $dt["dt_l16"],
            "analyse.dt_si_alimentation_remplacement" => $dt["dt_alim"],
            "analyse.dt_si_service_securite" => $dt["dt_svc_sec"],
            "analyse.dt_si_personnel_jour" => $dt["dt_pers_jour"],
            "analyse.dt_si_personnel_nuit" => $dt["dt_pers_nuit"],
        );
    }

    /**
     * Surcharge de la récupération des libellés des champs de fusion
     * 
     * @return array         tableau associatif
     */
    function get_labels_merge_fields() {
        //
        $labels = array(
            "analyse" => array(
                "analyse.type" => __("analyses_type"),
                "analyse.objet" => __("objet"),
                "analyse.descriptif_etablissement" => __("descriptif_etablissement_om_html"),
                "analyse.ua_analysees" => __("liste des UA analysées"),
                "analyse.compte_rendu" => __("compte_rendu_om_html"),
                "analyse.observations" => __("observation_om_html"),
                "analyse.reglementation_applicable" => __("reglementation_applicable_om_html"),
                "analyse.documents_presentes_pendant" => __("document_presente_pendant_om_html"),
                "analyse.documents_presentes_apres" => __("document_presente_apres_om_html"),
                "analyse.essais_realises" => __("tableau des essais realises"),
                "analyse.prescriptions" => __("tableau des prescriptions (avec ligne séparatrice entre réglementaire et spécifique)"),
                "analyse.prescriptions_no_bdr" => __("tableau des prescriptions sans bordure"),
                "analyse.prescriptions_no_bdr_no_bgr" => __("tableau des prescriptions sans bordure sans fond"),
                "analyse.prescriptions_unifiees" => __("tableau des prescriptions unifiées (sans ligne séparatrice entre réglementaire et spécifique)"),
                "analyse.prescriptions_unifiees_no_bdr" => __("tableau des prescriptions unifiées sans bordure"),
                "analyse.prescriptions_unifiees_no_bdr_no_bgr" => __("tableau des prescriptions unifiées sans bordure sans fond"),
                "analyse.prescriptions_defavorables" => __("liste des prescriptions defavorables"),
                "analyse.dt_si_effectif_public" => __("si_effectif_public"),
                "analyse.dt_si_effectif_personnel" => __("si_effectif_personnel"),
                "analyse.dt_si_type_ssi" => __("si_type_ssi"),
                "analyse.dt_si_type_alarme" => __("si_type_alarme"),
                "analyse.dt_si_conformite_l16" => __("si_conformite_l16"),
                "analyse.dt_si_alimentation_remplacement" => __("si_alimentation_remplacement"),
                "analyse.dt_si_service_securite" => __("si_service_securite"),
                "analyse.dt_si_personnel_jour" => __("si_personnel_jour"),
                "analyse.dt_si_personnel_nuit" => __("si_personnel_nuit"),
                "analyse.proposition_avis" => __("analyses_avis"),
                "analyse.proposition_avis_complement" => __("avis_complement"),
                "analyse.dec1" => __("proposition autorite de police 1 - decision"),
                "analyse.delai1" => __("proposition autorite de police 1 - delai"),
                "analyse.dec2" => __("proposition autorite de police 2 - decision"),
                "analyse.delai2" => __("proposition autorite de police 2 - delai"),
            ),
        );
        //
        if ($this->f->is_option_unite_accessibilite_enabled() !== true) {
            unset($labels["analyse"]["analyse.ua_analysees"]);
        }
        //
        return $labels;
    }

    /**
     * Formate la valeur d'un booléen select en oui/non/NC
     * 
     * @param  [mixed]   $value  valeur brute du booléen
     * @return [string]          valeur formatée
     */
    function translate_select_boolean_value_in_string($value) {
        switch ($value) {

            case 1:
            case "t":
            case "Oui":
            case "true":
            $value = __("oui");
            break;

            case '':
            case "null":
            case "NULL":
            $value = __("NC");
            break;

            default:
            $value = __("non");
            break;
        }
        return $value;
    }

    /**
     * Formate la valeur d'un booléen checkbox en oui/non
     * 
     * @param  [mixed]   $value  valeur brute du booléen
     * @return [string]          valeur formatée
     */
    function translate_checkbox_value_in_string($value) {
        switch ($value) {

            case 1:
            case "t":
            case "Oui":
            case "true":
            $value = __("oui");
            break;

            case 0:
            case "f":
            case "Non":
            case "false":
            $value = __("non");
            break;

            default:
            $value = "";
            break;
        }
        return $value;
    }

    /**
     * Retourne toutes les informations de l'adresse d'un objet instancié.
     *
     * @param string  $obj  Enregistrement dont il faut récupérer l'adresse
     *
     * @return string       Code HTML de l'adresse formatée
     */
    function concat_adresse($obj) {
        $html = "";

        // Ajoute "BP" devant la boite postale si celle-ci est renseignée
        $bp = $obj->getVal("boite_postale");
        if ($bp != "") {
            $bp = __("BP")." ".$bp;
        }

        // Ajoute "Cedex" devant le cedex si celui-ci est renseigné
        $cedex = $obj->getVal("cedex");
        if ($cedex != "") {
            $cedex = __("Cedex")." ".$cedex;
        }

        // Découpage de l'adresse par ligne
        $adr = array();
        // Ligne 1
        $adr[] = array(
            $obj->getVal("adresse_numero"),
            $obj->getVal("adresse_numero2"),
            $this->f->get_label_of_foreign_key("etablissement","adresse_voie",$obj->getVal("adresse_voie")),
        );
        // Ligne 2
        $adr[] = array(
            $obj->getVal("adresse_complement"),
        );
        // Ligne 3
        $adr[] = array(
            $obj->getVal("lieu_dit"),
            $bp,
        );
        // Ligne 4
        // Vérifie si l'adresse est composée d'un arrondissement
        $adresse_arrondissement = "";
        if ($obj->getVal("adresse_arrondissement") != "") {
            $adresse_arrondissement = $this->f->get_label_of_foreign_key("etablissement","adresse_arrondissement",$obj->getVal("adresse_arrondissement"));
        }
        $adr[] = array(
            $obj->getVal("adresse_cp"),
            $obj->getVal("adresse_ville"),
            $adresse_arrondissement,
            $cedex,
        );

        // Compteur de ligne
        $i = 1;
        // Pour chaque ligne
        foreach ($adr as $ligne) {
            // Concat des données
            $value = $this->f->concat_text($ligne, " ");
            // Si la ligne n'est pas vide
            if (!empty($value)) {
                // Ajout de la ligne
                $html .=  sprintf('<br><span>%s</span>',$value);
                // Incrémente le compteur
                $i++;
            }
        }
        return $html;
    }

    /**
     * Retourne la liste des modèles d'édition paramétrés pour l'analyse.
     * 
     * @return [string] Code HTML de la liste
     */
    function get_modeles_edition() {
        return sprintf('<ul class="liste_consultation_analyse"><li>%s : %s</li><li>%s : %s</li><li>%s : %s</li></ul>',
            __("Rapport"),
            $this->get_libelle("modele_edition", "modele_edition", "modele_edition", null, null, $this->getVal("modele_edition_rapport")),
            __("Compte-rendu"),
            $this->get_libelle("modele_edition", "modele_edition", "modele_edition", null, null, $this->getVal("modele_edition_compte_rendu")),
            __("Proces-verbal"),
            $this->get_libelle("modele_edition", "modele_edition", "modele_edition", null, null, $this->getVal("modele_edition_proces_verbal"))
        );
    }

    /**
     * Retourne le contenu du select des prescriptions réglementaires filtrées
     * par le service de l'analyse et par les types et catégorie d'établissement
     * définis dans le dossier de coordination.
     * 
     * @param   [string]  pr_id  prescription réglementaire récupérée (facultative)
     * @return  [mixed]          tableau du select ou faux si erreur de bdd
     */
    function get_prescriptions_reglementaires($pr_id = "") {
        // Récupération du service de l'analyse
        $id_service = $this->getVal("service");
        // Instanciation du dossier d'instruction
        $di = $this->f->get_inst__om_dbform(array(
            "obj" => "dossier_instruction",
            "idx" => $this->getVal("dossier_instruction"),
        ));
        // Instanciation du dossier de coordination
        $dc = $this->f->get_inst__om_dbform(array(
            "obj" => "dossier_coordination",
            "idx" => $di->getVal("dossier_coordination"),
        ));
        // Récupération des types et catégories définies dans le DC
        $categorie = 0;
        if ($dc->getVal("etablissement_categorie") != '') {
            $categorie = intval($dc->getVal("etablissement_categorie"));
        }
        $types = array();
        if ($dc->getVal("etablissement_type") != '') {
            $types[] = intval($dc->getVal("etablissement_type"));
        }
        if ($dc->getVal("etablissement_type_secondaire") != '') {
            $types_secondaires = explode(';', $dc->getVal("etablissement_type_secondaire"));
            $types = array_merge($types, $types_secondaires);
        }
        if (empty($types)) {
            $types = 0;
        } else {
            $types = implode(', ', $types);
        }
        // Requête
        $sql_select = "
            SELECT 
                DISTINCT prescription_reglementaire.prescription_reglementaire, 
                CONCAT(
                prescription_reglementaire.tete_de_chapitre1,
                ' - ',
                prescription_reglementaire.tete_de_chapitre2,
                ' - ',
                prescription_reglementaire.libelle) as plop
            FROM ".DB_PREFIXE."prescription_reglementaire
            LEFT JOIN ".DB_PREFIXE."lien_prescription_reglementaire_etablissement_categorie as cat
                 ON cat.prescription_reglementaire = prescription_reglementaire.prescription_reglementaire
            LEFT JOIN ".DB_PREFIXE."lien_prescription_reglementaire_etablissement_type as type
                 ON type.prescription_reglementaire = prescription_reglementaire.prescription_reglementaire
            WHERE service = ".$id_service."
                AND cat.etablissement_categorie = ".$categorie."
                AND type.etablissement_type in (".$types.")
                AND (
                    (((prescription_reglementaire.om_validite_debut IS NULL
                    AND (prescription_reglementaire.om_validite_fin IS NULL
                    OR prescription_reglementaire.om_validite_fin > CURRENT_DATE))
                    OR (prescription_reglementaire.om_validite_debut <= CURRENT_DATE
                    AND (prescription_reglementaire.om_validite_fin IS NULL
                    OR prescription_reglementaire.om_validite_fin > CURRENT_DATE))))";
        if ($pr_id != "") {
            $sql_select .= "OR prescription_reglementaire.prescription_reglementaire =".$pr_id;
        }
        $sql_select .=") 
            ORDER BY 
                CONCAT(
                prescription_reglementaire.tete_de_chapitre1,
                ' - ',
                prescription_reglementaire.tete_de_chapitre2,
                ' - ',
                prescription_reglementaire.libelle) ASC";
        $res = $this->f->db->query($sql_select);
        $this->f->addToLog(__METHOD__."(): db->query(\"".$sql_select."\");", VERBOSE_MODE);
        // Si la construction du select échoue
        if ($this->f->isDatabaseError($res, true)) {
            // Appel de la methode de recuperation des erreurs
            $this->erreur_db($res->getDebugInfo(), $res->getMessage(), '');
            $this->correct = false;
            return false;
        }
        $k=1;
        $contenu = array();
        $contenu[0][0] = '';
        $contenu[1][0] = __("choisir la ").__("prescription_reglementaire");
        while($row = &$res->fetchRow()){
            $contenu[0][$k] = $row[0];
            $contenu[1][$k] = $row[1];
            $k++;
        }
        return $contenu;
    }

    /**
     *
     */
    function get_all_merge_fields($type) {
        //
        $all = array(
            "dossier_instruction",
            "dossier_coordination",
            "etablissement",
        );
        //
        switch ($type) {
            case 'values':
                //
                $values = array();
                //
                foreach ($all as $key => $value) {
                    $elem_method = "get_inst_".$value;
                    $elem = $this->$elem_method();
                    if ($elem != null) {
                        $elem_values = $elem->get_merge_fields($type);
                        $values = array_merge($values, $elem_values);                        
                    }
                }
                return $values;
                break;
            case 'labels':
                //
                $labels = array();
                foreach ($all as $key => $value) {
                    $elem = $this->f->get_inst__om_dbform(array(
                        "obj" => $value,
                        "idx" => 0,
                    ));
                    $elem_labels = $elem->get_merge_fields($type);
                    $labels = array_merge($labels, $elem_labels);
                }
                return $labels;
                break;
            default:
                return array();
                break;
        }
    }

    var $inst_dossier_instruction = null;
    var $inst_dossier_coordination = null;
    var $inst_etablissement = null;
    /**
     * 
     */
    function get_inst_dossier_instruction($dossier_instruction = null) {
        //
        if (!is_null($dossier_instruction)) {
            return $this->f->get_inst__om_dbform(array(
                "obj" => "dossier_instruction",
                "idx" => $dossier_instruction,
            ));
        }
        //
        if (is_null($this->inst_dossier_instruction)) {
            //
            $dossier_instruction = $this->getVal("dossier_instruction");
            $this->inst_dossier_instruction = $this->f->get_inst__om_dbform(array(
                "obj" => "dossier_instruction",
                "idx" => $dossier_instruction,
            ));
        }
        //
        return $this->inst_dossier_instruction;
    }

    /**
     * 
     */
    function get_inst_dossier_coordination($dossier_coordination = null) {
        //
        if (!is_null($dossier_coordination)) {
            return $this->f->get_inst__om_dbform(array(
                "obj" => "dossier_coordination",
                "idx" => $dossier_coordination,
            ));
        }
        //
        if (is_null($this->inst_dossier_coordination)) {
            //
            $inst_dossier_instruction = $this->get_inst_dossier_instruction();
            $this->inst_dossier_coordination = $inst_dossier_instruction->get_inst_dossier_coordination();
        }
        //
        return $this->inst_dossier_coordination;
    }

    /**
     *
     */
    function get_inst_etablissement($etablissement = null) {
        //
        if (!is_null($etablissement)) {
            return $this->f->get_inst__om_dbform(array(
                "obj" => "etablissement",
                "idx" => $etablissement,
            ));
        }
        //
        if (is_null($this->inst_etablissement)) {
            //
            $inst_dossier_instruction = $this->get_inst_dossier_instruction();
            $this->inst_etablissement = $inst_dossier_instruction->get_inst_etablissement();
        }
        //
        return $this->inst_etablissement;
    }

    /**
     * Permet de modifier le fil d'Ariane depuis l'objet pour un sous-formulaire
     * @param string    $subEnt Fil d'Ariane récupéréré 
     * @return                  Fil d'Ariane
     */
    function getSubFormTitle($subEnt) {

        return "";
    }

    // {{{ BEGIN - GET INST

    /**
     *
     */
    function get_inst_analyses_type($analyses_type = null) {
        return $this->get_inst_common("analyses_type", $analyses_type);
    }

    // }}} END - GET INST

}
