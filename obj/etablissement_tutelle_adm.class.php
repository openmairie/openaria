<?php
/**
 * Ce script définit la classe 'etablissement_tutelle_adm'.
 *
 * @package openaria
 * @version SVN : $Id$
 */

require_once "../gen/obj/etablissement_tutelle_adm.class.php";

/**
 * Définition de la classe 'etablissement_tutelle_adm' (om_dbform).
 */
class etablissement_tutelle_adm extends etablissement_tutelle_adm_gen {


    /**
     *
     */
    function setVal(&$form, $maj, $validation, &$dnu1 = null, $dnu2 = null) {
        //
        parent::setVal($form, $maj, $validation);
        //
        if ($validation==0) {
            if ($maj == 0){
                $form->setVal("om_validite_debut", date('Y-m-d'));
            }
        }
    }

}

