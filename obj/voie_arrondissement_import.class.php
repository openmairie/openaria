<?php
/**
 * Ce script définit la classe 'voie_arrondissement_import'.
 *
 * @package openaria
 * @version SVN : $Id$
 */

/**
 * Définition de la classe 'voie_arrondissement_import'.
 *
 * Cette classe contient les différentes méthodes permettant la synchronisation
 * des voies.
 */
class voie_arrondissement_import {

    var $table_voie;
    var $table_voie_arrondissement;

    /**
     * Constructeur.
     *
     * @param om_application $f handler de l'instance
     */
    function __construct($f) {
        $this->f = $f;
        $this->table_voie = DB_PREFIXE."voie";
        $this->table_voie_arrondissement = DB_PREFIXE."voie_arrondissement";
    }

    /**
     * Permet de savoir si une voie en base existe pour l'identifiant du
     * référentiel des voie passé en paramètre.
     *
     * @param string $id_voie identifiant de la voie dans le référentiel
     *
     * @return boolean false si pas de valeur dans la table false sinon
     */
    function is_voie_exist($id_voie) {
        if(empty($id_voie)) {
            return false;
        }
        $sql = "SELECT count(*) FROM ".DB_PREFIXE."voie WHERE id_voie_ref='".$id_voie."'";
        $count = $this->f->db->getone($sql);
        $this->f->isDatabaseError($count);
        if($count == 0) {
            return false;
        } elseif($count > 0) {
            return true;
        }
    }

    /**
     * Permet de savoir si une voie en base existe pour l'identifiant du
     * référentiel des voie passé en paramètre.
     *
     * @param string $id_voie identifiant de la voie dans le référentiel
     *
     * @return boolean false si pas de valeur dans la table false sinon
     */
    function is_voie_manuelle_exist($rivoli, $libelle) {
        if(empty($rivoli) or empty($libelle)) {
            return false;
        }
        $encodages = array("UTF-8", "ASCII", "Windows-1252", "ISO-8859-15", "ISO-8859-1");
        $sql = "SELECT voie FROM ".DB_PREFIXE."voie WHERE
        lpad(rivoli, 4, '0')='".iconv(mb_detect_encoding($rivoli, $encodages), "UTF-8", $rivoli)."' AND
        libelle='".$this->f->db->escapeSimple(iconv(mb_detect_encoding($libelle, $encodages), "UTF-8", $libelle))."'";
        $voie = $this->f->db->getone($sql);
        $this->f->isDatabaseError($voie);
        if(empty($voie)) {
            return false;
        } else {
            return $voie;
        }
    }

    /**
     * Récupération de l'id de la voie dont l'id du référentiel est passé
     * en paramètre.
     *
     * @param string $id_voie_ref id du référentiel
     *
     * @return mixed false si erreur bdd sinon id de la voie
     */
    function get_id_voie($id_voie_ref) {
        if(empty($id_voie_ref)) {
            return false;
        }
        $sql = "SELECT voie FROM ".DB_PREFIXE."voie WHERE id_voie_ref='".$id_voie_ref."'";
        $voie = $this->f->db->getone($sql);
        if($this->f->isDatabaseError($voie, true)) {
            return false;
        } else {
            return $voie;
        }
    }

    /**
     * Permet de mettre à jour les voies existantes ainsi que leurs liens aux 
     * arrondissements.
     *
     * @param array $valImport données d'une ligne du csv importé
     *
     * @return boolean false si erreur sini true
     */
    function update_voie($valImport) {
        if(empty($valImport) or count($valImport) != 4) {
            return false;
        }
        $encodages = array("UTF-8", "ASCII", "Windows-1252", "ISO-8859-15", "ISO-8859-1");

        $data=array();
        $data['libelle'] = iconv(mb_detect_encoding($valImport['lib_voie'], $encodages), "UTF-8", $valImport['lib_voie']);
        $data['rivoli'] = iconv(mb_detect_encoding($valImport["code_rivoli"], $encodages), "UTF-8", $valImport["code_rivoli"]);
        $data['om_validite_fin'] = null;
        // insertion de la voie
        $res = $this->f->db->autoExecute(
            $this->table_voie,
            $data,
            DB_AUTOQUERY_UPDATE,
            "id_voie_ref = '".$valImport['numero_voie']."'"
        );
        if ($this->f->isDatabaseError($res, true)) {
            return false;
        }
        // Récupération de l'id de la voie
        $voie = $this->get_id_voie($valImport['numero_voie']);
        if($voie === false) {
            return false;
        }
        // Création du lien voie/arrondissement
        if(
            $this->add_arrondissement_link(
                $voie,
                $valImport["numero_arrondissement"]
                ) === false
            ) {
            return false;
        }

    }

    /**
     * Permet de mettre à jour les voies existantes ajoutée manuellement
     * ainsi que leurs liens aux arrondissements.
     *
     * @param array $valImport données d'une ligne du csv importé
     *
     * @return boolean false si erreur sini true
     */
    function update_voie_by_lib($voie, $valImport) {
        if(empty($valImport) or count($valImport) != 4) {
            return false;
        }

        if($this->delete_voie_arrondissement($voie) === false) {
            return false;
        }
        $encodages = array("UTF-8", "ASCII", "Windows-1252", "ISO-8859-15", "ISO-8859-1");

        $data=array();
        $data['libelle'] = iconv(mb_detect_encoding($valImport['lib_voie'], $encodages), "UTF-8", $valImport['lib_voie']);
        $data['rivoli'] = iconv(mb_detect_encoding($valImport["code_rivoli"], $encodages), "UTF-8", $valImport["code_rivoli"]);
        $data['id_voie_ref'] = iconv(mb_detect_encoding($valImport["numero_voie"], $encodages), "UTF-8", $valImport["numero_voie"]);
        $data['om_validite_debut'] = date('Y-m-d');
        $data['om_validite_fin'] = null;
        // insertion de la voie
        $res = $this->f->db->autoExecute(
            $this->table_voie,
            $data,
            DB_AUTOQUERY_UPDATE,
            "voie = '".$voie."'"
        );
        if ($this->f->isDatabaseError($res, true)) {
            return false;
        }
        // Création du lien voie/arrondissement
        if(
            $this->add_arrondissement_link(
                $voie,
                $valImport["numero_arrondissement"]
                ) === false
            ) {
            return false;
        }

    }
    /**
     * Permet de créer les voies ainsi que leurs liens aux arrondissements.
     *
     * @param array $valImport données d'une ligne du csv importé
     *
     * @return boolean false si erreur sini true
     */
    function insert_voie($valImport) {

        if(empty($valImport) or count($valImport) != 4) {
            return false;
        }
        // Définition des encodages sources possibles
        $encodages = array("UTF-8", "ASCII", "Windows-1252", "ISO-8859-15", "ISO-8859-1");
        $data_voie=array();
        $data_voie['voie'] = $this->f->db->nextId(DB_PREFIXE."voie");
        $data_voie['libelle'] = iconv(mb_detect_encoding($valImport['lib_voie'], $encodages), "UTF-8", $valImport['lib_voie']);
        $data_voie['rivoli'] = iconv(mb_detect_encoding($valImport["code_rivoli"], $encodages), "UTF-8", $valImport["code_rivoli"]);
        $data_voie['id_voie_ref'] = iconv(mb_detect_encoding($valImport["numero_voie"], $encodages), "UTF-8", $valImport["numero_voie"]);
        $data_voie['om_collectivite'] = $this->f->collectivite['om_collectivite_idx'];
        $data_voie['om_validite_debut'] = date('Y-m-d');

        // insertion de la voie
        $res = $this->f->db->autoExecute(
            $this->table_voie,
            $data_voie,
            DB_AUTOQUERY_INSERT
        );
        if ($this->f->isDatabaseError($res, true)) {
            return false;
        }
        // Création du lien voie/arrondissement
        if(
            $this->add_arrondissement_link(
                $data_voie['voie'],
                $valImport["numero_arrondissement"]
                ) === false
            ) {
            return false;
        }

        return true;
    }

    /**
     * Archivage de toutes les voies précédement importées du référentiel.
     *
     * @return boolean false si erreur sinon true
     */
    function disable_voies() {
        $data_voie['om_validite_fin'] = date("Y-m-d");
        // insertion de la voie
        $res = $this->f->db->autoExecute(
            $this->table_voie,
            $data_voie,
            DB_AUTOQUERY_UPDATE,
            "id_voie_ref IS NOT NULL AND id_voie_ref <> '' AND (om_validite_fin IS NULL OR om_validite_fin > NOW())"
        );

        if ($this->f->isDatabaseError($res, true)) {
            return false;
        }

        return $this->f->db->affectedRows();
    }

    /**
     * Suppression de tous les liens des voies précédements importées.
     *
     * @return boolean false si erreur sinon true
     */
    function delete_voie_arrondissement($voie = null) {
        $sql = "DELETE FROM ".$this->table_voie_arrondissement."
            WHERE voie IN (
                SELECT voie FROM ".$this->table_voie;
        if($voie != null) {
            $sql .=" WHERE voie=".$voie.")";
        } else {
            $sql .=" WHERE id_voie_ref IS NOT NULL AND id_voie_ref <> '')";
        }
        $res = $this->f->db->query($sql);
        if ($this->f->isDatabaseError($res, true)) {
            return false;
        }
        return true;
    }

    /**
     * Création d'un lien entre la voie et l'arrondissement passé en paramètre.
     *
     * @param integer $id             identifiant de la voie
     * @param integer $arrondissement identifiant de l'arrondissement
     */
    function add_arrondissement_link($id, $arrondissement) {
        $data_voie_arrondissement["voie_arrondissement"] =
        $this->f->db->nextId(DB_PREFIXE."voie_arrondissement");

        $data_voie_arrondissement["voie"] = $id;
        $data_voie_arrondissement["arrondissement"] = $arrondissement;

        // Ajout du lien avec l'arrondissement
        $res = $this->f->db->autoExecute(
            $this->table_voie_arrondissement,
            $data_voie_arrondissement,
            DB_AUTOQUERY_INSERT
        );
        if ($this->f->isDatabaseError($res, true)) {
            return false;
        }

        return true;
    }


}

