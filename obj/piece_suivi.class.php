<?php
/**
 * Ce script définit la classe 'piece_suivi'.
 *
 * @package openaria
 * @version SVN : $Id$
 */

require_once "../obj/piece.class.php";

/**
 * Définition de la classe 'piece_suivi' (om_dbform).
 *
 * Surcharge de la classe 'piece'. Cette classe permet d'afficher seulement les
 * pièces qui sont suivi et dont la date butoir est dépassée.
 */
class piece_suivi extends piece {

    /**
     *
     */
    protected $_absolute_class_name = "piece_suivi";

}

