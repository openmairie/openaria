<?php
/**
 * Ce script définit la classe 'dossier_coordination_message'.
 *
 * @package openaria
 * @version SVN : $Id$
 */

require_once "../gen/obj/dossier_coordination_message.class.php";

/**
 * Définition de la classe 'dossier_coordination_message' (om_dbform).
 */
class dossier_coordination_message extends dossier_coordination_message_gen {


    /**
     *
     */
    function get_inst_dossier_coordination($dossier_coordination = null) {
        return $this->get_inst_common("dossier_coordination", $dossier_coordination);
    }

    /**
     *
     */
    function get_inst_dossier_instruction($dossier_instruction) {
        return $this->get_inst_common("dossier_instruction", $dossier_instruction);
    }

    /**
     *
     */
    function get_inst_dossier_instruction_for_user_service() {
        $inst_dc = $this->get_inst_dossier_coordination();
        $di_id = $inst_dc->get_id_dossier_instruction(
            $this->f->get_service_code($_SESSION["service"])
        );
        return $this->get_inst_dossier_instruction($di_id);
    }

    /**
     * Définition des actions disponibles sur la classe.
     *
     * @return void
     */
    function init_class_actions() {

        // On récupère les actions génériques définies dans la méthode
        // d'initialisation de la classe parente
        parent::init_class_actions();

        //
        $this->class_actions[0] = array();
        $this->class_actions[1] = array();
        $this->class_actions[2] = array();

        // ACTION - 031 - marquer_si_cadre_comme_lu
        $this->class_actions[31] = array(
            "identifier" => "marquer_si_cadre_comme_lu",
            "portlet" => array(
                "type" => "action-direct",
                "libelle" => __("marquer comme lu (cadre SI)"),
                "order" => 131,
                "class" => "mark-16",
            ),
            "view" => "formulaire",
            "method" => "marquer_si_cadre_comme_lu",
            "button" => "lu",
            "permission_suffix" => "marquer_si_cadre_comme_lu",
            "condition" => array(
                "is_si_mode_lecture_1_3",
                "is_si_cadre_unread",
            ),
        );

        // ACTION - 032 - marquer_si_cadre_comme_non_lu
        $this->class_actions[32] = array(
            "identifier" => "marquer_si_cadre_comme_non_lu",
            "portlet" => array(
                "type" => "action-direct",
                "libelle" => __("marquer comme non lu (cadre SI)"),
                "order" => 132,
                "class" => "unmark-16",
            ),
            "view" => "formulaire",
            "method" => "marquer_si_cadre_comme_non_lu",
            "button" => "non_lu",
            "permission_suffix" => "marquer_si_cadre_comme_non_lu",
            "condition" => array(
                "is_si_mode_lecture_1_3",
                "is_si_cadre_read",
            ),
        );
        // ACTION - 033 - marquer_si_cadre_comme_lu
        $this->class_actions[33] = array(
            "identifier" => "marquer_si_technicien_comme_lu",
            "portlet" => array(
                "type" => "action-direct",
                "libelle" => __("marquer comme lu (tech SI)"),
                "order" => 133,
                "class" => "mark-16",
            ),
            "view" => "formulaire",
            "method" => "marquer_si_technicien_comme_lu",
            "button" => "lu",
            "permission_suffix" => "marquer_si_technicien_comme_lu",
            "condition" => array(
                "is_si_mode_lecture_2_3",
                "is_si_technicien_unread",
                "is_user_di_instructor",
            ),
        );

        // ACTION - 034 - marquer_si_technicien_comme_non_lu
        $this->class_actions[34] = array(
            "identifier" => "marquer_si_technicien_comme_non_lu",
            "portlet" => array(
                "type" => "action-direct",
                "libelle" => __("marquer comme non lu (tech SI)"),
                "order" => 134,
                "class" => "unmark-16",
            ),
            "view" => "formulaire",
            "method" => "marquer_si_technicien_comme_non_lu",
            "button" => "non_lu",
            "permission_suffix" => "marquer_si_technicien_comme_non_lu",
            "condition" => array(
                "is_si_mode_lecture_2_3",
                "is_si_technicien_read",
                "is_user_di_instructor",
            ),
        );
        // ACTION - 035 - marquer_acc_cadre_comme_lu
        $this->class_actions[35] = array(
            "identifier" => "marquer_acc_cadre_comme_lu",
            "portlet" => array(
                "type" => "action-direct",
                "libelle" => __("marquer comme lu (cadre ACC)"),
                "order" => 135,
                "class" => "mark-16",
            ),
            "view" => "formulaire",
            "method" => "marquer_acc_cadre_comme_lu",
            "button" => "lu",
            "permission_suffix" => "marquer_acc_cadre_comme_lu",
            "condition" => array(
                "is_acc_mode_lecture_1_3",
                "is_acc_cadre_unread",
            ),
        );

        // ACTION - 036 - marquer_acc_cadre_comme_non_lu
        $this->class_actions[36] = array(
            "identifier" => "marquer_acc_cadre_comme_non_lu",
            "portlet" => array(
                "type" => "action-direct",
                "libelle" => __("marquer comme non lu (cadre ACC)"),
                "order" => 136,
                "class" => "unmark-16",
            ),
            "view" => "formulaire",
            "method" => "marquer_acc_cadre_comme_non_lu",
            "button" => "non_lu",
            "permission_suffix" => "marquer_acc_cadre_comme_non_lu",
            "condition" => array(
                "is_acc_mode_lecture_1_3",
                "is_acc_cadre_read",
            ),
        );
        // ACTION - 037 - marquer_acc_technicien_comme_lu
        $this->class_actions[37] = array(
            "identifier" => "marquer_acc_technicien_comme_lu",
            "portlet" => array(
                "type" => "action-direct",
                "libelle" => __("marquer comme lu (tech ACC)"),
                "order" => 137,
                "class" => "mark-16",
            ),
            "view" => "formulaire",
            "method" => "marquer_acc_technicien_comme_lu",
            "button" => "lu",
            "permission_suffix" => "marquer_acc_technicien_comme_lu",
            "condition" => array(
                "is_acc_mode_lecture_2_3",
                "is_acc_technicien_unread",
                "is_user_di_instructor",
            ),
        );

        // ACTION - 038 - marquer_acc_technicien_comme_non_lu
        $this->class_actions[38] = array(
            "identifier" => "marquer_acc_technicien_comme_non_lu",
            "portlet" => array(
                "type" => "action-direct",
                "libelle" => __("marquer comme non lu (tech ACC)"),
                "order" => 138,
                "class" => "unmark-16",
            ),
            "view" => "formulaire",
            "method" => "marquer_acc_technicien_comme_non_lu",
            "button" => "non_lu",
            "permission_suffix" => "marquer_acc_technicien_comme_non_lu",
            "condition" => array(
                "is_acc_mode_lecture_2_3",
                "is_acc_technicien_read",
                "is_user_di_instructor",
            ),
        );

        // ACTION - 081 - treatment_all_messages_async
        $this->class_actions[81] = array(
            "identifier" => "treatment_all_messages_async",
            "view" => "formulaire",
            "method" => "treat_all_messages_async",
            "button" => "treat_all_messages_async",
            "permission_suffix" => "controlpanel",
        );

        // ACTION - 099 - controlpanel
        $this->class_actions[99] = array(
            "identifier" => "controlpanel",
            "view" => "view_controlpanel",
            "permission_suffix" => "controlpanel",
        );
    }

    /**
     *
     */
    function get_var_sql_forminc__champs() {
        return array(
            "dossier_coordination_message",
            "categorie",
            "dossier_coordination",
            "type",
            "emetteur",
            "date_emission",
            "contenu",
            "contenu_json",
            "si_mode_lecture",
            "si_cadre_lu",
            "si_technicien_lu",
            "acc_mode_lecture",
            "acc_cadre_lu",
            "acc_technicien_lu",
        );
    }

    /**
     *
     */
    function setType(&$form, $maj) {
        //
        parent::setType($form, $maj);
        // Récupération du mode de l'action
        $crud = $this->get_action_crud($maj);
        // MODE AJOUTER
        if ($maj == 0 || $crud == 'create') {
            $form->setType("categorie", "select");
            $form->setType("si_mode_lecture", "select");
            $form->setType("acc_mode_lecture", "select");
            $form->setType("contenu_json", "hidden");
        }
        // MDOE MODIFIER
        if ($maj == 1 || $crud == 'update') {
            $form->setType("categorie", "selecthiddenstatic");
            $form->setType("si_mode_lecture", "selecthiddenstatic");
            $form->setType("acc_mode_lecture", "selecthiddenstatic");
            $form->setType("date_emission", "datetimehiddenstatic");
            $form->setType("contenu_json", "hidden");
        }
        // MODE SUPPRIMER
        if ($maj == 2 || $crud == 'delete') {
            $form->setType("contenu_json", "hidden");
        }
        // MODE CONSULTER
        if ($maj == 3 || $crud == 'read') {
            $form->setType("categorie", "selectstatic");
            $form->setType("si_mode_lecture", "hidden");
            $form->setType("acc_mode_lecture", "hidden");
            $form->setType("si_cadre_lu", "hidden");
            $form->setType("acc_cadre_lu", "hidden");
            $form->setType("si_technicien_lu", "hidden");
            $form->setType("acc_technicien_lu", "hidden");
            $form->setType("date_emission", "datetimehiddenstatic");
            $form->setType("contenu_json", "hidden");
        }

        // Pour les actions appelée en POST en Ajax, il est nécessaire de
        // qualifier le type de chaque champs (Si le champ n'est pas défini un
        // PHP Notice:  Undefined index dans core/om_formulaire.class.php est
        // levé). On sélectionne donc les actions de portlet de type
        // action-direct ou assimilé et les actions spécifiques avec le même
        // comportement : 81 - treatment_all_messages_async.
        if ($this->get_action_param($maj, "portlet_type") == "action-direct"
            || $this->get_action_param($maj, "portlet_type") == "action-direct-with-confirmation"
            || $maj == 81) {
            //
            foreach ($this->champs as $key => $value) {
                $form->setType($value, 'hidden');
            }
            $form->setType($this->clePrimaire, "hiddenstatic");
        }
    }

    /**
     *
     */
    function setLib(&$form, $maj) {
        //
        parent::setLib($form, $maj);
        //
        $form->setLib("dossier_coordination_message", __("identifiant"));
        $form->setLib("si_cadre_lu", __("Marqueur de lecture cadre SI"));
        $form->setLib("si_technicien_lu", __("Marqueur de lecture tech SI"));
        $form->setLib("si_mode_lecture", __("Mode de lecture SI"));
        $form->setLib("acc_cadre_lu", __("Marqueur de lecture cadre ACC"));
        $form->setLib("acc_technicien_lu", __("Marqueur de lecture tech ACC"));
        $form->setLib("acc_mode_lecture", __("Mode de lecture ACC"));
    }

    /**
     *
     */
    function setMax(&$form, $maj) {
        //
        parent::setMax($form, $maj);
        //
        $form->setMax("date_emission", 20);
    }

    /**
     *
     */
    function setTaille(&$form, $maj) {
        //
        parent::setTaille($form, $maj);
        //
        $form->setTaille("date_emission", 20);
    }

    /**
     *
     */
    function set_form_default_values(&$form, $maj, $validation) {
        //
        if ($validation == 0 && $maj == 0) {
            $form->setVal("date_emission", date("d/m/Y H:i:s"));
        }
    }

    /**
     *
     */
    function get_vocab_mode_lecture() {
        return array(
            "0" => array(
                "mode0",
                "mode1",
                "mode2",
                "mode3",
            ),
            "1" => array(
                "0 - Aucun marqueur",
                "1 - Marqueur de lecture 'cadre'",
                "2 - Marqueur de lecture 'tech'",
                "3 - Les deux marqueurs",
            ),
        );
    }

    /**
     * Méthode qui effectue les requêtes de configuration des champs.
     *
     * @param object  $form Instance du formulaire.
     * @param integer $maj  Mode du formulaire.
     * @param null    $dnu1 @deprecated Ancienne ressource de base de données.
     * @param null    $dnu2 @deprecated Ancien marqueur de débogage.
     *
     * @return void
     */
    function setSelect(&$form, $maj, &$dnu1 = null, $dnu2 = null) {
        //
        parent::setSelect($form, $maj, $dnu1, $dnu2);
        //
        $contenu = array(
            "0" => array("interne", "entrant", "sortant", ),
            "1" => array("interne", "entrant", "sortant", ),
        );
        $form->setSelect("categorie", $contenu);
        //
        $form->setSelect("si_mode_lecture", $this->get_vocab_mode_lecture());
        $form->setSelect("acc_mode_lecture", $this->get_vocab_mode_lecture());
    }

    /**
     *
     */
    function getFormTitle($ent) {
        //
        if ($this->getParameter("maj") == 99) {
            $ent = __("administration_parametrage")." -> ".__("messages");
        } elseif ($this->getParameter("maj") != 0) {
            $datetime_aff = "";
            if (DateTime::createFromFormat('Y-m-d H:i:s', $this->getVal("date_emission")) !== false) {
                $dateFormat = new DateTime($this->getVal("date_emission"));
                $datetime_aff = $dateFormat->format('d/m/Y H:i:s');
            }
            $ent .= " -> ".$datetime_aff;
        }
        return $ent;
    }

    /**
     *
     */
    function getSubFormTitle($ent) {
        //
        if ($this->getParameter("maj") != 0) {
            $datetime_aff = "";
            if (DateTime::createFromFormat('Y-m-d H:i:s', $this->getVal("date_emission")) !== false) {
                $dateFormat = new DateTime($this->getVal("date_emission"));
                $datetime_aff = $dateFormat->format('d/m/Y H:i:s');
            }
            $ent .= " -> ".$datetime_aff;
        }
        return $ent;
    }

    /**
     *
     */
    function formSpecificContent($maj) {
        if ($maj == 3) {
            echo $this->get_display_read_infos();
        }
    }

    /**
     *
     */
    function sousformSpecificContent($maj) {
        if ($maj == 3) {
            echo $this->get_display_read_infos();
        }
    }

    /**
     *
     */
    function get_display_read_infos() {
        $out = "";
        //
        $unread = '<span class="om-icon om-icon-16 om-icon-fix unread-16" title="Non Lu">Non Lu</span>';
        $read = '<span class="om-icon om-icon-16 om-icon-fix read-16" title="Lu">Lu</span>';
        $empty = '-';
        //
        $mode_lecture_template = '
        <div class="bloc-mode_lecture">
        <table class="table table-condensed table-bordered" style="width:auto;">
        <thead>
            <tr>
                <th style="width:150px;">Lecture</th>
                <th style="width:150px;">SI</th>
                <th style="width:150px;">ACC</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <th>Mode de lecture</th><td>%1$s</td><td>%4$s</td>
            </tr>
            <tr>
                <th>Marqueur de lecture \'cadre\'</th><td>%2$s</td><td>%5$s</td>
            </tr>
            <tr>
                <th>Marqueur de lecture \'tech\'</th><td>%3$s</td><td>%6$s</td>
            </tr>
        </tbody>
        </table>
        </div>
        ';
        //
        $vocab_mode_lecture = $this->get_vocab_mode_lecture();
        $label_si_mode_lecture = $vocab_mode_lecture[1][array_search(
            $this->getVal('si_mode_lecture'),
            $vocab_mode_lecture[0]
        )];
        $label_acc_mode_lecture = $vocab_mode_lecture[1][array_search(
            $this->getVal('acc_mode_lecture'),
            $vocab_mode_lecture[0]
        )];
        //
        if ($this->getVal("si_mode_lecture") == "mode1"
            || $this->getVal("si_mode_lecture") == "mode3") {
            if ($this->getVal("si_cadre_lu") == "t") {
                $si_cadre_lu = $read;
            } else {
                $si_cadre_lu = $unread;
            }
        } else {
            $si_cadre_lu = $empty;
        }
        //
        if ($this->getVal("si_mode_lecture") == "mode2"
            || $this->getVal("si_mode_lecture") == "mode3") {
            if ($this->getVal("si_technicien_lu") == "t") {
                $si_technicien_lu = $read;
            } else {
                $si_technicien_lu = $unread;
            }
        } else {
            $si_technicien_lu = $empty;
        }
        //
        if ($this->getVal("acc_mode_lecture") == "mode1"
            || $this->getVal("acc_mode_lecture") == "mode3") {
            if ($this->getVal("acc_cadre_lu") == "t") {
                $acc_cadre_lu = $read;
            } else {
                $acc_cadre_lu = $unread;
            }
        } else {
            $acc_cadre_lu = $empty;
        }
        //
        if ($this->getVal("acc_mode_lecture") == "mode2"
            || $this->getVal("acc_mode_lecture") == "mode3") {
            if ($this->getVal("acc_technicien_lu") == "t") {
                $acc_technicien_lu = $read;
            } else {
                $acc_technicien_lu = $unread;
            }
        } else {
            $acc_technicien_lu = $empty;
        }
        //
        return sprintf(
            $mode_lecture_template,
            $label_si_mode_lecture,
            $si_cadre_lu,
            $si_technicien_lu,
            $label_acc_mode_lecture,
            $acc_cadre_lu,
            $acc_technicien_lu
        );
    }

    /**
     * TREATMENT - mark_as_read_or_unread.
     *
     * Cette méthode permet de positionner le marqueur passé en paramètre à
     * 'true' ou à 'false'.
     *
     * @param array $val Tableau de paramètres :
     *                   - 'key' : chaine de caractères représentant le nom du
     *                     champ du marqueur ('acc_cadre_lu',
     *                    'si_technicien_lu', 'si_cadre_lu',
     *                    'acc_technicien_lu')
     *                   - 'value' : chaine de caractères représentant l'état
     *                     de destination ('read', 'unread')
     *
     * @return boolean
     */
    function mark_as_read_or_unread($val = array()) {
        $this->begin_treatment(__METHOD__);
        //
        $keys_available = array(
            "acc_cadre_lu",
            "acc_technicien_lu",
            "si_cadre_lu",
            "si_technicien_lu",
        );
        //
        if (!isset($val["key"]) || !in_array($val["key"], $keys_available)) {
            $this->addToMessage(__("Erreur"));
            return $this->end_treatment(__METHOD__, false);
        }
        //
        $values_available = array("read", "unread", );
        //
        if (!isset($val["value"]) || !in_array($val["value"], $values_available)) {
            $this->addToMessage(__("Erreur"));
            return $this->end_treatment(__METHOD__, false);
        }
        //
        $marker = null;
        if ($val["value"] == "read") {
            $marker = true;
        } elseif ($val["value"] == "unread") {
            $marker = false;
        }
        //
        $ret = $this->update_autoexecute(
            array(
                $val["key"] => $marker,
            ),
            null,
            false
        );
        if ($ret !== true) {
            return $this->end_treatment(__METHOD__, false);
        }
        if ($marker === true) {
            $this->addToMessage(__("Le message a été marqué comme lu."));
        } else {
            $this->addToMessage(__("Le message a été marqué comme non lu."));
        }
        return $this->end_treatment(__METHOD__, true);
    }


    /**
     * TREATMENT - marquer_si_cadre_comme_non_lu.
     *
     * @return boolean
     */
    function marquer_si_cadre_comme_non_lu($val = array()) {
        return $this->mark_as_read_or_unread(array(
            "key" => "si_cadre_lu",
            "value" => "unread",
        ));
    }

    /**
     * TREATMENT - marquer_si_cadre_comme_lu.
     *
     * @return boolean
     */
    function marquer_si_cadre_comme_lu($val = array()) {
        return $this->mark_as_read_or_unread(array(
            "key" => "si_cadre_lu",
            "value" => "read",
        ));
    }

    /**
     * TREATMENT - marquer_acc_cadre_comme_non_lu.
     *
     * @return boolean
     */
    function marquer_acc_cadre_comme_non_lu($val = array()) {
        return $this->mark_as_read_or_unread(array(
            "key" => "acc_cadre_lu",
            "value" => "unread",
        ));
    }

    /**
     * TREATMENT - marquer_acc_cadre_comme_lu.
     *
     * @return boolean
     */
    function marquer_acc_cadre_comme_lu($val = array()) {
        return $this->mark_as_read_or_unread(array(
            "key" => "acc_cadre_lu",
            "value" => "read",
        ));
    }

    /**
     * TREATMENT - marquer_si_technicien_comme_non_lu.
     *
     * @return boolean
     */
    function marquer_si_technicien_comme_non_lu($val = array()) {
        return $this->mark_as_read_or_unread(array(
            "key" => "si_technicien_lu",
            "value" => "unread",
        ));
    }

    /**
     * TREATMENT - marquer_si_technicien_comme_lu.
     *
     * @return boolean
     */
    function marquer_si_technicien_comme_lu($val = array()) {
        return $this->mark_as_read_or_unread(array(
            "key" => "si_technicien_lu",
            "value" => "read",
        ));
    }

    /**
     * TREATMENT - marquer_acc_technicien_comme_non_lu.
     *
     * @return boolean
     */
    function marquer_acc_technicien_comme_non_lu($val = array()) {
        return $this->mark_as_read_or_unread(array(
            "key" => "acc_technicien_lu",
            "value" => "unread",
        ));
    }

    /**
     * TREATMENT - marquer_acc_technicien_comme_lu.
     *
     * @return boolean
     */
    function marquer_acc_technicien_comme_lu($val = array()) {
        return $this->mark_as_read_or_unread(array(
            "key" => "acc_technicien_lu",
            "value" => "read",
        ));
    }


    /**
     * CONDITION - is_acc_mode_lecture_1_3.
     *
     * @return boolean
     */
    function is_acc_mode_lecture_1_3() {
        if ($this->getVal('acc_mode_lecture') == "mode1"
            || $this->getVal('acc_mode_lecture') == "mode3") {
            return true;
        }
        return false;
    }

    /**
     * CONDITION - is_acc_mode_lecture_2_3.
     *
     * @return boolean
     */
    function is_acc_mode_lecture_2_3() {
        if ($this->getVal('acc_mode_lecture') == "mode2"
            || $this->getVal('acc_mode_lecture') == "mode3") {
            return true;
        }
        return false;
    }

    /**
     * CONDITION - is_si_mode_lecture_1_3.
     *
     * @return boolean
     */
    function is_si_mode_lecture_1_3() {
        if ($this->getVal('si_mode_lecture') == "mode1"
            || $this->getVal('si_mode_lecture') == "mode3") {
            return true;
        }
        return false;
    }

    /**
     * CONDITION - is_si_mode_lecture_2_3.
     *
     * @return boolean
     */
    function is_si_mode_lecture_2_3() {
        if ($this->getVal('si_mode_lecture') == "mode2"
            || $this->getVal('si_mode_lecture') == "mode3") {
            return true;
        }
        return false;
    }

    /**
     * CONDITION - is_si_cadre_unread.
     *
     * @return boolean
     */
    function is_si_cadre_unread() {
        if ($this->getVal('si_cadre_lu') == "f") {
            return true;
        }
        return false;
    }

    /**
     * CONDITION - is_si_cadre_read.
     *
     * @return boolean
     */
    function is_si_cadre_read() {
        if ($this->getVal('si_cadre_lu') == "t") {
            return true;
        }
        return false;
    }

    /**
     * CONDITION - is_si_technicien_unread.
     *
     * @return boolean
     */
    function is_si_technicien_unread() {
        if ($this->getVal('si_technicien_lu') == "f") {
            return true;
        }
        return false;
    }

    /**
     * CONDITION - is_si_technicien_read.
     *
     * @return boolean
     */
    function is_si_technicien_read() {
        if ($this->getVal('si_technicien_lu') == "t") {
            return true;
        }
        return false;
    }

    /**
     * CONDITION - is_acc_cadre_unread.
     *
     * @return boolean
     */
    function is_acc_cadre_unread() {
        if ($this->getVal('acc_cadre_lu') == "f") {
            return true;
        }
        return false;
    }

    /**
     * CONDITION - is_acc_cadre_read.
     *
     * @return boolean
     */
    function is_acc_cadre_read() {
        if ($this->getVal('acc_cadre_lu') == "t") {
            return true;
        }
        return false;
    }

    /**
     * CONDITION - is_acc_technicien_unread.
     *
     * @return boolean
     */
    function is_acc_technicien_unread() {
        if ($this->getVal('acc_technicien_lu') == "f") {
            return true;
        }
        return false;
    }

    /**
     * CONDITION - is_acc_technicien_read.
     *
     * @return boolean
     */
    function is_acc_technicien_read() {
        if ($this->getVal('acc_technicien_lu') == "t") {
            return true;
        }
        return false;
    }

    /**
     * CONDITION - is_user_di_instructor.
     *
     * @return boolean
     */
    function is_user_di_instructor() {
        $inst_di = $this->get_inst_dossier_instruction_for_user_service();
        return $inst_di->is_user_di_instructor();
    }

    /**
     * TRIGGER - triggerajouter.
     *
     * - ...
     *
     * @return boolean
     */
    function triggerajouter($id, &$dnu1 = null, $val = array(), $dnu2 = null) {
        //
        $exchange = $this->get_config($this->valF["type"]);
        if ($exchange == null) {
            $this->addToMessage("Ce type de message n'est pas pris en charge.");
            $this->correct = false;
            return false;
        }
        //
        if (isset($exchange["mode_lecture"]) !== true) {
            $exchange["mode_lecture"] = 0;
        }
        $this->handle_mode_lecture($exchange["mode_lecture"]);
        //
        if (isset($exchange["treatment"]) === true) {
            if (isset($exchange["treatment"]["mode"]) === false
                || (isset($exchange["treatment"]["mode"]) === true
                    && $exchange["treatment"]["mode"] != "createasync")) {
                //
                $ret = $this->create_or_attach_dossier_coordination(array(
                    "treatment" => $exchange["treatment"],
                    "contenu" => $this->valF["contenu"],
                ));
                if ($ret !== true) {
                    $this->addToMessage("Impossible de connecter le message au référentiel.");
                    $this->correct = false;
                    return false;
                }
            }
        }
        //
        $method = "trigger_".$exchange["code"];
        if (method_exists($this, $method) === false) {
            return true;
        }
        $ret = $this->$method(array(
            "exchange" => $exchange,
        ));
        if ($ret !== true) {
            $this->addToMessage("Erreur lors de l'exécution du trigger.");
            $this->correct = false;
            return false;
        }
        //
        return true;
    }

    function trigger_113() {
        $inst_dc = $this->get_inst_dossier_coordination($this->valF["dossier_coordination"]);
        $ret = $inst_dc->update_autoexecute(
            array(
                "depot_de_piece" => true
            ),
            null,
            true
        );
        return $ret;
    }

    function trigger_114() {
        $inst_dc = $this->get_inst_dossier_coordination($this->valF["dossier_coordination"]);
        
        $enjeu_ads = false;

        $json = object_to_array(json_decode($this->valF["contenu_json"]));
        if ($json["contenu"]["Dossier à enjeu ADS"] == "oui"){
            $enjeu_ads = true;
        }

        $ret = $inst_dc->update_autoexecute(
            array(
                "enjeu_ads" => $enjeu_ads
            ),
            null,
            true
        );
        return $ret;
    }

    /**
     *
     */
    function handle_mode_lecture($mode_lecture) {
        //
        if (is_int($mode_lecture) === true) {
            $mode_lecture = array(
                "acc" => $mode_lecture,
                "si" => $mode_lecture,
            );
        }
        //
        if (is_array($mode_lecture) !== true) {
            return null;
        }
        //
        if (isset($mode_lecture["acc"]) !== true
            || isset($mode_lecture["si"]) !== true
            || in_array($mode_lecture["acc"], array(0, 1, 2, 3, ), true) !== true
            || in_array($mode_lecture["si"], array(0, 1, 2, 3, ), true) !== true) {
            return null;
        }
        //
        switch ($mode_lecture["acc"]) {
            case 0 :
                $this->valF["acc_cadre_lu"] = true;
                $this->valF["acc_technicien_lu"] = true;
                $this->valF["acc_mode_lecture"] = "mode0";
                break;
            case 1 :
                $this->valF["acc_cadre_lu"] = false;
                $this->valF["acc_technicien_lu"] = true;
                $this->valF["acc_mode_lecture"] = "mode1";
                break;
            case 2 :
                $this->valF["acc_cadre_lu"] = true;
                $this->valF["acc_technicien_lu"] = false;
                $this->valF["acc_mode_lecture"] = "mode2";
                break;
            case 3 :
                $this->valF["acc_cadre_lu"] = false;
                $this->valF["acc_technicien_lu"] = false;
                $this->valF["acc_mode_lecture"] = "mode3";
                break;
        }
        //
        switch ($mode_lecture["si"]) {
            case 0 :
                $this->valF["si_cadre_lu"] = true;
                $this->valF["si_technicien_lu"] = true;
                $this->valF["si_mode_lecture"] = "mode0";
                break;
            case 1 :
                $this->valF["si_cadre_lu"] = false;
                $this->valF["si_technicien_lu"] = true;
                $this->valF["si_mode_lecture"] = "mode1";
                break;
            case 2 :
                $this->valF["si_cadre_lu"] = true;
                $this->valF["si_technicien_lu"] = false;
                $this->valF["si_mode_lecture"] = "mode2";
                break;
            case 3 :
                $this->valF["si_cadre_lu"] = false;
                $this->valF["si_technicien_lu"] = false;
                $this->valF["si_mode_lecture"] = "mode3";
                break;
        }
    }

    /**
     *
     */
    function create_or_attach_dossier_coordination($params = array()) {

        // Instance de la classe dossier_coordination
        $inst_dc = $this->get_inst_dossier_coordination(0);

        //
        $di_ads = $this->get_dossier_instruction_ads_from_contenu($params["contenu"]);
        if ($di_ads === null) {
            $this->addToMessage("Impossible de réucupérer le dossier d'instruction ADS depuis le contenu.");
            return false;
        }

        //
        $di_ads_infos = $this->f->execute_action_to_referentiel_ads(
            212,
            array("dossier_instruction" => $di_ads, )
        );
        if ($di_ads_infos === false) {
            $this->correct = false;
            $this->addToMessage("Le dossier d'instruction n'existe pas dans le référentiel ADS.");
            return false;
        }

        /**
         * La clé 'treatment' du paramètre $params contient les instructions pour le traitement.
         *
         * $params["treatment"] = array(
         *     "mode" => "create|attach|create_or_attach|createasync",
         *     "dossier_coordination_type_code" => ...
         *     "extras" => array(
         *         "create_and_get_dcparent_infos|create_and_get_dcparent_infos_and_qualif" => array(
         *             "dossier_coordination_type_code|..." => ...
         *         )
         *     ),
         * );
         */
        $dossier_coordination_type_code = null;
        if (isset($params["treatment"]["dossier_coordination_type_code"])) {
            $dossier_coordination_type_code = $params["treatment"]["dossier_coordination_type_code"];
        }
        //
        $extras = array(
            // Configuration transmise
            "params" => null,
            // Liste des modes définis dans extras
            "extras" => array(),
        );
        if (is_array($params) === true
            && array_key_exists("treatment", $params) === true
            && is_array($params["treatment"]) === true
            && array_key_exists("extras", $params["treatment"]) === true
            && is_array($params["treatment"]["extras"]) === true) {
            // On conserve la configuration transmise
            $extras["params"] = $params["treatment"]["extras"];
            // On récupère les extras définis dans extras
            $extras["extras"] = array_keys($params["treatment"]["extras"]);
        }


        // On cherche un dossier de coordination dont le champ DI ADS
        // correspond au paramètre et éventuellement au type de DC
        // et qui est connecté au référentiel ADS
        $ret = $inst_dc->get_dossier_coordination_for_dossier_ads(
            $di_ads,
            $dossier_coordination_type_code
        );
        if ($ret === false) {
            $this->addToMessage("Erreur interne.");
            return false;
        }

        //
        $mode = "";
        if ($params["treatment"]["mode"] == "create"
            || $params["treatment"]["mode"] == "createasync") {
            //
            if ($ret !== 0) {
                $this->addToMessage("Un dossier existe déjà. Création impossible.");
                return false;
            }
            $mode = "create";
        } elseif ($params["treatment"]["mode"] == "create_or_attach") {
            if ($ret === 0) {
                $mode = "create";
            } elseif (is_array($ret) === true) {
                $this->addToMessage("Plusieurs dossiers existent. Rattachement impossible.");
                return false;
            } else {
                $mode = "attach";
            }
        } elseif ($params["treatment"]["mode"] == "attach") {
            if ($ret === 0) {
                $this->addToMessage("Aucun dossier. Rattachement impossible.");
                return false;
            } elseif (is_array($ret) === true) {
                $this->addToMessage("Plusieurs dossiers existent. Rattachement impossible.");
                $this->addToErrors("plup", "plop", "plip");
                return false;
            }
            $mode = "attach";
        } elseif ($params["treatment"]["mode"] == "attachmulti") {
            if ($this->valF["dossier_coordination"] === null) {
                $this->addToMessage("Aucun dossier fourni.");
                return false;
            }
            return true;
        }
        //
        if ($mode == "attach") {
            $this->valF["dossier_coordination"] = $ret;
            return true;
        }
        if ($mode == "") {
            $this->addToMessage("Erreur interne.");
            return false;
        }

        // Instance de la classe dossier_coordination
        $inst_dc = $this->get_inst_dossier_coordination("]");
        // Initialise à vide tous les champs de l'enregistrement
        foreach ($inst_dc->champs as $champ) {
            $val[$champ] = "";
        }
        $references_cadastrales = "";
        foreach ($di_ads_infos["references_cadastrales"] as $key => $value) {
            $references_cadastrales .= $value["quartier"].$value["section"].$value["parcelle"].";";
        }
        //
        $inst_dc_type = $this->get_inst_common(
            "dossier_coordination_type",
            $inst_dc->get_dossier_coordination_type_by_code($dossier_coordination_type_code)
        );

        //
        // Par défaut, on récupère l'information de qualification du dossier de coordination
        // depuis le paramétrage du type de DC
        //
        $dc_a_qualifier = $inst_dc_type->getVal("a_qualifier");
        $dc_dossier_instruction_secu = $inst_dc_type->getVal("dossier_instruction_secu");
        $dc_dossier_instruction_acc = $inst_dc_type->getVal("dossier_instruction_acc");

        //
        // Gestion d'un éventuel lien avec un DC parent
        //
        $dc_parent = null;
        $dc_parent_etablissement = null;
        $dc_parent_erp = null;
        $dc_parent_etablissement_type = null;
        $dc_parent_etablissement_categorie = null;
        $dc_parent_etablissement_locaux_sommeil = null;
        $dc_parent_etablissement_types_secondaires = null;
        if (in_array("create_and_get_dcparent_infos", $extras["extras"]) === true
            || in_array("create_and_get_dcparent_infos_and_qualif", $extras["extras"]) === true) {
            // On récupère le type de DC sur lequel filtrer les dc parents à récupérer
            $dc_type = null;
            if (in_array("create_and_get_dcparent_infos", $extras["extras"]) === true) {
                if (is_array($extras["params"]["create_and_get_dcparent_infos"]) === true
                    && array_key_exists("dossier_coordination_type_code", $extras["params"]["create_and_get_dcparent_infos"]) === true) {
                    $dc_type = $extras["params"]["create_and_get_dcparent_infos"]["dossier_coordination_type_code"];
                }
            } elseif (in_array("create_and_get_dcparent_infos_and_qualif", $extras["extras"]) === true) {
                if (is_array($extras["params"]["create_and_get_dcparent_infos_and_qualif"]) === true
                    && array_key_exists("dossier_coordination_type_code", $extras["params"]["create_and_get_dcparent_infos_and_qualif"]) === true) {
                    $dc_type = $extras["params"]["create_and_get_dcparent_infos_and_qualif"]["dossier_coordination_type_code"];
                }
            }
            // On cherche un dossier de coordination dont le champ DI ADS
            // correspond au paramètre et éventuellement au type de DC
            // et qui est connecté au référentiel ADS
            $ret = $inst_dc->get_dossier_coordination_for_dossier_ads(
                $di_ads,
                $dc_type
            );
            if ($ret === false) {
                $this->addToMessage("Erreur interne.");
                return false;
            }
            if ($ret === 0) {
                // On a trouvé aucun DC ARIA qui correspond à un DI ADS, on cherche donc
                // un DC ARIA qui correspond à un DA ADS.
                $da_ads = $di_ads_infos["dossier_autorisation"];
                $ret = $inst_dc->get_dossier_coordination_for_dossier_ads(
                    $da_ads,
                    $dc_type,
                    "da"
                );
            }
            if ($ret !== 0 && is_array($ret) !== true) {
                //
                $dc_parent = $ret;
                $inst_dc_parent = $this->get_inst_common(
                    "dossier_coordination",
                    $dc_parent
                );
                $dc_parent_etablissement = $inst_dc_parent->getVal("etablissement");
                $dc_parent_erp = $inst_dc_parent->getVal("erp");
                $dc_parent_etablissement_type = $inst_dc_parent->getVal("etablissement_type");
                $dc_parent_etablissement_categorie = $inst_dc_parent->getVal("etablissement_categorie");
                $dc_parent_etablissement_locaux_sommeil = $inst_dc_parent->getVal("etablissement_locaux_sommeil");
                $dc_parent_etablissement_types_secondaires = $inst_dc_parent->get_etablissement_type_secondaire();
                //
                if (in_array("create_and_get_dcparent_infos_and_qualif", $extras["extras"]) === true) {
                    $dc_a_qualifier = $inst_dc_parent->getVal("a_qualifier");
                    $dc_dossier_instruction_secu = $inst_dc_parent->getVal("dossier_instruction_secu");
                    $dc_dossier_instruction_acc = $inst_dc_parent->getVal("dossier_instruction_acc");
                }
            }
        }

        //
        // Composition de l'enregistrement
        //
        $valF = array(
            //
            "dossier_coordination_type" => $inst_dc_type->getVal($inst_dc_type->clePrimaire),
            //
            "a_qualifier" => $dc_a_qualifier,
            "dossier_instruction_secu" => $dc_dossier_instruction_secu,
            "dossier_instruction_acc" => $dc_dossier_instruction_acc,
            //
            "dossier_instruction_ads" => $di_ads,
            "dossier_autorisation_ads" => $di_ads_infos["dossier_autorisation"],
            //
            "date_demande" => date('d/m/Y'),
            "references_cadastrales" => $references_cadastrales,
            //
            "dossier_coordination_parent" => $dc_parent,
            "etablissement" => $dc_parent_etablissement,
            "erp" => $dc_parent_erp,
            "etablissement_type" => $dc_parent_etablissement_type,
            "etablissement_categorie" => $dc_parent_etablissement_categorie,
            "etablissement_locaux_sommeil" => $dc_parent_etablissement_locaux_sommeil,
        );
        // Fusionne les tableaux de données
        $data = array_merge($val, $valF);
        // Ajoute le dossier de coordination
        $add = $inst_dc->ajouter($data);
        // Si l'ajout échoue
        if ($add === false) {
            $this->f->addToLog(__METHOD__."(): Echec lors de la création du DC. ".$inst_dc->msg, DEBUG_MODE);
            // Stop le traitement
            return false;
        }
        $id = $inst_dc->valF[$inst_dc->clePrimaire];
        $this->valF["dossier_coordination"] = $id;
        // Gestion des types secondaires dans le cas de la gestion du dossier parent
        if (is_array($dc_parent_etablissement_types_secondaires) === true
            && count($dc_parent_etablissement_types_secondaires) > 0) {
            //
            foreach ($dc_parent_etablissement_types_secondaires as $value) {
                // 
                $ret = $inst_dc->add_type_secondaire(array(
                    'lien_dossier_coordination_etablissement_type' => "",
                    'dossier_coordination' => $inst_dc->valF[$inst_dc->clePrimaire],
                    'etablissement_type' => $value["etablissement_type"]
                ));
                if ($ret !== true) {
                    return false;
                }
            }
        }

        //
        $ret = $this->create_contacts($di_ads_infos, $id);
        if ($ret !== true) {
            return false;
        }

        //
        $inst_dc = $this->get_inst_dossier_coordination($id);
        $ret = $inst_dc->mark_as_connected_to_referentiel_ads();
        if ($ret !== true) {
            return false;
        }

        return true;
    }

    /**
     * @return boolean
     */
    function create_contacts($di_ads_infos, $dc_id) {
        //
        $contacts = array();
        // petitionnaire_principal
        $contacts[0] = $di_ads_infos["petitionnaire_principal"];
        // autres_petitionnaires
        if (array_key_exists("autres_petitionnaires", $di_ads_infos) === true
            && is_array($di_ads_infos["autres_petitionnaires"]) === true) {
            //
            foreach ($di_ads_infos["autres_petitionnaires"] as $key => $value) {
                $contacts[] = $value;
            }
        }
        //
        $inst_contact_type = $this->get_inst_common("contact_type", 0);
        $contact_type_prin = $inst_contact_type->get_id_from_code('PRIN');
        $contact_type_autr = $inst_contact_type->get_id_from_code('AUTR');
        //
        $inst_contact_civilite = $this->get_inst_common("contact_civilite", 0);
        //
        foreach ($contacts as $key => $contact) {
            //
            if ($key == 0) {
                $contact_type = $contact_type_prin; // PRIN 'Demandeur Principal'
            } else {
                $contact_type = $contact_type_autr; // AUTR 'Autre Demandeur'
            }
            //
            if (is_int($contact["numero"]) === true) {
                $adresse_numero = $contact["numero"];
                $adresse_numero2 = "";
            } else {
                $adresse_numero = null;
                $adresse_numero2 = $contact["numero"];
            }
            //
            if ($contact["qualite"] == "particulier") {
                $civilite = $inst_contact_civilite->get_id_from_libelle(
                    $contact["particulier_civilite"]
                );
                $nom = $contact["particulier_nom"];
                $prenom = $contact["particulier_prenom"];
                $denomination = "";
                $raison_sociale = "";
                $siret = "";
                $categorie_juridique = "";
            } else {
                $civilite = $inst_contact_civilite->get_id_from_libelle(
                    $contact["personne_morale_civilite"]
                );
                $nom = $contact["personne_morale_nom"];
                $prenom = $contact["personne_morale_prenom"];
                $denomination = $contact["personne_morale_denomination"];
                $raison_sociale = $contact["personne_morale_raison_sociale"];
                $siret = $contact["personne_morale_siret"];
                $categorie_juridique = $contact["personne_morale_categorie_juridique"];
            }
            //
            $inst_contact = $this->get_inst_common("contact", "]");
            $val = array(
                "contact" => "",
                "etablissement" => null,
                "contact_type" => $contact_type,
                "qualite" => $contact["qualite"],
                "civilite" => $civilite,
                "nom" => $nom,
                "prenom" => $prenom,
                "titre" => "",
                "telephone" => $contact["telephone_fixe"],
                "mobile" => $contact["telephone_mobile"],
                "fax" => $contact["fax"],
                "courriel" => $contact["courriel"],
                "adresse_numero" => $adresse_numero,
                "adresse_numero2" => $adresse_numero2,
                "adresse_voie" => $contact["voie"],
                "adresse_complement" => $contact["complement"],
                "adresse_cp" => $contact["code_postal"],
                "adresse_ville" => $contact["localite"],
                "lieu_dit" => $contact["lieu_dit"],
                "boite_postale" => $contact["bp"],
                "cedex" => $contact["cedex"],
                "pays" => $contact["pays"],
                "denomination" => $denomination,
                "raison_sociale" => $raison_sociale,
                "siret" => $siret,
                "categorie_juridique" => $categorie_juridique,
                "service" => null,
                "reception_convocation" => null,
                "reception_programmation" => null,
                "reception_commission" => null,
                "om_validite_debut" => null,
                "om_validite_fin" => null,
            );
            $ret = $inst_contact->ajouter($val);
            if ($ret !== true) {
                return false;
            }
            //
            $inst_lien_dc_contact = $this->get_inst_common("lien_dossier_coordination_contact", "]");
            $val = array(
                "lien_dossier_coordination_contact" => "",
                "dossier_coordination" => $dc_id,
                "contact" => $inst_contact->valF[$inst_contact->clePrimaire],
            );
            $ret = $inst_lien_dc_contact->ajouter($val);
            if ($ret !== true) {
                return false;
            }
        }
        //
        return true;
    }

    /**
     *
     */
    function get_config($type = null) {
        //
        $interface_referentiel_ads = $this->f->get_inst_referentiel_ads();
        // Toutes les actions
        if ($type == null) {
            return $interface_referentiel_ads->actions;            
        }
        // Une action en particulier
        foreach ($interface_referentiel_ads->actions as $key => $value) {
            if ($value["type"] === $type) {
                return $value;
            }
        }
        return null;
    }


    /**
     *
     */
    function get_dossier_instruction_ads_from_contenu($contenu) {
        $elems = explode("\n", $contenu);
        foreach ($elems as $key => $value) {
            if ($this->f->starts_with($value, "dossier_instruction :") === true) {
                $dossier_instruction = str_replace(
                    "dossier_instruction :",
                    "",
                    $value
                );
                return trim($dossier_instruction);
            }
        }
        return null;
    }


    /**
     * TREATMENT - treat_all_messages_async
     *
     * @return boolean
     */
    function treat_all_consultation($val = array()) {
        //On recupere les consultation non traiter
        $messages_consultation = $this->get_all_not_treated_consultation();
        if ($messages_consultation === false) {
            $this->addToMessage("Erreur interne sur la récupération des messages.");
            return false;
        }
        $treatment_results = 0;
        $date = new DateTime();

        //Pour chaque consultation non traitée
        foreach ($messages_consultation as $key => $value) {
            // On recupere le contenu (consultation et dossier instruction)
            $json = json_decode($value["contenu_json"], true);
            $consultation = $json['contenu']['consultation'];
            $infos = array(
                "consultation" => $consultation,
                "dossier_instruction" => $this->get_dossier_instruction_ads_from_contenu($value['contenu']),
                "dossier_coordination" => $value['dossier_coordination']
            );

            // On marque le dossier traiter
            $json['contenu']['date_traitement'] = $this->dateDB($date->format('d/m/Y'));
            $valF = array('contenu_json' => json_encode($json));
            $res = $this->f->db->autoExecute(DB_PREFIXE.$this->table, $valF, DB_AUTOQUERY_UPDATE, $this->getCle($value["dossier_coordination_message"]));
            $this->f->addToLog(__METHOD__."(): db->autoExecute(\"".DB_PREFIXE.$this->table."\", ".print_r($valF, true).", DB_AUTOQUERY_UPDATE, \"".$this->getCle($value["dossier_coordination_message"])."\")", VERBOSE_MODE);
            if ($this->f->isDatabaseError($res, true)) {
                $this->erreur_db($res->getDebugInfo(), $res->getMessage(), '');
                return false;
            }
            // On le transmet au referentiel
            $ret = $this->f->execute_action_to_referentiel_ads(213, $infos);
            if ($ret === false) {
                $this->addToMessage(
                    sprintf(
                        "%s(): message %s : Erreur lors de l'exécution de la requête.",
                        __METHOD__,
                        $value["dossier_coordination_message"]
                    )
                );
                return false;
            }
            ++$treatment_results;
        }
        $this->correct = true;
        $this->addToMessage(
            sprintf(
                "Traitement terminé. %s message(s) traité(s).",
                $treatment_results
            )
        );
        return true;
    }

    /**
     * Cette méthode retourne la liste des messages de consultation
     * pour toutes les consultations non traitées.
     *
     * @return mixed En cas d'erreur de base de données 'false' sinon un tableau.
     */
    function get_all_not_treated_consultation() {
        //
        $query = sprintf(
            "SELECT * FROM %sdossier_coordination_message
            WHERE (type LIKE 'ADS_ERP__PC__CONSULTATION_OFFICIELLE_POUR_AVIS'
            OR type LIKE 'ADS_ERP__PC__CONSULTATION_OFFICIELLE_POUR_CONFORMITE')
            AND contenu_json NOT LIKE '%%date_traitement%%'",
            DB_PREFIXE
        );
        $res = $this->f->db->query($query);
        $this->addToLog(
            __METHOD__."(): db->query(\"".$query."\");",
            VERBOSE_MODE
        );
        if ($this->f->isDatabaseError($res, true)) {
            return false;
        }
        //
        $list = array();
        while ($row =& $res->fetchrow(DB_FETCHMODE_ASSOC)) {
            $list[] = $row;
        }
        return $list;
    }


    /**
     * TREATMENT - treat_all_messages_async
     *
     * @return boolean
     */
    function treat_all_messages_async($val = array()) {
        //
        $messagesasync = $this->get_all_messages_async();
        if ($messagesasync === false) {
            $this->addToMessage("Erreur interne sur la récupération des messages.");
            return false;
        }
        //
        $treatment_results = array(
            "ok" => 0,
            "ko" => 0,
            "total" => count($messagesasync),
        );
        //
        foreach ($messagesasync as $key => $value) {
            //
            $exchange = $this->get_config($value["type"]);
            if ($exchange == null) {
                $this->addToLog(
                    sprintf(
                        "%s(): message %s %s %s : Ce type de message n'est pas pris en charge.",
                        __METHOD__,
                        $value["dossier_coordination_message"],
                        $value["date_emission"],
                        $value["type"]
                    ),
                    DEBUG_MODE
                );
                $treatment_results["ko"] += 1;
                continue;
            }
            //
            if (isset($exchange["treatment"]) === true) {
                //
                $ret = $this->create_or_attach_dossier_coordination(array(
                    "treatment" => $exchange["treatment"],
                    "contenu" => $value["contenu"],
                ));
                if ($ret !== true) {
                    $this->addToLog(
                        sprintf(
                            "%s(): message %s %s %s : Impossible de connecter le message au référentiel.",
                            __METHOD__,
                            $value["dossier_coordination_message"],
                            $value["date_emission"],
                            $value["type"]
                        ),
                        DEBUG_MODE
                    );
                    $treatment_results["ko"] += 1;
                    continue;
                }
                //
                $inst_dcm = $this->f->get_inst__om_dbform(array(
                    "obj" => "dossier_coordination_message",
                    "idx" => $value["dossier_coordination_message"],
                ));
                $ret = $inst_dcm->update_autoexecute(
                    array(
                        "dossier_coordination" => $this->valF["dossier_coordination"],
                    ),
                    null,
                    false
                );
                if ($ret !== true) {
                    $this->addToLog(
                        sprintf(
                            "%s(): message %s %s %s : Impossible de lier le message à son dossier de coordination.",
                            __METHOD__,
                            $value["dossier_coordination_message"],
                            $value["date_emission"],
                            $value["type"]
                        ),
                        DEBUG_MODE
                    );
                    $treatment_results["ko"] += 1;
                    continue;
                }
            }
            //
            $method = "trigger_".$exchange["code"];
            if (method_exists($this, $method) === true) {
                $ret = $this->$method(array(
                    "exchange" => $exchange,
                ));
                if ($ret !== true) {
                    $this->addToLog(
                        sprintf(
                            "%s(): message %s %s %s : Erreur lors de l'exécution du trigger.",
                            __METHOD__,
                            $value["dossier_coordination_message"],
                            $value["date_emission"],
                            $value["type"]
                        ),
                        DEBUG_MODE
                    );
                    $treatment_results["ko"] += 1;
                    continue;
                }
            }
            //
            $treatment_results["ok"] += 1;
        }
        //
        $this->correct = true;
        $this->addToMessage(
            sprintf(
                "Traitement terminé. %s message(s) traité(s).",
                $treatment_results["total"],
                $treatment_results["ok"],
                $treatment_results["ko"]
            )
        );
        return true;
    }

    /**
     * Cette méthode retourne la liste des messages dont le mode de création de
     * DC est asynchrone et qui ne sont pas rattachés à aucun DC.
     *
     * @return mixed En cas d'erreur de base de données 'false' sinon un tableau.
     */
    function get_all_messages_async() {
        //
        $types_async = array();
        //
        $config = $this->get_config();
        foreach ($config as $key => $value) {
            if (isset($value["treatment"]) === true
                && isset($value["treatment"]["mode"]) === true
                && isset($value["treatment"]["mode"]) === "createasync") {
                $types_async[] = $value["type"];
            }
        }
        //
        $types_async_selection = "";
        if (count($types_async) != 0) {
            $types_async_selection = " AND type IN (";
            foreach ($types_async as $key => $value) {
                if ($key != "0") {
                    $types_async_selection .= ",";
                }
                $types_async_selection .= "'".$value."'";
            }
            $types_async_selection .= ")";
        }
        //
        $query = sprintf(
            "SELECT * FROM %sdossier_coordination_message WHERE dossier_coordination IS NULL %s ORDER BY date_emission",
            DB_PREFIXE,
            $types_async_selection
        );
        $res = $this->f->db->query($query);
        $this->addToLog(
            __METHOD__."(): db->query(\"".$query."\");",
            VERBOSE_MODE
        );
        if ($this->f->isDatabaseError($res, true)) {
            return false;
        }
        //
        $list = array();
        while ($row =& $res->fetchrow(DB_FETCHMODE_ASSOC)) {
            $list[] = $row;
        }
        return $list;
    }

    /**
     * VIEW - view_controlpanel.
     *
     * @return void
     */
    function view_controlpanel() {
        //
        $this->checkAccessibility();
        /**
         * Traitements
         */
        $this->f->displaySubTitle("Traitements");
        printf(
            '
            <ul class="portlet-list">
            <li>
            <a 
            href="#"
            id="action-controlpanel-dossier_coordination_message-treatment_all_messages_async"
            class="action action-direct action-with-confirmation"
            title="Ce traitement permet de gérer tous les messages dont le mode de création de DC est asynchrone"
            data-href="'.OM_ROUTE_FORM.'&obj=dossier_coordination_message&amp;action=81&amp;idx=0">
            <span class="om-prev-icon om-icon-16">Traitement des messages asynchrones</span>
            </a>
            </li>
            </ul>
            <br/>'
        );
        /**
         * Stats
         */
        $this->f->displaySubTitle("Statistiques");
        //
        $total = 0;
        $sql = "select categorie, type, count(*) from ".DB_PREFIXE."dossier_coordination_message  group by categorie, type order by categorie DESC, type;";
        $res = $this->f->db->query($sql);
        $this->f->isDatabaseError($res);
        $results = array();
        while ($row =& $res->fetchrow(DB_FETCHMODE_ASSOC)) {
            $results[] = $row;
            $total += $row['count'];
        }
        //
        $stats = array();
        //
        foreach ($this->get_config() as $key => $value) {
            $stat = $value;
            $stat["code"] = $key;
            $stat["count"] = "";
            foreach ($results as $result) {
                if ($result["type"] === $value["type"]) {
                    $stat["count"] = $result["count"];
                }
            }
            $stats[] = $stat;
        }
        //
        $table_template = '
        Total : %s
        <table class="table table-condensed table-bordered table-striped table-hover">
            <tr><th>categorie</th><th>code</th><th>type</th><th>label</th><th>count</th></tr>
            %s
        </table>';
        //
        $line_template = '<tr class="%s"><td>%s</td><td>%s</td><td>%s</td><td>%s</td><td>%s</td></tr>';
        //
        $table_content = '';
        foreach ($stats as $key => $value) {
            $table_content .= sprintf(
                $line_template,
                ($value['categorie'] == "entrant" ? "warning" : "info"),
                $value['categorie'],
                $value['code'],
                $value["type"],
                $value["label"],
                $value["count"]
            );
        }
        //
        printf(
            $table_template,
            $total,
            $table_content
        );
    }

}

