<?php
/**
 * Ce script définit la classe 'acteur'.
 *
 * @package openaria
 * @version SVN : $Id$
 */

require_once "../gen/obj/acteur.class.php";

/**
 * Définition de la classe 'acteur' (om_dbform).
 */
class acteur extends acteur_gen {

    var $inst_om_utilisateur = null;

    /**
     *
     */
    function get_inst_om_utilisateur($om_utilisateur = null) {
        //
        if (is_null($this->inst_om_utilisateur)) {
            //
            if (is_null($om_utilisateur)) {
                $om_utilisateur = $this->getVal("om_utilisateur");
            }
            //
            $this->inst_om_utilisateur = $this->f->get_inst__om_dbform(array(
                "obj" => "om_utilisateur",
                "idx" => $om_utilisateur,
            ));
        }
        //
        return $this->inst_om_utilisateur;
    }

    function setType(&$form, $maj) {
        //
        parent::setType($form, $maj);
        //type
        if ($maj==0){ //ajout
            $form->setType('role', 'select');
            $form->setType('couleur', 'color');
        }// fin ajout
        if ($maj==1){ //modifier
            $form->setType('role', 'select');
            $form->setType('couleur', 'color');
        }// fin modifier
        if ($maj==2){ //supprimer
            $form->setType('role', 'selectstatic');
        }//fin supprimer
        if ($maj==3){ //consulter
            $form->setType('role', 'selectstatic');
        }//fin consulter
    }

    /**
     * Méthode qui effectue les requêtes de configuration des champs.
     *
     * @param object  $form Instance du formulaire.
     * @param integer $maj  Mode du formulaire.
     * @param null    $dnu1 @deprecated Ancienne ressource de base de données.
     * @param null    $dnu2 @deprecated Ancien marqueur de débogage.
     *
     * @return void
     */
    function setSelect(&$form, $maj, &$dnu1 = null, $dnu2 = null) {
        //
        parent::setSelect($form, $maj, $dnu1, $dnu2);
        // widget config - role
        $contenu=array();
        $contenu[0]=array(
            '',
            'cadre',
            'qualificateur',
            'secretaire',
            'technicien'
            );
        $contenu[1]=array(
            __('choisir role'),
            __('cadre'),
            __('qualificateur'),
            __('secretaire'),
            __('technicien')
            );
        $form->setSelect("role", $contenu);
    }

    /**
     *
     */
    var $merge_fields_to_avoid_obj = array(
        "om_validite_debut",
        "om_validite_fin",
        "acteur",
        "om_utilisateur",
        "couleur",
    );

    /**
     * Surcharge de la récupération des libellés des champs de fusion
     * 
     * @return [array]  tableau associatif objet => champ de fusion => libellé
     */
    function get_labels_merge_fields() {
        //
        $labels = parent::get_labels_merge_fields();
        // On récupère l'email et le login de l'acteur dans la table om_utilisateur
        $labels["acteur"]["acteur.login"] = __("login");
        $labels["acteur"]["acteur.email"] = __("email");
        // Retour de tous les libellés
        return $labels;
    }

     /**
     * Surcharge de la récupération des valeurs des champs de fusion
     * 
     * @return [array]  tableau associatif champ de fusion => valeur
     */
    function get_values_merge_fields() {
        //
        $values = parent::get_values_merge_fields();
        // On récupère l'email et le login de l'acteur dans la table om_utilisateur
        $inst_om_utilisateur = $this->get_inst_om_utilisateur();
        $values["acteur.login"] = $inst_om_utilisateur->getVal("login");
        $values["acteur.email"] = $inst_om_utilisateur->getVal("email");
        // Retour de tous les libellés
        return $values;
    }
}
