<?php
/**
 * Ce script définit la classe 'programmation'.
 *
 * @package openaria
 * @version SVN : $Id$
 */

require_once "../gen/obj/programmation.class.php";

/**
 * Définition de la classe 'programmation' (om_dbform).
 */
class programmation extends programmation_gen {

    // Distances en mètres dans laquelle un établissement est considéré comme proche
    var $rayons = array('50', '100', '200', '300', '500');


    // METHODES FRAMEWORK //

    /**
     * Définition des actions disponibles sur la classe.
     *
     * @return void
     */
    function init_class_actions() {

        // On récupère les actions génériques définies dans la méthode 
        // d'initialisation de la classe parente
        parent::init_class_actions();

        // ACTION - 001 - modifier
        // On supprime la possibilité d'accéder à la modification d'une 
        // programmation car aucun donnée ne peut être modifiée après
        // création.
        $this->class_actions[1] = array();

        // ACTION - 002 - supprimer
        //
        $this->class_actions[2]["condition"] = array(
            "is_from_good_service",
        );

        // ACTION - 005 - programmer
        //
        $this->class_actions[5] = array(
            "identifier" => "programmer",
            "portlet" => array(
                "type" => "action-self",
                "libelle" => __("Programmer les visites"),
                "order" => 100,
                "class" => "programmation-16",
            ),
            "view" => "programmer",
            "permission_suffix" => "planifier",
            "condition" => array("is_from_good_service", "is_planifiable", ),
        );

        // ACTION - 006 - get_first_day
        //
        $this->class_actions[6] = array(
            "identifier" => "get_first_day",
            "view" => "get_first_day",
            "permission_suffix" => "planifier",
        );

        // ACTION - 007 - recuperer_toutes_les_visites
        //
        $this->class_actions[7] = array(
            "identifier" => "recuperer_toutes_les_visites",
            "view" => "recuperer_toutes_les_visites",
            "permission_suffix" => "planifier",
        );

        // ACTION - 008 - recuperer_visites_technicien
        //
        $this->class_actions[8] = array(
            "identifier" => "recuperer_visites_technicien",
            "view" => "recuperer_visites_technicien",
            "permission_suffix" => "planifier",
        );

        // ACTION - 009 - finaliser
        //
        $this->class_actions[9] = array(
            "identifier" => "finaliser",
            "portlet" => array(
                "type" => "action-direct",
                "libelle" => __("Finaliser la programmation"),
                "order" => 200,
                "class" => "agenda-16",
            ),
            "view" => "formulaire",
            "method" => "finaliser",
            "button" => "finaliser",
            "permission_suffix" => "finaliser",
            "condition" => array("is_from_good_service", "is_finalisable", ),
        );

        // ACTION - 010 - reouvrir
        //
        $this->class_actions[10] = array(
            "identifier" => "reouvrir",
            "portlet" => array(
                "type" => "action-direct",
                "libelle" => __("Reouvrir la programmation"),
                "order" => 300,
                "class" => "agenda-16",
            ),
            "view" => "formulaire",
            "method" => "reouvrir",
            "button" => "reouvrir",
            "permission_suffix" => "reouvrir",
            "condition" => array("is_from_good_service", "is_openable", ),
        );

        // ACTION - 015 - nouvelle_version
        //
        $this->class_actions[15] = array(
            "identifier" => "nouvelle_version",
            "portlet" => array(
                "type" => "action-direct",
                "libelle" => __("Créer une nouvelle version"),
                "order" => 300,
                "class" => "agenda-16",
            ),
            "view" => "formulaire",
            "method" => "nouvelle_version",
            "button" => "nouvelle_version",
            "permission_suffix" => "nouvelle_version",
            "condition" => array("is_from_good_service", "is_programmation_validee", ),
        );

        // ACTION - 011 - valider
        //
        $this->class_actions[11] = array(
            "identifier" => "valider",
            "portlet" => array(
                "type" => "action-direct",
                "libelle" => __("Valider la programmation"),
                "order" => 400,
                "class" => "valid-16",
            ),
            "view" => "formulaire",
            "method" => "valider",
            "button" => "valider",
            "permission_suffix" => "valider",
            "condition" => array("is_from_good_service", "is_validable", ),
        );

        // ACTION - 012 - envoyer_part
        //
        $this->class_actions[12] = array(
            "identifier" => "envoyer_part",
            "portlet" => array(
                "type" => "action-direct-with-confirmation",
                "libelle" => __("Envoyer les convocations partenaires"),
                "order" => 501,
                "class" => "send-mail-16",
            ),
            "view" => "formulaire",
            "method" => "envoyer_part",
            "button" => "envoyer_part",
            "permission_suffix" => "envoyer_part",
            "condition" => array(
                "is_from_good_service", 
                "is_programmation_validee", 
            ),
        );

        // ACTION - 016 - view_edition
        //
        $this->class_actions[16] = array(
            "identifier" => "edition-programmation_planning",
            "portlet" => array(
                "type" => "action-blank",
                "libelle" => __("Planning"),
                "order" => 502,
                "class" => "pdf-16",
            ),
            "view" => "view_edition",
            "permission_suffix" => "consulter",
            "condition" => array(
                "is_from_good_service", 
            ),
        );

        // ACTION - 013 - envoyer_convoc_exploit
        //
        $this->class_actions[13] = array(
            "identifier" => "envoyer_convoc_exploit",
            "portlet" => array(
                "type" => "action-direct",
                "libelle" => __("Generer les convocations exploitants"),
                "order" => 500,
                "class" => "pdf-16",
            ),
            "view" => "formulaire",
            "method" => "envoyer_convoc_exploit",
            "button" => "envoyer_convoc_exploit",
            "permission_suffix" => "envoyer_convoc_exploit",
            "condition" => array("is_from_good_service", "is_convoc_send", "is_programmation_validee", ),
        );

        // ACTION - 014 - get_data_datatable
        //
        $this->class_actions[14] = array(
            "identifier" => "get_data_datatable",
            "view" => "get_data_datatable",
            "permission_suffix" => "planifier",
        );

        // ACTION - 017 - get_data_datatable_restricted
        //
        $this->class_actions[17] = array(
            "identifier" => "get_data_datatable_restricted",
            "view" => "get_data_datatable_restricted",
            "permission_suffix" => "planifier",
        );
        // ACTION - 021 - next
        // 
        $this->class_actions[21] = array(
            "identifier" => "next",
            "view" => "view_next",
            "permission_suffix" => "consulter",
            "condition" => array(
                "exists",
                "is_from_good_service",
                "is_next_week_exists",
            ),
            "portlet" => array(
                "type" => "action-self",
                "libelle" => __("semaine suivante"),
                "order" => 730,
                "class" => "next-16",
            ),
        );
        // ACTION - 022 - prev
        // 
        $this->class_actions[22] = array(
            "identifier" => "prev",
            "view" => "view_prev",
            "permission_suffix" => "consulter",
            "condition" => array(
                "exists",
                "is_from_good_service",
                "is_prev_week_exists",
            ),
            "portlet" => array(
                "type" => "action-self",
                "libelle" => __("semaine précédente"),
                "order" => 720,
                "class" => "prev-16",
            ),
        );
    }

    /**
     * 
     */
    function get_week_id($type) {
        $available_types = array("next", "prev", );
        if (in_array($type, $available_types) !== true) {
            return null;
        }
        $varname = sprintf("_%s_week_id", $type);
        $methodname = sprintf("define_%s_week", $type);
        if (isset($this->$varname) === true) {
            return $this->$varname;
        }
        $week = $this->f->$methodname($this->getVal("annee"), $this->getVal("numero_semaine"));
        $query = sprintf(
            'SELECT programmation.programmation FROM %1$sprogrammation WHERE annee=%2$s AND numero_semaine=%3$s AND service=%4$s',
            DB_PREFIXE,
            intval($week["annee"]),
            intval($week["semaine"]),
            intval($this->getVal("service"))
        );
        $week_id = $this->f->db->getone($query);
        $this->addToLog(__METHOD__."(): db->getone(\"".$query."\");", VERBOSE_MODE);
        $this->f->isDatabaseError($week_id);
        $this->$varname = $week_id;
        return $this->$varname;
    }

    /**
     * CONDITION - is_next_week_exists.
     *
     * @return boolean
     */
    function is_next_week_exists() {
        $next_week_id = $this->get_week_id("next");
        if ($next_week_id === null) {
            return false;
        }
        return true;
    }

    /**
     * CONDITION - is_prev_week_exists.
     *
     * @return boolean
     */
    function is_prev_week_exists() {
        $prev_week_id = $this->get_week_id("prev");
        if ($prev_week_id === null) {
            return false;
        }
        return true;
    }

    /**
     * VIEW - view_prev.
     *
     * @return void
     */
    function view_prev() {
        $this->checkAccessibility();
        $override = array(
            "idx" => $this->get_week_id("prev"),
            "maj" => 3,
            "validation" => 0,
        );
        if ($this->getParameter("retourformulaire") != "") {
            $url = $this->compose_form_url("sousform", $override);
        } else {
            $url = $this->compose_form_url("form", $override);
        }
        $url = str_replace("&amp;", "&", $url);
        header("location:".$url);
        die();
    }

    /**
     * VIEW - view_next.
     *
     * @return void
     */
    function view_next() {
        $this->checkAccessibility();
        $override = array(
            "idx" => $this->get_week_id("next"),
            "maj" => 3,
            "validation" => 0,
        );
        if ($this->getParameter("retourformulaire") != "") {
            $url = $this->compose_form_url("sousform", $override);
        } else {
            $url = $this->compose_form_url("form", $override);
        }
        $url = str_replace("&amp;", "&", $url);
        header("location:".$url);
        die();
    }

    /**
     *
     * @return array
     */
    function get_var_sql_forminc__champs() {
        return array(
            "programmation",
            "semaine_annee",
            "annee",
            "numero_semaine",
            "service",
            "version",
            "date_modification",
            "programmation_etat",
            "convocation_exploitants",
            "convocation_membres",
            "date_envoi_ce",    
            "date_envoi_cm",
            "planifier_nouveau",
        );
    }

    function get_libelle() {
        //
        $ent = __("Semaine n°").$this->getVal("numero_semaine")." du ";
        $ent .= $this->get_date_firstday("d/m/Y");
        $ent .= " au ";
        $ent .= $this->get_date_lastday("d/m/Y");
        $ent .= " V".$this->getVal("version");
        return $ent;
    }

    /**
     * Retourne la date du premier jour de la semaine de programmation
     */
    function get_date_firstday($format = "d/m/Y") {
        //
        $annee = trim($this->getVal("annee"));
        $semaine = trim($this->getVal("numero_semaine"));
        $firstday = new DateTime();
        $firstday->setISODate($annee,$semaine,1);
        $firstday = $firstday->format($format);
        return $firstday;
    }

    /**
     * Retourne la date du dernier jour de la semaine de programmation
     */
    function get_date_lastday($format = "d/m/Y") {
        //
        $annee = trim($this->getVal("annee"));
        $semaine = trim($this->getVal("numero_semaine"));
        $lastday = new DateTime();
        $lastday->setISODate($annee,$semaine,7);
        $lastday = $lastday->format($format);
        return $lastday;
    }

    /**
     *
     */
    function setType(&$form,$maj) {
        parent::setType($form,$maj);

        // En mode "ajouter"
        if ($maj == 0) {
            $form->setType('programmation_etat', 'selecthidden');
            $form->setType('version', 'hidden');
            $form->setType('date_modification', 'hiddendate');
            $form->setType('convocation_exploitants', 'hidden');
            $form->setType('date_envoi_ce', 'hiddendate');
            $form->setType('convocation_membres', 'hidden');
            $form->setType('date_envoi_cm', 'hiddendate');
            $form->setType('semaine_annee', 'hidden');
            // On cache le marqueur
            $form->setType('planifier_nouveau', "hidden");
        }
        
        // En mode "modifier"
        if ($maj == 1) {
            $form->setType('convocation_exploitants', 'select');
            $form->setType('convocation_membres', 'select');
            $form->setType('semaine_annee', 'hiddenstatic');
            $form->setType('version', 'hidden');
            $form->setType('date_modification', 'hiddendate');
            $form->setType('annee', 'hidden');
            $form->setType('numero_semaine', 'hidden');
            // On cache le marqueur
            $form->setType('planifier_nouveau', "hidden");
        }

        // En modes "supprimer" et "consulter"
        if ($maj == 2 || $maj == 3) {
            $form->setType('programmation', 'hidden');
            $form->setType('semaine_annee', 'hidden');
            $form->setType('annee', 'hidden');
            $form->setType('numero_semaine', 'hidden');
            // On cache le marqueur
            $form->setType('planifier_nouveau', "hidden");
        }
        // Pour les actions supplémentaires qui utilisent la vue formulaire
        // il est nécessaire de cacher les champs ou plutôt de leur affecter un
        // type pour que l'affichage se fasse correctement
        if ($maj != 0 && $maj != 1 && $maj != 2 && $maj != 3) {
            //
            foreach ($this->champs as $champ) {
                $form->setType($champ, "hidden");
            }
        }
    }

    /**
     *
     */
    function get_list_of_concerned_di() {
        //
        $sql = "
        SELECT
            visite.dossier_instruction
        FROM 
            ".DB_PREFIXE."visite
            LEFT JOIN ".DB_PREFIXE."visite_etat
                ON visite.visite_etat = visite_etat.visite_etat
        WHERE
            visite.programmation=".$this->getVal($this->clePrimaire)."
            AND visite_etat.code != 'ANN'
        GROUP BY
            visite.dossier_instruction
        ";
        //
        $res = $this->f->db->query($sql);
        $this->addToLog(
            __METHOD__."(): db->query(\"".$sql."\");",
            VERBOSE_MODE
        );
        $this->f->isDatabaseError($res);
        //
        $di = array();
        while ($row=& $res->fetchRow(DB_FETCHMODE_ASSOC)) {
            $di[] =$row['dossier_instruction'];
        }
        return $di;
    }

    /**
     *
     */
    function setVal(&$form, $maj, $validation, &$dnu1 = null, $dnu2 = null) {
        //
        parent::setVal($form, $maj, $validation);
        // En mode "ajouter"
        if ($maj == 0) {
            // Etat de programmation à "En préparation" par défaut
            $id_etat_programmation = $this->get_programmation_etat_id_by_code('PRE');
            $form->setVal('programmation_etat', $id_etat_programmation);
            // Version à "1" par défaut
            $form->setVal('version', '1');
            // Date de modification à date du jour par défaut
            $form->setVal('date_modification', date('d/m/Y'));
            // Année et semaine à la suivante par défaut
                $annee_max = $this->get_annee_max();
                $semaine_max = $this->get_semaine_max($annee_max);
            $prochaine_semaine = $this->f->define_next_week($annee_max, $semaine_max);
            if ($prochaine_semaine != false) {
                $form->setVal('annee', $prochaine_semaine['annee']);
                $form->setVal('numero_semaine', $prochaine_semaine['semaine']);
            }
        }
        
        // En modes "consulter" et "supprimer"
        if ($maj == 2 || $maj == 3) {

            // convocation exploitants
            switch ($form->val['convocation_exploitants']) {

                case "non_effectuees":
                $form->setVal('convocation_exploitants', __('non_effectuees'));
                break;

                case "a_envoyer":
                $form->setVal('convocation_exploitants', __('a_envoyer'));
                break;

                case "envoyees":
                $form->setVal('convocation_exploitants', __('envoyees'));
                break;

                case "a_completer":
                $form->setVal('convocation_exploitants', __('a_completer'));
                break;
            }
            // convocation membres
            switch ($form->val['convocation_membres']) {

                case "non_envoyees":
                $form->setVal('convocation_membres', __('non_envoyees'));
                break;

                case "a_envoyer":
                $form->setVal('convocation_membres', __('a_envoyer'));
                break;

                case "envoyees":
                $form->setVal('convocation_membres', __('envoyees'));
                break;
                    
                        case "a_renvoyer":
                $form->setVal('convocation_membres', __('a_renvoyer'));
                break;
            }
        }
    }

    /**
     * Méthode qui effectue les requêtes de configuration des champs.
     *
     * @param object  $form Instance du formulaire.
     * @param integer $maj  Mode du formulaire.
     * @param null    $dnu1 @deprecated Ancienne ressource de base de données.
     * @param null    $dnu2 @deprecated Ancien marqueur de débogage.
     *
     * @return void
     */
    function setSelect(&$form, $maj, &$dnu1 = null, $dnu2 = null) {

        //
        parent::setSelect($form, $maj, $dnu1, $dnu2);

        // En mode "modifier"
        if ($maj == 1) {
            
            /* Convocation exploitants */
            $contenu = array();
            $contenu[0][0] = "";
            $contenu[1][0] = __('choisir')." ".__('convocation_exploitants');
            $contenu[0][1] = "non_effectuees";
            $contenu[1][1] = __('non_effectuees');
            $contenu[0][2] = "a_envoyer";
            $contenu[1][2] = __('a_envoyer');
            $contenu[0][3] = "envoyees";
            $contenu[1][3] = __('envoyees');
            $contenu[0][4] = "a_completer";
            $contenu[1][4] = __('a_completer');
            $form->setSelect("convocation_exploitants", $contenu);
            /* Convocation membres */
            $contenu = array();
            $contenu[0][0] = "";
            $contenu[1][0] = __('choisir')." ".__('convocation_membres');
            $contenu[0][1] = "non_envoyees";
            $contenu[1][1] = __('non_envoyees');
            $contenu[0][2] = "a_envoyer";
            $contenu[1][2] = __('a_envoyer');
            $contenu[0][3] = "envoyees";
            $contenu[1][3] = __('envoyees');
            $form->setSelect("convocation_membres", $contenu);
        }
    }

    /**
     * En mode "ajouter" permet de valoriser des champs avant la vérification.
     *
     * @param array   $val   Liste des valeurs
     *
     * @return boolean
     */
    function setValFAjout($val = array()) {
        parent::setValFAjout($val);

        // On formate la semaine d'après les valeurs postées
        $this->valF['semaine_annee'] = $this->formater_semaine($val['numero_semaine'], $val['annee']);
    }

    /**
     * Permet de définir l’attribut “onchange” sur chaque champ.
     * 
     * @param object $form Formumaire
     * @param int    $maj  Mode d'insertion
     */
    function setOnchange(&$form,$maj) {
        parent::setOnchange($form,$maj);

        $form->setOnchange('annee',
            'verifier_annee(this);');
        $form->setOnchange('numero_semaine',
            'verifier_semaine(this);str_pad_js(this,2);');
    }

    /**
     *
     */
    function set_form_specificity(&$form, $maj) {
        //
        parent::set_form_specificity($form, $this->getParameter("maj"));

        /**
         * Gestion du champ service
         */
        // 
        $this->set_form_specificity_service_common($form, $this->getParameter("maj"));
    }

    /**
     * Permet de rajouter un contenu spécifique
     * après le bouton retour.
     */
    function afterFormSpecificContent() {
        //
        if ($this->getParameter("maj") == 3) {
            // On ouvre le conteneur agenda
            echo '<div id="bloc_agenda">';
            //$this->widget_technicien();
            // On ouvre le bloc calendrier
            echo '<div class="agenda_calendrier" id="agenda_calendrier_'.$this->getVal($this->clePrimaire).'">';
            $this->f->addHTMLHeadJs("../app/lib/moments/moments.js");
            $this->f->addHTMLHeadJs("../app/lib/fullcalendar/fullcalendar.js");
            $this->f->addHTMLHeadJs("../app/lib/fullcalendar/lang/fr.js");
            $this->f->layout->layout->display_script_css_call("../app/lib/fullcalendar/fullcalendar.css");
            // On ferme le bloc calendrier
            echo '</div>';   
            // On ferme le conteneur agenda
            echo '</div>';
        }
    }

    /**
     * Permet de modifier le fil d'Ariane.
     * 
     * @param string $ent Fil d'Ariane
     * @param array  $val Valeurs de l'objet
     * @param intger $maj Mode du formulaire
     */
    function getFormTitle($ent) {
        
        $ent = __("suivi")." -> ".__("programmations")." -> ".__("gestion");

        // Si différent de l'ajout
        // et si les champs année et numéro de semaine existent
        if ($this->getParameter("maj") != 0) {

            $ent .= " -> ";
            $ent .= $this->get_libelle();

        }
        // Change le fil d'Ariane
        return $ent;
    }

    /**
     *
     */
    function verifierAjout($val = array(), &$dnu1 = null) {
        //
        parent::verifierAjout($val);
        // Vérification du numéro de la semaine pour une année donnée
        $annee = $this->valF['annee'];
        $semaine = $this->valF['numero_semaine'];
        // Si les champs sont renseignés on vérifie la correspondance
        if ($annee != '' && $semaine != '') {
            $lundi = new DateTime();
            $lundi->setISODate($annee,$semaine,1);
            $annee_suivante = $annee + 1;
            // Si le lundi du couple année/semaine fourni appartient
            // à l'année d'après alors il s'agit d'une nouvelle semaine
            if ($lundi->format('Y') == $annee_suivante) {
                $this->correct = false;
                $message = __("L'annee ");
                $message .= $annee;
                $message .= __(" ne comporte pas de semaine ");
                $message .= $semaine;
                $message .= ".";
                $this->addToMessage($message);
            }
            if ($semaine < 1 || $semaine > 53) {
                $this->correct = false;
                $message = __("Le numero de la semaine est invalide.");
                $this->addToMessage($message);
            }
            if ($annee < 1900 || $annee > 2100) {
                $this->correct = false;
                $message = __("L'annee est invalide.");
                $this->addToMessage($message);
            }
        }
    }

    // FIN METHODES FRAMEWORK
    // METHODES OBJET

    /**
     * Récupère l'année et la semaine d'une programmation donnée
     * 
     * @param  [integer] $id Identifiant de la programmation
     * @return [array]       Tableau associatif
     */
    function get_week_and_year($id) {

        $sql = "SELECT
            annee,
            numero_semaine
        FROM ".DB_PREFIXE."programmation
        WHERE programmation = ".$id;
        //
        $res = $this->f->db->query($sql);
        $this->addToLog(
            __METHOD__."(): db->query(\"".$sql."\");",
            VERBOSE_MODE
        );
        $this->f->isDatabaseError($res);
        //
        $row=& $res->fetchRow(DB_FETCHMODE_ASSOC);
        return array(
            "annee" => $row['annee'],
            "semaine" => $row['numero_semaine']
        );
    }

    /**
     * Récupère l'année maximum des programmations
     * 
     * @return string année maximum
     */
    function get_annee_max() {

        $sql = "SELECT max(annee)
            FROM ".DB_PREFIXE."programmation";
        // XXX WHERE service = ?
        $annee = $this->f->db->getOne($sql);
        $this->addToLog(
            __METHOD__."(): db->getone(\"".$sql."\");",
            VERBOSE_MODE
        );
        if ($this->f->isDatabaseError($annee, true)) {
            $this->erreur_db($annee->getDebugInfo(), $annee->getMessage(), '');
            $this->correct = false;
            return false;
        }
        //
        if (!empty($annee)) {
            return $annee;
        } else {
            return "";
        }
    }

    /**
     * Récupère la semaine maximum des programmations pour une année donnée
     *
     * @param  string $annee  année de la programmation
     * @return string         semaine maximum
     */
    function get_semaine_max($annee) {

        if (empty($annee)) {
            return '';
        }
        $sql = "SELECT max(numero_semaine)
            FROM ".DB_PREFIXE."programmation
            WHERE annee='".$annee."'";
            // XXX and service = ?
        $semaine = $this->f->db->getOne($sql);
        $this->addToLog(
            __METHOD__."(): db->getone(\"".$sql."\");",
            VERBOSE_MODE
        );
        if ($this->f->isDatabaseError($semaine, true)) {
            $this->erreur_db($semaine->getDebugInfo(), $semaine->getMessage(), '');
            $this->correct = false;
            return false;
        }
        //
        if (!empty($semaine)) {
            return $semaine;
        } else {
            return "";
        }
    }

    

    /**
     * Formate la semaine en 'SS/AAAA'
     * 
     * @param  string $semaine numéro de semaine
     * @param  string $annee   numéro d'année
     * 
     * @return string          semaine formatée
     */
    function formater_semaine($semaine, $annee) {

        // Formatage de la semaine sur deux chiffres
        $semaine_formatee = str_pad($semaine, 2, "0", STR_PAD_LEFT);;
        // Concaténation avec l'année
        $semaine_annee = $annee."/".$semaine_formatee;
        return $semaine_annee;
    }



    /**
     * Permet de rendre l'action de génération des convocations accessible
     *
     * @return boolean true si disponible false sinon
     */
    function is_convoc_send() {
        if($this->getVal("convocation_exploitants") == 'envoyees') {
            return false;
        }
        return true;
    }

    /**
     * TREATMENT - envoyer_convoc_exploit.
     *
     * Ce traitement “Générer les convocations exploitant” permet de générer
     * pour chaque visite dont le statut d’envoi de convocation est “à envoyer”
     * un document généré à chaque contact de l’établissement marqué comme
     * destinataire des courriers (que ce soit pour les courriers de
     * convocation ou pour les courriers d’annulation).
     *
     * @param array $val Valeurs soumises par le formulaire
     *
     * @return boolean
     */
    function envoyer_convoc_exploit($val = array()) {
        $this->begin_treatment(__METHOD__);

        /**
         * Récupération des visites dont les convocations exploitants sont à
         * envoyer.
         */
        $sql = "
        SELECT 
            visite 
        FROM 
            ".DB_PREFIXE."visite 
        WHERE 
            programmation = ".$this->getVal($this->clePrimaire)." 
            AND (convocation_exploitants != 'envoyees' 
                 OR convocation_exploitants IS NULL)
        ";
        $res = $this->f->db->query($sql);
        $this->addToLog(
            __METHOD__."(): db->query(\"".$sql."\");",
            VERBOSE_MODE
        );
        if ($this->f->isDatabaseError($res, true)) {
            $this->erreur_db($res->getDebugInfo(), $res->getMessage(), '');
            $this->correct = false;
            return $this->end_treatment(__METHOD__, false);
        }

        /**
         * On instancie l'utilitaire de gestion des courriers
         */
        $courrier = $this->f->get_inst__om_dbform(array(
            "obj" => "courrier",
            "idx" => 0,
        ));

        /**
         * On boucle sur chaque visite pour appeler la méthode de création de
         * courriers pour chaque contact.
         */
        $list_all_courrier_child = array();
        while ($row = $res->fetchrow(DB_FETCHMODE_ASSOC)) {
            // Instanciation de chaque visite
            $visite = $this->f->get_inst__om_dbform(array(
                "obj" => "visite",
                "idx" => $row["visite"],
            ));
            //
            if ($visite->add_mail_for_contact() === false) {
                $this->addToLog(
                    __METHOD__."()(id=".$this->getVal($this->clePrimaire)."): Erreur lors de l'ajout du document généré sur une visite",
                    DEBUG_MODE
                );
                $this->correct = false;
                $this->cleanMessage();
                $this->addToMessage(__("Erreur lors de la génération des convocation exploitants."));
                $this->addToMessage("-> ".$visite->msg);
                return $this->end_treatment(__METHOD__, false);
            }
            //
            $list_courrier_child = $courrier->get_list_courrier_child($visite->valF['courrier_convocation_exploitants']);
            //
            $list_all_courrier_child = array_merge($list_all_courrier_child, $list_courrier_child);
        }

        /**
         * Modification du statut pour les convocations exploitants sur la programmation.
         */
        $ret = $this->update_autoexecute(
            array(
                "convocation_exploitants" => "envoyees",
                "date_envoi_ce" => date("Y-m-d"),
            ),
            null,
            false
        );
        if ($ret !== true) {
            $this->addToLog(
                __METHOD__."()(id=".$this->getVal($this->clePrimaire)."): Erreur lors de la mise à jour de l'enregistrement en base de données",
                DEBUG_MODE
            );
            $this->correct = false;
            $this->cleanMessage();
            $this->addToMessage(__("Erreur lors de la mise à jour du statut de l'envoi des convocations exploitant de la programmation."));
            return $this->end_treatment(__METHOD__, false);
        }

        //
        if (count($list_all_courrier_child) == 0) {
            $this->addToMessage(__("Aucune nouvelle convocation exploitant à envoyer."));
            $this->addToMessage(__("Le statut des convocations exploitant a été correctement mis à jour."));
            return $this->end_treatment(__METHOD__, true);
        }

        /**
         * On génère un fichier concaténant toutes les convocations exploitant
         * pour en permettre le téléchargement rapide du document pdf. Il n'est
         * pas stocké de manière pérenne.
         */
        $tmpfile_uid = $courrier->generate_all_courriers_pdf(
            $list_all_courrier_child,
            "temporary"
        );
        if ($tmpfile_uid === false) {
            $this->addToLog(
                __METHOD__."()(id=".$this->getVal($this->clePrimaire)."): Erreur lors de la génération du fichier pdf concaténé à télécharger",
                DEBUG_MODE
            );
            $this->correct = false;
            $this->cleanMessage();
            $this->addToMessage(__("Erreur lors de la génération du document pdf téléchargeable des convocations exploitant."));
            return $this->end_treatment(__METHOD__, false);
        }

        /**
         * Tout s'est bien passé. On affiche un message de succès ainsi qu'un
         * lien vers le téléchargement du document pdf concaténant toutes les
         * convocations exploitant générérées.
         */
        $this->addToMessage(__("Toutes les convocations exploitant ont été générées."));
        $this->addToMessage(
            sprintf(
                '
                %s
                <a class="om-prev-icon pdf-16" target="_blank" href="%s&snippet=file&mode=temporary&amp;uid=%s">
                    %s
                </a>
                ',
                __("Lettre de convocation des exploitants : "),
                OM_ROUTE_FORM,
                $tmpfile_uid,
                __("Convocation")
            )
        );
        $this->addToMessage(__("Le statut des convocations exploitant a été correctement mis à jour."));
        //
        return $this->end_treatment(__METHOD__, true);
    }

    /**
     * TREATMENT - envoyer_part.
     * 
     * Permet d'envoyer par mail le planning de la programmation aux 
     * partenaires.
     *
     * @param array $val Valeurs soumises par le formulaire
     *
     * @return boolean
     */
    function envoyer_part($val = array()) {
        // Begin
        $this->begin_treatment(__METHOD__);

        // Maj de la base, rollback possible si bug envoi mail
        // Récuperation de la valeur de la cle primaire de l'objet
        $id = $this->getVal($this->clePrimaire);
        $valF = array(
            "convocation_membres" => "envoyees",
            "date_envoi_cm" => date("Y-m-d"),
        );

        // Execution de la requête de modification des donnees de l'attribut
        // valF de l'objet dans l'attribut table de l'objet
        $res = $this->f->db->autoExecute(
            DB_PREFIXE.$this->table,
            $valF,
            DB_AUTOQUERY_UPDATE,
            $this->getCle($id)
        );
        // Logger
        $this->addToLog(
            __METHOD__."(): db->autoExecute(\"".DB_PREFIXE.$this->table."\", ".print_r($valF, true).", DB_AUTOQUERY_UPDATE, \"".$this->getCle($id)."\")",
            VERBOSE_MODE
        );
        // Si une erreur survient
        if (database::isError($res, true)) {
            // Appel de la methode de recuperation des erreurs
            $this->erreur_db($res->getDebugInfo(), $res->getMessage(), '');
            // Return
            return $this->end_treatment(__METHOD__, false);
        }
        // Génération de la convocation
        $pdfedition = $this->compute_pdf_output(
            "lettretype",
            "programmation_planning"
        );

        // On teste que le fichier a correctement été générée
        if($pdfedition["pdf_output"] == '') {
            //
            $this->addToMessage(__("La generation du fichier de convocation des partenaires a echouee.")." ".__("Veuillez contacter votre administrateur."));
            // Return
            return $this->end_treatment(__METHOD__, false);
        }

        // Liste des mails
        $mail_list = array();

        // Instancie la classe contact_institutionnel
        $contact_institutionnel = $this->f->get_inst__om_dbform(array(
            "obj" => "contact_institutionnel",
            "idx" => 0,
        ));

        // Récupère la liste des contacts institutionnels
        $list_contacts_institutionnels = $contact_institutionnel->get_contacts_institutionnels($this->getVal("service"), true);
        //
        foreach ($list_contacts_institutionnels as $key => $value) {
            //
            $mail_list = array_merge(
                $mail_list,
                array_filter(explode(";", $this->clean_emails_list($value["courriel"])))
            );
        }

        // Si les destinaitaires ont des adresses mails
        if (empty($mail_list)) {
            //
            $this->addToMessage(__("Aucun des partenaires a une adresse mail parametree."));
            // Return
            return $this->end_treatment(__METHOD__, false);
        }

        //
        $mail_list = implode(",", $mail_list);

        // Préparation du mail
        $title = sprintf(
            __("Convocations pour visites de la semaine %s"),
            $this->getVal('numero_semaine')
        );
        $corps = 
            __("Vous etes convoqués aux visites dont vous trouverez le planning de la programmation dans la piece jointe à ce mail.");

        // Pièce jointe
        $pj['content'] = $pdfedition["pdf_output"];
        $pj['title'] = "convocation_".$this->getVal('numero_semaine').
            $this->getVal('annee')."_".date('YmdHis').".pdf";
        $pj['stream'] = true;
        // Envoi du mail avec message de retour
        $ret_mail = $this->f->sendMail(
            $title,
            $corps,
            $mail_list,
            array($pj)
        );

        //
        if($ret_mail === false) {
            $this->addToMessage(__("L'envoi des convocations aux partenaires par mail a echoue.")." ".__("Veuillez contacter votre administrateur."));
            // Return
            return $this->end_treatment(__METHOD__, false);
        }

        //
        $this->addToMessage(__("La convocations des partenaires a ete generee et envoyee par mail."));
        // Return
        return $this->end_treatment(__METHOD__, true);
    }

    /**
     * TREATMENT - finaliser.
     * 
     * Permet de finaliser une programmation.
     *
     * @param array $val Valeurs soumises par le formulaire
     *
     * @return boolean
     */
    function finaliser($val = array()) {
        // Begin
        $this->begin_treatment(__METHOD__);

        //
        $ret = $this->manage_programmation_state("finalise", $val);
        // Si le traitement ne s'est pas déroulé correctement
        if ($ret !== true) {
            // Return
            return $this->end_treatment(__METHOD__, false);
        }

        // Return
        return $this->end_treatment(__METHOD__, true);
    }

    /**
     * TREATMENT - reouvrir.
     * 
     * Permet de réouvrir une programmation.
     *
     * @param array $val Valeurs soumises par le formulaire
     *
     * @return boolean
     */
    function reouvrir($val = array()) {
        // Begin
        $this->begin_treatment(__METHOD__);

        //
        $ret = $this->manage_programmation_state("unfinalise", $val);
        // Si le traitement ne s'est pas déroulé correctement
        if ($ret !== true) {
            // Return
            return $this->end_treatment(__METHOD__, false);
        }

        // Return
        return $this->end_treatment(__METHOD__, true);
    }

    /**
     * TREATMENT - valider.
     * 
     * Permet de valider une programmation
     *
     * @param array $val Valeurs soumises par le formulaire
     *
     * @return boolean
     */
    function valider($val = array()) {
        // Begin
        $this->begin_treatment(__METHOD__);

        //
        $ret = $this->manage_programmation_state("validate", $val);
        // Si le traitement ne s'est pas déroulé correctement
        if ($ret !== true) {
            // Return
            return $this->end_treatment(__METHOD__, false);
        }

        // Return
        return $this->end_treatment(__METHOD__, true);
    }
    
    /**
     * TREATMENT - nouvelle_version.
     * 
     * Permet de créer une nouvelle version de la programmation
     *
     * @param array $val Valeurs soumises par le formulaire
     *
     * @return boolean
     */
    function nouvelle_version($val = array()) {
        // Begin
        $this->begin_treatment(__METHOD__);

        //
        $ret = $this->manage_programmation_state("create", $val);
        // Si le traitement ne s'est pas déroulé correctement
        if ($ret !== true) {
            // Return
            return $this->end_treatment(__METHOD__, false);
        }

        // Return
        return $this->end_treatment(__METHOD__, true);
    }

    /**
     * Gestion de la finalisation et réouverture d'une programmation
     *
     * @param string $mode action
     * @param array  $val  valeurs soumises par le formulaire
     *
     * @return boolean true si valide false sinon
     */
    function manage_programmation_state($mode = null, $val = array()) {
        // Logger
        $this->addToLog(__METHOD__."() - begin", EXTRA_VERBOSE_MODE);
        // Si le mode n'existe pas on retourne false
        if ($mode != "finalise" && $mode != "unfinalise" && $mode != "validate" 
            && $mode != "create") {
            return false;
        }
        // Récuperation de la valeur de la cle primaire de l'objet
        $id = $this->getVal($this->clePrimaire);
        // 
        if ($mode == "finalise") {
            // Id de l'état de la programmation
            $id_etat = $this->get_programmation_etat_id_by_code('fin');
            // Valeurs à modifier
            $valF = array(
                "programmation_etat" => $id_etat,
            );
            //
            $valid_message = __("Finalisation correctement effectue.");
        } elseif ($mode == "unfinalise") {
            // Id de l'état de la programmation
            $id_etat = $this->get_programmation_etat_id_by_code('pre');
            // Valeurs à modifier
            $valF = array(
                "programmation_etat" => $id_etat,
            );
            //
            $valid_message = __("Reouverture correctement effectue.");
        } elseif ($mode == "validate") {
            // Id de l'état de la programmation
            $id_etat = $this->get_programmation_etat_id_by_code('val');
            // Valeurs à modifier
            $valF = array(
                "programmation_etat" => $id_etat,
                "convocation_exploitants" => "a_envoyer",
                "convocation_membres" => "a_envoyer",
            );
            //
            $valid_message = __("Validation correctement effectue.");
        } elseif ($mode == "create") {
            // Id de l'état de la programmation
            $id_etat = $this->get_programmation_etat_id_by_code('pre');
            $convocation_exploitants = ($this->getVal("convocation_exploitants")=="envoyees")?
                    "a_completer":"";
            $convocation_membres = ($this->getVal("convocation_membres")=="envoyees")?
                    "a_renvoyer":"";
            $version = $this->getVal("version");
            // Valeurs à modifier
            $valF = array(
                "programmation_etat" => $id_etat,
                "convocation_exploitants" => $convocation_exploitants,
                "convocation_membres" => $convocation_membres,
                "version" => ++$version
            );
            //
            $valid_message = __("Une nouvelle version de la programmation a été créée avec succès.");
        }
        // On met à jour la date de modification à chaque changement d'état de la programmation
        $valF["date_modification"] = date("Y-m-d");
        //
        $this->correct = true;
        // Execution de la requête de modification des donnees de l'attribut
        // valF de l'objet dans l'attribut table de l'objet
        $res = $this->f->db->autoExecute(
            DB_PREFIXE.$this->table,
            $valF,
            DB_AUTOQUERY_UPDATE,
            $this->getCle($id)
        );
        // Si une erreur survient
        if (database::isError($res, true)) {
            // Appel de la methode de recuperation des erreurs
            $this->erreur_db($res->getDebugInfo(), $res->getMessage(), '');
            $this->correct = false;
            return false;
        } else {
            //
            $main_res_affected_rows = $this->f->db->affectedRows();
            // Log
            $this->addToLog(
                __METHOD__."(): ".__("Requete executee"),
                VERBOSE_MODE
            );
            // Log
            $message = __("Enregistrement")."&nbsp;".$id."&nbsp;";
            $message .= __("de la table")."&nbsp;\"".$this->table."\"&nbsp;";
            $message .= "[&nbsp;".$main_res_affected_rows."&nbsp;";
            $message .= __("enregistrement(s) mis a jour")."&nbsp;]";
            $this->addToLog(__METHOD__."(): ".$message, VERBOSE_MODE);
            // Message de validation
            if ($main_res_affected_rows == 0) {
                $this->addToMessage(__("Attention vous n'avez fait aucune modification.")."<br/>");
            } else {
                $this->addToMessage($valid_message."<br/>");
            }
        }
        // Logger
        $this->addToLog(__METHOD__."() - end", EXTRA_VERBOSE_MODE);
        //
        return true;
    }

    /**
     * Permet de retourner le code de l'état de la programmation
     *
     * @param integer $id identifiant de l'état
     *
     * @return mixed Code de l'état si il existe sinon false
     */
    function get_programmation_etat_code_by_id($id) {
        if (!is_numeric($id) OR $id == ''){
            return false;
        }
        //Requête de récupération du code de la programmation
        $sql = "SELECT lower(code)
                FROM ".DB_PREFIXE."programmation_etat
                WHERE programmation_etat=".$id;
        //
        $code_programmation_etat = $this->f->db->getOne($sql);
        $this->addToLog(
            __METHOD__."(): db->getone(\"".$sql."\");",
            VERBOSE_MODE
        );
        $this->f->isDatabaseError($code_programmation_etat);
        if(empty($code_programmation_etat)) {
            return false;
        }
        return $code_programmation_etat;
    }

    /**
     * Permet de retourner l'identifiant de l'état de la programmation
     *
     * @param integer $code code de l'état
     *
     * @return mixed Identifiant de l'état si il existe sinon false
     */
    function get_programmation_etat_id_by_code($code) {
        if ($code == ''){
            return false;
        }
        //Requête de récupération du code de la programmation
        $sql = "SELECT programmation_etat
                FROM ".DB_PREFIXE."programmation_etat
                WHERE LOWER(code) = '".strtolower($code)."'";
        //
        $id_programmation_etat = $this->f->db->getOne($sql);
        $this->addToLog(
            __METHOD__."(): db->getone(\"".$sql."\");",
            VERBOSE_MODE
        );
        $this->f->isDatabaseError($id_programmation_etat);
        if(empty($id_programmation_etat)) {
            return false;
        }
        return $id_programmation_etat;
    }


    /**
     * Retourne la couleur ayant le plus de contraste
     * avec celle donnée en paramètre (méthode YIQ)
     * 
     * @param  [string] $color Code hexadécimal de la couleur à tester
     * @return [string]        blanc ou noir en hexadécimal
     */
    function get_contrast($color){

        $r = hexdec(substr($color,0,2));
        $g = hexdec(substr($color,2,2));
        $b = hexdec(substr($color,4,2));
        $yiq = (($r*299)+($g*587)+($b*114))/1000;
        return ($yiq >= 128) ? '000000' : 'FFFFFF';
    }

    // FIN METHODES OBJET    
    // ACTIONS SPECIFIQUES

    /**
     * VIEW - view_edition.
     *
     * @return void
     */
    function view_edition() {
        //
        $this->checkAccessibility();
        //
        $annee = trim($this->getVal("annee"));
        $semaine = trim($this->getVal("numero_semaine"));
        $version = trim($this->getVal("version"));
        //
        if ($this->get_action_key_for_identifier("edition-programmation_planning") == $this->getParameter("maj")) {
            //
            $pdfedition = $this->compute_pdf_output(
                "lettretype", 
                "programmation_planning",
                null,
                null,
                array(
                    "watermark" => (strtolower($this->get_programmation_etat_code_by_id($this->getVal("programmation_etat"))) == 'val' ? false : true),
                )
            );
            //
            $this->expose_pdf_output(
                $pdfedition["pdf_output"], 
                "programmation_".$annee."_".$semaine."_v".$version."_".date('YmdHis').".pdf"
            );
        } else {
            die();
        }
    }

    /**
     * VIEW - programmer.
     * 
     * Interface de programmation
     * 
     * @return void
     */
    function programmer() {
        //
        $this->checkAccessibility();
        //
        printf("<div>");
        $this->retour();
        printf("</div>");
        
        $this->bloc_entete();
        $this->bloc_propositions();
        $this->bloc_agenda($this->getVal($this->clePrimaire));
        
        printf("<div class=\"formControls formControls-bottom\">");
        $this->retour();
        printf("</div>");
    }

    /**
     * Affichage de l'entete
     */
    function bloc_entete(){
        echo "<div id=\"msg_view_programmation\"></div>";
        echo "<div id=\"bloc_entete\">";
        $libelle = __("Semaine ").$this->getVal("semaine_annee");
        echo $libelle;
        echo "</div>";
    }
    
    /**
     * Affichage du bloc de propositions
     */
    function bloc_propositions(){

        printf("<div id=\"bloc_propositions\" class=\"col_12\">");
        $this->f->addHTMLHeadJs("../app/lib/datatables/js/jquery.dataTables.min.js");
        printf("<div id=\"bloc_boutons\" class=\"col_12\">");
        // L'option SIG doit être activée pour pouvoir géolocaliser les établissements proches
        if ($this->is_option_sig_enabled() === true) {
            $this->bloc_geolocalisation();
        }
        $this->bloc_filtres();
        printf("</div>");
        printf("<div id=\"bloc_tableau\" class=\"col_12\">");
        // Identifiant de la semaine de programmation caché, nécessaire pour
        // récupérer l'idx en javascript
        printf("<input type='hidden' id='programmation' name='programmation' value='".$this->getVal('programmation')."' />");
        $this->bloc_tableau();
        printf("</div>");
        printf("</div>");
        printf("<div class=\"visualClear\"></div>");
    }

    /**
     * Affichage de l'interface de filtre par établissements proches
     */
    function bloc_geolocalisation() {
        // Liste des champs
        $champs = array('btn_tous', 'btn_proches',
            'code_etab', 'rayon', 'btn_valider');
        $form = $this->f->get_inst__om_formulaire(array(
            "maj" => 0,
            "validation" => 0,
            "champs" => $champs,
        ));

        // Types
        $form->setType('btn_tous', 'button');
        $form->setType('btn_proches', 'button');
        $form->setType('code_etab', 'text');
        $form->setType('rayon', 'select');
        $form->setType('btn_valider', 'button');

        // Libellés
        $form->setLib('btn_tous', '');
        $form->setLib('btn_proches', '');
        $form->setLib('code_etab', __('Code établissement'));
        $form->setLib('rayon', __('Rayon'));
        $form->setLib('btn_valider', '');

        // Paramètres
        $form->setSelect('btn_tous', array('name'=>__('Tous')));
        $form->setSelect('btn_proches', array('name'=>__('Proches')));
        $form->setSelect('btn_valider', array('name'=>__('Valider')));

        // Tailles
        $form->setTaille('code_etab', 5);
        $form->setMax('code_etab', 25);

        // Listes déroulantes
        $options = array();
        foreach ($this->rayons as $rayon) {
            $options[0][] = $rayon;
            $options[1][] = $rayon.__('m');
        }
        $form->setSelect('rayon', $options);

        // Actions JS
        $form->setOnclick('btn_tous', 'bloc_filtre_visite_etablissement(\'tous\');');
        $form->setOnclick('btn_proches', 'bloc_filtre_visite_etablissement(\'proches\');');
        $form->setOnclick('btn_valider', 'bloc_filtre_visite_etablissement(\'valider\');');

        // Affichage
        $form->setBloc('btn_tous', 'D', __('Établissements'), 'group bloc_filtre_visite_etablissement');
        $form->setBloc('btn_proches', 'F');
        $form->setBloc('code_etab', 'D', "", 'group bloc_filtre_visite_etablissement_proches_fields');
        $form->setBloc('btn_valider', 'F');
        $form->afficher($champs, 0, false, false);
    }
    
    /**
     * Affichage des boutons de filtrage des propositions de visite.
     */
    function bloc_filtres(){
        //Liste des champs
        $champs = array("type_visite_btn", "a_poursuivre_btn", 
            "locaux_a_sommeil_btn", "retard_btn", "prioritaire_btn");
        
        $form = $this->f->get_inst__om_formulaire(array(
            "maj" => 0,
            "validation" => 0,
            "champs" => $champs,
        ));
        
        //Configuration du select type_visite_btn
        $form->setLib("type_visite_btn", "");
        $form->setType("type_visite_btn", "select");
        
        $contenu = array();
        $contenu[0][] = '';
        $contenu[1][] = __("tous les types de visite");

        $contenu[0][] = 'VP';
        $contenu[1][] = __("périodique");

        $contenu[0][] = 'VC';
        $contenu[1][] = __("contrôle");

        $contenu[0][] = 'VR';
        $contenu[1][] = __("réception");
        $form->setSelect("type_visite_btn", $contenu);
        
        //Configuration des checkboxs a_poursuivre_btn, locaux_a_sommeil_btn
        $form->setLib("a_poursuivre_btn", __("à poursuivre"));
        $form->setLib("locaux_a_sommeil_btn", __("locaux à sommeil"));
        $form->setLib("retard_btn", __("retard"));
        $form->setLib("prioritaire_btn", __("prioritaire"));
        for($i=1;$i<count($champs);$i++){
            $form->setType($champs[$i], "checkbox");
            $form->setTaille($champs[$i], 1);
            $form->setMax($champs[$i], 1);
        }
        
        //Ajout des actions javascript sur les champs
        foreach ($champs as $champ){
            $form->setOnchange($champ, "filtre_propositions(this);");
        }
        
        //
        $form->setBloc("type_visite_btn", "D", __("Filtres"), "group");
        $form->setBloc("prioritaire_btn", "F");
        //
        $form->afficher($champs, 0, false, false);
    }
    
    /**
     * Affichage du tableau
     */
    function bloc_tableau(){
        // Liste des champs du tableau
        $champs = $this->get_champs_tab_propositions();

        // Tableau
        printf('<table id="tableau_propositions" class="tab-tab">');

        // Le tableau n'est composé seulement de l'entête et du pied,
        // les données sont ajoutées depuis le javascript
        $table = $this->generate_tab_header_footer_proposition($champs);
        printf($table);

        //
        printf('</table>');
    }

    /**
     * Construit le tableau du bloc des propositions.
     *
     * @param array $champs Liste des champs du tableau
     *
     * @return string
     */
    function generate_tab_header_footer_proposition($champs) {
        // Ouvre l'entête du tableau
        $thead = '<thead>';
        $thead .= '<tr class="ui-tabs-nav ui-accordion ui-state-default tab-title">';

        // Ouvre le pied du tableau
        $tfoot = '<tfoot class="search_tab_tfoot">';
        $tfoot .= '<tr class="search_tab_row">';

        // Pour chaque champs
        foreach ($champs as $key => $champ) {
            // Cellule de l'entête
            $thead .= '<th class="'.$champ.' title col-'.$key.'">';
            $thead .= '<span class="name">'.__($champ).'</span>';
            $thead .= '</th>';

            // Cellule du pied : ce sont les champs de recherche
            $tfoot .= '<th class="'.$champ.' search_tab_cell col-'.$key.'">';
            $tfoot .= '<span class="name">'.__('Recherche').'</span>';
            $tfoot .= '</th>';
        }
        // Ferme l'entête
        $thead .= '</tr>';
        $thead .= '</thead>';

        // Ferme le pied
        $tfoot .= '</tr>';
        $tfoot .= '</tfoot>';
        
        // Retourne l'entête et le pied du tableau
        $return = $thead.$tfoot;
        return $return;
    }

    /**
     * Affichage du bloc agenda
     */
    function bloc_agenda($id){
    
        // On ouvre le conteneur agenda
        echo '<div id="bloc_agenda">';
        $this->widget_technicien();
        // On ouvre le bloc calendrier
        echo '<div class="agenda_calendrier" id="agenda_calendrier_'.$id.'">';
        $this->f->addHTMLHeadJs("../app/lib/moments/moments.js");
        $this->f->addHTMLHeadJs("../app/lib/fullcalendar/fullcalendar.js");
        $this->f->addHTMLHeadJs("../app/lib/fullcalendar/lang/fr.js");
        $this->f->layout->layout->display_script_css_call("../app/lib/fullcalendar/fullcalendar.css");
        // On ferme le bloc calendrier
        echo '</div>';   
        // On ferme le conteneur agenda
        echo '</div>';   
    }

    /**
     * Affichage du widget de sélection du technicien
     */
    function widget_technicien() {
    
        // On ouvre le bloc de sélection
        printf('<div id="agenda_selection_technicien">');
        // On affiche l'infobulle
        $info =__("Selectionnez un technicien afin de pouvoir programmer une visite");
        printf("<span id=\"filtre_technicien_info\">");
        printf("<span title=\"".$info."\" class=\"info-16\">");
        printf("</span>");
        printf("</span>");
        // On crée le formulaire
        $champs = array('acteur');
        $form = $this->f->get_inst__om_formulaire(array(
            "maj" => 0,
            "validation" => 0,
            "champs" => $champs,
        ));
        // On crée le contenu du select
        $contenu = array();
        //Requête de récupération des techniciens
        // QUERY liste des acteurs qui peuvent instruire un dossier
        // XXX En fonction de la semaine de programmation ?
        $sql_technicien = "
            SELECT 
                acteur.acteur, 
                acteur.nom_prenom
            FROM 
                ".DB_PREFIXE."acteur
            WHERE 
                (acteur.role = 'technicien'
                OR acteur.role='cadre')
                AND (service = ".$this->getVal('service')." OR service IS NULL)
                AND ((acteur.om_validite_debut IS NULL 
                      AND (acteur.om_validite_fin IS NULL 
                           OR acteur.om_validite_fin > '".$this->get_date_firstday("Y-m-d")."'))
                     OR (acteur.om_validite_debut <= '".$this->get_date_lastday("Y-m-d")."'
                         AND (acteur.om_validite_fin IS NULL 
                              OR acteur.om_validite_fin > '".$this->get_date_firstday("Y-m-d")."')))
            ORDER BY 
                acteur.nom_prenom ASC
        ";
        $res = $this->f->db->query($sql_technicien);
        $this->addToLog(
            __METHOD__."(): db->query(\"".$sql_technicien."\");",
            VERBOSE_MODE
        );
        $this->f->isDatabaseError($res);
        $contenu[0][0] = '';
        $contenu[1][0] = __('tous les techniciens');
        $k=1;
        while($row =& $res->fetchRow()){
            $contenu[0][$k] = $row[0];
            $contenu[1][$k] = $row[1];
            $k++;
        }
        // On valorise le champ
        $form->setSelect("acteur", $contenu);
        $form->setType("acteur", "select");
        $form->setOnchange("acteur", "handle_programmation_technicien();");
        $form->setLib("acteur", __("Agenda de"));
        $form->afficher($champs, 0, false, false);
        // On ferme le bloc de sélection
        printf('</div>');
    }

    /**
     * VIEW - get_first_day.
     * 
     * Retourne en JSON le lundi de la programmation postée.
     *
     * @return void
     */
    function get_first_day() {
        //
        $this->checkAccessibility();
        //
        $this->f->disableLog();
        //
        $lundi = new DateTime();
        //
        $lundi->setISODate(
            $this->getVal('annee'),
            $this->getVal('numero_semaine'),
            1
        );
        //
        $lundi = $lundi->format('Y-m-d');
        //
        echo json_encode(array(
                "lundi" => $lundi,
            )
        );
    }

    /**
     * VIEW - recuperer_toutes_les_visites.
     * 
     * Retourne en JSON les visites programmées
     * pour créer des événements dans l'agenda.
     *
     * @return void
     */
    function recuperer_toutes_les_visites() {
        //
        $this->checkAccessibility();
        //
        $this->f->disableLog();
        //
        $id_programmation = $this->getVal($this->clePrimaire);
        //
        $visites = $this->get_visites_non_annulees_by_programmation($id_programmation);
        //
        echo json_encode($visites);
    }

    /**
     * Retourne le nombre de visites planifiées.
     *
     * @return integer
     */
    function get_nb_visites_planifiees() {
        $query = sprintf(
            'SELECT
                count(*)
            FROM
                %1$svisite
                    LEFT JOIN %1$svisite_etat
                        ON visite.visite_etat = visite_etat.visite_etat
            WHERE
                visite.programmation = %2$s
                AND visite_etat.code != \'ANN\'',
            DB_PREFIXE,
            intval($this->getVal("programmation"))
        );
        $res = $this->f->db->getone($query);
        $this->addToLog(
            __METHOD__."(): db->getone(\"".$query."\");",
            VERBOSE_MODE
        );
        $this->f->isDatabaseError($res);
        return intval($res);
    }

    /**
     * Récupère toutes les visites non annulées d'une programmation,
     * filtrées optionnellement par un technicien.
     * 
     * @param   string  $id_programmation  ID de la programmation
     * @param   string  $id_technicien     ID de l'acteur (filtre facultatif)
     * @return  array                      Tableau des visites
     */
    function get_visites_non_annulees_by_programmation($id_programmation, $id_technicien = "") {
        // Préparation de la requête
        $sql = "SELECT
            visite.visite as id,
            visite.date_visite as jour,
            visite.heure_debut as debut,
            visite.heure_fin as fin,
            acteur.acronyme as acronyme,
            acteur.couleur as couleur,
            etablissement.code as etablissement
        FROM ".DB_PREFIXE."visite
        LEFT JOIN ".DB_PREFIXE."acteur
            ON visite.acteur = acteur.acteur
        LEFT JOIN ".DB_PREFIXE."dossier_instruction
            ON visite.dossier_instruction = dossier_instruction.dossier_instruction
        LEFT JOIN ".DB_PREFIXE."dossier_coordination
            ON dossier_instruction.dossier_coordination = dossier_coordination.dossier_coordination
        LEFT JOIN ".DB_PREFIXE."etablissement
            ON dossier_coordination.etablissement = etablissement.etablissement
        LEFT JOIN ".DB_PREFIXE."visite_etat
            ON visite.visite_etat = visite_etat.visite_etat
        WHERE programmation = ".$id_programmation." AND
            visite_etat.code != 'ANN'";
        // Filtre technicien le cas échéant
        if (!empty($id_technicien)) {
            $sql .= " AND visite.acteur = ".$id_technicien;
        }
        // Exécution de la requête
        $res = $this->f->db->query($sql);
        $this->addToLog(
            __METHOD__."(): db->query(\"".$sql."\");",
            VERBOSE_MODE
        );
        $this->f->isDatabaseError($res);
        // Initialisation du tableau de visites
        $visites = array();
        // Construction du tableau
        while ($row=& $res->fetchRow(DB_FETCHMODE_ASSOC)) {
            $link = OM_ROUTE_SOUSFORM."&obj=visite&action=3&retourformulaire=programmation&idxformulaire=".$id_programmation."&idx=".$row['id'];
            $color = "#2270C3";
            $text_color = "#FFFFFF";
            // Si pas de filtre technicien alors couleur personnalisée
            if (empty($id_technicien)) {
                $color = "#".$row['couleur'];
                $text_color = "#".$this->get_contrast($row['couleur']);
            }
            $visites[] = array(
                "id" => "visite_".$row['id'],
                "title" => $row['etablissement']." - ".$row['acronyme'],
                "start" => $row['jour']."T".$row['debut'],
                "end" => $row['jour']."T".$row['fin'],
                "color" => $color,
                "textColor" => $text_color,
                "url" => $link
            );
        }
        // retour tableau de visites
        return $visites;
    }

    /**
     * VIEW - recuperer_visites_technicien.
     * 
     * Retourne en JSON les visites programmées d'un technicien donné
     * pour créer des événements dans l'agenda.
     * 
     * @return void
     */
    function recuperer_visites_technicien() {
        //
        $this->checkAccessibility();
        //
        $this->f->disableLog();
        //
        $id_programmation = $this->getVal($this->clePrimaire);
        $id_technicien = $this->f->get_submitted_get_value('id_technicien');
        //
        $visites = $this->get_visites_non_annulees_by_programmation($id_programmation, $id_technicien);
        $plages = $this->recuperer_plages_technicien($id_technicien, $id_programmation);
        $conges = $this->recuperer_conges_technicien($id_technicien);
        $evenements = array();
        $evenements = array_merge($visites, $plages, $conges);
        //
        echo json_encode($evenements);
    }



    function recuperer_conges_technicien($id_technicien) {
        
        // Valorisation du titre et de la couleur de fond de la plage
        $couleur = '#DF2030';
        $titre = 'Congés';
        // Requete de récupération des plages préférées du technicien
        $sql = "SELECT
            acteur_conge.date_debut as date_debut,
            acteur_conge.heure_debut as heure_debut,
            acteur_conge.date_fin as date_fin,
            acteur_conge.heure_fin as heure_fin
        FROM ".DB_PREFIXE."acteur_conge
        WHERE acteur_conge.acteur = ".$id_technicien;
        //
        $res = $this->f->db->query($sql);
        $this->addToLog(
            __METHOD__."(): db->query(\"".$sql."\");",
            VERBOSE_MODE
        );
        $this->f->isDatabaseError($res);
        //
        $conges = array();
        // Création des congés concernés
        while ($row=& $res->fetchRow(DB_FETCHMODE_ASSOC)) {
            $conges[] = array(
                "title" => $titre,
                "start" => $row['date_debut']."T".$row['heure_debut'],
                "end" => $row['date_fin']."T".$row['heure_fin'],
                "color" => $couleur
            );
        }
        // Retour du tableau des congés
        return $conges;
    }

    /**
     * Récupère les plages préférées de visite d'un technicien donné
     * sous forme d'événements pour le calendrier de la programmation donnée
     * 
     * @param  [integer] $id_technicien     Identifiant du technicien
     * @param  [integer] $id_programmation  Identifiant de la programmation
     * @return [array]                      Tableau associatif
     */
    function recuperer_plages_technicien($id_technicien, $id_programmation) {

        // Valorisation du titre et de la couleur de fond de la plage
        $couleur = '#5A734E';
        $titre = 'Plage préférée';
        // Récupération de l'année et la semaine
        $tab_temp = $this->get_week_and_year($id_programmation);
        $annee = $tab_temp['annee'];
        $semaine = $tab_temp['semaine'];
        // Définition des jours
        $lundi = new DateTime();
        $lundi->setISODate($annee,$semaine,1);
        $lundi = $lundi->format('Y-m-d');
        $mardi = new DateTime();
        $mardi->setISODate($annee,$semaine,2);
        $mardi = $mardi->format('Y-m-d');
        $mercredi = new DateTime();
        $mercredi->setISODate($annee,$semaine,3);
        $mercredi = $mercredi->format('Y-m-d');
        $jeudi = new DateTime();
        $jeudi->setISODate($annee,$semaine,4);
        $jeudi = $jeudi->format('Y-m-d');
        $vendredi = new DateTime();
        $vendredi->setISODate($annee,$semaine,5);
        $vendredi = $vendredi->format('Y-m-d');
        // Définition des heures
        $debut_matin = '08:00:00';
        $fin_matin = '12:00:00';
        $debut_apresmidi = '14:00:00';
        $fin_apresmidi = '18:00:00';
        // Requete de récupération des plages préférées du technicien
        $sql = "SELECT
            acteur_plage_visite.lundi_matin as lundi_matin,
            acteur_plage_visite.lundi_apresmidi as lundi_apresmidi,
            acteur_plage_visite.mardi_matin as mardi_matin,
            acteur_plage_visite.mardi_apresmidi as mardi_apresmidi,
            acteur_plage_visite.mercredi_matin as mercredi_matin,
            acteur_plage_visite.mercredi_apresmidi as mercredi_apresmidi,
            acteur_plage_visite.jeudi_matin as jeudi_matin,
            acteur_plage_visite.jeudi_apresmidi as jeudi_apresmidi,
            acteur_plage_visite.vendredi_matin as vendredi_matin,
            acteur_plage_visite.vendredi_apresmidi as vendredi_apresmidi
        FROM ".DB_PREFIXE."acteur_plage_visite
        WHERE acteur_plage_visite.acteur = ".$id_technicien;
        //
        $res = $this->f->db->query($sql);
        $this->addToLog(
            __METHOD__."(): db->query(\"".$sql."\");",
            VERBOSE_MODE
        );
        $this->f->isDatabaseError($res);
        //
        $plages = array();
        $row =& $res->fetchRow(DB_FETCHMODE_ASSOC);
        // Si au moins une ligne est renvoyée par la requête
        if (is_array($row) === true) {
            // Définition des plages suivant le résultat de la requete
            ($row['lundi_matin'] == 't' ? $lundi_matin = true : $lundi_matin = false);
            ($row['lundi_apresmidi'] == 't' ? $lundi_apresmidi = true : $lundi_apresmidi = false);
            ($row['mardi_matin'] == 't' ? $mardi_matin = true : $mardi_matin = false);
            ($row['mardi_apresmidi'] == 't' ? $mardi_apresmidi = true : $mardi_apresmidi = false);
            ($row['mercredi_matin'] == 't' ? $mercredi_matin = true : $mercredi_matin = false);
            ($row['mercredi_apresmidi'] == 't' ? $mercredi_apresmidi = true : $mercredi_apresmidi = false);
            ($row['jeudi_matin'] == 't' ? $jeudi_matin = true : $jeudi_matin = false);
            ($row['jeudi_apresmidi'] == 't' ? $jeudi_apresmidi = true : $jeudi_apresmidi = false);
            ($row['vendredi_matin'] == 't' ? $vendredi_matin = true : $vendredi_matin = false);
            ($row['vendredi_apresmidi'] == 't' ? $vendredi_apresmidi = true : $vendredi_apresmidi = false);
        } else {
            $lundi_matin = false;
            $lundi_apresmidi = false;
            $mardi_matin = false;
            $mardi_apresmidi = false;
            $mercredi_matin = false;
            $mercredi_apresmidi = false;
            $jeudi_matin = false;
            $jeudi_apresmidi = false;
            $vendredi_matin = false;
            $vendredi_apresmidi = false;
        }
        // Création des plages concernées
        if ($lundi_matin == true) {
            $plages[] = array(
                "title" => $titre,
                "start" => $lundi."T".$debut_matin,
                "end" => $lundi."T".$fin_matin,
                "color" => $couleur
            );
        }
        if ($lundi_apresmidi == true) {
            $plages[] = array(
                "title" => $titre,
                "start" => $lundi."T".$debut_apresmidi,
                "end" => $lundi."T".$fin_apresmidi,
                "color" => $couleur
            );
        }
        if ($mardi_matin == true) {
            $plages[] = array(
                "title" => $titre,
                "start" => $mardi."T".$debut_matin,
                "end" => $mardi."T".$fin_matin,
                "color" => $couleur
            );
        }
        if ($mardi_apresmidi == true) {
            $plages[] = array(
                "title" => $titre,
                "start" => $mardi."T".$debut_apresmidi,
                "end" => $mardi."T".$fin_apresmidi,
                "color" => $couleur
            );
        }
        if ($mercredi_matin == true) {
            $plages[] = array(
                "title" => $titre,
                "start" => $mercredi."T".$debut_matin,
                "end" => $mercredi."T".$fin_matin,
                "color" => $couleur
            );
        }
        if ($mercredi_apresmidi == true) {
            $plages[] = array(
                "title" => $titre,
                "start" => $mercredi."T".$debut_apresmidi,
                "end" => $mercredi."T".$fin_apresmidi,
                "color" => $couleur
            );
        }
        if ($jeudi_matin == true) {
            $plages[] = array(
                "title" => $titre,
                "start" => $jeudi."T".$debut_matin,
                "end" => $jeudi."T".$fin_matin,
                "color" => $couleur
            );
        }
        if ($jeudi_apresmidi == true) {
            $plages[] = array(
                "title" => $titre,
                "start" => $jeudi."T".$debut_apresmidi,
                "end" => $jeudi."T".$fin_apresmidi,
                "color" => $couleur
            );
        }
        if ($vendredi_matin == true) {
            $plages[] = array(
                "title" => $titre,
                "start" => $vendredi."T".$debut_matin,
                "end" => $vendredi."T".$fin_matin,
                "color" => $couleur
            );
        }
        if ($vendredi_apresmidi == true) {
            $plages[] = array(
                "title" => $titre,
                "start" => $vendredi."T".$debut_apresmidi,
                "end" => $vendredi."T".$fin_apresmidi,
                "color" => $couleur
            );
        }
        // Retour du tableau des plages
        return $plages;
    }

    /**
     * VIEW - get_data_datatable.
     * 
     * Vue JSON envoyée au dataTable comme source de données :
     * tableau de propositions de visite sans filtre d'établissement.
     * 
     * @return void
     */
    function get_data_datatable() {
        //
        $this->checkAccessibility();
        $this->f->disableLog();

        //
        try {
            $json = $this->get_json_propositions();
        } catch (treatment_exception $e) {
            echo json_encode(array(
                'status' => 'KO',
                'message' => $e->getMessage(),
            ));
            return;
        }
        echo json_encode($json);
        return;
    }

    /**
     * VIEW - get_data_datatable_restricted.
     * 
     * Vue JSON envoyée au dataTable comme source de données :
     * tableau de propositions de visite filtrées par établissements proches.
     * 
     * @return void
     */
    function get_data_datatable_restricted() {

        //
        $this->checkAccessibility();
        $this->f->disableLog();

        //
        $this->f->set_submitted_value();
        $code_etab = $this->f->get_submitted_post_value('code_etab');
        $rayon = $this->f->get_submitted_post_value('rayon');

        // Contrôle des données
        if ($code_etab === null or $rayon === null) {
            echo json_encode(array(
                'status' => 'KO',
                'message' => __("Ce formulaire a été corrompu avant d'être soumis."),
            ));
            return;
        }
        if (in_array($rayon, $this->rayons) === false) {
            echo json_encode(array(
                'status' => 'KO',
                'message' => __("Ce formulaire a été corrompu avant d'être soumis."),
            ));
            return;
        }
        if (preg_match('/^T[0-9]+$/i', $code_etab) === 0) {
            echo json_encode(array(
                'status' => 'KO',
                'message' => __('Veuillez saisir un code établissement valide (numéro T).'),
            ));
            return;
        }

        // Récupération des établissements proches
        try {
            $liste_etablissement = $this->get_etablissements_proches(strtoupper($code_etab), $rayon);
        } catch (treatment_exception $e) {
            echo json_encode(array(
                'status' => 'KO',
                'message' => $e->getMessage(),
            ));
            return;
        }
        
        // Cas sans établissement
        if (empty($liste_etablissement) === true) {
            echo json_encode(array(
                'status' => 'OK',
                'message' => sprintf(__("Aucun établissement trouvé dans un rayon de %s mètres autour de l'établissement %s."),
                    $rayon, strtoupper($code_etab)
                    ),
                'result' => null,
            ));
            return;
        }
        // Cas avec établissement(s)
        $filtre_etablissement = sprintf(
            "AND etablissement.code IN (%s)",
            "'".implode("','", $liste_etablissement)."'"
        );
        try {
            $json = $this->get_json_propositions($filtre_etablissement);
        } catch (treatment_exception $e) {
            echo json_encode(array(
                'status' => 'KO',
                'message' => $e->getMessage(),
            ));
            return;
        }
        if (count($json->result) === 0) {
            echo json_encode(array(
                'status' => 'OK',
                'message' => sprintf(__("Géolocalisé autour de l'établissement %s dans un rayon de %s mètres mais aucun dossier visite ne correspond."),
                    strtoupper($code_etab), $rayon
                    ),
                'result' => null,
            ));
            return;
        }
        echo json_encode(array(
            'status' => 'OK',
            'message' => sprintf(__("Géolocalisé autour de l'établissement %s dans un rayon de %s mètres."),
                strtoupper($code_etab), $rayon
                ),
            'result' =>$json->result,
        ));
        return;
    }

    /**
     * Récupère depuis le SIG la liste des établissements proche de
     * celui passé en paramètre dans un rayon passé également en paramètre.
     * 
     * @param   string  $code_etab  Numéro T de l'établissement
     * @param   string  $rayon      Distance en mètres
     * @return  array               Liste des numéros T des établissements proches
     */
    private function get_etablissements_proches($code_etab, $rayon) {
        try {
            // WS OUT
            $geoaria = $this->f->get_inst_geoaria();
            $data = array(
                'distance'=> $rayon,
                'idcentre' => array(
                    'id' => $code_etab,
                    'type' => 'etablissement',
                ),
            );
            $liste_etablissements_proches_sig = $geoaria->lister_etablissements_proches($data);
        } catch (geoaria_exception $e) {
            throw new treatment_exception($this->handle_geoaria_exception($e));
        }
        $liste_etablissements_proches = array();
        foreach ($liste_etablissements_proches_sig as $key => $etablissement) {
            $liste_etablissements_proches[] = $etablissement['id'];
        }
        return $liste_etablissements_proches;
    }

    /**
     * Récupère en BDD les dossiers de visite à afficher comme propositions
     * dans le tableau de programmation.
     *
     * Possiblité de les filtrer par établissements proches.
     * 
     * @param   string  $filtre_etablissement  éventuelle clause WHERE
     * @return  object                         statut et résultat de la requête
     */
    private function get_json_propositions($filtre_etablissement = '') {
        // Requête SQL
        $sql = "SELECT
            dossier_instruction.libelle as dossier_instruction_libelle,
            CONCAT(etablissement.code, ' - ', etablissement.libelle) as etablissement_libelle,
            CONCAT(etablissement.adresse_numero, ' ', etablissement.adresse_numero2, ' ', voie.libelle) as adresse,
            etablissement.adresse_cp as code_postal,
            etablissement_type.libelle as etablissement_type_libelle,
            etablissement_categorie.libelle as etablissement_categorie_libelle,
            to_char(dossier_coordination.date_demande,'DD/MM/YYYY') as date_demande,
            reunion_avis.libelle as dernier_avis, 
            CASE WHEN service.code = 'ACC'
                THEN to_char(etablissement.acc_derniere_visite_date,'DD/MM/YYYY')
                ELSE to_char(etablissement.si_derniere_visite_date,'DD/MM/YYYY')
            END as date_dernier_avis, 
            COALESCE(
                CASE WHEN service.code = 'ACC'
                    THEN etab_acc_acteur.nom_prenom
                    ELSE etab_secu_acteur.nom_prenom
                END, 
                di_acteur.nom_prenom)
            as acteur_derniere_visite, 
            to_char(etablissement.si_prochaine_visite_periodique_date_previsionnelle,'DD/MM/YYYY') as si_prochaine_visite_periodique_date_previsionnelle,
            to_char(etablissement.si_prochaine_visite_date,'DD/MM/YYYY') as si_prochaine_visite_date,
            dossier_coordination_type.code as type_visite,
            CASE WHEN dossier_instruction.statut = 'a_poursuivre'
                THEN 't'
                ELSE 'f'
            END as a_poursuivre,
            etablissement.si_locaux_sommeil as locaux_a_sommeil,
            CASE WHEN dossier_coordination.date_butoir <= NOW() OR dossier_coordination.date_butoir IS NULL
                THEN 't'
                ELSE 'f'
            END as retard,
            dossier_instruction.prioritaire as prioritaire,
            dossier_instruction.dossier_instruction as dossier_instruction_id
        FROM ".DB_PREFIXE."dossier_instruction
            LEFT JOIN ".DB_PREFIXE."dossier_coordination
                ON dossier_instruction.dossier_coordination = dossier_coordination.dossier_coordination
            LEFT JOIN ".DB_PREFIXE."dossier_coordination_type
                ON dossier_coordination.dossier_coordination_type = dossier_coordination_type.dossier_coordination_type
            LEFT JOIN ".DB_PREFIXE."dossier_type
                ON dossier_coordination_type.dossier_type = dossier_type.dossier_type
            LEFT JOIN ".DB_PREFIXE."etablissement
                ON dossier_coordination.etablissement = etablissement.etablissement
            LEFT JOIN ".DB_PREFIXE."acteur as di_acteur
                ON dossier_instruction.technicien = di_acteur.acteur
            LEFT JOIN ".DB_PREFIXE."acteur as etab_acc_acteur
                ON etablissement.acc_derniere_visite_technicien = etab_acc_acteur.acteur
            LEFT JOIN ".DB_PREFIXE."acteur as etab_secu_acteur
                ON etablissement.si_derniere_visite_technicien = etab_secu_acteur.acteur
            LEFT JOIN ".DB_PREFIXE."voie
                ON etablissement.adresse_voie = voie.voie
            LEFT JOIN ".DB_PREFIXE."etablissement_type
                ON etablissement.etablissement_type = etablissement_type.etablissement_type
            LEFT JOIN ".DB_PREFIXE."etablissement_categorie
                ON etablissement.etablissement_categorie = etablissement_categorie.etablissement_categorie
            LEFT JOIN ".DB_PREFIXE."service
                ON dossier_instruction.service = service.service
            LEFT JOIN ".DB_PREFIXE."reunion_avis
                ON CASE WHEN service.code = 'ACC'
                        THEN etablissement.acc_derniere_visite_avis
                        ELSE etablissement.si_derniere_visite_avis
                    END = reunion_avis.reunion_avis
        WHERE LOWER(dossier_type.code) = LOWER('visit')
            AND dossier_instruction.dossier_cloture IS FALSE
            AND (dossier_instruction.statut = 'a_programmer' 
                OR dossier_instruction.statut = 'a_poursuivre'
                OR dossier_instruction.statut IS NULL)
            AND dossier_instruction.service = '".$this->getVal('service')."'
            ".$filtre_etablissement."
        ORDER BY
            (CASE
                WHEN dossier_instruction.statut = 'a_poursuivre'
                    THEN 1
                WHEN LOWER(dossier_coordination_type.code) = LOWER('VISITEP') AND etablissement.si_locaux_sommeil = 't'
                    THEN 2
                WHEN LOWER(dossier_coordination_type.code) like LOWER('VISITC%') AND etablissement.si_locaux_sommeil = 't'
                    THEN 3
                WHEN LOWER(dossier_coordination_type.code) = LOWER('DR')
                    THEN 4
                WHEN LOWER(dossier_coordination_type.code) = LOWER('VISITEP') AND etablissement.si_locaux_sommeil = 'f'
                    THEN 5
                WHEN LOWER(dossier_coordination_type.code) like LOWER('VISITC%') AND etablissement.si_locaux_sommeil = 'f'
                    THEN 6
            END) ASC,
            dossier_coordination.date_demande ASC
        ";
        // Exécution de la requête
        $propositions = $this->f->db->query($sql);
        $this->addToLog(
            __METHOD__."(): db->query(\"".$sql."\");",
            VERBOSE_MODE
        );
        if (database::isError($propositions, true)) {
            $err_mess = __("Erreur de base de donnees. Contactez votre administrateur.");
            throw new treatment_exception(
                $err_mess,
                __METHOD__.": ".$propositions->getMessage()
            );
        }
        //
        $champs = $this->get_champs_tab_propositions();
        $json = new stdClass();
        $json->result = array();
        $json->status = 'OK';
        $i = 0;
        //
        while ($row =& $propositions->fetchRow(DB_FETCHMODE_ASSOC)) {
            foreach ($champs as $key => $champ) {
                $json->result[$i][$champ] = $row[$champ];
            }
            $i++;
        }
        //
        return $json;
    }

    /**
     * Liste des champs du tableau du bloc des propositions.
     *
     * @return array
     */
    function get_champs_tab_propositions() {
        // Liste des champs
        // Ces même champs doivent être déclarés dans le javascript lors
        // de l'initialisation du datatable
        $champs = array(
            "dossier_instruction_libelle",
            "etablissement_libelle",
            "adresse",
            "code_postal",
            "etablissement_type_libelle",
            "etablissement_categorie_libelle",
            "date_demande",
            "dernier_avis",
            "date_dernier_avis",
            "acteur_derniere_visite",
            "si_prochaine_visite_periodique_date_previsionnelle",
            "si_prochaine_visite_date",
            "type_visite",
            "a_poursuivre",
            "locaux_a_sommeil",
            "retard",
            "prioritaire",
            "dossier_instruction_id"
        );

        // Traduction des champs du tableau
        __("dossier_instruction_libelle");
        __("etablissement_libelle");
        __("adresse");
        __("code_postal");
        __("etablissement_type_libelle");
        __("etablissement_categorie_libelle");
        __("dernier_avis");
        __("date_dernier_avis");
        __("acteur_derniere_visite");
        __("si_prochaine_visite_periodique_date_previsionnelle");
        __("si_prochaine_visite_date");
        __("type_visite");
        __("a_poursuivre");
        __("locaux_a_sommeil");
        __("retard");
        __("prioritaire");
        __("dossier_instruction_id");

        // Retourne la liste des champs
        return $champs;
    }

    /**
     * CONDITION - is_in_working_state.
     *
     * La programmation est dans l'état "En préparation" représente son état de
     * travail.
     *
     * @return boolean 
     */
    function is_in_working_state() {
        //
        if ($this->get_programmation_etat_code_by_id($this->getVal("programmation_etat")) === 'pre') {
            //
            return true;
        }
        //
        return false;
    }

    /**
     * CONDITION - is_planifiable.
     *
     * On vérifie que la plannification est dans un état qui la rend plannifiable.
     *
     * @return boolean 
     */
    function is_planifiable(){
        //Si la programmation a un état
        if($this->get_programmation_etat_code_by_id($this->getVal("programmation_etat")) === 'pre') {
            //On vérifie que la programmation est bien dans un état "En préparation"
            $this->addToLog(__METHOD__."(): return true;", EXTRA_VERBOSE_MODE);
            return true;
        }
        return false;
    }

    /**
     * CONDITION - is_programmation_validee.
     *
     * Permet de définir si l'action est executable.
     *
     * @return boolean
     */
    function is_programmation_validee() {
        //Si la programmation est validée
        if($this->get_programmation_etat_code_by_id($this->getVal("programmation_etat")) === 'val') {
            //On vérifie que la programmation est bien dans un état "Validée"
            $this->addToLog(__METHOD__."(): return true;", EXTRA_VERBOSE_MODE);
            return true;
        }
        $this->addToLog(__METHOD__."(): return false;", EXTRA_VERBOSE_MODE);
        return false;
    }


    /**
     * CONDITION - is_finalisable.
     *
     * @return boolean
     */
    function is_finalisable() {
        //Si la programmation a un état
        if($this->get_programmation_etat_code_by_id($this->getVal("programmation_etat")) === 'pre') {
            //On vérifie que la programmation est bien dans un état "En préparation"
            $this->addToLog(__METHOD__."(): return true;", EXTRA_VERBOSE_MODE);
            return true;
        }
        $this->addToLog(__METHOD__."(): return false;", EXTRA_VERBOSE_MODE);
        return false;
    }

    /**
     * CONDITION - is_openable.
     *
     * @return boolean
     */
    function is_openable() {
        //Si la programmation a un état
        if($this->get_programmation_etat_code_by_id($this->getVal("programmation_etat")) === 'fin') {
            //On vérifie que la programmation est bien dans un état "En préparation"
            $this->addToLog(__METHOD__."(): return true;", EXTRA_VERBOSE_MODE);
            return true;
        }
        $this->addToLog(__METHOD__."(): return false;", EXTRA_VERBOSE_MODE);
        return false;
    }

    /**
     * CONDITION - is_validable.
     *
     * @return boolean
     */
    function is_validable() {
        //Si la programmation a un état
        if($this->get_programmation_etat_code_by_id($this->getVal("programmation_etat")) === 'fin') {
            //On vérifie que la programmation est bien dans un état "En préparation"
            $this->addToLog(__METHOD__."(): return true;", EXTRA_VERBOSE_MODE);
            return true;
        }
        $this->addToLog(__METHOD__."(): return false;", EXTRA_VERBOSE_MODE);
        return false;
    }


    /**
     * TREATMENT - marquer_comme_planifier_nouveau.
     *
     * Permet de ...
     *
     * @param array $val Valeurs soumises par le formulaire
     *
     * @return boolean
     */
    function marquer_comme_planifier_nouveau($val = array()) {
        // Begin
        $this->begin_treatment(__METHOD__);

        //
        $valF = array(
            "planifier_nouveau" => true,
        );
        // Execution de la requête de modification des donnees de l'attribut
        // valF de l'objet dans l'attribut table de l'objet
        $res = $this->f->db->autoExecute(DB_PREFIXE.$this->table, $valF, DB_AUTOQUERY_UPDATE, $this->getCle($this->getVal($this->clePrimaire)));
        // Logger
        $this->addToLog(
            __METHOD__."(): db->autoExecute(\"".DB_PREFIXE.$this->table."\", ".print_r($valF, true).", DB_AUTOQUERY_UPDATE, \"".$this->getCle($this->getVal($this->clePrimaire))."\")",
            VERBOSE_MODE
        );
        // Si une erreur survient
        if (database::isError($res, true)) {
            // Appel de la methode de recuperation des erreurs
            $this->erreur_db($res->getDebugInfo(), $res->getMessage(), '');
            $this->correct = false;
            // Return
            return $this->end_treatment(__METHOD__, false);
        } else {
            //
            $main_res_affected_rows = $this->f->db->affectedRows();        
            // Message de validation
            if ($main_res_affected_rows == 0) {
                $this->addToMessage(__("Attention vous n'avez fait aucune modification.")."<br/>");
            } else {
                $this->addToMessage(__("La réunion a été correctement déclôturée.")."<br/>");
            }
        }

        // Return
        return $this->end_treatment(__METHOD__, true);
    }

    /**
     * Nettoyage des champs de saisie de liste de courriels.
     *
     * L'utilisateur peut saisir dans les champs des adresses email séparées par
     * des espaces, des virgules, des points virgules, des sauts de ligne. 
     * Cette méthode permet de nettoyer tous ces caractères pour séparer chaque 
     * adresse email par un unique point virgule.
     * 
     * @param string $emails_list Chaîne de caractères à nettoyer.
     *
     * @return string
     */
    function clean_emails_list($emails_list) {
        //
        $cleaned_emails_list = $emails_list;
        //
        $cleaned_emails_list = preg_replace('/\r\n?/', ';', $cleaned_emails_list);
        $cleaned_emails_list = str_replace(" ", ";", $cleaned_emails_list);
        $cleaned_emails_list = str_replace(",", ";", $cleaned_emails_list);
        $cleaned_emails_list = str_replace("\n", ";", $cleaned_emails_list);
        while (strpos($cleaned_emails_list, ";;") != false) {
            $cleaned_emails_list = str_replace(";;", ";", $cleaned_emails_list);
        }
        //
        return $cleaned_emails_list;
    }

    /**
     * MERGE_FIELDS - get_labels_merge_fields.
     * 
     * @return array
     */
    function get_labels_merge_fields() {
        $labels = parent::get_labels_merge_fields();

        $labels["programmation"]["programmation.premier_jour"] = __("premier jour de la programmation (Format : 14/01/1978)");
        $labels["programmation"]["programmation.premier_jour_lettre"] = __("premier jour de la programmation (Format : 14 janvier 1978)");
        $labels["programmation"]["programmation.dernier_jour"] = __("dernier jour de la programmation (Format : 14/01/1978)");
        $labels["programmation"]["programmation.dernier_jour_lettre"] = __("dernier jour de la programmation (Format : 14 janvier 1978)");

        $labels["programmation"]["programmation.nb_visites"] = __("nombre de visites planifiées");

        return $labels;
    }

    /**
     * MERGE_FIELDS - get_values_merge_fields.
     *
     * @return array
     */
    function get_values_merge_fields() {
        $values = parent::get_values_merge_fields();

        $values["programmation.premier_jour"] = $this->get_date_firstday("d/m/Y");
        $values["programmation.premier_jour_lettre"] = strftime("%e %B %Y", $this->get_date_firstday("U"));
        $values["programmation.dernier_jour"] = $this->get_date_lastday("d/m/Y");
        $values["programmation.dernier_jour_lettre"] = strftime("%e %B %Y", $this->get_date_lastday("U"));

        $values["programmation.nb_visites"] = $this->get_nb_visites_planifiees();

        return $values;
    }
}

