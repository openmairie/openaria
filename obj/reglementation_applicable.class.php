<?php
/**
 * Ce script définit la classe 'reglementation_applicable'.
 *
 * @package openaria
 * @version SVN : $Id$
 */

require_once "../gen/obj/reglementation_applicable.class.php";

/**
 * Définition de la classe 'reglementation_applicable' (om_dbform).
 */
class reglementation_applicable extends reglementation_applicable_gen {

    /**
     * Liaison NaN
     */
    var $liaisons_nan = array(
        "etablissement_categorie" => array(
            "table_l" => "lien_reglementation_applicable_etablissement_categorie",
            "table_f" => "etablissement_categorie",
            "field" => "etablissement_categorie"
        ),
        "etablissement_type" => array(
            "table_l" => "lien_reglementation_applicable_etablissement_type",
            "table_f" => "etablissement_type",
            "field" => "etablissement_type"
        )
    );


    /**
     *
     * @return array
     */
    function get_var_sql_forminc__champs() {
        return array(
            "reglementation_applicable.reglementation_applicable",
            "reglementation_applicable.service",
            "reglementation_applicable.libelle",
            "reglementation_applicable.description",
            "array_to_string(
                array_agg(
                    DISTINCT lien_reglementation_applicable_etablissement_categorie.etablissement_categorie
                ),
            ';') as etablissement_categorie",
            //
            "array_to_string(
                array_agg(
                    DISTINCT lien_reglementation_applicable_etablissement_type.etablissement_type
                ),
            ';') as etablissement_type",
            "reglementation_applicable.annee_debut_application",
            "reglementation_applicable.annee_fin_application",
            "reglementation_applicable.om_validite_debut",
            "reglementation_applicable.om_validite_fin",
        );
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__tableSelect() {
        return sprintf(
            '%1$s%2$s
            LEFT JOIN %1$slien_reglementation_applicable_etablissement_categorie
                ON lien_reglementation_applicable_etablissement_categorie.reglementation_applicable = reglementation_applicable.reglementation_applicable
            LEFT JOIN %1$slien_reglementation_applicable_etablissement_type
                ON lien_reglementation_applicable_etablissement_type.reglementation_applicable = reglementation_applicable.reglementation_applicable',
            DB_PREFIXE,
            $this->table
        );
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__selection() {
        return " GROUP BY reglementation_applicable.reglementation_applicable ";
    }

    /**
     * Liaison N à N - réglementation applicable / catégorie d'établissement
     */
    function get_var_sql_forminc__sql_etablissement_categorie() {
        return "
        SELECT etablissement_categorie.etablissement_categorie, etablissement_categorie.libelle as lib
        FROM ".DB_PREFIXE."etablissement_categorie
        ORDER BY etablissement_categorie.libelle";
    }

    /**
     *
     */
    function get_var_sql_forminc__sql_etablissement_categorie_by_id() {
        return "
        SELECT etablissement_categorie.etablissement_categorie, etablissement_categorie.libelle as lib
        FROM ".DB_PREFIXE."etablissement_categorie
        WHERE etablissement_categorie.etablissement_categorie IN (<idx>)
        ORDER BY etablissement_categorie.libelle";
    }

    /**
     * Liaison N à N - réglementation applicable / type d'établissement
     */
    function get_var_sql_forminc__sql_etablissement_type() {
        return "
        SELECT etablissement_type.etablissement_type, etablissement_type.libelle as lib
        FROM ".DB_PREFIXE."etablissement_type
        ORDER BY etablissement_type.libelle";
    }

    /**
     *
     */
    function get_var_sql_forminc__sql_etablissement_type_by_id() {
        return "
        SELECT etablissement_type.etablissement_type, etablissement_type.libelle as lib
        FROM ".DB_PREFIXE."etablissement_type
        WHERE etablissement_type.etablissement_type IN (<idx>)
        ORDER BY etablissement_type.libelle";
    }

    /**
     * Permet de définir le type des champs.
     *
     * @param object  &$form Instance du formulaire
     * @param integer $maj   Mode du formulaire
     */
    function setType(&$form, $maj) {
        parent::setType($form, $maj);
        //
        if ($maj==0){ //ajout
            $form->setType('etablissement_categorie', 'select_multiple');
            $form->setType('etablissement_type', 'select_multiple');
        }// fin ajout
        if ($maj==1){ //modifier
            $form->setType('etablissement_categorie', 'select_multiple');
            $form->setType('etablissement_type', 'select_multiple');
        }// fin modifier
        if ($maj==2){ //supprimer
            $form->setType('etablissement_categorie', 'select_multiple_static');
            $form->setType('etablissement_type', 'select_multiple_static');
        }//fin supprimer
        if ($maj==3){ //consulter
            $form->setType('etablissement_categorie', 'select_multiple_static');
            $form->setType('etablissement_type', 'select_multiple_static');
        }//fin consulter
    }

    /**
     *
     */
    function setLib(&$form, $maj) {
        //
        parent::setLib($form, $this->getParameter("maj"));
        // Liaison NaN
        $form->setLib("etablissement_categorie", __("categorie(s) d'etablissement"));
        $form->setLib("etablissement_type", __("type(s) d'etablissement"));
    }

    /**
     * Méthode qui effectue les requêtes de configuration des champs.
     *
     * @param object  $form Instance du formulaire.
     * @param integer $maj  Mode du formulaire.
     * @param null    $dnu1 @deprecated Ancienne ressource de base de données.
     * @param null    $dnu2 @deprecated Ancien marqueur de débogage.
     *
     * @return void
     */
    function setSelect(&$form, $maj, &$dnu1 = null, $dnu2 = null) {
        //
        parent::setSelect($form, $maj, $dnu1, $dnu2);
        //
        $this->init_select($form, $dnu1, $maj, $dnu2, "etablissement_categorie", $this->get_var_sql_forminc__sql("etablissement_categorie"), $this->get_var_sql_forminc__sql("etablissement_categorie_by_id"), false, true);
        //
        $this->init_select($form, $dnu1, $maj, $dnu2, "etablissement_type", $this->get_var_sql_forminc__sql("etablissement_type"), $this->get_var_sql_forminc__sql("etablissement_type_by_id"), false, true);
    }

    /**
     * TRIGGER - triggerajouterapres.
     *
     * - ...
     *
     * @return boolean
     */
    function triggerajouterapres($id, &$dnu1 = null, $val = array(), $dnu2 = null) {
        // Liaisons NaN
        foreach ($this->liaisons_nan as $liaison_nan) {
            // Ajout des liaisons table Nan
            $nb_liens = $this->ajouter_liaisons_table_nan(
                $liaison_nan["table_l"], 
                $liaison_nan["table_f"], 
                $liaison_nan["field"]
            );
        }
    }

    /**
     * TRIGGER - triggermodifierapres.
     *
     * - ...
     *
     * @return boolean
     */
    function triggermodifierapres($id, &$dnu1 = null, $val = array(), $dnu2 = null) {
        // Liaisons NaN
        foreach ($this->liaisons_nan as $liaison_nan) {
            // Suppression des liaisons table NaN
            $this->supprimer_liaisons_table_nan($liaison_nan["table_l"]);
            // Ajout des liaisons table Nan
            $this->ajouter_liaisons_table_nan(
                $liaison_nan["table_l"], 
                $liaison_nan["table_f"], 
                $liaison_nan["field"]
            );
        }

    }

    /**
     *
     */
    function triggersupprimer($id, &$dnu1 = null, $val = array(), $dnu2 = null) {

        // Liaisons NaN
        foreach ($this->liaisons_nan as $liaison_nan) {
            // Suppression des liaisons table NaN
            $this->supprimer_liaisons_table_nan($liaison_nan["table_l"]);
        }

    }

    /**
     *
     */
    function ajouter_liaisons_table_nan($table_l, $table_f, $field) {
        // Récupération des données du select multiple
        $postvar = $this->getParameter("postvar");
        if (isset($postvar[$field])
            && is_array($postvar[$field])) {
            $multiple_values = $postvar[$field];
        } else {
            $multiple_values = array();
        }
        // Ajout des liaisons
        $nb_liens = 0;
        // Boucle sur la liste des valeurs sélectionnées
        foreach ($multiple_values as $value) {
            // Test si la valeur par défaut est sélectionnée
            if ($value == "") {
                continue;
            }
            // On compose les données de l'enregistrement
            $donnees = array(
                $this->clePrimaire => $this->valF[$this->clePrimaire],
                $table_f => $value,
                $table_l => ""
            );
            // On ajoute l'enregistrement
            $obj_l = $this->f->get_inst__om_dbform(array(
                "obj" => $table_l,
                "idx" => "]",
            ));
            $obj_l->ajouter($donnees);
            // On compte le nombre d'éléments ajoutés
            $nb_liens++;
        }
        //
        return $nb_liens;
    }

    /**
     *
     */
    function supprimer_liaisons_table_nan($table) {
        // Suppression de tous les enregistrements correspondants à l'id 
        // de l'objet instancié en cours dans la table NaN
        $sql = "DELETE FROM ".DB_PREFIXE.$table." WHERE ".$this->clePrimaire."=".$this->getVal($this->clePrimaire);
        $res = $this->f->db->query($sql);
        $this->f->addToLog(__METHOD__."(): db->query(\"".$sql."\");", VERBOSE_MODE);
        if (database::isError($res)) {
            die();
        }
    }

    /**
     * Surcharge de la méthode rechercheTable pour éviter de court-circuiter le générateur
     * en devant surcharger la méthode cleSecondaire afin de supprimer les éléments liés dans
     * les tables NaN.
     */
    function rechercheTable(&$dnu1 = null,  $table = "", $field = "", $id = null, $dnu2 = null, $selection = "") {
        //
        if (in_array($table, array_keys($this->liaisons_nan))) {
            //
            $this->addToLog(__METHOD__."(): On ne vérifie pas la table ".$table." car liaison nan.", EXTRA_VERBOSE_MODE);
            return;
        }
        //
        parent::rechercheTable($this->f->db, $table, $field, $id, null, $selection);
    }
}
