<?php
/**
 * Ce script définit la classe 'etablissement_unite__contexte_etab__ua_valide'.
 *
 * @package openaria
 * @version SVN : $Id$
 */

require_once "../obj/etablissement_unite.class.php";

/**
 * Définition de la classe 'etablissement_unite__contexte_etab__ua_valide' (om_dbform).
 *
 * Surcharge de la classe 'etablissement_unite'.
 */
class etablissement_unite__contexte_etab__ua_valide extends etablissement_unite {

    /**
     *
     */
    protected $_absolute_class_name = "etablissement_unite__contexte_etab__ua_valide";

    /**
     * Définition des actions disponibles sur la classe.
     *
     * @return void
     */
    function init_class_actions() {

        // On récupère les actions génériques définies dans la méthode 
        // d'initialisation de la classe parente
        parent::init_class_actions();

        /**
         *
         */
        //
        $this->class_actions = array();
        foreach (array(0, 1, 3, 5, 8, ) as $key) {
            if (isset($this->class_actions_available[$key])) {
                $this->class_actions[$key] = $this->class_actions_available[$key];
            }
        }
    }


    /**
     *
     */
    function setValFAjout($val = array()) {
        //
        parent::setvalFAjout($val);
        //
        $this->valF['etat'] = "valide";
        //
        $this->valF['etablissement'] = $this->getParameter("idxformulaire");
    }

}

