<?php
/**
 * Ce script définit la classe 'autorite_police_non_notifiee_executee'.
 *
 * @package openaria
 * @version SVN : $Id$
 */

require_once "../obj/autorite_police.class.php";

/**
 * Définition de la classe 'autorite_police_non_notifiee_executee' (om_dbform).
 *
 * Surcharge de la classe 'autorite_police'. Cette classe permet d'afficher
 * seulement les autorités de police non notifiées ou executées.
 */
class autorite_police_non_notifiee_executee extends autorite_police {

    /**
     *
     */
    protected $_absolute_class_name = "autorite_police_non_notifiee_executee";

}
