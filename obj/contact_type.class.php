<?php
/**
 * Ce script définit la classe 'contact_type'.
 *
 * @package openaria
 * @version SVN : $Id$
 */

require_once "../gen/obj/contact_type.class.php";

/**
 * Définition de la classe 'contact_type' (om_dbform).
 */
class contact_type extends contact_type_gen {


    /**
     * Initialisation des actions supplémentaires de la classe.
     */
    function init_class_actions() {
        parent::init_class_actions();
        // ACTION - 004 - get_contact_type_code_by_id
        // Récupère le code du type de contact
        $this->class_actions[4] = array(
            "identifier" => "get_contact_type_code_by_id",
            "view" => "view_get_contact_type_code_by_id",
            "permission_suffix" => "json",
        );
    }

    /**
     * VIEW - get_contact_type_code_by_id.
     *
     * Permet de récupérer le code du type de contact.
     * Selon ce dernier certains champs seront affichés/masqués.
     *
     * return void
     */
    function view_get_contact_type_code_by_id() {
        // Désactive les logs
        $this->f->disableLog();
        // Retourne le code en lettres minuscules
        echo json_encode(strtolower($this->getVal('code')));
    }

    /**
     *
     */
    function get_id_from_code($code) {
        $sql = sprintf(
            "SELECT contact_type FROM %scontact_type WHERE code='%s'",
            DB_PREFIXE,
            $this->f->db->escapeSimple($code)
        );
        $id = $this->f->db->getone($sql);
        $this->addToLog(__METHOD__."(): db->getone(\"".$sql."\");", VERBOSE_MODE);
        if ($this->f->isDatabaseError($id, true) === true) {
            return null;
        }
        return $id;
    }
}

