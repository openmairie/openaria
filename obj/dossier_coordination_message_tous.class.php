<?php
/**
 * Ce script définit la classe 'dossier_coordination_message_tous'.
 *
 * @package openaria
 * @version SVN : $Id$
 */

require_once "../obj/dossier_coordination_message.class.php";

/**
 * Définition de la classe 'dossier_coordination_message_tous' (om_dbform).
 *
 * Surcharge de la classe 'dossier_coordination_message'.
 */
class dossier_coordination_message_tous extends dossier_coordination_message {

    /**
     *
     */
    protected $_absolute_class_name = "dossier_coordination_message_tous";

}

