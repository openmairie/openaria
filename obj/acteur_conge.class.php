<?php
/**
 * Ce script définit la classe 'acteur_conge'.
 *
 * @package openaria
 * @version SVN : $Id$
 */

require_once "../gen/obj/acteur_conge.class.php";

/**
 * Définition de la classe 'acteur_conge' (om_dbform).
 */
class acteur_conge extends acteur_conge_gen {


    function setType(&$form,$maj) {
        parent::setType($form,$maj);

        if ($maj == 0 || $maj == 1) {
            $form->setType('heure_debut','heure_minute');
            $form->setType('heure_fin','heure_minute');
        }
    }

}
