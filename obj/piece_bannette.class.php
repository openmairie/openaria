<?php
/**
 * Ce script définit la classe 'piece_bannette'.
 *
 * @package openaria
 * @version SVN : $Id$
 */

require_once "../obj/piece.class.php";

/**
 * Définition de la classe 'piece_bannette' (om_dbform).
 *
 * Surcharge de la classe 'piece'. Cette classe permet d'afficher seulement les
 * pièces qui ne sont pas liés à un etab/DC/DI.
 */
class piece_bannette extends piece {

    /**
     *
     */
    protected $_absolute_class_name = "piece_bannette";

    /**
     * Mutateur.
     *
     * Permet d'effectuer des appels aux mutateurs spécifiques sur le formulaire
     * de manière fonctionnelle et non en fonction du mutateur. Exemple : au lieu 
     * de gérer le champ service dans les méthodes setType, setSelect, le setLib, 
     * ... Nous allons les gérer dans cette méthode et appeler tous les mutateurs
     * à la suite.
     * 
     * @param resource $form Instance formulaire.
     * @param integer  $maj  Clé de l'action.
     *
     * @return void
     */
    function set_form_specificity(&$form, $maj) {
        //
        parent::set_form_specificity($form, $this->getParameter("maj"));
        //  Si utilisateur sans service et mode modifier
        if (is_null($_SESSION['service']) 
            && $this->getParameter("maj") == 1) {
            // On peut rechanger de service
            $form->setType("service", "select");
            // service
        	$this->init_select($form, $this->f->db, $maj, null, "service", $this->get_var_sql_forminc__sql("service"), $this->get_var_sql_forminc__sql("service_by_id"), true);
        }
    }
}

