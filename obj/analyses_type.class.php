<?php
/**
 * Ce script définit la classe 'analyses_type'.
 *
 * @package openaria
 * @version SVN : $Id$
 */

require_once "../gen/obj/analyses_type.class.php";

/**
 * Définition de la classe 'analyses_type' (om_dbform).
 */
class analyses_type extends analyses_type_gen {

    /**
     * filtre les modèles d'édition
     */
    function get_var_sql_forminc__sql_modele_edition() {
        return "SELECT modele_edition.modele_edition, modele_edition.libelle 
        FROM ".DB_PREFIXE."modele_edition 
        LEFT JOIN ".DB_PREFIXE."courrier_type ON courrier_type.courrier_type = modele_edition.courrier_type
        WHERE ((modele_edition.om_validite_debut IS NULL AND (modele_edition.om_validite_fin IS NULL OR modele_edition.om_validite_fin > CURRENT_DATE)) OR (modele_edition.om_validite_debut <= CURRENT_DATE AND (modele_edition.om_validite_fin IS NULL OR modele_edition.om_validite_fin > CURRENT_DATE)))
        AND LOWER(courrier_type.code) = LOWER('<courrier_type_code>')
        ORDER BY modele_edition.libelle ASC";
    }

    /**
     * Méthode qui effectue les requêtes de configuration des champs.
     *
     * @param object  $form Instance du formulaire.
     * @param integer $maj  Mode du formulaire.
     * @param null    $dnu1 @deprecated Ancienne ressource de base de données.
     * @param null    $dnu2 @deprecated Ancien marqueur de débogage.
     *
     * @return void
     */
    function setSelect(&$form, $maj, &$dnu1 = null, $dnu2 = null) {
        //
        parent::setSelect($form, $maj, $dnu1, $dnu2);

        // Filtre par service
        $service = "";
        $champ = "service";
        if (isset($_POST[$champ])) {
            $service = $_POST[$champ];
        } elseif ($this->getParameter($champ) != "") {
            $service = $this->getParameter($champ);
        } elseif (isset($form->val[$champ])) {
            $service = $form->val[$champ];
        } elseif ($this->getVal($champ) != null) {
            $service = $this->getVal($champ);
        }
        if ($service == "") {
            $service = -1;
        }
        $service_code = $this->f->get_service_code($service);

        // modele_edition_compte_rendu
        $this->init_select(
            $form,
            $this->f->db,
            $maj,
            null,
            "modele_edition_compte_rendu",
            str_replace(
                '<courrier_type_code>',
                'ANL-'.$service_code.'-CRD',
                $this->get_var_sql_forminc__sql("modele_edition")
            ),
            $this->get_var_sql_forminc__sql("modele_edition_compte_rendu_by_id"),
            true
        );
        // modele_edition_proces_verbal
        $this->init_select(
            $form,
            $this->f->db,
            $maj,
            null,
            "modele_edition_proces_verbal",
            str_replace(
                '<courrier_type_code>',
                'ANL-'.$service_code.'-PV',
                $this->get_var_sql_forminc__sql("modele_edition")
            ),
            $this->get_var_sql_forminc__sql("modele_edition_proces_verbal_by_id"),
            true
        );
        // modele_edition_rapport
        $this->init_select(
            $form,
            $this->f->db,
            $maj,
            null,
            "modele_edition_rapport",
            str_replace(
                '<courrier_type_code>',
                'ANL-'.$service_code.'-RPT',
                $this->get_var_sql_forminc__sql("modele_edition")
            ),
            $this->get_var_sql_forminc__sql("modele_edition_rapport_by_id"),
            true
        );
    }

    /**
     *
     */
    function set_form_specificity(&$form, $maj) {
        //
        parent::set_form_specificity($form, $this->getParameter("maj"));

        /**
         * Gestion du champ service
         */
        //
        $this->set_form_specificity_service_common($form, $this->getParameter("maj"));
        // Si l'utilisateur est sur un service
        if (is_null($_SESSION['service'])) {
            // En mode AJOUTER
            if ($this->getParameter("maj") == 0) {
                // A la modification du champ service alors on met à jour les champs
                // liés pour que seules les valeurs correspondantes soient affichées.
                $form->setOnchange(
                    "service",
                    "filterSelect(this.value, 'modele_edition_rapport', 'service', 'analyses_type');".
                    "filterSelect(this.value, 'modele_edition_compte_rendu', 'service', 'analyses_type');".
                    "filterSelect(this.value, 'modele_edition_proces_verbal', 'service', 'analyses_type');"
                );
            }
        }
    }

}

