<?php
/**
 * Ce script définit la classe 'reunion_avis'.
 *
 * @package openaria
 * @version SVN : $Id$
 */

require_once "../gen/obj/reunion_avis.class.php";

/**
 * Définition de la classe 'reunion_avis' (om_dbform).
 */
class reunion_avis extends reunion_avis_gen {


    /**
     * Définition des actions disponibles sur la classe.
     *
     * @return void
     */
    function init_class_actions() {
        // On récupère les actions génériques définies dans la méthode 
        // d'initialisation de la classe parente
        parent::init_class_actions();

        // ACTION - 001 - modifier
        //
        $this->class_actions[1]["condition"] = array("is_from_good_service", );

        // ACTION - 002 - supprimer
        //
        $this->class_actions[2]["condition"] = array("is_from_good_service", );
    }

    /**
     *
     */
    function set_form_specificity(&$form, $maj) {
        //
        parent::set_form_specificity($form, $this->getParameter("maj"));

        /**
         * Gestion du champ service
         */
        // 
        $this->set_form_specificity_service_common($form, $this->getParameter("maj"));
    }

    /**
     *
     */
    function setType(&$form, $maj) {
        //
        parent::setType($form, $maj);
        // Récupération du mode de l'action
        $crud = $this->get_action_crud($maj);
        // MODE AJOUTER
        if ($maj == 0 || $crud == 'create') {
            $form->setType("categorie", "select");
        }
        // MDOE MODIFIER
        if ($maj == 1 || $crud == 'update') {
            $form->setType("categorie", "select");
        }
        // MODE CONSULTER
        if ($maj == 3 || $crud == 'read') {
            $form->setType("categorie", "selectstatic");
        }
    }

    /**
     * Méthode qui effectue les requêtes de configuration des champs.
     *
     * @param object  $form Instance du formulaire.
     * @param integer $maj  Mode du formulaire.
     * @param null    $dnu1 @deprecated Ancienne ressource de base de données.
     * @param null    $dnu2 @deprecated Ancien marqueur de débogage.
     *
     * @return void
     */
    function setSelect(&$form, $maj, &$dnu1 = null, $dnu2 = null) {
        //
        parent::setSelect($form, $maj, $dnu1, $dnu2);
        //
        $contenu = array(
            "0" => array("", "Favorable", "Favorable avec réserve", "Défavorable", ),
            "1" => array("", "Favorable", "Favorable avec réserve", "Défavorable", ),
        );
        $form->setSelect("categorie", $contenu);
    }

    /**
     *
     */
    function setLib(&$form, $maj) {
        //
        parent::setLib($form, $maj);
        //
        $form->setLib("categorie", __("Correspondance référentiel ADS"));
    }

}
