<?php
/**
 * Ce script définit la classe 'contact_institutionnel'.
 *
 * @package openaria
 * @version SVN : $Id$
 */

require_once "../obj/contact.class.php";

/**
 * Définition de la classe 'contact_institutionnel' (om_dbform).
 *
 * Surcharge de la classe 'contact'.
 */
class contact_institutionnel extends contact {

    /**
     *
     */
    protected $_absolute_class_name = "contact_institutionnel";

    /**
     * Permet de définir le type des champs.
     *
     * @param object  &$form Instance du formulaire
     * @param integer $maj   Mode du formulaire
     *
     * @return void
     */
    function setType(&$form, $maj) {
        //
        parent::setType($form, $maj);

        //
        $form->setType('reception_convocation', 'hidden');

        // En ajout
        if ($maj == 0) {
            //
            $form->setType('contact_type', 'hidden');
            $form->setType('etablissement', 'hidden');
        }

        // En modification
        if ($maj == 1) {
            //
            $form->setType('contact_type', 'hidden');
            $form->setType('etablissement', 'hidden');
        }

        // En suppression
        if ($maj == 2) {
            //
            $form->setType('contact_type', 'hidden');
            $form->setType('etablissement', 'hidden');
        }

        // En consultation
        if ($maj == 3) {
            //
            $form->setType('contact_type', 'hidden');
            $form->setType('etablissement', 'hidden');
        }
    }

    /**
     * Permet de définir les valeurs des champs.
     *
     * @param object  $form       Objet formulaire
     * @param integer $maj        Mode du formulaire
     * @param integer $validation Validation du formulaire
     * @param null    $dnu1  @deprecated Ancienne ressource de base de données.
     * @param null    $dnu2  @deprecated Ancien marqueur de débogage.
     *
     */
    function setVal(&$form, $maj, $validation, &$dnu1 = null, $dnu2 = null) {
        //
        parent::setVal($form, $maj, $validation, $dnu1, $dnu2);

        //
        $form->setVal("contact_type", $this->get_contact_type_by_code('inst'));
    }

    /**
     * Permet de modifier le fil d'Ariane.
     * 
     * @param string $ent Fil d'Ariane
     *
     * @return string
     */
    function getFormTitle($ent) {
        // Identifiant de l'enregistrement
        $idx = trim($this->getVal($this->clePrimaire));

        // On n'affiche pas les informations d'un contact en mode ajout
        if ($this->getParameter("maj") != 0) {
            // Informations de l'enregistrement
            $idz = trim($this->get_contact_civilite_libelle($this->getVal('civilite'))." ".$this->getVal('nom')." ".$this->getVal('prenom'));

            //
            if (!empty($idx)) {
                // Ajoute l'identifiant
                $ent .= " -> ".$idx;
            }

            //
            if (!empty($idz)) {
                // Ajoute les informations
                $ent .= " ".$idz;
            }
        }

        // Change le fil d'Ariane
        return $ent;
    }

    /**
     * Récupère une liste des contacts institutionnels.
     *
     * @param integer $service                 Filtre par service
     * @param boolean $reception_programmation Filtre par réception des progs
     * @param boolean $reception_commission    Filtre par réception des réunions
     *
     * @return array
     */
    function get_contacts_institutionnels($service, $reception_programmation = false, $reception_commission = false) {
        // Initialisation du résultat retourné
        $return = array();

        // Si le code n'est pas vide
        if (!empty($service)) {

            // Requête SQL
            $sql = "SELECT contact.contact, contact.courriel
                    FROM ".DB_PREFIXE."contact
                        LEFT JOIN ".DB_PREFIXE."contact_type
                            ON contact.contact_type = contact_type.contact_type
                    WHERE LOWER(contact_type.code) = LOWER('INST')
                        AND contact.service = ".$service;
            //
            if ($reception_programmation == true) {
                //
                $sql .= " AND reception_programmation = 't'";
            }
            //
            if ($reception_commission == true) {
                //
                $sql .= " AND reception_commission = 't'";
            }
            //
            $res = $this->f->db->query($sql);
            $this->f->addToLog(__METHOD__."(): db->query(\"".$sql."\");", VERBOSE_MODE);
            $this->f->isDatabaseError($res);

            //
            while ($row =& $res->fetchrow(DB_FETCHMODE_ASSOC)) {
                //
                $return[] = $row;
            }
        }

        // Résultat retourné
        return $return;
    }

}

