<?php
/**
 * Ce script définit la classe 'om_table'.
 *
 * @package openaria
 * @version SVN : $Id$
 */

require_once PATH_OPENMAIRIE."om_table.class.php";

/**
 * Définition de la classe 'om_table' (om_table).
 *
 * Cette classe est destinée à permettre la surcharge de certaines méthodes de
 * la classe 'om_table' du framework pour des besoins spécifiques de
 * l'application.
 */
class om_table extends table {

    /**
     * Surcharge correction du bug du parameter maj qui n'était pas passé
     * à l'objet pour identifier que nous sommes dans le cas de la recherche avancée.
     *
     * @return string
     */
    private function gen_advs_id() {
        return str_replace(array('.',','), '', microtime(true));
    }

    /**
     * Surcharge correction du bug du parameter maj qui n'était pas passé
     * à l'objet pour identifier que nous sommes dans le cas de la recherche avancée.
     */
    protected function displayAdvancedSearch() {
        // Mode recherche avancée
        $maj = 999;

        /* Recuperation du nom de l'objet sur lequel la recherche seffectue */

        $form = $this->f->get_inst__om_formulaire(array(
            "validation" => 0,
            "maj" => $maj,
            "champs" => $this->htmlChampRechercheAv,
        ));

        /* Creation dun objet vide pour pouvoir creer facilement les champs de
           type select */

        $object = $this->f->get_inst__om_dbform(array(
            "obj" => $this->absolute_object,
            "idx" => "]",
        ));
        // SURCHARGE
        $object->setParameter("maj", $maj);
        // On appelle la méthode setSelect de l'objet
        $object->setSelect($form, $maj, $this->db, false);

        $paramChamp = $this->paramChampRechercheAv;

        // Affichage du formulaire
        echo "\t<form action=\"";
        $params = array(
            "validation" => 0,
            "premier" => 0,
            "advs_id"=> $this->gen_advs_id()
        );
        echo $this->composeURL($params);
        echo "\" method=\"post\" id=\"advanced-form\">\n";
        echo "\t\t<fieldset class=\"cadre ui-corner-all ui-widget-content adv-search-fieldset\">\n";
        echo "\t\t<legend id=\"toggle-advanced-display\" class=\"ui-corner-all ui-widget-content ui-state-active\">";
        echo __("Recherche");
        echo "\t\t</legend>";

        // construction du message d'aide
        $help_text = __('Utilisation de * pour zones de saisie').':';

        if ($this->wildcard['left'] == '') {
            $help_text .= " ".__("*ABC finit par 'ABC'.");
        }

        if ($this->wildcard['right'] == '') {
            $help_text .= " ".__("ABC* commence par 'ABC'.");
        }

        if ($this->wildcard['left'] == '' and $this->wildcard['right'] == '') {
            $help_text .= " ".__("*ABC* contient 'ABC'.");
        }

        $help_text .= " ".__("A*D peut correspondre a 'ABCD'.");

        if ($this->wildcard['left'] != '' and $this->wildcard['right'] != '') {
            $help_text .= " ".__("Par defaut * est ajoute au debut et a la fin des recherches.");
        } else {
            if ($this->wildcard['left'] != '') {
                $help_text .= " ".__("Par defaut * est toujours ajoute au debut des recherches.");
            }

            if ($this->wildcard['right'] != '') {
                $help_text .= " ".__("Par defaut * est toujours ajoute a la fin des recherches.");
            }
        }

        /* Affichage du widget de recherche multicriteres classique */

        echo "\t\t\t<div id=\"adv-search-classic-fields\">";

        echo "\t\t\t\t<div class=\"adv-search-widget\">\n";
        echo "\t\t\t\t\t<label>".__("Rechercher")."&nbsp;<input type=\"text\" name=\"recherche\" ";
        echo "value=\"".$this->getParam("recherche")."\" ";
        echo "class=\"champFormulaire\" /></label>\n";
        echo "\t\t\t\t</div>\n";

        echo "\t\t\t<p class=\"adv-search-helptext\">".$help_text."</p>";

        echo "\t\t\t</div>";

        /* Affichage des widgets de recherche avancee */

        echo "\t\t\t<div id=\"adv-search-adv-fields\">";

        foreach ($this->dbChampRechercheAv as $champ) {

            /* Gestion de l'affichage de deux champs HTML date pour pouvoir
               soumettre un intervalle. Les deux champs sont crees avec
               deux attributs "name" differents: le premier avec le suffixe
               "_min" et le second "_max", representant respectivement la date
               minimale et la date maximale. */

            $champs_html = array($champ);

            if ($paramChamp[$champ]["type"] == "date" and
                key_exists("where", $paramChamp[$champ]) and
                $paramChamp[$champ]["where"] == "intervaldate") {

                $champs_html = array($champ."_min", $champ."_max");

                /* Gestion de l'affichage de deux champs HTML checkbox pour pouvoir
                   soumettre des valeurs booleennes. Les deux champs sont crees avec
                   deux attributs "name" differents: le premier avec le suffixe
                   "_true" et le second "_false". */

            } elseif ($paramChamp[$champ]["type"] == "checkbox" and
                      key_exists("where", $paramChamp[$champ]) and
                      ($paramChamp[$champ]["where"] == "boolean")) {

                $champs_html = array($champ."_true", $champ."_false");
            }

            foreach ($champs_html as $champ_html) {

                $form->setType($champ_html, $paramChamp[$champ]["type"]);

                //// Gestion de l'affichage des libellés de critères de recherche
                // On initialise le libellé à vide.
                $libelle = "";
                // Le libellé standard est récupéré dans l'attribut "libellé"
                // si il existe.
                if (isset($paramChamp[$champ]["libelle"])) {
                    //
                    $libelle = $paramChamp[$champ]["libelle"];
                }
                // On ajoute un icône d'aide qui fait apparaître une explication
                // au survol de la souris . L'explication est récupérée dans
                // l'attribut "help" si il existe et n'est pas vide.
                if (isset($paramChamp[$champ]["help"])
                    && $paramChamp[$champ]["help"] != "") {
                    // On concatène le libellé avec l'icône et l'explication.
                    $libelle .= sprintf(
                        ' <span class="info-16" title="%s"></span>',
                        $paramChamp[$champ]["help"]
                    );
                }
                // libelle des intervales de date
                if ($paramChamp[$champ]['type'] == 'date' and
                    isset($paramChamp[$champ]['where']) and
                    $paramChamp[$champ]['where'] == 'intervaldate') {

                    // premier champ
                    if ($champ_html == $champ.'_min') {

                        $form->setBloc($champ_html, 'D',
                                       $libelle,
                                       'intervaldate');

                        // si lib1 n'existe pas, on utilise `du`
                        if (isset($paramChamp[$champ]['lib1'])) {
                            $form->setLib($champ_html,
                                          $paramChamp[$champ]['lib1']);
                        } else {
                            $form->setLib($champ_html, __('du'));
                        }
                    }

                    // second champ
                    if ($champ_html == $champ.'_max') {

                        $form->setBloc($champ_html, 'F');

                        // si lib2 n'existe pas, on utilise `au`
                        if (isset($paramChamp[$champ]['lib2'])) {
                            $form->setLib($champ_html,
                                          $paramChamp[$champ]['lib2']);
                        } else {
                            $form->setLib($champ_html, __('au'));
                        }
                    }

                } else {
                    $form->setLib($champ_html, $libelle);
                }

                if (isset($paramChamp[$champ]["taille"])) {
                     $form->setTaille($champ_html, $paramChamp[$champ]["taille"]);
                }

                if (isset($paramChamp[$champ]["max"])) {
                    $form->setMax($champ_html, $paramChamp[$champ]["max"]);
                }

                if ($paramChamp[$champ]["type"] == "select" and
                    key_exists("subtype", $paramChamp[$champ]) and
                    $paramChamp[$champ]["subtype"] == "manualselect") {
                    $form->setSelect($champ_html, $paramChamp[$champ]["args"]);
                }
                if ($paramChamp[$champ]["type"] == "select" and
                    key_exists("subtype", $paramChamp[$champ]) and
                    $paramChamp[$champ]["subtype"] == "sqlselect") {
                    $object->init_select($form, $this->db, 0, false, $champ,
                           $paramChamp[$champ]["sql"]);
                }

                if ($paramChamp[$champ]["type"] == "date") {
                    $form->setOnchange($champ_html, "fdate(this)");
                }

                if ($this->f->get_submitted_post_value($champ_html) != "") {
                    $form->setVal(
                        $champ_html,
                        $this->f->get_submitted_post_value($champ_html)
                    );
                }
            }

        }

        $form->entete();
        $form->afficher($this->htmlChampRechercheAv, 0, false, false);
        $form->enpied();

        /* Message d'aide */

        echo "<div class=\"visualClear\"></div>";
        echo "<p class=\"adv-search-helptext\">".$help_text."</p>";


        echo "\t\t\t</div>";

        /* Fin du fieldset recherche avancee */

        echo "\t\t</fieldset>\n";

        /* Affichage des boutons de controle du formulaire */

        // si une recherche avancee est faite
        if ($this->f->get_submitted_post_value("advanced-search-submit") !== null) {
            // on affiche la recherche avancee
            echo "\t\t<button type=\"submit\" id=\"adv-search-submit\" name=\"advanced-search-submit\">";

        } elseif ($this->f->get_submitted_post_value("classic-search-submit") !== null) {
            // si une recherche simple est faite on affiche la recherche simple
            echo "\t\t<button type=\"submit\" id=\"adv-search-submit\" name=\"classic-search-submit\">";

        } else {
            // si aucune recherche n'est faite, on affiche le formulaire
            // configure par defaut
            if ($this->advs_default_form == "advanced") {
                echo "\t\t<button type=\"submit\" id=\"adv-search-submit\" name=\"advanced-search-submit\">";
            } else {
                echo "\t\t<button type=\"submit\" id=\"adv-search-submit\" name=\"classic-search-submit\">";
            }
        }

        echo __("Recherche");
        echo "</button>\n";

        echo "\t\t<a href=\"#\" class=\"raz_advs\" onclick=\"clear_form($('#advanced-form'));\">";
        echo __("Vider le formulaire");
        echo "</a>";
        echo "\t</form>\n";
    }

    function process_global_action_validity() {
        //
        if ($this->_om_validite == true) { // si c'est un établissement

            if ($this->params['obj'] == 'etablissement_tous'
                || $this->params['obj'] == 'etablissement_referentiel_erp') {

                if ($this->getParam('valide') == 'false') {
                    $om_validite_message = __("Masquer les elements archives");
                    $params['valide'] = 'true';
                } else {
                    $om_validite_message = __("Afficher les elements archives");
                    $params['valide'] = 'false';
                }
                //
                $action = array(
                    "type" => "om_validite",
                    "class" => "om_validite",
                    "link" => $this->composeURL($params),
                    "title" => $om_validite_message,
                );
                //
                $this->add_action_to_global_actions($action);
            } else { // sinon c'est une table de référence
                //
                if ($this->getParam('valide') == 'false') {
                    $om_validite_message = __("Masquer les elements expires");
                    $params['valide'] = 'true';
                } else {
                    $om_validite_message = __("Afficher les elements expires");
                    $params['valide'] = 'false';
                }
                //
                $action_id = "action-";
                if ($this->getParam('onglet') === false) {
                    $action_id .= "tab-";
                } else {
                    $action_id .= "soustab-";
                }
                $action_id .= $this->getParam("obj")."-global-om_validite-".$params['valide'];
                //
                $action = array(
                    "id" => $action_id,
                    "type" => "om_validite",
                    "class" => "om_validite",
                    "link" => $this->composeURL($params),
                    "title" => $om_validite_message,
                );
                //
                $this->add_action_to_global_actions($action);
            }            
        }
    }


}

