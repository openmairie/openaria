<?php
/**
 * Ce script définit la classe 'modele_edition'.
 *
 * @package openaria
 * @version SVN : $Id$
 */

require_once "../gen/obj/modele_edition.class.php";

/**
 * Définition de la classe 'modele_edition' (om_dbform).
 */
class modele_edition extends modele_edition_gen {

    /**
     *
     * @return array
     */
    function get_var_sql_forminc__champs() {
        return array(
            "modele_edition",
            "code",
            "libelle",
            "courrier_type",
            "description",
            "om_lettretype_id",
            "om_validite_debut",
            "om_validite_fin"
        );
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_om_lettretype_id() {
        return "SELECT distinct om_lettretype.id, '['||om_lettretype.id||'] '||om_lettretype.libelle as lib FROM ".DB_PREFIXE."om_lettretype ORDER BY om_lettretype.id ASC";
    }

    /**
     * Permet de définir le type des champs.
     * 
     * @param object  &$form Instance du formulaire
     * @param integer $maj   Mode du formulaire
     *
     * @return void
     */
    function setType(&$form, $maj) {
        //
        parent::setType($form, $maj);
        //
        if ($maj == 0 || $maj == 1) {
            $form->setType("om_lettretype_id", "select");
        }
    }

    /**
     * @return void
     */
    function setLib(&$form, $maj) {
        //
        parent::setLib($form, $maj);
        //
        $form->setLib("om_lettretype_id", __("om_lettretype"));
    }

    /**
     * [get_om_lettretype_id description]
     *
     * @return [type] [description]
     */
    function get_om_lettretype_id() {
        $om_lettretype_id = "";
        if ($this->getVal("om_lettretype_id") !== "") {
            $om_lettretype_id = $this->getVal('om_lettretype_id');
        }
        return $om_lettretype_id;
    }

    /**
     * [get_modele_edition_by_code description]
     *
     * @param [type] $code [description]
     *
     * @return [type] [description]
     */
    function get_modele_edition_by_code($code) {
        //
        $modele_edition_id = "";
        //
        if (!empty($code)) {
            //
            $sql = "SELECT modele_edition
                    FROM ".DB_PREFIXE."modele_edition
                    WHERE LOWER(code) = LOWER('".$this->f->db->escapeSimple($code)."')";
            $modele_edition_id = $this->f->db->getOne($sql);
            $this->addToLog(__METHOD__."(): db->getone(\"".$sql."\")", VERBOSE_MODE);
            $this->f->isDatabaseError($modele_edition_id);
        }

        //
        return $modele_edition_id;
    }

    /**
     * Méthode qui effectue les requêtes de configuration des champs.
     *
     * @param object  $form Instance du formulaire.
     * @param integer $maj  Mode du formulaire.
     * @param null    $dnu1 @deprecated Ancienne ressource de base de données.
     * @param null    $dnu2 @deprecated Ancien marqueur de débogage.
     *
     * @return void
     */
    function setSelect(&$form, $maj, &$dnu1 = null, $dnu2 = null) {
        parent::setSelect($form, $maj, $dnu1, $dnu2);
        $this->init_select(
            $form,
            $this->f->db,
            $maj,
            null,
            "om_lettretype_id",
            $this->get_var_sql_forminc__sql("om_lettretype_id"),
            "SELECT null, null;",
            true
        );
    }

    /**
     *
     * @return void
     */
    function setLayout(&$form, $maj) {
        parent::setLayout($form, $maj);
        $form->setBloc($this->champs[0], "D", "", "form-modele_edition-action-".$maj);
        $form->setBloc(end($this->champs), "F");
    }

}

