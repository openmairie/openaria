<?php
/**
 * Ce script définit la classe 'courrier_suivi'.
 *
 * @package openaria
 * @version SVN : $Id$
 */

require_once "../obj/courrier.class.php";

/**
 * Définition de la classe 'courrier_suivi' (om_dbform).
 *
 * Surcharge de la classe 'courrier'.
 */
class courrier_suivi extends courrier {

    /**
     *
     */
    protected $_absolute_class_name = "courrier_suivi";

}

