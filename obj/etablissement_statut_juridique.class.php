<?php
/**
 * Ce script définit la classe 'etablissement_statut_juridique'.
 *
 * @package openaria
 * @version SVN : $Id$
 */

require_once "../gen/obj/etablissement_statut_juridique.class.php";

/**
 * Définition de la classe 'etablissement_statut_juridique' (om_dbform).
 */
class etablissement_statut_juridique extends etablissement_statut_juridique_gen {


    /**
     * Définition des actions disponibles sur la classe.
     *
     * @return void
     */
    function init_class_actions() {

        // On récupère les actions génériques définies dans la méthode 
        // d'initialisation de la classe parente
        parent::init_class_actions();

        // ACTION - 090 - get_code_statut_juridique
        //
        $this->class_actions[90] = array(
            "identifier" => "get_code_statut_juridique",
            "view" => "get_code_statut_juridique",
            "permission_suffix" => "json",
        );

    }

    /**
     * VIEW - get_code_statut_juridique.
     * 
     * Retourne en JSON le code du statut juridique posté.
     *
     * @return void
     */
    function get_code_statut_juridique() {
        // Désactive les logs
        $this->f->disableLog();

        $postvar = $this->getParameter("postvar");
        $id_statut = $postvar['id_statut'];
        
        $sql = "SELECT LOWER(code)
                FROM ".DB_PREFIXE."etablissement_statut_juridique
                WHERE etablissement_statut_juridique = ".$id_statut;
        $this->f->addToLog("get_code_statut_juridique() db->getOne(\"".$sql."\");",
            VERBOSE_MODE);
        $code = $this->f->db->getOne($sql);
        $this->f->isDatabaseError($code);

        // Traitement du retour :
        echo json_encode(array(
            "resultat"=>$code
            )
        );
    }

}

