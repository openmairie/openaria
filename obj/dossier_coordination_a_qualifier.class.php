<?php
/**
 * Ce script définit la classe 'dossier_coordination_a_qualifier'.
 *
 * @package openaria
 * @version SVN : $Id$
 */

require_once "../obj/dossier_coordination.class.php";

/**
 * Définition de la classe 'dossier_coordination_a_qualifier' (om_dbform).
 *
 * Surcharge de la classe 'dossier_coordination'. Cette classe permet d'afficher
 * directement seulement les dossiers de coordination à qualifier.
 */
class dossier_coordination_a_qualifier extends dossier_coordination {

    /**
     *
     */
    protected $_absolute_class_name = "dossier_coordination_a_qualifier";

}

