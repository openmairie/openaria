<?php
/**
 * Ce script définit la classe 'dossier_coordination_nouveau'.
 *
 * @package openaria
 * @version SVN : $Id$
 */

require_once "../obj/dossier_coordination.class.php";

/**
 * Définition de la classe 'dossier_coordination_nouveau' (om_dbform).
 *
 * Surcharge de la classe 'dossier_coordination'. Cette classe permet d'afficher
 * directement le formulaire en mode ajouter depuis le menu.
 */
class dossier_coordination_nouveau extends dossier_coordination {

    /**
     *
     */
    protected $_absolute_class_name = "dossier_coordination_nouveau";

    /**
     *
     */
    function get_back_link($view = "formulaire") {
        $get_back_link = parent::get_back_link($view);
        // Récupération de la validation
        $validation_parameter = $this->getParameter('validation');
        $validation = (isset($validation_parameter)) ? $validation_parameter : 0;
        // Récupération du mode du formulaire
        $maj_parameter = $this->getParameter('maj');
        $maj = (isset($maj_parameter)) ? $maj_parameter : 0;
        // Si on est en mode ajout et que le formulaire a été validé
        if ($maj == 0 && $validation > 0 && !empty($this->valF[$this->clePrimaire])
            && $this->correct == true) {
            //
            $get_back_link = str_replace(
                "obj=dossier_coordination_nouveau",
                "obj=dossier_coordination",
                $get_back_link
            );
            return $get_back_link;
        } elseif ($maj == 0) {
            //
            return OM_ROUTE_DASHBOARD;
        } else {
            //
            return $get_back_link;
        }
    }

}

