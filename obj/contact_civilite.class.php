<?php
/**
 * Ce script définit la classe 'contact_civilite'.
 *
 * @package openaria
 * @version SVN : $Id$
 */

require_once "../gen/obj/contact_civilite.class.php";

/**
 * Définition de la classe 'contact_civilite' (om_dbform).
 */
class contact_civilite extends contact_civilite_gen {


    /**
     *
     */
    function get_id_from_libelle($libelle) {
        $sql = sprintf(
            "SELECT contact_civilite FROM %scontact_civilite WHERE libelle='%s'",
            DB_PREFIXE,
            $this->f->db->escapeSimple($libelle)
        );
        $id = $this->f->db->getone($sql);
        $this->addToLog(__METHOD__."(): db->getone(\"".$sql."\");", VERBOSE_MODE);
        if ($this->f->isDatabaseError($id, true) === true) {
            return null;
        }
        return $id;
    }
}

