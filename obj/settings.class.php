<?php
/**
 * Ce script définit la classe 'settings'.
 *
 * @package openaria
 * @version SVN : $Id$
 */

/**
 * Définition de la classe 'settings'.
 *
 * Cette classe gère le module 'Settings'. Ce module permet de gérer les interfaces
 * d'administration et paramétrage et traitement.
 */
class settings {

    /**
     * Instance de la classe utils
     * @var resource
     */
    var $f = null;

    /**
     * Constructeur.
     */
    public function __construct() {
        //
        $this->init_om_utils();
    }

    /**
     *
     */
    function view_main() {
        //
        if (isset($_GET["controlpanel"]) 
            && $_GET["controlpanel"] == "filestorage") {
            $this->view_filestorage_settings();
        } elseif (isset($_GET["controlpanel"]) 
            && $_GET["controlpanel"] == "interface_avec_le_referentiel_ads") {
            $interface_referentiel_ads = $this->f->get_inst_referentiel_ads();
            $interface_referentiel_ads->view_controlpanel();
        } else {
            $this->view_menu();
        }
    }

    /**
     *
     */
    function view_menu() {
        //
        $this->f->setRight("settings");
        $this->f->setTitle(__("administration_parametrage")." -> ".__("menu"));
        $this->f->setFlag(null);
        $this->f->display();
        //
        printf('
        <form id="settings-live-search" action="" class="styled" method="post">
        <div class="filter-icon">
        <svg xmlns="http://www.w3.org/2000/svg" width="24px" viewBox="0 0 24 24" height="24px"><path d="M15.5 14h-.79l-.28-.27C15.41 12.59 16 11.11 16 9.5 16 5.91 13.09 3 9.5 3S3 5.91 3 9.5 5.91 16 9.5 16c1.61 0 3.09-.59 4.23-1.57l.27.28v.79l5 4.99L20.49 19l-4.99-5zm-6 0C7.01 14 5 11.99 5 9.5S7.01 5 9.5 5 14 7.01 14 9.5 11.99 14 9.5 14z"/><path fill="none" d="M0 0h24v24H0z"/></svg>
        <input type="text" class="text-input" id="filter" value="" placeholder="Rechercher" />
        </div>
        </form>');
        //
        $list = array();
        $new_list = null;
        //
        $menu_to_display = array();

        // On inclut le fichier de configuration du menu
        $menu = $this->f->get_config__menu();

        // Recuperation des variables
        $scriptAppele = explode("/", $_SERVER["PHP_SELF"]);
        $scriptAppele = $scriptAppele[ count($scriptAppele) - 1 ];
        $obj = (isset($_GET['obj']) ? $_GET['obj'] : "");

        //
        foreach ($menu as $m => $rubrik) {
            // Gestion des paramètres
            if (isset($rubrik["parameters"])
                && is_array($rubrik["parameters"])) {
                //
                $flag_parameter = false;
                //
                foreach ($rubrik["parameters"] as $parameter_key => $parameter_value) {
                    //
                    if ($parameter_key == "is_settings_view_enabled") {
                        continue;
                    }
                    //
                    if ($this->f->getParameter($parameter_key) != $parameter_value) {
                        //
                        $flag_parameter = true;
                        break;
                    }
                }
                //
                if ($flag_parameter == true) {
                    // On passe directement a l'iteration suivante de la boucle
                    continue;
                }
            }
            // Gestion des droits d'acces : si l'utilisateur n'a pas la
            // permission necessaire alors la rubrique n'est pas affichee
            if (!isset($rubrik['right']) 
                || ( $rubrik['right'] != "menu_parametrage"
                    && $rubrik['right'] != "menu_administration")) {
                // On passe directement a l'iteration suivante de la boucle
                continue;
            }
            // Initialisation
            $rubrik_to_display = $rubrik;
            $elems_in_rubrik_to_display = array();
            $cpt_links = 0;


            // Test des criteres pour determiner si la rubrique est active
            if (isset($rubrik['open'])) {
                foreach ($rubrik['open'] as $scriptobj) {
                    // separation du nom de fichier et du obj
                    $scriptobjarray = explode("|", $scriptobj);
                    $cle_script = $scriptobjarray[0];
                    $cle_obj = $scriptobjarray[1];

                    $cle_script_ok = true;
                    if ($cle_script != "" and $cle_script != $scriptAppele) {
                        $cle_script_ok = false;
                    }
                    $cle_obj_ok = true;
                    if ($cle_obj != "" and $cle_obj != $obj){
                            $cle_obj_ok = false;
                    }
                    if ($cle_obj_ok and $cle_script_ok){
                        $rubrik_to_display["selected"] = "selected";
                    }
                }
            }

            // Boucle sur les entrees de menu
            foreach ($rubrik['links'] as $link) {
                // Gestion des paramètres
                if (isset($link["parameters"])
                    && is_array($link["parameters"])) {
                    //
                    $flag_parameter = false;
                    //
                    foreach ($link["parameters"] as $parameter_key => $parameter_value) {
                        //
                        if ($this->f->getParameter($parameter_key) != $parameter_value) {
                            //
                            $flag_parameter = true;
                            break;
                        }
                    }
                    //
                    if ($flag_parameter == true) {
                        // On passe directement a l'iteration suivante de la boucle
                        continue;
                    }
                }
                // Gestion des droits d'acces : si l'utilisateur n'a pas la
                // permission necessaire alors l'entree n'est pas affichee
                if (isset($link['right'])
                    and !$this->f->isAccredited($link['right'], "OR")) {

                    // On passe directement a l'iteration suivante de la boucle
                    continue;

                }
                //
                $cpt_links++;

                // Entree de menu
                if (trim($link['title']) != "<hr />" and trim($link['title']) != "<hr/>"
                    and trim($link['title']) != "<hr>") {
                    // MENU OPEN
                    $link_actif = "";
                    if (isset($link['open'])) {
                        if (gettype($link['open']) == "string") {
                            $link['open'] = array($link['open'],);
                        }
    
                        foreach ($link['open'] as $scriptobj) {
                            // separation du nom de fichier et du obj
                            $scriptobjarray = explode("|", $scriptobj);
                            $cle_script = $scriptobjarray[0];
                            $cle_obj = $scriptobjarray[1];
    
                            $cle_script_ok = true;
                            if ($cle_script != "" and $cle_script != $scriptAppele) {
                                $cle_script_ok = false;
                            }
                            $cle_obj_ok = true;
                            if ($cle_obj != "" and $cle_obj != $obj) {
                                $cle_obj_ok = false;
                            }
                            if ($cle_obj_ok and $cle_script_ok){
                                $rubrik_to_display["selected"] = "selected";
                                $link["selected"] = "selected";
                            }
                        }
                    }
                }
                $elems_in_rubrik_to_display[] = $link;
            }
            
            //
            $rubrik_to_display["links"] = $elems_in_rubrik_to_display;
            // Si des liens ont ete affiches dans la rubrique alors on
            // affiche la rubrique
            if ($cpt_links != 0) {
                //
                $menu_to_display[] = $rubrik_to_display;
            }
        }
        //

        //

        foreach ($menu_to_display as $key => $value) {
            //
            if ($value["right"] == "menu_parametrage" || $value["right"] == "menu_administration") {
                //
                foreach ($value["links"] as $key1 => $value1) {
                    //
                    if (isset($value1["title"]) && $value1["title"] == "<hr/>") {
                        continue;
                    }
                    //
                    if (isset($value1["class"]) && $value1["class"] == "category") {
                        if ($new_list != null) {
                            $list[] = $new_list;
                        }
                        //
                        $new_list = array(
                            "title" => $value1,
                            "list" => array(),
                        );
                        //
                        continue;
                    }
                    //
                    if ($new_list === null) {
                        //
                        $new_list = array(
                            "title" => "",
                            "list" => array(),
                        );
                    } else {
                        $new_list["list"][] = $value1;
                    }
                }
            }
        }
        if ($new_list !== null) {
            $list[] = $new_list;
        }

        $settings = "";
        foreach ($list as $key => $value) {
            //
            $title = sprintf(
                '<div class="list-group-title">%s</div>',
                $value["title"]["title"]
            );
            $description = "";
            if (isset($value["title"]["description"])) {
                $description = $value["title"]["description"];
            }
            //
            $items = "";
            foreach ($value["list"] as $key2 => $value2) {
                //
                $items .= sprintf(
                '
                  <a href="%s" class="list-group-item %s">
                    <h4 class="list-group-item-heading">%s</h4>
                    <p class="list-group-item-text">%s</p>
                  </a>',
                  $value2["href"],
                  $value2["class"],
                  $value2["title"],
                  (isset($value2["description"]) ? $value2["description"] : "")
                );
            }
            //
            $settings .= sprintf(
                '<div class="item">
                    %s
                    %s
                    <div class="list-group">
                        %s
                    </div>
                </div>',
                $title,
                $description,
                $items
            );
        }
        //
        printf('
        <div id="settings" class="container-fluid">
        <div class="row">
        %s
        </div>
        </div>
            ',
            $settings
        );
    }

    /**
     *
     */
    function view_filestorage_settings() {
        //
        $this->f->setRight("settings");
        $this->f->setTitle(__("administration_parametrage")." -> ".__("filestorage"));
        $this->f->setFlag(null);
        $this->f->display();
        $this->f->displaySubTitle("FileFields");
        // Mapping des champs fichiers
        $filefields = array(
            "om_logo.fichier" => array(
                "label" => "logo",
                "table" => "om_logo",
                "field" => "fichier",
            ),
            "courrier.om_fichier_finalise_courrier" => array(
                "label" => "document généré finalisé",
                "table" => "courrier",
                "field" => "om_fichier_finalise_courrier",
            ),
            "courrier.om_fichier_signe_courrier" => array(
                "label" => "document généré numérisé signé",
                "table" => "courrier",
                "field" => "om_fichier_signe_courrier",
            ),
            "piece.uid" => array(
                "label" => "document entrant numérisé",
                "table" => "piece",
                "field" => "uid",
            ),
            "proces_verbal.om_fichier_signe" => array(
                "label" => "procès verbal numérisé ajouté",
                "table" => "proces_verbal",
                "field" => "om_fichier_signe",
            ),
            "reunion.om_fichier_reunion_odj" => array(
                "label" => "ordre du jour de réunion finalisé",
                "table" => "reunion",
                "field" => "om_fichier_reunion_odj",
            ),
            "reunion.om_fichier_reunion_cr_global" => array(
                "label" => "compte rendu global de réunion finalisé",
                "table" => "reunion",
                "field" => "om_fichier_reunion_cr_global",
            ),
            "reunion.om_fichier_reunion_cr_global_signe" => array(
                "label" => "compte rendu global de réunion numérisé signé",
                "table" => "reunion",
                "field" => "om_fichier_reunion_cr_global_signe",
            ),
            "reunion.om_fichier_reunion_cr_par_dossier_signe" => array(
                "label" => "ensemble des comptes rendus de réunion individuels numérisés signés",
                "table" => "reunion",
                "field" => "om_fichier_reunion_cr_par_dossier_signe",
            ),
        );
        //
        foreach ($filefields as $key => $value) {
            //
            $query = sprintf(
                'SELECT 
                    count(*)
                FROM
                    %1$s%2$s
                ;',
                DB_PREFIXE,
                $value["table"],
                $value["field"]
            );
            //
            $total_lines_in_table = $this->f->db->getone($query);
            $filefields[$key]["total_lines_in_table"] = $total_lines_in_table;
            //
            $query = sprintf(
                'SELECT 
                    count(*)
                FROM
                    %1$s%2$s
                WHERE
                    %3$s <> \'\'
                ;',
                DB_PREFIXE,
                $value["table"],
                $value["field"]
            );
            //
            $total_files_in_table = $this->f->db->getone($query);
            $filefields[$key]["total_files_in_table"] = $total_files_in_table;
        }
        //
        $table_template = '
        <table class="table table-condensed table-bordered table-striped table-hover">
            <tr><th>Label [table.champ]</th><th>Nb de fichiers</th><th>Nb de lignes dans la table</th></tr>
            %s
        </table>';
        //
        $line_template = '<tr><td>%s [%s]</td><td>%s</td><td>%s</td></tr>';
        //
        $table_content = '';
        foreach ($filefields as $key => $value) {
            $table_content .= sprintf(
                $line_template,
                $value["label"],
                $key,
                $value["total_files_in_table"],
                $value["total_lines_in_table"]
            );
        }
        //
        printf(
            $table_template,
            $table_content
        );
        //
        $this->f->displaySubTitle("FileStorage");
        //
        printf(
            '<pre>%s</pre>',
            print_r($this->f->filestorage_config, true)
        );
    }

    // {{{ BEGIN - UTILS, LOGGER, ERROR

    /**
     * Initialisation de la classe utils.
     *
     * Cette méthode permet de vérifier que l'attribut f de la classe contient
     * bien la ressource utils du framework et si ce n'est pas le cas de la
     * récupérer.
     *
     * @return boolean
     */
    function init_om_utils() {
        //
        if (isset($this->f) && $this->f != null) {
            return true;
        }
        //
        if (isset($GLOBALS["f"])) {
            $this->f = $GLOBALS["f"];
            return true;
        }
        //
        return false;
    }

    /**
     * Ajout d'un message au système de logs.
     *
     * Cette méthode permet de logger un message.
     *
     * @param string  $message Message à logger.
     * @param integer $type    Niveau de log du message.
     *
     * @return void
     */
    function addToLog($message, $type = DEBUG_MODE) {
        //
        if (isset($this->f) && method_exists($this->f, "elapsedtime")) {
            logger::instance()->log(
                $this->f->elapsedtime()." : class ".get_class($this)." - ".$message,
                $type
            );
        } else {
            logger::instance()->log(
                "X.XXX : class ".get_class($this)." - ".$message,
                $type
            );
        }
    }

    // }}} END - UTILS, LOGGER, ERROR

}

