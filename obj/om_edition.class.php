<?php
/**
 * Ce script définit la classe 'app_om_edition'.
 *
 * @package openaria
 * @version SVN : $Id$
 */

require_once PATH_OPENMAIRIE."om_edition.class.php";

/**
 * Définition de la classe 'app_om_edition' (om_edition).
 *
 * Cette classe est destinée à permettre la surcharge de certaines méthodes de
 * la classe 'om_edition' du framework pour des besoins spécifiques de
 * l'application.
 */
class app_om_edition extends edition {

    /**
     * Remplace dans la chaîne passée en paramètre les variables de substitutions
     * et les champs de fusion par leurs valeurs.
     *
     * Méthode spécifique à OM_ETAT et OM_LETTRETYPE.
     * La boucle est réalisée 5 fois pour permettre de remplacer les champs de
     * dans les variables de remplacement et inversement.
     *
     * @param string $bloc                     Chaîne de caractères.
     * @param array  $substitution_vars_values Tableau de valeurs des variables de
     *                                         remplacement.
     * @param array  $merge_fields_values Tableau de valeurs des champs de fusion.
     *
     * @return string
     */
    function replace_all_elements($bloc, $substitution_vars_values, $merge_fields_values) {
        //
        $bloc = parent::replace_all_elements($bloc, $substitution_vars_values, $merge_fields_values);
        /**
         * Ce script est utilisé pour remplacer la variable &contraintes_obj dans les éditions PDF
         * où obj peut prendre la valeur de 'dc' ou 'etab'.
         *
         * Le fait que le champ de fusion des contraintes soit paramétrable oblige à utiliser les
         * variables de remplacement. Les éditions concernées faisant appel aux requêtes objet
         * on se sert du fonctionnement de ces dernières pour tester et récupérer l'ID des objets.
         *
         * &contraintes_obj
         * Liste de toutes les contraintes du DC/établissement
         *
         * &contraintes_obj(liste_groupe=g1,g2...;liste_ssgroupe=sg1,sg2...)
         * Les options liste_groupe et liste_ssgroupe sont optionnelles et peuvent contenir une
         * valeur unique ou plusieurs valeurs separées par une virgule, sans espace.
         *
         * &contraintes_obj(affichage_sans_arborescence=t)
         * L'option affichage_sans_arborescence permet d'afficher une liste de contraintes sans
         * leurs groupes et sous-groupes, et sans puces. Il peut prendre t (Oui) ou f (Non) comme
         * valeur.
         */
        $useless = "";
        // Cas DC
        if (isset($merge_fields_values['dossier_coordination.dossier_coordination']) === true
            && empty($merge_fields_values['dossier_coordination.dossier_coordination']) === false) {
            $this->f->replace_contrainte('dc', $merge_fields_values['dossier_coordination.dossier_coordination'], $bloc, $useless);
        }
        // Cas établissement
        if (isset($merge_fields_values['etablissement.etablissement']) === true
            && empty($merge_fields_values['etablissement.etablissement']) === false) {
            $this->f->replace_contrainte('etab', $merge_fields_values['etablissement.etablissement'], $bloc, $useless);
        }
        // Purge des éventuelles variables de remplacement restantes
        $this->f->empty_contrainte('dc', $bloc, $useless);
        $this->f->empty_contrainte('etab', $bloc, $useless);
        //
        return $bloc;
    }

    /**
     * Surcharge de la méthode du core dans le but de stocker temporairement
     * l'id de l'édition en cours, afin de pouvoir le récupérer dans la méthode
     * 'get_substitution_vars_values', afin de l'affecter à la variable de
     * substitution '&lettre_type_identifiant'.
     *
     * @todo À corriger dès que possible (hack non pérenne).
     */
    function get_edition_from_collectivite($table, $id_edition, $id_collectivite, $idx_edition_direct_preview = null) {
        $this->_tmp_edition_courante_id = null;
        $edition = parent::get_edition_from_collectivite($table, $id_edition, $id_collectivite, $idx_edition_direct_preview);
        if (!empty($edition)) {
            $this->_tmp_edition_courante_id = $edition['id'];
        }
        return $edition;
    }

    /**
     * Surcharge de la méthode du core dans le but d'affecter l'id de la lettre
     * type courante à la variable de substitution '&lettre_type_identifiant',
     * stocké temporairement par la méthode 'get_edition_from_collectivite())'.
     *
     * @todo À corriger dès que possible (hack non pérenne).
     */
    function get_substitution_vars_values($om_collectivite_idx = null) {
        $substitution_vars_values = parent::get_substitution_vars_values($om_collectivite_idx);
        if (!empty($substitution_vars_values) && !empty($this->_tmp_edition_courante_id)) {
            $substitution_vars_values['lettre_type_identifiant'] = $this->_tmp_edition_courante_id;
        }
        return $substitution_vars_values;
    }
}
