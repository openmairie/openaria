<?php
/**
 * Ce script définit la classe 'treatment_exception'.
 *
 * @package openaria
 * @version SVN : $Id$
 */

/**
 * Définition de la classe 'treatment_exception'.
 *
 * Cette classe gèrant les erreurs rencontrées lors des traitements avec 
 * transaction.
 */
class treatment_exception extends Exception {

    /**
     * Construit l'exception
     *
     * @param string  $message  Le message de l'exception à lancer.
     * @param string  $log      Le message à logguer.
     */
    public function __construct($message, $log = null) {
        parent::__construct($message);
        if ($log !== null) {
            $this->log($log);
        }
    }

    /**
     * Cette fonction ajoute dans le message fourni dans le fichier de log.
     * 
     * @param string $log  Le message a logger.
     */
    private function log($log) {
        require_once PATH_OPENMAIRIE."om_logger.class.php";
        logger::instance()->log($log);
    }
}

