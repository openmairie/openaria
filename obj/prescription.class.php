<?php
/**
 * Ce script définit la classe 'prescription'.
 *
 * @package openaria
 * @version SVN : $Id$
 */

require_once "../gen/obj/prescription.class.php";

/**
 * Définition de la classe 'prescription' (om_dbform).
 */
class prescription extends prescription_gen {

    /**
     * @return array
     */
    protected function get_prescriptions_for_analyse($analyse_id) {
        if (isset($this->_prescriptions_for_analyse) === true
            && is_array($this->_prescriptions_for_analyse) === true
            && array_key_exists(intval($analyse_id), $this->_prescriptions_for_analyse) === true) {
            return $this->_prescriptions_for_analyse[$analyse_id];
        }
        //
        $query = sprintf(
            'SELECT
                ordre as ordre,
                CASE WHEN prescription_reglementaire IS NULL
                    THEN
                        \'\'
                    ELSE
                        pr_description_om_html
                END as prescription_reglementaire,
                ps_description_om_html as prescription_specifique
            FROM 
                %1$sprescription
            WHERE
                analyses = %2$s
            ORDER BY 
                ordre ASC',
            DB_PREFIXE,
            intval($analyse_id)
        );
        $res = $this->f->db->query($query);
        $this->f->addToLog(__METHOD__."(): db->query(\"".$query."\");", VERBOSE_MODE);
        if ($this->f->isDatabaseError($res, true)) {
            return null;
        }
        $this->_prescriptions_for_analyse[$analyse_id] = array();
        while ($row =& $res->fetchrow(DB_FETCHMODE_ASSOC)) {
            $this->_prescriptions_for_analyse[$analyse_id][] = $row;
        }
        return $this->_prescriptions_for_analyse[$analyse_id];
    }

    /**
     * Retourne un tableau de prescriptions pour une analyse donnée
     * ou une chaîne 'Aucune' si aucun résultat.
     * 
     * @param  [integer] $idx Valeur de la clé primaire de l'analyse
     * @return [string]       Code HTML du tableau
     */
    function get_html_output__prescriptions_for_analyse($analyse_id, $params = array()) {
        $prescriptions = $this->get_prescriptions_for_analyse($analyse_id);
        if ($prescriptions === null) {
            return "";
        }
        //
        $default_params = array(
            "separator" => true,
            "border" => true,
            "background" => true,
        );
        if (is_array($params) === false) {
            $params = $default_params;
        } else {
            foreach ($default_params as $key => $value) {
                if (array_key_exists($key, $params) === false) {
                    $params[$key] = $value;
                }
            }
        }
        // S'il n'y a aucun résultat
        if (count($prescriptions) == 0) {
            return __("Aucune");
        }

        /**
         * Template
         */
        //
        $template_table = '
        %s
        ';
        //
        $template_body_line_simple = '
        <table nobr="true" cellpadding="5">
            <tbody>
                <tr>
                    <td style="width:5%%;text-align:center;%1$s">%2$s</td>
                    <td style="width:95%%;%1$s">%3$s%4$s<</td>
                </tr>
            </tbody>
        </table>
        ';
        $template_body_line_double = '
        <table nobr="true" cellpadding="5">
            <tbody>
                <tr>
                    <td rowspan="2" style="width:5%%;text-align:center;%1$s">%2$s</td>
                    <td style="width:95%%;%1$s">%3$s</td>
                </tr>
                <tr>
                    <td style="width:95%%;%1$s">%4$s</td>
                </tr>
            </tbody>
        </table>
        ';
        // Style CSS
        $css_border = "";
        if ($params["border"] == true) {
            $css_border = "border: 0.5px solid #000;";
        }
        $css_bg_line_odd = "";
        if ($params["background"] == true) {
            $css_bg_line_odd = "background-color: #F9F9F9;";
        }
        $css_bg_line_even = "";

        /**
         * S'il y a au moins un résultat
         */
        //
        $i = 0;
        $table_body_content = "";
        foreach ($prescriptions as $key => $value) {
            //
            if ($i % 2 == 0) {
                $css_bg_line = $css_bg_line_even;
            } else {
                $css_bg_line = $css_bg_line_odd;
            }
            //
            if (strlen($value["prescription_reglementaire"]) == 0
                || strlen($value["prescription_specifique"]) == 0) {
                //
                $template = $template_body_line_simple;
            } else {
                if ($params["separator"] === true) {
                    $template = $template_body_line_double;
                } else {
                    $template = $template_body_line_simple;
                }
            }
            //
            $table_body_content .= sprintf(
                $template,
                $css_border.$css_bg_line,
                $value["ordre"],
                $value["prescription_reglementaire"],
                $value["prescription_specifique"]
            );
            //
            $i++;
        }
        //
        return sprintf(
            $template_table,
            $table_body_content
        );
    }

    /**
     * Retourne une liste de prescriptions défavorables pour une analyse donnée
     * ou une chaîne 'Aucune' si aucun résultat.
     * 
     * @param  [integer] $idx Valeur de la clé primaire de l'analyse
     * @return [string]       Code HTML du tableau
     */
    function get_prescription_defavorable_from_analyse($idx) {

        /**
         * Template
         */
        //
        $template_table = '
        <table style="%s">
            <tbody>
                %s
            </tbody>
        </table>
        ';
        //
        $template_body_line = '
        <tr>
            <td rowspan="2" style="width:5%%;text-align:center;%1$s">%2$s</td>
            <td style="width:95%%;%1$s">%3$s</td>
        </tr>
        <tr>
            <td style="width:95%%;%1$s">%4$s</td>
        </tr>
        ';
        // Style CSS
        $css_center = "text-align:center;";
        $css_border = "";
        $css_bg_head = "background-color: #D0D0D0;";
        $css_bg_line_odd = "";
        $css_bg_line_even = "";

        /**
         * Récupération des prescriptions
         */
        // Requête
        $sql = "
        SELECT
            ordre as ordre,
            CASE WHEN prescription_reglementaire IS NULL
                THEN
                    ''
                ELSE
                    pr_description_om_html
            END as prescription_reglementaire,
            ps_description_om_html as prescription_specifique
        FROM 
            ".DB_PREFIXE."prescription
        WHERE
            analyses = ".intval($idx)."
            AND prescription.pr_defavorable IS TRUE
        ORDER BY 
            ordre ASC
        ";
        // Exécution de la requête
        $res = $this->f->db->query($sql);
        $this->f->addToLog(__METHOD__."(): db->query(\"".$sql."\");", VERBOSE_MODE);
        // Traitement des erreurs de base de données
        if ($this->f->isDatabaseError($res, true)) {
            return "";
        }

        /**
         * S'il n'y a aucun résultat
         */
        //
        if ($res->numRows() == 0) {
            return __("Aucune");
        }

        /**
         * S'il y a au moins un résultat
         */
        //
        $i = 0;
        $table_body_content = "";
        while ($row =& $res->fetchRow(DB_FETCHMODE_ASSOC)) {
            //
            if ($i % 2 == 0) {
                $css_bg_line = $css_bg_line_even;
            } else {
                $css_bg_line = $css_bg_line_odd;
            }
            //
            $table_body_content .= sprintf(
                $template_body_line,
                $css_border.$css_bg_line,
                "-",
                $row["prescription_reglementaire"],
                $row["prescription_specifique"]
            );
            //
            $i++;
        }
        //
        return sprintf(
            $template_table,
            $css_border,
            $table_body_content
        );
    }
}

