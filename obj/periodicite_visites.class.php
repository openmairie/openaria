<?php
/**
 * Ce script définit la classe 'periodicite_visites'.
 *
 * @package openaria
 * @version SVN : $Id$
 */

require_once "../gen/obj/periodicite_visites.class.php";

/**
 * Définition de la classe 'periodicite_visites' (om_dbform).
 */
class periodicite_visites extends periodicite_visites_gen {

    /**
     * Définition des actions disponibles sur la classe.
     *
     * @return void
     */
    function init_class_actions() {

        // On récupère les actions génériques définies dans la méthode 
        // d'initialisation de la classe parente
        parent::init_class_actions();

        // ACTION - 010 - update_all_dossier_coordination_periodique
        // Cette action permet de mettre à jour tous les établissements
        $this->class_actions[10] = array(
            "identifier" => "update_all_dossier_coordination_periodique",
            "view" => "formulaire",
            "method" => "update_all_dossier_coordination_periodique",
            "button" => "update_all_dossier_coordination_periodique",
            "permission_suffix" => "treatment_periodicity",
        );

        // ACTION - 099 - controlpanel
        $this->class_actions[99] = array(
            "identifier" => "controlpanel",
            "view" => "view_controlpanel",
            "permission_suffix" => "controlpanel",
        );
    }

    /**
     * Récupère la valeur de la périodicité en années pour une entrée correspondant
     * au type, à la catégorie et au caractère locaux à sommeil de l'établissement
     * dans la matrice de la périodicité des visites.
     *
     * @param integer $etablissement_type           Type de l'établissement.
     * @param integer $etablissement_categorie      Catégorie de l'établissement.
     * @param boolean $etablissement_locaux_sommeil Caractère locaux à sommeil de l'établissement.
     *
     * @return integer|null Périodicité en années ou null si pas de résultat.
     */
    function get_periodicite_by_type_categorie_locaux_sommeil($etablissement_type, $etablissement_categorie, $etablissement_locaux_sommeil) {
        $ret = null;
        // Gestion du caractère locaux à sommeil
        if ($etablissement_locaux_sommeil === true) {
            $field_locaux_sommeil = "avec_locaux_sommeil";
        } elseif ($etablissement_locaux_sommeil === false) {
            $field_locaux_sommeil = "sans_locaux_sommeil";
        } else {
            return $ret;
        }
        // Si le type et la catégorie de l'établissement sont renseignés
        if (!empty($etablissement_type) && !empty($etablissement_categorie)) {
            // Requête SQL
            $sql = sprintf(
                'SELECT 
                    periodicite
                FROM 
                    %1$speriodicite_visites
                WHERE 
                    etablissement_type = %2$s
                    AND etablissement_categorie = %3$s
                    AND %4$s IS TRUE',
                DB_PREFIXE,
                intval($etablissement_type),
                intval($etablissement_categorie),
                $field_locaux_sommeil
            );
            $this->f->addToLog(__METHOD__."() : db->getOne(\"".$sql."\")", VERBOSE_MODE);
            $ret = $this->f->db->getOne($sql);
            $this->f->isDatabaseError($ret);
        }
        return $ret;
    }

    /**
     *
     */
    function setType(&$form, $maj) {
        //
        parent::setType($form, $maj);
        //
        if ($maj == 10) {
            //
            foreach ($this->champs as $key => $value) {
                $form->setType($value, 'hidden');
            }
        }
    }

    /**
     * SETTER_FORM - setLib.
     *
     * @return void
     */
    function setLib(&$form, $maj) {
        parent::setLib($form, $maj);
        //
        $form->setLib($this->clePrimaire, __("id"));
        $form->setLib("periodicite", __("périodicité (en années)"));
    }

    /**
     * TREATMENT - update_all_dossier_coordination_periodique.
     *
     * Traite tous les établissements concernant le dossier de coordination
     * de visites périodiques obligatoire sur les ERP Référentiel.
     *
     * @param array $val Valeurs soumises par le formulaire
     *
     * @return boolean
     */
    function update_all_dossier_coordination_periodique($val = array()) {
        // Begin
        $this->begin_treatment(__METHOD__);
        // Récupère la liste de tous les établissements
        $inst_util__etab = $this->f->get_inst__om_dbform(array(
            "obj" => "etablissement",
            "idx" => 0,
        ));
        $list_etablissement = $inst_util__etab->get_list_etablissement();
        $inst_util__etab->__destruct();
        // Initialisation des compteurs
        $cpt_updated = 0;
        $cpt_not_updated = 0;
        // On instancie chaque établissement de la liste pour y appeler
        // la méthode de gestion des visites périodiques
        foreach ($list_etablissement as $etablissement_id) {
            $inst__etab = $this->f->get_inst__om_dbform(array(
                "obj" => "etablissement",
                "idx" => $etablissement_id,
            ));
            $ret = $inst__etab->handle_dossier_coordination_periodique("all");
            if ($ret == true) {
                $cpt_updated++;
            } else {
                $cpt_not_updated++;
            }
            $inst__etab->__destruct();
        }
        // Affiche les messages à l'utilisateur
        $this->addToMessage(sprintf(__("Nombre d'etablissement total : %s"), "<span class='bold'>".count($list_etablissement)."</span>"));
        $this->addToMessage(sprintf(__("Nombre d'etablissement mise a jour : %s"), "<span class='bold'>".$cpt_updated."</span>"));
        $this->addToMessage(sprintf(__("Nombre d'etablissement ignore : %s"), "<span class='bold'>".$cpt_not_updated."</span>"));
        // Return
        return $this->end_treatment(__METHOD__, true);
    }


    /**
     *
     */
    function getFormTitle($ent) {
        if ($this->getParameter("maj") == 99) {
            return __("administration_parametrage")." -> ".__("Gestion de la périodicité");
        } else {
            return $ent;
        }
    }

    /**
     * VIEW - view_controlpanel.
     *
     * @return void
     */
    function view_controlpanel() {
        //
        $this->checkAccessibility();
        //
        $this->retour();
        /**
         * Traitements
         */
        //
        echo "<br/>";
        //
        echo "<br/>";
        $this->f->displaySubTitle("Traitement");
        printf(
            '
            <ul class="portlet-list">
            <li>
            <a 
            href="#"
            id="action-controlpanel-periodicite_visites-update_all_dossier_coordination_periodique"
            class="action action-direct action-with-confirmation"
            title="Ce traitement permet de mettre à jour la périodicité des visites pour tous les établissements et dossiers de coordination concernés"
            data-href="'.OM_ROUTE_FORM.'&obj=periodicite_visites&amp;action=10&amp;idx=0">
            <span class="om-prev-icon om-icon-16">Cliquer ici pour déclencher le traitement de périodicité</span>
            </a>
            </li>
            </ul>
            <br/>'
        );
        //
        echo "<br/>";
        $this->f->displaySubTitle("Configuration");
        //
        echo "<u>paramètres :</u>";
        $parametres = array(
            "etablissement_nature_periodique",
            "etablissement_etat_periodique",
            "etablissement_autorite_competente_periodique",
            "dossier_coordination_type_periodique",
        );
        $out = '';
        foreach ($parametres as $parametre) {
            $out .= sprintf(
                "- %s : <b>%s</b>\n",
                $parametre,
                var_export($this->f->getParameter($parametre), true)
            );
        }
        printf(
            '<pre>%s</pre>',
            $out
        );

        /**
         * Statistiques
         */
        //
        echo "<br/>";
        $this->f->displaySubTitle("Statistiques");
        //
        echo '<div id="content-accordion">';

        /**
         * Onglet - TABS-3
         * Deux statistiques dans cet onglet :
         * - Cette statistique permet d'afficher le nombre d'établissements
         * par nature et par état. Ce sont les deux critères primaires
         * indiquant que l'établissement peut être soumis à des visites
         * périodiques.
         * - Cette statistique permet d'afficher le nombre d'établissements
         * par type/catégorie/locaux à sommeil (filtrés sur les deux
         * critères primaires indiquant que l'établissement peut être
         * soumis à des visites périodiques).
         */
        //
        $etablissement_nb_total = 0;
        $etablissement_nb_criteres_periodiques_primaires = 0;
        $etablissement_nb_criteres_periodiques_primaires_et_secondaires = 0;
        //
        $sql = sprintf(
            'SELECT 
                etablissement_nature.code as etablissement_nature_code,
                etablissement_etat.code as etablissement_etat_code,
                autorite_competente.code as autorite_competente_visite_code,
                count(*)
            FROM
                %1$setablissement
                LEFT JOIN %1$setablissement_nature 
                    ON etablissement.etablissement_nature=etablissement_nature.etablissement_nature
                LEFT JOIN %1$setablissement_etat
                    ON etablissement.etablissement_etat=etablissement_etat.etablissement_etat
                LEFT JOIN %1$sautorite_competente
                    ON autorite_competente.autorite_competente=etablissement.si_autorite_competente_visite
            GROUP BY
                etablissement_nature.code,
                etablissement_etat.code,
                autorite_competente.code
            ORDER BY
                etablissement_nature.code DESC, etablissement_etat.code;',
            DB_PREFIXE
        );
        $res = $this->f->db->query($sql);
        $this->f->isDatabaseError($res);
        $results = array();
        while ($row =& $res->fetchrow(DB_FETCHMODE_ASSOC)) {
            $results[] = $row;
            $etablissement_nb_total += $row['count'];
        }
	    //
        $param__etablissement_autorite_competente_periodique = $this->f->get_param__etablissement_autorite_competente_periodique();
        $inst_util__om_formulaire = $this->f->get_inst__om_formulaire();
        //
        $table_template = '
        <table class="table table-condensed table-bordered table-striped table-hover">
            <tr><th>nature</th><th>état</th><th>autorité compétente</th><th>nombre d\'établissements</th></tr>
            %s
        </table>';
        //
        $line_template = '<tr class="%s"><td>%s</td><td>%s</td><td>%s</td><td class="nb-etab">%s</td></tr>';
        //
        $table_content = '';
        foreach ($results as $key => $value) {
            $attr_class = sprintf(
                '%s-%s-%s',
                $inst_util__om_formulaire->normalize_string($value['etablissement_nature_code']),
                $inst_util__om_formulaire->normalize_string($value['etablissement_etat_code']),
                $inst_util__om_formulaire->normalize_string($value['autorite_competente_visite_code'])
            );
            if ($value['etablissement_nature_code'] == $this->f->getParameter("etablissement_nature_periodique")
                && $value['etablissement_etat_code'] == $this->f->getParameter("etablissement_etat_periodique")
                && (count($param__etablissement_autorite_competente_periodique["codes"]) == 0
                    || in_array($value['autorite_competente_visite_code'], $param__etablissement_autorite_competente_periodique["codes"]))) {
                //
                $attr_class .= " danger";
            } else {
                $attr_class .= " default";
            }
            $table_content .= sprintf(
                $line_template,
                $attr_class,
                $value['etablissement_nature_code'],
                $value['etablissement_etat_code'],
                $value['autorite_competente_visite_code'],
                $value["count"]
            );
        }
        //
        $table_etablissement_par_criteres_primaires = sprintf(
            $table_template,
            $table_content
        );
        //
        $sql = sprintf(
            'SELECT 
                etablissement_type.libelle as etablissement_type_libelle,
                etablissement_categorie.libelle as etablissement_categorie_libelle,
                CASE WHEN etablissement.si_locaux_sommeil is true then \'avec\' else \'sans\' END as etablissement_locaux_sommeil,
                count(*) as count_total
            FROM
                %1$setablissement
                LEFT JOIN %1$setablissement_nature 
                    ON etablissement.etablissement_nature=etablissement_nature.etablissement_nature
                LEFT JOIN %1$setablissement_etat
                    ON etablissement.etablissement_etat=etablissement_etat.etablissement_etat
                LEFT JOIN %1$setablissement_type 
                    ON etablissement.etablissement_type=etablissement_type.etablissement_type
                LEFT JOIN %1$setablissement_categorie
                    ON etablissement.etablissement_categorie=etablissement_categorie.etablissement_categorie
                LEFT JOIN %1$sautorite_competente
                    ON etablissement.si_autorite_competente_visite=autorite_competente.autorite_competente
            WHERE
                etablissement_nature.code=\'%2$s\'
                AND  etablissement_etat.code=\'%3$s\'
                %4$s
            GROUP BY
                etablissement_type.libelle, etablissement_categorie.libelle, etablissement_locaux_sommeil
            ORDER BY
                etablissement_type.libelle, etablissement_categorie.libelle, etablissement_locaux_sommeil
                ;',
            DB_PREFIXE,
            $this->f->db->escapeSimple($this->f->getParameter("etablissement_nature_periodique")),
            $this->f->db->escapeSimple($this->f->getParameter("etablissement_etat_periodique")),
            $param__etablissement_autorite_competente_periodique["query_where_codes"]
        );
        $res = $this->f->db->query($sql);
        $this->f->isDatabaseError($res);
        $results = array();
        while ($row =& $res->fetchrow(DB_FETCHMODE_ASSOC)) {
            $results[] = $row;
            if ($this->has_periodic_config($row['etablissement_type_libelle'], $row['etablissement_categorie_libelle'], $row['etablissement_locaux_sommeil']) === true) {
                $etablissement_nb_criteres_periodiques_primaires_et_secondaires += $row['count_total'];
            }
            $etablissement_nb_criteres_periodiques_primaires += $row['count_total'];
        }
        //
        $table_template = '
        <table class="table table-condensed table-bordered table-striped table-hover">
            <tr><th>type</th><th>catégorie</th><th>locaux à sommeil</th><th>nombre d\'établissements</th></tr>
            %s
        </table>';
        //
        $line_template = '<tr class="%s"><td>%s</td><td>%s</td><td>%s</td><td>%s</td></tr>';
        //
        $table_content = '';
        foreach ($results as $key => $value) {
            $attr_class = sprintf(
                '%s-%s-%s',
                $inst_util__om_formulaire->normalize_string($value['etablissement_type_libelle']),
                $inst_util__om_formulaire->normalize_string($value['etablissement_categorie_libelle']),
                $inst_util__om_formulaire->normalize_string($value['etablissement_locaux_sommeil'])
            );
            if ($this->has_periodic_config($value['etablissement_type_libelle'], $value['etablissement_categorie_libelle'], $value['etablissement_locaux_sommeil']) === true) {
                $attr_class .= " danger";
            } else {
                $attr_class .= " default";
            }
            $table_content .= sprintf(
                $line_template,
                $attr_class,
                $value['etablissement_type_libelle'],
                $value['etablissement_categorie_libelle'],
                $value['etablissement_locaux_sommeil'],
                $value["count_total"]
            );
        }
        //
        $table_etablissement_par_criteres_secondaires = sprintf(
            $table_template,
            $table_content
        );
        //
        //
        $tabs3_title = __("Établissements par critères de périodicité");
        printf('<h3 id="onglet-etablissements-par-criteres-de-periodicite">%s</h3>', $tabs3_title);
        printf(
            '
            <div id="tabs-3">
                <h4 class="titre-nb-etab-total">> Nombre total d\'établissements : <span class="nb-etat-total">%1$s</span></h4>
                <br/>
                <p>Les critères primaires indiquant que l\'établissement peut être soumis à des visites périodiques sont : la nature, l\'état et l\'autorité compétente (visite SI) de l\'établissement.</p>
                %6$s
                <br/>
                <h4 class="titre-nb-etab-criteres-periodiques-primaires">> Nombre d\'établissements correspondant aux critères primaires "<span class="criteres-primaires">%2$s/%3$s/%8$s</span>" : <span class="nb-etab-criteres-periodiques-primaires">%4$s</span></h4>
                <br/>
                <p>Les critères secondaires indiquant que l\'établissement peut êtres soumis à des visites périodiques sont : le type, la catégorie et le caractère locaux à sommeil de l\'établissement.</p>
                %7$s
                <br/>
                <h4 class="titre-nb-etab-criteres-periodiques-primaires-et-secondaires">> Nombre d\'établissements correspondant aux critères primaires "<span class="criteres-primaires">%2$s/%3$s/%8$s</span>" et aux critères secondaires (type/catégorie/locaux à sommeil) du paramétrage de la périodicité des visites : <span class="nb-etab-criteres-periodiques-primaires-et-secondaires">%5$s</span></h4>
            </div>',
            $etablissement_nb_total,
            $this->f->getParameter("etablissement_nature_periodique"),
            $this->f->getParameter("etablissement_etat_periodique"),
            $etablissement_nb_criteres_periodiques_primaires,
            $etablissement_nb_criteres_periodiques_primaires_et_secondaires,
            //
            $table_etablissement_par_criteres_primaires,
            $table_etablissement_par_criteres_secondaires,
            implode(";", $param__etablissement_autorite_competente_periodique["codes"])
        );

        /**
         * Onglet - TABS-7
         */
        //
        $total = 0;
        $sql = sprintf(
            'SELECT 
                dossier_coordination.libelle as dc_libelle,
                dossier_coordination.etablissement as etablissement,
                etablissement.code as etablissement_code,
                etablissement.libelle as etablissement_libelle
            FROM
                %1$sdossier_coordination
                LEFT JOIN %1$sdossier_coordination_type
                    ON dossier_coordination.dossier_coordination_type=dossier_coordination_type.dossier_coordination_type
                LEFT JOIN %1$setablissement
                    ON dossier_coordination.etablissement=etablissement.etablissement
            WHERE
                dossier_coordination_type.code=\'%2$s\'
                AND dossier_coordination.dossier_cloture=\'f\'
                AND etablissement.dossier_coordination_periodique IS NULL
            ORDER BY
                dossier_coordination.libelle
                ;',
            DB_PREFIXE,
            $this->f->db->escapeSimple($this->f->getParameter("dossier_coordination_type_periodique"))
        );
        $res = $this->f->db->query($sql);
        $this->f->isDatabaseError($res);
        $results = array();
        while ($row =& $res->fetchrow(DB_FETCHMODE_ASSOC)) {
            $results[] = $row;
            $total += 1;
        }
        //
        $table_template = '
        <div id="tabs-7">
        Nombre de DC de visite périodique orphelins : %s
        <table class="table table-condensed table-bordered table-striped table-hover">
            <tr><th>libelle</th><th>etablissement</th></tr>
            %s
        </table>
        </div>';
        //
        $line_template = '<tr class="%s"><td>%s</td><td>%s - %s</td></tr>';
        //
        $table_content = '';
        foreach ($results as $key => $value) {
            $table_content .= sprintf(
                $line_template,
                "default",
                $value['dc_libelle'],
                $value['etablissement_code'],
                $value['etablissement_libelle']
            );
        }
        //
        $tabs7_title = sprintf(
            "DC %s non clôturés et non rattachés au champ périodique de l'établissement <span class='badge'>%s</span>",
            $this->f->getParameter("dossier_coordination_type_periodique"),
            $total
        );
        printf('<h3 id="onglet-dc-vps-non-cloturer-et-non-rattaches-au-champ-periodique-de-l-etablissement">%s</h3>', $tabs7_title);
        printf(
            $table_template,
            $total,
            $table_content
        );

        /**
         *
         */
        //
        echo "</div>";
        echo "<br/>";
        $this->retour();
    }

    /**
     * Retourne si une entrée correspond dans la matrice de périodicité ou non.
     *
     * @param string $type Libellé du type d'établissement : "G".
     * @param string $categorie Libellé de la catégorie de l'établissement : "1".
     * @param string $locaux_a_sommeil Caractère de locaux à sommeil de l'établissement : "avec" ou "sans".
     *
     * @return boolean
     */
    function has_periodic_config($type, $categorie, $locaux_a_sommeil) {
        // Composition d'un tableau représentant la matrice et on le stocke au
        // premier appel de la méthode pour ne pas avoir à le recalculer à
        // chaque interrogation.
        if (!isset($this->_all_periodic_config)) {
            $results = array();
            $sql = sprintf(
                'SELECT
                    etablissement_type.libelle as etablissement_type_libelle,
                    etablissement_categorie.libelle as etablissement_categorie_libelle,
                    \'avec\' as etablissement_locaux_sommeil,
                    periodicite_visites.periodicite_visites as id,
                    periodicite_visites.periodicite as frequence_annuelle
                FROM
                    %1$speriodicite_visites
                    LEFT JOIN %1$setablissement_type 
                        ON periodicite_visites.etablissement_type=etablissement_type.etablissement_type
                    LEFT JOIN %1$setablissement_categorie
                        ON periodicite_visites.etablissement_categorie=etablissement_categorie.etablissement_categorie
                WHERE
                    periodicite_visites.avec_locaux_sommeil IS TRUE
                ;',
                DB_PREFIXE
            );
            $res = $this->f->db->query($sql);
            $this->f->isDatabaseError($res);
            while ($row =& $res->fetchrow(DB_FETCHMODE_ASSOC)) {
                //
                $etab_type = $row["etablissement_type_libelle"];
                if (array_key_exists($etab_type, $results) === false) {
                    $results[$etab_type] = array();
                }
                //
                $etab_categorie = $row["etablissement_categorie_libelle"];
                if (array_key_exists($etab_categorie, $results[$etab_type]) === false) {
                    $results[$etab_type][$etab_categorie] = array();
                }
                //
                if (array_key_exists("avec", $results[$etab_type][$etab_categorie]) === false) {
                    $results[$etab_type][$etab_categorie]["avec"] = array();
                }
                //
                $results[$etab_type][$etab_categorie]["avec"][] = $row;
            }
            $sql = sprintf(
                'SELECT
                    etablissement_type.libelle as etablissement_type_libelle,
                    etablissement_categorie.libelle as etablissement_categorie_libelle,
                    \'sans\' as etablissement_locaux_sommeil,
                    periodicite_visites.periodicite_visites as id,
                    periodicite_visites.periodicite as frequence_annuelle
                FROM
                    %1$speriodicite_visites
                    LEFT JOIN %1$setablissement_type 
                        ON periodicite_visites.etablissement_type=etablissement_type.etablissement_type
                    LEFT JOIN %1$setablissement_categorie
                        ON periodicite_visites.etablissement_categorie=etablissement_categorie.etablissement_categorie
                WHERE
                    periodicite_visites.sans_locaux_sommeil IS TRUE
                ;',
                DB_PREFIXE
            );
            $res = $this->f->db->query($sql);
            $this->f->isDatabaseError($res);
            while ($row =& $res->fetchrow(DB_FETCHMODE_ASSOC)) {
                //
                $etab_type = $row["etablissement_type_libelle"];
                if (array_key_exists($etab_type, $results) === false) {
                    $results[$etab_type] = array();
                }
                //
                $etab_categorie = $row["etablissement_categorie_libelle"];
                if (array_key_exists($etab_categorie, $results[$etab_type]) === false) {
                    $results[$etab_type][$etab_categorie] = array();
                }
                //
                if (array_key_exists("sans", $results[$etab_type][$etab_categorie]) === false) {
                    $results[$etab_type][$etab_categorie]["sans"] = array();
                }
                //
                $results[$etab_type][$etab_categorie]["sans"][] = $row;
            }
            //
            $this->_all_periodic_config = $results;
        }
        //
        if (array_key_exists($type, $this->_all_periodic_config) === true
            && array_key_exists($categorie, $this->_all_periodic_config[$type])  === true
            && array_key_exists($locaux_a_sommeil, $this->_all_periodic_config[$type][$categorie]) === true) {
            //
            return true;
        }
        return false;
    }
}
