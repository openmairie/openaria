<?php
/**
 * Ce script définit la classe 'etablissement_unite'.
 *
 * @package openaria
 * @version SVN : $Id$
 */

require_once "../gen/obj/etablissement_unite.class.php";

/**
 * Définition de la classe 'etablissement_unite' (om_dbform).
 */
class etablissement_unite extends etablissement_unite_gen {


    /**
     * Définition des actions disponibles sur la classe.
     *
     * @return void
     */
    function init_class_actions() {

        // On récupère les actions génériques définies dans la méthode 
        // d'initialisation de la classe parente
        parent::init_class_actions();

        //
        $this->class_actions_available = $this->class_actions;

        // ACTION - 004 - analyser
        //
        $this->class_actions_available[4] = array(
            "identifier" => "analyser",
            "portlet" => array(
                "type" => "action-self",
                "libelle" => __("analyser"),
                "order" => 40,
                "class" => "zip-16",
            ),
            "view" => "formulaire",
            "method" => "analyser",
            "button" => "analyser",
            "permission_suffix" => "analyser",
            "condition" => array(
                "is_not_archived",
                "is_in_valid_state",
                "is_not_already_referenced_in_di",
            ),
        );

        // ACTION - 005 - archiver
        //
        $this->class_actions_available[5] = array(
            "identifier" => "archiver",
            "portlet" => array(
                "type" => "action-direct-with-confirmation",
                "libelle" => __("archiver"),
                "order" => 40,
                "class" => "zip-16",
            ),
            "view" => "formulaire",
            "method" => "archiver",
            "button" => "archiver",
            "permission_suffix" => "archiver",
            "condition" => array(
                "is_not_archived",
            ),
        );

        // ACTION - 006 - desarchiver
        //
        $this->class_actions_available[6] = array(
            "identifier" => "desarchiver",
            "portlet" => array(
                "type" => "action-direct-with-confirmation",
                "libelle" => __("desarchiver"),
                "order" => 50,
                "class" => "unzip-16",
            ),
            "view" => "formulaire",
            "method" => "desarchiver",
            "button" => "desarchiver",
            "permission_suffix" => "desarchiver",
            "condition" => array(
                "is_archived",
            ),
        );

        // ACTION - 007 - valider
        //
        $this->class_actions_available[7] = array(
            "identifier" => "valider",
            "portlet" => array(
                "type" => "action-direct-with-confirmation",
                "libelle" => __("valider"),
                "order" => 50,
                "class" => "unzip-16",
            ),
            "view" => "formulaire",
            "method" => "valider",
            "button" => "valider",
            "permission_suffix" => "valider",
            "condition" => array(
                "is_not_archived",
                "is_in_project_state",
            ),
        );


        // ACTION - 008 - repasser_en_projet
        //
        $this->class_actions_available[8] = array(
            "identifier" => "repasser_en_projet",
            "portlet" => array(
                "type" => "action-direct-with-confirmation",
                "libelle" => __("repasser_en_projet"),
                "order" => 50,
                "class" => "unzip-16",
            ),
            "view" => "formulaire",
            "method" => "repasser_en_projet",
            "button" => "repasser_en_projet",
            "permission_suffix" => "repasser_en_projet",
            "condition" => array(
                "is_not_archived",
                "is_in_valid_state",
                "is_not_used_as_reference",
            ),
        );


        // ACTION - 030 - Redirection vers le SIG
        $this->class_actions_available[30] = array(
            "identifier" => "localiser",
            "view" => "view_localiser",
            "permission_suffix" => "consulter",
        );


        /**
         *
         */
        //
        $this->class_actions = array();
        foreach (array(3, 30) as $key) {
            if (isset($this->class_actions_available[$key])) {
                $this->class_actions[$key] = $this->class_actions_available[$key];
            }
        }
    }

    /**
     *
     * @return array
     */
    function get_var_sql_forminc__champs() {
        return array(
            "etablissement_unite",
            //
            "libelle",
            "etat",
            "archive",
            "etablissement",
            "dossier_instruction",
            "etablissement_unite_lie",
            //
            "acc_handicap_auditif",
            "acc_handicap_mental",
            "acc_handicap_physique",
            "acc_handicap_visuel",
            "acc_ascenseur",
            "acc_elevateur",
            "acc_boucle_magnetique",
            "acc_sanitaire",
            "acc_douche",
            "acc_places_assises_public",
            "acc_chambres_amenagees",
            "acc_places_stationnement_amenagees",
            //
            "acc_derogation_scda",
            //
            "acc_descriptif_ua_om_html",
            "acc_notes_om_html",
            //
            "adap_date_validation",
            "adap_duree_validite",
            "adap_annee_debut_travaux",
            "adap_annee_fin_travaux",
            //
        );
    }


    /**
     * CONDITION - is_not_already_referenced_in_di.
     *
     * @return boolean
     */
    function is_not_already_referenced_in_di() {
        //
        $sql = "
        SELECT
            count(*)
        FROM
            ".DB_PREFIXE."etablissement_unite
        WHERE
            etablissement_unite.etablissement_unite_lie=".$this->getVal($this->clePrimaire)."
            and etablissement_unite.dossier_instruction=".$this->getParameter("idxformulaire")."
        ";
        $this->f->addToLog(__METHOD__.": db->getone(\"".$sql."\");", VERBOSE_MODE);
        $nb = $this->f->db->getone($sql);
        $this->f->isDatabaseError($nb);
        if ($nb != 0) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * CONDITION - is_not_used_as_reference.
     *
     * @return boolean
     */
    function is_not_used_as_reference() {
        //
        $sql = "
        SELECT
            count(*)
        FROM
            ".DB_PREFIXE."etablissement_unite
        WHERE
            etablissement_unite.etablissement_unite_lie=".$this->getVal($this->clePrimaire)."
        ";
        $this->f->addToLog(__METHOD__.": db->getone(\"".$sql."\");", VERBOSE_MODE);
        $nb = $this->f->db->getone($sql);
        $this->f->isDatabaseError($nb);
        if ($nb != 0) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * CONDITION - is_in_project_state.
     *
     * @return boolean
     */
    function is_in_project_state() {
        // Vérification de l'état 
        if ($this->getVal("etat") === 'enprojet') {
            return true;
        } else {
            return false;
        }
    }

    /**
     * CONDITION - is_in_valid_state.
     *
     * @return boolean
     */
    function is_in_valid_state() {
        // Vérification de l'état 
        if ($this->getVal("etat") === 'valide') {
            return true;
        } else {
            return false;
        }
    }

    /**
     * CONDITION - is_not_archived.
     *
     * @return boolean
     */
    function is_not_archived() {
        // Vérification de l'état d'archivage
        if ($this->getVal("archive") != 't') {
            return true;
        } else {
            return false;
        }
    }

    /**
     * CONDITION - is_archived.
     *
     * @return boolean
     */
    function is_archived() {
        // Vérification de l'état d'archivage
        if ($this->getVal("archive") == 't') {
            return true;
        } else {
            return false;
        }
    }

    /**
     * TREATMENT - repasser_en_projet.
     * 
     * Permet de modifier tous les champs de l'UA à l'exception des champs
     *
     * @param array $val Valeurs soumises par le formulaire
     *
     * @return boolean
     */
    function repasser_en_projet($val = array()) {
        // Begin
        $this->begin_treatment(__METHOD__);

        //
        $ret = $this->manage_state("repasser_en_projet", $val);
        // Si le traitement ne s'est pas déroulé correctement
        if ($ret !== true) {
            // Return
            return $this->end_treatment(__METHOD__, false);
        }

        // Return
        return $this->end_treatment(__METHOD__, true);
    }


    /**
     * TREATMENT - valider.
     * 
     * Permet de modifier tous les champs de l'UA à l'exception des champs
     *
     * @param array $val Valeurs soumises par le formulaire
     *
     * @return boolean
     */
    function valider($val = array()) {
        // Begin
        $this->begin_treatment(__METHOD__);

        //
        $ret = $this->manage_state("valider", $val);
        // Si le traitement ne s'est pas déroulé correctement
        if ($ret !== true) {
            // Return
            return $this->end_treatment(__METHOD__, false);
        }

        // Return
        return $this->end_treatment(__METHOD__, true);
    }

    /**
     * TREATMENT - analyser.
     * 
     * Permet de modifier tous les champs de l'UA à l'exception des champs
     *
     * @param array $val Valeurs soumises par le formulaire
     *
     * @return boolean
     */
    function analyser($val = array()) {
        // Begin
        $this->begin_treatment(__METHOD__);

        //
        $inst_ua = $this->f->get_inst__om_dbform(array(
            "obj" => "etablissement_unite",
            "idx" => "]",
        ));
        // Valeurs à modifier
        $val['etablissement'] = $this->getVal("etablissement");
        $val['dossier_instruction'] = $this->getParameter("idxformulaire");
        $val['etat'] = "enprojet";
        $val['etablissement_unite_lie'] = $this->getVal($this->clePrimaire);

        // Ajoute l'unité d'accessibilité et vérifie si cela a échoué
        $add = $inst_ua->ajouter($val);
        if ($add == false) {
            // Message d'erreur
            $this->correct = false;
            $this->addToMessage(
                sprintf(
                    __("%s"), 
                    $inst_ua->msg
                )
            );

            // Stop le traitement
            return false;
        } else {
                        $this->addToMessage(
                sprintf(
                    __("%s"), 
                    $inst_ua->msg
                )
            );
        }

        // Return
        return $this->end_treatment(__METHOD__, true);
    }

    /**
     * TREATMENT - archiver.
     * 
     * Permet de ...
     *
     * @param array $val Valeurs soumises par le formulaire
     *
     * @return boolean
     */
    function archiver($val = array()) {
        // Begin
        $this->begin_treatment(__METHOD__);

        //
        $ret = $this->manage_archiving("archive", $val);
        // Si le traitement ne s'est pas déroulé correctement
        if ($ret !== true) {
            // Return
            return $this->end_treatment(__METHOD__, false);
        }

        // Return
        return $this->end_treatment(__METHOD__, true);
    }

    /**
     * TREATMENT - desarchiver.
     * 
     * Permet de ...
     *
     * @param array $val Valeurs soumises par le formulaire
     *
     * @return boolean
     */
    function desarchiver($val = array()) {
        // Begin
        $this->begin_treatment(__METHOD__);

        //
        $ret = $this->manage_archiving("unarchive", $val);
        // Si le traitement ne s'est pas déroulé correctement
        if ($ret !== true) {
            // Return
            return $this->end_treatment(__METHOD__, false);
        }

        // Return
        return $this->end_treatment(__METHOD__, true);
    }

    /**
     *
     */
    function manage_state($mode = null, $val = array()) {
        // Logger
        $this->addToLog(__METHOD__."() - begin", EXTRA_VERBOSE_MODE);
        // Si le mode n'existe pas on retourne false
        if ($mode != "valider" && $mode != "repasser_en_projet") {
            return false;
        }
        // Récuperation de la valeur de la cle primaire de l'objet
        $id = $this->getVal($this->clePrimaire);
        // 
        if ($mode == "valider") {
            // Valeurs à modifier
            $valF = array(
                "etat" => 'valide',
            );
            //
            $valid_message = __("UA passée dans l'état 'validé'.");
            $trigger_mode = "archive";
        } elseif ($mode == "repasser_en_projet") {
            // Valeurs à modifier
            $valF = array(
                "etat" => 'enprojet',
            );
            //
            $valid_message = __("UA repassée dans l'état 'en projet'.");
            $trigger_mode = "unarchive";
        }
        //
        $this->correct = true;
        // Execution de la requête de modification des donnees de l'attribut
        // valF de l'objet dans l'attribut table de l'objet
        $res = $this->f->db->autoExecute(
            DB_PREFIXE.$this->table,
            $valF,
            DB_AUTOQUERY_UPDATE,
            $this->getCle($id)
        );
        // Si une erreur survient
        if (database::isError($res, true)) {
            // Appel de la methode de recuperation des erreurs
            $this->erreur_db($res->getDebugInfo(), $res->getMessage(), '');
            $this->correct = false;
            return false;
        } else {
            //
            $main_res_affected_rows = $this->f->db->affectedRows();
            // Log
            $this->addToLog(__("Requete executee"), VERBOSE_MODE);
            // Log
            $message = __("Enregistrement")."&nbsp;".$id."&nbsp;";
            $message .= __("de la table")."&nbsp;\"".$this->table."\"&nbsp;";
            $message .= "[&nbsp;".$main_res_affected_rows."&nbsp;";
            $message .= __("enregistrement(s) mis a jour")."&nbsp;]";
            $this->addToLog($message, VERBOSE_MODE);
            // Message de validation
            if ($main_res_affected_rows == 0) {
                $this->addToMessage(__("Attention vous n'avez fait aucune modification.")."<br/>");
            } else {
                $this->addToMessage($valid_message."<br/>");
            }
            //
            $val["trigger_mode"] = $trigger_mode;
            if($this->trigger_after_manage_state($id, $this->f->db, $val, null) === false) {
                $this->correct = false;
                $this->addToLog(__METHOD__."(): ERROR", DEBUG_MODE);
                // Return
                return false;
            }
        }
        // Logger
        $this->addToLog(__METHOD__."() - end", EXTRA_VERBOSE_MODE);
        //
        return true;
    }

    /**
     *
     * @param string $id
     * @param null &$dnu1 @deprecated Ancienne ressource de base de données.
     * @param array $val
     * @param null $dnu2 @deprecated Ancien marqueur de débogage.
     */
    function trigger_after_manage_state($id, &$dnu1 = null, $val = array(), $dnu2 = null) {
        // Logger
        $this->addToLog(__METHOD__."() - begin", EXTRA_VERBOSE_MODE);
        // Si aucune UA n'est référencée
        if ($this->getVal("etablissement_unite_lie") == "") {
            // Logger
            $this->addToLog(__METHOD__."() - aucune UA liée", EXTRA_VERBOSE_MODE);
            $this->addToLog(__METHOD__."() - end - return true;", EXTRA_VERBOSE_MODE);
            // Alors on retourne true pour dire que tout est ok
            return true;
        }
        //
        if (isset($val["trigger_mode"]) && $val["trigger_mode"] == "archive") {
            // Instance de la classe etablissement_unite
            $inst_ua = $this->f->get_inst__om_dbform(array(
                "obj" => "etablissement_unite__contexte_etab__ua_valide",
                "idx" => $this->getVal("etablissement_unite_lie"),
            ));
            if ($inst_ua->is_action_condition_satisfied(5)) {
                //
                $ret = $inst_ua->archiver();
                // Si le traitement ne s'est pas déroulé correctement
                if ($ret !== true) {
                    // Logger
                    $this->addToLog(__METHOD__."() - archiver KO", EXTRA_VERBOSE_MODE);
                    $this->addToLog(__METHOD__."() - end - return false;", EXTRA_VERBOSE_MODE);
                    // Return
                    return false;
                }
            }
            // Logger
            $this->addToLog(__METHOD__."() - archiver OK", EXTRA_VERBOSE_MODE);
            $this->addToLog(__METHOD__."() - end - return true;", EXTRA_VERBOSE_MODE);
            //
            return true;
        } elseif (isset($val["trigger_mode"]) && $val["trigger_mode"] == "unarchive") {
            // Instance de la classe etablissement_unite
            $inst_ua = $this->f->get_inst__om_dbform(array(
                "obj" => "etablissement_unite__contexte_etab__ua_archive",
                "idx" => $this->getVal("etablissement_unite_lie"),
            ));
            if ($inst_ua->is_action_condition_satisfied(6)) {
                //
                $ret = $inst_ua->desarchiver();
                // Si le traitement ne s'est pas déroulé correctement
                if ($ret !== true) {
                    // Logger
                    $this->addToLog(__METHOD__."() - desarchiver KO", EXTRA_VERBOSE_MODE);
                    $this->addToLog(__METHOD__."() - end - return false;", EXTRA_VERBOSE_MODE);
                    // Return
                    return false;
                }
            }
            // Logger
            $this->addToLog(__METHOD__."() - desarchiver OK", EXTRA_VERBOSE_MODE);
            $this->addToLog(__METHOD__."() - end - return true;", EXTRA_VERBOSE_MODE);
            //
            return true;
        }
        // Logger
        $this->addToLog(__METHOD__."() - end - return false;", EXTRA_VERBOSE_MODE);
        //
        return false;
    }

    /**
     *
     */
    function manage_archiving($mode = null, $val = array()) {
        // Logger
        $this->addToLog(__METHOD__."() - begin", EXTRA_VERBOSE_MODE);
        // Si le mode n'existe pas on retourne false
        if ($mode != "archive" && $mode != "unarchive") {
            return false;
        }
        // Récuperation de la valeur de la cle primaire de l'objet
        $id = $this->getVal($this->clePrimaire);
        // 
        if ($mode == "archive") {
            // Valeurs à modifier
            $valF = array(
                "archive" => 't',
            );
            //
            $valid_message = __("Archivage correctement effectue.");
        } elseif ($mode == "unarchive") {
            // Valeurs à modifier
            $valF = array(
                "archive" => 'f',
            );
            //
            $valid_message = __("Desarchivage correctement effectue.");
        }
        //
        $this->correct = true;
        // Execution de la requête de modification des donnees de l'attribut
        // valF de l'objet dans l'attribut table de l'objet
        $res = $this->f->db->autoExecute(
            DB_PREFIXE.$this->table,
            $valF,
            DB_AUTOQUERY_UPDATE,
            $this->getCle($id)
        );
        // Si une erreur survient
        if (database::isError($res, true)) {
            // Appel de la methode de recuperation des erreurs
            $this->erreur_db($res->getDebugInfo(), $res->getMessage(), '');
            $this->correct = false;
            return false;
        } else {
            //
            $main_res_affected_rows = $this->f->db->affectedRows();
            // Log
            $this->addToLog(__("Requete executee"), VERBOSE_MODE);
            // Log
            $message = __("Enregistrement")."&nbsp;".$id."&nbsp;";
            $message .= __("de la table")."&nbsp;\"".$this->table."\"&nbsp;";
            $message .= "[&nbsp;".$main_res_affected_rows."&nbsp;";
            $message .= __("enregistrement(s) mis a jour")."&nbsp;]";
            $this->addToLog($message, VERBOSE_MODE);
            // Message de validation
            if ($main_res_affected_rows == 0) {
                $this->addToMessage(__("Attention vous n'avez fait aucune modification.")."<br/>");
            } else {
                $this->addToMessage($valid_message."<br/>");
            }
        }
        // Logger
        $this->addToLog(__METHOD__."() - end", EXTRA_VERBOSE_MODE);
        //
        return true;
    }

    /**
     *
     */
    function get_list_for_etablissement($etablissement) {
        //
        $sql = "
        SELECT
            etablissement_unite.libelle as libelle,
            etablissement_unite.acc_descriptif_ua_om_html as description
        FROM
            ".DB_PREFIXE."etablissement_unite
        WHERE
            etablissement_unite.etablissement=".intval($etablissement)."
            AND etablissement_unite.etat='valide'
            AND etablissement_unite.archive IS FALSE
        ORDER BY
            etablissement_unite.libelle
        ";
        //
        $this->f->addToLog(__METHOD__.": db->query(\"".$sql."\");", VERBOSE_MODE);
        $res = $this->f->db->query($sql);
        $this->f->isDatabaseError($res);
        // Par défaut tableau de résultats vide
        $results = array();
        // Tant qu'il a des résultats
        while ($row =& $res->fetchRow(DB_FETCHMODE_ASSOC)) {
            $results[] = $row;
        }
        return $results;
    }

    /**
     *
     */
    function get_list_for_dossier_instruction($dossier_instruction) {
        //
        $sql = "
        SELECT
            etablissement_unite.libelle as libelle,
            etablissement_unite.acc_descriptif_ua_om_html as description
        FROM
            ".DB_PREFIXE."etablissement_unite
        WHERE
            etablissement_unite.dossier_instruction='".$this->f->db->escapeSimple($dossier_instruction)."'
        ORDER BY
            etablissement_unite.libelle
        ";
        //
        $this->f->addToLog(__METHOD__.": db->query(\"".$sql."\");", VERBOSE_MODE);
        $res = $this->f->db->query($sql);
        $this->f->isDatabaseError($res);
        // Par défaut tableau de résultats vide
        $results = array();
        // Tant qu'il a des résultats
        while ($row =& $res->fetchRow(DB_FETCHMODE_ASSOC)) {
            $results[] = $row;
        }
        return $results;
    }

    /**
     *
     */
    function setType(&$form, $maj) {

        /**
         * 
         */
        //
        switch ($maj) {
            case '4':
                parent::setType($form, '1');
                break;
            
            default:
                parent::setType($form, $maj);
                break;
        }

        /**
         *
         */
        //
        $form->setType('etablissement_unite', 'hidden');

        /**
         *
         */
        //
        if ($maj == 0) {
            $form->setType('archive', 'hidden');
        } elseif ($maj == 1 || $maj == 2 || $maj == 4) {
            $form->setType('archive', 'checkboxhiddenstatic');
        } elseif ($maj == 3 ) {
            $form->setType('etat', 'checkboxstatic');
        }

        /**
         *
         */
        //
        if ($maj == 0) {
            $form->setType('etat', 'hidden');
        } elseif ($maj == 1 || $maj == 2 || $maj == 4) {
            $form->setType('etat', 'selecthiddenstatic');
        } elseif ($maj == 3 ) {
            $form->setType('etat', 'selectstatic');
        }

        /**
         *
         */
        //
        if ($maj == 0) {
            $form->setType('etablissement_unite_lie', 'hidden');
        } elseif ($maj == 1|| $maj == 2 || $maj == 3|| $maj == 4) {
            if ($this->getVal("etablissement_unite_lie") !== "") {
                $form->setType('etablissement_unite_lie', 'link');
            } else {
                $form->setType('etablissement_unite_lie', 'hidden');
            }
        }

        /**
         * Gestion spécifique des booléens à trois valeurs
         */
        // 
        if ($maj == 0 || $maj == 1 || $maj == 4) {
            $form->setType('acc_handicap_physique', 'select');
            $form->setType('acc_handicap_auditif', 'select');
            $form->setType('acc_handicap_visuel', 'select');
            $form->setType('acc_handicap_mental', 'select');
            $form->setType('acc_ascenseur', 'select');
            $form->setType('acc_elevateur', 'select');
            $form->setType('acc_boucle_magnetique', 'select');
            $form->setType('acc_sanitaire', 'select');
            $form->setType('acc_douche', 'select');
        } elseif ($maj == 2 || $maj == 3) {
            $form->setType('acc_handicap_physique', 'selectstatic');
            $form->setType('acc_handicap_auditif', 'selectstatic');
            $form->setType('acc_handicap_visuel', 'selectstatic');
            $form->setType('acc_handicap_mental', 'selectstatic');
            $form->setType('acc_ascenseur', 'selectstatic');
            $form->setType('acc_elevateur', 'selectstatic');
            $form->setType('acc_boucle_magnetique', 'selectstatic');
            $form->setType('acc_sanitaire', 'selectstatic');
            $form->setType('acc_douche', 'selectstatic');
        }

        /**
         * Gestion spécifique du champ 'etablissement'
         */
        //
        if ($maj == 0) {
            $form->setType('etablissement', 'hidden');
        } elseif ($maj == 1 || $maj == 2 || $maj == 3 || $maj == 4) {
            $form->setType('etablissement', 'etablissement');
        }

        /**
         * Gestion spécifique du champ 'dossier_instruction'
         */
        //
        if ($maj == 0) {
            $form->setType('dossier_instruction', 'hidden');
        } elseif ($maj == 1 || $maj == 2 || $maj == 3 || $maj == 4) {
            if ($this->getVal("dossier_instruction") !== "") {
                $form->setType('dossier_instruction', 'link');
            } else {
                $form->setType('dossier_instruction', 'hidden');
            }
        }


        // Pour les actions supplémentaires qui utilisent la vue formulaire
        // il est nécessaire de cacher les champs ou plutôt de leur affecter un
        // type pour que l'affichage se fasse correctement
        if ($maj != 0 && $maj != 1 && $maj != 2 && $maj != 3 && $maj != 4) {
            //
            foreach ($this->champs as $champ) {
                $form->setType($champ, "hidden");
            }
        }

    }

    /**
     *
     */
    function setLib(&$form, $maj) {
        //
        parent::setLib($form, $maj);
        //
        $form->setLib('acc_sanitaire', __('sanitaire amenage'));
        $form->setLib('acc_douche', __('douche amenagee'));
        $form->setLib('acc_notes_om_html', __('notes'));
        //
        $form->setLib('adap_date_validation', __('Date de validation'));
        $form->setLib('adap_duree_validite', __('Durée de validité (en années)'));
        $form->setLib('adap_annee_debut_travaux', __('Année de début des travaux'));
        $form->setLib('adap_annee_fin_travaux', __('Année de fin des travaux'));
    }

    /**
     *
     */
    function setLayout(&$form, $maj) {
        //
        $form->setBloc('etablissement_unite','D', "","");
        //
        $form->setFieldset('etablissement_unite','D', "UA / Établissement", "col_12");
        $form->setFieldset('etablissement_unite_lie','F');
        //
        $form->setFieldset('acc_handicap_auditif','D',__('Amenagements'), "col_12");
        $form->setFieldset('acc_places_stationnement_amenagees','F');
        //
        $form->setFieldset('acc_derogation_scda','D',__('Details'), "col_12");
        $form->setFieldset('acc_notes_om_html','F');
        //
        $form->setFieldset('adap_date_validation','D',__('ADAP'), "col_12");
        $form->setFieldset('adap_annee_fin_travaux','F');
        //
        $form->setBloc('adap_annee_fin_travaux','F');
    }

    /**
     * Permet de modifier le fil d'Ariane.
     * 
     * @param string $ent Fil d'Ariane
     *
     * @return string
     */
    function getFormTitle($ent) {

        // Si différent de l'ajout
        if ($this->getParameter("maj") != 0) {

            // Si le champ libelle existe
            $libelle = trim($this->getVal("libelle"));
            if (!empty($libelle)) {
                $ent .= " -> ".$libelle;
            }
            //
            $inst_etablissement = $this->get_inst_etablissement();
            $ent .= " [".$inst_etablissement->getVal("code")." - ".$inst_etablissement->getVal("libelle")."]";
        }

        // Change le fil d'Ariane
        return $ent;
    }

    /**
     *
     */
    function setSelect(&$form, $maj, &$dnu1 = null, $dnu2 = null) {

        //
        switch ($maj) {
            case '4':
                parent::setSelect($form, '1');
                break;
            
            default:
                parent::setSelect($form, $maj);
                break;
        }

        // Ajout des tableaux des données pour les select
        $booleens = array(
            array("null", "t", "f"), 
            array(__("Choisir..."), __("Oui"), __("Non")),
        );

        // Transformation des champs de type booléen en select oui/non
        // au lieu de la case à cocher
        $form->setSelect("acc_handicap_physique", $booleens);
        $form->setSelect("acc_handicap_auditif", $booleens);
        $form->setSelect("acc_handicap_visuel", $booleens);
        $form->setSelect("acc_handicap_mental", $booleens);
        $form->setSelect("acc_ascenseur", $booleens);
        $form->setSelect("acc_elevateur", $booleens);
        $form->setSelect("acc_boucle_magnetique", $booleens);
        $form->setSelect("acc_sanitaire", $booleens);
        $form->setSelect("acc_douche", $booleens);

        // Paramètres envoyés au type link pour générer le lien
        // vers le formulaire de l'établissement
        if ($maj == 2 || $maj == 3 || $maj == 4 || $maj == 1) {

            //
            if ($this->getVal("dossier_instruction") !== "") {
                // Paramétres envoyés au type link dossier_instruction
                $params = array();
                $params['idx'] = $this->getVal("dossier_instruction");
                // Instance de la classe dossier_instruction
                $dossier_instruction = $this->f->get_inst__om_dbform(array(
                    "obj" => "dossier_instruction",
                    "idx" => $params['idx'],
                ));
                $params['obj'] =  "dossier_instruction";
                $params['libelle'] = $dossier_instruction->getVal("libelle");
                $form->setSelect("dossier_instruction", $params);
            }



            // Paramétres envoyés au type link UA
            if($form->val['etablissement_unite_lie'] != '') {
                $params = array();
                $params['idx'] = $form->val['etablissement_unite_lie'];
                // Instance de la classe etablissement_unite
                $etablissement_unite = $this->f->get_inst__om_dbform(array(
                    "obj" => "etablissement_unite",
                    "idx" => $params['idx'],
                ));
                $params['obj'] =  "etablissement_unite";
                $params['libelle'] = $etablissement_unite->getVal("libelle");
                $form->setSelect("etablissement_unite_lie", $params);
            }

            // Initialisation de données à vide
            $libelle = "";
            $values_etablissement = array(
                "code" => "",
                "libelle" => "",
                "adresse_numero" => "",
                "adresse_numero2" => "",
                "adresse_voie" => "",
                "adresse_complement" => "",
                "lieu_dit" => "",
                "boite_postale" => "",
                "adresse_cp" => "",
                "adresse_ville" => "",
                "adresse_arrondissement" => "",
                "cedex" => "",
                "etablissement_nature_libelle" => "",
                "etablissement_type_libelle" => "",
                "etablissement_categorie_libelle" => "",
                "si_locaux_sommeil" => "",
                "geolocalise" => "",
            );
            // Initialisation de l'objet
            $obj = "etablissement_tous";
            // Si le champ etablissement n'est pas vide
            if (!empty($form->val['etablissement'])) {
                // Instance de la classe etablissement
                $inst_etab = $this->get_inst_etablissement($form->val['etablissement']);
                // Récupère le libellé de l'établissement
                $libelle = $inst_etab->getVal("code")." - ".$inst_etab->getVal("libelle");
                // Récupère toutes les données nécessaires de l'établissement
                $values_etablissement = $inst_etab->get_data($form->val['etablissement']);
            }
            // Paramétres envoyés au type link et au type spécifique 
            // etablissement
            $params = array();
            $params['obj'] = $obj;
            $params['libelle'] = $libelle;
            $params['values'] = $values_etablissement;
            $form->setSelect("etablissement", $params);
        }

        // dans tous les modes dont recherche avancée
        if ($maj == 999 || $maj == 0 || $maj == 1 || $maj == 2 || $maj == 3 || $maj == 4) {
            // select de l'état
            $select_etat = array(
                0 => array("enprojet", "valide", ),
                1 => array(__("en projet"), __("validé")),
            );
            $form->setSelect("etat", $select_etat);
        } 
    }

    /**
     *
     */
    function setvalF($val = array()) {
        //
        parent::setvalF($val);
        
        // Surcharge des booléens transformés en select pour accepter le null
        $boolean_three_values_fields = array(
            "acc_handicap_physique", 
            "acc_handicap_auditif",
            "acc_handicap_visuel",
            "acc_handicap_mental",
            "acc_ascenseur",
            "acc_elevateur",
            "acc_boucle_magnetique",
            "acc_sanitaire",
            "acc_douche",
        );
        foreach ($boolean_three_values_fields as $field_id) {
            //
            switch ($val[$field_id]) {
                //
                case 1:
                case "t":
                case "Oui":
                    $this->valF[$field_id] = true;
                    break;
                //
                case "":
                case "null":
                case "NULL":
                    $this->valF[$field_id] = null;
                    break;
                //
                default:
                    $this->valF[$field_id] = false;
                    break;
            }
        }

        /**
         *
         */
        unset($this->valF["etat"]);
        unset($this->valF["archive"]);
        unset($this->valF["etablissement"]);
        unset($this->valF["dossier_instruction"]);
    }

    /**
     *
     */
    function setValFAjout($val = array()) {
        //
        parent::setvalFAjout($val);
        //
        $this->valF['etat'] = $val["etat"];
        $this->valF['archive'] = "f";
        if (!is_numeric($val['etablissement'])) {
            $this->valF['etablissement'] = ""; // -> requis
        } else {
            $this->valF['etablissement'] = $val['etablissement'];
        }
        if (!is_numeric($val['dossier_instruction'])) {
            $this->valF['dossier_instruction'] = NULL;
        } else {
            $this->valF['dossier_instruction'] = $val['dossier_instruction'];
        }
    }

    /**
     *  Permet de pré-remplir les valeurs des formulaires.
     *  
     * @param [object]   $form        formulaire
     * @param [integer]  $maj         mode
     * @param [integer]  $validation  validation
     */
    function set_form_default_values(&$form, $maj, $validation) {
        if($maj == 4) { 
            $form->setval('etat', 'enprojet');
        }
    }

    /**
     * Selon le champ et l'établissement passé en paramètre, on procède toujours
     * au même raisonnement. Pour toutes les UA de cet établissement :
     *
     * - s'il n'y a aucune UA alors retourne null ;
     * - sinon, si le champ vaut non pour au moins une UA alors retourne non ;
     * - sinon, si le champ vaut null pour au moins une UA alors retourne NC ;
     * - sinon retourne oui.
     * 
     * @param   [integer]  idx    Clé primaire de l'établissement
     * @param   [string]   field  Colonne de l'UA à tester
     * @return  [mixed]           null/Oui/Non/''
     */
    function get_general_value_switch_field_by_etablissement($idx, $field) {
        // On récupère toutes les valeurs du champ
        $sql = "SELECT ".$field.", etablissement_unite FROM ".DB_PREFIXE."etablissement_unite
        WHERE etat='valide' and etablissement=".$idx;
        $this->f->addToLog(__METHOD__.": db->query(\"".$sql."\");", VERBOSE_MODE);
        $res = $this->f->db->query($sql);
        $this->f->isDatabaseError($res);
        // Par défaut tableau de résultats vide
        $total = 0;
        // Par défaut pas de 'non'
        $non = false;
        // Par défaut pas de vide
        $empty = false;
        // Tant qu'il a des résultats
        while ($row =& $res->fetchRow(DB_FETCHMODE_ASSOC)) {
            if (empty($row[$field])) {
                $empty = true;
            }
            if ($row[$field] === 'f' || $row[$field] === 'false' || $row[$field] === false) {
                $non = true;
            }
            $total ++;
        }
        // Si aucun résultat retourne null
        if ($total === 0) {
            return null;
        }
        // Sinon si au moins un NON alors retourne NON
        if ($non === true) {
            return __('Non');
        }
        // Sinon si au moins un NULL alors retourne une chaîne vide
        if ($empty === true) {
            return '';
        }
        // Sinon retourne OUI
        return __('Oui');
    }

    /**
     * Pour un établissement donné, parmi toutes ses UA y'en a-t-il au moins une
     * qui possède une dérogation ?
     * 
     * @param   [string]   idx  Clé primaire de l'établissement
     * @return  [boolean]    
     */
    function is_any_ua_having_scda($idx) {
        // On compte le nombre d'UA qui possèdent une dérogation
        $sql = "SELECT count(*) FROM ".DB_PREFIXE."etablissement_unite
        WHERE etat='valide' and acc_derogation_scda IS NOT NULL and etablissement=".$idx;
        $this->f->addToLog(__METHOD__.": db->getOne(\"".$sql."\");", VERBOSE_MODE);
        $res = $this->f->db->getOne($sql);
        $this->f->isDatabaseError($res);
        if ($res > 0) {
            return true;
        }
        return false;
    }

    // {{{ START - Champs de fusion

    /**
     * Retourne une liste d'UA en fonction des paramètres donnés ou une chaîne
     * 'Aucune' si aucun résultat.
     * 
     * @return string
     */
    function get_display_for_merge_field($params) {

        /**
         * Template
         */
        //
        $template_table = '
        <table style="%s">
            <tbody>
                %s
            </tbody>
        </table>
        ';
        //
        $template_body_line = '
        <tr>
            <td style="width:5%%;text-align:center;%1$s">%2$s</td>
            <td style="width:95%%;text-align:justify;%1$s"><div>%3$s</div>%4$s</td>
        </tr>
        ';
        // Style CSS
        $css_center = "text-align:center;";
        $css_border = "";
        $css_bg_head = "background-color: #D0D0D0;";
        $css_bg_line_odd = "";
        $css_bg_line_even = "";

        /**
         * Récupération des UA
         */
        //
        $ua_list = array();
        //
        if (isset($params)
            && isset($params["obj"])
            && isset($params["idx"])
            && $params["idx"] != "") {
            //
            if ($params["obj"] == "etablissement") {
                //
                $ua_list = $this->get_list_for_etablissement($params["idx"]);
            } elseif ($params["obj"] == "dossier_instruction") {
                //
                $ua_list = $this->get_list_for_dossier_instruction($params["idx"]);
            }
        }

        /**
         * S'il n'y a aucun résultat
         */
        //
        if (count($ua_list) == 0) {
            return __("Aucune");
        }

        /**
         * S'il y a au moins un résultat
         */
        //
        $i = 0;
        $table_body_content = "";
        foreach ($ua_list as $key => $value) {
            //
            if ($i % 2 == 0) {
                $css_bg_line = $css_bg_line_even;
            } else {
                $css_bg_line = $css_bg_line_odd;
            }
            //
            $table_body_content .= sprintf(
                $template_body_line,
                $css_border.$css_bg_line,
                "-",
                $value["libelle"],
                $value["description"]
            );
            //
            $i++;
        }
        //
        return sprintf(
            $template_table,
            $css_border,
            $table_body_content
        );
    }

    // }}} END - Champs de fusion

    // {{{ START

    /**
     *
     */
    var $inst_etablissement = null;

    /**
     *
     */
    function get_inst_etablissement($etablissement = null) {
        //
        if (!is_null($etablissement)) {
            return $this->f->get_inst__om_dbform(array(
                "obj" => "etablissement",
                "idx" => $etablissement,
            ));
        }
        //
        if (is_null($this->inst_etablissement)) {
            //
            $etablissement = $this->getVal("etablissement");
            $this->inst_etablissement = $this->f->get_inst__om_dbform(array(
                "obj" => "etablissement",
                "idx" => $etablissement,
            ));
        }
        //
        return $this->inst_etablissement;
    }


    /**
     *
     */
    var $inst_dossier_instruction = null;

    /**
     *
     */
    function get_inst_dossier_instruction($dossier_instruction = null) {
        //
        if (!is_null($dossier_instruction)) {
            return $this->f->get_inst__om_dbform(array(
                "obj" => "dossier_instruction",
                "idx" => $dossier_instruction,
            ));
        }
        //
        if (is_null($this->inst_dossier_instruction)) {
            //
            $dossier_instruction = $this->getVal("dossier_instruction");
            $this->inst_dossier_instruction = $this->f->get_inst__om_dbform(array(
                "obj" => "dossier_instruction",
                "idx" => $dossier_instruction,
            ));
        }
        //
        return $this->inst_dossier_instruction;
    }

    // }}} END

    /**
     * VIEW - view_localiser
     * Redirige l'utilisateur vers le SIG externe.
     *
     * @return void
     */
    public function view_localiser() {

        $inst_etablissement = $this->get_inst_etablissement();
        // On définit la valeur du paramètre maj à 30, afin que l'action view_localiser 
        // (30) de la classe etablissement soit accessible
        $inst_etablissement->setParameter('maj', '30');
        $inst_etablissement->view_localiser();

    }

}

