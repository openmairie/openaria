<?php
/**
 * Ce script définit la classe 'piece_a_valider'.
 *
 * @package openaria
 * @version SVN : $Id$
 */

require_once "../obj/piece.class.php";

/**
 * Définition de la classe 'piece_a_valider' (om_dbform).
 *
 * Surcharge de la classe 'piece'. Cette classe permet d'afficher seulement les
 * pièces qui sont à valider, c'est-à-dire les pièces dont le statut est
 * "qualifié".
 */
class piece_a_valider extends piece {

    /**
     *
     */
    protected $_absolute_class_name = "piece_a_valider";

}

