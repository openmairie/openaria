<?php
/**
 * Ce script définit la classe 'contact'.
 *
 * @package openaria
 * @version SVN : $Id$
 */

require_once "../gen/obj/contact.class.php";

/**
 * Définition de la classe 'contact' (om_dbform).
 */
class contact extends contact_gen {

    var $required_field = array(
        "contact",
        "qualite"
    );


    /**
     * Définition des actions disponibles sur la classe.
     *
     * @return void
     */
    function init_class_actions() {

        // On récupère les actions génériques définies dans la méthode 
        // d'initialisation de la classe parente
        parent::init_class_actions();

        // ACTION - 080 - afficher_synthese
        //
        $this->class_actions[80] = array(
            "identifier" => "afficher_synthese",
            "view" => "afficherSyntheseAction",
            "permission_suffix" => "consulter",
        );

        // ACTION - 100 - add_form_overlay
        //
        $this->class_actions[100] = array(
            "identifier" => "add_form_overlay",
            "view" => "view_add_form_overlay",
            "permission_suffix" => "ajouter",
        );

    }

    /**
     *
     * @return array
     */
    function get_var_sql_forminc__champs() {
        return array(
            "contact",
            "etablissement",
            "contact_type",
            "service",
            "qualite",
            // personne morale
            "denomination",
            "raison_sociale",
            "siret",
            "categorie_juridique",
            // particulier
            "civilite",
            "nom",
            "prenom",
            "titre",
            // adresse
            "adresse_numero",
            "adresse_numero2",
            "adresse_voie",
            "adresse_complement",
            "lieu_dit",
            "boite_postale",
            "adresse_cp",
            "adresse_ville",
            "cedex",
            "pays",
            // coordonnées
            "telephone",
            "mobile",
            "fax",
            "courriel",
            "reception_convocation",
            "reception_programmation",
            "reception_commission",
            //
            "om_validite_debut",
            "om_validite_fin",
        );
    }

    /**
     *
     */
    function get_var_sql_forminc__sql_contact_type_whitout_expl_and_inst() {
        return "SELECT contact_type.contact_type, contact_type.libelle FROM ".DB_PREFIXE."contact_type WHERE LOWER(code) != LOWER('expl') AND LOWER(code) != LOWER('inst') AND ((contact_type.om_validite_debut IS NULL AND (contact_type.om_validite_fin IS NULL OR contact_type.om_validite_fin > CURRENT_DATE)) OR (contact_type.om_validite_debut <= CURRENT_DATE AND (contact_type.om_validite_fin IS NULL OR contact_type.om_validite_fin > CURRENT_DATE))) ORDER BY contact_type.libelle ASC";
    }

    /**
     *
     */
    function get_var_sql_forminc__sql_contact_type_whitout_expl_and_inst_by_id() {
        return "SELECT contact_type.contact_type, contact_type.libelle FROM ".DB_PREFIXE."contact_type WHERE LOWER(code) != LOWER('expl') AND contact_type = <idx>";
    }

    /**
     * Indique si la redirection vers le lien de retour est activée ou non.
     *
     * L'objectif de cette méthode est de permettre d'activer ou de désactiver
     * la redirection dans certains contextes.
     *
     * @return boolean
     */
    function is_back_link_redirect_activated() {
        // Si c'est un overlay
        if ($this->getParameter('retour') === 'overlay') {
            return false;
        }
        return true;
    }

    /**
     * SETTER_FORM - setMax.
     */
    function setMax(&$form, $maj) {
        parent::setMax($form, $maj);
        // On fixe le max à 9 caractères, le générateur ne gère pas correctement
        // le max sur un champs de type integer
        $form->setMax("adresse_numero", 9);
    }

    /**
     *
     */
    function setVal(&$form, $maj, $validation, &$dnu1 = null, $dnu2 = null) {
        //
        parent::setVal($form, $maj, $validation);
        //
        if ($validation == 0 && $maj == 0) {
                
            $form->setVal("adresse_ville", $this->f->getParameter("ville"));
            $form->setVal("pays", "France");
        }

        if ($maj == 2 || $maj == 3) {
            
            if ($form->val['qualite'] == 'particulier') {
                $form->setVal('qualite', __('particulier'));
            }
            if ($form->val['qualite'] == 'personne_morale') {
                 $form->setVal('qualite', __('personne morale'));
            }
            if ($form->val['cedex'] != '') {
                $form->setVal('cedex', __('Cedex').' '.$form->val['cedex']);
            }
            if ($form->val['boite_postale'] != '') {
                $form->setVal('boite_postale', __('BP').' '.$form->val['boite_postale']);
            }
        }
    }

    /**
     * Permet de définir les valeurs des champs en sous-formualire.
     *
     * @param object  $form             Instance du formulaire.
     * @param integer $maj              Mode du formulaire.
     * @param integer $validation       Validation du form.
     * @param integer $idxformulaire    Identifiant de l'objet parent.
     * @param string  $retourformulaire Objet du formulaire.
     * @param mixed   $typeformulaire   Type du formulaire.
     * @param null    $dnu1 @deprecated Ancienne ressource de base de données.
     * @param null    $dnu2 @deprecated Ancien marqueur de débogage.
     */
    function setValsousformulaire(&$form, $maj, $validation, $idxformulaire, $retourformulaire, $typeformulaire, &$dnu1 = null, $dnu2 = null) {
        //
        parent::setValsousformulaire($form, $maj, $validation, $idxformulaire, $retourformulaire, $typeformulaire, $dnu1, $dnu2);
        //
        if ($validation == 0 && $maj == 0) {
                
            $form->setVal("adresse_ville", $this->f->getParameter("ville"));
            $form->setVal("pays", "France");
        }

        if ($maj == 2 || $maj == 3) {
            
            if ($form->val['qualite'] == 'particulier') {
                $form->setVal('qualite', __('particulier'));
            }
            if ($form->val['qualite'] == 'personne_morale') {
                 $form->setVal('qualite', __('personne morale'));
            }
            if ($form->val['cedex'] != '') {
                $form->setVal('cedex', __('Cedex').' '.$form->val['cedex']);
            }
            if ($form->val['boite_postale'] != '') {
                $form->setVal('boite_postale', __('BP').' '.$form->val['boite_postale']);
            }
        }
    }// fin setValsousformulaire

    function setType(&$form,$maj) {
        parent::setType($form,$maj);

        if (!in_array($this->retourformulaire, $this->foreign_keys_extended['etablissement'])) {
            $form->setType('etablissement', 'selecthidden');
        }
        
        // En mode "ajouter"
        if ($maj == 0) {
            $form->setType('qualite', 'select');
        }

        // En mode "modifier"
        if ($maj == 1) {

            // S'il s'agit d'un exploitant on ne peut changer son type
            $code_contact_type = $this->get_contact_type_code($this->getVal("contact_type"));
            if ($code_contact_type == 'expl') {
                $form->setType('contact_type','selecthiddenstatic');
                $form->setType('qualite','selecthiddenstatic');
            } else {
                $form->setType('contact_type','select');
                $form->setType('qualite','select');
            }
        }

        // En modes "supprimer" et "consulter"
        if ($maj == 2 || $maj == 3) {

            if ($this->getVal("qualite") != 'personne_morale') {
                $form->setType('denomination', 'hidden');
                $form->setType('raison_sociale', 'hidden');
                $form->setType('siret', 'hidden');
                $form->setType('categorie_juridique', 'hidden');
            }
            $code_contact_type = $this->get_contact_type_code($this->getVal("contact_type"));
            if ($code_contact_type != 'inst') {
                $form->setType('service', 'selecthidden');
                $form->setType('reception_programmation', 'hidden');
                $form->setType('reception_commission', 'hidden');
            }
        }

        $form->setType('om_validite_debut', 'hiddendate');
        $form->setType('om_validite_fin', 'hiddendate');
        
	// La case "reception_convocation" est non modifiable si le contact est 
        // un exploitant
        if ($maj>0 && $this->get_contact_type_code($this->getVal("contact_type"))==="expl") {
            //
            $form->setType('reception_convocation', 'checkboxstatic');
        }
    }


    /**
     * Méthode qui effectue les requêtes de configuration des champs.
     *
     * @param object  $form Instance du formulaire.
     * @param integer $maj  Mode du formulaire.
     * @param null    $dnu1 @deprecated Ancienne ressource de base de données.
     * @param null    $dnu2 @deprecated Ancien marqueur de débogage.
     *
     * @return void
     */
    function setSelect(&$form, $maj, &$dnu1 = null, $dnu2 = null) {
        //
        parent::setSelect($form, $maj, $dnu1, $dnu2);
        // En mode "modifier"
        if ($maj == 1) {
            // Suppression du type exploitant en personnalisant le select
            // sauf si la personne en est un pour continuer à l'afficher
            $code_contact_type = $this->get_contact_type_code($this->getVal("contact_type"));
            if ($code_contact_type != 'expl') {
                // contact_type
                $this->init_select($form, $this->f->db, $maj, null, "contact_type", $this->get_var_sql_forminc__sql("contact_type_whitout_expl_and_inst"), $this->get_var_sql_forminc__sql("contact_type_whitout_expl_and_inst_by_id"), true);
            }
        }

        // En mode "ajouter"
        if ($maj == 0) {
            // Suppression du type exploitant en personnalisant le select
            // contact_type
            $this->init_select($form, $this->f->db, $maj, null, "contact_type", $this->get_var_sql_forminc__sql("contact_type_whitout_expl_and_inst"), $this->get_var_sql_forminc__sql("contact_type_whitout_expl_and_inst_by_id"), true);
        }

        // En modes "ajouter" et "modifier"
        if ($maj == 0 || $maj == 1) {

            /* qualité */
            $contenu = array();
            $contenu[0][0] = "particulier";
            $contenu[1][0] = __('particulier');
            $contenu[0][1] = "personne_morale";
            $contenu[1][1] = __('personne morale');
            $form->setSelect("qualite", $contenu);
        }
    }

    // Récupération du libellé du type de contact
    function get_contact_type_code($contact_type) {

        // Initialisation du résultat retourné
        $code = '';

        // Si contact_type n'est pas vide
        if ($contact_type != '') {

            // Requête SQL
            $sql = "
                SELECT LOWER(code)
                FROM ".DB_PREFIXE."contact_type
                WHERE contact_type = ".$contact_type;
            $code = $this->f->db->getOne($sql);
            $this->f->isDatabaseError($code);

        }

        // Résultat retourné
        return $code;
    }

    /**
     * Récupère l'identifiant du type du contact par son code.
     *
     * @param string $code Code du type du contact
     *
     * @return integer
     */
    function get_contact_type_by_code($code) {

        // Initialisation du résultat retourné
        $contact_type = "";

        // Si le code n'est pas vide
        if (!empty($code)) {

            // Requête SQL
            $sql = "SELECT contact_type
                    FROM ".DB_PREFIXE."contact_type
                    WHERE LOWER(code) = LOWER('".$this->db->escapeSimple($code)."')";
            $contact_type = $this->f->db->getOne($sql);
            $this->f->isDatabaseError($contact_type);
        }

        // Résultat retourné
        return $contact_type;
    }

    // Mise en page
    function setLayout(&$form, $maj) {

        $form->setBloc('contact','D');
            $form->setBloc('contact','D');
            $form->setBloc('qualite','F');
            $form->setFieldset('denomination','D',__('Informations'), "");  
            $form->setBloc('denomination','D', "", "personne_morale_fields"); 
            $form->setBloc('categorie_juridique','F',"", ""); 
            $form->setBloc('civilite','D', "", "particulier_fields"); 
            if ($maj == 2 || $maj == 3) {
                $form->setBloc('civilite','D','', 'group');
                $form->setBloc('prenom','F');
                $form->setBloc('titre','DF','', 'group');
            }
            $form->setBloc('titre','F',"", ""); 
            $form->setFieldset('titre','F');
            if ($maj == 0 || $maj == 1) {
                $form->setFieldset('adresse_numero','D',__('Adresse postale'),'');
                $form->setFieldset('pays','F');
            }      
            if ($maj == 2 || $maj == 3) {
                $form->setFieldset('adresse_numero','D',__('Adresse postale'),'');
                    $form->setBloc('adresse_numero','D','','group');
                    $form->setBloc('adresse_voie','F');
                    $form->setBloc('adresse_complement','DF');
                    $form->setBloc('lieu_dit','D','','group');
                    $form->setBloc('boite_postale','F');
                    $form->setBloc('adresse_cp','D','','group');
                    $form->setBloc('cedex','F');
                    $form->setBloc('pays','D');
                    $form->setBloc('pays','F');
                $form->setFieldset('pays','F');
            }
            $form->setFieldset('telephone','D',__('Autres coordonnees')); 
            $form->setFieldset('reception_commission','F'); 
        $form->setBloc('om_validite_fin','F'); 
    }

    // Libellés des champs
    function setLib(&$form,$maj) {
        parent::setLib($form,$maj);

        if ($maj == 0 || $maj == 1) {

            $form->setLib('nom',__("nom")." ".$form->required_tag);
            $form->setLib('denomination',__("denomination")." ".$form->required_tag);
            $form->setLib('raison_sociale',__("raison_sociale")." ".$form->required_tag);
        }

        if ($maj == 2 || $maj == 3) {

            $form->setLib('civilite','');
            $form->setLib('nom','');
            $form->setLib('prenom','');
            $form->setLib('titre','');
            $form->setLib('adresse_numero','');
            $form->setLib('adresse_numero2','');
            $form->setLib('adresse_type_voie','');
            $form->setLib('adresse_voie','');
            $form->setLib('adresse_complement','');
            $form->setLib('adresse_cp','');
            $form->setLib('adresse_ville','');
            $form->setLib('lieu_dit','');
            $form->setLib('boite_postale','');
            $form->setLib('cedex','');
            $form->setLib('pays','');
        }
    }

    /**
     * Récupère les données de l'enregistrement
     *
     * @param integer $id Identifiant
     *
     * @return array Liste des résultas
     */
    function get_data($id) {
        // Initialisation de la vatiable de résultat
        $values = array();
        // Si l'identifiant est renseigné
        if (!empty($id)) {
            // Récupère les données nécessaires
            $values = array(
                "contact_type" => $this->getVal("contact_type"),
                "civilite" => $this->getVal("civilite"),
                "nom" => $this->getVal("nom"),
                "prenom" => $this->getVal("prenom"),
                "titre" => $this->getVal("titre"),
                "telephone" => $this->getVal("telephone"),
                "mobile" => $this->getVal("mobile"),
                "fax" => $this->getVal("fax"),
                "courriel" => $this->getVal("courriel"),
                "adresse_numero" => $this->getVal("adresse_numero"),
                "adresse_numero2" => $this->getVal("adresse_numero2"),
                "adresse_voie" => $this->getVal("adresse_voie"),
                "adresse_complement" => $this->getVal("adresse_complement"),
                "adresse_cp" => $this->getVal("adresse_cp"),
                "adresse_ville" => $this->getVal("adresse_ville"),
                "lieu_dit" => $this->getVal("lieu_dit"),
                "boite_postale" => $this->getVal("boite_postale"),
                "cedex" => $this->getVal("cedex"),
                "pays" => $this->getVal("pays"),
                "qualite" => $this->getVal("qualite"),
                "denomination" => $this->getVal("denomination"),
                "raison_sociale" => $this->getVal("raison_sociale"),
                "siret" => $this->getVal("siret"),
                "categorie_juridique" => $this->getVal("categorie_juridique"),
            );

            // Récupère le libellé de la civilité
            if (!empty($values['civilite'])) {
                $sql_civilite_by_id = str_replace('<idx>', $values['civilite'], $this->get_var_sql_forminc__sql("civilite_by_id"));
                $res = $this->f->db->query($sql_civilite_by_id);
                $this->addToLog("setSelect(): db->query(".$sql_civilite_by_id.");", VERBOSE_MODE);
                $this->f->isDatabaseError($res);
                $row =& $res->fetchRow();
                $values['civilite'] = $row[1];
            }

            // Récupère le type
            if (!empty($values['contact_type'])) {
                $sql_contact_type_by_id = str_replace('<idx>', $values['contact_type'], $this->get_var_sql_forminc__sql("contact_type_by_id"));
                $res = $this->f->db->query($sql_contact_type_by_id);
                $this->addToLog("setSelect(): db->query(".$sql_contact_type_by_id.");", VERBOSE_MODE);
                $this->f->isDatabaseError($res);
                $row =& $res->fetchRow();
                $values['contact_type'] = $row[1];
            }

        }

        // Retourne le résultat
        return $values;
    }

    /**
     *
     */
    function verifier($val = array(), &$dnu1 = null, $dnu2 = null) {
        //
        parent::verifier($val);
        // le nom du particulier est obligatoire
        if($this->valF['qualite'] == "particulier" AND $this->valF['nom'] == "") {
            $this->correct = false;
            $this->addToMessage(__("Le champ")." <span class=\"bold\">".
                                __("nom")."</span> ".__("est obligatoire."));
        }

        // la dénomination ou la raison sociale est obligatoire pour une personne morale
        if($this->valF['qualite'] == "personne_morale" 
           AND $this->valF['denomination'] == "" 
                AND $this->valF['raison_sociale'] == "") {
            $this->correct = false;
            $this->addToMessage(__("Un des champs")." <span class=\"bold\">".
                                __("denomination")."</span> ou <span class=\"bold\">".
                                __("raison_sociale")."</span> ".__("doit etre rempli."));
        }
    }

    /*
     * Ajoute l'action javascript sur le select de la qualité
     */
    function setOnchange(&$form,$maj){
        parent::setOnchange($form,$maj);
        
        $form->setOnchange("qualite","verifierContactType();");
    }

    /**
     * VIEW - afficherSyntheseAction.
     * 
     * Synthèse des contacts pour le formulaire des dossiers de coordination
     *
     * @return void
     */
    function afficherSyntheseAction() {
        //
        $this->f->disableLog();
        //
        $postvar = $this->getParameter("postvar");
        $linkable = $postvar["linkable"];
        //
        $this->afficherSynthese($linkable);
    }

    /**
     * Synthèse des contacts pour le formulaire des dossiers de coordination
     */
    function afficherSynthese($linkable) {

        $id_contact= $this->getVal("contact");
        $values = $this->get_data($id_contact);

        // Conteneur du contact
        echo "<div class=\"contact col_3\" id=\"contact_".$id_contact."\">\n";
        echo "<div class=\"legend_synthese_contact\">\n";
        if($linkable) {
            echo "<a href=\"#\" onclick=\"removeContact(".$id_contact."); return false;\">
                <span class=\"contact_del om-icon om-icon-16 om-icon-fix delete-16\" title=\"".
                __("Supprimer le contact")."\">".__("Supprimer le contact")."</span>
                </a>";
        }
        if ($values['contact_type'] != '') {
            echo $values['contact_type'];
        } else {
            echo __("Contact");
        }
        echo "</div>\n";
        echo "<div class=\"synthese_contact\">\n";
        
        // Valeur de formulaire à retourner
        echo "<input type=\"hidden\" class=\"contact_id\" name=\"contact[]".
            "\" value=\"".$id_contact."\" />\n";
            
        // Lien de modification du contact
        if ($linkable) {
            echo "<a class=\"edit_contact\" href=\"#\"
                onclick=\"editContact(".$id_contact.");return false;\">\n";
        }

        $civilite = "";
        if(!empty($values['civilite'])) {
            $sql = "SELECT libelle FROM ".DB_PREFIXE."contact_civilite WHERE contact_civilite=".
                        $this->val[array_search('civilite', $this->champs)];
            $civilite = $this->f->db->getone($sql);
            $this->f->addToLog("afficherSyntheseAction() : db->getone(\"".$sql."\");", VERBOSE_MODE);
            if (database::isError($civilite)) {
                die();
            }
        }
        // Affichage des infos du contact
        if ($values['qualite'] == 'particulier') {
            echo trim($civilite." ".$values['nom']." ".$values['prenom'])."<br/>\n";
        } elseif ($values['qualite'] == 'personne_morale') {
            if ($values['nom'] != "" || $values['prenom'] != "") {
                echo $values['raison_sociale']." ".$values['denomination']." représenté(e) par ".trim($civilite." ".$values['nom']." ".$values['prenom'])."<br/>\n";
            } else {
                echo $values['raison_sociale']." ".$values['denomination']."<br/>\n";
            }
            echo (($values['siret'] != "")? 
            $values['siret']."<br/>\n" : "");
            echo (($values['categorie_juridique'] != "")? 
            $values['categorie_juridique']."<br/>\n" : "");
        }
        // Adresse : 1ère ligne
        echo (($values['adresse_numero'] != "")? 
            $values['adresse_numero'] : "").
        (($values['adresse_numero2'] != "")? 
            " ".$values['adresse_numero2'] : "").
        (($values['adresse_voie'] != "")? 
            " ".$values['adresse_voie'] : "");
        if ($values['adresse_numero'] != ''
            || $values['adresse_numero2'] != ''
            || $values['adresse_voie'] != '') echo "<br/>\n";
        // Adresse : 2ème ligne
        echo (($values['adresse_complement'] != "")? 
            $values['adresse_complement']."<br/>\n" : "");
        // Adresse : 3ème ligne
        echo (($values['lieu_dit'] != "")? 
            $values['lieu_dit'] : "").
        (($values['boite_postale'] != "")? 
            " ".__("BP")." ".$values['boite_postale'] : "");
        if ($values['lieu_dit'] != ''
            || $values['boite_postale'] != '') echo "<br/>\n";
        // Adresse : 4ème ligne
        echo (($values['adresse_cp'] != "")? 
            $values['adresse_cp'] : "").
        (($values['adresse_ville'] != "")? 
            " ".$values['adresse_ville'] : "").
        (($values['cedex'] != "")? 
            " ".__("Cedex")." ".$values['cedex'] : "");
        if ($values['adresse_cp'] != ''
            || $values['adresse_ville'] != ''
            || $values['cedex'] != '') echo "<br/>\n";
        // Adresse : 6ème ligne
        echo (($values['pays'] != "")? 
            $values['pays']."<br/>\n" : "");
        // Moyens de communication
        echo (($values['courriel'] != "")? 
            __("Mel.")." : ".$values['courriel']."<br/>\n" : "");
        echo (($values['telephone'] != "")? 
            __("Tel. fixe")." : ".$values['telephone'] : "").
        (($values['mobile'] != "")? 
            " ".__("Tel. portable")." : ".$values['mobile'] : "");        
        if ($linkable) {
            echo "</a>\n";
        }
        echo "</div>\n";
        echo "</div>\n";
    }

    /**
     * Ajout d'un champs caché permettant de linker l'id du contact
     * recement ajouté
     */
    function formSpecificContent($maj) {
        $id_contact = $this->getVal("contact");
        if(isset($this->valF["contact"]) AND !empty($this->valF["contact"])) {
            echo "<input id=\"id_retour\" name=\"idRetour\" type=\"hidden\" value=\"".
                    $this->valF["contact"]."\" />";
        } elseif(isset($id_contact) AND !empty($id_contact) AND $maj == 1) {
            echo "<input id=\"id_retour\" name=\"idRetour\" type=\"hidden\" value=\"".
                    $this->getVal("contact")."\" />";
        }
    }

    function setvalF($val = array()) {
        //
        parent::setValF($val);
        // Si le contact est un exploitant, reception_convocation est à vrai
        if ($this->get_contact_type_code($this->valF["contact_type"])==="expl") {
            $this->valF["reception_convocation"] = "t";
        }
    }

    /**
     * Récupère le libellé de la civilité.
     *
     * @param integer $contact_civilite_id Identifiant de la civilité
     *
     * @return string
     */
    function get_contact_civilite_libelle($contact_civilite_id) {
        // Initialisation du résultat
        $libelle = "";

        // Si l'identifiant de la civilité n'es pas vide
        if (!empty($contact_civilite_id)) {
            // Instance de la classe contact_civilite
            $contact_civilite = $this->f->get_inst__om_dbform(array(
                "obj" => "contact_civilite",
                "idx" => $contact_civilite_id,
            ));

            // Récupère le libellé
            $libelle = $contact_civilite->getVal('libelle');
        }

        // Retourne le libellé
        return $libelle;
    }

    var $inst_civilite = null;

    /**
     *
     */
    function get_inst_civilite($civilite = null) {
        //
        if (is_null($this->inst_civilite)) {
            //
            if (is_null($civilite)) {
                $civilite = $this->getVal("civilite");
            }
            //
            $this->inst_civilite = $this->f->get_inst__om_dbform(array(
                "obj" => "contact_civilite",
                "idx" => $civilite,
            ));
        }
        //
        return $this->inst_civilite;
    }

    /**
     *
     */
    var $merge_fields_to_avoid_obj = array(
        "contact",
        "etablissement",
        "reception_convocation",
        "service",
        "reception_programmation",
        "reception_commission",
    );

    /**
     * Surcharge de la récupération des libellés des champs de fusion
     * 
     * @return [array]  tableau associatif objet => champ de fusion => libellé
     */
    function get_labels_merge_fields() {
        //
        $labels = parent::get_labels_merge_fields();
        //
        $labels["contact"]["contact.label"] = __("nom du contact (particulier : 'civilité nom prénom' ou personne morale : 'raison sociale dénomination' ou personne morale avec représentant : 'raison sociale dénomination représenté(e) par civilité nom prénom')");
        // Adresse complète sur quatre lignes
        $labels["contact"]["contact.adresse_complete_sur_quatre_lignes"] = __("adresse complète sur quatre lignes");
        //
        return $labels;
    }

    /**
     *
     */
    function get_values_merge_fields() {
        //
        $values = parent::get_values_merge_fields();

        //
        $inst_civilite = $this->get_inst_civilite();
        $values["contact.civilite"] = $inst_civilite->getVal("libelle");
        //
        if ($this->getVal("cedex") == "") {
            $values["contact.cedex"] = "";
        } else {
            $values["contact.cedex"] = "cedex ".$this->getVal("cedex");
        }
        //
        if ($this->getVal('qualite') == 'particulier') {
            $values["contact.label"] = trim($values["contact.civilite"]." ".$values["contact.nom"]." ".$values["contact.prenom"]);
        } elseif (trim($values["contact.civilite"]." ".$values["contact.nom"]." ".$values["contact.prenom"]) != "") {
            $values["contact.label"] = trim($values["contact.raison_sociale"]." ".$values["contact.denomination"]." représenté(e) par ".$values["contact.civilite"]." ".$values["contact.nom"]." ".$values["contact.prenom"]);
        } else {
            $values["contact.label"] = trim($values["contact.raison_sociale"]." ".$values["contact.denomination"]);
        }
        // Adresse complète sur quatre lignes
        $table = $this->table;
        $values[$table.'.adresse_complete_sur_quatre_lignes'] = nl2br($this->get_formatted_address(array(
            "format" => "4_lines",
            "complement" => $this->getVal('adresse_complement'),
            "numero" => $this->getVal('adresse_numero'),
            "numero2" => $this->getVal('adresse_numero2'),
            "voie" => $this->getVal('adresse_voie'),
            "lieu_dit" => $this->getVal('lieu_dit'),
            "boite_postale" => $this->getVal('boite_postale'),
            "cp" => $this->getVal('adresse_cp'),
            "ville" => $this->getVal('adresse_ville'),
            "cedex" => $this->getVal('cedex'),
        )));
        //
        return $values;
    }


    /**
     * [view_add_form_overlay description]
     *
     * @return [type] [description]
     */
    public function view_add_form_overlay() {
        // Vérification de l'accessibilité sur l'élément
        $this->checkAccessibility();

        // Objet à charger
        $obj = "contact";
        //
        if ($this->getParameter('retourformulaire') === 'dossier_coordination') {
            //
            $obj = 'contact_contexte_dossier_coordination';
        }
        // Construction de l'url de sousformulaire à appeler
        $url = OM_ROUTE_SOUSFORM."&obj=".$obj;
        $url .= "&action=0";
        $url .= "&retourformulaire=".$this->getParameter('retourformulaire');
        $url .= "&idxformulaire=".$this->getParameter('idxformulaire');
        $url .= "&retour=overlay";
        // Affichage du container permettant le reffraichissement du contenu
        // dans le cas des action-direct.
        printf('
            <div id="sousform-href" data-href="%s">
            </div>',
            $url
        );
        // Affichage du container permettant de charger le retour de la requête
        // ajax récupérant le sous formulaire.
        printf('
            <div id="sousform-%s">
            </div>
            <script>
            ajaxIt(\'%s\', \'%s\');
            </script>',
            $obj,
            $obj,
            $url
        );
    }


    /**
     * Permet de modifier le fil d'Ariane depuis l'objet pour un
     * sous-formulaire.
     *
     * @param string $subEnt Fil d'Ariane récupéréré.
     *
     * @return string
     */
    public function getSubFormTitle($subEnt) {
        // Si c'est l'action 100 ou que l'action 0 est ouvert dans un overlay
        if ($this->getParameter('maj') === '100'
            || ($this->getParameter('maj') === 0
                && $this->getParameter('retour') === 'overlay')) {
            // On n'affiche pas de fil d'Ariane
            $subEnt = '';
        }

        //
        return $subEnt;
    }


}

