<?php
/**
 * Ce script définit la classe 'dossier_instruction_tous_plans'.
 *
 * @package openaria
 * @version SVN : $Id$
 */

require_once "../obj/dossier_instruction.class.php";

/**
 * Définition de la classe 'dossier_instruction_tous_plans' (om_dbform).
 *
 * Surcharge de la classe 'dossier_instruction'.
 */
class dossier_instruction_tous_plans extends dossier_instruction {

    /**
     *
     */
    protected $_absolute_class_name = "dossier_instruction_tous_plans";

}

