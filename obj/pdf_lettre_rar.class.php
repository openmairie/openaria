<?php
/**
 * Ce script définit la classe 'pdf_lettre_rar'.
 *
 * @package openaria
 * @version SVN : $Id$
 */

require_once PATH_OPENMAIRIE."fpdf_etat.php";

/**
 * Définition de la classe 'pdf_lettre_rar'.
 *
 * Classe de base permettant l'édition d'un document pdf à imprimer sur
 * les feuillets d'accusé de récéption de la poste.
 */
class pdf_lettre_rar extends PDF {

    var $adresse_dest;
    var $adresse_emetteur;
    var $specifique_content;

    var $cell_width = 84;
    var $cell_height = 4;


    // Initialisation des attributs
    function init() {
        // Initialisation de la classe utils
        $this->init_om_application();
        //Paramétrage du pdf
        $this->SetMargins(0,0,0);
        $this->SetAutoPageBreak(false, 0);
        $this->setPrintHeader(false);
        // Definition des attributs
        $this->font = 'Helvetica';
        $this->fontsize = 9;
        $this->SetFont($this->font, '', $this->fontsize);
        //
        $this->adresse_emetteur = array();
        //
        if (DBCHARSET=="UTF8"){
            $this->adresse_emetteur = $this->encodeToUtf8($this->adresse_emetteur);
        }
    }

    /**
     * Méthode d'affichage de l'entête de page.
     * 
     * @return void
     */
    function Header() {
    }

    // Ajout d'une page au document pdf
    function addLetter($adresse_dest, $specifique_content) {
        
        $this->adresse_dest = $adresse_dest;
        $this->specifique_content = $specifique_content;
        //Formatage des données si la base de données est en UTF8
        if(DBCHARSET=="UTF8"){
            $this->adresse_dest = $this->encodeToUtf8($adresse_dest);
            $this->specifique_content = $this->encodeToUtf8($specifique_content);
        }
        $this->addPage();
        $this->printAvisPassage();
        $this->printPreuveDistri();
        $this->printAvisRecept();
    }

    // Insertion des adresses dans la partie "Avis de passage"
    function printAvisPassage() {
        if( DEBUG != 0) {
            $this->SetXY(0, 0);
            $this->SetDrawColor(255, 215, 0);
            $this->Cell(209.5, 98.5,"",1,2,true);
        }
        $this->printCellRAR(35, 41, $this->adresse_dest);
        $this->printCellRAR(120, 41, $this->adresse_dest);
    }

    // Insertion des adresses dans la partie "Preuve de distribution"
    function printPreuveDistri() {
        if( DEBUG != 0) {
            $this->SetXY(0, 98.5);
            $this->SetDrawColor(131, 131, 131);
            $this->Cell(209.5, 101.5,"",1,2,true);
        }
        $this->printCellRAR(55, 142, $this->adresse_dest);
        $this->printCellRAR(55, 172, $this->adresse_emetteur, true);
    }

    // Insertion des adresses dans la partie "Avis de reception"
    function printAvisRecept() {
        if( DEBUG != 0) {
            $this->SetXY(0, 200);
            $this->SetDrawColor(205, 51, 51);
            $this->Cell(209.5, 97,"",1,2,true);
        }
        $this->printCellRAR(55, 231, $this->specifique_content, true);
        $this->printCellRAR(55, 261, $this->adresse_emetteur, true);
    }

    // Insertion d'une adresse
    function printCellRAR($left, $top, $content,$fullBold = false) {

        // 1ere ligne en gras
        $this->SetFont($this->font, 'B', $this->fontsize);
        $this->SetXY($left, $top);
        $border = 0;
        if( DEBUG != 0) {
            $this->SetDrawColor(0, 0, 0);
            $border = 1;
        }
        foreach ($content as $line) {
            
            //Gestion du code barres
            if ( preg_match('/^\|{5}[0-9]{12}\|{5}$/', $line)){
                $this->Code128($this->GetX()+19, $this->GetY(), str_replace("|||||", "", $line), 50, 8);
                $this->Cell($this->cell_width, $this->cell_height*2,"",$border,2,false);
            }
            else{
                $this->Cell($this->cell_width, $this->cell_height,/*utf8_decode(*/trim($line)/*)*/,$border,2,false);
            }
            // test si tout le contenu de la cellule doit être en gras
            if(!$fullBold) {
                $this->SetFont($this->font, '', $this->fontsize);
            }
        }

    }    
    
    // Surcharge afin que le nombre de page ne s'affiche pas
    function Footer() {
    }
    
    /**
     * Encode les données passées au tableau en paramètre en UTF8
     * @param $data array Le tableau de données à traiter
     * 
     * @return array Le tableau de données encodées en UTF8
     */
    function encodeToUtf8($data) {
        
        //Tableau de résultats
        $res = array();
        //Liste des encodages possibles
        $encodage = array("UTF-8", "ASCII", "Windows-1252", "ISO-8859-15", "ISO-8859-1");
        
        foreach ($data as $key => $value) {
            //
            $res[] = iconv(mb_detect_encoding($value,$encodage), "UTF-8", $value);
        }

        return $res;
    }
}

