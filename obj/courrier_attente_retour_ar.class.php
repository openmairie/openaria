<?php
/**
 * Ce script définit la classe 'courrier_attente_retour_ar'.
 *
 * @package openaria
 * @version SVN : $Id$
 */

require_once "../obj/courrier.class.php";

/**
 * Définition de la classe 'courrier_attente_retour_ar' (om_dbform).
 *
 * Surcharge de la classe 'courrier'. Cette classe permet d'afficher seulement
 * les pièces qui sont en attente de retour AR.
 */
class courrier_attente_retour_ar extends courrier {

    /**
     *
     */
    protected $_absolute_class_name = "courrier_attente_retour_ar";

}

