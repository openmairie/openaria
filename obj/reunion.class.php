<?php
/**
 * Ce script définit la classe 'reunion'.
 *
 * @package openaria
 * @version SVN : $Id$
 */

require_once "../gen/obj/reunion.class.php";

/**
 * Définition de la classe 'reunion' (om_dbform).
 */
class reunion extends reunion_gen {

    /**
     * On définit le type global de champs spécifiques.
     */
    var $abstract_type = array(
        "om_fichier_reunion_odj" => "file",
        "om_fichier_reunion_cr_global" => "file",
        "om_fichier_reunion_cr_global_signe" => "file",
        "om_fichier_reunion_cr_par_dossier_signe" => "file",
    );


    /**
     * Définition des actions disponibles sur la classe.
     *
     * @return void
     */
    function init_class_actions() {

        // On récupère les actions génériques définies dans la méthode 
        // d'initialisation de la classe parente
        parent::init_class_actions();

        // ACTION - 001 - modifier
        // On ajoute la vérification du service de l'utilisateur
        $this->class_actions[1]["condition"] = array(
            "is_from_good_service",
            "is_not_closed",
        );

        // ACTION - 002 - supprimer
        // On ajoute la vérification du service de l'utilisateur
        $this->class_actions[2]["condition"] = array(
            "is_from_good_service", 
            "is_not_closed",
            "has_none_avis_rendu",
        );

        // ACTION - 301 - meeting
        //
        $this->class_actions[301] = array(
            "identifier" => "meeting",
            "view" => "view_meeting",
            "portlet" => array(
                "type" => "action-self",
                "libelle" => __("réunion"),
                "order" => 60,
                "class" => "reunion-16",
            ),
            "condition" => array(
                "is_from_good_service", 
            ),
            "permission_suffix" => "gerer",
        );

        // ACTION - 060 - planifier
        //
        $this->class_actions[60] = array(
            "identifier" => "planifier",
            "view" => "view_form_planning",
            "portlet" => array(
                "type" => "action-self",
                "libelle" => __("planifier"),
                "order" => 71,
            ),
            "condition" => array(
                "is_from_good_service", 
                "is_not_closed", 
            ),
            "permission_suffix" => "planifier",
        );

        // ACTION - 061 - deplanifier
        //
        $this->class_actions[61] = array(
            "identifier" => "deplanifier",
            "view" => "view_form_planning",
            "portlet" => array(
                "type" => "action-self",
                "libelle" => __("deplanifier"),
                "order" => 72,
            ),
            "condition" => array(
                "is_from_good_service", 
                "is_not_closed", 
            ),
            "permission_suffix" => "deplanifier",
        );

        // ACTION - 062 - planifier-nouveau
        //
        $this->class_actions[62] = array(
            "identifier" => "planifier-nouveau",
            "view" => "view_form_adding_and_planning",
            "portlet" => array(
                "type" => "action-self",
                "libelle" => __("planifier nouveau"),
                "order" => 73,
            ),
            "condition" => array(
                "is_from_good_service", 
                "is_not_closed", 
            ),
            "permission_suffix" => "planifier_nouveau",
        );


        // ACTION - 081 - numeroter
        //
        $this->class_actions[81] = array(
            "identifier" => "numeroter",
            "view" => "formulaire",
            "portlet" => array(
                "type" => "action-direct-with-confirmation",
                "libelle" => __("numeroter"),
                "order" => 75,
            ),
            "method" => "numeroter",
            "button" => "numeroter",
            "permission_suffix" => "numeroter",
            "condition" => array(
                "is_from_good_service", 
                "is_not_numbered",
                "is_not_closed",
            ),
        );



        // ACTION - 082 - convoquer
        // Cette action permet d'accéder à un formulaire de confirmation de  
        // l'envoi de la convocation par mail en donnant le choix à 
        // l'utilisateur de joindre ou non par mail l'ordre du jour
        $this->class_actions[82] = array(
            "identifier" => "convoquer",
            "view" => "view_form_sendmail",
            "portlet" => array(
                "type" => "action-self",
                "libelle" => __("convoquer"),
                "order" => 80,
                "class" => "send-mail-16",
            ),
            "permission_suffix" => "convoquer",
            "condition" => array(
                "is_from_good_service", 
                "is_not_closed", 
            ),
        );


        // ACTION - 080 - cloturer
        // Cette action permet d'accéder à un formulaire de confirmation de la 
        // clôture de la réunion en donnant le choix à l'utilisateur de 
        // diffuser ou non par mail le compte-rendu global.
        $this->class_actions[80] = array(
            "identifier" => "cloturer",
            "view" => "view_form_sendmail",
            "portlet" => array(
                "type" => "action-self",
                "libelle" => __("cloturer"),
                "order" => 90,
                "class" => "lock-16",
            ),
            "permission_suffix" => "cloturer",
            "condition" => array(
                "is_from_good_service", 
                "is_numbered", 
                "is_not_closed", 
                "has_all_avis_rendu", 
            ),
        );

        // ACTION - 081 - decloturer
        //
        $this->class_actions[83] = array(
            "identifier" => "decloturer",
            "view" => "formulaire",
            "portlet" => array(
                "type" => "action-direct-with-confirmation",
                "libelle" => __("déclôturer"),
                "order" => 79,
                "class" => "unlock-16",
            ),
            "method" => "decloturer",
            "button" => "decloturer",
            "permission_suffix" => "decloturer",
            "condition" => array(
                "is_from_good_service", 
                "is_closed", 
            ),
        );

        // ACTION - 084 - renumeroter
        //
        $this->class_actions[84] = array(
            "identifier" => "numeroter",
            "view" => "formulaire",
            "portlet" => array(
                "type" => "action-direct-with-confirmation",
                "libelle" => __("renuméroter"),
                "order" => 76,
            ),
            "method" => "renumeroter",
            "button" => "renumeroter",
            "permission_suffix" => "renumeroter",
            "condition" => array(
                "is_from_good_service", 
                "is_numbered",
                "is_not_closed",
                "has_none_avis_rendu",
            ),
        );

        // ACTION - 201 - edition-ordre_du_jour
        //
        $this->class_actions[201] = array(
            "identifier" => "edition-ordre_du_jour",
            "view" => "view_edition",
            "portlet" => array(
                "type" => "action-blank",
                "libelle" => __("Ordre du jour"),
                "order" => 81,
                "class" => "pdf-16",
            ),
            "condition" => array("is_from_good_service", "is_available_edition_ordre_du_jour", ),
            "permission_suffix" => "edition",
        );

        // ACTION - 202 - edition-compte_rendu_specifique
        //
        $this->class_actions[202] = array(
            "identifier" => "edition-compte_rendu_global",
            "view" => "view_edition",
            "portlet" => array(
                "type" => "action-blank",
                "libelle" => __("Compte rendu"),
                "order" => 84,
                "class" => "pdf-16",
            ),
            "permission_suffix" => "edition",
            "condition" => array("is_from_good_service", "is_available_edition_compte_rendu_global", ),
        );
        // ACTION - 203 - edition-compte_rendu_specifique
        //
        $this->class_actions[203] = array(
            "identifier" => "edition-compte_rendu_global_signe",
            "portlet" => array(
                "type" => "action-blank",
                "libelle" => __("Compte rendu signe"),
                "order" => 121,
                "class" => "pdf-16",
                "url" => OM_ROUTE_FORM."&snippet=file&obj=reunion&champ=om_fichier_reunion_cr_global_signe&id=",
            ),
            "permission_suffix" => "edition",
            "condition" => array("is_from_good_service", "is_available_edition_compte_rendu_global_signe", ),
        );

        // ACTION - 204 - edition-compte_rendu_specifique
        // Cette action permet d'imprimer l'ensembre des
        // compte-rendus spécifiques de toutes les demandes de
        // passage en un seul fichier.
        $this->class_actions[204] = array(
            "identifier" => "edition-compte_rendu_specifique",
            "view" => "view_edition",
            "portlet" => array(
                "type" => "action-blank",
                "libelle" => __("Compte rendus d'avis"),
                "order" => 85,
                "class" => "pdf-16",
            ),
            "permission_suffix" => "edition",
            "condition" => array("is_from_good_service", "is_available_edition_compte_rendu_specifique", ),
        );

        // ACTION - 205 - edition-compte_rendu_specifique
        // Cette action permet d'imprimer l'ensembre des
        // compte-rendus spécifiques de toutes les demandes de
        // passage en un seul fichier.
        $this->class_actions[205] = array(
            "identifier" => "edition-compte_rendu_specifique_signe",
            "portlet" => array(
                "type" => "action-blank",
                "libelle" => __("Compte rendus d'avis signe"),
                "order" => 122,
                "class" => "pdf-16",
                "url" => OM_ROUTE_FORM."&snippet=file&obj=reunion&champ=om_fichier_reunion_cr_par_dossier_signe&id=",
            ),
            "permission_suffix" => "edition",
            "condition" => array("is_from_good_service", "is_available_edition_compte_rendu_specifique_signe", ),
        );

        // ACTION - 206 - edition-feuille_presence
        //
        $this->class_actions[206] = array(
            "identifier" => "edition-feuille_presence",
            "view" => "view_edition",
            "portlet" => array(
                "type" => "action-blank",
                "libelle" => __("Feuille de présence"),
                "order" => 83,
                "class" => "pdf-16",
            ),
            "permission_suffix" => "edition",
            "condition" => array("is_from_good_service", "is_available_edition_feuille_presence", ),
        );

        // ACTION - 401 - integrer-documents-numerises
        //
        $this->class_actions[401] = array(
            "identifier" => "integrer-documents-numerises",
            "view" => "formulaire",
            "method" => "modifier",
            "portlet" => array(
                "type" => "action-self",
                "libelle" => __("Intégrer documents signes"),
                "order" => 120,
                "class" => "edit-16",
            ),
            "permission_suffix" => "integrer_documents_numerises",
            "condition" => array(
                "is_from_good_service", 
                "is_closed", 
            ),
        );

    }

    /**
     * Filtre par service
     */
    function get_var_sql_forminc__sql_reunion_type_by_service() {
        return "SELECT reunion_type.reunion_type, reunion_type.libelle FROM ".DB_PREFIXE."reunion_type WHERE reunion_type.service = <idx_service> AND (((reunion_type.om_validite_debut IS NULL AND (reunion_type.om_validite_fin IS NULL OR reunion_type.om_validite_fin > CURRENT_DATE)) OR (reunion_type.om_validite_debut <= CURRENT_DATE AND (reunion_type.om_validite_fin IS NULL OR reunion_type.om_validite_fin > CURRENT_DATE)))) ORDER BY reunion_type.libelle ASC";
    }

    /**
     * Filtre par service
     */
    function get_var_sql_forminc__sql_reunion_type_by_service_by_id() {
        return "SELECT reunion_type.reunion_type, reunion_type.libelle FROM ".DB_PREFIXE."reunion_type WHERE reunion_type.service = <idx_service> AND reunion_type = <idx>";
    }

    /**
     * Liste des instances 
     */
    var $instances = null;

    /**
     * Instance du type de réunion lié
     * @var resource
     */
    var $inst_reunion_type = null;

    /**
     * Liste des contacts institutionnels du même service que la réunion et
     * recevant les éditions liées aux commissions.
     *
     * @var resource
     */
    var $contacts_institutionnels = null;

    /**
     *
     */
    var $ordre_du_jour = null;

    /**
     *
     */
    var $common_query_dossier_instruction_reunion = null;

    function get_libelle() {
        $libelle = "(".$this->getVal("code").") ".$this->getVal("libelle")." du ".$this->f->formatDate($this->getVal("date_reunion"), true);
        return $libelle;
    }

    /**
     * Surcharge du fil d'ariane.
     *
     * @param string $ent Chaîne initiale
     *
     * @return chaîne de sortie
     */
    function getFormTitle($ent) {
        if ($this->getParameter("maj") != 0) {
            return $ent." -> ".$this->get_libelle();
        } else {
            return $ent." -> +";
        }
    }

    /**
     *
     */
    function get_common_query_dossier_instruction_reunion() {
        //
        if (is_null($this->common_query_dossier_instruction_reunion)) {
            //
            $this->common_query_dossier_instruction_reunion = "
            SELECT 
                dossier_instruction_reunion.dossier_instruction_reunion,

                dossier_instruction_reunion.reunion_type_categorie as dossier_instruction_reunion_categorie,
                reunion_categorie.libelle as reunion_categorie_libelle,

                dossier_instruction_reunion.ordre as dossier_instruction_reunion_ordre,

                CONCAT(etablissement.code,' - ',etablissement.libelle) as etablissement_libelle,
                etablissement.adresse_numero as etablissement_adresse_numero,
                etablissement.adresse_numero2 as etablissement_adresse_mention,
                voie.libelle as etablissement_adresse_voie_libelle,
                etablissement.adresse_cp as etablissement_adresse_cp,
                etablissement.adresse_ville as etablissement_adresse_ville,
                etablissement_type.libelle as etablissement_type_libelle,
                etablissement_categorie.libelle as etablissement_categorie_libelle,

                dossier_instruction.dossier_instruction as dossier_instruction,
                dossier_instruction.libelle as dossier_instruction_libelle,
                dossier_coordination_type.libelle as dossier_coordination_type_libelle,
                dossier_coordination.dossier_instruction_ads as dossier_instruction_ads,
                to_char(dossier_coordination.date_demande, 'DD/MM/YYYY') as dossier_coordination_date_demande,
                acteur.acronyme as technicien_acronyme,
                acteur.nom_prenom as technicien_nom_prenom,

                to_char(dossier_instruction_reunion.date_souhaitee, 'DD/MM/YYYY') as dossier_instruction_reunion_date_souhaitee,
                dossier_instruction_reunion.motivation as dossier_instruction_reunion_motivation,

                CASE 
                    WHEN dossier_instruction_reunion.proposition_avis IS NOT NULL
                        THEN dossier_instruction_reunion.proposition_avis
                    WHEN analyses.reunion_avis IS NOT NULL
                        THEN analyses.reunion_avis
                    ELSE
                        0
                END AS dossier_instruction_reunion_proposition_avis,

                CASE
                    WHEN dossier_instruction_reunion.proposition_avis IS NOT NULL
                        THEN reunion_avis2.code
                    WHEN analyses.reunion_avis IS NOT NULL
                        THEN reunion_avis3.code
                    ELSE
                        ''
                END AS dossier_instruction_reunion_proposition_avis_code,

                CASE
                    WHEN dossier_instruction_reunion.proposition_avis IS NOT NULL
                        THEN reunion_avis2.libelle
                    WHEN analyses.reunion_avis IS NOT NULL
                        THEN reunion_avis3.libelle
                    ELSE
                        ''
                END AS dossier_instruction_reunion_proposition_avis_libelle,

                CASE
                    WHEN dossier_instruction_reunion.proposition_avis IS NOT NULL
                        THEN dossier_instruction_reunion.proposition_avis_complement
                    WHEN analyses.reunion_avis IS NOT NULL
                        THEN analyses.avis_complement
                    ELSE 
                        ''
                END AS dossier_instruction_reunion_proposition_avis_complement,

                dossier_instruction_reunion.avis as dossier_instruction_reunion_avis,
                reunion_avis0.code as dossier_instruction_reunion_avis_code,
                reunion_avis0.libelle as dossier_instruction_reunion_avis_libelle,
                dossier_instruction_reunion.avis_complement as dossier_instruction_reunion_avis_complement,

                (SELECT
                        string_agg(
                            CONCAT(
                                to_char(visite.date_visite, 'YYYY-MM-DD'),
                                '-',
                                visite.heure_debut
                            ),
                            ', ' ORDER BY visite.date_visite DESC, visite.heure_debut DESC
                        )
                    FROM 
                        ".DB_PREFIXE."visite
                            LEFT JOIN ".DB_PREFIXE."visite_etat
                                ON visite.visite_etat=visite_etat.visite_etat
                    WHERE 
                        visite.dossier_instruction=dossier_instruction.dossier_instruction
                        AND visite_etat.code='PLA') as list_visits_order_desc

            FROM
                ".DB_PREFIXE."dossier_instruction_reunion
                    LEFT JOIN ".DB_PREFIXE."dossier_instruction
                        ON dossier_instruction_reunion.dossier_instruction = dossier_instruction.dossier_instruction
                    LEFT JOIN ".DB_PREFIXE."analyses
                        ON dossier_instruction.dossier_instruction = analyses.dossier_instruction
                    LEFT JOIN ".DB_PREFIXE."dossier_coordination
                        ON dossier_instruction.dossier_coordination = dossier_coordination.dossier_coordination
                    LEFT JOIN ".DB_PREFIXE."dossier_coordination_type
                        ON dossier_coordination.dossier_coordination_type = dossier_coordination_type.dossier_coordination_type
                    LEFT JOIN ".DB_PREFIXE."etablissement
                        ON dossier_coordination.etablissement = etablissement.etablissement
                    LEFT JOIN ".DB_PREFIXE."voie
                        ON etablissement.adresse_voie = voie.voie
                    LEFT JOIN ".DB_PREFIXE."etablissement_type
                        ON etablissement.etablissement_type = etablissement_type.etablissement_type
                    LEFT JOIN ".DB_PREFIXE."etablissement_categorie
                        ON etablissement.etablissement_categorie = etablissement_categorie.etablissement_categorie
                    LEFT JOIN ".DB_PREFIXE."acteur
                        ON dossier_instruction.technicien = acteur.acteur
                    LEFT JOIN ".DB_PREFIXE."reunion_categorie
                        ON dossier_instruction_reunion.reunion_type_categorie=reunion_categorie.reunion_categorie
                    LEFT JOIN ".DB_PREFIXE."reunion_avis as reunion_avis0 
                        ON dossier_instruction_reunion.avis=reunion_avis0.reunion_avis 
                    LEFT JOIN ".DB_PREFIXE."reunion_avis as reunion_avis2 
                        ON dossier_instruction_reunion.proposition_avis=reunion_avis2.reunion_avis 
                    LEFT JOIN ".DB_PREFIXE."reunion_avis as reunion_avis3 
                        ON analyses.reunion_avis=reunion_avis3.reunion_avis 
        ";
        }
        //
        return $this->common_query_dossier_instruction_reunion;
    }


    /**
     * @return array Liste des catégories rattachées au type de réunion de cette réunion
     */
    function get_categories() {
        $inst_reunion_type = $this->get_inst_reunion_type();
        return $inst_reunion_type->get_categories();
    }

    /**
     * TRIGGER - triggerajouterapres.
     *
     * - ...
     *
     * @return boolean
     */
    function triggerajouterapres($id, &$dnu1 = null, $val = array(), $dnu2 = null) {
        //
        $reunion_type = $this->get_inst_reunion_type($val["reunion_type"]);
        // Ajout des liaisons
        $nb_liens = 0;
        // Boucle sur la liste des valeurs sélectionnées
        foreach ($reunion_type->get_all_instances() as $key => $value) {
            // Test si la valeur par défaut est sélectionnée
            if ($value == "") {
                continue;
            }
            // On compose les données de l'enregistrement
            $donnees = array(
                "lien_reunion_r_instance_r_i_membre" => 0,
                "reunion_instance" => $value["reunion_instance"],
                "reunion" => $id,
                "reunion_instance_membre" => null,
                "observation" => "",
            );
            // On ajoute l'enregistrement
            $obj_l = $this->f->get_inst__om_dbform(array(
                "obj" => "lien_reunion_r_instance_r_i_membre",
                "idx" => "]",
            ));
            $obj_l->ajouter($donnees);
            // On compte le nombre d'éléments ajoutés
            $nb_liens++;
        }
        //
        return true;
    }

    /**
     * Récupération de l'instance 'type de réunion' lié à la réunion.
     */
    function get_inst_reunion_type($reunion_type = null) {
        //
        if (is_null($this->inst_reunion_type)) {
            //
            if (is_null($reunion_type)) {
                $reunion_type = $this->getVal("reunion_type");
            }
            //
            $this->inst_reunion_type = $this->f->get_inst__om_dbform(array(
                "obj" => "reunion_type",
                "idx" => $reunion_type,
            ));
        }
        //
        return $this->inst_reunion_type;
    }

    /**
     * Permet de récupérer la liste des demandes de passage en réunion
     * planifiée pour la réunion.
     */
    function get_ordre_du_jour() {
        //
        if (is_null($this->ordre_du_jour)) {
            //
            if ($this->getVal($this->clePrimaire) == "") {
                $this->ordre_du_jour = array();
            } else {
                //
                $sql = $this->get_common_query_dossier_instruction_reunion()."
                WHERE
                    dossier_instruction_reunion.reunion=".$this->getVal($this->clePrimaire)."
                ORDER BY
                    reunion_categorie.ordre, dossier_instruction_reunion.ordre, list_visits_order_desc, acteur.acronyme, etablissement.code
                ";
                //
                $res = $this->f->db->query($sql);
                $this->addToLog(__METHOD__."(): db->query(\"".$sql."\");", VERBOSE_MODE);
                $this->f->isDatabaseError($res);
                //
                $this->ordre_du_jour = array();
                while ($row=& $res->fetchRow(DB_FETCHMODE_ASSOC)) {
                    $this->ordre_du_jour[] = $row;
                }
            }
        }
        //
        return $this->ordre_du_jour;
    }

    function get_ordre_du_jour_nb_planifie() {
        //
        return count($this->get_ordre_du_jour());
    }

    function get_ordre_du_jour_nb_avis_rendu() {
        //
        $nb = 0;
        foreach ($this->get_ordre_du_jour() as $key => $value) {
            if ($value["dossier_instruction_reunion_avis"] != "") {
                $nb++;
            }
        }
        return $nb;
    }

    /**
     *
     */
    function setValFAjout($val = array()) {
        //
        $this->valF['numerotation_simple'] = 0;
    }

    /**
     * SETTER_FORM - setLib.
     *
     * @return void
     */
    function setLib(&$form, $maj) {
        parent::setLib($form, $maj);
        $form->setLib("numerotation_mode", __("mode de numérotation"));
    }

    /**
     *
     */
    function setSelect(&$form, $maj, &$dnu1 = null, $dnu2 = null) {
        if (!is_null($_SESSION["service"])) {
            // Filtre par service
            $sql_reunion_type_by_service = str_replace('<idx_service>', $_SESSION["service"], $this->get_var_sql_forminc__sql("reunion_type_by_service"));
            $sql_reunion_type_by_service_by_id = str_replace('<idx_service>', $_SESSION["service"], $this->get_var_sql_forminc__sql("reunion_type_by_service_by_id"));
            // reunion_type
            $this->init_select(
                $form, 
                $this->f->db,
                $maj,
                null,
                "reunion_type",
                $sql_reunion_type_by_service,
                $sql_reunion_type_by_service_by_id,
                true
            );
        } else {
            parent::setSelect($form, $maj);
        }
        $form->setSelect("numerotation_mode", array(
            array("globale", "par_categorie", ),
            array(__("globale"), __("par catégorie"), ),
        ));
        $form->setSelect(
            "om_fichier_reunion_cr_global_signe",
            array(
                "constraint" => array(
                    "extension" => ".pdf",
                ),
            )
        );
        $form->setSelect(
            "om_fichier_reunion_cr_par_dossier_signe",
            array(
                "constraint" => array(
                    "extension" => ".pdf",
                ),
            )
        );
    }

    /**
     * Le type de commission n'est pas modifiable une fois la commission ajoutée.
     *
     * @return void
     */
    function setType(&$form, $maj) {
        //
        parent::setType($form, $maj);

        //
        if ($maj == 0){
            // Cache le champ code en ajout
            $form->setType('code', 'hidden');
            $form->setType("numerotation_mode", "select");
        }

        //        
        if ($maj == 1 || $maj == 2 || $maj == 3) {
            // Le type de réunion et le code ne doivent plus être modifié après
            // création
            $form->setType('reunion_type', 'selecthiddenstatic');
            $form->setType("numerotation_mode", "selecthiddenstatic");
            $form->setType('code', 'hiddenstatic');
        }
        
        //Cache les champs pour la finalisation
        $form->setType('om_fichier_reunion_odj', 'hidden');
        $form->setType('om_final_reunion_odj', 'hidden');
        $form->setType('om_fichier_reunion_cr_global', 'hidden');
        $form->setType('om_final_reunion_cr_global', 'hidden');
        $form->setType('om_fichier_reunion_cr_global_signe', 'hidden');
        $form->setType('om_fichier_reunion_cr_par_dossier_signe', 'hidden');

        // On cache le champ de numerotation 
        $form->setType("numerotation_simple", 'hidden');
        $form->setType("numerotation_complexe", 'hidden');
        // On cache les champs de cloture 
        $form->setType('reunion_cloture', 'hidden');
        $form->setType('date_cloture', 'hiddendate');
        // On cache le champ de numerotation 
        $form->setType('date_convocation', 'hiddendate');
        // On cache le marqueur
        $form->setType('planifier_nouveau', "hidden");

        // Pour les actions supplémentaires qui utilisent la vue formulaire
        // il est nécessaire de cacher les champs ou plutôt de leur affecter un
        // type pour que l'affichage se fasse correctement
        if ($maj != 0 && $maj != 1 && $maj != 2 && $maj != 3) {
            //
            foreach ($this->champs as $champ) {
                $form->setType($champ, "hidden");
            }
        }

        //
        if ($maj == 401) {
            //
            $form->setType('om_fichier_reunion_cr_global_signe', 'upload');
            $form->setType('om_fichier_reunion_cr_par_dossier_signe', 'upload');
        }

    }

    /**
     * Action javascript au changement du type de la commission.
     *
     * @return void
     */
    function setOnchange(&$form, $maj) {
        //
        parent::setOnchange($form, $maj);
        //
        $form->setOnchange("reunion_type", "process_data_from_reunion_type(this.value);");
    }

    /**
     * SETTER_FORM - set_form_default_values.
     */
    function set_form_default_values(&$form, $maj, $validation) {
        parent::set_form_default_values($form, $maj, $validation);
        // Dans le formulaire d'ajout
        if ($maj == 0) {
            // La date par défaut est la date du jour
            $form->setVal("date_reunion", date('d/m/Y'));
            // La numérotation est à 0 par défaut
            $form->setVal("numerotation_simple", 0);
        }
    }

    /**
     * Génération automatique du code de la commission.
     * 
     * @return void
     */
    function setvalF($val = array()) {
        //
        parent::setValF($val);

        // Récupération du code du type de la commission
        $codeTypeCommission = "";
        if ( isset($val['reunion_type']) && is_numeric($val['reunion_type'])) {
            $codeTypeCommission = $this->getCodeTypeCommission($val['reunion_type']);
        }

        //Formatte la date
        $dateFormatee = $this->f->formatDate($val['date_reunion'], false);
        
        $this->valF['code'] = $codeTypeCommission."-".$dateFormatee;
        // Ce champ json n'est pas modifiable via le formulaire standard
        unset($this->valF["numerotation_complexe"]);
    }
    
    /**
     * Retourne le code du type de la commission passée en paramètre.
     *
     * @param integer $typeCommission Identifiant du type de réunion.
     *
     * @return string
     */
    function getCodeTypeCommission($typeCommission) {
        //
        $reunion_type = $this->f->get_inst__om_dbform(array(
            "obj" => "reunion_type",
            "idx" => $typeCommission,
        ));
        return $reunion_type->getVal("code");
    }

    /**
     *
     */
    function triggersupprimer($id, &$dnu1 = null, $val = array(), $dnu2 = null) {
        //
        if ($this->supprimer_liaisons_table_nan("lien_reunion_r_instance_r_i_membre") === false) {
            return false;
        }
        //
        if ($this->deplanifier_tout() === false) {
            return false;
        }
        //
        return true;
    }

    /**
     *
     */
    function deplanifier_tout() {
        // Logger
        $this->addToLog(__METHOD__."() - begin", EXTRA_VERBOSE_MODE);
        //
        $flag_correct = true;
        //
        foreach ($this->get_ordre_du_jour() as $key => $value) {
            //
            $di_r = $this->f->get_inst__om_dbform(array(
                "obj" => "dossier_instruction_reunion",
                "idx" => $value["dossier_instruction_reunion"],
            ));
            //
            $ret = $di_r->deplanifier_de_la_reunion($this->getVal($this->clePrimaire));
            if ($ret !== true) {
                $flag_correct &= false;
                $this->correct = false;
                $this->addToMessage($value["dossier_instruction_reunion"]."=>".$di_r->msg);
            } else {
                $flag_correct &= true;
            }
        }
        // Logger
        $this->addToLog(__METHOD__."() - end", EXTRA_VERBOSE_MODE);
        //
        return $flag_correct;
    }

    /**
     *
     */
    function supprimer_liaisons_table_nan($table) {
        // Suppression de tous les enregistrements correspondants à l'id 
        // de l'objet instancié en cours dans la table NaN
        $sql = "DELETE FROM ".DB_PREFIXE.$table." WHERE ".$this->clePrimaire."=".$this->getVal($this->clePrimaire);
        $res = $this->f->db->query($sql);
        $this->f->addToLog(__METHOD__."(): db->query(\"".$sql."\");", VERBOSE_MODE);
        if ($this->f->isDatabaseError($res, true)) {
            $this->correct = false;
            return false;
        }
        //
        return true;
    }

    /**
     * Surcharge de la méthode rechercheTable pour éviter de court-circuiter le générateur
     * en devant surcharger la méthode cleSecondaire afin de supprimer les éléments liés dans
     * les tables NaN.
     */
    function rechercheTable(&$dnu1 = null,  $table = "", $field = "", $id = null, $dnu2 = null, $selection = "") {
        //
        if (in_array($table, array("dossier_instruction_reunion", "lien_reunion_r_instance_r_i_membre", ))) {
            //
            $this->addToLog(__METHOD__."(): On ne vérifie pas la table ".$table."", EXTRA_VERBOSE_MODE);
            return;
        }
        //
        parent::rechercheTable($this->f->db, $table, $field, $id, null, $selection);
    }


    /**
     * Récupération de toutes les instances.
     */
    function get_instances() {
        //
        if (is_null($this->instances)) {
            //
            $reunion_type = $this->get_inst_reunion_type();
            // Boucle sur la liste des valeurs sélectionnées
            $this->instances = $reunion_type->get_all_instances();
        }
        //
        return $this->instances;
    }

    /**
     * Récupération des contacts institutionnels du même service que la réunion et
     * recevant les éditions liées aux commissions.
     *
     * @return array
     */
    function get_contacts_institutionnels() {
        //
        if (is_null($this->contacts_institutionnels)) {
            //
            $reunion_type = $this->get_inst_reunion_type();
            //
            $contact_institutionnel = $this->f->get_inst__om_dbform(array(
                "obj" => "contact_institutionnel",
                "idx" => 0,
            ));
            //
            $this->contacts_institutionnels = $contact_institutionnel->get_contacts_institutionnels($reunion_type->getVal("service"), false, true);
        }
        //
        return $this->contacts_institutionnels;
    }

    /**
     * Nettoyage des champs de saisie de liste de courriels.
     *
     * L'utilisateur peut saisir dans les champs des adresses email séparées par
     * des espaces, des virgules, des points virgules, des sauts de ligne. 
     * Cette méthode permet de nettoyer tous ces caractères pour séparer chaque 
     * adresse email par un unique point virgule.
     * 
     * @param string $emails_list Chaîne de caractères à nettoyer.
     *
     * @return string
     */
    function clean_emails_list($emails_list) {
        //
        $cleaned_emails_list = $emails_list;
        //
        $cleaned_emails_list = preg_replace('/\r\n?/', ';', $cleaned_emails_list);
        $cleaned_emails_list = str_replace(" ", ";", $cleaned_emails_list);
        $cleaned_emails_list = str_replace(",", ";", $cleaned_emails_list);
        $cleaned_emails_list = str_replace("\n", ";", $cleaned_emails_list);
        while (strpos($cleaned_emails_list, ";;") != false) {
            $cleaned_emails_list = str_replace(";;", ";", $cleaned_emails_list);
        }
        //
        return $cleaned_emails_list;
    }

    /**
     * Récupération de toutes les adresses email à notifier.
     *
     * L'objet de cette méthode est de récupérer la liste des adresses email à
     * notifier que ce soit pour la convocation à la réunion ou pour la 
     * transmission du compte-rendu global. Cette liste est composée :
     *  - du champ 'listes de diffusion',
     *  - des adresses renseignées dans les instances sélectionnées dans le 
     *    type de réunion.
     *
     * @return string
     */
    function get_emails_to_notify() {
        //
        $emails_to_notify = array();
        //
        $emails_to_notify = array_merge(
            $emails_to_notify,
            array_filter(explode(";", $this->clean_emails_list($this->getVal("listes_de_diffusion"))))
        );
        //
        foreach ($this->get_instances() as $key => $value) {
            //
            $emails_to_notify = array_merge(
                $emails_to_notify,
                array_filter(explode(";", $this->clean_emails_list($value["courriels"])))
            );
        }
        //
        foreach ($this->get_contacts_institutionnels() as $key => $value) {
            //
            $emails_to_notify = array_merge(
                $emails_to_notify,
                array_filter(explode(";", $this->clean_emails_list($value["courriel"])))
            );
        }
        //
        $this->addToLog(__METHOD__."(): \$emails = ".print_r($emails_to_notify, true), EXTRA_VERBOSE_MODE);
        return implode(",", $emails_to_notify);
    }

    /**
     * VIEW - view_form_sendmail.
     *
     * @return void
     */
    function view_form_sendmail() {
        // Logger
        $this->addToLog(__METHOD__."() - begin", EXTRA_VERBOSE_MODE);
        // Vérification de l'accessibilité sur l'élément
        $this->checkAccessibility();
        //
        $objet_common = $this->getVal("libelle")." ".__("du")." ".$this->f->formatDate($this->getVal("date_reunion"), true);
        // 
        if ($this->getParameter("maj") == $this->get_action_key_for_identifier("convoquer")) {
            //
            $mode = "convoquer";
            //
            $this->f->displaySubTitle("-> " .__("Envoyer le courriel de convocation"));
            //
            $objet = $objet_common." - ".__("Convocation");
            $modele_courriel = "modele_courriel_convoquer";
            //
            $joindre_edition_libelle = __("Joindre l'ordre du jour au courriel ?");
            $filename = "ordre-du-jour";
            $modele_edition = "modele_ordre_du_jour";
            $available_edition_method = "is_available_edition_ordre_du_jour";
            //
            $valF = array(
                "date_convocation" => date("Y-m-d"),
            );
            //
            $message_validation = __("La convocation a été correctement effectuée.");
        } elseif ($this->getParameter("maj") == $this->get_action_key_for_identifier("cloturer")) {
            //
            $mode = "cloturer";
            //
            $this->f->displaySubTitle("-> " .__("Clôturer la réunion"));
            //
            $objet = $objet_common." - ".__("Compte-rendu");
            $modele_courriel = "modele_courriel_cloturer";
            //
            $joindre_edition_libelle = __("Joindre le compte rendu au courriel ?");
            $filename = "compte-rendu";
            $modele_edition = "modele_compte_rendu_global";
            $available_edition_method = "is_available_edition_compte_rendu_global";
            //
            $valF = array(
                "date_cloture" => date("Y-m-d"),
                "reunion_cloture" => true,
            );
            //
            $message_validation = __("La clôture de la réunion a été correctement effectuée.");
        } else {
            return;
        }
        //
        $display_form = true;
        // TRAITEMENT
        //
        $this->correct = true;
        $postvar = $this->getParameter("postvar");
        if (isset($postvar["validation"]) && $this->get_emails_to_notify() == "") {
            //
            $this->addToMessage(__("Aucun courriel n'est renseigné. Au moins un courriel est nécessaire."));
            $this->correct = false;
        } elseif (isset($postvar["validation"])) {
            //
            $this->f->db->autocommit(false);
            // Finalisation de l'édition PDF 'ordre du jour'
            if ($this->correct === true
                && $mode == "cloturer"
                && $this->is_available_edition_ordre_du_jour() === true) {
                //
                $ret = $this->finalize(array("field" => "om_fichier_reunion_odj",));
                if ($ret !== true) {
                    $this->correct = false;
                    $this->undoValidation();
                    $this->addToMessage(__("La finalisation de l'ordre du jour a échouée. Contactez votre administrateur."));
                }
            }
            // Finalisation de l'édition PDF 'compte rendu'
            if ($this->correct === true
                && $mode == "cloturer"
                && $this->is_available_edition_compte_rendu_global() === true) {
                //
                $ret = $this->finalize(array("field" => "om_fichier_reunion_cr_global",));
                if ($ret !== true) {
                    $this->correct = false;
                    $this->undoValidation();
                    $this->addToMessage(__("La finalisation du compte-rendu a échouée. Contactez votre administrateur."));
                }
            }
            // Execution de la requête de modification des donnees de l'attribut
            // valF de l'objet dans l'attribut table de l'objet
            if ($this->correct === true) {
                $res = $this->f->db->autoExecute(
                    DB_PREFIXE.$this->table,
                    $valF,
                    DB_AUTOQUERY_UPDATE,
                    $this->getCle($this->getVal($this->clePrimaire))
                );
                // Si une erreur survient
                if ($this->f->isDatabaseError($res, true)) {
                    // Appel de la methode de recuperation des erreurs
                    $this->erreur_db($res->getDebugInfo(), $res->getMessage(), '');
                    $this->undoValidation();
                    $this->correct = false;
                }
            }
            // trigger_cloturer_apres - Mise à jour du suivi des visites sur
            // tous les établissements concernés par la réunion.
            if ($this->correct === true) {
                foreach ($this->get_list_of_concerned_di() as $di_id) {
                    $inst_di = $this->f->get_inst__om_dbform(array(
                        "obj" => "dossier_instruction",
                        "idx" => $di_id,
                    ));
                    $inst_etab = $inst_di->get_inst_etablissement();
                    $ret = $inst_etab->update_tracking_visit_dates();
                    if ($ret !== true) {
                        $this->undoValidation();
                        $this->addToMessage(__("La mise à jour du suivi des visites sur les établissements a échouée. Contactez votre administrateur."));
                        $this->correct = false;
                        break;
                    }
                } 
            }
            // Envoi du mail
            if ($this->correct === true) {
                //
                $pjs = array();
                if (isset($postvar["joindre_edition"])
                    && $this->$available_edition_method()) {
                    //
                    $reunion_type = $this->get_inst_reunion_type();
                    $modele = $reunion_type->getVal($modele_edition);
                    $pdfedition = $this->compute_pdf_output("modele_edition", $modele);
                    //
                    $pj=array();
                    $pj['content'] = $pdfedition["pdf_output"];
                    $pj['title'] = $this->getVal('code')."_".$filename."_".date('YmdHis').".pdf";
                    $pj['stream'] = true;
                    $pjs[] = $pj;
                }
                // Envoi du mail avec message de retour
                @$ret_mail = $this->f->sendMail(
                    $postvar["objet"],
                    str_replace("\n", "<br/>", $postvar["message"]),
                    $this->get_emails_to_notify(),
                    $pjs
                );
                if ($ret_mail === false) {
                    $this->correct = false;
                    $this->undoValidation();
                    $this->addToMessage(__("L'envoi du message a échoué. Contactez votre administrateur."));
                }
            }
            //
            if ($this->correct === true) {
                $this->f->db->commit();
                $this->addToMessage($message_validation);
                $display_form = false;
            }
        }

        // Affichage du message avant d'afficher le formulaire
        $this->message();
        //
        $this->retour();
        if ($display_form == false) {
            return;
        }
        // Ouverture du formulaire
        echo "\t<form";
        echo " method=\"post\"";
        echo " id=\"courrier_suivi_par_code_barres_form\"";
        echo " action=\"".$this->getDataSubmit()."\"";
        echo ">\n";
        // Liste des champs
        $champs = array("courriels", "objet", "message", );
        if ($this->$available_edition_method()) {
            $champs[] = "joindre_edition";
        }
        $form = $this->f->get_inst__om_formulaire(array(
            "maj" => 0,
            "validation" => 0,
            "champs" => $champs,
        ));
        //
        $form->setType('courriels', 'textareastatic');
        $form->setVal('courriels', $this->get_emails_to_notify());
        $form->setLib('courriels', __("Listes des courriels")." *");
        //
        $form->setType('objet', 'text');
        $form->setVal('objet', $objet);
        $form->setLib('objet', __("Objet"));
        $form->setTaille('objet', 80);
        $form->setMax('objet', 100);
        //
        $reunion_type = $this->get_inst_reunion_type();
        $message = $reunion_type->getVal($modele_courriel);
        $message = str_replace('[date]', $this->f->formatDate($this->getVal("date_reunion")), $message);
        $message = str_replace('[heure]', $this->getVal("heure_reunion"), $message);
        $message = str_replace('[adresse]', $this->getVal("lieu_adresse_ligne1"), $message);
        $message = str_replace('[complement]', $this->getVal("lieu_adresse_ligne2"), $message);
        $message = str_replace('[salle]', $this->getVal("lieu_salle"), $message);

        $form->setType('message', 'textarea');
        $form->setVal('message', $message);
        $form->setLib('message', __("Message"));
        $form->setTaille('message', 60);
        $form->setMax('message', 15);
        //
        if ($this->$available_edition_method()) {
            $form->setType('joindre_edition', 'checkbox');
            $form->setLib('joindre_edition', $joindre_edition_libelle);
            $form->setTaille('joindre_edition', 60);
            $form->setMax('joindre_edition', 15);
            $form->setVal('joindre_edition', 't');
        }
        // Affichage du formulaire
        $form->entete();
        $form->afficher($champs, 0, false, false);
        $form->enpied();
        // Affichage du bouton
        echo "\t<div class=\"formControls formControls-bottom\">\n";
        $this->f->layout->display_form_button(array("value" => __("Valider"), "name" => "validation"));
        $this->retour();
        echo "\t</div>\n";
        // Fermeture du formulaire
        echo "\t</form>\n";

    }


    /**
     * VIEW - view_edition.
     *
     * @return void
     */
    function view_edition() {
        // Logger
        $this->addToLog(__METHOD__."() - begin", EXTRA_VERBOSE_MODE);
        // Vérification de l'accessibilité sur l'élément
        $this->checkAccessibility();
        //
        if ($this->get_action_key_for_identifier("edition-ordre_du_jour") == $this->getParameter("maj")) {
            // Si l'ordre du jour est finalisé alors on renvoit le fichier finalisé
            // sinon on renvoit vers l'édition générée à la volée
            if ($this->getVal("om_final_reunion_odj") == "t") {
                //
                $redirect = sprintf(
                    "%s&snippet=file&obj=reunion&champ=om_fichier_reunion_odj&id=%s",
                    OM_ROUTE_FORM,
                    $this->getVal($this->clePrimaire)
                );
                header("location:".$redirect);
                die();
            } else {
                //
                $reunion_type = $this->get_inst_reunion_type();
                $modele = $reunion_type->getVal("modele_ordre_du_jour");
                $pdfedition = $this->compute_pdf_output("modele_edition", $modele);
                $this->expose_pdf_output(
                    $pdfedition["pdf_output"], 
                    $this->getVal('code')."_ordre-du-jour_".date('YmdHis').".pdf"
                );
            }
        } elseif ($this->get_action_key_for_identifier("edition-compte_rendu_global") == $this->getParameter("maj")) {
            // Si l'ordre du jour est finalisé alors on renvoit le fichier finalisé
            // sinon on renvoit vers l'édition générée à la volée
            if ($this->getVal("om_final_reunion_cr_global") == "t") {
                //
                $redirect = sprintf(
                    "%s&snippet=file&obj=reunion&champ=om_fichier_reunion_cr_global&id=%s",
                    OM_ROUTE_FORM,
                    $this->getVal($this->clePrimaire)
                );
                header("location:".$redirect);
                die();
            } else {
                //
                $reunion_type = $this->get_inst_reunion_type();
                $modele = $reunion_type->getVal("modele_compte_rendu_global");
                $pdfedition = $this->compute_pdf_output("modele_edition", $modele);
                $this->expose_pdf_output(
                    $pdfedition["pdf_output"], 
                    $this->getVal('code')."_compte-rendu_".date('YmdHis').".pdf"
                );
            }
        } elseif ($this->get_action_key_for_identifier("edition-feuille_presence") == $this->getParameter("maj")) {
            //
            $reunion_type = $this->get_inst_reunion_type();
            $modele = $reunion_type->getVal("modele_feuille_presence");
            $pdfedition = $this->compute_pdf_output("modele_edition", $modele);
            $this->expose_pdf_output(
                $pdfedition["pdf_output"], 
                $this->getVal('code')."_feuille-presence_".date('YmdHis').".pdf"
            );

        } elseif ($this->get_action_key_for_identifier("edition-compte_rendu_specifique") == $this->getParameter("maj")) {
            //
            $ordre_du_jour_list_ids = "";
            foreach ($this->get_ordre_du_jour() as $key => $value) {
                $ordre_du_jour_list_ids .= $value["dossier_instruction_reunion"].";";
            }
            //
            $reunion_type = $this->get_inst_reunion_type();
            $modele = $reunion_type->getVal("modele_compte_rendu_specifique");
            $pdfedition = $this->compute_pdf_output("modele_edition", $modele, null, $ordre_du_jour_list_ids);
            $this->expose_pdf_output(
                $pdfedition["pdf_output"], 
                $this->getVal('code')."_compte-rendus-d-avis_".date('YmdHis').".pdf"
            );

        } else {
            die();
        }
    }

    /**
     * VIEW - view_meeting.
     *
     * @return void
     */
    function view_meeting() {
        // Logger
        $this->addToLog(__METHOD__."() - begin", EXTRA_VERBOSE_MODE);
        // Vérification de l'accessibilité sur l'élément
        $this->checkAccessibility();

        // Affichage du container permettant le reffraichissement du contenu
        // dans le cas des action-direct.
        printf('
            <div id="sousform-href">
            </div>
            <div id="sousform-dossier_instruction_reunion_contexte_reunion">'
        );
        //
        $this->f->displaySubTitle("-> " .__("Ordre du jour"));
        //
        $this->retour();
        //Affichage du tableau
        echo '<table class="meeting table table-condensed table-bordered table-striped table-hover">';
        // Entête de tableau
        $template_tab_header = '
        <thead>
            <tr>
                <th><!-- --></th>
                <th>
                    <span class="name">%s</span>
                </th>
                <th>
                    <span class="name">%s</span>
                </th>
                <th>
                    <span class="name">%s</span>
                </th>
                <th>
                    <span class="name">%s</span>
                </th>
                <th>
                    <span class="name">%s</span>
                </th>
            </tr>
        </thead>
        ';
        //
        printf(
            $template_tab_header,
            __('n°'),
            __('etablissement'),
            __('dossier'),
            __("proposition d'avis"),
            __("avis")
        );
        //
        echo '<tbody>';
        $categorie = null;
        //Ajout des données au tableau
        foreach ($this->get_ordre_du_jour() as $key => $value) {
            //
            if ($value["dossier_instruction_reunion_categorie"] != $categorie) {
                //
                $categorie = $value["dossier_instruction_reunion_categorie"];
                //
                printf(
                    '<tr class="categorie"><td colspan="6">%s</td></tr>',
                    $value["reunion_categorie_libelle"]
                );
            }
            //
            echo '<tr>';
            // Icône de visualisation
            // Checkbox
            echo '<td class="icons">';
            // Objet à charger
            $obj = "dossier_instruction_reunion_contexte_reunion";
            // Construction de l'url de sousformulaire à appeler
            $url = OM_ROUTE_SOUSFORM."&obj=".$obj;
            $url .= "&idx=".$value['dossier_instruction_reunion'];
            $url .= "&action=3";
            $url .= "&retourformulaire=reunion";
            $url .= "&idxformulaire=".$this->getParameter("idx");
            $url .= "&retour=form";
            //
            //css=#action-form-${obj}-${action}
            echo sprintf('
                <a href="#" id="action-view_meeting-reunion-left-consulter-%s" onclick="ajaxIt(\'%s\', \'%s\');">
                <span class="om-icon om-icon-16 om-icon-fix consult-16">-></span>
                </a>',
                $value['dossier_instruction_reunion'],
                $obj,
                $url,
                $value['dossier_instruction_reunion']
            );
            echo '</td>';
            // Numéro de dossier
            echo '<td class="col-1">';
                echo $value['dossier_instruction_reunion_ordre'];
            echo '</td>';
            //
            echo '<td class="col-3">';
            echo sprintf('
                <strong>%s</strong> [ %s / %s ]<br/>
                %s %s %s<br/>
                %s %s
                ',
                $value['etablissement_libelle'],
                $value['etablissement_categorie_libelle'],
                $value['etablissement_type_libelle'],
                $value['etablissement_adresse_numero'],
                $value['etablissement_adresse_mention'],
                $value['etablissement_adresse_voie_libelle'],
                $value['etablissement_adresse_cp'],
                $value['etablissement_adresse_ville']
            );
            echo '</td>';
            // Instance du dossier d'instruction
            $inst_dossier_instruction = $this->f->get_inst__om_dbform(array(
                "obj" => "dossier_instruction",
                "idx" => $value['dossier_instruction'],
            ));

            // Si c'est un dossier visite, on affiche la liste des visites
            if ($inst_dossier_instruction->get_dossier_type_code_by_dossier_coordination($inst_dossier_instruction->getVal('dossier_coordination')) == "visit") {
                // Récupère la liste des visites du dossier d'instruction
                $list_visits = sprintf(
                    "Visite(s) : %s<br/>",
                    $inst_dossier_instruction->display_list_visits()
                );
            } else {
                $list_visits = "";
            }

            //
            echo '<td class="col-2">';
            if ($this->f->isAccredited(array("dossier_instruction", "dossier_instruction_consulter"), "OR") != true) {
                $dossier_instruction_label = $value['dossier_instruction_libelle'];
            } else {
                $dossier_instruction_label = sprintf(
                    '<a class="om-prev-icon dossier_instruction-16" href="%1$s">%2$s</a>',
                    sprintf(
                        '%1$s&obj=dossier_instruction&action=3&idx=%2$s',
                        OM_ROUTE_FORM,
                        $value["dossier_instruction"]
                    ),
                    $value['dossier_instruction_libelle']
                );
            }
            echo sprintf('
                %s<br/>
                %s / %s<br/>
                Demande du : %s<br/>
                %s
                => %s
                ',
                $value['dossier_coordination_type_libelle'],
                $dossier_instruction_label,
                $value['dossier_instruction_ads'],
                $value['dossier_coordination_date_demande'],
                $list_visits,
                $value['technicien_acronyme']
            );
            echo '</td>';

            // Motication
            echo '<td class="col-4">';
            echo sprintf('
                <strong>%s</strong><br/>
                %s
                ',
                $value['dossier_instruction_reunion_proposition_avis_libelle'],
                $value['dossier_instruction_reunion_proposition_avis_complement']
            );
            echo '</td>';
            // date_souhaitee
            echo '<td class="col-5">';
            echo sprintf('
                <strong>%s</strong><br/>
                %s
                ',
                $value['dossier_instruction_reunion_avis_libelle'],
                $value['dossier_instruction_reunion_avis_complement']
            );
            echo '</td>';
            echo "</tr>";
        }

        echo '</tbody>';
        echo '</table>';
        //
        $this->retour();
        printf('
            </div>'
        );
    }

    /**
     * VIEW - view_form_planning.
     * 
     * Permet de gérer les deux vues planifier et deplanifier.
     * 
     * @return void
     */
    function view_form_planning() {
        // Logger
        $this->addToLog(__METHOD__."() - begin", EXTRA_VERBOSE_MODE);
        // Vérification de l'accessibilité sur l'élément
        $this->checkAccessibility();


        /**
         *
         */
        //
        $val = array("postvar" => $this->getParameter("postvar"), );
        // 
        if ($this->getParameter("maj") == $this->get_action_key_for_identifier("planifier")) {
            $mode = "plan";
            //
            $this->f->displaySubTitle("-> " .__("Planifier des demandes de passage existantes"));
        } elseif ($this->getParameter("maj") == $this->get_action_key_for_identifier("deplanifier")) {
            $mode = "unplan";
            //
            $this->f->displaySubTitle("-> " .__("Déplanifier des demandes de pages de l'ordre du jour"));
        } else {
            return;
        }
        // Initialisation
        $this->correct = true;

        /**
         * VERIFICATION DE L'EXISTENCE DE LA REUNION
         */
        // On récupère l'identifiant de la réunion sur laquelle on se trouve
        $id = $this->getVal($this->clePrimaire);
        // On vérifie que la réunion existe bien
        if ($id == null) {
            $this->correct = false;
            $this->addToMessage("Cette réunion n'existe pas.");
            $this->message();
            //bouton de validation
            echo "\t<div class=\"formControls\">\n";
            $this->retour();
            echo "\t</div>\n";
            //
            return;
        }

        /**
         * SPECIFICITES DE CHAQUE MODE
         */
        if ($mode == "plan") {
            //
            $di_r_method = "planifier_pour_la_reunion";
            //
            $msg_valid1 = __("Le dossier selectionne a ete correctement planifie.");
            $msg_valid2 = __("Les dossiers selectionnes ont ete correctement planifies.");
            $msg_error = __("Un ou plusieurs dossiers n'ont pas ete correctement planifies.");
            // Filtre
            $filter_query = "";
            if (isset($val["postvar"]) 
                && isset($val["postvar"]["categorie"]) 
                && $val["postvar"]["categorie"] != "") {
                //
                $filter_categorie = $val["postvar"]["categorie"];
                $filter_query .= " AND dossier_instruction_reunion.reunion_type_categorie=".$filter_categorie." ";
            }
            //
            if (isset($val["postvar"]) 
                && isset($val["postvar"]["date_souhaitee_fin"]) 
                && $val["postvar"]["date_souhaitee_fin"] != "") {
                $filter_date_souhaitee_fin = $val["postvar"]["date_souhaitee_fin"];
                $filter_query .= " AND dossier_instruction_reunion.date_souhaitee<='".$this->f->formatDate($filter_date_souhaitee_fin, false)."' ";
            }
            //
            if (isset($val["postvar"]) 
                && isset($val["postvar"]["date_souhaitee_debut"]) 
                && $val["postvar"]["date_souhaitee_debut"] != "") {
                $filter_date_souhaitee_debut = $val["postvar"]["date_souhaitee_debut"];
                $filter_query .= " AND dossier_instruction_reunion.date_souhaitee>='".$this->f->formatDate($filter_date_souhaitee_debut, false)."' ";
            }
            //
            $sql = $this->get_common_query_dossier_instruction_reunion()."
                WHERE
                    dossier_instruction_reunion.reunion IS NULL AND 
                    dossier_instruction_reunion.reunion_type = 
                    ( 
                        SELECT 
                            reunion_type 
                        FROM
                            ".DB_PREFIXE."reunion
                        WHERE reunion.reunion = ".$id." 
                    )
            ";
            $sql .= $filter_query;
            $sql .= "
                ORDER BY
                    dossier_instruction_reunion.date_souhaitee
                ";
        } elseif ($mode == "unplan") {
            //
            $di_r_method = "deplanifier_de_la_reunion";
            //
            $msg_valid1 = __("Le dossier selectionne a ete correctement deplanifie.");
            $msg_valid2 = __("Les dossiers selectionnes ont ete correctement deplanifies.");
            $msg_error = __("Un ou plusieurs dossiers n'ont pas ete correctement deplanifies.");
            //
            $sql = $this->get_common_query_dossier_instruction_reunion()."
                WHERE
                    dossier_instruction_reunion.reunion = ".$id."
                    AND dossier_instruction_reunion.avis IS NULL
                ORDER BY
                    dossier_instruction_reunion.date_souhaitee";
        }

        /**
         * EXECUTION DU TRAITEMENT SI VALIDATION DU FORMULAIRE
         */
        if (isset($val["postvar"]) && isset($val["postvar"]["submit"])) {
            //
            $postvar = $val["postvar"];
            // Si auncun dossier n'est sélectionné alors on affiche un message 
            // à l'utilisateur
            if (!isset($postvar["di_r"])) {
                //
                $this->correct = false;
                $this->addToMessage("Vous n'avez selectionne aucun dossier.");
            } else {
                $this->f->db->autoCommit(false);
                //
                $flag_eif = true;
                //
                foreach ($postvar["di_r"] as $key => $value) {
                    //
                    $di_r = $this->f->get_inst__om_dbform(array(
                        "obj" => "dossier_instruction_reunion",
                        "idx" => $value,
                    ));
                    //
                    $ret = $di_r->$di_r_method($id);
                    if ($ret !== true) {
                        $flag_eif &= false;
                        $this->addToMessage($value."=>".$di_r->msg);
                    } else {
                        $flag_eif &= true;
                    }
                }
                //
                if ($flag_eif == true) {
                    //
                    $this->correct = true;
                    if (count($postvar["di_r"]) == 1) {
                        $message = $msg_valid1;
                    } else {
                        $message = $msg_valid2;
                    }
                    //
                    if ($mode == "plan" && $this->is_numbered()) {
                        $ret = $this->numeroter();
                        if ($ret == false) {
                            $this->correct = false;
                            $message = $msg_error;
                        }
                    }
                } else {
                    //
                    $this->correct = false;
                    $message = $msg_error;
                }
                if ($this->correct !== false) {
                    $this->f->db->commit(); // Validation des transactions
                } else {
                    $this->undoValidation(); // Annulation des transactions
                }
                //
                $this->addToMessage($message);
            }
        }

        /**
         * MESSAGE
         */
        // Affichage du message
        $this->message();
        $this->retour();



        /**
         * RECUPERATION DES DI_R SELECTIONNABLES
         */
        //
        $res = $this->f->db->query($sql);
        $this->addToLog(__METHOD__."(): db->query(\"".$sql."\");", VERBOSE_MODE);
        $this->f->isDatabaseError($res);

        /**
         * CONSTRUCTION DU FORMULAIRE
         */
        //
        echo '<div class="row">';
        //
        echo "\n<!-- ########## START DBFORM ########## -->\n";
        echo "<form id=\"form_reunion_planning\"";
        echo " method=\"post\"";
        echo " name=\"f1\"";
        echo " action=\"";
        echo $this->getDataSubmit();
        echo "\"";
        echo ">\n";

        // Formulaire de recherche filtre ajax sur le tableau
        if ($mode == "plan") {
            echo '<div  id="meeting-plan-form-filter" class="row">';
            //
            $champs = array("categorie", "date_souhaitee_debut", "date_souhaitee_fin", );
            // Création d'un nouvel objet de type formulaire
            $form = $this->f->get_inst__om_formulaire(array(
                "maj" => 0,
                "validation" => 0,
                "champs" => $champs,
            ));
            // Caractéristique du champ catégorie
            $form->setLib("categorie", __("categorie"));
            $form->setType("categorie", "select");
            $form->setTaille("categorie", 10);
            $form->setMax("categorie", 10);
            $query_categories = "
            SELECT reunion_categorie.reunion_categorie, reunion_categorie.libelle 
            FROM ".DB_PREFIXE."reunion_categorie 
                LEFT JOIN ".DB_PREFIXE."reunion_type_reunion_categorie
                    ON reunion_type_reunion_categorie.reunion_categorie=reunion_categorie.reunion_categorie
            WHERE reunion_type_reunion_categorie.reunion_type=".$this->getVal("reunion_type")."";
            $this->init_select($form, $this->f->db, 0, false, "categorie", $query_categories);
            //
            if (isset($val["postvar"]) 
                && isset($val["postvar"]["categorie"]) 
                && $val["postvar"]["categorie"] != "") {
                $form->setVal("categorie", $val["postvar"]["categorie"]);
            }
            //
            $form->setType("date_souhaitee_debut", "date");
            $form->setOnchange("date_souhaitee_debut", "fdate(this)");
            $form->setTaille("date_souhaitee_debut", 12);
            $form->setMax("date_souhaitee_debut", 12);
            $form->setLib("date_souhaitee_debut", __("du"));
            //
            if (isset($val["postvar"]) 
                && isset($val["postvar"]["date_souhaitee_debut"]) 
                && $val["postvar"]["date_souhaitee_debut"] != "") {
                $form->setVal("date_souhaitee_debut", $val["postvar"]["date_souhaitee_debut"]);
            }
            //
            $form->setType("date_souhaitee_fin", "date");
            $form->setOnchange("date_souhaitee_fin", "fdate(this)");
            $form->setTaille("date_souhaitee_fin", 12);
            $form->setMax("date_souhaitee_fin", 12);
            $form->setLib("date_souhaitee_fin", __("au"));
            //
            if (isset($val["postvar"]) 
                && isset($val["postvar"]["date_souhaitee_fin"]) 
                && $val["postvar"]["date_souhaitee_fin"] != "") {
                $form->setVal("date_souhaitee_fin", $val["postvar"]["date_souhaitee_fin"]);
            }
            //
            $form->setBloc("categorie", "D", "", "group");
            $form->setBloc("date_souhaitee_fin", "F");
            //
            $form->afficher($champs, 1, false, false);
            echo sprintf('
                <input type="submit" name="plop" value="%s" />
                ',
                __("Filtrer")
            );
            //
            echo '</div>';
            echo '<div class="visualClear"><!-- --></div>';

        }

        // Création d'un nouvel objet de type formulaire
        $form = $this->f->get_inst__om_formulaire(array(
            "maj" => 0,
            "validation" => 0,
            "champs" => array(),
        ));
        $form->entete();
        //Affichage du tableau
        echo '<table class="meeting-'.$mode.' table table-condensed table-bordered table-striped table-hover">';
        // Entête de tableau
        $template_tab_header = '
        <thead>
            <tr class="ui-tabs-nav ui-accordion ui-state-default tab-title">
                <th class="title col-0"><input type="checkbox" id="dir-checkall" /></th>
                <th class="title">
                    <span class="name">%s</span>
                </th>
                <th class="title">
                    <span class="name">%s</span>
                </th>
                <th class="title">
                    <span class="name">%s</span>
                </th>
                <th class="title">
                    <span class="name">%s</span>
                </th>
                <th class="title">
                    <span class="name">%s</span>
                </th>
            </tr>
        </thead>
        ';
        //
        printf(
            $template_tab_header,
            __('etablissement'),
            __('di'),
            __('technicien'),
            __('categorie/motivation'),
            __('date_souhaitee')
        );
        //
        echo '<tbody>';
        // Affiche des données résultats
        if ($res->numrows() > 0 ) {
            //Ajout des données au tableau
            while($value =& $res->fetchRow(DB_FETCHMODE_ASSOC)){
                //
                echo '<tr class="tab-data odd">';
                // Icône de visualisation
                // Checkbox
                echo '<td class="icons">';
                echo '<input type="checkbox" class="dir-checkbox" name="di_r[]" value="'.$value['dossier_instruction_reunion'].'"/>';
                echo '</td>';
                // Numéro de dossier
                echo '<td class="col-1">';
                    echo $value['etablissement_libelle'];
                echo '</td>';
                // Numéro de dossier
                echo '<td class="col-2">';
                    echo $value['dossier_instruction_libelle'];
                echo '</td>';
                // Instructeur
                echo '<td class="col-3">';
                    echo $value['technicien_nom_prenom'];
                echo '</td>';
                // Motication
                echo '<td class="col-4">';
                echo sprintf('
                    <strong>%s</strong><br/>
                    %s',
                    $value['reunion_categorie_libelle'],
                    $value['dossier_instruction_reunion_motivation']
                );
                echo '</td>';
                // date_souhaitee
                echo '<td class="col-5">';
                echo sprintf('
                    %s',
                    $value['dossier_instruction_reunion_date_souhaitee']
                );
                echo '</td>';
                echo "</tr>";
            }
        } else {
            //
            printf(
                '<tr><td colspan="6">%s</td></tr>',
                __("Aucune demande de passage pour ce type de commission.")
            );
        }
        echo '</tbody>';
        echo '</table>';
        $form->enpied();
        //
        echo "\t<div class=\"formControls formControls-bottom\">\n";
        // XXX Pour que le bouton s'affiche correctement
        $this->correct = false;
        //
        $this->bouton($this->getParameter("maj"));
        $this->retour();
        echo "\t</div>\n";
        //
        echo "</form>\n";
        echo '</div>';
    }


    /**
     *
     */
    function get_list_of_concerned_di() {
        //
        $sql = "
        SELECT
            dossier_instruction_reunion.dossier_instruction
        FROM 
            ".DB_PREFIXE."dossier_instruction_reunion
        WHERE
            dossier_instruction_reunion.reunion=".$this->getVal($this->clePrimaire)."
        ";
        //
        $this->f->addToLog(__METHOD__."() db->query(\"".$sql."\");", VERBOSE_MODE);
        $res = $this->f->db->query($sql);
        $this->f->isDatabaseError($res);
        $di = array();
        while ($row=& $res->fetchRow(DB_FETCHMODE_ASSOC)) {
            $di[] =$row['dossier_instruction'];
        }
        return $di;
    }


    /**
     * VIEW - view_form_adding_and_planning.
     *
     * @return void
     */
    function view_form_adding_and_planning() {
        // Logger
        $this->addToLog(__METHOD__."() - begin", EXTRA_VERBOSE_MODE);
        // Vérification de l'accessibilité sur l'élément
        $this->checkAccessibility();
        //
        $this->correct = true;
        //
        $this->f->displaySubTitle("-> " .__("Planifier un ou plusieurs dossiers sans demande de passage"));

        /**
         * VERIFICATION DE L'EXISTENCE DE LA REUNION
         */
        // On récupère l'identifiant de la réunion sur laquelle on se trouve
        $id = $this->getVal($this->clePrimaire);
        // On vérifie que la réunion existe bien
        if ($id == null) {
            $this->correct = false;
            $this->addToMessage("Cette réunion n'existe pas.");
            $this->message();
            //bouton de validation
            echo "\t<div class=\"formControls\">\n";
            $this->retour();
            echo "\t</div>\n";
            //
            return;
        }
        // On recherche le service en fonction du code de dossier transmis
        $rt = $this->f->get_inst__om_dbform(array(
            "obj" => "reunion_type",
            "idx" => $this->getVal("reunion_type"),
        ));
        $service = $rt->getVal("service");
        //
        $reunion_type = $this->getVal("reunion_type");

        /**
         * EXECUTION DU TRAITEMENT SI VALIDATION DU FORMULAIRE
         */
        //
        $postvar = $this->getParameter("postvar");
        if (isset($postvar) && isset($postvar["submit"])) {

            // Vérification des valeurs transmises

            //
            if (isset($postvar["reunion_choix_mode_planif"]) 
                &&  $postvar["reunion_choix_mode_planif"] == "") {
                //
                $this->correct = false;
                $this->addToMessage(__("La selection d'un mode de planification est obligatoire."));
            } elseif (isset($postvar["reunion_choix_mode_planif"]) 
                      && $postvar["reunion_choix_mode_planif"] == "programmation") {
                //
                if (isset($postvar["programmation"]) && $postvar["programmation"] == "") {
                    //
                    $this->correct = false;
                    $this->addToMessage(__("La selection de la programmation est obligatoire."));
                }
            } elseif (isset($postvar["reunion_choix_mode_planif"]) 
                      && $postvar["reunion_choix_mode_planif"] == "reunion") {
                //
                if (isset($postvar["reunion"]) && $postvar["reunion"] == "") {
                    //
                    $this->correct = false;
                    $this->addToMessage(__("La selection de la reunion est obligatoire."));
                }
            }  elseif (isset($postvar["reunion_choix_mode_planif"]) 
                      && $postvar["reunion_choix_mode_planif"] == "dossier") {
                //
                if (isset($postvar["dossier"]) && $postvar["dossier"] == "") {
                    //
                    $this->correct = false;
                    $this->addToMessage(__("La saisie d'un code de dossier est obligatoire."));
                }
            }

            //
            if ($this->correct == true) {
                //
                if (isset($postvar["categorie"]) && $postvar["categorie"] == "") {
                    //
                    $this->correct = false;
                    $this->addToMessage("La selection d'une categorie est obligatoire.");
                }
            }

            //
            if ($this->correct == true) {
                //
                $this->f->db->autoCommit(false);
                //
                if ($postvar["reunion_choix_mode_planif"] == "programmation") {
                    //
                    $programmation = $this->f->get_inst__om_dbform(array(
                        "obj" => "programmation",
                        "idx" => $postvar["programmation"],
                    ));
                    $di_ids = $programmation->get_list_of_concerned_di();
                    //
                    $motivation = sprintf(
                        __("Planification directe depuis la programmation %s le %s."),
                        $programmation->get_libelle(),
                        date("d/m/Y")
                    );
                    // Si aucun dossier instruction n'a été trouvé
                    if (count($di_ids) === 0) {
                        // Alors on indique à l'utilisateur que la programmation
                        // ne contient pas de visite
                        $this->correct = false;
                        $this->addToMessage(__("Aucun dossier sur cette programmation."));
                    }
                } elseif ($postvar["reunion_choix_mode_planif"] == "reunion") {
                    //
                    $reunion = $this->f->get_inst__om_dbform(array(
                        "obj" => "reunion",
                        "idx" => $postvar["reunion"],
                    ));
                    $di_ids = $reunion->get_list_of_concerned_di();
                    //
                    $motivation = sprintf(
                        __("Planification directe depuis la réunion %s le %s."),
                        $reunion->get_libelle(),
                        date("d/m/Y")
                    );
                } elseif ($postvar["reunion_choix_mode_planif"] == "dossier") {
                    //
                    $postvar["dossier"] = trim($postvar["dossier"]);
                    // On recherche le dossier d'instruction en fonction du code
                    // de dossier transmis
                    $di = $this->f->get_inst__om_dbform(array(
                        "obj" => "dossier_instruction",
                        "idx" => 0,
                    ));
                    $val = array(
                        "dossier" => $postvar["dossier"], 
                        "service" => $service,
                    );
                    $di_id = $di->get_id_for_di_or_dc_code($val);
                    // Si aucun dossier instruction n'a été trouvé
                    if ($di_id == null) {
                        // Alors on indique à l'utilisateur que le dossier n'existe pas
                        $this->correct = false;
                        $this->addToMessage(__("Ce dossier n'existe pas."));
                    }
                    //
                    $di_ids = array($di_id, );
                    $motivation = sprintf(
                        __("Planification directe le %s."),
                        date("d/m/Y")
                    );
                }
                //
                if ($this->correct == true) {
                    //
                    $di_r = $this->f->get_inst__om_dbform(array(
                        "obj" => "dossier_instruction_reunion",
                        "idx" => 0,
                    ));
                    //
                    foreach ($di_ids as $di_id) {
                        // Sinon on cré l'entrée de dossier_instruction_reunion
                        $val = array(
                            "reunion_type" => $reunion_type,
                            "reunion" => $id,
                            "dossier_instruction" => $di_id,
                            "service" => $service,
                            "date_souhaitee" => $this->f->formatDate($this->getVal("date_reunion"), true),
                            "reunion_type_categorie" => $postvar["categorie"],
                            "motivation" => $motivation,
                        );
                        //
                        $ret = $di_r->ajouter_depuis_une_reunion($val);
                        //
                        if ($ret !== true) {
                            //
                            break;
                        }
                    }
                    //
                    if ($ret === true) {
                        //
                        if ($this->is_numbered()) {
                            //
                            $ret = $this->numeroter();
                        }
                    }
                    if ($ret === true) {
                        //
                        if ($postvar["reunion_choix_mode_planif"] == "programmation") {
                            //
                            $ret = $programmation->marquer_comme_planifier_nouveau();
                        } elseif ($postvar["reunion_choix_mode_planif"] == "reunion") {
                            //
                            $ret = $reunion->marquer_comme_planifier_nouveau();
                        }
                    }
                    //
                    if ($ret === true) {
                        //
                        $this->addToMessage(__("Dossier(s) planifie(s) avec succes."));
                        //
                        $this->f->db->commit(); // Validation des transactions
                        //
                        unset($postvar);
                    } else {
                        //
                        $this->undoValidation(); // Annulation des transactions
                        //
                        $this->correct = false;
                        $this->addToMessage(__("Une erreur est survenue. Contactez votre administrateur."));
                    }
                }
            }
        }

        /**
         * MESSAGE
         */
        // Affichage du message
        $this->message();
        $this->retour();

        /**
         * CONSTRUCTION DU FORMULAIRE
         */
        //
        echo "\n<!-- ########## START DBFORM ########## -->\n";
        echo "<form";
        echo " method=\"post\"";
        echo " name=\"f1\"";
        echo " action=\"";
        echo $this->getDataSubmit();
        echo "\"";
        echo ">\n";
        //
        $champs = array(
            "reunion_choix_mode_planif",
            "programmation",
            "reunion",
            "dossier",
            "categorie",
        );
        // Création d'un nouvel objet de type formulaire
        $form = $this->f->get_inst__om_formulaire(array(
            "maj" => 0,
            "validation" => 0,
            "champs" => $champs,
        ));
        // Caractéristique du champ mode
        $form->setLib("reunion_choix_mode_planif", __("reunion_choix_mode_planif"));
        $form->setType("reunion_choix_mode_planif", "select");
        $form->setTaille("reunion_choix_mode_planif", 25);
        $form->setMax("reunion_choix_mode_planif", 25);
        $contenu = array(
            array(
                "", 
                "programmation", 
                "reunion", 
                "dossier", 
            ),
            array(
                __("choisir l'élément à planifier"), 
                __("programmation"), 
                __("reunion"), 
                __("dossier"), 
            ),  
        );
        $form->setSelect("reunion_choix_mode_planif", $contenu);
        $form->setOnChange("reunion_choix_mode_planif", "show_hide_fields_by_reunion_choix_mode_planif(this.value)");
        $form->setVal("reunion_choix_mode_planif", (isset($postvar) && isset($postvar["submit"]) && isset($postvar["reunion_choix_mode_planif"]) ? $postvar["reunion_choix_mode_planif"] : ""));
        // Caractéristique du champ programmation
        $form->setLib("programmation", __("programmation"));
        $form->setType("programmation", "select");
        $form->setTaille("programmation", 25);
        $form->setMax("programmation", 25);
        $sql = "
        SELECT 
            programmation.programmation, 
            concat('Programmation n°', programmation.semaine_annee) as libelle
        FROM 
            ".DB_PREFIXE."programmation
        WHERE 
            programmation.service=".$service."
            AND (
                (EXTRACT(YEAR FROM now()) = programmation.annee
                 AND EXTRACT(WEEK FROM now()) > programmation.numero_semaine)
                OR (EXTRACT(YEAR FROM now()) > programmation.annee)
            )
            AND programmation.planifier_nouveau IS FALSE
        ORDER BY
            programmation.annee DESC, programmation.numero_semaine DESC";
        $this->init_select($form, $this->f->db, 0, false, "programmation", $sql);
        $form->setVal("programmation", (isset($postvar) && isset($postvar["submit"]) && isset($postvar["programmation"]) ? $postvar["programmation"] : ""));
        // Caractéristique du champ reunion
        $form->setLib("reunion", __("reunion"));
        $form->setType("reunion", "select");
        $form->setTaille("reunion", 25);
        $form->setMax("reunion", 25);
        $sql = "
        SELECT 
            reunion.reunion, 
            concat('(', reunion.code, ') ', reunion.libelle, ' du ', to_char(reunion.date_reunion, 'DD/MM/YYYY')) as libelle
        FROM 
            ".DB_PREFIXE."reunion 
                LEFT JOIN ".DB_PREFIXE."reunion_type
                    ON reunion.reunion_type=reunion_type.reunion_type
        WHERE 
            reunion_type.service=".$service."
            AND reunion_type.commission IS FALSE
            AND reunion.reunion_cloture IS TRUE
            AND reunion.planifier_nouveau IS FALSE
        ORDER BY
            reunion.date_reunion DESC";
        $this->init_select($form, $this->f->db, 0, false, "reunion", $sql);
        $form->setVal("reunion", (isset($postvar) && isset($postvar["submit"]) && isset($postvar["reunion"]) ? $postvar["reunion"] : ""));
        // Caractéristique du champ dossier
        $form->setLib("dossier", __("dossier de coordination ou d'instruction"));
        $form->setType("dossier", "text");
        $form->setTaille("dossier", 25);
        $form->setMax("dossier", 25);
        $form->setVal("dossier", (isset($postvar) && isset($postvar["submit"]) && isset($postvar["dossier"]) ? $postvar["dossier"] : ""));
        // Caractéristique du champ catégorie
        $form->setLib("categorie", __("categorie"));
        $form->setType("categorie", "select");
        $form->setTaille("categorie", 10);
        $form->setMax("categorie", 10);
        $form->setVal("categorie", (isset($postvar) && isset($postvar["submit"]) && isset($postvar["categorie"]) ? $postvar["categorie"] : ""));
        $sql = "
        SELECT 
            reunion_categorie.reunion_categorie, 
            reunion_categorie.libelle 
        FROM 
            ".DB_PREFIXE."reunion_categorie 
                LEFT JOIN ".DB_PREFIXE."reunion_type_reunion_categorie
                    ON reunion_type_reunion_categorie.reunion_categorie=reunion_categorie.reunion_categorie
        WHERE 
            reunion_type_reunion_categorie.reunion_type=".$reunion_type."";
        $this->init_select($form, $this->f->db, 0, false, "categorie", $sql);
        //
        $form->entete();
        $form->afficher($champs, $this->getParameter("validation"), false, false);
        $form->enpied();
        //
        echo "\t<div class=\"formControls formControls-bottom\">\n";
        // XXX Pour que le bouton s'affiche correctement
        $this->correct = false;
        //
        $this->bouton($this->getParameter("maj"));
        $this->retour();
        echo "\t</div>\n";
        //
        echo "</form>\n";
    }

    /**
     * TREATMENT - renumeroter.
     *
     * @return boolean
     */
    function renumeroter($val = array()) {
        $this->begin_treatment(__METHOD__);
        $query = sprintf(
            'UPDATE %1$sdossier_instruction_reunion SET ordre=null WHERE reunion=%2$s',
            DB_PREFIXE,
            $this->getVal($this->clePrimaire)
        );
        $res = $this->f->db->query($query);
        $this->f->addToLog(__METHOD__."(): db->query(\"".$query."\");", VERBOSE_MODE);
        if ($this->f->isDatabaseError($res, true)) {
            $this->erreur_db($res->getDebugInfo(), $res->getMessage(), '');
            $this->correct = false;
            return $this->end_treatment(__METHOD__, false);
        }
        $valF = array(
            "numerotation_simple" => 0,
            "numerotation_complexe" => null,
        );
        $res = $this->f->db->autoExecute(
            DB_PREFIXE.$this->table,
            $valF,
            DB_AUTOQUERY_UPDATE,
            $this->getCle($this->getVal($this->clePrimaire))
        );
        $this->addToLog(
            __METHOD__."(): db->autoExecute(\"".DB_PREFIXE.$this->table."\", ".print_r($valF, true).", DB_AUTOQUERY_UPDATE, \"".$this->getCle($this->getVal($this->clePrimaire))."\")", 
            VERBOSE_MODE
        );
        if ($this->f->isDatabaseError($res, true)) {
            $this->erreur_db($res->getDebugInfo(), $res->getMessage(), '');
            $this->correct = false;
            return $this->end_treatment(__METHOD__, false);
        }
        $this->init_record_data($this->getVal($this->clePrimaire));
        $this->ordre_du_jour = null;
        $ret = $this->numeroter($val);
        return $this->end_treatment(__METHOD__, $ret);
    }

    /**
     * TREATMENT - numeroter.
     *
     * Numéroter l'ordre du jour.
     *
     * Cette action permet de déclencher la numérotation de l'ordre du jour.
     * Elle numérote les dossiers qui composent l'ordre du jour à partir de 1
     * dans l'ordre des catégories. Une fois que la numérotation a été 
     * déclenchée, tout nouveau dossier prendra le numéro suivant. Un dossier
     * retiré de l'ordre du jour laissera un vide dans la numérotation.
     *
     * @param array $val Valeurs soumises par le formulaire
     *
     * @return boolean
     */
    function numeroter($val = array()) {
        // Begin
        $this->begin_treatment(__METHOD__);
        //
        $numerotation_mode = $this->getVal("numerotation_mode");
        $last_num = $this->getVal("numerotation_simple");
        $numerotation_complexe = null;
        if ($this->getVal("numerotation_complexe") != "") {
            $numerotation_complexe = json_decode($this->getVal("numerotation_complexe"), true);
        } elseif ($numerotation_mode == "par_categorie") {
            $categories = $this->get_categories();
            $numerotation_complexe = array();
            $i = 1;
            foreach ($categories as $key => $value) {
                $numerotation_complexe[$value["reunion_categorie_id"]] = array(
                    "last" => $i * 100,
                    "min" => ($i * 100) + 1,
                    "max" => ($i * 100) + 99,
                );
                $i += 1;
            }
        }
        //
        $sql = "UPDATE ".DB_PREFIXE."dossier_instruction_reunion SET ordre=? WHERE dossier_instruction_reunion=?";
        $sth = $this->f->db->prepare($sql);
        // Logger
        $this->addToLog(__METHOD__."(): db->prepare(\"".$sql."\");", EXTRA_VERBOSE_MODE);
        //
        if ($this->f->isDatabaseError($sth, true)) {
            // Appel de la methode de recuperation des erreurs
            $this->erreur_db($sth->getDebugInfo(), $sth->getMessage(), '');
            // Return
            return $this->end_treatment(__METHOD__, false);
        }
        //
        $alldata = array();
        foreach ($this->get_ordre_du_jour() as $key => $value) {
            if (is_null($value["dossier_instruction_reunion_ordre"])
                || $value["dossier_instruction_reunion_ordre"] == "") {
                if ($numerotation_mode == "globale") {
                    $next_num = ++$last_num;
                } elseif ($numerotation_mode == "par_categorie") {
                    $next_num = ++$numerotation_complexe[$value["dossier_instruction_reunion_categorie"]]["last"];
                    if ($next_num < $numerotation_complexe[$value["dossier_instruction_reunion_categorie"]]["min"]
                        || $next_num > $numerotation_complexe[$value["dossier_instruction_reunion_categorie"]]["max"]) {
                        $this->correct = false;
                        $this->addToMessage(__("Erreur de numérotation, la limite maximale de numéro par catégorie est atteinte."));
                        return $this->end_treatment(__METHOD__, false);
                    }
                }
                //
                $alldata[] = array(
                    $next_num,
                    $value["dossier_instruction_reunion"]
                );
            }
        }
        //
        $res = $this->f->db->executeMultiple($sth, $alldata);
        // Logger
        $this->addToLog(
            __METHOD__."(): db->executeMultiple(\$sth, \"".print_r($alldata, true)."\");",
            EXTRA_VERBOSE_MODE
        );
        //
        if ($this->f->isDatabaseError($res, true)) {
            // Appel de la methode de recuperation des erreurs
            $this->erreur_db($res->getDebugInfo(), $res->getMessage(), '');
            $this->correct = false;
            // Return
            return $this->end_treatment(__METHOD__, false);
        }
        //
        $main_res_affected_rows = $this->f->db->affectedRows();
        // Logger
        $this->addToLog(
            __METHOD__."(): db->affectedRows() = ".$main_res_affected_rows."", 
            EXTRA_VERBOSE_MODE
        );
        // Message de validation
        if ($main_res_affected_rows == 0) {
            $this->addToMessage(__("Attention vous n'avez fait aucune modification."));
        } else {
            $this->addToMessage(__("Numérotation terminée."));
        }
        //
        $valF = array(
            "numerotation_simple" => $last_num,
            "numerotation_complexe" => json_encode($numerotation_complexe),
        );
        // Execution de la requête de modification des donnees de l'attribut
        // valF de l'objet dans l'attribut table de l'objet
        $res = $this->f->db->autoExecute(
            DB_PREFIXE.$this->table,
            $valF,
            DB_AUTOQUERY_UPDATE,
            $this->getCle($this->getVal($this->clePrimaire))
        );
        // Logger
        $this->addToLog(
            __METHOD__."(): db->autoExecute(\"".DB_PREFIXE.$this->table."\", ".print_r($valF, true).", DB_AUTOQUERY_UPDATE, \"".$this->getCle($this->getVal($this->clePrimaire))."\")", 
            VERBOSE_MODE
        );
        // Si une erreur survient
        if ($this->f->isDatabaseError($res, true)) {
            // Appel de la methode de recuperation des erreurs
            $this->erreur_db($res->getDebugInfo(), $res->getMessage(), '');
            $this->correct = false;
            // Return
            return $this->end_treatment(__METHOD__, false);
        }

        // Return
        return $this->end_treatment(__METHOD__, true);
    }

    /**
     * TREATMENT - decloturer.
     *
     * Permet de ...
     *
     * @param array $val Valeurs soumises par le formulaire
     *
     * @return boolean
     */
    function decloturer($val = array()) {
        // Begin
        $this->begin_treatment(__METHOD__);

        //
        $valF = array(
            "reunion_cloture" => false,
        );
        // Execution de la requête de modification des donnees de l'attribut
        // valF de l'objet dans l'attribut table de l'objet
        $res = $this->f->db->autoExecute(DB_PREFIXE.$this->table, $valF, DB_AUTOQUERY_UPDATE, $this->getCle($this->getVal($this->clePrimaire)));
        // Logger
        $this->addToLog(
            __METHOD__."(): db->autoExecute(\"".DB_PREFIXE.$this->table."\", ".print_r($valF, true).", DB_AUTOQUERY_UPDATE, \"".$this->getCle($this->getVal($this->clePrimaire))."\")", 
            VERBOSE_MODE
        );
        // Si une erreur survient
        if (database::isError($res, true)) {
            // Appel de la methode de recuperation des erreurs
            $this->erreur_db($res->getDebugInfo(), $res->getMessage(), '');
            $this->correct = false;
            // Return
            return $this->end_treatment(__METHOD__, false);
        } else {
            //
            $main_res_affected_rows = $this->f->db->affectedRows();        
            // Message de validation
            if ($main_res_affected_rows == 0) {
                $this->addToMessage(__("Attention vous n'avez fait aucune modification.")."<br/>");
            } else {
                $this->addToMessage(__("La réunion a été correctement déclôturée.")."<br/>");
            }
        }

        // Return
        return $this->end_treatment(__METHOD__, true);
    }

    /**
     * TREATMENT - finalize.
     *
     * Finalisation des documents.
     *
     * @return boolean
     */
    function finalize($val = array()) {
        $this->begin_treatment(__METHOD__);

        // Identifiant de l'enregistrement
        $idx = $this->getVal($this->clePrimaire);

        // Vérification des paramètres
        if (!isset($val["field"])
            || !in_array($val["field"], array("om_fichier_reunion_odj", "om_fichier_reunion_cr_global", ))) {
            $this->addToLog(
                __METHOD__."(): Erreur de paramètre.",
                DEBUG_MODE
            );
            $this->correct = false;
            return $this->end_treatment(__METHOD__, false);
        }

        // Initialisation des variables en fonction du paramètre
        $field = $val["field"];
        if ($field == "om_fichier_reunion_odj") {
            $filename = "ordre-du-jour";
            $modele_edition = "modele_ordre_du_jour";
            $available_edition_method = "is_available_edition_ordre_du_jour";
            $field_finalize_flag = "om_final_reunion_odj";
        } elseif ($field == "om_fichier_reunion_cr_global") {
            $filename = "compte-rendu";
            $modele_edition = "modele_compte_rendu_global";
            $available_edition_method = "is_available_edition_compte_rendu_global";
            $field_finalize_flag = "om_final_reunion_cr_global";
        }

        //
        if ($this->$available_edition_method() !== true) {
            return $this->end_treatment(__METHOD__, false);
        }

        // Génération du fichier PDF
        $reunion_type = $this->get_inst_reunion_type();
        $modele = $reunion_type->getVal($modele_edition);
        $pdf_result = $this->compute_pdf_output(
            "modele_edition",
            $modele,
            null,
            $idx
        );

        // Composition des métadonnées du document.
        $metadata = array_merge(
            array(
                'filename' => $this->getVal('code')."_".$filename."_".date('YmdHis').".pdf",
                'mimetype' => 'application/pdf',
                'size' => strlen($pdf_result["pdf_output"])
            ),
            $this->getMetadata($field)
        );

        /**
         * Stockage du document
         */
        // Si le document a déjà été finalisé, on le met à jour en conservant
        // son uid
        if ($this->getVal($field_finalize_flag) != "f") {
            $uid = $this->f->storage->update(
                $this->getVal($field),
                $pdf_result["pdf_output"],
                $metadata
            );
            if ($uid == OP_FAILURE) {
                $this->addToLog(
                    __METHOD__ . "(): Erreur lors de la mise à jour du document sur le système de stockage des fichiers [reunion.".$field."]",
                    DEBUG_MODE
                );
                $this->correct = false;
                $this->cleanMessage();
                $this->addToMessage(__("Une erreur s'est produite lors de la finalisation. Contactez votre administrateur."));
                return $this->end_treatment(__METHOD__, false);
            }
        } else { // Sinon, ajoute le document et récupère son uid
            $uid = $this->f->storage->create(
                $pdf_result["pdf_output"],
                $metadata
            );
            if ($uid == OP_FAILURE) {
                $this->addToLog(
                    __METHOD__ . "(): Erreur lors de la création du document sur le système de stockage des fichiers [reunion.".$field."]",
                    DEBUG_MODE
                );
                $this->correct = false;
                $this->cleanMessage();
                $this->addToMessage(__("Une erreur s'est produite lors de la finalisation. Contactez votre administrateur."));
                return $this->end_treatment(__METHOD__, false);
            }
        }

        // Modifie uniquement les valeurs des champs concernant la finalisation
        // du document
        $valF = array(
            $field => $uid,
            $field_finalize_flag => true,
        );
        // Execution de la requête de modification des donnees de l'attribut
        // valF de l'objet dans l'attribut table de l'objet
        $res = $this->db->autoExecute(
            DB_PREFIXE.$this->table,
            $valF,
            DB_AUTOQUERY_UPDATE,
            $this->getCle($idx)
        );
        // Si une erreur survient
        if (database::isError($res)) {
            // Appel de la methode de recuperation des erreurs
            $this->erreur_db($res->getDebugInfo(), $res->getMessage(), '');
            $this->correct = false;
            return $this->end_treatment(__METHOD__, false);
        }

        // Log
        $this->addToLog(__("Requete executee"), VERBOSE_MODE);
        // Log
        $message = __("Enregistrement")."&nbsp;".$idx."&nbsp;";
        $message .= __("de la table")."&nbsp;\"".$this->table."\"&nbsp;";
        $message .= "[&nbsp;".$this->db->affectedRows()."&nbsp;";
        $message .= __("enregistrement(s) mis a jour")."&nbsp;]";
        $this->addToLog($message, VERBOSE_MODE);
        //
        return $this->end_treatment(__METHOD__, true);
    }

    /**
     * CONDITION - has_none_avis_rendu.
     *
     * @return bool
     */
    function has_none_avis_rendu() {
        //
        if ($this->get_ordre_du_jour_nb_avis_rendu() !== 0) {
            //
            return false;
        }
        //
        return true;
    }

    /**
     * CONDITION - has_all_avis_rendu.
     *
     * @return bool
     */
    function has_all_avis_rendu() {
        //
        if ($this->get_ordre_du_jour_nb_planifie() !== 0
            && $this->get_ordre_du_jour_nb_avis_rendu() == $this->get_ordre_du_jour_nb_planifie()) {
            //
            return true;
        }
        //
        return false;
    }

    /**
     * CONDITION - is_available_edition_ordre_du_jour.
     *
     * @return bool
     */
    function is_available_edition_ordre_du_jour() {
        //
        $reunion_type = $this->get_inst_reunion_type();
        //
        if (is_null($reunion_type->getVal("modele_ordre_du_jour"))
            || $reunion_type->getVal("modele_ordre_du_jour") == "") {
            return false;
        }
        //
        return true;
    }

    /**
     * CONDITION - is_available_edition_compte_rendu_global.
     *
     * @return bool
     */
    function is_available_edition_compte_rendu_global() {
        //
        $reunion_type = $this->get_inst_reunion_type();
        //
        if (is_null($reunion_type->getVal("modele_compte_rendu_global"))
            || $reunion_type->getVal("modele_compte_rendu_global") == "") {
            return false;
        }
        //
        return true;
    }

    /**
     * CONDITION - is_available_edition_compte_rendu_specifique.
     *
     * @return bool
     */
    function is_available_edition_compte_rendu_specifique() {
        //
        $reunion_type = $this->get_inst_reunion_type();
        //
        if (is_null($reunion_type->getVal("modele_compte_rendu_specifique"))
            || $reunion_type->getVal("modele_compte_rendu_specifique") == "") {
            return false;
        }
        //
        return true;
    }

    /**
     * CONDITION - is_available_edition_feuille_presence.
     *
     * @return bool
     */
    function is_available_edition_feuille_presence() {
        //
        $reunion_type = $this->get_inst_reunion_type();
        //
        if (is_null($reunion_type->getVal("modele_feuille_presence"))
            || $reunion_type->getVal("modele_feuille_presence") == "") {
            return false;
        }
        //
        return true;
    }

    /**
     * CONDITION - is_not_numbered.
     *
     * @return bool
     */
    function is_not_numbered() {
        if ($this->getVal("numerotation_simple") == 0
            && $this->getVal("numerotation_complexe") == "") {
            return true;
        }
        return false;
    }

    /**
     * CONDITION - is_numbered.
     *
     * @return bool
     */
    function is_numbered() {
        return !$this->is_not_numbered();
    }

    /**
     * CONDITION - is_not_closed.
     *
     * @return bool
     */
    function is_not_closed() {
        //
        if ($this->getVal("reunion_cloture") == 'f') {
            return true;
        }
        //
        return false;
    }

    /**
     * CONDITION - is_closed.
     *
     * @return bool
     */
    function is_closed() {
        //
        if ($this->getVal("reunion_cloture") == 't') {
            return true;
        }
        //
        return false;
    }

    /**
     *
     */
    function formSpecificContent($maj) {
        //
        if ($this->getParameter("maj") == 3) {
            //
            $numerotation = "";
            if ($this->is_numbered() === true) {
                if ($this->getVal("numerotation_mode") == "globale") {
                    $numerotation .= __("numérotation confirmée - dernier numéro :");
                    $numerotation .= " ".$this->getVal("numerotation_simple");
                } elseif ($this->getVal("numerotation_mode") == "par_categorie") {
                    $numerotation_complexe = json_decode($this->getVal("numerotation_complexe"), true);
                    $numerotation .= __("numérotation confirmée - dernier(s) numéro(s) :")."<ul>";
                    foreach ($this->get_categories() as $key => $value) {
                        $numerotation .= "<li>".$value["reunion_categorie_libelle"]." : ".$numerotation_complexe[$value["reunion_categorie_id"]]["last"]."</li>";
                    }
                    $numerotation .= "</ul>";
                }
            } else {
                $numerotation .= __("L'ordre du jour n'est pas numéroté");
            }
            //
            $convocation = "";
            if ($this->getVal("date_convocation") != "") {
                $convocation .= __("Le dernier envoi de convocation a eu lieu le :")." ";
                $convocation .= $this->f->formatDate($this->getVal("date_convocation"), true);
            } else {
                $convocation .= __("Aucune convocation envoyée");
            }
            //
            $nb_dossiers = "";
            $nb_dossiers .= $this->get_ordre_du_jour_nb_avis_rendu();
            $nb_dossiers .= " ";
            $nb_dossiers .= __("avis rendus");
            $nb_dossiers .= " / ";
            $nb_dossiers .= $this->get_ordre_du_jour_nb_planifie();
            $nb_dossiers .= " ";
            $nb_dossiers .= __("planifiés");
            //
            printf('
                <div id="reunion-status" class="container-fluid">
                    <div class="row">
                        <h3>Statut</h3>
                        <ul>
                            <li>
                                %s
                            </li>
                            <li>
                                %s
                            </li>
                            <li>
                                %s
                            </li>
                        </ul>
                    </div>
                </div>
                ',
                $numerotation,
                $convocation,
                $nb_dossiers
            );
        }
    }

    /**
     * CONDITION - is_from_good_service.
     */
    function is_from_good_service() {
        //
        $reunion_type = $this->get_inst_reunion_type();
        //
        return $reunion_type->is_from_good_service();
    }

    /**
     * CONDITION - is_available_edition_compte_rendu_global_signe.
     */
    function is_available_edition_compte_rendu_global_signe() {
        //
        if ($this->getVal("om_fichier_reunion_cr_global_signe") == "") {
            return false;
        }
        //
        return true;
    }

    /**
     * CONDITION - is_available_edition_compte_rendu_specifique_signe.
     */
    function is_available_edition_compte_rendu_specifique_signe() {
        //
        if ($this->getVal("om_fichier_reunion_cr_par_dossier_signe") == "") {
            return false;
        }
        //
        return true;
    }

    /**
     * Surcharge de la récupération des libellés des champs de fusion
     * 
     * @return [array]  tableau associatif objet => champ de fusion => libellé
     */
    function get_labels_merge_fields() {
        //
        return array(
            __("reunion") => array(
                "reunion.code" => __("code"),
                "reunion.libelle" => __("libelle"),
                "reunion.type" => __("type"),
                "reunion.date" => __("date"),
                "reunion.heure" => __("heure"),
                "reunion.salle" => __("salle"),
                "reunion.adresse" => __("adresse"),
                "reunion.adresse_comp" => __("complement d'adresse"),
                "reunion.president" => __("instance présidente"),
                "reunion.president_signataire_membre" => __("nom du membre signataire de l'instance présidente"),
                "reunion.president_signataire_membre_description" => __("description du membre signataire de l'instance présidente"),
                "reunion.president_signataire_observation" => __("observation sur le signataire de l'instance présidente"),
                "reunion.participants" => __("participants"),
                "reunion.signataires" => __("tableau des signataires (format sur 3 colonnes)"),
                "reunion.signataires_3col" => __("tableau des signataires (format sur 3 colonnes : instance, membre, observations)"),
                "reunion.signataires_2col_1lig" => __("tableau des signataires (format sur 2 colonnes : instance, membre et 1 ligne : observations)"),
                "reunion.signataires_sans_president" => __("tableau des signataires sans l'instance présidente (format sur 3 colonnes)"),
                "reunion.signataires_sans_president_3col" => __("tableau des signataires sans l'instance présidente (format sur 3 colonnes : instance, membre, observations)"),
                "reunion.signataires_sans_president_2col_1lig" => __("tableau des signataires sans l'instance présidente (format sur 2 colonnes : instance, membre et 1 ligne : observations)"),
            ),
        );
    }

    /**
     * Surcharge de la récupération des valeurs des champs de fusion
     * 
     * @return [array]  tableau associatif champ de fusion => valeur
     */
    function get_values_merge_fields() {
        // On intancie son type
        $rn_type = $this->f->get_inst__om_dbform(array(
            "obj" => "reunion_type",
            "idx" => $this->getVal("reunion_type"),
        ));
        // future valeur des champ de fusion reunion.president_nom et reunion.president_desc
        $president_signataire_membre = "";
        $president_signataire_membre_description = "";
        $president_signataire_observation = "";
        // récupération de l'ID du président
        $president_id = $rn_type->getVal('president');
        // un président de réunion a été désigné
        if (!empty($president_id)) { // on peut utiliser empty() car les ID démarrent à 1
            // récupération de l'ID du membre signataire de l'instance de réunion
            $signataire_id = null;
            $sql = sprintf(
                'SELECT
                    reunion_instance_membre,
                    observation
                FROM 
                    %1$slien_reunion_r_instance_r_i_membre
                WHERE
                    reunion=%2$s
                    AND reunion_instance=%3$s',
                DB_PREFIXE,
                intval($this->getVal('reunion')),
                intval($president_id)
            );
            $res = $this->f->db->limitquery($sql, 0, 1);
            $this->addToLog(
                __METHOD__."(): db->limitquery(\"".$sql."\", 0, 1);",
                VERBOSE_MODE
            );
            if ($this->f->isDatabaseError($res, true)) {
                $this->erreur_db($res->getDebugInfo(),
                $res->getMessage(),
                DB_PREFIXE.'lien_reunion_r_instance_r_i_membre');
                return array();
            }
            $row = $res->fetchRow(DB_FETCHMODE_ASSOC);
            if ($row) {
                $signataire_id = $row["reunion_instance_membre"];
                $president_signataire_observation = nl2br($row["observation"]);
            }
            // s'il y a un membsre signataire
            if (!empty($signataire_id)) { // on peut utiliser empty() car les ID démarrent à 1
                // récupération de l'instance du membre signataire
                $rn_signataire = $this->f->get_inst__om_dbform(array(
                    'obj' => 'reunion_instance_membre',
                    'idx' => $signataire_id,
                ));
                // nom du membre
                $president_signataire_membre = $rn_signataire->getVal('membre');
                // description du membre
                $president_signataire_membre_description = nl2br($rn_signataire->getVal('description'));
            }
        }
        // retour
        return array(
            "reunion.code" => $this->getVal("code"),
            "reunion.libelle" => $this->getVal("libelle"),
            "reunion.type" => $rn_type->getVal("libelle"),
            "reunion.date" => $this->dateDBToForm($this->getVal("date_reunion")),
            "reunion.heure" => $this->getVal("heure_reunion"),
            "reunion.salle" => $this->getVal("lieu_salle"),
            "reunion.adresse" => $this->getVal("lieu_adresse_ligne1"),
            "reunion.adresse_comp" => $this->getVal("lieu_adresse_ligne2"),
            "reunion.president" => $this->f->get_label_of_foreign_key("reunion_type","president",$rn_type->getVal("president")),
            "reunion.president_signataire_membre" => $president_signataire_membre,
            "reunion.president_signataire_membre_description" => $president_signataire_membre_description,
            "reunion.president_signataire_observation" => $president_signataire_observation,
            "reunion.participants" => str_replace("\n", "<br/>", $this->getVal("participants")),
            "reunion.signataires" => $this->get_html_output__signataires(array("format" => "3col", )),
            "reunion.signataires_3col" => $this->get_html_output__signataires(array("format" => "3col", )),
            "reunion.signataires_2col_1lig" => $this->get_html_output__signataires(array("format" => "2col_1lig", )),
            "reunion.signataires_sans_president" => $this->get_html_output__signataires(array("format" => "3col", "president" => false, )),
            "reunion.signataires_sans_president_3col" => $this->get_html_output__signataires(array("format" => "3col", "president" => false, )),
            "reunion.signataires_sans_president_2col_1lig" => $this->get_html_output__signataires(array("format" => "2col_1lig", "president" => false, )),
        );
    }

    /**
     * TREATMENT - marquer_comme_planifier_nouveau.
     *
     * Permet de ...
     *
     * @param array $val Valeurs soumises par le formulaire
     *
     * @return boolean
     */
    function marquer_comme_planifier_nouveau($val = array()) {
        // Begin
        $this->begin_treatment(__METHOD__);

        //
        $valF = array(
            "planifier_nouveau" => true,
        );
        // Execution de la requête de modification des donnees de l'attribut
        // valF de l'objet dans l'attribut table de l'objet
        $res = $this->f->db->autoExecute(DB_PREFIXE.$this->table, $valF, DB_AUTOQUERY_UPDATE, $this->getCle($this->getVal($this->clePrimaire)));
        // Logger
        $this->addToLog(
            __METHOD__."(): db->autoExecute(\"".DB_PREFIXE.$this->table."\", ".print_r($valF, true).", DB_AUTOQUERY_UPDATE, \"".$this->getCle($this->getVal($this->clePrimaire))."\")", 
            VERBOSE_MODE
        );
        // Si une erreur survient
        if (database::isError($res, true)) {
            // Appel de la methode de recuperation des erreurs
            $this->erreur_db($res->getDebugInfo(), $res->getMessage(), '');
            $this->correct = false;
            // Return
            return $this->end_treatment(__METHOD__, false);
        } else {
            //
            $main_res_affected_rows = $this->f->db->affectedRows();        
            // Message de validation
            if ($main_res_affected_rows == 0) {
                $this->addToMessage(__("Attention vous n'avez fait aucune modification.")."<br/>");
            } else {
                $this->addToMessage(__("La réunion a été correctement déclôturée.")."<br/>");
            }
        }

        // Return
        return $this->end_treatment(__METHOD__, true);
    }

    /**
     * @return array Liste des signataires de la réunion
     */
    protected function get_signataires() {
        if (isset($this->_signataires) === true
            && is_array($this->_signataires) === true) {
            return $this->_signataires;
        }
        $query = sprintf(
            'SELECT
                membre.membre as nom,
                instance.libelle as organisme,
                lien.observation as signature,
                CASE WHEN lien.reunion_instance = reunion_type.president THEN \'oui\' ELSE \'non\' END as president
            FROM 
                %1$sreunion 
                    LEFT JOIN %1$slien_reunion_r_instance_r_i_membre AS lien 
                        ON lien.reunion = reunion.reunion 
                    LEFT JOIN %1$sreunion_instance AS instance 
                        ON lien.reunion_instance = instance.reunion_instance 
                    LEFT JOIN %1$sreunion_instance_membre AS membre 
                        ON lien.reunion_instance_membre = membre.reunion_instance_membre
                    LEFT JOIN %1$sreunion_type
                        ON reunion.reunion_type = reunion_type.reunion_type
            WHERE 
                reunion.reunion=%2$s
            ORDER BY instance.ordre',
            DB_PREFIXE,
            intval($this->getVal($this->clePrimaire))
        );
        $res = $this->f->db->query($query);
        $this->f->addToLog(__METHOD__."(): db->query(\"".$query."\");", VERBOSE_MODE);
        if ($this->f->isDatabaseError($res, true)) {
            return null;
        }
        $this->_signataires = array();
        while ($row =& $res->fetchrow(DB_FETCHMODE_ASSOC)) {
            $this->_signataires[] = $row;
        }
        return $this->_signataires;
    }

    /**
     * Retour la liste des signataires de la réunion.
     * 
     * @return [string]       Code HTML du tableau
     */
    function get_html_output__signataires($params = array()) {
        $signataires = $this->get_signataires();
        if ($signataires === null) {
            return "";
        }

        /**
         * S'il n'y a aucun résultat
         */
        //
        if (count($signataires) == 0) {
            return __("Aucun");
        }

        $default_params = array(
            "format" => "3col",
            "president" => true,
        );
        if (is_array($params) === false) {
            $params = $default_params;
        } else {
            foreach ($default_params as $key => $value) {
                if (array_key_exists($key, $params) === false) {
                    $params[$key] = $value;
                }
            }
        }

        /**
         * Template
         */
        //
        $template_table = '
        <table style="%s">
            <tbody>
                %s
            </tbody>
        </table>
        ';
        //
        $template_body_line_3col = '
        <tr nobr="true">
            <td style="width:30%%;text-align:left;font-weight:bold;%1$s">%2$s</td>
            <td style="width:50%%;text-align:left;%1$s">%3$s</td>
            <td style="width:20%%;text-align:left;%1$s">%4$s</td>
        </tr>
        ';
        $template_body_line_2col_1lig_with_obs = '
        <tr nobr="true">
            <td style="width:30%%;text-align:left;font-weight:bold;%1$s">%2$s</td>
            <td style="width:70%%;text-align:left;%1$s">%3$s</td>
        </tr>
        <tr nobr="true">
            <td style="width:100%%;text-align:left;%1$s">%4$s</td>
        </tr>
        ';
        $template_body_line_2col_1lig_without_obs = '
        <tr nobr="true">
            <td style="width:30%%;text-align:left;font-weight:bold;%1$s">%2$s</td>
            <td style="width:70%%;text-align:left;%1$s">%3$s</td>
        </tr>%4$s
        ';
        // Style CSS
        $css_border = "";
        $css_bg_line_odd = "";
        $css_bg_line_even = "";

        /**
         * S'il y a au moins un résultat
         */
        $i = 0;
        $table_body_content = "";
        foreach ($signataires as $key => $value) {
            if ($params["president"] === false && $value["president"] === "oui") {
                continue;
            }
            //
            if ($i % 2 == 0) {
                $css_bg_line = $css_bg_line_even;
            } else {
                $css_bg_line = $css_bg_line_odd;
            }
            if ($params["format"] == "2col_1lig" && $value["signature"] === "") {
                $template_body_line = $template_body_line_2col_1lig_without_obs;
            } elseif ($params["format"] == "2col_1lig" && $value["signature"] !== "") {
                $template_body_line = $template_body_line_2col_1lig_with_obs;
            } elseif ($params["format"] == "3col") {
                $template_body_line = $template_body_line_3col;
            }
            //
            $table_body_content .= sprintf(
                $template_body_line,
                $css_border.$css_bg_line,
                $value["nom"],
                $value["organisme"],
                $value["signature"]
            );
            //
            $i++;
        }
        //
        return sprintf(
            $template_table,
            $css_border,
            $table_body_content
        );
    }

    // {{{ BEGIN - GET INST

    /**
     *
     */
    function get_inst_reunion($reunion = null) {
        if ($reunion === null) {
            return $this;
        }
        return $this->get_inst_common("reunion", $reunion);
    }

    // }}} END - GET INST

    // {{{ BEGIN - METADATA FILESTORAGE

    /**
     *
     */
    var $metadata = array(
        "om_fichier_reunion_odj" => array(
            "titre" => "get_md_titre_odj",
            "description" => "get_md_description_odj",
            "origine" => "get_md_origine_genere",
            "code_reunion" => "get_md_code_reunion",
            "date_reunion" => "get_md_date_reunion",
            "type_reunion" => "get_md_type_reunion",
            "commission" => "get_md_commission",
        ),
        "om_fichier_reunion_cr_global" => array(
            "titre" => "get_md_titre_cr_global",
            "description" => "get_md_description_cr_global",
            "origine" => "get_md_origine_genere",
            "code_reunion" => "get_md_code_reunion",
            "date_reunion" => "get_md_date_reunion",
            "type_reunion" => "get_md_type_reunion",
            "commission" => "get_md_commission",
        ),
        "om_fichier_reunion_cr_global_signe" => array(
            "filename" => "get_md_filename_cr_global_signe",
            "titre" => "get_md_titre_cr_global_signe",
            "description" => "get_md_description_cr_global_signe",
            "origine" => "get_md_origine_televerse",
            "code_reunion" => "get_md_code_reunion",
            "date_reunion" => "get_md_date_reunion",
            "type_reunion" => "get_md_type_reunion",
            "commission" => "get_md_commission",
        ),
        "om_fichier_reunion_cr_par_dossier_signe" => array(
            "filename" => "get_md_filename_cr_par_dossier_signe",
            "titre" => "get_md_titre_cr_par_dossier_signe",
            "description" => "get_md_description_cr_par_dossier_signe",
            "origine" => "get_md_origine_televerse",
            "code_reunion" => "get_md_code_reunion",
            "date_reunion" => "get_md_date_reunion",
            "type_reunion" => "get_md_type_reunion",
            "commission" => "get_md_commission",
        ),
    );

    function get_md_filename_cr_global_signe() {
        return $this->getVal('code')."_compte-rendu_".date('YmdHis')."_signe.pdf";
    }
    function get_md_filename_cr_par_dossier_signe() {
        return $this->getVal('code')."_compte-rendu-par-dossier_".date('YmdHis')."_signe.pdf";
    }
    function get_md_titre_odj() { return $this->get_libelle()." - ordre du jour"; }
    function get_md_titre_cr_global() { return $this->get_libelle()." - compte rendu global"; }
    function get_md_titre_cr_global_signe() { return $this->get_libelle()." - compte rendu global (signé)"; }
    function get_md_titre_cr_par_dossier_signe() { return $this->get_libelle()." - compte rendu par dossier (signé)"; }
    function get_md_description_odj() { return "ordre du jour de réunion finalisé"; }
    function get_md_description_cr_global() { return "compte rendu global de réunion finalisé"; }
    function get_md_description_cr_global_signe() { return "compte rendu global de réunion numérisé signé"; }
    function get_md_description_cr_par_dossier_signe() { return "ensemble des comptes rendus de réunion individuels numérisés signés"; }

    // }}} END - METADATA FILESTORAGE

}
