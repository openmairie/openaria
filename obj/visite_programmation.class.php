<?php
/**
 * Ce script définit la classe 'visite_programmation'.
 *
 * @package openaria
 * @version SVN : $Id$
 */

require_once "../obj/visite.class.php";

/**
 * Définition de la classe 'visite_programmation' (om_dbform).
 *
 * Surcharge de la classe 'visite'. Cette classe représente une visite dans le
 * contexte d'une programmation.
 */
class visite_programmation extends visite {

    /**
     *
     */
    protected $_absolute_class_name = "visite_programmation";

}
