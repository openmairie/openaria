<?php
/**
 * Ce script définit la classe 'dossier_coordination_a_cloturer'.
 *
 * @package openaria
 * @version SVN : $Id$
 */

require_once "../obj/dossier_coordination.class.php";

/**
 * Définition de la classe 'dossier_coordination_a_cloturer' (om_dbform).
 *
 * Surcharge de la classe 'dossier_coordination'. Cette classe permet d'afficher
 * directement seulement les dossiers de coordination à clore.
 */
class dossier_coordination_a_cloturer extends dossier_coordination {

    /**
     *
     */
    protected $_absolute_class_name = "dossier_coordination_a_cloturer";

}

