<?php
/**
 * Ce script définit la classe 'lien_autorite_police_courrier'.
 *
 * @package openaria
 * @version SVN : $Id$
 */

require_once "../gen/obj/lien_autorite_police_courrier.class.php";

/**
 * Définition de la classe 'lien_autorite_police_courrier' (om_dbform).
 */
class lien_autorite_police_courrier extends lien_autorite_police_courrier_gen {

    /**
     * Supprime tous les enregistrements de l'autorité de police.
     *
     * @param integer $autorite_police Identifiant de l'autorité de police
     *
     * @return boolean
     */
    function delete_by_autorite_police($autorite_police) {
        // Récupère la liste des enregistrements
        $list_lien_autorite_police_courrier = $this->get_lien_autorite_police_courrier_by_autorite_police($autorite_police);
        // S'il y a des liens
        if (is_array($list_lien_autorite_police_courrier) && count($list_lien_autorite_police_courrier) > 0) {
            // Pour chaque enregistrement
            foreach ($list_lien_autorite_police_courrier as $lien_autorite_police_courrier) {
                // Instancie l'enregistrment
                $lien_autorite_police_courrier_instance = $this->f->get_inst__om_dbform(array(
                    "obj" => lien_autorite_police_courrier,
                    "idx" => $lien_autorite_police_courrier,
                ));
                // initialisation de la clé primaire
                $val['lien_autorite_police_courrier'] = $lien_autorite_police_courrier;
                // Supprime l'enregistrement
                $delete = $lien_autorite_police_courrier_instance->supprimer($val);
                //
                if ($delete == false) {
                    return false;
                }
            }
        }
        //
        return true;
    }

    /**
     * Récupère la liste des liens par l'identifiant de l'autorité de police.
     *
     * @param integer $autorite_police Identifiant de l'autorité de police
     *
     * @return boolean
     */
    function get_lien_autorite_police_courrier_by_autorite_police($autorite_police) {
        // Initialisation de la variable de résultat
        $result = array();
        // Si l'autorité de police est renseigné
        if (!empty($autorite_police)) {
            // Requête SQL
            $sql = "SELECT lien_autorite_police_courrier
                    FROM ".DB_PREFIXE."lien_autorite_police_courrier
                    WHERE autorite_police = ".intval($autorite_police);
            $res = $this->f->db->query($sql);
            $this->f->addToLog(__METHOD__."(): db->query(\"".$sql."\");", VERBOSE_MODE);
            $this->f->isDatabaseError($res);
            // Stockage du résultat dans un tableau
            while ($row =& $res->fetchRow(DB_FETCHMODE_ASSOC)) {
                $result[] = $row['lien_autorite_police_courrier'];
            }
        }
        // Retourne le résultat
        return $result;
    }

}

