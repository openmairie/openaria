<?php
/**
 * Ce script définit la classe 'reunion_type'.
 *
 * @package openaria
 * @version SVN : $Id$
 */

require_once "../gen/obj/reunion_type.class.php";

/**
 * Définition de la classe 'reunion_type' (om_dbform).
 */
class reunion_type extends reunion_type_gen {

    var $all_instances = null;


    /**
     * Définition des actions disponibles sur la classe.
     *
     * @return void
     */
    function init_class_actions() {
        // On récupère les actions génériques définies dans la méthode 
        // d'initialisation de la classe parente
        parent::init_class_actions();

        // ACTION - 001 - modifier
        //
        $this->class_actions[1]["condition"] = array("is_from_good_service", );

        // ACTION - 002 - supprimer
        //
        $this->class_actions[2]["condition"] = array("is_from_good_service", );

        // ACTION - 101 - get_infos
        //
        $this->class_actions[101] = array(
            "identifier" => "get_infos",
            "view" => "get_infos",
            "permission_suffix" => "json",
        );
    }

    /**
     *
     * @return array
     */
    function get_var_sql_forminc__champs() {
        return array(
            //
            "reunion_type.service",
            //
            "reunion_type.reunion_type",
            "reunion_type.code",
            "reunion_type.libelle",
            "reunion_type.lieu_adresse_ligne1",
            "reunion_type.lieu_adresse_ligne2",
            "reunion_type.lieu_salle",
            "reunion_type.heure",
            "reunion_type.listes_de_diffusion",
            "reunion_type.modele_courriel_convoquer",
            "reunion_type.modele_courriel_cloturer",
            "reunion_type.participants",
            "reunion_type.numerotation_mode",
            "reunion_type.commission",
            "reunion_type.president",
            "reunion_type.modele_ordre_du_jour",
            "reunion_type.modele_compte_rendu_global",
            "reunion_type.modele_compte_rendu_specifique",
            "reunion_type.modele_feuille_presence",
            //
            "array_to_string(
                array_agg(
                    distinct(reunion_type_reunion_categorie.reunion_categorie)),
            ';') as categories_autorisees",
            //
            "array_to_string(
                array_agg(
                    distinct(reunion_type_reunion_avis.reunion_avis)),
            ';') as avis_autorises",
            //
            "array_to_string(
                array_agg(
                    distinct(reunion_type_reunion_instance.reunion_instance)),
            ';') as instances_autorisees",
            //
            "reunion_type.om_validite_debut",
            "reunion_type.om_validite_fin",
        );
    }

    /**
     *
     */
    function get_var_sql_forminc__tableSelect() {
        return sprintf(
            '%1$s%2$s
            LEFT JOIN %1$sreunion_type_reunion_avis
                ON reunion_type_reunion_avis.reunion_type = reunion_type.reunion_type
            LEFT JOIN %1$sreunion_avis
                ON reunion_type_reunion_avis.reunion_avis = reunion_avis.reunion_avis
            LEFT JOIN %1$sreunion_type_reunion_instance
                ON reunion_type_reunion_instance.reunion_type = reunion_type.reunion_type
            LEFT JOIN %1$sreunion_instance
                ON reunion_type_reunion_instance.reunion_instance = reunion_instance.reunion_instance
            LEFT JOIN %1$sreunion_type_reunion_categorie
                ON reunion_type_reunion_categorie.reunion_type = reunion_type.reunion_type
            LEFT JOIN %1$sreunion_categorie
                ON reunion_type_reunion_categorie.reunion_categorie = reunion_categorie.reunion_categorie',
            DB_PREFIXE,
            $this->table
        );
    }

    /**
     *
     */
    function get_var_sql_forminc__selection() {
        return " GROUP BY reunion_type.libelle, reunion_type.reunion_type ";
    }

    /**
     *
     */
    function set_form_specificity(&$form, $maj) {
        //
        parent::set_form_specificity($form, $this->getParameter("maj"));

        /**
         * Gestion du champ service
         */
        // 
        $this->set_form_specificity_service_common($form, $this->getParameter("maj"));
        // Si l'utilisateur est sur un service
        if (is_null($_SESSION['service'])) {
            // En mode AJOUTER
            if ($this->getParameter("maj") == 0) {
                // A la modification du champ service alors on met à jour les champs
                // liés pour que seules les valeurs correspondantes soient affichées.
                $form->setOnchange(
                    "service", 
                    "filterSelect(this.value, 'categories_autorisees', 'service', 'reunion_type');".
                    "filterSelect(this.value, 'instances_autorisees', 'service', 'reunion_type');".
                    "filterSelect(this.value, 'avis_autorises', 'service', 'reunion_type');".
                    "filterSelect(this.value, 'president', 'service', 'reunion_type');"
                );
            }
        }
    }

    /**
     * VIEW - get_infos.
     *
     * Renvoi un tableau JSON représentant toutes les données de 
     * l'enregistrement courant.
     *
     * @return void
     */
    function get_infos() {
        //
        $this->f->disableLog();
        //
        $infos = array();
        foreach ($this->champs as $key => $value) {
            $infos[$value] = $this->getVal($value);
        }
        //
        echo json_encode($infos);
    }

    /**
     * SETTER_FORM - setLib.
     *
     * @return void
     */
    function setLib(&$form, $maj) {
        parent::setLib($form, $maj);
        $form->setLib("numerotation_mode", __("mode de numérotation"));
        // Liaison NaN
        $form->setLib("avis_autorises", __("avis_autorises"));
        $form->setLib("instances_autorisees", __("instances_autorisees"));
        $form->setLib("categories_autorisees", __("categories_autorisees"));
    }

    /**
     *
     */
    function setType(&$form, $maj) {
        //
        parent::setType($form, $this->getParameter("maj"));
        //
        if ($this->getParameter("maj") == 0 || $this->getParameter("maj") == 1) {
            $form->setType("numerotation_mode", "select");
            // Liaison NaN
            $form->setType("avis_autorises", "select_multiple");
            $form->setType("instances_autorisees", "select_multiple");
            $form->setType("categories_autorisees", "select_multiple");
        }
        //
        if ($this->getParameter("maj") == 2 || $this->getParameter("maj") == 3) {
            // Liaison NaN
            $form->setType("avis_autorises", "select_multiple_static");
            $form->setType("instances_autorisees", "select_multiple_static");
            $form->setType("categories_autorisees", "select_multiple_static");
        }
    }

    /**
     * Filtre par service
     */
    function get_var_sql_forminc__sql_president() {
        return "
        SELECT 
            reunion_instance.reunion_instance, 
            reunion_instance.libelle 
        FROM ".DB_PREFIXE."reunion_instance 
        WHERE
            reunion_instance.service = <idx_service>
            AND ((reunion_instance.om_validite_debut IS NULL 
                  AND (reunion_instance.om_validite_fin IS NULL 
                       OR reunion_instance.om_validite_fin > CURRENT_DATE)) 
                 OR (reunion_instance.om_validite_debut <= CURRENT_DATE 
                     AND (reunion_instance.om_validite_fin IS NULL 
                          OR reunion_instance.om_validite_fin > CURRENT_DATE))) 
        ORDER BY reunion_instance.libelle ASC
        ";
    }

    /**
     *
     */
    function get_var_sql_forminc__sql_president_by_id() {
        return "
        SELECT 
            reunion_instance.reunion_instance, 
            reunion_instance.libelle 
        FROM ".DB_PREFIXE."reunion_instance 
        WHERE 
            reunion_instance = <idx>
        ";
    }

    /**
     * Liaison NaN - reunion_type/reunion_categorie
     */
    function get_var_sql_forminc__sql_categories_autorisees() {
        return "
        SELECT
            reunion_categorie.reunion_categorie,
            reunion_categorie.libelle as lib
        FROM ".DB_PREFIXE."reunion_categorie
        WHERE reunion_categorie.service = <idx_service>
        AND ((reunion_categorie.om_validite_debut IS NULL AND (reunion_categorie.om_validite_fin IS NULL OR reunion_categorie.om_validite_fin > CURRENT_DATE)) OR (reunion_categorie.om_validite_debut <= CURRENT_DATE AND (reunion_categorie.om_validite_fin IS NULL OR reunion_categorie.om_validite_fin > CURRENT_DATE)))
        ORDER BY lib";
    }

    /**
     *
     */
    function get_var_sql_forminc__sql_categories_autorisees_by_id() {
        return "
        SELECT
            reunion_categorie.reunion_categorie,
            reunion_categorie.libelle as lib
        FROM ".DB_PREFIXE."reunion_categorie
        WHERE reunion_categorie.service = <idx_service>
            AND reunion_categorie.reunion_categorie IN (<idx>)
        ORDER BY reunion_categorie.ordre, lib";
    }

    /**
     * Liaison NaN - reunion_type/reunion_avis
     */
    function get_var_sql_forminc__sql_avis_autorises() {
        return "
        SELECT
            reunion_avis.reunion_avis,
            reunion_avis.libelle as lib
        FROM ".DB_PREFIXE."reunion_avis
        WHERE reunion_avis.service = <idx_service>
        AND ((reunion_avis.om_validite_debut IS NULL AND (reunion_avis.om_validite_fin IS NULL OR reunion_avis.om_validite_fin > CURRENT_DATE)) OR (reunion_avis.om_validite_debut <= CURRENT_DATE AND (reunion_avis.om_validite_fin IS NULL OR reunion_avis.om_validite_fin > CURRENT_DATE)))
        ORDER BY lib";
    }

    /**
     *
     */
    function get_var_sql_forminc__sql_avis_autorises_by_id() {
        return "
        SELECT
            reunion_avis.reunion_avis,
            reunion_avis.libelle as lib
        FROM ".DB_PREFIXE."reunion_avis
        WHERE reunion_avis.service = <idx_service>
            AND reunion_avis.reunion_avis IN (<idx>)
        ORDER BY lib";
    }

    /**
     * Liaison NaN - reunion_type/reunion_instance
     */
    function get_var_sql_forminc__sql_instances_autorisees() {
        return "
        SELECT
            reunion_instance.reunion_instance,
            reunion_instance.libelle as lib
        FROM ".DB_PREFIXE."reunion_instance
        WHERE reunion_instance.service = <idx_service>
        AND ((reunion_instance.om_validite_debut IS NULL AND (reunion_instance.om_validite_fin IS NULL OR reunion_instance.om_validite_fin > CURRENT_DATE)) OR (reunion_instance.om_validite_debut <= CURRENT_DATE AND (reunion_instance.om_validite_fin IS NULL OR reunion_instance.om_validite_fin > CURRENT_DATE)))
        ORDER BY lib";
    }

    /**
     *
     */
    function get_var_sql_forminc__sql_instances_autorisees_by_id() {
        return "
        SELECT
            reunion_instance.reunion_instance,
            reunion_instance.libelle as lib
        FROM ".DB_PREFIXE."reunion_instance
        WHERE reunion_instance.service = <idx_service>
            AND reunion_instance.reunion_instance IN (<idx>)
        ORDER BY lib";
    }

    /**
     * filtre les modèles d'édition
     */
    function get_var_sql_forminc__sql_modele_edition() {
        return "SELECT modele_edition.modele_edition, modele_edition.libelle 
        FROM ".DB_PREFIXE."modele_edition 
        LEFT JOIN ".DB_PREFIXE."courrier_type ON courrier_type.courrier_type = modele_edition.courrier_type
        WHERE ((modele_edition.om_validite_debut IS NULL AND (modele_edition.om_validite_fin IS NULL OR modele_edition.om_validite_fin > CURRENT_DATE)) OR (modele_edition.om_validite_debut <= CURRENT_DATE AND (modele_edition.om_validite_fin IS NULL OR modele_edition.om_validite_fin > CURRENT_DATE)))
        AND LOWER(courrier_type.code) = LOWER('<courrier_type_code>')
        ORDER BY modele_edition.libelle ASC";
    }

    /**
     *
     */
    function setSelect(&$form, $maj, &$dnu1 = null, $dnu2 = null) {
        // Attention : l'appel au parent est commenté volontairement
        // parent::setSelect($form, $maj);
        $form->setSelect("numerotation_mode", array(
            array("globale", "par_categorie", ),
            array(__("globale"), __("par catégorie"), ),
        ));
        // modele_compte_rendu_global
        $sql_modele_compte_rendu_global = $this->get_var_sql_forminc__sql("modele_edition");
        $sql_modele_compte_rendu_global = str_replace('<courrier_type_code>', 'REU-CRG', $sql_modele_compte_rendu_global);
        $this->init_select($form, $this->f->db, $maj, null, "modele_compte_rendu_global", $sql_modele_compte_rendu_global, $this->get_var_sql_forminc__sql("modele_compte_rendu_global_by_id"), true);
        // modele_compte_rendu_specifique
        $sql_modele_compte_rendu_specifique = $this->get_var_sql_forminc__sql("modele_edition");
        $sql_modele_compte_rendu_specifique = str_replace('<courrier_type_code>', 'REU-CRA', $sql_modele_compte_rendu_specifique);
        $this->init_select($form, $this->f->db, $maj, null, "modele_compte_rendu_specifique", $sql_modele_compte_rendu_specifique, $this->get_var_sql_forminc__sql("modele_compte_rendu_specifique_by_id"), true);
        // modele_feuille_presence
        $sql_modele_feuille_presence = $this->get_var_sql_forminc__sql("modele_edition");
        $sql_modele_feuille_presence = str_replace('<courrier_type_code>', 'REU-FP', $sql_modele_feuille_presence);
        $this->init_select($form, $this->f->db, $maj, null, "modele_feuille_presence", $sql_modele_feuille_presence, $this->get_var_sql_forminc__sql("modele_feuille_presence_by_id"), true);
        // modele_ordre_du_jour
        $sql_modele_ordre_du_jour = $this->get_var_sql_forminc__sql("modele_edition");
        $sql_modele_ordre_du_jour = str_replace('<courrier_type_code>', 'REU-ODJ', $sql_modele_ordre_du_jour);
        $this->init_select($form, $this->f->db, $maj, null, "modele_ordre_du_jour", $sql_modele_ordre_du_jour, $this->get_var_sql_forminc__sql("modele_ordre_du_jour_by_id"), true);
        // service
        $this->init_select($form, $this->f->db, $maj, null, "service", $this->get_var_sql_forminc__sql("service"), $this->get_var_sql_forminc__sql("service_by_id"), true);

        // Filtre par service
        $service = "";
        $champ = "service";
        if (isset($_POST[$champ])) {
            $service = $_POST[$champ];
        } elseif($this->getParameter($champ) != "") {
            $service = $this->getParameter($champ);
        } elseif(isset($form->val[$champ])) {
            $service = $form->val[$champ];
        } elseif($this->getVal($champ) != null) {
            $service = $this->getVal($champ);
        }
        if ($service == "") {
            $service = -1;
        }

        // Filtre par service
        $sql_president = str_replace('<idx_service>', $service, $this->get_var_sql_forminc__sql("president"));
        $sql_president_by_id = str_replace('<idx_service>', $service, $this->get_var_sql_forminc__sql("president_by_id"));
        $this->init_select($form, $this->f->db, $this->getParameter("maj"), null, "president", $sql_president, $sql_president_by_id, true);

        // Filtre par service
        $sql_categories_autorisees = str_replace('<idx_service>', $service, $this->get_var_sql_forminc__sql("categories_autorisees"));
        $sql_categories_autorisees_by_id = str_replace('<idx_service>', $service, $this->get_var_sql_forminc__sql("categories_autorisees_by_id"));
        // Liaison NaN
        $this->init_select($form, $this->f->db, $this->getParameter("maj"), null, "categories_autorisees", $sql_categories_autorisees, $sql_categories_autorisees_by_id, true, true);
        // Suppression du choix vide
        if ($this->getParameter("maj") == 0 || $this->getParameter("maj") == 1) {
            array_shift($form->select["categories_autorisees"][0]);
            array_shift($form->select["categories_autorisees"][1]);
        }

        // Filtre par service
        $sql_avis_autorises = str_replace('<idx_service>', $service, $this->get_var_sql_forminc__sql("avis_autorises"));
        $sql_avis_autorises_by_id = str_replace('<idx_service>', $service, $this->get_var_sql_forminc__sql("avis_autorises_by_id"));
        // Liaison NaN
        $this->init_select($form, $this->f->db, $this->getParameter("maj"), null, "avis_autorises", $sql_avis_autorises, $sql_avis_autorises_by_id, true, true);
        // Suppression du choix vide
        if ($this->getParameter("maj") == 0 || $this->getParameter("maj") == 1) {
            array_shift($form->select["avis_autorises"][0]);
            array_shift($form->select["avis_autorises"][1]);
        }

        // Filtre par service
        $sql_instances_autorisees = str_replace('<idx_service>', $service, $this->get_var_sql_forminc__sql("instances_autorisees"));
        $sql_instances_autorisees_by_id = str_replace('<idx_service>', $service, $this->get_var_sql_forminc__sql("instances_autorisees_by_id"));
        // Liaison NaN
        $this->init_select($form, $this->f->db, $this->getParameter("maj"), null, "instances_autorisees", $sql_instances_autorisees, $sql_instances_autorisees_by_id, true, true);
        // Suppression du choix vide
        if ($this->getParameter("maj") == 0 || $this->getParameter("maj") == 1) {
            array_shift($form->select["instances_autorisees"][0]);
            array_shift($form->select["instances_autorisees"][1]);
        }
    }

    /**
     * Liaison NaN
     */
    var $liaisons_nan = array(
        //
        "reunion_type_reunion_avis" => array(
            "table_l" => "reunion_type_reunion_avis",
            "table_f" => "reunion_avis",
            "field" => "avis_autorises",
        ),
        "reunion_type_reunion_instance" => array(
            "table_l" => "reunion_type_reunion_instance",
            "table_f" => "reunion_instance",
            "field" => "instances_autorisees",
        ),
        "reunion_type_reunion_categorie" => array(
            "table_l" => "reunion_type_reunion_categorie",
            "table_f" => "reunion_categorie",
            "field" => "categories_autorisees",
        ),
    );

    /**
     * TRIGGER - triggerajouterapres.
     *
     * - ...
     *
     * @return boolean
     */
    function triggerajouterapres($id, &$dnu1 = null, $val = array(), $dnu2 = null) {
        // Liaisons NaN
        foreach ($this->liaisons_nan as $liaison_nan) {
            // Ajout des liaisons table Nan
            $nb_liens = $this->ajouter_liaisons_table_nan(
                $liaison_nan["table_l"], 
                $liaison_nan["table_f"], 
                $liaison_nan["field"]
            );
            // Message de confirmation
            if ($nb_liens > 0) {
                if ($nb_liens == 1 ){
                    $this->addToMessage(sprintf(__("Creation d'une nouvelle liaison realisee avec succes.")));
                } else {
                    $this->addToMessage(sprintf(__("Creation de %s nouvelles liaisons realisee avec succes."), $nb_liens));
                }
            }
        }
    }

    /**
     * TRIGGER - triggermodifierapres.
     *
     * - ...
     *
     * @return boolean
     */
    function triggermodifierapres($id, &$dnu1 = null, $val = array(), $dnu2 = null) {
        // Liaisons NaN
        foreach ($this->liaisons_nan as $liaison_nan) {
            // Suppression des liaisons table NaN
            $this->supprimer_liaisons_table_nan($liaison_nan["table_l"]);
            // Ajout des liaisons table Nan
            $nb_liens = $this->ajouter_liaisons_table_nan(
                $liaison_nan["table_l"], 
                $liaison_nan["table_f"], 
                $liaison_nan["field"]
            );
            // Message de confirmation
            if ($nb_liens > 0) {
                $this->addToMessage(__("Mise a jour des liaisons realisee avec succes."));
            }
        }

    }

    /**
     *
     */
    function triggersupprimer($id, &$dnu1 = null, $val = array(), $dnu2 = null) {

        // Liaisons NaN
        foreach ($this->liaisons_nan as $liaison_nan) {
            // Suppression des liaisons table NaN
            $this->supprimer_liaisons_table_nan($liaison_nan["table_l"]);
        }

    }

    /**
     *
     */
    function ajouter_liaisons_table_nan($table_l, $table_f, $field) {
        // Récupération des données du select multiple
        $postvar = $this->getParameter("postvar");
        if (isset($postvar[$field])
            && is_array($postvar[$field])) {
            $multiple_values = $postvar[$field];
        } else {
            $multiple_values = array();
        }
        // Ajout des liaisons
        $nb_liens = 0;
        // Boucle sur la liste des valeurs sélectionnées
        foreach ($multiple_values as $value) {
            // Test si la valeur par défaut est sélectionnée
            if ($value == "") {
                continue;
            }
            // On compose les données de l'enregistrement
            $donnees = array(
                $this->clePrimaire => $this->valF[$this->clePrimaire],
                $table_f => $value,
                $table_l => "",
            );
            // On ajoute l'enregistrement
            $obj_l = $this->f->get_inst__om_dbform(array(
                "obj" => $table_l,
                "idx" => "]",
            ));
            $obj_l->ajouter($donnees);
            // On compte le nombre d'éléments ajoutés
            $nb_liens++;
        }
        //
        return $nb_liens;
    }

    /**
     *
     */
    function supprimer_liaisons_table_nan($table) {
        // Suppression de tous les enregistrements correspondants à l'id 
        // de l'objet instancié en cours dans la table NaN
        $sql = "DELETE FROM ".DB_PREFIXE.$table." WHERE ".$this->clePrimaire."=".$this->getVal($this->clePrimaire);
        $res = $this->f->db->query($sql);
        $this->f->addToLog(__METHOD__."(): db->query(\"".$sql."\");", VERBOSE_MODE);
        if (database::isError($res)) {
            die();
        }
    }

    /**
     * Surcharge de la méthode rechercheTable pour éviter de court-circuiter le générateur
     * en devant surcharger la méthode cleSecondaire afin de supprimer les éléments liés dans
     * les tables NaN.
     */
    function rechercheTable(&$dnu1 = null,  $table = "", $field = "", $id = null, $dnu2 = null, $selection = "") {
        //
        if (in_array($table, array_keys($this->liaisons_nan))) {
            //
            $this->addToLog(__METHOD__."(): On ne vérifie pas la table ".$table." car liaison nan.", EXTRA_VERBOSE_MODE);
            return;
        }
        //
        parent::rechercheTable($this->f->db, $table, $field, $id, null, $selection);
    }

    /**
     *
     */
    function get_all_instances() {
        //
        if (is_null($this->all_instances)) {
            //
            $this->all_instances = array();
            //
            if ($this->getVal($this->clePrimaire) != "") {
                //
                $president_instance = -1;
                if ($this->getVal("president") != "") {
                    $president_instance = $this->getVal("president");
                }
                //
                $query = "
                SELECT
                    distinct(reunion_instance.reunion_instance),
                    reunion_instance.courriels
                FROM 
                    ".DB_PREFIXE."reunion_instance
                    LEFT JOIN  ".DB_PREFIXE."reunion_type_reunion_instance
                        ON reunion_instance.reunion_instance=reunion_type_reunion_instance.reunion_instance
                WHERE
                    reunion_type_reunion_instance.reunion_type=".$this->getVal($this->clePrimaire)."
                    OR reunion_instance.reunion_instance = ".$president_instance."";
                //
                $res = $this->f->db->query($query);
                //
                $this->f->addToLog(__METHOD__."(): db->query(\"".$query."\");", VERBOSE_MODE);
                //
                $this->f->isDatabaseError($res);
                //
                $results = array();
                while ($row =& $res->fetchrow(DB_FETCHMODE_ASSOC)) {
                    $results[] = $row;
                }
                $this->all_instances = $results;
            }
        }
        //
        return $this->all_instances;
    }

    /**
     * @return array Liste des catégories rattachées à ce type de réunion
     */
    function get_categories() {
        if (isset($this->_categories) === true
            && is_array($this->_categories) === true) {
            return $this->_categories;
        }
        $query = sprintf(
            'SELECT
                reunion_categorie.reunion_categorie as reunion_categorie_id,
                reunion_categorie.code as reunion_categorie_code,
                reunion_categorie.libelle as reunion_categorie_libelle
            FROM
                %1$sreunion_type_reunion_categorie
                    LEFT JOIN %1$sreunion_categorie
                        ON reunion_type_reunion_categorie.reunion_categorie = reunion_categorie.reunion_categorie
            WHERE
                reunion_type_reunion_categorie.reunion_type=%2$s
            ORDER BY
                reunion_categorie.ordre',
            DB_PREFIXE,
            intval($this->getVal($this->clePrimaire))
        );
        $res = $this->f->db->query($query);
        $this->f->addToLog(__METHOD__."(): db->query(\"".$query."\");", VERBOSE_MODE);
        if ($this->f->isDatabaseError($res, true)) {
            return null;
        }
        $this->_categories = array();
        while ($row =& $res->fetchrow(DB_FETCHMODE_ASSOC)) {
            $this->_categories[] = $row;
        }
        return $this->_categories;
    }
}
