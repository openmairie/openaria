<?php
/**
 * Ce script définit la classe 'dossier_coordination_message_contexte_di'.
 *
 * @package openaria
 * @version SVN : $Id$
 */

require_once "../obj/dossier_coordination_message_contexte_dc.class.php";

/**
 * Définition de la classe 'dossier_coordination_message_contexte_di' (om_dbform).
 *
 * Surcharge de la classe 'dossier_coordination_message_contexte_dc'.
 */
class dossier_coordination_message_contexte_di extends dossier_coordination_message_contexte_dc {

    /**
     *
     */
    protected $_absolute_class_name = "dossier_coordination_message_contexte_di";

    /**
     * Définition des actions disponibles sur la classe.
     *
     * @return void
     */
    function init_class_actions() {
        // On récupère les actions génériques définies dans la méthode
        // d'initialisation de la classe parente
        parent::init_class_actions();
        // ACTION - 021 - redirect_context_di
        // Redirection vers la vue consulter du message dans le contexte de son
        // dossier d'instruction
        $this->class_actions[21] = array(
            "identifier" => "redirect_context_di",
            "view" => "view_redirect_context_di",
            "permission_suffix" => "consulter",
        );
    }

    /**
     * VIEW - view_redirect_context_dc.
     *
     * @return void
     */
    function view_redirect_context_di() {
        //
        $this->checkAccessibility();
        // Si il n'y a pas de DC lié alors on renvoi vers la vue dans le
        // contexte du message
        $inst_dc = $this->get_inst_dossier_coordination();
        if ($inst_dc->getVal($inst_dc->clePrimaire) === "") {
            $redirect =  sprintf(
                "%s&obj=dossier_coordination_message_tous&idx=%s&action=3",
                OM_ROUTE_FORM,
                $this->getVal($this->clePrimaire)
            );
            header("Location: ".$redirect);
            die();
        }
        // Si il n'y a pas de DI lié alors on renvoi vers la vue dans le
        // contexte du DC
        $di = null;
        if ($this->f->get_service_code($_SESSION["service"]) == "acc") {
            $di = $inst_dc->get_id_dossier_instruction("acc");
        } elseif ($this->f->get_service_code($_SESSION["service"]) == "si") {
            $di = $inst_dc->get_id_dossier_instruction("si");
        }
        if ($di === null) {
            $this->view_redirect_context_dc();
            return;
        }
        // Si il y a un DI lié alors on renvoi vers la vue dans le contexte du
        // DI
        $redirect = sprintf(
            "%s&direct_link=true&obj=dossier_instruction&idx=%s&action=3&direct_form=dossier_coordination_message_contexte_di&direct_idx=%s&direct_action=3",
            OM_ROUTE_FORM,
            $di,
            $this->getVal($this->clePrimaire)
        );
        header("Location: ".$redirect);
        die();
    }

}

