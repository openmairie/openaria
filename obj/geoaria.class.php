<?php
/**
 * Ce script définit les classes 'geoaria', 'geoaria_base' et 'geoaria*exception'.
 *
 * La classe 'geoaria' est une classe d'abstraction, spécifique à openARIA,
 * permettant de gérer les requêtes vers divers webservices SIG et ainsi
 * proposer aux utilisateurs des informations géographiques.
 * Cette classe est instanciée et utilisée par d'autres scripts pour
 * gérer notamment la vérification de parcelles et ce peu importe le SIG utilisé.
 * Son objectif est d'instancier les classes spécifiques aux SIG aussi appelées
 * connecteurs correspondant au paramétrage de la collectivité.
 *
 * Ces connecteurs héritent de la classe 'geoaria_base' qui leur sert de modèle.
 *
 * Enfin la classe 'geoaria_exception' permet de gérer les erreurs.
 * Plusieurs classes en héritent afin de spécifier le type d'exception.
 *
 * @package openaria
 * @version SVN : $Id$
 */

/**
 * Définition de la classe 'geoaria'.
 *
 * Abstracteur de la géolocalisation spécifique à openARIA
 */
class geoaria {
    /**
     * Cet attribut permet de stocker l'instance du connecteur SIG utilisé.
     * Sa valeur doit être remplie en fonction du paramétrage de la collectivité.
     *
     * @var object  instance du connecteur SIG
     */
    var $sig = null;

    /**
     * Paramètres de la collectivité fournie à l'abstracteur
     * 
     * @var array
     */
    var $collectivite = null;

    /**
     * Le constructeur instancie la classe du SIG envoyée par le paramétrage
     * de la collectivité.
     *
     * @param array $collectivite Paramètres de la collectivité.
     */
    public function __construct(array $collectivite) {
        // Récupération de la conf sig de la collectivité
        $this->collectivite = $collectivite;
        if(!isset($this->collectivite['sig'])) {
            throw new geoaria_configuration_exception();
        }
        // instanciation du connecteur
        $path = "";
        if(isset($this->collectivite['sig']['path'])) {
            $path = $this->collectivite['sig']['path'];
        }
        $connecteur = 'geoaria_'.$this->collectivite['sig']['connector'];
        if (file_exists($path.$connecteur.'.class.php') === false) {
            throw new geoaria_configuration_exception();
        }
        require_once $path.$connecteur.'.class.php';
        $this->sig = new $connecteur($this->collectivite);
    }

    
    /**
     * Le destructeur permet de détruire la ressource instanciée
     * dans le constructeur
     */
    public function __destruct() {
        if ($this->sig != null) {
            unset($this->sig);
        }
    }


    /**
     * Vérification de l'existence des éléments fournis (numero, voie, dossier ads,
     * liste de parcelles) et tentative de localisation de l'élémént.
     *
     * Le SIG renvoie un booléen vrai si l'élément a pu être localiser, sinon faux.
     * 
     * @param string $obj    Le type d'élément openARIA, "etablissement" ou "dossier".
     * @param string $idx    Identifiant de l'objet.
     * @param array  $params Un tableau contenant un ou plusieurs des éléments suivants :
     *                          array (
     *                              'parcelles' => array (
     *                                  integer array(
     *                                      'quartier' => string,
     *                                      'section' => string,
     *                                      'parcelle' => string
     *                                  ),
     *                                  ...
     *                              )
     *                              'numero' => string,
     *                              'ref_voie' => string,
     *                              'dossier_ads' => string
     *                          ).
     *
     * @throws geoaria_connector_4XX_exception  Demande de dessin OU déjà gécodé
     * @throws geoaria_argument_exception Vérifications des arguments obligatoires
     * @return array x : coordonnées x de l'établissement selon le référentiel passé en paramètre
     *               y : coordonnées y de l'établissement selon le référentiel passé en paramètre
     *               precision : Précision de la localisation (distance en mètres)
     */
    public function geocoder_objet($obj, $idx, array $params) {

        // Vérifications des arguments obligatoires
        if ($obj === '' || $obj === null
            || $idx === '' || $idx === null) {
            //
            throw new geoaria_argument_exception();
        }

        //
        return $this->sig->geocoder_objet($obj, $idx, $params);
    }


    /**
     * Récupération de toutes les contraintes existantes pour une collectivite.
     * 
     * openARIA appelle le SIG en précisant seulement le code INSEE de la collectivite.
     * Il renvoie une collection de l'intégralité des contraintes existantes.
     *
     * @param string $insee Le code INSEE de la commune dont on souhaite synchroniser les
     * contraintes.
     * @return array Tableau de toutes les contraintes existantes.
     *         array(
     *            "M70" => array(
     *                "id_referentiel" => "M70",
     *                "libelle" => "Contrainte 13055.M70",
     *                "texte" => "Une première contrainte du PLU",
     *                "groupe" => "ZONE DU PLU",
     *                "sousgroupe" => "M7",
     *            )
     *         )
     * Le retour peut être un tableau vide s'il n'y a pas de contraintes.
     */
    public function synchro_contraintes($insee = null) {

        // Récupère les contraintes applicables sur la collectivite
        return $this->sig->synchro_contraintes($insee);
    }


    /**
     * Récupération des contraintes applicables sur une ou plusieurs parcelles.
     * 
     * openARIA appelle le web service contrainte en lui fournissant la liste des
     * parcelles dont on souhaite récupérer les contraintes.
     * Le SIG renvoie une collection de contraintes qui s'y appliquent.
     * 
     * @param array $parcelles Un tableau contenant une ou plusieurs parcelles.
     * @throws geoaria_argument_exception Vérifications des arguments obligatoires
     * @return array Tableau des contraintes applicables aux parcelles fournies.
     *         array(
     *            "M70" => array(
     *                "id_referentiel" => "M70",
     *                "libelle" => "Contrainte 13055.M70",
     *                "texte" => "Une première contrainte du PLU",
     *                "groupe" => "ZONE DU PLU",
     *                "sousgroupe" => "M7",
     *            )
     *         )
     * Le retour peut être un tableau vide s'il n'y a pas de contrainte.
     */
    public function recup_contraintes(array $parcelles) {
        // S'il n'y a aucune parcelle en paramètre
        if (!is_array($parcelles) or count($parcelles) == 0){
            // On lève une exception
            throw new geoaria_argument_exception();
        }

        // Récupère le tableau des contraintes applicables sur ces parcelles
        return $this->sig->recup_contraintes($parcelles);
    }


    /**
     * Redirection vers le SIG dans le contexte de visualisation de l'objet,
     * qui peut être un dossier de coordination, un établissement, ou une parcelle.
     *
     * @param string $obj  Le type d'objet à visualiser :
     *                     - 'parcelle'
     *                     - 'etablissement'
     *                     - 'dossier_coordination'
     * @param array  $data Tableau contenant un ou plusieurs identifiants de l'objet :
     *                     - une ou plusieurs parcelles
     *                         array (
     *                             0 => array (
     *                                     'prefixe' => '201',
     *                                     'quartier' => '806',
     *                                     'section' => 'AB',
     *                                     'parcelle' => '0025',
     *                                  ),
     *                             1 => array(
     *                                     'prefixe' => '208',
     *                                     'quartier' => 806,
     *                                     'section' => ' A',
     *                                     'parcelle' => '0050',
     *                                 ),
     *                             ),
     *                         )
     *                     - un ou plusieurs numéros d'établissements
     *                         array(
     *                             0 => 'T5',
     *                             1 => 'T10',
     *                             2 => 'T1111',
     *                         )
     *                     - un ou plusieurs numéros de dossiers de coordination
     *                         array(
     *                             0 => array(
     *                                 'id' => 'VPS-VISIT-000010',
     *                                 'type' => 'VPS',
     *                                 )
     *                             1 => array(
     *                                 'id' => 'PC-PLAN-000001',
     *                                 'type' => 'PC',
     *                                 )
     *                         )
     * 
     * @return string  L'URL du SIG centré sur le ou les éléments si l'objet a été
     *                 précisé, sinon l'URL vers la vue par défaut du SIG.
     */
    public function redirection_web($obj = null, array $data = null) {
        //
        return $this->sig->redirection_web($obj, $data);
    }


    /**
     * Redirection vers le SIG dans le contexte de création manuelle d'un élément sur le
     * SIG.
     *
     * @param string $obj  Le type d'objet :
     *                     - 'etablissement'
     *                     - 'dossier_coordination'
     * @param array  $data Tableau de parmètres
     *        array(
     *           'id' => '5478', // identifiant de l'objet
     *           'type' => 'AT', // type de dossier si objet 'dossier_coordination'
     *           'ref_voie' => '2', // identifiant de la voie
     *        )
     * @throws geoaria_argument_exception Vérifications des arguments obligatoires
     * @return string L'url du SIG pour la création manuelle.
     */
    public function redirection_web_emprise($obj, array $data) {
        // S'il n'a pas été passé d'identifiant(s), ou s'il manque le type d'objet
        if ($obj === "" or isset($data['id']) === false or $data['id'] === "") {
            // On lève une exception
            throw new geoaria_argument_exception();
        }
        // S'il n'y a pas le type du dossier alors que rediriger vers un DC
        if ($obj === 'dossier_coordination'
            && isset($data['type']) === false) {
            throw new geoaria_argument_exception();
        }
        // 
        return $this->sig->redirection_web_emprise($obj, $data);
    }


    /**
     * Récupération d'une liste d'établissements proches de l'objet.
     *
     * openARIA appelle le SIG en lui fournissant un ou plusieurs des éléments suivants :
     * array(
     *     "idcentre" => array (
     *                      "id" => string
     *                      "type" => string // 'etablissement' ou 'dossier_ads'
     *                   )
     *     "listeparcelles" => array (
     *                             0 => array (
     *                                     'prefixe' => '201',
     *                                     'quartier' => '806',
     *                                     'section' => 'AB',
     *                                     'parcelle' => '0025',
     *                                  ),
     *                             1 => array(
     *                                     'prefixe' => '208',
     *                                     'quartier' => 806,
     *                                     'section' => ' A',
     *                                     'parcelle' => '0050',
     *                                 ),
     *                             ),
     *                         )
     *     "limite" => string // nombre total maximum de résultats
     *     "distance" => integer // distance maximum acceptée en mètres
     * )
     * 
     * @param array $data Un tableau associatif contenant
     *                    un ou plusieurs des paramètres ci-dessus
     * @throws geoaria_argument_exception Vérifications des arguments obligatoires
     * @return array Tableau d'établissement(s)
     *         array(
     *             "id" => string // code établissement (numéro T)
     *             "distance" => integer // distance en mètres qui sépare
     *             // l'établissement de l'objet qui a été passé en paramètre
     *         )
     */
    public function lister_etablissements_proches(array $data) {
        // S'il n'a pas été passé de paramètres
        if (!is_array($data) or count($data) == 0) {
            // On lève une exception
            throw new geoaria_argument_exception();
        }

        // Retourne un tableau d'établissements
        return $this->sig->lister_etablissements_proches($data);
    }


    /**
     * Récupération des informations sur les propriétaires des parcelles.
     *
     * openARIA appelle le SIG en lui fournissant une ou plusieurs parcelles dont on
     * souhaite obtenir les détails sur leur(s) propriétaire(s). Le résultat est un
     * ensemble de propriétaires confondus (non identifiés par leur parcelle).
     *
     * @param array $parcelles Tableau de parcelles.
     * @throws geoaria_connector_4XX_exception  Parcelle inexistante
     * @throws geoaria_argument_exception Vérifications des arguments obligatoires
     * @header(409)  Simulation d'erreur HTTP
     * @return array Un tableau contenant les informations d'un ou plusieurs propriétaires
     * d'une ou plusieurs parcelles.
     *         array(
     *             array(
     *                 "ident1" => "DUPONT",
     *                 "ident2" => "CLAUDE",
     *                 "adr1" => "4EME ETAGE DROITE",
     *                 "adr2" => "42 AV ROGER SALENGRO",
     *                 "adr3" => "QUARTIER EUROMED",
     *                 "cp_ville_pays" => "13003 MARSEILLE",
     *                 "code_pays" => ""
     *             ),
     *         )
     */
    public function lister_proprietaires_parcelles(array $parcelles) {
        // Si aucune parcelle n'a été fournie
        if (!is_array($parcelles) or count($parcelles) == 0) {
            // On lève une exception
            throw new geoaria_argument_exception();
        }

        // Retourne un tableau contenant les informations des proprétaires.
        return $this->sig->lister_proprietaires_parcelles($parcelles);
    }

    /**
     * Permet de vérifier si la localisation des DC est activée ou non.
     *
     * @return boolean
     */
    function is_localization_dc_enabled() {
        if (isset($this->collectivite['sig'])
            && isset($this->collectivite['sig']['sig_couches_dc'])
            && is_array($this->collectivite['sig']['sig_couches_dc'])
            && count($this->collectivite['sig']['sig_couches_dc']) !== 0
        ) {
            return true;
        }
        return false;
    }

    /**
     * Getter d'une clé de la configuration sig.inc.php du connecteur instancié.
     *
     * @return string
     */
    function get_sig_config($key) {
        return $this->sig->get_sig_config($key);
    }



}

/**
 * Définition de la classe 'geoaria_base'.
 *
 * Classe parente de tous les connecteurs SIG
 */
class geoaria_base {


     /**
     * Paramètres de connexion au sig
     *
     * @var array
     */
    var $sig_parameters = array();

    /**
     * Paramètres de la collectivite
     *
     * @var array
     */
    var $collectivite_parameters = array();


    /**
     * Le constructeur instancie le connecteur SIG selon la configuration
     *
     * @param array $collectivite Configuration du connecteur.
     */
    public function __construct(array $collectivite) {
        // Config du connecteur SIG de la collectivité en attribut
        $this->set_sig_config($collectivite['sig']);
    }


    public function redirection_web($obj, array $data) {
        // Cette méthode doit être implémentée par tous les connecteurs
        throw new geoaria_connector_method_not_implemented_exception();
    }


    public function redirection_web_emprise($obj, array $data) {
        // Cette méthode doit être implémentée par tous les connecteurs
        throw new geoaria_connector_method_not_implemented_exception();
    }


    public function geocoder_objet($obj, $idx, array $params) {
        // Cette méthode doit être implémentée par tous les connecteurs
        throw new geoaria_connector_method_not_implemented_exception();
    }


    public function synchro_contraintes($code_insee = null) {
        // Cette méthode doit être implémentée par tous les connecteurs
        throw new geoaria_connector_method_not_implemented_exception();
    }


    public function recup_contraintes(array $parcelles) {
        // Cette méthode doit être implémentée par tous les connecteurs
        throw new geoaria_connector_method_not_implemented_exception();
    }


    public function lister_etablissements_proches(array $params) {
        // Cette méthode doit être implémentée par tous les connecteurs
        throw new geoaria_connector_method_not_implemented_exception();
    }


    public function lister_proprietaires_parcelles(array $parcelles) {
        // Cette méthode doit être implémentée par tous les connecteurs
        throw new geoaria_connector_method_not_implemented_exception();
    }


    /**
     * Définit l'attribut contenant le tableau de configuration du SIG.
     *
     * @param array $conf Tableau de config.
     */
    public function set_sig_config(array $conf) {
        $this->sig_parameters = $conf;
    }


    /**
     * Permet de récupérer un élement de configuration.
     *
     * @param string $key Nom de la clé de l'élément.
     *
     * @return string Valeur de la config.
     */
    public function get_sig_config($key) {
        if(isset($this->sig_parameters[$key]) === false) {
            throw new geoaria_configuration_exception();
            
        }
        return $this->sig_parameters[$key];
    }


}

/**
 * Définition de la classe 'geoaria_exception'.
 *
 * Classe gérant les erreurs (une exception est levée pour chacune).
 */
class geoaria_exception extends Exception {

    /**
     * Donnée supplémentaire permettant de caractériser les erreurs remontées par
     * le SIG
     *
     * @var string
     */
    var $typeException;

    /**
     * Construit l'exception
     *
     * @param string    $message       Le message de l'exception à lancer.
     * @param integer   $code          Le code de l'exception.
     * @param Exception $previous      L'exception précédente, utilisée pour le chaînage d'exception.
     * @param String    $typeException Description de l'exception.
     */
    public function __construct($message = "" , $code = 0, Exception $previous = null, $typeException = "") {
        parent::__construct($message, $code, $previous);
        $this->set_typeException($typeException);
        logger::instance()->writeErrorLogToFile();
        // Le logger est susceptible d'être appelé plusieurs fois successivement.
        // On clean les logs après avoir écrit dans le fichier afin d'éviter les doublons.
        logger::instance()->cleanLog();
    }


    /**
     * Cette fonction ajoute dans le fichier de log.
     * 
     * @param string $code Le nom de fichier, ou l'identifiant du fichier.
     * @param string $msg  Le message a logger.
     */
    protected function addToLog($code, $msg) {
        require_once PATH_OPENMAIRIE."om_logger.class.php";
        logger::instance()->log("SIG Connector - Error code : ".$code." -> ".$msg);
    }


    /**
     * Setter
     *
     * @param String $typeException Type d'erreur.
     */
    public function set_typeException($typeException) {
        $this->typeException = $typeException;
    }


    /**
     * Accesseur.
     *
     * @return String Type d'erreur.
     */
    public function get_typeException() {
        return $this->typeException;
    }



}

/**
 * Définition de la classe 'geoaria_bdd_exception'.
 */
class geoaria_bdd_exception extends geoaria_exception {


    /**
     * Redéfinition du message d'erreur.
     */
    public function __construct() {
        $message = __("Erreur de base de donnees. Contactez votre administrateur.");
        // Appel du parent
        parent::__construct($message);
    }


}

/**
 * Définition de la classe 'geoaria_configuration_exception'.
 */
class geoaria_configuration_exception extends geoaria_exception {



    /**
     * Redéfinition du message d'erreur.
     */
    public function __construct() {
        $message = __("Erreur de configuration SIG.")." ".__("Veuillez contacter votre administrateur.");
        // Appel du parent
        parent::__construct($message);
    }


}

/**
 * Définition de la classe 'geoaria_argument_exception'.
 */
class geoaria_argument_exception extends geoaria_exception {


    /**
     * Redéfinition du message d'erreur.
     */
    public function __construct($message = "") {
        if($message == "") {
            $message = __("Paramètres d'appel au SIG non valides.")." ".__("Veuillez contacter votre administrateur.");
        }
        // Appel du parent
        parent::__construct($message);
    }


}

/**
 * Définition de la classe 'geoaria_om_parameter_exception'.
 */
class geoaria_om_parameter_exception extends geoaria_exception {


    /**
     * Redéfinition du message d'erreur.
     */
    public function __construct($message = "") {
        if($message == "") {
            $message = __("Paramètres d'appel au SIG non valides.")." ".__("Veuillez vérifier les paramètres généraux.");
        }
        // Appel du parent
        parent::__construct($message);
    }


}


/**
 * Définition de la classe 'geoaria_connector_method_not_implemented_exception'.
 *
 * Classe de gestion des exceptions sur les methodes non implémentées
 */
class geoaria_connector_method_not_implemented_exception extends geoaria_exception {


    /**
     * Redéfinition du message d'erreur.
     */
    public function __construct() {
        $message = __("Erreur lors de la connexion au SIG.")." ".__("Veuillez contacter votre administrateur.");
        // Appel du parent
        parent::__construct($message);
    }


}

/**
 * Définition de la classe 'geoaria_connector_4XX_exception'.
 *
 * Classe de gestion des exceptions retournée lors d'un code 4XX
 */
class geoaria_connector_4XX_exception extends geoaria_exception {


    /**
     * Redéfinition du message d'erreur.
     *
     * @param string  $message Message d'erreur http.
     * @param integer $code    Code de l'erreur http.
     */
    public function __construct($message = "" , $code = 0, Exception $previous = null, $typeException = "") {
        // Log de l'erreur technique
        $this->addToLog($code, $message);
        // Création du log utilisateur
        $message = __("Erreur lors de la connexion au SIG.")." ".__("Veuillez contacter votre administrateur.");
        // Appel du parent
        parent::__construct($message , $code, $previous, $typeException);
    }


}

/**
 * Définition de la classe 'geoaria_connector_5XX_exception'.
 *
 * Classe de gestion des exceptions retournée lors d'un code 5XX
 */
class geoaria_connector_5XX_exception extends geoaria_exception {


    /**
     * Log et redéfinition du message d'erreur.
     *
     * @param string    $message       Le message de l'exception à lancer.
     * @param integer   $code          Le code de l'exception.
     * @param Exception $previous      L'exception précédente, utilisée pour le chaînage d'exception.
     * @param String    $typeException Description de l'exception.
     */
    public function __construct($message = "" , $code = 0, Exception $previous = null, $typeException = "") {
        // Log de l'erreur technique
        $this->addToLog($code, $message);
        // Création du log utilisateur
        $message = __("Erreur de traitement du SIG.")." ".__("Veuillez contacter le service responsable du SIG");
        // Appel du parent
        parent::__construct($message, $code, $previous, $typeException);
    }


}

/**
 * Définition de la classe 'geoaria_connector_exception'.
 *
 * Classe de gestion des exceptions génériques remontées par le générateur
 */
class geoaria_connector_exception extends geoaria_exception {


    /**
     * Log et redéfinition du message d'erreur.
     *
     * @param string    $message       Le message de l'exception à lancer.
     * @param integer   $code          Le code de l'exception.
     * @param Exception $previous      L'exception précédente, utilisée pour le chaînage d'exception.
     * @param String    $typeException Description de l'exception.
     */
    public function __construct($message = "" , $code = 0, Exception $previous = null, $typeException = "") {
        // Log de l'erreur technique
        $this->addToLog($code, $message);
        // Création du log utilisateur
        $message = __("Erreur SIG.")." ".__("Veuillez contacter votre administrateur.");
        // Appel du parent
        parent::__construct($message, $code, $previous, $typeException);
    }


}

