<?php
/**
 * Ce script définit la classe 'etablissement_referentiel_erp'.
 *
 * @package openaria
 * @version SVN : $Id$
 */

require_once "../obj/etablissement_tous.class.php";

/**
 * Définition de la classe 'etablissement_referentiel_erp' (om_dbform).
 *
 * Surcharge de la classe 'etablissement'.
 */
class etablissement_referentiel_erp extends etablissement_tous {

    /**
     *
     */
    protected $_absolute_class_name = "etablissement_referentiel_erp";

}

