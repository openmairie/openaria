<?php
/**
 * Ce script définit la classe 'etablissement_unite__contexte_di_analyse__ua_valide_sur_etab'.
 *
 * @package openaria
 * @version SVN : $Id$
 */

require_once "../obj/etablissement_unite.class.php";

/**
 * Définition de la classe 'etablissement_unite__contexte_di_analyse__ua_valide_sur_etab' (om_dbform).
 *
 * Surcharge de la classe 'etablissement_unite'.
 */
class etablissement_unite__contexte_di_analyse__ua_valide_sur_etab extends etablissement_unite {

    /**
     *
     */
    protected $_absolute_class_name = "etablissement_unite__contexte_di_analyse__ua_valide_sur_etab";

    /**
     * Définition des actions disponibles sur la classe.
     *
     * @return void
     */
    function init_class_actions() {

        // On récupère les actions génériques définies dans la méthode 
        // d'initialisation de la classe parente
        parent::init_class_actions();

        /**
         *
         */
        //
        $this->class_actions = array();
        foreach (array(3, 4, ) as $key) {
            if (isset($this->class_actions_available[$key])) {
                $this->class_actions[$key] = $this->class_actions_available[$key];
            }
        }
    }
}

