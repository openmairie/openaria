<?php
//$Id$ 
//gen openMairie le 03/05/2018 09:12

require_once "../obj/om_dbform.class.php";

class periodicite_visites_gen extends om_dbform {

    protected $_absolute_class_name = "periodicite_visites";

    var $table = "periodicite_visites";
    var $clePrimaire = "periodicite_visites";
    var $typeCle = "N";
    var $required_field = array(
        "etablissement_categorie",
        "etablissement_type",
        "periodicite",
        "periodicite_visites"
    );
    
    var $foreign_keys_extended = array(
        "etablissement_categorie" => array("etablissement_categorie", ),
        "etablissement_type" => array("etablissement_type", ),
    );
    
    /**
     *
     * @return string
     */
    function get_default_libelle() {
        return $this->getVal($this->clePrimaire)."&nbsp;".$this->getVal("periodicite");
    }

    /**
     *
     * @return array
     */
    function get_var_sql_forminc__champs() {
        return array(
            "periodicite_visites",
            "periodicite",
            "etablissement_type",
            "etablissement_categorie",
            "commentaire",
            "avec_locaux_sommeil",
            "sans_locaux_sommeil",
        );
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_etablissement_categorie() {
        return "SELECT etablissement_categorie.etablissement_categorie, etablissement_categorie.libelle FROM ".DB_PREFIXE."etablissement_categorie WHERE ((etablissement_categorie.om_validite_debut IS NULL AND (etablissement_categorie.om_validite_fin IS NULL OR etablissement_categorie.om_validite_fin > CURRENT_DATE)) OR (etablissement_categorie.om_validite_debut <= CURRENT_DATE AND (etablissement_categorie.om_validite_fin IS NULL OR etablissement_categorie.om_validite_fin > CURRENT_DATE))) ORDER BY etablissement_categorie.libelle ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_etablissement_categorie_by_id() {
        return "SELECT etablissement_categorie.etablissement_categorie, etablissement_categorie.libelle FROM ".DB_PREFIXE."etablissement_categorie WHERE etablissement_categorie = <idx>";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_etablissement_type() {
        return "SELECT etablissement_type.etablissement_type, etablissement_type.libelle FROM ".DB_PREFIXE."etablissement_type WHERE ((etablissement_type.om_validite_debut IS NULL AND (etablissement_type.om_validite_fin IS NULL OR etablissement_type.om_validite_fin > CURRENT_DATE)) OR (etablissement_type.om_validite_debut <= CURRENT_DATE AND (etablissement_type.om_validite_fin IS NULL OR etablissement_type.om_validite_fin > CURRENT_DATE))) ORDER BY etablissement_type.libelle ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_etablissement_type_by_id() {
        return "SELECT etablissement_type.etablissement_type, etablissement_type.libelle FROM ".DB_PREFIXE."etablissement_type WHERE etablissement_type = <idx>";
    }




    function setvalF($val = array()) {
        //affectation valeur formulaire
        if (!is_numeric($val['periodicite_visites'])) {
            $this->valF['periodicite_visites'] = ""; // -> requis
        } else {
            $this->valF['periodicite_visites'] = $val['periodicite_visites'];
        }
        if (!is_numeric($val['periodicite'])) {
            $this->valF['periodicite'] = ""; // -> requis
        } else {
            $this->valF['periodicite'] = $val['periodicite'];
        }
        if (!is_numeric($val['etablissement_type'])) {
            $this->valF['etablissement_type'] = ""; // -> requis
        } else {
            $this->valF['etablissement_type'] = $val['etablissement_type'];
        }
        if (!is_numeric($val['etablissement_categorie'])) {
            $this->valF['etablissement_categorie'] = ""; // -> requis
        } else {
            $this->valF['etablissement_categorie'] = $val['etablissement_categorie'];
        }
            $this->valF['commentaire'] = $val['commentaire'];
        if ($val['avec_locaux_sommeil'] == 1 || $val['avec_locaux_sommeil'] == "t" || $val['avec_locaux_sommeil'] == "Oui") {
            $this->valF['avec_locaux_sommeil'] = true;
        } else {
            $this->valF['avec_locaux_sommeil'] = false;
        }
        if ($val['sans_locaux_sommeil'] == 1 || $val['sans_locaux_sommeil'] == "t" || $val['sans_locaux_sommeil'] == "Oui") {
            $this->valF['sans_locaux_sommeil'] = true;
        } else {
            $this->valF['sans_locaux_sommeil'] = false;
        }
    }

    //=================================================
    //cle primaire automatique [automatic primary key]
    //==================================================

    function setId(&$dnu1 = null) {
    //numero automatique
        $this->valF[$this->clePrimaire] = $this->f->db->nextId(DB_PREFIXE.$this->table);
    }

    function setValFAjout($val = array()) {
    //numero automatique -> pas de controle ajout cle primaire
    }

    function verifierAjout($val = array(), &$dnu1 = null) {
    //numero automatique -> pas de verfication de cle primaire
    }

    //==========================
    // Formulaire  [form]
    //==========================
    /**
     *
     */
    function setType(&$form, $maj) {
        // Récupération du mode de l'action
        $crud = $this->get_action_crud($maj);

        // MODE AJOUTER
        if ($maj == 0 || $crud == 'create') {
            $form->setType("periodicite_visites", "hidden");
            $form->setType("periodicite", "text");
            if ($this->is_in_context_of_foreign_key("etablissement_type", $this->retourformulaire)) {
                $form->setType("etablissement_type", "selecthiddenstatic");
            } else {
                $form->setType("etablissement_type", "select");
            }
            if ($this->is_in_context_of_foreign_key("etablissement_categorie", $this->retourformulaire)) {
                $form->setType("etablissement_categorie", "selecthiddenstatic");
            } else {
                $form->setType("etablissement_categorie", "select");
            }
            $form->setType("commentaire", "textarea");
            $form->setType("avec_locaux_sommeil", "checkbox");
            $form->setType("sans_locaux_sommeil", "checkbox");
        }

        // MDOE MODIFIER
        if ($maj == 1 || $crud == 'update') {
            $form->setType("periodicite_visites", "hiddenstatic");
            $form->setType("periodicite", "text");
            if ($this->is_in_context_of_foreign_key("etablissement_type", $this->retourformulaire)) {
                $form->setType("etablissement_type", "selecthiddenstatic");
            } else {
                $form->setType("etablissement_type", "select");
            }
            if ($this->is_in_context_of_foreign_key("etablissement_categorie", $this->retourformulaire)) {
                $form->setType("etablissement_categorie", "selecthiddenstatic");
            } else {
                $form->setType("etablissement_categorie", "select");
            }
            $form->setType("commentaire", "textarea");
            $form->setType("avec_locaux_sommeil", "checkbox");
            $form->setType("sans_locaux_sommeil", "checkbox");
        }

        // MODE SUPPRIMER
        if ($maj == 2 || $crud == 'delete') {
            $form->setType("periodicite_visites", "hiddenstatic");
            $form->setType("periodicite", "hiddenstatic");
            $form->setType("etablissement_type", "selectstatic");
            $form->setType("etablissement_categorie", "selectstatic");
            $form->setType("commentaire", "hiddenstatic");
            $form->setType("avec_locaux_sommeil", "hiddenstatic");
            $form->setType("sans_locaux_sommeil", "hiddenstatic");
        }

        // MODE CONSULTER
        if ($maj == 3 || $crud == 'read') {
            $form->setType("periodicite_visites", "static");
            $form->setType("periodicite", "static");
            $form->setType("etablissement_type", "selectstatic");
            $form->setType("etablissement_categorie", "selectstatic");
            $form->setType("commentaire", "textareastatic");
            $form->setType("avec_locaux_sommeil", "checkboxstatic");
            $form->setType("sans_locaux_sommeil", "checkboxstatic");
        }

    }


    function setOnchange(&$form, $maj) {
    //javascript controle client
        $form->setOnchange('periodicite_visites','VerifNum(this)');
        $form->setOnchange('periodicite','VerifNum(this)');
        $form->setOnchange('etablissement_type','VerifNum(this)');
        $form->setOnchange('etablissement_categorie','VerifNum(this)');
    }
    /**
     * Methode setTaille
     */
    function setTaille(&$form, $maj) {
        $form->setTaille("periodicite_visites", 11);
        $form->setTaille("periodicite", 11);
        $form->setTaille("etablissement_type", 11);
        $form->setTaille("etablissement_categorie", 11);
        $form->setTaille("commentaire", 80);
        $form->setTaille("avec_locaux_sommeil", 1);
        $form->setTaille("sans_locaux_sommeil", 1);
    }

    /**
     * Methode setMax
     */
    function setMax(&$form, $maj) {
        $form->setMax("periodicite_visites", 11);
        $form->setMax("periodicite", 11);
        $form->setMax("etablissement_type", 11);
        $form->setMax("etablissement_categorie", 11);
        $form->setMax("commentaire", 6);
        $form->setMax("avec_locaux_sommeil", 1);
        $form->setMax("sans_locaux_sommeil", 1);
    }


    function setLib(&$form, $maj) {
    //libelle des champs
        $form->setLib('periodicite_visites', __('periodicite_visites'));
        $form->setLib('periodicite', __('periodicite'));
        $form->setLib('etablissement_type', __('etablissement_type'));
        $form->setLib('etablissement_categorie', __('etablissement_categorie'));
        $form->setLib('commentaire', __('commentaire'));
        $form->setLib('avec_locaux_sommeil', __('avec_locaux_sommeil'));
        $form->setLib('sans_locaux_sommeil', __('sans_locaux_sommeil'));
    }
    /**
     *
     */
    function setSelect(&$form, $maj, &$dnu1 = null, $dnu2 = null) {

        // etablissement_categorie
        $this->init_select(
            $form, 
            $this->f->db,
            $maj,
            null,
            "etablissement_categorie",
            $this->get_var_sql_forminc__sql("etablissement_categorie"),
            $this->get_var_sql_forminc__sql("etablissement_categorie_by_id"),
            true
        );
        // etablissement_type
        $this->init_select(
            $form, 
            $this->f->db,
            $maj,
            null,
            "etablissement_type",
            $this->get_var_sql_forminc__sql("etablissement_type"),
            $this->get_var_sql_forminc__sql("etablissement_type_by_id"),
            true
        );
    }


    //==================================
    // sous Formulaire
    //==================================
    

    function setValsousformulaire(&$form, $maj, $validation, $idxformulaire, $retourformulaire, $typeformulaire, &$dnu1 = null, $dnu2 = null) {
        $this->retourformulaire = $retourformulaire;
        if($validation == 0) {
            if($this->is_in_context_of_foreign_key('etablissement_categorie', $this->retourformulaire))
                $form->setVal('etablissement_categorie', $idxformulaire);
            if($this->is_in_context_of_foreign_key('etablissement_type', $this->retourformulaire))
                $form->setVal('etablissement_type', $idxformulaire);
        }// fin validation
        $this->set_form_default_values($form, $maj, $validation);
    }// fin setValsousformulaire

    //==================================
    // cle secondaire
    //==================================
    

}
