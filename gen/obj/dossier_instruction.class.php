<?php
//$Id$ 
//gen openMairie le 20/05/2020 18:03

require_once "../obj/om_dbform.class.php";

class dossier_instruction_gen extends om_dbform {

    protected $_absolute_class_name = "dossier_instruction";

    var $table = "dossier_instruction";
    var $clePrimaire = "dossier_instruction";
    var $typeCle = "N";
    var $required_field = array(
        "dossier_coordination",
        "dossier_instruction",
        "service"
    );
    var $unique_key = array(
      "libelle",
    );
    var $foreign_keys_extended = array(
        "autorite_competente" => array("autorite_competente", ),
        "dossier_coordination" => array("dossier_coordination", "dossier_coordination_nouveau", "dossier_coordination_a_qualifier", "dossier_coordination_a_cloturer", ),
        "service" => array("service", ),
        "acteur" => array("acteur", ),
    );
    
    /**
     *
     * @return string
     */
    function get_default_libelle() {
        return $this->getVal($this->clePrimaire)."&nbsp;".$this->getVal("libelle");
    }

    /**
     *
     * @return array
     */
    function get_var_sql_forminc__champs() {
        return array(
            "dossier_instruction",
            "libelle",
            "dossier_coordination",
            "technicien",
            "service",
            "a_qualifier",
            "incompletude",
            "piece_attendue",
            "description",
            "notes",
            "autorite_competente",
            "dossier_cloture",
            "prioritaire",
            "statut",
            "date_cloture",
            "date_ouverture",
        );
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_autorite_competente() {
        return "SELECT autorite_competente.autorite_competente, autorite_competente.libelle FROM ".DB_PREFIXE."autorite_competente ORDER BY autorite_competente.libelle ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_autorite_competente_by_id() {
        return "SELECT autorite_competente.autorite_competente, autorite_competente.libelle FROM ".DB_PREFIXE."autorite_competente WHERE autorite_competente = <idx>";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_dossier_coordination() {
        return "SELECT dossier_coordination.dossier_coordination, dossier_coordination.libelle FROM ".DB_PREFIXE."dossier_coordination ORDER BY dossier_coordination.libelle ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_dossier_coordination_by_id() {
        return "SELECT dossier_coordination.dossier_coordination, dossier_coordination.libelle FROM ".DB_PREFIXE."dossier_coordination WHERE dossier_coordination = <idx>";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_service() {
        return "SELECT service.service, service.libelle FROM ".DB_PREFIXE."service WHERE ((service.om_validite_debut IS NULL AND (service.om_validite_fin IS NULL OR service.om_validite_fin > CURRENT_DATE)) OR (service.om_validite_debut <= CURRENT_DATE AND (service.om_validite_fin IS NULL OR service.om_validite_fin > CURRENT_DATE))) ORDER BY service.libelle ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_service_by_id() {
        return "SELECT service.service, service.libelle FROM ".DB_PREFIXE."service WHERE service = <idx>";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_technicien() {
        return "SELECT acteur.acteur, acteur.nom_prenom FROM ".DB_PREFIXE."acteur WHERE ((acteur.om_validite_debut IS NULL AND (acteur.om_validite_fin IS NULL OR acteur.om_validite_fin > CURRENT_DATE)) OR (acteur.om_validite_debut <= CURRENT_DATE AND (acteur.om_validite_fin IS NULL OR acteur.om_validite_fin > CURRENT_DATE))) ORDER BY acteur.nom_prenom ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_technicien_by_id() {
        return "SELECT acteur.acteur, acteur.nom_prenom FROM ".DB_PREFIXE."acteur WHERE acteur = <idx>";
    }




    function setvalF($val = array()) {
        //affectation valeur formulaire
        if (!is_numeric($val['dossier_instruction'])) {
            $this->valF['dossier_instruction'] = ""; // -> requis
        } else {
            $this->valF['dossier_instruction'] = $val['dossier_instruction'];
        }
        if ($val['libelle'] == "") {
            $this->valF['libelle'] = NULL;
        } else {
            $this->valF['libelle'] = $val['libelle'];
        }
        if (!is_numeric($val['dossier_coordination'])) {
            $this->valF['dossier_coordination'] = ""; // -> requis
        } else {
            $this->valF['dossier_coordination'] = $val['dossier_coordination'];
        }
        if (!is_numeric($val['technicien'])) {
            $this->valF['technicien'] = NULL;
        } else {
            $this->valF['technicien'] = $val['technicien'];
        }
        if (!is_numeric($val['service'])) {
            $this->valF['service'] = ""; // -> requis
        } else {
            $this->valF['service'] = $val['service'];
        }
        if ($val['a_qualifier'] == 1 || $val['a_qualifier'] == "t" || $val['a_qualifier'] == "Oui") {
            $this->valF['a_qualifier'] = true;
        } else {
            $this->valF['a_qualifier'] = false;
        }
        if ($val['incompletude'] == 1 || $val['incompletude'] == "t" || $val['incompletude'] == "Oui") {
            $this->valF['incompletude'] = true;
        } else {
            $this->valF['incompletude'] = false;
        }
            $this->valF['piece_attendue'] = $val['piece_attendue'];
            $this->valF['description'] = $val['description'];
            $this->valF['notes'] = $val['notes'];
        if (!is_numeric($val['autorite_competente'])) {
            $this->valF['autorite_competente'] = NULL;
        } else {
            $this->valF['autorite_competente'] = $val['autorite_competente'];
        }
        if ($val['dossier_cloture'] == 1 || $val['dossier_cloture'] == "t" || $val['dossier_cloture'] == "Oui") {
            $this->valF['dossier_cloture'] = true;
        } else {
            $this->valF['dossier_cloture'] = false;
        }
        if ($val['prioritaire'] == 1 || $val['prioritaire'] == "t" || $val['prioritaire'] == "Oui") {
            $this->valF['prioritaire'] = true;
        } else {
            $this->valF['prioritaire'] = false;
        }
        if ($val['statut'] == "") {
            $this->valF['statut'] = NULL;
        } else {
            $this->valF['statut'] = $val['statut'];
        }
        if ($val['date_cloture'] != "") {
            $this->valF['date_cloture'] = $this->dateDB($val['date_cloture']);
        } else {
            $this->valF['date_cloture'] = NULL;
        }
        if ($val['date_ouverture'] != "") {
            $this->valF['date_ouverture'] = $this->dateDB($val['date_ouverture']);
        } else {
            $this->valF['date_ouverture'] = NULL;
        }
    }

    //=================================================
    //cle primaire automatique [automatic primary key]
    //==================================================

    function setId(&$dnu1 = null) {
    //numero automatique
        $this->valF[$this->clePrimaire] = $this->f->db->nextId(DB_PREFIXE.$this->table);
    }

    function setValFAjout($val = array()) {
    //numero automatique -> pas de controle ajout cle primaire
    }

    function verifierAjout($val = array(), &$dnu1 = null) {
    //numero automatique -> pas de verfication de cle primaire
    }

    //==========================
    // Formulaire  [form]
    //==========================
    /**
     *
     */
    function setType(&$form, $maj) {
        // Récupération du mode de l'action
        $crud = $this->get_action_crud($maj);

        // MODE AJOUTER
        if ($maj == 0 || $crud == 'create') {
            $form->setType("dossier_instruction", "hidden");
            $form->setType("libelle", "text");
            if ($this->is_in_context_of_foreign_key("dossier_coordination", $this->retourformulaire)) {
                $form->setType("dossier_coordination", "selecthiddenstatic");
            } else {
                $form->setType("dossier_coordination", "select");
            }
            if ($this->is_in_context_of_foreign_key("acteur", $this->retourformulaire)) {
                $form->setType("technicien", "selecthiddenstatic");
            } else {
                $form->setType("technicien", "select");
            }
            if ($this->is_in_context_of_foreign_key("service", $this->retourformulaire)) {
                $form->setType("service", "selecthiddenstatic");
            } else {
                $form->setType("service", "select");
            }
            $form->setType("a_qualifier", "checkbox");
            $form->setType("incompletude", "checkbox");
            $form->setType("piece_attendue", "textarea");
            $form->setType("description", "textarea");
            $form->setType("notes", "textarea");
            if ($this->is_in_context_of_foreign_key("autorite_competente", $this->retourformulaire)) {
                $form->setType("autorite_competente", "selecthiddenstatic");
            } else {
                $form->setType("autorite_competente", "select");
            }
            $form->setType("dossier_cloture", "checkbox");
            $form->setType("prioritaire", "checkbox");
            $form->setType("statut", "text");
            $form->setType("date_cloture", "date");
            $form->setType("date_ouverture", "date");
        }

        // MDOE MODIFIER
        if ($maj == 1 || $crud == 'update') {
            $form->setType("dossier_instruction", "hiddenstatic");
            $form->setType("libelle", "text");
            if ($this->is_in_context_of_foreign_key("dossier_coordination", $this->retourformulaire)) {
                $form->setType("dossier_coordination", "selecthiddenstatic");
            } else {
                $form->setType("dossier_coordination", "select");
            }
            if ($this->is_in_context_of_foreign_key("acteur", $this->retourformulaire)) {
                $form->setType("technicien", "selecthiddenstatic");
            } else {
                $form->setType("technicien", "select");
            }
            if ($this->is_in_context_of_foreign_key("service", $this->retourformulaire)) {
                $form->setType("service", "selecthiddenstatic");
            } else {
                $form->setType("service", "select");
            }
            $form->setType("a_qualifier", "checkbox");
            $form->setType("incompletude", "checkbox");
            $form->setType("piece_attendue", "textarea");
            $form->setType("description", "textarea");
            $form->setType("notes", "textarea");
            if ($this->is_in_context_of_foreign_key("autorite_competente", $this->retourformulaire)) {
                $form->setType("autorite_competente", "selecthiddenstatic");
            } else {
                $form->setType("autorite_competente", "select");
            }
            $form->setType("dossier_cloture", "checkbox");
            $form->setType("prioritaire", "checkbox");
            $form->setType("statut", "text");
            $form->setType("date_cloture", "date");
            $form->setType("date_ouverture", "date");
        }

        // MODE SUPPRIMER
        if ($maj == 2 || $crud == 'delete') {
            $form->setType("dossier_instruction", "hiddenstatic");
            $form->setType("libelle", "hiddenstatic");
            $form->setType("dossier_coordination", "selectstatic");
            $form->setType("technicien", "selectstatic");
            $form->setType("service", "selectstatic");
            $form->setType("a_qualifier", "hiddenstatic");
            $form->setType("incompletude", "hiddenstatic");
            $form->setType("piece_attendue", "hiddenstatic");
            $form->setType("description", "hiddenstatic");
            $form->setType("notes", "hiddenstatic");
            $form->setType("autorite_competente", "selectstatic");
            $form->setType("dossier_cloture", "hiddenstatic");
            $form->setType("prioritaire", "hiddenstatic");
            $form->setType("statut", "hiddenstatic");
            $form->setType("date_cloture", "hiddenstatic");
            $form->setType("date_ouverture", "hiddenstatic");
        }

        // MODE CONSULTER
        if ($maj == 3 || $crud == 'read') {
            $form->setType("dossier_instruction", "static");
            $form->setType("libelle", "static");
            $form->setType("dossier_coordination", "selectstatic");
            $form->setType("technicien", "selectstatic");
            $form->setType("service", "selectstatic");
            $form->setType("a_qualifier", "checkboxstatic");
            $form->setType("incompletude", "checkboxstatic");
            $form->setType("piece_attendue", "textareastatic");
            $form->setType("description", "textareastatic");
            $form->setType("notes", "textareastatic");
            $form->setType("autorite_competente", "selectstatic");
            $form->setType("dossier_cloture", "checkboxstatic");
            $form->setType("prioritaire", "checkboxstatic");
            $form->setType("statut", "static");
            $form->setType("date_cloture", "datestatic");
            $form->setType("date_ouverture", "datestatic");
        }

    }


    function setOnchange(&$form, $maj) {
    //javascript controle client
        $form->setOnchange('dossier_instruction','VerifNum(this)');
        $form->setOnchange('dossier_coordination','VerifNum(this)');
        $form->setOnchange('technicien','VerifNum(this)');
        $form->setOnchange('service','VerifNum(this)');
        $form->setOnchange('autorite_competente','VerifNum(this)');
        $form->setOnchange('date_cloture','fdate(this)');
        $form->setOnchange('date_ouverture','fdate(this)');
    }
    /**
     * Methode setTaille
     */
    function setTaille(&$form, $maj) {
        $form->setTaille("dossier_instruction", 11);
        $form->setTaille("libelle", 30);
        $form->setTaille("dossier_coordination", 11);
        $form->setTaille("technicien", 11);
        $form->setTaille("service", 11);
        $form->setTaille("a_qualifier", 1);
        $form->setTaille("incompletude", 1);
        $form->setTaille("piece_attendue", 80);
        $form->setTaille("description", 80);
        $form->setTaille("notes", 80);
        $form->setTaille("autorite_competente", 11);
        $form->setTaille("dossier_cloture", 1);
        $form->setTaille("prioritaire", 1);
        $form->setTaille("statut", 30);
        $form->setTaille("date_cloture", 12);
        $form->setTaille("date_ouverture", 12);
    }

    /**
     * Methode setMax
     */
    function setMax(&$form, $maj) {
        $form->setMax("dossier_instruction", 11);
        $form->setMax("libelle", 100);
        $form->setMax("dossier_coordination", 11);
        $form->setMax("technicien", 11);
        $form->setMax("service", 11);
        $form->setMax("a_qualifier", 1);
        $form->setMax("incompletude", 1);
        $form->setMax("piece_attendue", 6);
        $form->setMax("description", 6);
        $form->setMax("notes", 6);
        $form->setMax("autorite_competente", 11);
        $form->setMax("dossier_cloture", 1);
        $form->setMax("prioritaire", 1);
        $form->setMax("statut", 255);
        $form->setMax("date_cloture", 12);
        $form->setMax("date_ouverture", 12);
    }


    function setLib(&$form, $maj) {
    //libelle des champs
        $form->setLib('dossier_instruction', __('dossier_instruction'));
        $form->setLib('libelle', __('libelle'));
        $form->setLib('dossier_coordination', __('dossier_coordination'));
        $form->setLib('technicien', __('technicien'));
        $form->setLib('service', __('service'));
        $form->setLib('a_qualifier', __('a_qualifier'));
        $form->setLib('incompletude', __('incompletude'));
        $form->setLib('piece_attendue', __('piece_attendue'));
        $form->setLib('description', __('description'));
        $form->setLib('notes', __('notes'));
        $form->setLib('autorite_competente', __('autorite_competente'));
        $form->setLib('dossier_cloture', __('dossier_cloture'));
        $form->setLib('prioritaire', __('prioritaire'));
        $form->setLib('statut', __('statut'));
        $form->setLib('date_cloture', __('date_cloture'));
        $form->setLib('date_ouverture', __('date_ouverture'));
    }
    /**
     *
     */
    function setSelect(&$form, $maj, &$dnu1 = null, $dnu2 = null) {

        // autorite_competente
        $this->init_select(
            $form, 
            $this->f->db,
            $maj,
            null,
            "autorite_competente",
            $this->get_var_sql_forminc__sql("autorite_competente"),
            $this->get_var_sql_forminc__sql("autorite_competente_by_id"),
            false
        );
        // dossier_coordination
        $this->init_select(
            $form, 
            $this->f->db,
            $maj,
            null,
            "dossier_coordination",
            $this->get_var_sql_forminc__sql("dossier_coordination"),
            $this->get_var_sql_forminc__sql("dossier_coordination_by_id"),
            false
        );
        // service
        $this->init_select(
            $form, 
            $this->f->db,
            $maj,
            null,
            "service",
            $this->get_var_sql_forminc__sql("service"),
            $this->get_var_sql_forminc__sql("service_by_id"),
            true
        );
        // technicien
        $this->init_select(
            $form, 
            $this->f->db,
            $maj,
            null,
            "technicien",
            $this->get_var_sql_forminc__sql("technicien"),
            $this->get_var_sql_forminc__sql("technicien_by_id"),
            true
        );
    }


    //==================================
    // sous Formulaire
    //==================================
    

    function setValsousformulaire(&$form, $maj, $validation, $idxformulaire, $retourformulaire, $typeformulaire, &$dnu1 = null, $dnu2 = null) {
        $this->retourformulaire = $retourformulaire;
        if($validation == 0) {
            if($this->is_in_context_of_foreign_key('autorite_competente', $this->retourformulaire))
                $form->setVal('autorite_competente', $idxformulaire);
            if($this->is_in_context_of_foreign_key('dossier_coordination', $this->retourformulaire))
                $form->setVal('dossier_coordination', $idxformulaire);
            if($this->is_in_context_of_foreign_key('service', $this->retourformulaire))
                $form->setVal('service', $idxformulaire);
            if($this->is_in_context_of_foreign_key('acteur', $this->retourformulaire))
                $form->setVal('technicien', $idxformulaire);
        }// fin validation
        $this->set_form_default_values($form, $maj, $validation);
    }// fin setValsousformulaire

    //==================================
    // cle secondaire
    //==================================
    
    /**
     * Methode clesecondaire
     */
    function cleSecondaire($id, &$dnu1 = null, $val = array(), $dnu2 = null) {
        // On appelle la methode de la classe parent
        parent::cleSecondaire($id);
        // Verification de la cle secondaire : analyses
        $this->rechercheTable($this->f->db, "analyses", "dossier_instruction", $id);
        // Verification de la cle secondaire : courrier
        $this->rechercheTable($this->f->db, "courrier", "dossier_instruction", $id);
        // Verification de la cle secondaire : dossier_instruction_reunion
        $this->rechercheTable($this->f->db, "dossier_instruction_reunion", "dossier_instruction", $id);
        // Verification de la cle secondaire : etablissement_unite
        $this->rechercheTable($this->f->db, "etablissement_unite", "dossier_instruction", $id);
        // Verification de la cle secondaire : piece
        $this->rechercheTable($this->f->db, "piece", "dossier_instruction", $id);
        // Verification de la cle secondaire : proces_verbal
        $this->rechercheTable($this->f->db, "proces_verbal", "dossier_instruction", $id);
        // Verification de la cle secondaire : visite
        $this->rechercheTable($this->f->db, "visite", "dossier_instruction", $id);
    }


}
