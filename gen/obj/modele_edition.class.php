<?php
//$Id$ 
//gen openMairie le 18/11/2018 21:47

require_once "../obj/om_dbform.class.php";

class modele_edition_gen extends om_dbform {

    protected $_absolute_class_name = "modele_edition";

    var $table = "modele_edition";
    var $clePrimaire = "modele_edition";
    var $typeCle = "N";
    var $required_field = array(
        "courrier_type",
        "libelle",
        "modele_edition"
    );
    var $unique_key = array(
      "code",
    );
    var $foreign_keys_extended = array(
        "courrier_type" => array("courrier_type", ),
    );
    
    /**
     *
     * @return string
     */
    function get_default_libelle() {
        return $this->getVal($this->clePrimaire)."&nbsp;".$this->getVal("libelle");
    }

    /**
     *
     * @return array
     */
    function get_var_sql_forminc__champs() {
        return array(
            "modele_edition",
            "libelle",
            "description",
            "om_validite_debut",
            "om_validite_fin",
            "code",
            "courrier_type",
            "om_lettretype_id",
        );
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_courrier_type() {
        return "SELECT courrier_type.courrier_type, courrier_type.libelle FROM ".DB_PREFIXE."courrier_type WHERE ((courrier_type.om_validite_debut IS NULL AND (courrier_type.om_validite_fin IS NULL OR courrier_type.om_validite_fin > CURRENT_DATE)) OR (courrier_type.om_validite_debut <= CURRENT_DATE AND (courrier_type.om_validite_fin IS NULL OR courrier_type.om_validite_fin > CURRENT_DATE))) ORDER BY courrier_type.libelle ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_courrier_type_by_id() {
        return "SELECT courrier_type.courrier_type, courrier_type.libelle FROM ".DB_PREFIXE."courrier_type WHERE courrier_type = <idx>";
    }




    function setvalF($val = array()) {
        //affectation valeur formulaire
        if (!is_numeric($val['modele_edition'])) {
            $this->valF['modele_edition'] = ""; // -> requis
        } else {
            $this->valF['modele_edition'] = $val['modele_edition'];
        }
        $this->valF['libelle'] = $val['libelle'];
            $this->valF['description'] = $val['description'];
        if ($val['om_validite_debut'] != "") {
            $this->valF['om_validite_debut'] = $this->dateDB($val['om_validite_debut']);
        } else {
            $this->valF['om_validite_debut'] = NULL;
        }
        if ($val['om_validite_fin'] != "") {
            $this->valF['om_validite_fin'] = $this->dateDB($val['om_validite_fin']);
        } else {
            $this->valF['om_validite_fin'] = NULL;
        }
        if ($val['code'] == "") {
            $this->valF['code'] = NULL;
        } else {
            $this->valF['code'] = $val['code'];
        }
        if (!is_numeric($val['courrier_type'])) {
            $this->valF['courrier_type'] = ""; // -> requis
        } else {
            $this->valF['courrier_type'] = $val['courrier_type'];
        }
        if ($val['om_lettretype_id'] == "") {
            $this->valF['om_lettretype_id'] = NULL;
        } else {
            $this->valF['om_lettretype_id'] = $val['om_lettretype_id'];
        }
    }

    //=================================================
    //cle primaire automatique [automatic primary key]
    //==================================================

    function setId(&$dnu1 = null) {
    //numero automatique
        $this->valF[$this->clePrimaire] = $this->f->db->nextId(DB_PREFIXE.$this->table);
    }

    function setValFAjout($val = array()) {
    //numero automatique -> pas de controle ajout cle primaire
    }

    function verifierAjout($val = array(), &$dnu1 = null) {
    //numero automatique -> pas de verfication de cle primaire
    }
    /**
     * Methode verifier
     */
    function verifier($val = array(), &$dnu1 = null, $dnu2 = null) {
        // On appelle la methode de la classe parent
        parent::verifier($val, $this->f->db, null);

        // gestion des dates de validites
        $date_debut = $this->valF['om_validite_debut'];
        $date_fin = $this->valF['om_validite_fin'];

        if ($date_debut != '' and $date_fin != '') {
        
            $date_debut = explode('-', $this->valF['om_validite_debut']);
            $date_fin = explode('-', $this->valF['om_validite_fin']);

            $time_debut = mktime(0, 0, 0, $date_debut[1], $date_debut[2],
                                 $date_debut[0]);
            $time_fin = mktime(0, 0, 0, $date_fin[1], $date_fin[2],
                                 $date_fin[0]);

            if ($time_debut > $time_fin or $time_debut == $time_fin) {
                $this->correct = false;
                $this->addToMessage(__('La date de fin de validite doit etre future a la de debut de validite.'));
            }
        }
    }


    //==========================
    // Formulaire  [form]
    //==========================
    /**
     *
     */
    function setType(&$form, $maj) {
        // Récupération du mode de l'action
        $crud = $this->get_action_crud($maj);

        // MODE AJOUTER
        if ($maj == 0 || $crud == 'create') {
            $form->setType("modele_edition", "hidden");
            $form->setType("libelle", "text");
            $form->setType("description", "textarea");
            if ($this->f->isAccredited(array($this->table."_modifier_validite", $this->table, ))) {
                $form->setType("om_validite_debut", "date");
            } else {
                $form->setType("om_validite_debut", "hiddenstaticdate");
            }
            if ($this->f->isAccredited(array($this->table."_modifier_validite", $this->table, ))) {
                $form->setType("om_validite_fin", "date");
            } else {
                $form->setType("om_validite_fin", "hiddenstaticdate");
            }
            $form->setType("code", "text");
            if ($this->is_in_context_of_foreign_key("courrier_type", $this->retourformulaire)) {
                $form->setType("courrier_type", "selecthiddenstatic");
            } else {
                $form->setType("courrier_type", "select");
            }
            $form->setType("om_lettretype_id", "text");
        }

        // MDOE MODIFIER
        if ($maj == 1 || $crud == 'update') {
            $form->setType("modele_edition", "hiddenstatic");
            $form->setType("libelle", "text");
            $form->setType("description", "textarea");
            if ($this->f->isAccredited(array($this->table."_modifier_validite", $this->table, ))) {
                $form->setType("om_validite_debut", "date");
            } else {
                $form->setType("om_validite_debut", "hiddenstaticdate");
            }
            if ($this->f->isAccredited(array($this->table."_modifier_validite", $this->table, ))) {
                $form->setType("om_validite_fin", "date");
            } else {
                $form->setType("om_validite_fin", "hiddenstaticdate");
            }
            $form->setType("code", "text");
            if ($this->is_in_context_of_foreign_key("courrier_type", $this->retourformulaire)) {
                $form->setType("courrier_type", "selecthiddenstatic");
            } else {
                $form->setType("courrier_type", "select");
            }
            $form->setType("om_lettretype_id", "text");
        }

        // MODE SUPPRIMER
        if ($maj == 2 || $crud == 'delete') {
            $form->setType("modele_edition", "hiddenstatic");
            $form->setType("libelle", "hiddenstatic");
            $form->setType("description", "hiddenstatic");
            $form->setType("om_validite_debut", "hiddenstatic");
            $form->setType("om_validite_fin", "hiddenstatic");
            $form->setType("code", "hiddenstatic");
            $form->setType("courrier_type", "selectstatic");
            $form->setType("om_lettretype_id", "hiddenstatic");
        }

        // MODE CONSULTER
        if ($maj == 3 || $crud == 'read') {
            $form->setType("modele_edition", "static");
            $form->setType("libelle", "static");
            $form->setType("description", "textareastatic");
            $form->setType("om_validite_debut", "datestatic");
            $form->setType("om_validite_fin", "datestatic");
            $form->setType("code", "static");
            $form->setType("courrier_type", "selectstatic");
            $form->setType("om_lettretype_id", "static");
        }

    }


    function setOnchange(&$form, $maj) {
    //javascript controle client
        $form->setOnchange('modele_edition','VerifNum(this)');
        $form->setOnchange('om_validite_debut','fdate(this)');
        $form->setOnchange('om_validite_fin','fdate(this)');
        $form->setOnchange('courrier_type','VerifNum(this)');
    }
    /**
     * Methode setTaille
     */
    function setTaille(&$form, $maj) {
        $form->setTaille("modele_edition", 11);
        $form->setTaille("libelle", 30);
        $form->setTaille("description", 80);
        $form->setTaille("om_validite_debut", 12);
        $form->setTaille("om_validite_fin", 12);
        $form->setTaille("code", 15);
        $form->setTaille("courrier_type", 11);
        $form->setTaille("om_lettretype_id", 30);
    }

    /**
     * Methode setMax
     */
    function setMax(&$form, $maj) {
        $form->setMax("modele_edition", 11);
        $form->setMax("libelle", 100);
        $form->setMax("description", 6);
        $form->setMax("om_validite_debut", 12);
        $form->setMax("om_validite_fin", 12);
        $form->setMax("code", 15);
        $form->setMax("courrier_type", 11);
        $form->setMax("om_lettretype_id", 50);
    }


    function setLib(&$form, $maj) {
    //libelle des champs
        $form->setLib('modele_edition', __('modele_edition'));
        $form->setLib('libelle', __('libelle'));
        $form->setLib('description', __('description'));
        $form->setLib('om_validite_debut', __('om_validite_debut'));
        $form->setLib('om_validite_fin', __('om_validite_fin'));
        $form->setLib('code', __('code'));
        $form->setLib('courrier_type', __('courrier_type'));
        $form->setLib('om_lettretype_id', __('om_lettretype_id'));
    }
    /**
     *
     */
    function setSelect(&$form, $maj, &$dnu1 = null, $dnu2 = null) {

        // courrier_type
        $this->init_select(
            $form, 
            $this->f->db,
            $maj,
            null,
            "courrier_type",
            $this->get_var_sql_forminc__sql("courrier_type"),
            $this->get_var_sql_forminc__sql("courrier_type_by_id"),
            true
        );
    }


    //==================================
    // sous Formulaire
    //==================================
    

    function setValsousformulaire(&$form, $maj, $validation, $idxformulaire, $retourformulaire, $typeformulaire, &$dnu1 = null, $dnu2 = null) {
        $this->retourformulaire = $retourformulaire;
        if($validation == 0) {
            if($this->is_in_context_of_foreign_key('courrier_type', $this->retourformulaire))
                $form->setVal('courrier_type', $idxformulaire);
        }// fin validation
        $this->set_form_default_values($form, $maj, $validation);
    }// fin setValsousformulaire

    //==================================
    // cle secondaire
    //==================================
    
    /**
     * Methode clesecondaire
     */
    function cleSecondaire($id, &$dnu1 = null, $val = array(), $dnu2 = null) {
        // On appelle la methode de la classe parent
        parent::cleSecondaire($id);
        // Verification de la cle secondaire : analyses
        $this->rechercheTable($this->f->db, "analyses", "modele_edition_compte_rendu", $id);
        // Verification de la cle secondaire : analyses
        $this->rechercheTable($this->f->db, "analyses", "modele_edition_proces_verbal", $id);
        // Verification de la cle secondaire : analyses
        $this->rechercheTable($this->f->db, "analyses", "modele_edition_rapport", $id);
        // Verification de la cle secondaire : analyses_type
        $this->rechercheTable($this->f->db, "analyses_type", "modele_edition_compte_rendu", $id);
        // Verification de la cle secondaire : analyses_type
        $this->rechercheTable($this->f->db, "analyses_type", "modele_edition_proces_verbal", $id);
        // Verification de la cle secondaire : analyses_type
        $this->rechercheTable($this->f->db, "analyses_type", "modele_edition_rapport", $id);
        // Verification de la cle secondaire : courrier
        $this->rechercheTable($this->f->db, "courrier", "modele_edition", $id);
        // Verification de la cle secondaire : proces_verbal
        $this->rechercheTable($this->f->db, "proces_verbal", "modele_edition", $id);
        // Verification de la cle secondaire : reunion_type
        $this->rechercheTable($this->f->db, "reunion_type", "modele_compte_rendu_global", $id);
        // Verification de la cle secondaire : reunion_type
        $this->rechercheTable($this->f->db, "reunion_type", "modele_compte_rendu_specifique", $id);
        // Verification de la cle secondaire : reunion_type
        $this->rechercheTable($this->f->db, "reunion_type", "modele_feuille_presence", $id);
        // Verification de la cle secondaire : reunion_type
        $this->rechercheTable($this->f->db, "reunion_type", "modele_ordre_du_jour", $id);
    }


}
