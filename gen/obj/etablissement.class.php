<?php
//$Id$ 
//gen openMairie le 06/08/2020 10:34

require_once "../obj/om_dbform.class.php";

class etablissement_gen extends om_dbform {

    protected $_absolute_class_name = "etablissement";

    var $table = "etablissement";
    var $clePrimaire = "etablissement";
    var $typeCle = "N";
    var $required_field = array(
        "code",
        "etablissement",
        "etablissement_nature",
        "libelle"
    );
    var $unique_key = array(
      "code",
    );
    var $foreign_keys_extended = array(
        "reunion_avis" => array("reunion_avis", ),
        "acteur" => array("acteur", ),
        "arrondissement" => array("arrondissement", ),
        "voie" => array("voie", ),
        "dossier_coordination" => array("dossier_coordination", "dossier_coordination_nouveau", "dossier_coordination_a_qualifier", "dossier_coordination_a_cloturer", ),
        "etablissement_categorie" => array("etablissement_categorie", ),
        "etablissement_etat" => array("etablissement_etat", ),
        "etablissement_nature" => array("etablissement_nature", ),
        "etablissement_statut_juridique" => array("etablissement_statut_juridique", ),
        "etablissement_tutelle_adm" => array("etablissement_tutelle_adm", ),
        "etablissement_type" => array("etablissement_type", ),
        "autorite_competente" => array("autorite_competente", ),
        "analyses_type" => array("analyses_type", ),
        "visite_duree" => array("visite_duree", ),
    );
    
    /**
     *
     * @return string
     */
    function get_default_libelle() {
        return $this->getVal($this->clePrimaire)."&nbsp;".$this->getVal("libelle");
    }

    /**
     *
     * @return array
     */
    function get_var_sql_forminc__champs() {
        return array(
            "etablissement",
            "code",
            "libelle",
            "adresse_numero",
            "adresse_numero2",
            "adresse_voie",
            "adresse_complement",
            "lieu_dit",
            "boite_postale",
            "adresse_cp",
            "adresse_ville",
            "adresse_arrondissement",
            "cedex",
            "npai",
            "telephone",
            "fax",
            "etablissement_nature",
            "siret",
            "annee_de_construction",
            "etablissement_statut_juridique",
            "etablissement_tutelle_adm",
            "ref_patrimoine",
            "etablissement_type",
            "etablissement_categorie",
            "etablissement_etat",
            "date_arrete_ouverture",
            "autorite_police_encours",
            "om_validite_debut",
            "om_validite_fin",
            "si_effectif_public",
            "si_effectif_personnel",
            "si_locaux_sommeil",
            "si_periodicite_visites",
            "si_prochaine_visite_periodique_date_previsionnelle",
            "si_visite_duree",
            "si_derniere_visite_periodique_date",
            "si_derniere_visite_date",
            "si_derniere_visite_avis",
            "si_derniere_visite_technicien",
            "si_prochaine_visite_date",
            "si_prochaine_visite_type",
            "acc_derniere_visite_date",
            "acc_derniere_visite_avis",
            "acc_derniere_visite_technicien",
            "acc_consignes_om_html",
            "acc_descriptif_om_html",
            "si_consignes_om_html",
            "si_descriptif_om_html",
            "si_autorite_competente_visite",
            "si_autorite_competente_plan",
            "si_dernier_plan_avis",
            "si_type_alarme",
            "si_type_ssi",
            "si_conformite_l16",
            "si_alimentation_remplacement",
            "si_service_securite",
            "si_personnel_jour",
            "si_personnel_nuit",
            "references_cadastrales",
            "dossier_coordination_periodique",
            "geolocalise",
            "geom_point",
            "geom_emprise",
        );
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_acc_derniere_visite_avis() {
        return "SELECT reunion_avis.reunion_avis, reunion_avis.libelle FROM ".DB_PREFIXE."reunion_avis WHERE ((reunion_avis.om_validite_debut IS NULL AND (reunion_avis.om_validite_fin IS NULL OR reunion_avis.om_validite_fin > CURRENT_DATE)) OR (reunion_avis.om_validite_debut <= CURRENT_DATE AND (reunion_avis.om_validite_fin IS NULL OR reunion_avis.om_validite_fin > CURRENT_DATE))) ORDER BY reunion_avis.libelle ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_acc_derniere_visite_avis_by_id() {
        return "SELECT reunion_avis.reunion_avis, reunion_avis.libelle FROM ".DB_PREFIXE."reunion_avis WHERE reunion_avis = <idx>";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_acc_derniere_visite_technicien() {
        return "SELECT acteur.acteur, acteur.nom_prenom FROM ".DB_PREFIXE."acteur WHERE ((acteur.om_validite_debut IS NULL AND (acteur.om_validite_fin IS NULL OR acteur.om_validite_fin > CURRENT_DATE)) OR (acteur.om_validite_debut <= CURRENT_DATE AND (acteur.om_validite_fin IS NULL OR acteur.om_validite_fin > CURRENT_DATE))) ORDER BY acteur.nom_prenom ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_acc_derniere_visite_technicien_by_id() {
        return "SELECT acteur.acteur, acteur.nom_prenom FROM ".DB_PREFIXE."acteur WHERE acteur = <idx>";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_adresse_arrondissement() {
        return "SELECT arrondissement.arrondissement, arrondissement.libelle FROM ".DB_PREFIXE."arrondissement WHERE ((arrondissement.om_validite_debut IS NULL AND (arrondissement.om_validite_fin IS NULL OR arrondissement.om_validite_fin > CURRENT_DATE)) OR (arrondissement.om_validite_debut <= CURRENT_DATE AND (arrondissement.om_validite_fin IS NULL OR arrondissement.om_validite_fin > CURRENT_DATE))) ORDER BY arrondissement.libelle ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_adresse_arrondissement_by_id() {
        return "SELECT arrondissement.arrondissement, arrondissement.libelle FROM ".DB_PREFIXE."arrondissement WHERE arrondissement = <idx>";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_adresse_voie() {
        return "SELECT voie.voie, voie.libelle FROM ".DB_PREFIXE."voie WHERE ((voie.om_validite_debut IS NULL AND (voie.om_validite_fin IS NULL OR voie.om_validite_fin > CURRENT_DATE)) OR (voie.om_validite_debut <= CURRENT_DATE AND (voie.om_validite_fin IS NULL OR voie.om_validite_fin > CURRENT_DATE))) ORDER BY voie.libelle ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_adresse_voie_by_id() {
        return "SELECT voie.voie, voie.libelle FROM ".DB_PREFIXE."voie WHERE voie = <idx>";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_dossier_coordination_periodique() {
        return "SELECT dossier_coordination.dossier_coordination, dossier_coordination.libelle FROM ".DB_PREFIXE."dossier_coordination ORDER BY dossier_coordination.libelle ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_dossier_coordination_periodique_by_id() {
        return "SELECT dossier_coordination.dossier_coordination, dossier_coordination.libelle FROM ".DB_PREFIXE."dossier_coordination WHERE dossier_coordination = <idx>";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_etablissement_categorie() {
        return "SELECT etablissement_categorie.etablissement_categorie, etablissement_categorie.libelle FROM ".DB_PREFIXE."etablissement_categorie WHERE ((etablissement_categorie.om_validite_debut IS NULL AND (etablissement_categorie.om_validite_fin IS NULL OR etablissement_categorie.om_validite_fin > CURRENT_DATE)) OR (etablissement_categorie.om_validite_debut <= CURRENT_DATE AND (etablissement_categorie.om_validite_fin IS NULL OR etablissement_categorie.om_validite_fin > CURRENT_DATE))) ORDER BY etablissement_categorie.libelle ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_etablissement_categorie_by_id() {
        return "SELECT etablissement_categorie.etablissement_categorie, etablissement_categorie.libelle FROM ".DB_PREFIXE."etablissement_categorie WHERE etablissement_categorie = <idx>";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_etablissement_etat() {
        return "SELECT etablissement_etat.etablissement_etat, etablissement_etat.statut_administratif FROM ".DB_PREFIXE."etablissement_etat WHERE ((etablissement_etat.om_validite_debut IS NULL AND (etablissement_etat.om_validite_fin IS NULL OR etablissement_etat.om_validite_fin > CURRENT_DATE)) OR (etablissement_etat.om_validite_debut <= CURRENT_DATE AND (etablissement_etat.om_validite_fin IS NULL OR etablissement_etat.om_validite_fin > CURRENT_DATE))) ORDER BY etablissement_etat.statut_administratif ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_etablissement_etat_by_id() {
        return "SELECT etablissement_etat.etablissement_etat, etablissement_etat.statut_administratif FROM ".DB_PREFIXE."etablissement_etat WHERE etablissement_etat = <idx>";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_etablissement_nature() {
        return "SELECT etablissement_nature.etablissement_nature, etablissement_nature.nature FROM ".DB_PREFIXE."etablissement_nature WHERE ((etablissement_nature.om_validite_debut IS NULL AND (etablissement_nature.om_validite_fin IS NULL OR etablissement_nature.om_validite_fin > CURRENT_DATE)) OR (etablissement_nature.om_validite_debut <= CURRENT_DATE AND (etablissement_nature.om_validite_fin IS NULL OR etablissement_nature.om_validite_fin > CURRENT_DATE))) ORDER BY etablissement_nature.nature ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_etablissement_nature_by_id() {
        return "SELECT etablissement_nature.etablissement_nature, etablissement_nature.nature FROM ".DB_PREFIXE."etablissement_nature WHERE etablissement_nature = <idx>";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_etablissement_statut_juridique() {
        return "SELECT etablissement_statut_juridique.etablissement_statut_juridique, etablissement_statut_juridique.libelle FROM ".DB_PREFIXE."etablissement_statut_juridique WHERE ((etablissement_statut_juridique.om_validite_debut IS NULL AND (etablissement_statut_juridique.om_validite_fin IS NULL OR etablissement_statut_juridique.om_validite_fin > CURRENT_DATE)) OR (etablissement_statut_juridique.om_validite_debut <= CURRENT_DATE AND (etablissement_statut_juridique.om_validite_fin IS NULL OR etablissement_statut_juridique.om_validite_fin > CURRENT_DATE))) ORDER BY etablissement_statut_juridique.libelle ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_etablissement_statut_juridique_by_id() {
        return "SELECT etablissement_statut_juridique.etablissement_statut_juridique, etablissement_statut_juridique.libelle FROM ".DB_PREFIXE."etablissement_statut_juridique WHERE etablissement_statut_juridique = <idx>";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_etablissement_tutelle_adm() {
        return "SELECT etablissement_tutelle_adm.etablissement_tutelle_adm, etablissement_tutelle_adm.libelle FROM ".DB_PREFIXE."etablissement_tutelle_adm WHERE ((etablissement_tutelle_adm.om_validite_debut IS NULL AND (etablissement_tutelle_adm.om_validite_fin IS NULL OR etablissement_tutelle_adm.om_validite_fin > CURRENT_DATE)) OR (etablissement_tutelle_adm.om_validite_debut <= CURRENT_DATE AND (etablissement_tutelle_adm.om_validite_fin IS NULL OR etablissement_tutelle_adm.om_validite_fin > CURRENT_DATE))) ORDER BY etablissement_tutelle_adm.libelle ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_etablissement_tutelle_adm_by_id() {
        return "SELECT etablissement_tutelle_adm.etablissement_tutelle_adm, etablissement_tutelle_adm.libelle FROM ".DB_PREFIXE."etablissement_tutelle_adm WHERE etablissement_tutelle_adm = <idx>";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_etablissement_type() {
        return "SELECT etablissement_type.etablissement_type, etablissement_type.libelle FROM ".DB_PREFIXE."etablissement_type WHERE ((etablissement_type.om_validite_debut IS NULL AND (etablissement_type.om_validite_fin IS NULL OR etablissement_type.om_validite_fin > CURRENT_DATE)) OR (etablissement_type.om_validite_debut <= CURRENT_DATE AND (etablissement_type.om_validite_fin IS NULL OR etablissement_type.om_validite_fin > CURRENT_DATE))) ORDER BY etablissement_type.libelle ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_etablissement_type_by_id() {
        return "SELECT etablissement_type.etablissement_type, etablissement_type.libelle FROM ".DB_PREFIXE."etablissement_type WHERE etablissement_type = <idx>";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_si_autorite_competente_plan() {
        return "SELECT autorite_competente.autorite_competente, autorite_competente.libelle FROM ".DB_PREFIXE."autorite_competente ORDER BY autorite_competente.libelle ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_si_autorite_competente_plan_by_id() {
        return "SELECT autorite_competente.autorite_competente, autorite_competente.libelle FROM ".DB_PREFIXE."autorite_competente WHERE autorite_competente = <idx>";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_si_autorite_competente_visite() {
        return "SELECT autorite_competente.autorite_competente, autorite_competente.libelle FROM ".DB_PREFIXE."autorite_competente ORDER BY autorite_competente.libelle ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_si_autorite_competente_visite_by_id() {
        return "SELECT autorite_competente.autorite_competente, autorite_competente.libelle FROM ".DB_PREFIXE."autorite_competente WHERE autorite_competente = <idx>";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_si_dernier_plan_avis() {
        return "SELECT reunion_avis.reunion_avis, reunion_avis.libelle FROM ".DB_PREFIXE."reunion_avis WHERE ((reunion_avis.om_validite_debut IS NULL AND (reunion_avis.om_validite_fin IS NULL OR reunion_avis.om_validite_fin > CURRENT_DATE)) OR (reunion_avis.om_validite_debut <= CURRENT_DATE AND (reunion_avis.om_validite_fin IS NULL OR reunion_avis.om_validite_fin > CURRENT_DATE))) ORDER BY reunion_avis.libelle ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_si_dernier_plan_avis_by_id() {
        return "SELECT reunion_avis.reunion_avis, reunion_avis.libelle FROM ".DB_PREFIXE."reunion_avis WHERE reunion_avis = <idx>";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_si_derniere_visite_avis() {
        return "SELECT reunion_avis.reunion_avis, reunion_avis.libelle FROM ".DB_PREFIXE."reunion_avis WHERE ((reunion_avis.om_validite_debut IS NULL AND (reunion_avis.om_validite_fin IS NULL OR reunion_avis.om_validite_fin > CURRENT_DATE)) OR (reunion_avis.om_validite_debut <= CURRENT_DATE AND (reunion_avis.om_validite_fin IS NULL OR reunion_avis.om_validite_fin > CURRENT_DATE))) ORDER BY reunion_avis.libelle ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_si_derniere_visite_avis_by_id() {
        return "SELECT reunion_avis.reunion_avis, reunion_avis.libelle FROM ".DB_PREFIXE."reunion_avis WHERE reunion_avis = <idx>";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_si_derniere_visite_technicien() {
        return "SELECT acteur.acteur, acteur.nom_prenom FROM ".DB_PREFIXE."acteur WHERE ((acteur.om_validite_debut IS NULL AND (acteur.om_validite_fin IS NULL OR acteur.om_validite_fin > CURRENT_DATE)) OR (acteur.om_validite_debut <= CURRENT_DATE AND (acteur.om_validite_fin IS NULL OR acteur.om_validite_fin > CURRENT_DATE))) ORDER BY acteur.nom_prenom ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_si_derniere_visite_technicien_by_id() {
        return "SELECT acteur.acteur, acteur.nom_prenom FROM ".DB_PREFIXE."acteur WHERE acteur = <idx>";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_si_prochaine_visite_type() {
        return "SELECT analyses_type.analyses_type, analyses_type.libelle FROM ".DB_PREFIXE."analyses_type WHERE ((analyses_type.om_validite_debut IS NULL AND (analyses_type.om_validite_fin IS NULL OR analyses_type.om_validite_fin > CURRENT_DATE)) OR (analyses_type.om_validite_debut <= CURRENT_DATE AND (analyses_type.om_validite_fin IS NULL OR analyses_type.om_validite_fin > CURRENT_DATE))) ORDER BY analyses_type.libelle ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_si_prochaine_visite_type_by_id() {
        return "SELECT analyses_type.analyses_type, analyses_type.libelle FROM ".DB_PREFIXE."analyses_type WHERE analyses_type = <idx>";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_si_visite_duree() {
        return "SELECT visite_duree.visite_duree, visite_duree.libelle FROM ".DB_PREFIXE."visite_duree ORDER BY visite_duree.libelle ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_si_visite_duree_by_id() {
        return "SELECT visite_duree.visite_duree, visite_duree.libelle FROM ".DB_PREFIXE."visite_duree WHERE visite_duree = <idx>";
    }




    function setvalF($val = array()) {
        //affectation valeur formulaire
        if (!is_numeric($val['etablissement'])) {
            $this->valF['etablissement'] = ""; // -> requis
        } else {
            $this->valF['etablissement'] = $val['etablissement'];
        }
        $this->valF['code'] = $val['code'];
        $this->valF['libelle'] = $val['libelle'];
        if (!is_numeric($val['adresse_numero'])) {
            $this->valF['adresse_numero'] = NULL;
        } else {
            $this->valF['adresse_numero'] = $val['adresse_numero'];
        }
        if ($val['adresse_numero2'] == "") {
            $this->valF['adresse_numero2'] = NULL;
        } else {
            $this->valF['adresse_numero2'] = $val['adresse_numero2'];
        }
        if (!is_numeric($val['adresse_voie'])) {
            $this->valF['adresse_voie'] = NULL;
        } else {
            $this->valF['adresse_voie'] = $val['adresse_voie'];
        }
        if ($val['adresse_complement'] == "") {
            $this->valF['adresse_complement'] = NULL;
        } else {
            $this->valF['adresse_complement'] = $val['adresse_complement'];
        }
        if ($val['lieu_dit'] == "") {
            $this->valF['lieu_dit'] = NULL;
        } else {
            $this->valF['lieu_dit'] = $val['lieu_dit'];
        }
        if ($val['boite_postale'] == "") {
            $this->valF['boite_postale'] = NULL;
        } else {
            $this->valF['boite_postale'] = $val['boite_postale'];
        }
        if ($val['adresse_cp'] == "") {
            $this->valF['adresse_cp'] = NULL;
        } else {
            $this->valF['adresse_cp'] = $val['adresse_cp'];
        }
        if ($val['adresse_ville'] == "") {
            $this->valF['adresse_ville'] = NULL;
        } else {
            $this->valF['adresse_ville'] = $val['adresse_ville'];
        }
        if (!is_numeric($val['adresse_arrondissement'])) {
            $this->valF['adresse_arrondissement'] = NULL;
        } else {
            $this->valF['adresse_arrondissement'] = $val['adresse_arrondissement'];
        }
        if ($val['cedex'] == "") {
            $this->valF['cedex'] = NULL;
        } else {
            $this->valF['cedex'] = $val['cedex'];
        }
        if ($val['npai'] == 1 || $val['npai'] == "t" || $val['npai'] == "Oui") {
            $this->valF['npai'] = true;
        } else {
            $this->valF['npai'] = false;
        }
        if ($val['telephone'] == "") {
            $this->valF['telephone'] = NULL;
        } else {
            $this->valF['telephone'] = $val['telephone'];
        }
        if ($val['fax'] == "") {
            $this->valF['fax'] = NULL;
        } else {
            $this->valF['fax'] = $val['fax'];
        }
        if (!is_numeric($val['etablissement_nature'])) {
            $this->valF['etablissement_nature'] = ""; // -> requis
        } else {
            $this->valF['etablissement_nature'] = $val['etablissement_nature'];
        }
        if ($val['siret'] == "") {
            $this->valF['siret'] = NULL;
        } else {
            $this->valF['siret'] = $val['siret'];
        }
        if ($val['annee_de_construction'] == "") {
            $this->valF['annee_de_construction'] = NULL;
        } else {
            $this->valF['annee_de_construction'] = $val['annee_de_construction'];
        }
        if (!is_numeric($val['etablissement_statut_juridique'])) {
            $this->valF['etablissement_statut_juridique'] = NULL;
        } else {
            $this->valF['etablissement_statut_juridique'] = $val['etablissement_statut_juridique'];
        }
        if (!is_numeric($val['etablissement_tutelle_adm'])) {
            $this->valF['etablissement_tutelle_adm'] = NULL;
        } else {
            $this->valF['etablissement_tutelle_adm'] = $val['etablissement_tutelle_adm'];
        }
            $this->valF['ref_patrimoine'] = $val['ref_patrimoine'];
        if (!is_numeric($val['etablissement_type'])) {
            $this->valF['etablissement_type'] = NULL;
        } else {
            $this->valF['etablissement_type'] = $val['etablissement_type'];
        }
        if (!is_numeric($val['etablissement_categorie'])) {
            $this->valF['etablissement_categorie'] = NULL;
        } else {
            $this->valF['etablissement_categorie'] = $val['etablissement_categorie'];
        }
        if (!is_numeric($val['etablissement_etat'])) {
            $this->valF['etablissement_etat'] = NULL;
        } else {
            $this->valF['etablissement_etat'] = $val['etablissement_etat'];
        }
        if ($val['date_arrete_ouverture'] != "") {
            $this->valF['date_arrete_ouverture'] = $this->dateDB($val['date_arrete_ouverture']);
        } else {
            $this->valF['date_arrete_ouverture'] = NULL;
        }
        if ($val['autorite_police_encours'] == 1 || $val['autorite_police_encours'] == "t" || $val['autorite_police_encours'] == "Oui") {
            $this->valF['autorite_police_encours'] = true;
        } else {
            $this->valF['autorite_police_encours'] = false;
        }
        if ($val['om_validite_debut'] != "") {
            $this->valF['om_validite_debut'] = $this->dateDB($val['om_validite_debut']);
        } else {
            $this->valF['om_validite_debut'] = NULL;
        }
        if ($val['om_validite_fin'] != "") {
            $this->valF['om_validite_fin'] = $this->dateDB($val['om_validite_fin']);
        } else {
            $this->valF['om_validite_fin'] = NULL;
        }
        if (!is_numeric($val['si_effectif_public'])) {
            $this->valF['si_effectif_public'] = NULL;
        } else {
            $this->valF['si_effectif_public'] = $val['si_effectif_public'];
        }
        if (!is_numeric($val['si_effectif_personnel'])) {
            $this->valF['si_effectif_personnel'] = NULL;
        } else {
            $this->valF['si_effectif_personnel'] = $val['si_effectif_personnel'];
        }
        if ($val['si_locaux_sommeil'] == 1 || $val['si_locaux_sommeil'] == "t" || $val['si_locaux_sommeil'] == "Oui") {
            $this->valF['si_locaux_sommeil'] = true;
        } else {
            $this->valF['si_locaux_sommeil'] = false;
        }
        if (!is_numeric($val['si_periodicite_visites'])) {
            $this->valF['si_periodicite_visites'] = NULL;
        } else {
            $this->valF['si_periodicite_visites'] = $val['si_periodicite_visites'];
        }
        if ($val['si_prochaine_visite_periodique_date_previsionnelle'] != "") {
            $this->valF['si_prochaine_visite_periodique_date_previsionnelle'] = $this->dateDB($val['si_prochaine_visite_periodique_date_previsionnelle']);
        } else {
            $this->valF['si_prochaine_visite_periodique_date_previsionnelle'] = NULL;
        }
        if (!is_numeric($val['si_visite_duree'])) {
            $this->valF['si_visite_duree'] = NULL;
        } else {
            $this->valF['si_visite_duree'] = $val['si_visite_duree'];
        }
        if ($val['si_derniere_visite_periodique_date'] != "") {
            $this->valF['si_derniere_visite_periodique_date'] = $this->dateDB($val['si_derniere_visite_periodique_date']);
        } else {
            $this->valF['si_derniere_visite_periodique_date'] = NULL;
        }
        if ($val['si_derniere_visite_date'] != "") {
            $this->valF['si_derniere_visite_date'] = $this->dateDB($val['si_derniere_visite_date']);
        } else {
            $this->valF['si_derniere_visite_date'] = NULL;
        }
        if (!is_numeric($val['si_derniere_visite_avis'])) {
            $this->valF['si_derniere_visite_avis'] = NULL;
        } else {
            $this->valF['si_derniere_visite_avis'] = $val['si_derniere_visite_avis'];
        }
        if (!is_numeric($val['si_derniere_visite_technicien'])) {
            $this->valF['si_derniere_visite_technicien'] = NULL;
        } else {
            $this->valF['si_derniere_visite_technicien'] = $val['si_derniere_visite_technicien'];
        }
        if ($val['si_prochaine_visite_date'] != "") {
            $this->valF['si_prochaine_visite_date'] = $this->dateDB($val['si_prochaine_visite_date']);
        } else {
            $this->valF['si_prochaine_visite_date'] = NULL;
        }
        if (!is_numeric($val['si_prochaine_visite_type'])) {
            $this->valF['si_prochaine_visite_type'] = NULL;
        } else {
            $this->valF['si_prochaine_visite_type'] = $val['si_prochaine_visite_type'];
        }
        if ($val['acc_derniere_visite_date'] != "") {
            $this->valF['acc_derniere_visite_date'] = $this->dateDB($val['acc_derniere_visite_date']);
        } else {
            $this->valF['acc_derniere_visite_date'] = NULL;
        }
        if (!is_numeric($val['acc_derniere_visite_avis'])) {
            $this->valF['acc_derniere_visite_avis'] = NULL;
        } else {
            $this->valF['acc_derniere_visite_avis'] = $val['acc_derniere_visite_avis'];
        }
        if (!is_numeric($val['acc_derniere_visite_technicien'])) {
            $this->valF['acc_derniere_visite_technicien'] = NULL;
        } else {
            $this->valF['acc_derniere_visite_technicien'] = $val['acc_derniere_visite_technicien'];
        }
            $this->valF['acc_consignes_om_html'] = $val['acc_consignes_om_html'];
            $this->valF['acc_descriptif_om_html'] = $val['acc_descriptif_om_html'];
            $this->valF['si_consignes_om_html'] = $val['si_consignes_om_html'];
            $this->valF['si_descriptif_om_html'] = $val['si_descriptif_om_html'];
        if (!is_numeric($val['si_autorite_competente_visite'])) {
            $this->valF['si_autorite_competente_visite'] = NULL;
        } else {
            $this->valF['si_autorite_competente_visite'] = $val['si_autorite_competente_visite'];
        }
        if (!is_numeric($val['si_autorite_competente_plan'])) {
            $this->valF['si_autorite_competente_plan'] = NULL;
        } else {
            $this->valF['si_autorite_competente_plan'] = $val['si_autorite_competente_plan'];
        }
        if (!is_numeric($val['si_dernier_plan_avis'])) {
            $this->valF['si_dernier_plan_avis'] = NULL;
        } else {
            $this->valF['si_dernier_plan_avis'] = $val['si_dernier_plan_avis'];
        }
            $this->valF['si_type_alarme'] = $val['si_type_alarme'];
        if ($val['si_type_ssi'] == "") {
            $this->valF['si_type_ssi'] = NULL;
        } else {
            $this->valF['si_type_ssi'] = $val['si_type_ssi'];
        }
        if ($val['si_conformite_l16'] == 1 || $val['si_conformite_l16'] == "t" || $val['si_conformite_l16'] == "Oui") {
            $this->valF['si_conformite_l16'] = true;
        } else {
            $this->valF['si_conformite_l16'] = false;
        }
        if ($val['si_alimentation_remplacement'] == 1 || $val['si_alimentation_remplacement'] == "t" || $val['si_alimentation_remplacement'] == "Oui") {
            $this->valF['si_alimentation_remplacement'] = true;
        } else {
            $this->valF['si_alimentation_remplacement'] = false;
        }
        if ($val['si_service_securite'] == 1 || $val['si_service_securite'] == "t" || $val['si_service_securite'] == "Oui") {
            $this->valF['si_service_securite'] = true;
        } else {
            $this->valF['si_service_securite'] = false;
        }
        if (!is_numeric($val['si_personnel_jour'])) {
            $this->valF['si_personnel_jour'] = NULL;
        } else {
            $this->valF['si_personnel_jour'] = $val['si_personnel_jour'];
        }
        if (!is_numeric($val['si_personnel_nuit'])) {
            $this->valF['si_personnel_nuit'] = NULL;
        } else {
            $this->valF['si_personnel_nuit'] = $val['si_personnel_nuit'];
        }
            $this->valF['references_cadastrales'] = $val['references_cadastrales'];
        if (!is_numeric($val['dossier_coordination_periodique'])) {
            $this->valF['dossier_coordination_periodique'] = NULL;
        } else {
            $this->valF['dossier_coordination_periodique'] = $val['dossier_coordination_periodique'];
        }
        if ($val['geolocalise'] == 1 || $val['geolocalise'] == "t" || $val['geolocalise'] == "Oui") {
            $this->valF['geolocalise'] = true;
        } else {
            $this->valF['geolocalise'] = false;
        }
        if ($val['geom_point'] == "") {
            unset($this->valF['geom_point']);
        } else {
            $this->valF['geom_point'] = $val['geom_point'];
        }
        if ($val['geom_emprise'] == "") {
            unset($this->valF['geom_emprise']);
        } else {
            $this->valF['geom_emprise'] = $val['geom_emprise'];
        }
    }

    //=================================================
    //cle primaire automatique [automatic primary key]
    //==================================================

    function setId(&$dnu1 = null) {
    //numero automatique
        $this->valF[$this->clePrimaire] = $this->f->db->nextId(DB_PREFIXE.$this->table);
    }

    function setValFAjout($val = array()) {
    //numero automatique -> pas de controle ajout cle primaire
    }

    function verifierAjout($val = array(), &$dnu1 = null) {
    //numero automatique -> pas de verfication de cle primaire
    }
    /**
     * Methode verifier
     */
    function verifier($val = array(), &$dnu1 = null, $dnu2 = null) {
        // On appelle la methode de la classe parent
        parent::verifier($val, $this->f->db, null);

        // gestion des dates de validites
        $date_debut = $this->valF['om_validite_debut'];
        $date_fin = $this->valF['om_validite_fin'];

        if ($date_debut != '' and $date_fin != '') {
        
            $date_debut = explode('-', $this->valF['om_validite_debut']);
            $date_fin = explode('-', $this->valF['om_validite_fin']);

            $time_debut = mktime(0, 0, 0, $date_debut[1], $date_debut[2],
                                 $date_debut[0]);
            $time_fin = mktime(0, 0, 0, $date_fin[1], $date_fin[2],
                                 $date_fin[0]);

            if ($time_debut > $time_fin or $time_debut == $time_fin) {
                $this->correct = false;
                $this->addToMessage(__('La date de fin de validite doit etre future a la de debut de validite.'));
            }
        }
    }


    //==========================
    // Formulaire  [form]
    //==========================
    /**
     *
     */
    function setType(&$form, $maj) {
        // Récupération du mode de l'action
        $crud = $this->get_action_crud($maj);

        // MODE AJOUTER
        if ($maj == 0 || $crud == 'create') {
            $form->setType("etablissement", "hidden");
            $form->setType("code", "text");
            $form->setType("libelle", "text");
            $form->setType("adresse_numero", "text");
            $form->setType("adresse_numero2", "text");
            if ($this->is_in_context_of_foreign_key("voie", $this->retourformulaire)) {
                $form->setType("adresse_voie", "selecthiddenstatic");
            } else {
                $form->setType("adresse_voie", "select");
            }
            $form->setType("adresse_complement", "text");
            $form->setType("lieu_dit", "text");
            $form->setType("boite_postale", "text");
            $form->setType("adresse_cp", "text");
            $form->setType("adresse_ville", "text");
            if ($this->is_in_context_of_foreign_key("arrondissement", $this->retourformulaire)) {
                $form->setType("adresse_arrondissement", "selecthiddenstatic");
            } else {
                $form->setType("adresse_arrondissement", "select");
            }
            $form->setType("cedex", "text");
            $form->setType("npai", "checkbox");
            $form->setType("telephone", "text");
            $form->setType("fax", "text");
            if ($this->is_in_context_of_foreign_key("etablissement_nature", $this->retourformulaire)) {
                $form->setType("etablissement_nature", "selecthiddenstatic");
            } else {
                $form->setType("etablissement_nature", "select");
            }
            $form->setType("siret", "text");
            $form->setType("annee_de_construction", "text");
            if ($this->is_in_context_of_foreign_key("etablissement_statut_juridique", $this->retourformulaire)) {
                $form->setType("etablissement_statut_juridique", "selecthiddenstatic");
            } else {
                $form->setType("etablissement_statut_juridique", "select");
            }
            if ($this->is_in_context_of_foreign_key("etablissement_tutelle_adm", $this->retourformulaire)) {
                $form->setType("etablissement_tutelle_adm", "selecthiddenstatic");
            } else {
                $form->setType("etablissement_tutelle_adm", "select");
            }
            $form->setType("ref_patrimoine", "textarea");
            if ($this->is_in_context_of_foreign_key("etablissement_type", $this->retourformulaire)) {
                $form->setType("etablissement_type", "selecthiddenstatic");
            } else {
                $form->setType("etablissement_type", "select");
            }
            if ($this->is_in_context_of_foreign_key("etablissement_categorie", $this->retourformulaire)) {
                $form->setType("etablissement_categorie", "selecthiddenstatic");
            } else {
                $form->setType("etablissement_categorie", "select");
            }
            if ($this->is_in_context_of_foreign_key("etablissement_etat", $this->retourformulaire)) {
                $form->setType("etablissement_etat", "selecthiddenstatic");
            } else {
                $form->setType("etablissement_etat", "select");
            }
            $form->setType("date_arrete_ouverture", "date");
            $form->setType("autorite_police_encours", "checkbox");
            if ($this->f->isAccredited(array($this->table."_modifier_validite", $this->table, ))) {
                $form->setType("om_validite_debut", "date");
            } else {
                $form->setType("om_validite_debut", "hiddenstaticdate");
            }
            if ($this->f->isAccredited(array($this->table."_modifier_validite", $this->table, ))) {
                $form->setType("om_validite_fin", "date");
            } else {
                $form->setType("om_validite_fin", "hiddenstaticdate");
            }
            $form->setType("si_effectif_public", "text");
            $form->setType("si_effectif_personnel", "text");
            $form->setType("si_locaux_sommeil", "checkbox");
            $form->setType("si_periodicite_visites", "text");
            $form->setType("si_prochaine_visite_periodique_date_previsionnelle", "date");
            if ($this->is_in_context_of_foreign_key("visite_duree", $this->retourformulaire)) {
                $form->setType("si_visite_duree", "selecthiddenstatic");
            } else {
                $form->setType("si_visite_duree", "select");
            }
            $form->setType("si_derniere_visite_periodique_date", "date");
            $form->setType("si_derniere_visite_date", "date");
            if ($this->is_in_context_of_foreign_key("reunion_avis", $this->retourformulaire)) {
                $form->setType("si_derniere_visite_avis", "selecthiddenstatic");
            } else {
                $form->setType("si_derniere_visite_avis", "select");
            }
            if ($this->is_in_context_of_foreign_key("acteur", $this->retourformulaire)) {
                $form->setType("si_derniere_visite_technicien", "selecthiddenstatic");
            } else {
                $form->setType("si_derniere_visite_technicien", "select");
            }
            $form->setType("si_prochaine_visite_date", "date");
            if ($this->is_in_context_of_foreign_key("analyses_type", $this->retourformulaire)) {
                $form->setType("si_prochaine_visite_type", "selecthiddenstatic");
            } else {
                $form->setType("si_prochaine_visite_type", "select");
            }
            $form->setType("acc_derniere_visite_date", "date");
            if ($this->is_in_context_of_foreign_key("reunion_avis", $this->retourformulaire)) {
                $form->setType("acc_derniere_visite_avis", "selecthiddenstatic");
            } else {
                $form->setType("acc_derniere_visite_avis", "select");
            }
            if ($this->is_in_context_of_foreign_key("acteur", $this->retourformulaire)) {
                $form->setType("acc_derniere_visite_technicien", "selecthiddenstatic");
            } else {
                $form->setType("acc_derniere_visite_technicien", "select");
            }
            $form->setType("acc_consignes_om_html", "html");
            $form->setType("acc_descriptif_om_html", "html");
            $form->setType("si_consignes_om_html", "html");
            $form->setType("si_descriptif_om_html", "html");
            if ($this->is_in_context_of_foreign_key("autorite_competente", $this->retourformulaire)) {
                $form->setType("si_autorite_competente_visite", "selecthiddenstatic");
            } else {
                $form->setType("si_autorite_competente_visite", "select");
            }
            if ($this->is_in_context_of_foreign_key("autorite_competente", $this->retourformulaire)) {
                $form->setType("si_autorite_competente_plan", "selecthiddenstatic");
            } else {
                $form->setType("si_autorite_competente_plan", "select");
            }
            if ($this->is_in_context_of_foreign_key("reunion_avis", $this->retourformulaire)) {
                $form->setType("si_dernier_plan_avis", "selecthiddenstatic");
            } else {
                $form->setType("si_dernier_plan_avis", "select");
            }
            $form->setType("si_type_alarme", "textarea");
            $form->setType("si_type_ssi", "text");
            $form->setType("si_conformite_l16", "checkbox");
            $form->setType("si_alimentation_remplacement", "checkbox");
            $form->setType("si_service_securite", "checkbox");
            $form->setType("si_personnel_jour", "text");
            $form->setType("si_personnel_nuit", "text");
            $form->setType("references_cadastrales", "textarea");
            if ($this->is_in_context_of_foreign_key("dossier_coordination", $this->retourformulaire)) {
                $form->setType("dossier_coordination_periodique", "selecthiddenstatic");
            } else {
                $form->setType("dossier_coordination_periodique", "select");
            }
            $form->setType("geolocalise", "checkbox");
            $form->setType("geom_point", "geom");
            $form->setType("geom_emprise", "geom");
        }

        // MDOE MODIFIER
        if ($maj == 1 || $crud == 'update') {
            $form->setType("etablissement", "hiddenstatic");
            $form->setType("code", "text");
            $form->setType("libelle", "text");
            $form->setType("adresse_numero", "text");
            $form->setType("adresse_numero2", "text");
            if ($this->is_in_context_of_foreign_key("voie", $this->retourformulaire)) {
                $form->setType("adresse_voie", "selecthiddenstatic");
            } else {
                $form->setType("adresse_voie", "select");
            }
            $form->setType("adresse_complement", "text");
            $form->setType("lieu_dit", "text");
            $form->setType("boite_postale", "text");
            $form->setType("adresse_cp", "text");
            $form->setType("adresse_ville", "text");
            if ($this->is_in_context_of_foreign_key("arrondissement", $this->retourformulaire)) {
                $form->setType("adresse_arrondissement", "selecthiddenstatic");
            } else {
                $form->setType("adresse_arrondissement", "select");
            }
            $form->setType("cedex", "text");
            $form->setType("npai", "checkbox");
            $form->setType("telephone", "text");
            $form->setType("fax", "text");
            if ($this->is_in_context_of_foreign_key("etablissement_nature", $this->retourformulaire)) {
                $form->setType("etablissement_nature", "selecthiddenstatic");
            } else {
                $form->setType("etablissement_nature", "select");
            }
            $form->setType("siret", "text");
            $form->setType("annee_de_construction", "text");
            if ($this->is_in_context_of_foreign_key("etablissement_statut_juridique", $this->retourformulaire)) {
                $form->setType("etablissement_statut_juridique", "selecthiddenstatic");
            } else {
                $form->setType("etablissement_statut_juridique", "select");
            }
            if ($this->is_in_context_of_foreign_key("etablissement_tutelle_adm", $this->retourformulaire)) {
                $form->setType("etablissement_tutelle_adm", "selecthiddenstatic");
            } else {
                $form->setType("etablissement_tutelle_adm", "select");
            }
            $form->setType("ref_patrimoine", "textarea");
            if ($this->is_in_context_of_foreign_key("etablissement_type", $this->retourformulaire)) {
                $form->setType("etablissement_type", "selecthiddenstatic");
            } else {
                $form->setType("etablissement_type", "select");
            }
            if ($this->is_in_context_of_foreign_key("etablissement_categorie", $this->retourformulaire)) {
                $form->setType("etablissement_categorie", "selecthiddenstatic");
            } else {
                $form->setType("etablissement_categorie", "select");
            }
            if ($this->is_in_context_of_foreign_key("etablissement_etat", $this->retourformulaire)) {
                $form->setType("etablissement_etat", "selecthiddenstatic");
            } else {
                $form->setType("etablissement_etat", "select");
            }
            $form->setType("date_arrete_ouverture", "date");
            $form->setType("autorite_police_encours", "checkbox");
            if ($this->f->isAccredited(array($this->table."_modifier_validite", $this->table, ))) {
                $form->setType("om_validite_debut", "date");
            } else {
                $form->setType("om_validite_debut", "hiddenstaticdate");
            }
            if ($this->f->isAccredited(array($this->table."_modifier_validite", $this->table, ))) {
                $form->setType("om_validite_fin", "date");
            } else {
                $form->setType("om_validite_fin", "hiddenstaticdate");
            }
            $form->setType("si_effectif_public", "text");
            $form->setType("si_effectif_personnel", "text");
            $form->setType("si_locaux_sommeil", "checkbox");
            $form->setType("si_periodicite_visites", "text");
            $form->setType("si_prochaine_visite_periodique_date_previsionnelle", "date");
            if ($this->is_in_context_of_foreign_key("visite_duree", $this->retourformulaire)) {
                $form->setType("si_visite_duree", "selecthiddenstatic");
            } else {
                $form->setType("si_visite_duree", "select");
            }
            $form->setType("si_derniere_visite_periodique_date", "date");
            $form->setType("si_derniere_visite_date", "date");
            if ($this->is_in_context_of_foreign_key("reunion_avis", $this->retourformulaire)) {
                $form->setType("si_derniere_visite_avis", "selecthiddenstatic");
            } else {
                $form->setType("si_derniere_visite_avis", "select");
            }
            if ($this->is_in_context_of_foreign_key("acteur", $this->retourformulaire)) {
                $form->setType("si_derniere_visite_technicien", "selecthiddenstatic");
            } else {
                $form->setType("si_derniere_visite_technicien", "select");
            }
            $form->setType("si_prochaine_visite_date", "date");
            if ($this->is_in_context_of_foreign_key("analyses_type", $this->retourformulaire)) {
                $form->setType("si_prochaine_visite_type", "selecthiddenstatic");
            } else {
                $form->setType("si_prochaine_visite_type", "select");
            }
            $form->setType("acc_derniere_visite_date", "date");
            if ($this->is_in_context_of_foreign_key("reunion_avis", $this->retourformulaire)) {
                $form->setType("acc_derniere_visite_avis", "selecthiddenstatic");
            } else {
                $form->setType("acc_derniere_visite_avis", "select");
            }
            if ($this->is_in_context_of_foreign_key("acteur", $this->retourformulaire)) {
                $form->setType("acc_derniere_visite_technicien", "selecthiddenstatic");
            } else {
                $form->setType("acc_derniere_visite_technicien", "select");
            }
            $form->setType("acc_consignes_om_html", "html");
            $form->setType("acc_descriptif_om_html", "html");
            $form->setType("si_consignes_om_html", "html");
            $form->setType("si_descriptif_om_html", "html");
            if ($this->is_in_context_of_foreign_key("autorite_competente", $this->retourformulaire)) {
                $form->setType("si_autorite_competente_visite", "selecthiddenstatic");
            } else {
                $form->setType("si_autorite_competente_visite", "select");
            }
            if ($this->is_in_context_of_foreign_key("autorite_competente", $this->retourformulaire)) {
                $form->setType("si_autorite_competente_plan", "selecthiddenstatic");
            } else {
                $form->setType("si_autorite_competente_plan", "select");
            }
            if ($this->is_in_context_of_foreign_key("reunion_avis", $this->retourformulaire)) {
                $form->setType("si_dernier_plan_avis", "selecthiddenstatic");
            } else {
                $form->setType("si_dernier_plan_avis", "select");
            }
            $form->setType("si_type_alarme", "textarea");
            $form->setType("si_type_ssi", "text");
            $form->setType("si_conformite_l16", "checkbox");
            $form->setType("si_alimentation_remplacement", "checkbox");
            $form->setType("si_service_securite", "checkbox");
            $form->setType("si_personnel_jour", "text");
            $form->setType("si_personnel_nuit", "text");
            $form->setType("references_cadastrales", "textarea");
            if ($this->is_in_context_of_foreign_key("dossier_coordination", $this->retourformulaire)) {
                $form->setType("dossier_coordination_periodique", "selecthiddenstatic");
            } else {
                $form->setType("dossier_coordination_periodique", "select");
            }
            $form->setType("geolocalise", "checkbox");
            $form->setType("geom_point", "geom");
            $form->setType("geom_emprise", "geom");
        }

        // MODE SUPPRIMER
        if ($maj == 2 || $crud == 'delete') {
            $form->setType("etablissement", "hiddenstatic");
            $form->setType("code", "hiddenstatic");
            $form->setType("libelle", "hiddenstatic");
            $form->setType("adresse_numero", "hiddenstatic");
            $form->setType("adresse_numero2", "hiddenstatic");
            $form->setType("adresse_voie", "selectstatic");
            $form->setType("adresse_complement", "hiddenstatic");
            $form->setType("lieu_dit", "hiddenstatic");
            $form->setType("boite_postale", "hiddenstatic");
            $form->setType("adresse_cp", "hiddenstatic");
            $form->setType("adresse_ville", "hiddenstatic");
            $form->setType("adresse_arrondissement", "selectstatic");
            $form->setType("cedex", "hiddenstatic");
            $form->setType("npai", "hiddenstatic");
            $form->setType("telephone", "hiddenstatic");
            $form->setType("fax", "hiddenstatic");
            $form->setType("etablissement_nature", "selectstatic");
            $form->setType("siret", "hiddenstatic");
            $form->setType("annee_de_construction", "hiddenstatic");
            $form->setType("etablissement_statut_juridique", "selectstatic");
            $form->setType("etablissement_tutelle_adm", "selectstatic");
            $form->setType("ref_patrimoine", "hiddenstatic");
            $form->setType("etablissement_type", "selectstatic");
            $form->setType("etablissement_categorie", "selectstatic");
            $form->setType("etablissement_etat", "selectstatic");
            $form->setType("date_arrete_ouverture", "hiddenstatic");
            $form->setType("autorite_police_encours", "hiddenstatic");
            $form->setType("om_validite_debut", "hiddenstatic");
            $form->setType("om_validite_fin", "hiddenstatic");
            $form->setType("si_effectif_public", "hiddenstatic");
            $form->setType("si_effectif_personnel", "hiddenstatic");
            $form->setType("si_locaux_sommeil", "hiddenstatic");
            $form->setType("si_periodicite_visites", "hiddenstatic");
            $form->setType("si_prochaine_visite_periodique_date_previsionnelle", "hiddenstatic");
            $form->setType("si_visite_duree", "selectstatic");
            $form->setType("si_derniere_visite_periodique_date", "hiddenstatic");
            $form->setType("si_derniere_visite_date", "hiddenstatic");
            $form->setType("si_derniere_visite_avis", "selectstatic");
            $form->setType("si_derniere_visite_technicien", "selectstatic");
            $form->setType("si_prochaine_visite_date", "hiddenstatic");
            $form->setType("si_prochaine_visite_type", "selectstatic");
            $form->setType("acc_derniere_visite_date", "hiddenstatic");
            $form->setType("acc_derniere_visite_avis", "selectstatic");
            $form->setType("acc_derniere_visite_technicien", "selectstatic");
            $form->setType("acc_consignes_om_html", "hiddenstatic");
            $form->setType("acc_descriptif_om_html", "hiddenstatic");
            $form->setType("si_consignes_om_html", "hiddenstatic");
            $form->setType("si_descriptif_om_html", "hiddenstatic");
            $form->setType("si_autorite_competente_visite", "selectstatic");
            $form->setType("si_autorite_competente_plan", "selectstatic");
            $form->setType("si_dernier_plan_avis", "selectstatic");
            $form->setType("si_type_alarme", "hiddenstatic");
            $form->setType("si_type_ssi", "hiddenstatic");
            $form->setType("si_conformite_l16", "hiddenstatic");
            $form->setType("si_alimentation_remplacement", "hiddenstatic");
            $form->setType("si_service_securite", "hiddenstatic");
            $form->setType("si_personnel_jour", "hiddenstatic");
            $form->setType("si_personnel_nuit", "hiddenstatic");
            $form->setType("references_cadastrales", "hiddenstatic");
            $form->setType("dossier_coordination_periodique", "selectstatic");
            $form->setType("geolocalise", "hiddenstatic");
            $form->setType("geom_point", "geom");
            $form->setType("geom_emprise", "geom");
        }

        // MODE CONSULTER
        if ($maj == 3 || $crud == 'read') {
            $form->setType("etablissement", "static");
            $form->setType("code", "static");
            $form->setType("libelle", "static");
            $form->setType("adresse_numero", "static");
            $form->setType("adresse_numero2", "static");
            $form->setType("adresse_voie", "selectstatic");
            $form->setType("adresse_complement", "static");
            $form->setType("lieu_dit", "static");
            $form->setType("boite_postale", "static");
            $form->setType("adresse_cp", "static");
            $form->setType("adresse_ville", "static");
            $form->setType("adresse_arrondissement", "selectstatic");
            $form->setType("cedex", "static");
            $form->setType("npai", "checkboxstatic");
            $form->setType("telephone", "static");
            $form->setType("fax", "static");
            $form->setType("etablissement_nature", "selectstatic");
            $form->setType("siret", "static");
            $form->setType("annee_de_construction", "static");
            $form->setType("etablissement_statut_juridique", "selectstatic");
            $form->setType("etablissement_tutelle_adm", "selectstatic");
            $form->setType("ref_patrimoine", "textareastatic");
            $form->setType("etablissement_type", "selectstatic");
            $form->setType("etablissement_categorie", "selectstatic");
            $form->setType("etablissement_etat", "selectstatic");
            $form->setType("date_arrete_ouverture", "datestatic");
            $form->setType("autorite_police_encours", "checkboxstatic");
            $form->setType("om_validite_debut", "datestatic");
            $form->setType("om_validite_fin", "datestatic");
            $form->setType("si_effectif_public", "static");
            $form->setType("si_effectif_personnel", "static");
            $form->setType("si_locaux_sommeil", "checkboxstatic");
            $form->setType("si_periodicite_visites", "static");
            $form->setType("si_prochaine_visite_periodique_date_previsionnelle", "datestatic");
            $form->setType("si_visite_duree", "selectstatic");
            $form->setType("si_derniere_visite_periodique_date", "datestatic");
            $form->setType("si_derniere_visite_date", "datestatic");
            $form->setType("si_derniere_visite_avis", "selectstatic");
            $form->setType("si_derniere_visite_technicien", "selectstatic");
            $form->setType("si_prochaine_visite_date", "datestatic");
            $form->setType("si_prochaine_visite_type", "selectstatic");
            $form->setType("acc_derniere_visite_date", "datestatic");
            $form->setType("acc_derniere_visite_avis", "selectstatic");
            $form->setType("acc_derniere_visite_technicien", "selectstatic");
            $form->setType("acc_consignes_om_html", "htmlstatic");
            $form->setType("acc_descriptif_om_html", "htmlstatic");
            $form->setType("si_consignes_om_html", "htmlstatic");
            $form->setType("si_descriptif_om_html", "htmlstatic");
            $form->setType("si_autorite_competente_visite", "selectstatic");
            $form->setType("si_autorite_competente_plan", "selectstatic");
            $form->setType("si_dernier_plan_avis", "selectstatic");
            $form->setType("si_type_alarme", "textareastatic");
            $form->setType("si_type_ssi", "static");
            $form->setType("si_conformite_l16", "checkboxstatic");
            $form->setType("si_alimentation_remplacement", "checkboxstatic");
            $form->setType("si_service_securite", "checkboxstatic");
            $form->setType("si_personnel_jour", "static");
            $form->setType("si_personnel_nuit", "static");
            $form->setType("references_cadastrales", "textareastatic");
            $form->setType("dossier_coordination_periodique", "selectstatic");
            $form->setType("geolocalise", "checkboxstatic");
            $form->setType("geom_point", "geom");
            $form->setType("geom_emprise", "geom");
        }

    }


    function setOnchange(&$form, $maj) {
    //javascript controle client
        $form->setOnchange('etablissement','VerifNum(this)');
        $form->setOnchange('adresse_numero','VerifNum(this)');
        $form->setOnchange('adresse_voie','VerifNum(this)');
        $form->setOnchange('adresse_arrondissement','VerifNum(this)');
        $form->setOnchange('etablissement_nature','VerifNum(this)');
        $form->setOnchange('etablissement_statut_juridique','VerifNum(this)');
        $form->setOnchange('etablissement_tutelle_adm','VerifNum(this)');
        $form->setOnchange('etablissement_type','VerifNum(this)');
        $form->setOnchange('etablissement_categorie','VerifNum(this)');
        $form->setOnchange('etablissement_etat','VerifNum(this)');
        $form->setOnchange('date_arrete_ouverture','fdate(this)');
        $form->setOnchange('om_validite_debut','fdate(this)');
        $form->setOnchange('om_validite_fin','fdate(this)');
        $form->setOnchange('si_effectif_public','VerifNum(this)');
        $form->setOnchange('si_effectif_personnel','VerifNum(this)');
        $form->setOnchange('si_periodicite_visites','VerifNum(this)');
        $form->setOnchange('si_prochaine_visite_periodique_date_previsionnelle','fdate(this)');
        $form->setOnchange('si_visite_duree','VerifNum(this)');
        $form->setOnchange('si_derniere_visite_periodique_date','fdate(this)');
        $form->setOnchange('si_derniere_visite_date','fdate(this)');
        $form->setOnchange('si_derniere_visite_avis','VerifNum(this)');
        $form->setOnchange('si_derniere_visite_technicien','VerifNum(this)');
        $form->setOnchange('si_prochaine_visite_date','fdate(this)');
        $form->setOnchange('si_prochaine_visite_type','VerifNum(this)');
        $form->setOnchange('acc_derniere_visite_date','fdate(this)');
        $form->setOnchange('acc_derniere_visite_avis','VerifNum(this)');
        $form->setOnchange('acc_derniere_visite_technicien','VerifNum(this)');
        $form->setOnchange('si_autorite_competente_visite','VerifNum(this)');
        $form->setOnchange('si_autorite_competente_plan','VerifNum(this)');
        $form->setOnchange('si_dernier_plan_avis','VerifNum(this)');
        $form->setOnchange('si_personnel_jour','VerifNum(this)');
        $form->setOnchange('si_personnel_nuit','VerifNum(this)');
        $form->setOnchange('dossier_coordination_periodique','VerifNum(this)');
    }
    /**
     * Methode setTaille
     */
    function setTaille(&$form, $maj) {
        $form->setTaille("etablissement", 11);
        $form->setTaille("code", 25);
        $form->setTaille("libelle", 30);
        $form->setTaille("adresse_numero", 11);
        $form->setTaille("adresse_numero2", 10);
        $form->setTaille("adresse_voie", 11);
        $form->setTaille("adresse_complement", 30);
        $form->setTaille("lieu_dit", 30);
        $form->setTaille("boite_postale", 10);
        $form->setTaille("adresse_cp", 10);
        $form->setTaille("adresse_ville", 30);
        $form->setTaille("adresse_arrondissement", 11);
        $form->setTaille("cedex", 10);
        $form->setTaille("npai", 1);
        $form->setTaille("telephone", 20);
        $form->setTaille("fax", 20);
        $form->setTaille("etablissement_nature", 11);
        $form->setTaille("siret", 20);
        $form->setTaille("annee_de_construction", 10);
        $form->setTaille("etablissement_statut_juridique", 11);
        $form->setTaille("etablissement_tutelle_adm", 11);
        $form->setTaille("ref_patrimoine", 80);
        $form->setTaille("etablissement_type", 11);
        $form->setTaille("etablissement_categorie", 11);
        $form->setTaille("etablissement_etat", 11);
        $form->setTaille("date_arrete_ouverture", 12);
        $form->setTaille("autorite_police_encours", 1);
        $form->setTaille("om_validite_debut", 12);
        $form->setTaille("om_validite_fin", 12);
        $form->setTaille("si_effectif_public", 11);
        $form->setTaille("si_effectif_personnel", 11);
        $form->setTaille("si_locaux_sommeil", 1);
        $form->setTaille("si_periodicite_visites", 11);
        $form->setTaille("si_prochaine_visite_periodique_date_previsionnelle", 12);
        $form->setTaille("si_visite_duree", 11);
        $form->setTaille("si_derniere_visite_periodique_date", 12);
        $form->setTaille("si_derniere_visite_date", 12);
        $form->setTaille("si_derniere_visite_avis", 11);
        $form->setTaille("si_derniere_visite_technicien", 11);
        $form->setTaille("si_prochaine_visite_date", 12);
        $form->setTaille("si_prochaine_visite_type", 11);
        $form->setTaille("acc_derniere_visite_date", 12);
        $form->setTaille("acc_derniere_visite_avis", 11);
        $form->setTaille("acc_derniere_visite_technicien", 11);
        $form->setTaille("acc_consignes_om_html", 80);
        $form->setTaille("acc_descriptif_om_html", 80);
        $form->setTaille("si_consignes_om_html", 80);
        $form->setTaille("si_descriptif_om_html", 80);
        $form->setTaille("si_autorite_competente_visite", 11);
        $form->setTaille("si_autorite_competente_plan", 11);
        $form->setTaille("si_dernier_plan_avis", 11);
        $form->setTaille("si_type_alarme", 80);
        $form->setTaille("si_type_ssi", 30);
        $form->setTaille("si_conformite_l16", 1);
        $form->setTaille("si_alimentation_remplacement", 1);
        $form->setTaille("si_service_securite", 1);
        $form->setTaille("si_personnel_jour", 11);
        $form->setTaille("si_personnel_nuit", 11);
        $form->setTaille("references_cadastrales", 80);
        $form->setTaille("dossier_coordination_periodique", 11);
        $form->setTaille("geolocalise", 1);
        $form->setTaille("geom_point", 30);
        $form->setTaille("geom_emprise", 30);
    }

    /**
     * Methode setMax
     */
    function setMax(&$form, $maj) {
        $form->setMax("etablissement", 11);
        $form->setMax("code", 25);
        $form->setMax("libelle", 100);
        $form->setMax("adresse_numero", 11);
        $form->setMax("adresse_numero2", 10);
        $form->setMax("adresse_voie", 11);
        $form->setMax("adresse_complement", 40);
        $form->setMax("lieu_dit", 39);
        $form->setMax("boite_postale", 5);
        $form->setMax("adresse_cp", 6);
        $form->setMax("adresse_ville", 40);
        $form->setMax("adresse_arrondissement", 11);
        $form->setMax("cedex", 5);
        $form->setMax("npai", 1);
        $form->setMax("telephone", 20);
        $form->setMax("fax", 20);
        $form->setMax("etablissement_nature", 11);
        $form->setMax("siret", 20);
        $form->setMax("annee_de_construction", 4);
        $form->setMax("etablissement_statut_juridique", 11);
        $form->setMax("etablissement_tutelle_adm", 11);
        $form->setMax("ref_patrimoine", 6);
        $form->setMax("etablissement_type", 11);
        $form->setMax("etablissement_categorie", 11);
        $form->setMax("etablissement_etat", 11);
        $form->setMax("date_arrete_ouverture", 12);
        $form->setMax("autorite_police_encours", 1);
        $form->setMax("om_validite_debut", 12);
        $form->setMax("om_validite_fin", 12);
        $form->setMax("si_effectif_public", 11);
        $form->setMax("si_effectif_personnel", 11);
        $form->setMax("si_locaux_sommeil", 1);
        $form->setMax("si_periodicite_visites", 11);
        $form->setMax("si_prochaine_visite_periodique_date_previsionnelle", 12);
        $form->setMax("si_visite_duree", 11);
        $form->setMax("si_derniere_visite_periodique_date", 12);
        $form->setMax("si_derniere_visite_date", 12);
        $form->setMax("si_derniere_visite_avis", 11);
        $form->setMax("si_derniere_visite_technicien", 11);
        $form->setMax("si_prochaine_visite_date", 12);
        $form->setMax("si_prochaine_visite_type", 11);
        $form->setMax("acc_derniere_visite_date", 12);
        $form->setMax("acc_derniere_visite_avis", 11);
        $form->setMax("acc_derniere_visite_technicien", 11);
        $form->setMax("acc_consignes_om_html", 6);
        $form->setMax("acc_descriptif_om_html", 6);
        $form->setMax("si_consignes_om_html", 6);
        $form->setMax("si_descriptif_om_html", 6);
        $form->setMax("si_autorite_competente_visite", 11);
        $form->setMax("si_autorite_competente_plan", 11);
        $form->setMax("si_dernier_plan_avis", 11);
        $form->setMax("si_type_alarme", 6);
        $form->setMax("si_type_ssi", 100);
        $form->setMax("si_conformite_l16", 1);
        $form->setMax("si_alimentation_remplacement", 1);
        $form->setMax("si_service_securite", 1);
        $form->setMax("si_personnel_jour", 11);
        $form->setMax("si_personnel_nuit", 11);
        $form->setMax("references_cadastrales", 6);
        $form->setMax("dossier_coordination_periodique", 11);
        $form->setMax("geolocalise", 1);
        $form->setMax("geom_point", 551424);
        $form->setMax("geom_emprise", 551444);
    }


    function setLib(&$form, $maj) {
    //libelle des champs
        $form->setLib('etablissement', __('etablissement'));
        $form->setLib('code', __('code'));
        $form->setLib('libelle', __('libelle'));
        $form->setLib('adresse_numero', __('adresse_numero'));
        $form->setLib('adresse_numero2', __('adresse_numero2'));
        $form->setLib('adresse_voie', __('adresse_voie'));
        $form->setLib('adresse_complement', __('adresse_complement'));
        $form->setLib('lieu_dit', __('lieu_dit'));
        $form->setLib('boite_postale', __('boite_postale'));
        $form->setLib('adresse_cp', __('adresse_cp'));
        $form->setLib('adresse_ville', __('adresse_ville'));
        $form->setLib('adresse_arrondissement', __('adresse_arrondissement'));
        $form->setLib('cedex', __('cedex'));
        $form->setLib('npai', __('npai'));
        $form->setLib('telephone', __('telephone'));
        $form->setLib('fax', __('fax'));
        $form->setLib('etablissement_nature', __('etablissement_nature'));
        $form->setLib('siret', __('siret'));
        $form->setLib('annee_de_construction', __('annee_de_construction'));
        $form->setLib('etablissement_statut_juridique', __('etablissement_statut_juridique'));
        $form->setLib('etablissement_tutelle_adm', __('etablissement_tutelle_adm'));
        $form->setLib('ref_patrimoine', __('ref_patrimoine'));
        $form->setLib('etablissement_type', __('etablissement_type'));
        $form->setLib('etablissement_categorie', __('etablissement_categorie'));
        $form->setLib('etablissement_etat', __('etablissement_etat'));
        $form->setLib('date_arrete_ouverture', __('date_arrete_ouverture'));
        $form->setLib('autorite_police_encours', __('autorite_police_encours'));
        $form->setLib('om_validite_debut', __('om_validite_debut'));
        $form->setLib('om_validite_fin', __('om_validite_fin'));
        $form->setLib('si_effectif_public', __('si_effectif_public'));
        $form->setLib('si_effectif_personnel', __('si_effectif_personnel'));
        $form->setLib('si_locaux_sommeil', __('si_locaux_sommeil'));
        $form->setLib('si_periodicite_visites', __('si_periodicite_visites'));
        $form->setLib('si_prochaine_visite_periodique_date_previsionnelle', __('si_prochaine_visite_periodique_date_previsionnelle'));
        $form->setLib('si_visite_duree', __('si_visite_duree'));
        $form->setLib('si_derniere_visite_periodique_date', __('si_derniere_visite_periodique_date'));
        $form->setLib('si_derniere_visite_date', __('si_derniere_visite_date'));
        $form->setLib('si_derniere_visite_avis', __('si_derniere_visite_avis'));
        $form->setLib('si_derniere_visite_technicien', __('si_derniere_visite_technicien'));
        $form->setLib('si_prochaine_visite_date', __('si_prochaine_visite_date'));
        $form->setLib('si_prochaine_visite_type', __('si_prochaine_visite_type'));
        $form->setLib('acc_derniere_visite_date', __('acc_derniere_visite_date'));
        $form->setLib('acc_derniere_visite_avis', __('acc_derniere_visite_avis'));
        $form->setLib('acc_derniere_visite_technicien', __('acc_derniere_visite_technicien'));
        $form->setLib('acc_consignes_om_html', __('acc_consignes_om_html'));
        $form->setLib('acc_descriptif_om_html', __('acc_descriptif_om_html'));
        $form->setLib('si_consignes_om_html', __('si_consignes_om_html'));
        $form->setLib('si_descriptif_om_html', __('si_descriptif_om_html'));
        $form->setLib('si_autorite_competente_visite', __('si_autorite_competente_visite'));
        $form->setLib('si_autorite_competente_plan', __('si_autorite_competente_plan'));
        $form->setLib('si_dernier_plan_avis', __('si_dernier_plan_avis'));
        $form->setLib('si_type_alarme', __('si_type_alarme'));
        $form->setLib('si_type_ssi', __('si_type_ssi'));
        $form->setLib('si_conformite_l16', __('si_conformite_l16'));
        $form->setLib('si_alimentation_remplacement', __('si_alimentation_remplacement'));
        $form->setLib('si_service_securite', __('si_service_securite'));
        $form->setLib('si_personnel_jour', __('si_personnel_jour'));
        $form->setLib('si_personnel_nuit', __('si_personnel_nuit'));
        $form->setLib('references_cadastrales', __('references_cadastrales'));
        $form->setLib('dossier_coordination_periodique', __('dossier_coordination_periodique'));
        $form->setLib('geolocalise', __('geolocalise'));
        $form->setLib('geom_point', __('geom_point'));
        $form->setLib('geom_emprise', __('geom_emprise'));
    }
    /**
     *
     */
    function setSelect(&$form, $maj, &$dnu1 = null, $dnu2 = null) {

        // acc_derniere_visite_avis
        $this->init_select(
            $form, 
            $this->f->db,
            $maj,
            null,
            "acc_derniere_visite_avis",
            $this->get_var_sql_forminc__sql("acc_derniere_visite_avis"),
            $this->get_var_sql_forminc__sql("acc_derniere_visite_avis_by_id"),
            true
        );
        // acc_derniere_visite_technicien
        $this->init_select(
            $form, 
            $this->f->db,
            $maj,
            null,
            "acc_derniere_visite_technicien",
            $this->get_var_sql_forminc__sql("acc_derniere_visite_technicien"),
            $this->get_var_sql_forminc__sql("acc_derniere_visite_technicien_by_id"),
            true
        );
        // adresse_arrondissement
        $this->init_select(
            $form, 
            $this->f->db,
            $maj,
            null,
            "adresse_arrondissement",
            $this->get_var_sql_forminc__sql("adresse_arrondissement"),
            $this->get_var_sql_forminc__sql("adresse_arrondissement_by_id"),
            true
        );
        // adresse_voie
        $this->init_select(
            $form, 
            $this->f->db,
            $maj,
            null,
            "adresse_voie",
            $this->get_var_sql_forminc__sql("adresse_voie"),
            $this->get_var_sql_forminc__sql("adresse_voie_by_id"),
            true
        );
        // dossier_coordination_periodique
        $this->init_select(
            $form, 
            $this->f->db,
            $maj,
            null,
            "dossier_coordination_periodique",
            $this->get_var_sql_forminc__sql("dossier_coordination_periodique"),
            $this->get_var_sql_forminc__sql("dossier_coordination_periodique_by_id"),
            false
        );
        // etablissement_categorie
        $this->init_select(
            $form, 
            $this->f->db,
            $maj,
            null,
            "etablissement_categorie",
            $this->get_var_sql_forminc__sql("etablissement_categorie"),
            $this->get_var_sql_forminc__sql("etablissement_categorie_by_id"),
            true
        );
        // etablissement_etat
        $this->init_select(
            $form, 
            $this->f->db,
            $maj,
            null,
            "etablissement_etat",
            $this->get_var_sql_forminc__sql("etablissement_etat"),
            $this->get_var_sql_forminc__sql("etablissement_etat_by_id"),
            true
        );
        // etablissement_nature
        $this->init_select(
            $form, 
            $this->f->db,
            $maj,
            null,
            "etablissement_nature",
            $this->get_var_sql_forminc__sql("etablissement_nature"),
            $this->get_var_sql_forminc__sql("etablissement_nature_by_id"),
            true
        );
        // etablissement_statut_juridique
        $this->init_select(
            $form, 
            $this->f->db,
            $maj,
            null,
            "etablissement_statut_juridique",
            $this->get_var_sql_forminc__sql("etablissement_statut_juridique"),
            $this->get_var_sql_forminc__sql("etablissement_statut_juridique_by_id"),
            true
        );
        // etablissement_tutelle_adm
        $this->init_select(
            $form, 
            $this->f->db,
            $maj,
            null,
            "etablissement_tutelle_adm",
            $this->get_var_sql_forminc__sql("etablissement_tutelle_adm"),
            $this->get_var_sql_forminc__sql("etablissement_tutelle_adm_by_id"),
            true
        );
        // etablissement_type
        $this->init_select(
            $form, 
            $this->f->db,
            $maj,
            null,
            "etablissement_type",
            $this->get_var_sql_forminc__sql("etablissement_type"),
            $this->get_var_sql_forminc__sql("etablissement_type_by_id"),
            true
        );
        // si_autorite_competente_plan
        $this->init_select(
            $form, 
            $this->f->db,
            $maj,
            null,
            "si_autorite_competente_plan",
            $this->get_var_sql_forminc__sql("si_autorite_competente_plan"),
            $this->get_var_sql_forminc__sql("si_autorite_competente_plan_by_id"),
            false
        );
        // si_autorite_competente_visite
        $this->init_select(
            $form, 
            $this->f->db,
            $maj,
            null,
            "si_autorite_competente_visite",
            $this->get_var_sql_forminc__sql("si_autorite_competente_visite"),
            $this->get_var_sql_forminc__sql("si_autorite_competente_visite_by_id"),
            false
        );
        // si_dernier_plan_avis
        $this->init_select(
            $form, 
            $this->f->db,
            $maj,
            null,
            "si_dernier_plan_avis",
            $this->get_var_sql_forminc__sql("si_dernier_plan_avis"),
            $this->get_var_sql_forminc__sql("si_dernier_plan_avis_by_id"),
            true
        );
        // si_derniere_visite_avis
        $this->init_select(
            $form, 
            $this->f->db,
            $maj,
            null,
            "si_derniere_visite_avis",
            $this->get_var_sql_forminc__sql("si_derniere_visite_avis"),
            $this->get_var_sql_forminc__sql("si_derniere_visite_avis_by_id"),
            true
        );
        // si_derniere_visite_technicien
        $this->init_select(
            $form, 
            $this->f->db,
            $maj,
            null,
            "si_derniere_visite_technicien",
            $this->get_var_sql_forminc__sql("si_derniere_visite_technicien"),
            $this->get_var_sql_forminc__sql("si_derniere_visite_technicien_by_id"),
            true
        );
        // si_prochaine_visite_type
        $this->init_select(
            $form, 
            $this->f->db,
            $maj,
            null,
            "si_prochaine_visite_type",
            $this->get_var_sql_forminc__sql("si_prochaine_visite_type"),
            $this->get_var_sql_forminc__sql("si_prochaine_visite_type_by_id"),
            true
        );
        // si_visite_duree
        $this->init_select(
            $form, 
            $this->f->db,
            $maj,
            null,
            "si_visite_duree",
            $this->get_var_sql_forminc__sql("si_visite_duree"),
            $this->get_var_sql_forminc__sql("si_visite_duree_by_id"),
            false
        );
        // geom_point
        if ($maj == 1 || $maj == 3) {
            $contenu = array();
            $contenu[0] = array("etablissement", $this->getParameter("idx"), "0");
            $form->setSelect("geom_point", $contenu);
        }
        // geom_emprise
        if ($maj == 1 || $maj == 3) {
            $contenu = array();
            $contenu[0] = array("etablissement", $this->getParameter("idx"), "1");
            $form->setSelect("geom_emprise", $contenu);
        }
    }


    //==================================
    // sous Formulaire
    //==================================
    

    function setValsousformulaire(&$form, $maj, $validation, $idxformulaire, $retourformulaire, $typeformulaire, &$dnu1 = null, $dnu2 = null) {
        $this->retourformulaire = $retourformulaire;
        if($validation == 0) {
            if($this->is_in_context_of_foreign_key('arrondissement', $this->retourformulaire))
                $form->setVal('adresse_arrondissement', $idxformulaire);
            if($this->is_in_context_of_foreign_key('voie', $this->retourformulaire))
                $form->setVal('adresse_voie', $idxformulaire);
            if($this->is_in_context_of_foreign_key('dossier_coordination', $this->retourformulaire))
                $form->setVal('dossier_coordination_periodique', $idxformulaire);
            if($this->is_in_context_of_foreign_key('etablissement_categorie', $this->retourformulaire))
                $form->setVal('etablissement_categorie', $idxformulaire);
            if($this->is_in_context_of_foreign_key('etablissement_etat', $this->retourformulaire))
                $form->setVal('etablissement_etat', $idxformulaire);
            if($this->is_in_context_of_foreign_key('etablissement_nature', $this->retourformulaire))
                $form->setVal('etablissement_nature', $idxformulaire);
            if($this->is_in_context_of_foreign_key('etablissement_statut_juridique', $this->retourformulaire))
                $form->setVal('etablissement_statut_juridique', $idxformulaire);
            if($this->is_in_context_of_foreign_key('etablissement_tutelle_adm', $this->retourformulaire))
                $form->setVal('etablissement_tutelle_adm', $idxformulaire);
            if($this->is_in_context_of_foreign_key('etablissement_type', $this->retourformulaire))
                $form->setVal('etablissement_type', $idxformulaire);
            if($this->is_in_context_of_foreign_key('analyses_type', $this->retourformulaire))
                $form->setVal('si_prochaine_visite_type', $idxformulaire);
            if($this->is_in_context_of_foreign_key('visite_duree', $this->retourformulaire))
                $form->setVal('si_visite_duree', $idxformulaire);
        }// fin validation
        if ($validation == 0 and $maj == 0) {
            if($this->is_in_context_of_foreign_key('reunion_avis', $this->retourformulaire))
                $form->setVal('acc_derniere_visite_avis', $idxformulaire);
            if($this->is_in_context_of_foreign_key('acteur', $this->retourformulaire))
                $form->setVal('acc_derniere_visite_technicien', $idxformulaire);
            if($this->is_in_context_of_foreign_key('autorite_competente', $this->retourformulaire))
                $form->setVal('si_autorite_competente_plan', $idxformulaire);
            if($this->is_in_context_of_foreign_key('autorite_competente', $this->retourformulaire))
                $form->setVal('si_autorite_competente_visite', $idxformulaire);
            if($this->is_in_context_of_foreign_key('reunion_avis', $this->retourformulaire))
                $form->setVal('si_dernier_plan_avis', $idxformulaire);
            if($this->is_in_context_of_foreign_key('reunion_avis', $this->retourformulaire))
                $form->setVal('si_derniere_visite_avis', $idxformulaire);
            if($this->is_in_context_of_foreign_key('acteur', $this->retourformulaire))
                $form->setVal('si_derniere_visite_technicien', $idxformulaire);
        }// fin validation
        $this->set_form_default_values($form, $maj, $validation);
    }// fin setValsousformulaire

    //==================================
    // cle secondaire
    //==================================
    
    /**
     * Methode clesecondaire
     */
    function cleSecondaire($id, &$dnu1 = null, $val = array(), $dnu2 = null) {
        // On appelle la methode de la classe parent
        parent::cleSecondaire($id);
        // Verification de la cle secondaire : autorite_police
        $this->rechercheTable($this->f->db, "autorite_police", "etablissement", $id);
        // Verification de la cle secondaire : contact
        $this->rechercheTable($this->f->db, "contact", "etablissement", $id);
        // Verification de la cle secondaire : courrier
        $this->rechercheTable($this->f->db, "courrier", "etablissement", $id);
        // Verification de la cle secondaire : dossier_coordination
        $this->rechercheTable($this->f->db, "dossier_coordination", "etablissement", $id);
        // Verification de la cle secondaire : etablissement_parcelle
        $this->rechercheTable($this->f->db, "etablissement_parcelle", "etablissement", $id);
        // Verification de la cle secondaire : etablissement_unite
        $this->rechercheTable($this->f->db, "etablissement_unite", "etablissement", $id);
        // Verification de la cle secondaire : lien_contrainte_etablissement
        $this->rechercheTable($this->f->db, "lien_contrainte_etablissement", "etablissement", $id);
        // Verification de la cle secondaire : lien_etablissement_e_type
        $this->rechercheTable($this->f->db, "lien_etablissement_e_type", "etablissement", $id);
        // Verification de la cle secondaire : piece
        $this->rechercheTable($this->f->db, "piece", "etablissement", $id);
    }


}
