<?php
//$Id$ 
//gen openMairie le 21/07/2021 17:03

require_once "../obj/om_dbform.class.php";

class reunion_type_gen extends om_dbform {

    protected $_absolute_class_name = "reunion_type";

    var $table = "reunion_type";
    var $clePrimaire = "reunion_type";
    var $typeCle = "N";
    var $required_field = array(
        "code",
        "libelle",
        "reunion_type",
        "service"
    );
    
    var $foreign_keys_extended = array(
        "modele_edition" => array("modele_edition", ),
        "reunion_instance" => array("reunion_instance", ),
        "service" => array("service", ),
    );
    
    /**
     *
     * @return string
     */
    function get_default_libelle() {
        return $this->getVal($this->clePrimaire)."&nbsp;".$this->getVal("libelle");
    }

    /**
     *
     * @return array
     */
    function get_var_sql_forminc__champs() {
        return array(
            "reunion_type",
            "code",
            "libelle",
            "lieu_adresse_ligne1",
            "lieu_adresse_ligne2",
            "lieu_salle",
            "heure",
            "listes_de_diffusion",
            "modele_courriel_convoquer",
            "modele_courriel_cloturer",
            "commission",
            "president",
            "modele_ordre_du_jour",
            "modele_compte_rendu_global",
            "modele_compte_rendu_specifique",
            "modele_feuille_presence",
            "service",
            "om_validite_debut",
            "om_validite_fin",
            "participants",
            "numerotation_mode",
        );
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_modele_compte_rendu_global() {
        return "SELECT modele_edition.modele_edition, modele_edition.libelle FROM ".DB_PREFIXE."modele_edition WHERE ((modele_edition.om_validite_debut IS NULL AND (modele_edition.om_validite_fin IS NULL OR modele_edition.om_validite_fin > CURRENT_DATE)) OR (modele_edition.om_validite_debut <= CURRENT_DATE AND (modele_edition.om_validite_fin IS NULL OR modele_edition.om_validite_fin > CURRENT_DATE))) ORDER BY modele_edition.libelle ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_modele_compte_rendu_global_by_id() {
        return "SELECT modele_edition.modele_edition, modele_edition.libelle FROM ".DB_PREFIXE."modele_edition WHERE modele_edition = <idx>";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_modele_compte_rendu_specifique() {
        return "SELECT modele_edition.modele_edition, modele_edition.libelle FROM ".DB_PREFIXE."modele_edition WHERE ((modele_edition.om_validite_debut IS NULL AND (modele_edition.om_validite_fin IS NULL OR modele_edition.om_validite_fin > CURRENT_DATE)) OR (modele_edition.om_validite_debut <= CURRENT_DATE AND (modele_edition.om_validite_fin IS NULL OR modele_edition.om_validite_fin > CURRENT_DATE))) ORDER BY modele_edition.libelle ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_modele_compte_rendu_specifique_by_id() {
        return "SELECT modele_edition.modele_edition, modele_edition.libelle FROM ".DB_PREFIXE."modele_edition WHERE modele_edition = <idx>";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_modele_feuille_presence() {
        return "SELECT modele_edition.modele_edition, modele_edition.libelle FROM ".DB_PREFIXE."modele_edition WHERE ((modele_edition.om_validite_debut IS NULL AND (modele_edition.om_validite_fin IS NULL OR modele_edition.om_validite_fin > CURRENT_DATE)) OR (modele_edition.om_validite_debut <= CURRENT_DATE AND (modele_edition.om_validite_fin IS NULL OR modele_edition.om_validite_fin > CURRENT_DATE))) ORDER BY modele_edition.libelle ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_modele_feuille_presence_by_id() {
        return "SELECT modele_edition.modele_edition, modele_edition.libelle FROM ".DB_PREFIXE."modele_edition WHERE modele_edition = <idx>";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_modele_ordre_du_jour() {
        return "SELECT modele_edition.modele_edition, modele_edition.libelle FROM ".DB_PREFIXE."modele_edition WHERE ((modele_edition.om_validite_debut IS NULL AND (modele_edition.om_validite_fin IS NULL OR modele_edition.om_validite_fin > CURRENT_DATE)) OR (modele_edition.om_validite_debut <= CURRENT_DATE AND (modele_edition.om_validite_fin IS NULL OR modele_edition.om_validite_fin > CURRENT_DATE))) ORDER BY modele_edition.libelle ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_modele_ordre_du_jour_by_id() {
        return "SELECT modele_edition.modele_edition, modele_edition.libelle FROM ".DB_PREFIXE."modele_edition WHERE modele_edition = <idx>";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_president() {
        return "SELECT reunion_instance.reunion_instance, reunion_instance.libelle FROM ".DB_PREFIXE."reunion_instance WHERE ((reunion_instance.om_validite_debut IS NULL AND (reunion_instance.om_validite_fin IS NULL OR reunion_instance.om_validite_fin > CURRENT_DATE)) OR (reunion_instance.om_validite_debut <= CURRENT_DATE AND (reunion_instance.om_validite_fin IS NULL OR reunion_instance.om_validite_fin > CURRENT_DATE))) ORDER BY reunion_instance.libelle ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_president_by_id() {
        return "SELECT reunion_instance.reunion_instance, reunion_instance.libelle FROM ".DB_PREFIXE."reunion_instance WHERE reunion_instance = <idx>";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_service() {
        return "SELECT service.service, service.libelle FROM ".DB_PREFIXE."service WHERE ((service.om_validite_debut IS NULL AND (service.om_validite_fin IS NULL OR service.om_validite_fin > CURRENT_DATE)) OR (service.om_validite_debut <= CURRENT_DATE AND (service.om_validite_fin IS NULL OR service.om_validite_fin > CURRENT_DATE))) ORDER BY service.libelle ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_service_by_id() {
        return "SELECT service.service, service.libelle FROM ".DB_PREFIXE."service WHERE service = <idx>";
    }




    function setvalF($val = array()) {
        //affectation valeur formulaire
        if (!is_numeric($val['reunion_type'])) {
            $this->valF['reunion_type'] = ""; // -> requis
        } else {
            $this->valF['reunion_type'] = $val['reunion_type'];
        }
        $this->valF['code'] = $val['code'];
        $this->valF['libelle'] = $val['libelle'];
        if ($val['lieu_adresse_ligne1'] == "") {
            $this->valF['lieu_adresse_ligne1'] = NULL;
        } else {
            $this->valF['lieu_adresse_ligne1'] = $val['lieu_adresse_ligne1'];
        }
        if ($val['lieu_adresse_ligne2'] == "") {
            $this->valF['lieu_adresse_ligne2'] = NULL;
        } else {
            $this->valF['lieu_adresse_ligne2'] = $val['lieu_adresse_ligne2'];
        }
        if ($val['lieu_salle'] == "") {
            $this->valF['lieu_salle'] = NULL;
        } else {
            $this->valF['lieu_salle'] = $val['lieu_salle'];
        }
        if ($val['heure'] == "") {
            $this->valF['heure'] = NULL;
        } else {
            $this->valF['heure'] = $val['heure'];
        }
            $this->valF['listes_de_diffusion'] = $val['listes_de_diffusion'];
            $this->valF['modele_courriel_convoquer'] = $val['modele_courriel_convoquer'];
            $this->valF['modele_courriel_cloturer'] = $val['modele_courriel_cloturer'];
        if ($val['commission'] == 1 || $val['commission'] == "t" || $val['commission'] == "Oui") {
            $this->valF['commission'] = true;
        } else {
            $this->valF['commission'] = false;
        }
        if (!is_numeric($val['president'])) {
            $this->valF['president'] = NULL;
        } else {
            $this->valF['president'] = $val['president'];
        }
        if (!is_numeric($val['modele_ordre_du_jour'])) {
            $this->valF['modele_ordre_du_jour'] = NULL;
        } else {
            $this->valF['modele_ordre_du_jour'] = $val['modele_ordre_du_jour'];
        }
        if (!is_numeric($val['modele_compte_rendu_global'])) {
            $this->valF['modele_compte_rendu_global'] = NULL;
        } else {
            $this->valF['modele_compte_rendu_global'] = $val['modele_compte_rendu_global'];
        }
        if (!is_numeric($val['modele_compte_rendu_specifique'])) {
            $this->valF['modele_compte_rendu_specifique'] = NULL;
        } else {
            $this->valF['modele_compte_rendu_specifique'] = $val['modele_compte_rendu_specifique'];
        }
        if (!is_numeric($val['modele_feuille_presence'])) {
            $this->valF['modele_feuille_presence'] = NULL;
        } else {
            $this->valF['modele_feuille_presence'] = $val['modele_feuille_presence'];
        }
        if (!is_numeric($val['service'])) {
            $this->valF['service'] = ""; // -> requis
        } else {
            $this->valF['service'] = $val['service'];
        }
        if ($val['om_validite_debut'] != "") {
            $this->valF['om_validite_debut'] = $this->dateDB($val['om_validite_debut']);
        } else {
            $this->valF['om_validite_debut'] = NULL;
        }
        if ($val['om_validite_fin'] != "") {
            $this->valF['om_validite_fin'] = $this->dateDB($val['om_validite_fin']);
        } else {
            $this->valF['om_validite_fin'] = NULL;
        }
            $this->valF['participants'] = $val['participants'];
        if ($val['numerotation_mode'] == "") {
            $this->valF['numerotation_mode'] = NULL;
        } else {
            $this->valF['numerotation_mode'] = $val['numerotation_mode'];
        }
    }

    //=================================================
    //cle primaire automatique [automatic primary key]
    //==================================================

    function setId(&$dnu1 = null) {
    //numero automatique
        $this->valF[$this->clePrimaire] = $this->f->db->nextId(DB_PREFIXE.$this->table);
    }

    function setValFAjout($val = array()) {
    //numero automatique -> pas de controle ajout cle primaire
    }

    function verifierAjout($val = array(), &$dnu1 = null) {
    //numero automatique -> pas de verfication de cle primaire
    }
    /**
     * Methode verifier
     */
    function verifier($val = array(), &$dnu1 = null, $dnu2 = null) {
        // On appelle la methode de la classe parent
        parent::verifier($val, $this->f->db, null);

        // gestion des dates de validites
        $date_debut = $this->valF['om_validite_debut'];
        $date_fin = $this->valF['om_validite_fin'];

        if ($date_debut != '' and $date_fin != '') {
        
            $date_debut = explode('-', $this->valF['om_validite_debut']);
            $date_fin = explode('-', $this->valF['om_validite_fin']);

            $time_debut = mktime(0, 0, 0, $date_debut[1], $date_debut[2],
                                 $date_debut[0]);
            $time_fin = mktime(0, 0, 0, $date_fin[1], $date_fin[2],
                                 $date_fin[0]);

            if ($time_debut > $time_fin or $time_debut == $time_fin) {
                $this->correct = false;
                $this->addToMessage(__('La date de fin de validite doit etre future a la de debut de validite.'));
            }
        }
    }


    //==========================
    // Formulaire  [form]
    //==========================
    /**
     *
     */
    function setType(&$form, $maj) {
        // Récupération du mode de l'action
        $crud = $this->get_action_crud($maj);

        // MODE AJOUTER
        if ($maj == 0 || $crud == 'create') {
            $form->setType("reunion_type", "hidden");
            $form->setType("code", "text");
            $form->setType("libelle", "text");
            $form->setType("lieu_adresse_ligne1", "text");
            $form->setType("lieu_adresse_ligne2", "text");
            $form->setType("lieu_salle", "text");
            $form->setType("heure", "text");
            $form->setType("listes_de_diffusion", "textarea");
            $form->setType("modele_courriel_convoquer", "textarea");
            $form->setType("modele_courriel_cloturer", "textarea");
            $form->setType("commission", "checkbox");
            if ($this->is_in_context_of_foreign_key("reunion_instance", $this->retourformulaire)) {
                $form->setType("president", "selecthiddenstatic");
            } else {
                $form->setType("president", "select");
            }
            if ($this->is_in_context_of_foreign_key("modele_edition", $this->retourformulaire)) {
                $form->setType("modele_ordre_du_jour", "selecthiddenstatic");
            } else {
                $form->setType("modele_ordre_du_jour", "select");
            }
            if ($this->is_in_context_of_foreign_key("modele_edition", $this->retourformulaire)) {
                $form->setType("modele_compte_rendu_global", "selecthiddenstatic");
            } else {
                $form->setType("modele_compte_rendu_global", "select");
            }
            if ($this->is_in_context_of_foreign_key("modele_edition", $this->retourformulaire)) {
                $form->setType("modele_compte_rendu_specifique", "selecthiddenstatic");
            } else {
                $form->setType("modele_compte_rendu_specifique", "select");
            }
            if ($this->is_in_context_of_foreign_key("modele_edition", $this->retourformulaire)) {
                $form->setType("modele_feuille_presence", "selecthiddenstatic");
            } else {
                $form->setType("modele_feuille_presence", "select");
            }
            if ($this->is_in_context_of_foreign_key("service", $this->retourformulaire)) {
                $form->setType("service", "selecthiddenstatic");
            } else {
                $form->setType("service", "select");
            }
            if ($this->f->isAccredited(array($this->table."_modifier_validite", $this->table, ))) {
                $form->setType("om_validite_debut", "date");
            } else {
                $form->setType("om_validite_debut", "hiddenstaticdate");
            }
            if ($this->f->isAccredited(array($this->table."_modifier_validite", $this->table, ))) {
                $form->setType("om_validite_fin", "date");
            } else {
                $form->setType("om_validite_fin", "hiddenstaticdate");
            }
            $form->setType("participants", "textarea");
            $form->setType("numerotation_mode", "text");
        }

        // MDOE MODIFIER
        if ($maj == 1 || $crud == 'update') {
            $form->setType("reunion_type", "hiddenstatic");
            $form->setType("code", "text");
            $form->setType("libelle", "text");
            $form->setType("lieu_adresse_ligne1", "text");
            $form->setType("lieu_adresse_ligne2", "text");
            $form->setType("lieu_salle", "text");
            $form->setType("heure", "text");
            $form->setType("listes_de_diffusion", "textarea");
            $form->setType("modele_courriel_convoquer", "textarea");
            $form->setType("modele_courriel_cloturer", "textarea");
            $form->setType("commission", "checkbox");
            if ($this->is_in_context_of_foreign_key("reunion_instance", $this->retourformulaire)) {
                $form->setType("president", "selecthiddenstatic");
            } else {
                $form->setType("president", "select");
            }
            if ($this->is_in_context_of_foreign_key("modele_edition", $this->retourformulaire)) {
                $form->setType("modele_ordre_du_jour", "selecthiddenstatic");
            } else {
                $form->setType("modele_ordre_du_jour", "select");
            }
            if ($this->is_in_context_of_foreign_key("modele_edition", $this->retourformulaire)) {
                $form->setType("modele_compte_rendu_global", "selecthiddenstatic");
            } else {
                $form->setType("modele_compte_rendu_global", "select");
            }
            if ($this->is_in_context_of_foreign_key("modele_edition", $this->retourformulaire)) {
                $form->setType("modele_compte_rendu_specifique", "selecthiddenstatic");
            } else {
                $form->setType("modele_compte_rendu_specifique", "select");
            }
            if ($this->is_in_context_of_foreign_key("modele_edition", $this->retourformulaire)) {
                $form->setType("modele_feuille_presence", "selecthiddenstatic");
            } else {
                $form->setType("modele_feuille_presence", "select");
            }
            if ($this->is_in_context_of_foreign_key("service", $this->retourformulaire)) {
                $form->setType("service", "selecthiddenstatic");
            } else {
                $form->setType("service", "select");
            }
            if ($this->f->isAccredited(array($this->table."_modifier_validite", $this->table, ))) {
                $form->setType("om_validite_debut", "date");
            } else {
                $form->setType("om_validite_debut", "hiddenstaticdate");
            }
            if ($this->f->isAccredited(array($this->table."_modifier_validite", $this->table, ))) {
                $form->setType("om_validite_fin", "date");
            } else {
                $form->setType("om_validite_fin", "hiddenstaticdate");
            }
            $form->setType("participants", "textarea");
            $form->setType("numerotation_mode", "text");
        }

        // MODE SUPPRIMER
        if ($maj == 2 || $crud == 'delete') {
            $form->setType("reunion_type", "hiddenstatic");
            $form->setType("code", "hiddenstatic");
            $form->setType("libelle", "hiddenstatic");
            $form->setType("lieu_adresse_ligne1", "hiddenstatic");
            $form->setType("lieu_adresse_ligne2", "hiddenstatic");
            $form->setType("lieu_salle", "hiddenstatic");
            $form->setType("heure", "hiddenstatic");
            $form->setType("listes_de_diffusion", "hiddenstatic");
            $form->setType("modele_courriel_convoquer", "hiddenstatic");
            $form->setType("modele_courriel_cloturer", "hiddenstatic");
            $form->setType("commission", "hiddenstatic");
            $form->setType("president", "selectstatic");
            $form->setType("modele_ordre_du_jour", "selectstatic");
            $form->setType("modele_compte_rendu_global", "selectstatic");
            $form->setType("modele_compte_rendu_specifique", "selectstatic");
            $form->setType("modele_feuille_presence", "selectstatic");
            $form->setType("service", "selectstatic");
            $form->setType("om_validite_debut", "hiddenstatic");
            $form->setType("om_validite_fin", "hiddenstatic");
            $form->setType("participants", "hiddenstatic");
            $form->setType("numerotation_mode", "hiddenstatic");
        }

        // MODE CONSULTER
        if ($maj == 3 || $crud == 'read') {
            $form->setType("reunion_type", "static");
            $form->setType("code", "static");
            $form->setType("libelle", "static");
            $form->setType("lieu_adresse_ligne1", "static");
            $form->setType("lieu_adresse_ligne2", "static");
            $form->setType("lieu_salle", "static");
            $form->setType("heure", "static");
            $form->setType("listes_de_diffusion", "textareastatic");
            $form->setType("modele_courriel_convoquer", "textareastatic");
            $form->setType("modele_courriel_cloturer", "textareastatic");
            $form->setType("commission", "checkboxstatic");
            $form->setType("president", "selectstatic");
            $form->setType("modele_ordre_du_jour", "selectstatic");
            $form->setType("modele_compte_rendu_global", "selectstatic");
            $form->setType("modele_compte_rendu_specifique", "selectstatic");
            $form->setType("modele_feuille_presence", "selectstatic");
            $form->setType("service", "selectstatic");
            $form->setType("om_validite_debut", "datestatic");
            $form->setType("om_validite_fin", "datestatic");
            $form->setType("participants", "textareastatic");
            $form->setType("numerotation_mode", "static");
        }

    }


    function setOnchange(&$form, $maj) {
    //javascript controle client
        $form->setOnchange('reunion_type','VerifNum(this)');
        $form->setOnchange('president','VerifNum(this)');
        $form->setOnchange('modele_ordre_du_jour','VerifNum(this)');
        $form->setOnchange('modele_compte_rendu_global','VerifNum(this)');
        $form->setOnchange('modele_compte_rendu_specifique','VerifNum(this)');
        $form->setOnchange('modele_feuille_presence','VerifNum(this)');
        $form->setOnchange('service','VerifNum(this)');
        $form->setOnchange('om_validite_debut','fdate(this)');
        $form->setOnchange('om_validite_fin','fdate(this)');
    }
    /**
     * Methode setTaille
     */
    function setTaille(&$form, $maj) {
        $form->setTaille("reunion_type", 11);
        $form->setTaille("code", 10);
        $form->setTaille("libelle", 30);
        $form->setTaille("lieu_adresse_ligne1", 30);
        $form->setTaille("lieu_adresse_ligne2", 30);
        $form->setTaille("lieu_salle", 30);
        $form->setTaille("heure", 10);
        $form->setTaille("listes_de_diffusion", 80);
        $form->setTaille("modele_courriel_convoquer", 80);
        $form->setTaille("modele_courriel_cloturer", 80);
        $form->setTaille("commission", 1);
        $form->setTaille("president", 11);
        $form->setTaille("modele_ordre_du_jour", 11);
        $form->setTaille("modele_compte_rendu_global", 11);
        $form->setTaille("modele_compte_rendu_specifique", 11);
        $form->setTaille("modele_feuille_presence", 11);
        $form->setTaille("service", 11);
        $form->setTaille("om_validite_debut", 12);
        $form->setTaille("om_validite_fin", 12);
        $form->setTaille("participants", 80);
        $form->setTaille("numerotation_mode", 30);
    }

    /**
     * Methode setMax
     */
    function setMax(&$form, $maj) {
        $form->setMax("reunion_type", 11);
        $form->setMax("code", 10);
        $form->setMax("libelle", 100);
        $form->setMax("lieu_adresse_ligne1", 150);
        $form->setMax("lieu_adresse_ligne2", 100);
        $form->setMax("lieu_salle", 100);
        $form->setMax("heure", 5);
        $form->setMax("listes_de_diffusion", 6);
        $form->setMax("modele_courriel_convoquer", 6);
        $form->setMax("modele_courriel_cloturer", 6);
        $form->setMax("commission", 1);
        $form->setMax("president", 11);
        $form->setMax("modele_ordre_du_jour", 11);
        $form->setMax("modele_compte_rendu_global", 11);
        $form->setMax("modele_compte_rendu_specifique", 11);
        $form->setMax("modele_feuille_presence", 11);
        $form->setMax("service", 11);
        $form->setMax("om_validite_debut", 12);
        $form->setMax("om_validite_fin", 12);
        $form->setMax("participants", 6);
        $form->setMax("numerotation_mode", 30);
    }


    function setLib(&$form, $maj) {
    //libelle des champs
        $form->setLib('reunion_type', __('reunion_type'));
        $form->setLib('code', __('code'));
        $form->setLib('libelle', __('libelle'));
        $form->setLib('lieu_adresse_ligne1', __('lieu_adresse_ligne1'));
        $form->setLib('lieu_adresse_ligne2', __('lieu_adresse_ligne2'));
        $form->setLib('lieu_salle', __('lieu_salle'));
        $form->setLib('heure', __('heure'));
        $form->setLib('listes_de_diffusion', __('listes_de_diffusion'));
        $form->setLib('modele_courriel_convoquer', __('modele_courriel_convoquer'));
        $form->setLib('modele_courriel_cloturer', __('modele_courriel_cloturer'));
        $form->setLib('commission', __('commission'));
        $form->setLib('president', __('president'));
        $form->setLib('modele_ordre_du_jour', __('modele_ordre_du_jour'));
        $form->setLib('modele_compte_rendu_global', __('modele_compte_rendu_global'));
        $form->setLib('modele_compte_rendu_specifique', __('modele_compte_rendu_specifique'));
        $form->setLib('modele_feuille_presence', __('modele_feuille_presence'));
        $form->setLib('service', __('service'));
        $form->setLib('om_validite_debut', __('om_validite_debut'));
        $form->setLib('om_validite_fin', __('om_validite_fin'));
        $form->setLib('participants', __('participants'));
        $form->setLib('numerotation_mode', __('numerotation_mode'));
    }
    /**
     *
     */
    function setSelect(&$form, $maj, &$dnu1 = null, $dnu2 = null) {

        // modele_compte_rendu_global
        $this->init_select(
            $form, 
            $this->f->db,
            $maj,
            null,
            "modele_compte_rendu_global",
            $this->get_var_sql_forminc__sql("modele_compte_rendu_global"),
            $this->get_var_sql_forminc__sql("modele_compte_rendu_global_by_id"),
            true
        );
        // modele_compte_rendu_specifique
        $this->init_select(
            $form, 
            $this->f->db,
            $maj,
            null,
            "modele_compte_rendu_specifique",
            $this->get_var_sql_forminc__sql("modele_compte_rendu_specifique"),
            $this->get_var_sql_forminc__sql("modele_compte_rendu_specifique_by_id"),
            true
        );
        // modele_feuille_presence
        $this->init_select(
            $form, 
            $this->f->db,
            $maj,
            null,
            "modele_feuille_presence",
            $this->get_var_sql_forminc__sql("modele_feuille_presence"),
            $this->get_var_sql_forminc__sql("modele_feuille_presence_by_id"),
            true
        );
        // modele_ordre_du_jour
        $this->init_select(
            $form, 
            $this->f->db,
            $maj,
            null,
            "modele_ordre_du_jour",
            $this->get_var_sql_forminc__sql("modele_ordre_du_jour"),
            $this->get_var_sql_forminc__sql("modele_ordre_du_jour_by_id"),
            true
        );
        // president
        $this->init_select(
            $form, 
            $this->f->db,
            $maj,
            null,
            "president",
            $this->get_var_sql_forminc__sql("president"),
            $this->get_var_sql_forminc__sql("president_by_id"),
            true
        );
        // service
        $this->init_select(
            $form, 
            $this->f->db,
            $maj,
            null,
            "service",
            $this->get_var_sql_forminc__sql("service"),
            $this->get_var_sql_forminc__sql("service_by_id"),
            true
        );
    }


    //==================================
    // sous Formulaire
    //==================================
    

    function setValsousformulaire(&$form, $maj, $validation, $idxformulaire, $retourformulaire, $typeformulaire, &$dnu1 = null, $dnu2 = null) {
        $this->retourformulaire = $retourformulaire;
        if($validation == 0) {
            if($this->is_in_context_of_foreign_key('reunion_instance', $this->retourformulaire))
                $form->setVal('president', $idxformulaire);
            if($this->is_in_context_of_foreign_key('service', $this->retourformulaire))
                $form->setVal('service', $idxformulaire);
        }// fin validation
        if ($validation == 0 and $maj == 0) {
            if($this->is_in_context_of_foreign_key('modele_edition', $this->retourformulaire))
                $form->setVal('modele_compte_rendu_global', $idxformulaire);
            if($this->is_in_context_of_foreign_key('modele_edition', $this->retourformulaire))
                $form->setVal('modele_compte_rendu_specifique', $idxformulaire);
            if($this->is_in_context_of_foreign_key('modele_edition', $this->retourformulaire))
                $form->setVal('modele_feuille_presence', $idxformulaire);
            if($this->is_in_context_of_foreign_key('modele_edition', $this->retourformulaire))
                $form->setVal('modele_ordre_du_jour', $idxformulaire);
        }// fin validation
        $this->set_form_default_values($form, $maj, $validation);
    }// fin setValsousformulaire

    //==================================
    // cle secondaire
    //==================================
    
    /**
     * Methode clesecondaire
     */
    function cleSecondaire($id, &$dnu1 = null, $val = array(), $dnu2 = null) {
        // On appelle la methode de la classe parent
        parent::cleSecondaire($id);
        // Verification de la cle secondaire : dossier_instruction_reunion
        $this->rechercheTable($this->f->db, "dossier_instruction_reunion", "reunion_type", $id);
        // Verification de la cle secondaire : reunion
        $this->rechercheTable($this->f->db, "reunion", "reunion_type", $id);
        // Verification de la cle secondaire : reunion_type_reunion_avis
        $this->rechercheTable($this->f->db, "reunion_type_reunion_avis", "reunion_type", $id);
        // Verification de la cle secondaire : reunion_type_reunion_categorie
        $this->rechercheTable($this->f->db, "reunion_type_reunion_categorie", "reunion_type", $id);
        // Verification de la cle secondaire : reunion_type_reunion_instance
        $this->rechercheTable($this->f->db, "reunion_type_reunion_instance", "reunion_type", $id);
    }


}
