<?php
//$Id$ 
//gen openMairie le 03/05/2018 09:12

require_once "../obj/om_dbform.class.php";

class reunion_instance_membre_gen extends om_dbform {

    protected $_absolute_class_name = "reunion_instance_membre";

    var $table = "reunion_instance_membre";
    var $clePrimaire = "reunion_instance_membre";
    var $typeCle = "N";
    var $required_field = array(
        "reunion_instance",
        "reunion_instance_membre"
    );
    
    var $foreign_keys_extended = array(
        "reunion_instance" => array("reunion_instance", ),
    );
    
    /**
     *
     * @return string
     */
    function get_default_libelle() {
        return $this->getVal($this->clePrimaire)."&nbsp;".$this->getVal("membre");
    }

    /**
     *
     * @return array
     */
    function get_var_sql_forminc__champs() {
        return array(
            "reunion_instance_membre",
            "membre",
            "description",
            "reunion_instance",
            "om_validite_debut",
            "om_validite_fin",
        );
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_reunion_instance() {
        return "SELECT reunion_instance.reunion_instance, reunion_instance.libelle FROM ".DB_PREFIXE."reunion_instance WHERE ((reunion_instance.om_validite_debut IS NULL AND (reunion_instance.om_validite_fin IS NULL OR reunion_instance.om_validite_fin > CURRENT_DATE)) OR (reunion_instance.om_validite_debut <= CURRENT_DATE AND (reunion_instance.om_validite_fin IS NULL OR reunion_instance.om_validite_fin > CURRENT_DATE))) ORDER BY reunion_instance.libelle ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_reunion_instance_by_id() {
        return "SELECT reunion_instance.reunion_instance, reunion_instance.libelle FROM ".DB_PREFIXE."reunion_instance WHERE reunion_instance = <idx>";
    }




    function setvalF($val = array()) {
        //affectation valeur formulaire
        if (!is_numeric($val['reunion_instance_membre'])) {
            $this->valF['reunion_instance_membre'] = ""; // -> requis
        } else {
            $this->valF['reunion_instance_membre'] = $val['reunion_instance_membre'];
        }
        if ($val['membre'] == "") {
            $this->valF['membre'] = NULL;
        } else {
            $this->valF['membre'] = $val['membre'];
        }
            $this->valF['description'] = $val['description'];
        if (!is_numeric($val['reunion_instance'])) {
            $this->valF['reunion_instance'] = ""; // -> requis
        } else {
            $this->valF['reunion_instance'] = $val['reunion_instance'];
        }
        if ($val['om_validite_debut'] != "") {
            $this->valF['om_validite_debut'] = $this->dateDB($val['om_validite_debut']);
        } else {
            $this->valF['om_validite_debut'] = NULL;
        }
        if ($val['om_validite_fin'] != "") {
            $this->valF['om_validite_fin'] = $this->dateDB($val['om_validite_fin']);
        } else {
            $this->valF['om_validite_fin'] = NULL;
        }
    }

    //=================================================
    //cle primaire automatique [automatic primary key]
    //==================================================

    function setId(&$dnu1 = null) {
    //numero automatique
        $this->valF[$this->clePrimaire] = $this->f->db->nextId(DB_PREFIXE.$this->table);
    }

    function setValFAjout($val = array()) {
    //numero automatique -> pas de controle ajout cle primaire
    }

    function verifierAjout($val = array(), &$dnu1 = null) {
    //numero automatique -> pas de verfication de cle primaire
    }
    /**
     * Methode verifier
     */
    function verifier($val = array(), &$dnu1 = null, $dnu2 = null) {
        // On appelle la methode de la classe parent
        parent::verifier($val, $this->f->db, null);

        // gestion des dates de validites
        $date_debut = $this->valF['om_validite_debut'];
        $date_fin = $this->valF['om_validite_fin'];

        if ($date_debut != '' and $date_fin != '') {
        
            $date_debut = explode('-', $this->valF['om_validite_debut']);
            $date_fin = explode('-', $this->valF['om_validite_fin']);

            $time_debut = mktime(0, 0, 0, $date_debut[1], $date_debut[2],
                                 $date_debut[0]);
            $time_fin = mktime(0, 0, 0, $date_fin[1], $date_fin[2],
                                 $date_fin[0]);

            if ($time_debut > $time_fin or $time_debut == $time_fin) {
                $this->correct = false;
                $this->addToMessage(__('La date de fin de validite doit etre future a la de debut de validite.'));
            }
        }
    }


    //==========================
    // Formulaire  [form]
    //==========================
    /**
     *
     */
    function setType(&$form, $maj) {
        // Récupération du mode de l'action
        $crud = $this->get_action_crud($maj);

        // MODE AJOUTER
        if ($maj == 0 || $crud == 'create') {
            $form->setType("reunion_instance_membre", "hidden");
            $form->setType("membre", "text");
            $form->setType("description", "textarea");
            if ($this->is_in_context_of_foreign_key("reunion_instance", $this->retourformulaire)) {
                $form->setType("reunion_instance", "selecthiddenstatic");
            } else {
                $form->setType("reunion_instance", "select");
            }
            if ($this->f->isAccredited(array($this->table."_modifier_validite", $this->table, ))) {
                $form->setType("om_validite_debut", "date");
            } else {
                $form->setType("om_validite_debut", "hiddenstaticdate");
            }
            if ($this->f->isAccredited(array($this->table."_modifier_validite", $this->table, ))) {
                $form->setType("om_validite_fin", "date");
            } else {
                $form->setType("om_validite_fin", "hiddenstaticdate");
            }
        }

        // MDOE MODIFIER
        if ($maj == 1 || $crud == 'update') {
            $form->setType("reunion_instance_membre", "hiddenstatic");
            $form->setType("membre", "text");
            $form->setType("description", "textarea");
            if ($this->is_in_context_of_foreign_key("reunion_instance", $this->retourformulaire)) {
                $form->setType("reunion_instance", "selecthiddenstatic");
            } else {
                $form->setType("reunion_instance", "select");
            }
            if ($this->f->isAccredited(array($this->table."_modifier_validite", $this->table, ))) {
                $form->setType("om_validite_debut", "date");
            } else {
                $form->setType("om_validite_debut", "hiddenstaticdate");
            }
            if ($this->f->isAccredited(array($this->table."_modifier_validite", $this->table, ))) {
                $form->setType("om_validite_fin", "date");
            } else {
                $form->setType("om_validite_fin", "hiddenstaticdate");
            }
        }

        // MODE SUPPRIMER
        if ($maj == 2 || $crud == 'delete') {
            $form->setType("reunion_instance_membre", "hiddenstatic");
            $form->setType("membre", "hiddenstatic");
            $form->setType("description", "hiddenstatic");
            $form->setType("reunion_instance", "selectstatic");
            $form->setType("om_validite_debut", "hiddenstatic");
            $form->setType("om_validite_fin", "hiddenstatic");
        }

        // MODE CONSULTER
        if ($maj == 3 || $crud == 'read') {
            $form->setType("reunion_instance_membre", "static");
            $form->setType("membre", "static");
            $form->setType("description", "textareastatic");
            $form->setType("reunion_instance", "selectstatic");
            $form->setType("om_validite_debut", "datestatic");
            $form->setType("om_validite_fin", "datestatic");
        }

    }


    function setOnchange(&$form, $maj) {
    //javascript controle client
        $form->setOnchange('reunion_instance_membre','VerifNum(this)');
        $form->setOnchange('reunion_instance','VerifNum(this)');
        $form->setOnchange('om_validite_debut','fdate(this)');
        $form->setOnchange('om_validite_fin','fdate(this)');
    }
    /**
     * Methode setTaille
     */
    function setTaille(&$form, $maj) {
        $form->setTaille("reunion_instance_membre", 11);
        $form->setTaille("membre", 30);
        $form->setTaille("description", 80);
        $form->setTaille("reunion_instance", 11);
        $form->setTaille("om_validite_debut", 12);
        $form->setTaille("om_validite_fin", 12);
    }

    /**
     * Methode setMax
     */
    function setMax(&$form, $maj) {
        $form->setMax("reunion_instance_membre", 11);
        $form->setMax("membre", 100);
        $form->setMax("description", 6);
        $form->setMax("reunion_instance", 11);
        $form->setMax("om_validite_debut", 12);
        $form->setMax("om_validite_fin", 12);
    }


    function setLib(&$form, $maj) {
    //libelle des champs
        $form->setLib('reunion_instance_membre', __('reunion_instance_membre'));
        $form->setLib('membre', __('membre'));
        $form->setLib('description', __('description'));
        $form->setLib('reunion_instance', __('reunion_instance'));
        $form->setLib('om_validite_debut', __('om_validite_debut'));
        $form->setLib('om_validite_fin', __('om_validite_fin'));
    }
    /**
     *
     */
    function setSelect(&$form, $maj, &$dnu1 = null, $dnu2 = null) {

        // reunion_instance
        $this->init_select(
            $form, 
            $this->f->db,
            $maj,
            null,
            "reunion_instance",
            $this->get_var_sql_forminc__sql("reunion_instance"),
            $this->get_var_sql_forminc__sql("reunion_instance_by_id"),
            true
        );
    }


    //==================================
    // sous Formulaire
    //==================================
    

    function setValsousformulaire(&$form, $maj, $validation, $idxformulaire, $retourformulaire, $typeformulaire, &$dnu1 = null, $dnu2 = null) {
        $this->retourformulaire = $retourformulaire;
        if($validation == 0) {
            if($this->is_in_context_of_foreign_key('reunion_instance', $this->retourformulaire))
                $form->setVal('reunion_instance', $idxformulaire);
        }// fin validation
        $this->set_form_default_values($form, $maj, $validation);
    }// fin setValsousformulaire

    //==================================
    // cle secondaire
    //==================================
    
    /**
     * Methode clesecondaire
     */
    function cleSecondaire($id, &$dnu1 = null, $val = array(), $dnu2 = null) {
        // On appelle la methode de la classe parent
        parent::cleSecondaire($id);
        // Verification de la cle secondaire : lien_reunion_r_instance_r_i_membre
        $this->rechercheTable($this->f->db, "lien_reunion_r_instance_r_i_membre", "reunion_instance_membre", $id);
    }


}
