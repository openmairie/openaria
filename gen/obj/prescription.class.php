<?php
//$Id$ 
//gen openMairie le 03/05/2018 09:12

require_once "../obj/om_dbform.class.php";

class prescription_gen extends om_dbform {

    protected $_absolute_class_name = "prescription";

    var $table = "prescription";
    var $clePrimaire = "prescription";
    var $typeCle = "N";
    var $required_field = array(
        "analyses",
        "ordre",
        "prescription",
        "prescription_reglementaire"
    );
    
    var $foreign_keys_extended = array(
        "analyses" => array("analyses", ),
        "prescription_reglementaire" => array("prescription_reglementaire", ),
    );
    
    /**
     *
     * @return string
     */
    function get_default_libelle() {
        return $this->getVal($this->clePrimaire)."&nbsp;".$this->getVal("analyses");
    }

    /**
     *
     * @return array
     */
    function get_var_sql_forminc__champs() {
        return array(
            "prescription",
            "analyses",
            "ordre",
            "prescription_reglementaire",
            "pr_description_om_html",
            "pr_defavorable",
            "ps_description_om_html",
        );
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_analyses() {
        return "SELECT analyses.analyses, analyses.service FROM ".DB_PREFIXE."analyses ORDER BY analyses.service ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_analyses_by_id() {
        return "SELECT analyses.analyses, analyses.service FROM ".DB_PREFIXE."analyses WHERE analyses = <idx>";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_prescription_reglementaire() {
        return "SELECT prescription_reglementaire.prescription_reglementaire, prescription_reglementaire.libelle FROM ".DB_PREFIXE."prescription_reglementaire WHERE ((prescription_reglementaire.om_validite_debut IS NULL AND (prescription_reglementaire.om_validite_fin IS NULL OR prescription_reglementaire.om_validite_fin > CURRENT_DATE)) OR (prescription_reglementaire.om_validite_debut <= CURRENT_DATE AND (prescription_reglementaire.om_validite_fin IS NULL OR prescription_reglementaire.om_validite_fin > CURRENT_DATE))) ORDER BY prescription_reglementaire.libelle ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_prescription_reglementaire_by_id() {
        return "SELECT prescription_reglementaire.prescription_reglementaire, prescription_reglementaire.libelle FROM ".DB_PREFIXE."prescription_reglementaire WHERE prescription_reglementaire = <idx>";
    }




    function setvalF($val = array()) {
        //affectation valeur formulaire
        if (!is_numeric($val['prescription'])) {
            $this->valF['prescription'] = ""; // -> requis
        } else {
            $this->valF['prescription'] = $val['prescription'];
        }
        if (!is_numeric($val['analyses'])) {
            $this->valF['analyses'] = ""; // -> requis
        } else {
            $this->valF['analyses'] = $val['analyses'];
        }
        if (!is_numeric($val['ordre'])) {
            $this->valF['ordre'] = ""; // -> requis
        } else {
            $this->valF['ordre'] = $val['ordre'];
        }
        if (!is_numeric($val['prescription_reglementaire'])) {
            $this->valF['prescription_reglementaire'] = ""; // -> requis
        } else {
            $this->valF['prescription_reglementaire'] = $val['prescription_reglementaire'];
        }
            $this->valF['pr_description_om_html'] = $val['pr_description_om_html'];
        if ($val['pr_defavorable'] == 1 || $val['pr_defavorable'] == "t" || $val['pr_defavorable'] == "Oui") {
            $this->valF['pr_defavorable'] = true;
        } else {
            $this->valF['pr_defavorable'] = false;
        }
            $this->valF['ps_description_om_html'] = $val['ps_description_om_html'];
    }

    //=================================================
    //cle primaire automatique [automatic primary key]
    //==================================================

    function setId(&$dnu1 = null) {
    //numero automatique
        $this->valF[$this->clePrimaire] = $this->f->db->nextId(DB_PREFIXE.$this->table);
    }

    function setValFAjout($val = array()) {
    //numero automatique -> pas de controle ajout cle primaire
    }

    function verifierAjout($val = array(), &$dnu1 = null) {
    //numero automatique -> pas de verfication de cle primaire
    }

    //==========================
    // Formulaire  [form]
    //==========================
    /**
     *
     */
    function setType(&$form, $maj) {
        // Récupération du mode de l'action
        $crud = $this->get_action_crud($maj);

        // MODE AJOUTER
        if ($maj == 0 || $crud == 'create') {
            $form->setType("prescription", "hidden");
            if ($this->is_in_context_of_foreign_key("analyses", $this->retourformulaire)) {
                $form->setType("analyses", "selecthiddenstatic");
            } else {
                $form->setType("analyses", "select");
            }
            $form->setType("ordre", "text");
            if ($this->is_in_context_of_foreign_key("prescription_reglementaire", $this->retourformulaire)) {
                $form->setType("prescription_reglementaire", "selecthiddenstatic");
            } else {
                $form->setType("prescription_reglementaire", "select");
            }
            $form->setType("pr_description_om_html", "html");
            $form->setType("pr_defavorable", "checkbox");
            $form->setType("ps_description_om_html", "html");
        }

        // MDOE MODIFIER
        if ($maj == 1 || $crud == 'update') {
            $form->setType("prescription", "hiddenstatic");
            if ($this->is_in_context_of_foreign_key("analyses", $this->retourformulaire)) {
                $form->setType("analyses", "selecthiddenstatic");
            } else {
                $form->setType("analyses", "select");
            }
            $form->setType("ordre", "text");
            if ($this->is_in_context_of_foreign_key("prescription_reglementaire", $this->retourformulaire)) {
                $form->setType("prescription_reglementaire", "selecthiddenstatic");
            } else {
                $form->setType("prescription_reglementaire", "select");
            }
            $form->setType("pr_description_om_html", "html");
            $form->setType("pr_defavorable", "checkbox");
            $form->setType("ps_description_om_html", "html");
        }

        // MODE SUPPRIMER
        if ($maj == 2 || $crud == 'delete') {
            $form->setType("prescription", "hiddenstatic");
            $form->setType("analyses", "selectstatic");
            $form->setType("ordre", "hiddenstatic");
            $form->setType("prescription_reglementaire", "selectstatic");
            $form->setType("pr_description_om_html", "hiddenstatic");
            $form->setType("pr_defavorable", "hiddenstatic");
            $form->setType("ps_description_om_html", "hiddenstatic");
        }

        // MODE CONSULTER
        if ($maj == 3 || $crud == 'read') {
            $form->setType("prescription", "static");
            $form->setType("analyses", "selectstatic");
            $form->setType("ordre", "static");
            $form->setType("prescription_reglementaire", "selectstatic");
            $form->setType("pr_description_om_html", "htmlstatic");
            $form->setType("pr_defavorable", "checkboxstatic");
            $form->setType("ps_description_om_html", "htmlstatic");
        }

    }


    function setOnchange(&$form, $maj) {
    //javascript controle client
        $form->setOnchange('prescription','VerifNum(this)');
        $form->setOnchange('analyses','VerifNum(this)');
        $form->setOnchange('ordre','VerifNum(this)');
        $form->setOnchange('prescription_reglementaire','VerifNum(this)');
    }
    /**
     * Methode setTaille
     */
    function setTaille(&$form, $maj) {
        $form->setTaille("prescription", 11);
        $form->setTaille("analyses", 11);
        $form->setTaille("ordre", 11);
        $form->setTaille("prescription_reglementaire", 11);
        $form->setTaille("pr_description_om_html", 80);
        $form->setTaille("pr_defavorable", 1);
        $form->setTaille("ps_description_om_html", 80);
    }

    /**
     * Methode setMax
     */
    function setMax(&$form, $maj) {
        $form->setMax("prescription", 11);
        $form->setMax("analyses", 11);
        $form->setMax("ordre", 11);
        $form->setMax("prescription_reglementaire", 11);
        $form->setMax("pr_description_om_html", 6);
        $form->setMax("pr_defavorable", 1);
        $form->setMax("ps_description_om_html", 6);
    }


    function setLib(&$form, $maj) {
    //libelle des champs
        $form->setLib('prescription', __('prescription'));
        $form->setLib('analyses', __('analyses'));
        $form->setLib('ordre', __('ordre'));
        $form->setLib('prescription_reglementaire', __('prescription_reglementaire'));
        $form->setLib('pr_description_om_html', __('pr_description_om_html'));
        $form->setLib('pr_defavorable', __('pr_defavorable'));
        $form->setLib('ps_description_om_html', __('ps_description_om_html'));
    }
    /**
     *
     */
    function setSelect(&$form, $maj, &$dnu1 = null, $dnu2 = null) {

        // analyses
        $this->init_select(
            $form, 
            $this->f->db,
            $maj,
            null,
            "analyses",
            $this->get_var_sql_forminc__sql("analyses"),
            $this->get_var_sql_forminc__sql("analyses_by_id"),
            false
        );
        // prescription_reglementaire
        $this->init_select(
            $form, 
            $this->f->db,
            $maj,
            null,
            "prescription_reglementaire",
            $this->get_var_sql_forminc__sql("prescription_reglementaire"),
            $this->get_var_sql_forminc__sql("prescription_reglementaire_by_id"),
            true
        );
    }


    //==================================
    // sous Formulaire
    //==================================
    

    function setValsousformulaire(&$form, $maj, $validation, $idxformulaire, $retourformulaire, $typeformulaire, &$dnu1 = null, $dnu2 = null) {
        $this->retourformulaire = $retourformulaire;
        if($validation == 0) {
            if($this->is_in_context_of_foreign_key('analyses', $this->retourformulaire))
                $form->setVal('analyses', $idxformulaire);
            if($this->is_in_context_of_foreign_key('prescription_reglementaire', $this->retourformulaire))
                $form->setVal('prescription_reglementaire', $idxformulaire);
        }// fin validation
        $this->set_form_default_values($form, $maj, $validation);
    }// fin setValsousformulaire

    //==================================
    // cle secondaire
    //==================================
    

}
