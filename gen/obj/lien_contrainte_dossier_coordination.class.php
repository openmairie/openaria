<?php
//$Id$ 
//gen openMairie le 20/05/2020 18:03

require_once "../obj/om_dbform.class.php";

class lien_contrainte_dossier_coordination_gen extends om_dbform {

    protected $_absolute_class_name = "lien_contrainte_dossier_coordination";

    var $table = "lien_contrainte_dossier_coordination";
    var $clePrimaire = "lien_contrainte_dossier_coordination";
    var $typeCle = "N";
    var $required_field = array(
        "contrainte",
        "dossier_coordination",
        "lien_contrainte_dossier_coordination"
    );
    
    var $foreign_keys_extended = array(
        "contrainte" => array("contrainte", ),
        "dossier_coordination" => array("dossier_coordination", "dossier_coordination_nouveau", "dossier_coordination_a_qualifier", "dossier_coordination_a_cloturer", ),
    );
    
    /**
     *
     * @return string
     */
    function get_default_libelle() {
        return $this->getVal($this->clePrimaire)."&nbsp;".$this->getVal("dossier_coordination");
    }

    /**
     *
     * @return array
     */
    function get_var_sql_forminc__champs() {
        return array(
            "lien_contrainte_dossier_coordination",
            "dossier_coordination",
            "contrainte",
            "texte_complete",
            "recuperee",
        );
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_contrainte() {
        return "SELECT contrainte.contrainte, contrainte.libelle FROM ".DB_PREFIXE."contrainte WHERE ((contrainte.om_validite_debut IS NULL AND (contrainte.om_validite_fin IS NULL OR contrainte.om_validite_fin > CURRENT_DATE)) OR (contrainte.om_validite_debut <= CURRENT_DATE AND (contrainte.om_validite_fin IS NULL OR contrainte.om_validite_fin > CURRENT_DATE))) ORDER BY contrainte.libelle ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_contrainte_by_id() {
        return "SELECT contrainte.contrainte, contrainte.libelle FROM ".DB_PREFIXE."contrainte WHERE contrainte = <idx>";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_dossier_coordination() {
        return "SELECT dossier_coordination.dossier_coordination, dossier_coordination.libelle FROM ".DB_PREFIXE."dossier_coordination ORDER BY dossier_coordination.libelle ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_dossier_coordination_by_id() {
        return "SELECT dossier_coordination.dossier_coordination, dossier_coordination.libelle FROM ".DB_PREFIXE."dossier_coordination WHERE dossier_coordination = <idx>";
    }




    function setvalF($val = array()) {
        //affectation valeur formulaire
        if (!is_numeric($val['lien_contrainte_dossier_coordination'])) {
            $this->valF['lien_contrainte_dossier_coordination'] = ""; // -> requis
        } else {
            $this->valF['lien_contrainte_dossier_coordination'] = $val['lien_contrainte_dossier_coordination'];
        }
        if (!is_numeric($val['dossier_coordination'])) {
            $this->valF['dossier_coordination'] = ""; // -> requis
        } else {
            $this->valF['dossier_coordination'] = $val['dossier_coordination'];
        }
        if (!is_numeric($val['contrainte'])) {
            $this->valF['contrainte'] = ""; // -> requis
        } else {
            $this->valF['contrainte'] = $val['contrainte'];
        }
            $this->valF['texte_complete'] = $val['texte_complete'];
        if ($val['recuperee'] == 1 || $val['recuperee'] == "t" || $val['recuperee'] == "Oui") {
            $this->valF['recuperee'] = true;
        } else {
            $this->valF['recuperee'] = false;
        }
    }

    //=================================================
    //cle primaire automatique [automatic primary key]
    //==================================================

    function setId(&$dnu1 = null) {
    //numero automatique
        $this->valF[$this->clePrimaire] = $this->f->db->nextId(DB_PREFIXE.$this->table);
    }

    function setValFAjout($val = array()) {
    //numero automatique -> pas de controle ajout cle primaire
    }

    function verifierAjout($val = array(), &$dnu1 = null) {
    //numero automatique -> pas de verfication de cle primaire
    }

    //==========================
    // Formulaire  [form]
    //==========================
    /**
     *
     */
    function setType(&$form, $maj) {
        // Récupération du mode de l'action
        $crud = $this->get_action_crud($maj);

        // MODE AJOUTER
        if ($maj == 0 || $crud == 'create') {
            $form->setType("lien_contrainte_dossier_coordination", "hidden");
            if ($this->is_in_context_of_foreign_key("dossier_coordination", $this->retourformulaire)) {
                $form->setType("dossier_coordination", "selecthiddenstatic");
            } else {
                $form->setType("dossier_coordination", "select");
            }
            if ($this->is_in_context_of_foreign_key("contrainte", $this->retourformulaire)) {
                $form->setType("contrainte", "selecthiddenstatic");
            } else {
                $form->setType("contrainte", "select");
            }
            $form->setType("texte_complete", "textarea");
            $form->setType("recuperee", "checkbox");
        }

        // MDOE MODIFIER
        if ($maj == 1 || $crud == 'update') {
            $form->setType("lien_contrainte_dossier_coordination", "hiddenstatic");
            if ($this->is_in_context_of_foreign_key("dossier_coordination", $this->retourformulaire)) {
                $form->setType("dossier_coordination", "selecthiddenstatic");
            } else {
                $form->setType("dossier_coordination", "select");
            }
            if ($this->is_in_context_of_foreign_key("contrainte", $this->retourformulaire)) {
                $form->setType("contrainte", "selecthiddenstatic");
            } else {
                $form->setType("contrainte", "select");
            }
            $form->setType("texte_complete", "textarea");
            $form->setType("recuperee", "checkbox");
        }

        // MODE SUPPRIMER
        if ($maj == 2 || $crud == 'delete') {
            $form->setType("lien_contrainte_dossier_coordination", "hiddenstatic");
            $form->setType("dossier_coordination", "selectstatic");
            $form->setType("contrainte", "selectstatic");
            $form->setType("texte_complete", "hiddenstatic");
            $form->setType("recuperee", "hiddenstatic");
        }

        // MODE CONSULTER
        if ($maj == 3 || $crud == 'read') {
            $form->setType("lien_contrainte_dossier_coordination", "static");
            $form->setType("dossier_coordination", "selectstatic");
            $form->setType("contrainte", "selectstatic");
            $form->setType("texte_complete", "textareastatic");
            $form->setType("recuperee", "checkboxstatic");
        }

    }


    function setOnchange(&$form, $maj) {
    //javascript controle client
        $form->setOnchange('lien_contrainte_dossier_coordination','VerifNum(this)');
        $form->setOnchange('dossier_coordination','VerifNum(this)');
        $form->setOnchange('contrainte','VerifNum(this)');
    }
    /**
     * Methode setTaille
     */
    function setTaille(&$form, $maj) {
        $form->setTaille("lien_contrainte_dossier_coordination", 11);
        $form->setTaille("dossier_coordination", 11);
        $form->setTaille("contrainte", 11);
        $form->setTaille("texte_complete", 80);
        $form->setTaille("recuperee", 1);
    }

    /**
     * Methode setMax
     */
    function setMax(&$form, $maj) {
        $form->setMax("lien_contrainte_dossier_coordination", 11);
        $form->setMax("dossier_coordination", 11);
        $form->setMax("contrainte", 11);
        $form->setMax("texte_complete", 6);
        $form->setMax("recuperee", 1);
    }


    function setLib(&$form, $maj) {
    //libelle des champs
        $form->setLib('lien_contrainte_dossier_coordination', __('lien_contrainte_dossier_coordination'));
        $form->setLib('dossier_coordination', __('dossier_coordination'));
        $form->setLib('contrainte', __('contrainte'));
        $form->setLib('texte_complete', __('texte_complete'));
        $form->setLib('recuperee', __('recuperee'));
    }
    /**
     *
     */
    function setSelect(&$form, $maj, &$dnu1 = null, $dnu2 = null) {

        // contrainte
        $this->init_select(
            $form, 
            $this->f->db,
            $maj,
            null,
            "contrainte",
            $this->get_var_sql_forminc__sql("contrainte"),
            $this->get_var_sql_forminc__sql("contrainte_by_id"),
            true
        );
        // dossier_coordination
        $this->init_select(
            $form, 
            $this->f->db,
            $maj,
            null,
            "dossier_coordination",
            $this->get_var_sql_forminc__sql("dossier_coordination"),
            $this->get_var_sql_forminc__sql("dossier_coordination_by_id"),
            false
        );
    }


    //==================================
    // sous Formulaire
    //==================================
    

    function setValsousformulaire(&$form, $maj, $validation, $idxformulaire, $retourformulaire, $typeformulaire, &$dnu1 = null, $dnu2 = null) {
        $this->retourformulaire = $retourformulaire;
        if($validation == 0) {
            if($this->is_in_context_of_foreign_key('contrainte', $this->retourformulaire))
                $form->setVal('contrainte', $idxformulaire);
            if($this->is_in_context_of_foreign_key('dossier_coordination', $this->retourformulaire))
                $form->setVal('dossier_coordination', $idxformulaire);
        }// fin validation
        $this->set_form_default_values($form, $maj, $validation);
    }// fin setValsousformulaire

    //==================================
    // cle secondaire
    //==================================
    

}
