<?php
//$Id$ 
//gen openMairie le 21/07/2021 22:20

require_once "../obj/om_dbform.class.php";

class dossier_instruction_reunion_gen extends om_dbform {

    protected $_absolute_class_name = "dossier_instruction_reunion";

    var $table = "dossier_instruction_reunion";
    var $clePrimaire = "dossier_instruction_reunion";
    var $typeCle = "N";
    var $required_field = array(
        "date_souhaitee",
        "dossier_instruction",
        "dossier_instruction_reunion",
        "reunion_type",
        "reunion_type_categorie"
    );
    var $unique_key = array(
      array("ordre","reunion"),
    );
    var $foreign_keys_extended = array(
        "reunion_avis" => array("reunion_avis", ),
        "dossier_instruction" => array("dossier_instruction", "dossier_instruction_mes_plans", "dossier_instruction_mes_visites", "dossier_instruction_tous_plans", "dossier_instruction_tous_visites", "dossier_instruction_a_qualifier", "dossier_instruction_a_affecter", ),
        "reunion" => array("reunion", ),
        "reunion_type" => array("reunion_type", ),
        "reunion_categorie" => array("reunion_categorie", ),
    );
    
    /**
     *
     * @return string
     */
    function get_default_libelle() {
        return $this->getVal($this->clePrimaire)."&nbsp;".$this->getVal("dossier_instruction");
    }

    /**
     *
     * @return array
     */
    function get_var_sql_forminc__champs() {
        return array(
            "dossier_instruction_reunion",
            "dossier_instruction",
            "reunion_type",
            "reunion_type_categorie",
            "date_souhaitee",
            "motivation",
            "proposition_avis",
            "proposition_avis_complement",
            "reunion",
            "ordre",
            "avis",
            "avis_complement",
            "avis_motivation",
        );
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_avis() {
        return "SELECT reunion_avis.reunion_avis, reunion_avis.libelle FROM ".DB_PREFIXE."reunion_avis WHERE ((reunion_avis.om_validite_debut IS NULL AND (reunion_avis.om_validite_fin IS NULL OR reunion_avis.om_validite_fin > CURRENT_DATE)) OR (reunion_avis.om_validite_debut <= CURRENT_DATE AND (reunion_avis.om_validite_fin IS NULL OR reunion_avis.om_validite_fin > CURRENT_DATE))) ORDER BY reunion_avis.libelle ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_avis_by_id() {
        return "SELECT reunion_avis.reunion_avis, reunion_avis.libelle FROM ".DB_PREFIXE."reunion_avis WHERE reunion_avis = <idx>";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_dossier_instruction() {
        return "SELECT dossier_instruction.dossier_instruction, dossier_instruction.libelle FROM ".DB_PREFIXE."dossier_instruction ORDER BY dossier_instruction.libelle ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_dossier_instruction_by_id() {
        return "SELECT dossier_instruction.dossier_instruction, dossier_instruction.libelle FROM ".DB_PREFIXE."dossier_instruction WHERE dossier_instruction = <idx>";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_proposition_avis() {
        return "SELECT reunion_avis.reunion_avis, reunion_avis.libelle FROM ".DB_PREFIXE."reunion_avis WHERE ((reunion_avis.om_validite_debut IS NULL AND (reunion_avis.om_validite_fin IS NULL OR reunion_avis.om_validite_fin > CURRENT_DATE)) OR (reunion_avis.om_validite_debut <= CURRENT_DATE AND (reunion_avis.om_validite_fin IS NULL OR reunion_avis.om_validite_fin > CURRENT_DATE))) ORDER BY reunion_avis.libelle ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_proposition_avis_by_id() {
        return "SELECT reunion_avis.reunion_avis, reunion_avis.libelle FROM ".DB_PREFIXE."reunion_avis WHERE reunion_avis = <idx>";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_reunion() {
        return "SELECT reunion.reunion, reunion.libelle FROM ".DB_PREFIXE."reunion ORDER BY reunion.libelle ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_reunion_by_id() {
        return "SELECT reunion.reunion, reunion.libelle FROM ".DB_PREFIXE."reunion WHERE reunion = <idx>";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_reunion_type() {
        return "SELECT reunion_type.reunion_type, reunion_type.libelle FROM ".DB_PREFIXE."reunion_type WHERE ((reunion_type.om_validite_debut IS NULL AND (reunion_type.om_validite_fin IS NULL OR reunion_type.om_validite_fin > CURRENT_DATE)) OR (reunion_type.om_validite_debut <= CURRENT_DATE AND (reunion_type.om_validite_fin IS NULL OR reunion_type.om_validite_fin > CURRENT_DATE))) ORDER BY reunion_type.libelle ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_reunion_type_by_id() {
        return "SELECT reunion_type.reunion_type, reunion_type.libelle FROM ".DB_PREFIXE."reunion_type WHERE reunion_type = <idx>";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_reunion_type_categorie() {
        return "SELECT reunion_categorie.reunion_categorie, reunion_categorie.libelle FROM ".DB_PREFIXE."reunion_categorie WHERE ((reunion_categorie.om_validite_debut IS NULL AND (reunion_categorie.om_validite_fin IS NULL OR reunion_categorie.om_validite_fin > CURRENT_DATE)) OR (reunion_categorie.om_validite_debut <= CURRENT_DATE AND (reunion_categorie.om_validite_fin IS NULL OR reunion_categorie.om_validite_fin > CURRENT_DATE))) ORDER BY reunion_categorie.libelle ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_reunion_type_categorie_by_id() {
        return "SELECT reunion_categorie.reunion_categorie, reunion_categorie.libelle FROM ".DB_PREFIXE."reunion_categorie WHERE reunion_categorie = <idx>";
    }




    function setvalF($val = array()) {
        //affectation valeur formulaire
        if (!is_numeric($val['dossier_instruction_reunion'])) {
            $this->valF['dossier_instruction_reunion'] = ""; // -> requis
        } else {
            $this->valF['dossier_instruction_reunion'] = $val['dossier_instruction_reunion'];
        }
        if (!is_numeric($val['dossier_instruction'])) {
            $this->valF['dossier_instruction'] = ""; // -> requis
        } else {
            $this->valF['dossier_instruction'] = $val['dossier_instruction'];
        }
        if (!is_numeric($val['reunion_type'])) {
            $this->valF['reunion_type'] = ""; // -> requis
        } else {
            $this->valF['reunion_type'] = $val['reunion_type'];
        }
        if (!is_numeric($val['reunion_type_categorie'])) {
            $this->valF['reunion_type_categorie'] = ""; // -> requis
        } else {
            $this->valF['reunion_type_categorie'] = $val['reunion_type_categorie'];
        }
        if ($val['date_souhaitee'] != "") {
            $this->valF['date_souhaitee'] = $this->dateDB($val['date_souhaitee']);
        }
            $this->valF['motivation'] = $val['motivation'];
        if (!is_numeric($val['proposition_avis'])) {
            $this->valF['proposition_avis'] = NULL;
        } else {
            $this->valF['proposition_avis'] = $val['proposition_avis'];
        }
        if ($val['proposition_avis_complement'] == "") {
            $this->valF['proposition_avis_complement'] = NULL;
        } else {
            $this->valF['proposition_avis_complement'] = $val['proposition_avis_complement'];
        }
        if (!is_numeric($val['reunion'])) {
            $this->valF['reunion'] = NULL;
        } else {
            $this->valF['reunion'] = $val['reunion'];
        }
        if (!is_numeric($val['ordre'])) {
            $this->valF['ordre'] = NULL;
        } else {
            $this->valF['ordre'] = $val['ordre'];
        }
        if (!is_numeric($val['avis'])) {
            $this->valF['avis'] = NULL;
        } else {
            $this->valF['avis'] = $val['avis'];
        }
        if ($val['avis_complement'] == "") {
            $this->valF['avis_complement'] = NULL;
        } else {
            $this->valF['avis_complement'] = $val['avis_complement'];
        }
            $this->valF['avis_motivation'] = $val['avis_motivation'];
    }

    //=================================================
    //cle primaire automatique [automatic primary key]
    //==================================================

    function setId(&$dnu1 = null) {
    //numero automatique
        $this->valF[$this->clePrimaire] = $this->f->db->nextId(DB_PREFIXE.$this->table);
    }

    function setValFAjout($val = array()) {
    //numero automatique -> pas de controle ajout cle primaire
    }

    function verifierAjout($val = array(), &$dnu1 = null) {
    //numero automatique -> pas de verfication de cle primaire
    }

    //==========================
    // Formulaire  [form]
    //==========================
    /**
     *
     */
    function setType(&$form, $maj) {
        // Récupération du mode de l'action
        $crud = $this->get_action_crud($maj);

        // MODE AJOUTER
        if ($maj == 0 || $crud == 'create') {
            $form->setType("dossier_instruction_reunion", "hidden");
            if ($this->is_in_context_of_foreign_key("dossier_instruction", $this->retourformulaire)) {
                $form->setType("dossier_instruction", "selecthiddenstatic");
            } else {
                $form->setType("dossier_instruction", "select");
            }
            if ($this->is_in_context_of_foreign_key("reunion_type", $this->retourformulaire)) {
                $form->setType("reunion_type", "selecthiddenstatic");
            } else {
                $form->setType("reunion_type", "select");
            }
            if ($this->is_in_context_of_foreign_key("reunion_categorie", $this->retourformulaire)) {
                $form->setType("reunion_type_categorie", "selecthiddenstatic");
            } else {
                $form->setType("reunion_type_categorie", "select");
            }
            $form->setType("date_souhaitee", "date");
            $form->setType("motivation", "textarea");
            if ($this->is_in_context_of_foreign_key("reunion_avis", $this->retourformulaire)) {
                $form->setType("proposition_avis", "selecthiddenstatic");
            } else {
                $form->setType("proposition_avis", "select");
            }
            $form->setType("proposition_avis_complement", "text");
            if ($this->is_in_context_of_foreign_key("reunion", $this->retourformulaire)) {
                $form->setType("reunion", "selecthiddenstatic");
            } else {
                $form->setType("reunion", "select");
            }
            $form->setType("ordre", "text");
            if ($this->is_in_context_of_foreign_key("reunion_avis", $this->retourformulaire)) {
                $form->setType("avis", "selecthiddenstatic");
            } else {
                $form->setType("avis", "select");
            }
            $form->setType("avis_complement", "text");
            $form->setType("avis_motivation", "textarea");
        }

        // MDOE MODIFIER
        if ($maj == 1 || $crud == 'update') {
            $form->setType("dossier_instruction_reunion", "hiddenstatic");
            if ($this->is_in_context_of_foreign_key("dossier_instruction", $this->retourformulaire)) {
                $form->setType("dossier_instruction", "selecthiddenstatic");
            } else {
                $form->setType("dossier_instruction", "select");
            }
            if ($this->is_in_context_of_foreign_key("reunion_type", $this->retourformulaire)) {
                $form->setType("reunion_type", "selecthiddenstatic");
            } else {
                $form->setType("reunion_type", "select");
            }
            if ($this->is_in_context_of_foreign_key("reunion_categorie", $this->retourformulaire)) {
                $form->setType("reunion_type_categorie", "selecthiddenstatic");
            } else {
                $form->setType("reunion_type_categorie", "select");
            }
            $form->setType("date_souhaitee", "date");
            $form->setType("motivation", "textarea");
            if ($this->is_in_context_of_foreign_key("reunion_avis", $this->retourformulaire)) {
                $form->setType("proposition_avis", "selecthiddenstatic");
            } else {
                $form->setType("proposition_avis", "select");
            }
            $form->setType("proposition_avis_complement", "text");
            if ($this->is_in_context_of_foreign_key("reunion", $this->retourformulaire)) {
                $form->setType("reunion", "selecthiddenstatic");
            } else {
                $form->setType("reunion", "select");
            }
            $form->setType("ordre", "text");
            if ($this->is_in_context_of_foreign_key("reunion_avis", $this->retourformulaire)) {
                $form->setType("avis", "selecthiddenstatic");
            } else {
                $form->setType("avis", "select");
            }
            $form->setType("avis_complement", "text");
            $form->setType("avis_motivation", "textarea");
        }

        // MODE SUPPRIMER
        if ($maj == 2 || $crud == 'delete') {
            $form->setType("dossier_instruction_reunion", "hiddenstatic");
            $form->setType("dossier_instruction", "selectstatic");
            $form->setType("reunion_type", "selectstatic");
            $form->setType("reunion_type_categorie", "selectstatic");
            $form->setType("date_souhaitee", "hiddenstatic");
            $form->setType("motivation", "hiddenstatic");
            $form->setType("proposition_avis", "selectstatic");
            $form->setType("proposition_avis_complement", "hiddenstatic");
            $form->setType("reunion", "selectstatic");
            $form->setType("ordre", "hiddenstatic");
            $form->setType("avis", "selectstatic");
            $form->setType("avis_complement", "hiddenstatic");
            $form->setType("avis_motivation", "hiddenstatic");
        }

        // MODE CONSULTER
        if ($maj == 3 || $crud == 'read') {
            $form->setType("dossier_instruction_reunion", "static");
            $form->setType("dossier_instruction", "selectstatic");
            $form->setType("reunion_type", "selectstatic");
            $form->setType("reunion_type_categorie", "selectstatic");
            $form->setType("date_souhaitee", "datestatic");
            $form->setType("motivation", "textareastatic");
            $form->setType("proposition_avis", "selectstatic");
            $form->setType("proposition_avis_complement", "static");
            $form->setType("reunion", "selectstatic");
            $form->setType("ordre", "static");
            $form->setType("avis", "selectstatic");
            $form->setType("avis_complement", "static");
            $form->setType("avis_motivation", "textareastatic");
        }

    }


    function setOnchange(&$form, $maj) {
    //javascript controle client
        $form->setOnchange('dossier_instruction_reunion','VerifNum(this)');
        $form->setOnchange('dossier_instruction','VerifNum(this)');
        $form->setOnchange('reunion_type','VerifNum(this)');
        $form->setOnchange('reunion_type_categorie','VerifNum(this)');
        $form->setOnchange('date_souhaitee','fdate(this)');
        $form->setOnchange('proposition_avis','VerifNum(this)');
        $form->setOnchange('reunion','VerifNum(this)');
        $form->setOnchange('ordre','VerifNum(this)');
        $form->setOnchange('avis','VerifNum(this)');
    }
    /**
     * Methode setTaille
     */
    function setTaille(&$form, $maj) {
        $form->setTaille("dossier_instruction_reunion", 11);
        $form->setTaille("dossier_instruction", 11);
        $form->setTaille("reunion_type", 11);
        $form->setTaille("reunion_type_categorie", 11);
        $form->setTaille("date_souhaitee", 12);
        $form->setTaille("motivation", 80);
        $form->setTaille("proposition_avis", 11);
        $form->setTaille("proposition_avis_complement", 30);
        $form->setTaille("reunion", 11);
        $form->setTaille("ordre", 11);
        $form->setTaille("avis", 11);
        $form->setTaille("avis_complement", 30);
        $form->setTaille("avis_motivation", 80);
    }

    /**
     * Methode setMax
     */
    function setMax(&$form, $maj) {
        $form->setMax("dossier_instruction_reunion", 11);
        $form->setMax("dossier_instruction", 11);
        $form->setMax("reunion_type", 11);
        $form->setMax("reunion_type_categorie", 11);
        $form->setMax("date_souhaitee", 12);
        $form->setMax("motivation", 6);
        $form->setMax("proposition_avis", 11);
        $form->setMax("proposition_avis_complement", 250);
        $form->setMax("reunion", 11);
        $form->setMax("ordre", 11);
        $form->setMax("avis", 11);
        $form->setMax("avis_complement", 250);
        $form->setMax("avis_motivation", 6);
    }


    function setLib(&$form, $maj) {
    //libelle des champs
        $form->setLib('dossier_instruction_reunion', __('dossier_instruction_reunion'));
        $form->setLib('dossier_instruction', __('dossier_instruction'));
        $form->setLib('reunion_type', __('reunion_type'));
        $form->setLib('reunion_type_categorie', __('reunion_type_categorie'));
        $form->setLib('date_souhaitee', __('date_souhaitee'));
        $form->setLib('motivation', __('motivation'));
        $form->setLib('proposition_avis', __('proposition_avis'));
        $form->setLib('proposition_avis_complement', __('proposition_avis_complement'));
        $form->setLib('reunion', __('reunion'));
        $form->setLib('ordre', __('ordre'));
        $form->setLib('avis', __('avis'));
        $form->setLib('avis_complement', __('avis_complement'));
        $form->setLib('avis_motivation', __('avis_motivation'));
    }
    /**
     *
     */
    function setSelect(&$form, $maj, &$dnu1 = null, $dnu2 = null) {

        // avis
        $this->init_select(
            $form, 
            $this->f->db,
            $maj,
            null,
            "avis",
            $this->get_var_sql_forminc__sql("avis"),
            $this->get_var_sql_forminc__sql("avis_by_id"),
            true
        );
        // dossier_instruction
        $this->init_select(
            $form, 
            $this->f->db,
            $maj,
            null,
            "dossier_instruction",
            $this->get_var_sql_forminc__sql("dossier_instruction"),
            $this->get_var_sql_forminc__sql("dossier_instruction_by_id"),
            false
        );
        // proposition_avis
        $this->init_select(
            $form, 
            $this->f->db,
            $maj,
            null,
            "proposition_avis",
            $this->get_var_sql_forminc__sql("proposition_avis"),
            $this->get_var_sql_forminc__sql("proposition_avis_by_id"),
            true
        );
        // reunion
        $this->init_select(
            $form, 
            $this->f->db,
            $maj,
            null,
            "reunion",
            $this->get_var_sql_forminc__sql("reunion"),
            $this->get_var_sql_forminc__sql("reunion_by_id"),
            false
        );
        // reunion_type
        $this->init_select(
            $form, 
            $this->f->db,
            $maj,
            null,
            "reunion_type",
            $this->get_var_sql_forminc__sql("reunion_type"),
            $this->get_var_sql_forminc__sql("reunion_type_by_id"),
            true
        );
        // reunion_type_categorie
        $this->init_select(
            $form, 
            $this->f->db,
            $maj,
            null,
            "reunion_type_categorie",
            $this->get_var_sql_forminc__sql("reunion_type_categorie"),
            $this->get_var_sql_forminc__sql("reunion_type_categorie_by_id"),
            true
        );
    }


    //==================================
    // sous Formulaire
    //==================================
    

    function setValsousformulaire(&$form, $maj, $validation, $idxformulaire, $retourformulaire, $typeformulaire, &$dnu1 = null, $dnu2 = null) {
        $this->retourformulaire = $retourformulaire;
        if($validation == 0) {
            if($this->is_in_context_of_foreign_key('dossier_instruction', $this->retourformulaire))
                $form->setVal('dossier_instruction', $idxformulaire);
            if($this->is_in_context_of_foreign_key('reunion', $this->retourformulaire))
                $form->setVal('reunion', $idxformulaire);
            if($this->is_in_context_of_foreign_key('reunion_type', $this->retourformulaire))
                $form->setVal('reunion_type', $idxformulaire);
            if($this->is_in_context_of_foreign_key('reunion_categorie', $this->retourformulaire))
                $form->setVal('reunion_type_categorie', $idxformulaire);
        }// fin validation
        if ($validation == 0 and $maj == 0) {
            if($this->is_in_context_of_foreign_key('reunion_avis', $this->retourformulaire))
                $form->setVal('avis', $idxformulaire);
            if($this->is_in_context_of_foreign_key('reunion_avis', $this->retourformulaire))
                $form->setVal('proposition_avis', $idxformulaire);
        }// fin validation
        $this->set_form_default_values($form, $maj, $validation);
    }// fin setValsousformulaire

    //==================================
    // cle secondaire
    //==================================
    
    /**
     * Methode clesecondaire
     */
    function cleSecondaire($id, &$dnu1 = null, $val = array(), $dnu2 = null) {
        // On appelle la methode de la classe parent
        parent::cleSecondaire($id);
        // Verification de la cle secondaire : autorite_police
        $this->rechercheTable($this->f->db, "autorite_police", "dossier_instruction_reunion", $id);
        // Verification de la cle secondaire : autorite_police
        $this->rechercheTable($this->f->db, "autorite_police", "dossier_instruction_reunion_prochain", $id);
        // Verification de la cle secondaire : proces_verbal
        $this->rechercheTable($this->f->db, "proces_verbal", "dossier_instruction_reunion", $id);
    }


}
