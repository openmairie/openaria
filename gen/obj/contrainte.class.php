<?php
//$Id$ 
//gen openMairie le 03/05/2018 09:12

require_once "../obj/om_dbform.class.php";

class contrainte_gen extends om_dbform {

    protected $_absolute_class_name = "contrainte";

    var $table = "contrainte";
    var $clePrimaire = "contrainte";
    var $typeCle = "N";
    var $required_field = array(
        "contrainte",
        "libelle",
        "nature"
    );
    
    var $foreign_keys_extended = array(
    );
    
    /**
     *
     * @return string
     */
    function get_default_libelle() {
        return $this->getVal($this->clePrimaire)."&nbsp;".$this->getVal("libelle");
    }

    /**
     *
     * @return array
     */
    function get_var_sql_forminc__champs() {
        return array(
            "contrainte",
            "id_referentiel",
            "nature",
            "groupe",
            "sousgroupe",
            "libelle",
            "texte",
            "texte_surcharge",
            "ordre_d_affichage",
            "lie_a_un_referentiel",
            "om_validite_debut",
            "om_validite_fin",
        );
    }




    function setvalF($val = array()) {
        //affectation valeur formulaire
        if (!is_numeric($val['contrainte'])) {
            $this->valF['contrainte'] = ""; // -> requis
        } else {
            $this->valF['contrainte'] = $val['contrainte'];
        }
        if ($val['id_referentiel'] == "") {
            $this->valF['id_referentiel'] = NULL;
        } else {
            $this->valF['id_referentiel'] = $val['id_referentiel'];
        }
        $this->valF['nature'] = $val['nature'];
        if ($val['groupe'] == "") {
            $this->valF['groupe'] = NULL;
        } else {
            $this->valF['groupe'] = $val['groupe'];
        }
        if ($val['sousgroupe'] == "") {
            $this->valF['sousgroupe'] = NULL;
        } else {
            $this->valF['sousgroupe'] = $val['sousgroupe'];
        }
        $this->valF['libelle'] = $val['libelle'];
            $this->valF['texte'] = $val['texte'];
            $this->valF['texte_surcharge'] = $val['texte_surcharge'];
        if (!is_numeric($val['ordre_d_affichage'])) {
            $this->valF['ordre_d_affichage'] = NULL;
        } else {
            $this->valF['ordre_d_affichage'] = $val['ordre_d_affichage'];
        }
        if ($val['lie_a_un_referentiel'] == 1 || $val['lie_a_un_referentiel'] == "t" || $val['lie_a_un_referentiel'] == "Oui") {
            $this->valF['lie_a_un_referentiel'] = true;
        } else {
            $this->valF['lie_a_un_referentiel'] = false;
        }
        if ($val['om_validite_debut'] != "") {
            $this->valF['om_validite_debut'] = $this->dateDB($val['om_validite_debut']);
        } else {
            $this->valF['om_validite_debut'] = NULL;
        }
        if ($val['om_validite_fin'] != "") {
            $this->valF['om_validite_fin'] = $this->dateDB($val['om_validite_fin']);
        } else {
            $this->valF['om_validite_fin'] = NULL;
        }
    }

    //=================================================
    //cle primaire automatique [automatic primary key]
    //==================================================

    function setId(&$dnu1 = null) {
    //numero automatique
        $this->valF[$this->clePrimaire] = $this->f->db->nextId(DB_PREFIXE.$this->table);
    }

    function setValFAjout($val = array()) {
    //numero automatique -> pas de controle ajout cle primaire
    }

    function verifierAjout($val = array(), &$dnu1 = null) {
    //numero automatique -> pas de verfication de cle primaire
    }
    /**
     * Methode verifier
     */
    function verifier($val = array(), &$dnu1 = null, $dnu2 = null) {
        // On appelle la methode de la classe parent
        parent::verifier($val, $this->f->db, null);

        // gestion des dates de validites
        $date_debut = $this->valF['om_validite_debut'];
        $date_fin = $this->valF['om_validite_fin'];

        if ($date_debut != '' and $date_fin != '') {
        
            $date_debut = explode('-', $this->valF['om_validite_debut']);
            $date_fin = explode('-', $this->valF['om_validite_fin']);

            $time_debut = mktime(0, 0, 0, $date_debut[1], $date_debut[2],
                                 $date_debut[0]);
            $time_fin = mktime(0, 0, 0, $date_fin[1], $date_fin[2],
                                 $date_fin[0]);

            if ($time_debut > $time_fin or $time_debut == $time_fin) {
                $this->correct = false;
                $this->addToMessage(__('La date de fin de validite doit etre future a la de debut de validite.'));
            }
        }
    }


    //==========================
    // Formulaire  [form]
    //==========================
    /**
     *
     */
    function setType(&$form, $maj) {
        // Récupération du mode de l'action
        $crud = $this->get_action_crud($maj);

        // MODE AJOUTER
        if ($maj == 0 || $crud == 'create') {
            $form->setType("contrainte", "hidden");
            $form->setType("id_referentiel", "text");
            $form->setType("nature", "text");
            $form->setType("groupe", "text");
            $form->setType("sousgroupe", "text");
            $form->setType("libelle", "text");
            $form->setType("texte", "textarea");
            $form->setType("texte_surcharge", "textarea");
            $form->setType("ordre_d_affichage", "text");
            $form->setType("lie_a_un_referentiel", "checkbox");
            if ($this->f->isAccredited(array($this->table."_modifier_validite", $this->table, ))) {
                $form->setType("om_validite_debut", "date");
            } else {
                $form->setType("om_validite_debut", "hiddenstaticdate");
            }
            if ($this->f->isAccredited(array($this->table."_modifier_validite", $this->table, ))) {
                $form->setType("om_validite_fin", "date");
            } else {
                $form->setType("om_validite_fin", "hiddenstaticdate");
            }
        }

        // MDOE MODIFIER
        if ($maj == 1 || $crud == 'update') {
            $form->setType("contrainte", "hiddenstatic");
            $form->setType("id_referentiel", "text");
            $form->setType("nature", "text");
            $form->setType("groupe", "text");
            $form->setType("sousgroupe", "text");
            $form->setType("libelle", "text");
            $form->setType("texte", "textarea");
            $form->setType("texte_surcharge", "textarea");
            $form->setType("ordre_d_affichage", "text");
            $form->setType("lie_a_un_referentiel", "checkbox");
            if ($this->f->isAccredited(array($this->table."_modifier_validite", $this->table, ))) {
                $form->setType("om_validite_debut", "date");
            } else {
                $form->setType("om_validite_debut", "hiddenstaticdate");
            }
            if ($this->f->isAccredited(array($this->table."_modifier_validite", $this->table, ))) {
                $form->setType("om_validite_fin", "date");
            } else {
                $form->setType("om_validite_fin", "hiddenstaticdate");
            }
        }

        // MODE SUPPRIMER
        if ($maj == 2 || $crud == 'delete') {
            $form->setType("contrainte", "hiddenstatic");
            $form->setType("id_referentiel", "hiddenstatic");
            $form->setType("nature", "hiddenstatic");
            $form->setType("groupe", "hiddenstatic");
            $form->setType("sousgroupe", "hiddenstatic");
            $form->setType("libelle", "hiddenstatic");
            $form->setType("texte", "hiddenstatic");
            $form->setType("texte_surcharge", "hiddenstatic");
            $form->setType("ordre_d_affichage", "hiddenstatic");
            $form->setType("lie_a_un_referentiel", "hiddenstatic");
            $form->setType("om_validite_debut", "hiddenstatic");
            $form->setType("om_validite_fin", "hiddenstatic");
        }

        // MODE CONSULTER
        if ($maj == 3 || $crud == 'read') {
            $form->setType("contrainte", "static");
            $form->setType("id_referentiel", "static");
            $form->setType("nature", "static");
            $form->setType("groupe", "static");
            $form->setType("sousgroupe", "static");
            $form->setType("libelle", "static");
            $form->setType("texte", "textareastatic");
            $form->setType("texte_surcharge", "textareastatic");
            $form->setType("ordre_d_affichage", "static");
            $form->setType("lie_a_un_referentiel", "checkboxstatic");
            $form->setType("om_validite_debut", "datestatic");
            $form->setType("om_validite_fin", "datestatic");
        }

    }


    function setOnchange(&$form, $maj) {
    //javascript controle client
        $form->setOnchange('contrainte','VerifNum(this)');
        $form->setOnchange('ordre_d_affichage','VerifNum(this)');
        $form->setOnchange('om_validite_debut','fdate(this)');
        $form->setOnchange('om_validite_fin','fdate(this)');
    }
    /**
     * Methode setTaille
     */
    function setTaille(&$form, $maj) {
        $form->setTaille("contrainte", 11);
        $form->setTaille("id_referentiel", 30);
        $form->setTaille("nature", 10);
        $form->setTaille("groupe", 30);
        $form->setTaille("sousgroupe", 30);
        $form->setTaille("libelle", 30);
        $form->setTaille("texte", 80);
        $form->setTaille("texte_surcharge", 80);
        $form->setTaille("ordre_d_affichage", 11);
        $form->setTaille("lie_a_un_referentiel", 1);
        $form->setTaille("om_validite_debut", 12);
        $form->setTaille("om_validite_fin", 12);
    }

    /**
     * Methode setMax
     */
    function setMax(&$form, $maj) {
        $form->setMax("contrainte", 11);
        $form->setMax("id_referentiel", 250);
        $form->setMax("nature", 10);
        $form->setMax("groupe", 250);
        $form->setMax("sousgroupe", 250);
        $form->setMax("libelle", 250);
        $form->setMax("texte", 6);
        $form->setMax("texte_surcharge", 6);
        $form->setMax("ordre_d_affichage", 11);
        $form->setMax("lie_a_un_referentiel", 1);
        $form->setMax("om_validite_debut", 12);
        $form->setMax("om_validite_fin", 12);
    }


    function setLib(&$form, $maj) {
    //libelle des champs
        $form->setLib('contrainte', __('contrainte'));
        $form->setLib('id_referentiel', __('id_referentiel'));
        $form->setLib('nature', __('nature'));
        $form->setLib('groupe', __('groupe'));
        $form->setLib('sousgroupe', __('sousgroupe'));
        $form->setLib('libelle', __('libelle'));
        $form->setLib('texte', __('texte'));
        $form->setLib('texte_surcharge', __('texte_surcharge'));
        $form->setLib('ordre_d_affichage', __('ordre_d_affichage'));
        $form->setLib('lie_a_un_referentiel', __('lie_a_un_referentiel'));
        $form->setLib('om_validite_debut', __('om_validite_debut'));
        $form->setLib('om_validite_fin', __('om_validite_fin'));
    }
    /**
     *
     */
    function setSelect(&$form, $maj, &$dnu1 = null, $dnu2 = null) {

    }


    //==================================
    // sous Formulaire
    //==================================
    

    function setValsousformulaire(&$form, $maj, $validation, $idxformulaire, $retourformulaire, $typeformulaire, &$dnu1 = null, $dnu2 = null) {
        $this->retourformulaire = $retourformulaire;
        $this->set_form_default_values($form, $maj, $validation);
    }// fin setValsousformulaire

    //==================================
    // cle secondaire
    //==================================
    
    /**
     * Methode clesecondaire
     */
    function cleSecondaire($id, &$dnu1 = null, $val = array(), $dnu2 = null) {
        // On appelle la methode de la classe parent
        parent::cleSecondaire($id);
        // Verification de la cle secondaire : lien_contrainte_dossier_coordination
        $this->rechercheTable($this->f->db, "lien_contrainte_dossier_coordination", "contrainte", $id);
        // Verification de la cle secondaire : lien_contrainte_etablissement
        $this->rechercheTable($this->f->db, "lien_contrainte_etablissement", "contrainte", $id);
    }


}
