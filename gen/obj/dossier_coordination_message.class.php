<?php
//$Id$ 
//gen openMairie le 20/05/2020 18:03

require_once "../obj/om_dbform.class.php";

class dossier_coordination_message_gen extends om_dbform {

    protected $_absolute_class_name = "dossier_coordination_message";

    var $table = "dossier_coordination_message";
    var $clePrimaire = "dossier_coordination_message";
    var $typeCle = "N";
    var $required_field = array(
        "categorie",
        "date_emission",
        "dossier_coordination_message"
    );
    
    var $foreign_keys_extended = array(
        "dossier_coordination" => array("dossier_coordination", "dossier_coordination_nouveau", "dossier_coordination_a_qualifier", "dossier_coordination_a_cloturer", ),
    );
    
    /**
     *
     * @return string
     */
    function get_default_libelle() {
        return $this->getVal($this->clePrimaire)."&nbsp;".$this->getVal("categorie");
    }

    /**
     *
     * @return array
     */
    function get_var_sql_forminc__champs() {
        return array(
            "dossier_coordination_message",
            "categorie",
            "dossier_coordination",
            "type",
            "emetteur",
            "date_emission",
            "contenu",
            "contenu_json",
            "si_cadre_lu",
            "si_technicien_lu",
            "si_mode_lecture",
            "acc_cadre_lu",
            "acc_technicien_lu",
            "acc_mode_lecture",
        );
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_dossier_coordination() {
        return "SELECT dossier_coordination.dossier_coordination, dossier_coordination.libelle FROM ".DB_PREFIXE."dossier_coordination ORDER BY dossier_coordination.libelle ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_dossier_coordination_by_id() {
        return "SELECT dossier_coordination.dossier_coordination, dossier_coordination.libelle FROM ".DB_PREFIXE."dossier_coordination WHERE dossier_coordination = <idx>";
    }




    function setvalF($val = array()) {
        //affectation valeur formulaire
        if (!is_numeric($val['dossier_coordination_message'])) {
            $this->valF['dossier_coordination_message'] = ""; // -> requis
        } else {
            $this->valF['dossier_coordination_message'] = $val['dossier_coordination_message'];
        }
        $this->valF['categorie'] = $val['categorie'];
        if (!is_numeric($val['dossier_coordination'])) {
            $this->valF['dossier_coordination'] = NULL;
        } else {
            $this->valF['dossier_coordination'] = $val['dossier_coordination'];
        }
        if ($val['type'] == "") {
            $this->valF['type'] = NULL;
        } else {
            $this->valF['type'] = $val['type'];
        }
        if ($val['emetteur'] == "") {
            $this->valF['emetteur'] = NULL;
        } else {
            $this->valF['emetteur'] = $val['emetteur'];
        }
            $this->valF['date_emission'] = $val['date_emission'];
            $this->valF['contenu'] = $val['contenu'];
            $this->valF['contenu_json'] = $val['contenu_json'];
        if ($val['si_cadre_lu'] == 1 || $val['si_cadre_lu'] == "t" || $val['si_cadre_lu'] == "Oui") {
            $this->valF['si_cadre_lu'] = true;
        } else {
            $this->valF['si_cadre_lu'] = false;
        }
        if ($val['si_technicien_lu'] == 1 || $val['si_technicien_lu'] == "t" || $val['si_technicien_lu'] == "Oui") {
            $this->valF['si_technicien_lu'] = true;
        } else {
            $this->valF['si_technicien_lu'] = false;
        }
        if ($val['si_mode_lecture'] == "") {
            $this->valF['si_mode_lecture'] = NULL;
        } else {
            $this->valF['si_mode_lecture'] = $val['si_mode_lecture'];
        }
        if ($val['acc_cadre_lu'] == 1 || $val['acc_cadre_lu'] == "t" || $val['acc_cadre_lu'] == "Oui") {
            $this->valF['acc_cadre_lu'] = true;
        } else {
            $this->valF['acc_cadre_lu'] = false;
        }
        if ($val['acc_technicien_lu'] == 1 || $val['acc_technicien_lu'] == "t" || $val['acc_technicien_lu'] == "Oui") {
            $this->valF['acc_technicien_lu'] = true;
        } else {
            $this->valF['acc_technicien_lu'] = false;
        }
        if ($val['acc_mode_lecture'] == "") {
            $this->valF['acc_mode_lecture'] = NULL;
        } else {
            $this->valF['acc_mode_lecture'] = $val['acc_mode_lecture'];
        }
    }

    //=================================================
    //cle primaire automatique [automatic primary key]
    //==================================================

    function setId(&$dnu1 = null) {
    //numero automatique
        $this->valF[$this->clePrimaire] = $this->f->db->nextId(DB_PREFIXE.$this->table);
    }

    function setValFAjout($val = array()) {
    //numero automatique -> pas de controle ajout cle primaire
    }

    function verifierAjout($val = array(), &$dnu1 = null) {
    //numero automatique -> pas de verfication de cle primaire
    }

    //==========================
    // Formulaire  [form]
    //==========================
    /**
     *
     */
    function setType(&$form, $maj) {
        // Récupération du mode de l'action
        $crud = $this->get_action_crud($maj);

        // MODE AJOUTER
        if ($maj == 0 || $crud == 'create') {
            $form->setType("dossier_coordination_message", "hidden");
            $form->setType("categorie", "text");
            if ($this->is_in_context_of_foreign_key("dossier_coordination", $this->retourformulaire)) {
                $form->setType("dossier_coordination", "selecthiddenstatic");
            } else {
                $form->setType("dossier_coordination", "select");
            }
            $form->setType("type", "text");
            $form->setType("emetteur", "text");
            $form->setType("date_emission", "text");
            $form->setType("contenu", "textarea");
            $form->setType("contenu_json", "textarea");
            $form->setType("si_cadre_lu", "checkbox");
            $form->setType("si_technicien_lu", "checkbox");
            $form->setType("si_mode_lecture", "text");
            $form->setType("acc_cadre_lu", "checkbox");
            $form->setType("acc_technicien_lu", "checkbox");
            $form->setType("acc_mode_lecture", "text");
        }

        // MDOE MODIFIER
        if ($maj == 1 || $crud == 'update') {
            $form->setType("dossier_coordination_message", "hiddenstatic");
            $form->setType("categorie", "text");
            if ($this->is_in_context_of_foreign_key("dossier_coordination", $this->retourformulaire)) {
                $form->setType("dossier_coordination", "selecthiddenstatic");
            } else {
                $form->setType("dossier_coordination", "select");
            }
            $form->setType("type", "text");
            $form->setType("emetteur", "text");
            $form->setType("date_emission", "text");
            $form->setType("contenu", "textarea");
            $form->setType("contenu_json", "textarea");
            $form->setType("si_cadre_lu", "checkbox");
            $form->setType("si_technicien_lu", "checkbox");
            $form->setType("si_mode_lecture", "text");
            $form->setType("acc_cadre_lu", "checkbox");
            $form->setType("acc_technicien_lu", "checkbox");
            $form->setType("acc_mode_lecture", "text");
        }

        // MODE SUPPRIMER
        if ($maj == 2 || $crud == 'delete') {
            $form->setType("dossier_coordination_message", "hiddenstatic");
            $form->setType("categorie", "hiddenstatic");
            $form->setType("dossier_coordination", "selectstatic");
            $form->setType("type", "hiddenstatic");
            $form->setType("emetteur", "hiddenstatic");
            $form->setType("date_emission", "hiddenstatic");
            $form->setType("contenu", "hiddenstatic");
            $form->setType("contenu_json", "hiddenstatic");
            $form->setType("si_cadre_lu", "hiddenstatic");
            $form->setType("si_technicien_lu", "hiddenstatic");
            $form->setType("si_mode_lecture", "hiddenstatic");
            $form->setType("acc_cadre_lu", "hiddenstatic");
            $form->setType("acc_technicien_lu", "hiddenstatic");
            $form->setType("acc_mode_lecture", "hiddenstatic");
        }

        // MODE CONSULTER
        if ($maj == 3 || $crud == 'read') {
            $form->setType("dossier_coordination_message", "static");
            $form->setType("categorie", "static");
            $form->setType("dossier_coordination", "selectstatic");
            $form->setType("type", "static");
            $form->setType("emetteur", "static");
            $form->setType("date_emission", "static");
            $form->setType("contenu", "textareastatic");
            $form->setType("contenu_json", "textareastatic");
            $form->setType("si_cadre_lu", "checkboxstatic");
            $form->setType("si_technicien_lu", "checkboxstatic");
            $form->setType("si_mode_lecture", "static");
            $form->setType("acc_cadre_lu", "checkboxstatic");
            $form->setType("acc_technicien_lu", "checkboxstatic");
            $form->setType("acc_mode_lecture", "static");
        }

    }


    function setOnchange(&$form, $maj) {
    //javascript controle client
        $form->setOnchange('dossier_coordination_message','VerifNum(this)');
        $form->setOnchange('dossier_coordination','VerifNum(this)');
    }
    /**
     * Methode setTaille
     */
    function setTaille(&$form, $maj) {
        $form->setTaille("dossier_coordination_message", 11);
        $form->setTaille("categorie", 20);
        $form->setTaille("dossier_coordination", 11);
        $form->setTaille("type", 30);
        $form->setTaille("emetteur", 30);
        $form->setTaille("date_emission", 8);
        $form->setTaille("contenu", 80);
        $form->setTaille("contenu_json", 80);
        $form->setTaille("si_cadre_lu", 1);
        $form->setTaille("si_technicien_lu", 1);
        $form->setTaille("si_mode_lecture", 10);
        $form->setTaille("acc_cadre_lu", 1);
        $form->setTaille("acc_technicien_lu", 1);
        $form->setTaille("acc_mode_lecture", 10);
    }

    /**
     * Methode setMax
     */
    function setMax(&$form, $maj) {
        $form->setMax("dossier_coordination_message", 11);
        $form->setMax("categorie", 20);
        $form->setMax("dossier_coordination", 11);
        $form->setMax("type", 60);
        $form->setMax("emetteur", 40);
        $form->setMax("date_emission", 8);
        $form->setMax("contenu", 6);
        $form->setMax("contenu_json", 6);
        $form->setMax("si_cadre_lu", 1);
        $form->setMax("si_technicien_lu", 1);
        $form->setMax("si_mode_lecture", 10);
        $form->setMax("acc_cadre_lu", 1);
        $form->setMax("acc_technicien_lu", 1);
        $form->setMax("acc_mode_lecture", 10);
    }


    function setLib(&$form, $maj) {
    //libelle des champs
        $form->setLib('dossier_coordination_message', __('dossier_coordination_message'));
        $form->setLib('categorie', __('categorie'));
        $form->setLib('dossier_coordination', __('dossier_coordination'));
        $form->setLib('type', __('type'));
        $form->setLib('emetteur', __('emetteur'));
        $form->setLib('date_emission', __('date_emission'));
        $form->setLib('contenu', __('contenu'));
        $form->setLib('contenu_json', __('contenu_json'));
        $form->setLib('si_cadre_lu', __('si_cadre_lu'));
        $form->setLib('si_technicien_lu', __('si_technicien_lu'));
        $form->setLib('si_mode_lecture', __('si_mode_lecture'));
        $form->setLib('acc_cadre_lu', __('acc_cadre_lu'));
        $form->setLib('acc_technicien_lu', __('acc_technicien_lu'));
        $form->setLib('acc_mode_lecture', __('acc_mode_lecture'));
    }
    /**
     *
     */
    function setSelect(&$form, $maj, &$dnu1 = null, $dnu2 = null) {

        // dossier_coordination
        $this->init_select(
            $form, 
            $this->f->db,
            $maj,
            null,
            "dossier_coordination",
            $this->get_var_sql_forminc__sql("dossier_coordination"),
            $this->get_var_sql_forminc__sql("dossier_coordination_by_id"),
            false
        );
    }


    //==================================
    // sous Formulaire
    //==================================
    

    function setValsousformulaire(&$form, $maj, $validation, $idxformulaire, $retourformulaire, $typeformulaire, &$dnu1 = null, $dnu2 = null) {
        $this->retourformulaire = $retourformulaire;
        if($validation == 0) {
            if($this->is_in_context_of_foreign_key('dossier_coordination', $this->retourformulaire))
                $form->setVal('dossier_coordination', $idxformulaire);
        }// fin validation
        $this->set_form_default_values($form, $maj, $validation);
    }// fin setValsousformulaire

    //==================================
    // cle secondaire
    //==================================
    

}
