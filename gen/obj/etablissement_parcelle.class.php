<?php
//$Id$ 
//gen openMairie le 03/05/2018 09:12

require_once "../obj/om_dbform.class.php";

class etablissement_parcelle_gen extends om_dbform {

    protected $_absolute_class_name = "etablissement_parcelle";

    var $table = "etablissement_parcelle";
    var $clePrimaire = "etablissement_parcelle";
    var $typeCle = "N";
    var $required_field = array(
        "etablissement_parcelle"
    );
    
    var $foreign_keys_extended = array(
        "etablissement" => array("etablissement", "etablissement_referentiel_erp", "etablissement_tous", ),
    );
    
    /**
     *
     * @return string
     */
    function get_default_libelle() {
        return $this->getVal($this->clePrimaire)."&nbsp;".$this->getVal("etablissement");
    }

    /**
     *
     * @return array
     */
    function get_var_sql_forminc__champs() {
        return array(
            "etablissement_parcelle",
            "etablissement",
            "ref_cadastre",
        );
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_etablissement() {
        return "SELECT etablissement.etablissement, etablissement.libelle FROM ".DB_PREFIXE."etablissement WHERE ((etablissement.om_validite_debut IS NULL AND (etablissement.om_validite_fin IS NULL OR etablissement.om_validite_fin > CURRENT_DATE)) OR (etablissement.om_validite_debut <= CURRENT_DATE AND (etablissement.om_validite_fin IS NULL OR etablissement.om_validite_fin > CURRENT_DATE))) ORDER BY etablissement.libelle ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_etablissement_by_id() {
        return "SELECT etablissement.etablissement, etablissement.libelle FROM ".DB_PREFIXE."etablissement WHERE etablissement = <idx>";
    }




    function setvalF($val = array()) {
        //affectation valeur formulaire
        if (!is_numeric($val['etablissement_parcelle'])) {
            $this->valF['etablissement_parcelle'] = ""; // -> requis
        } else {
            $this->valF['etablissement_parcelle'] = $val['etablissement_parcelle'];
        }
        if (!is_numeric($val['etablissement'])) {
            $this->valF['etablissement'] = NULL;
        } else {
            $this->valF['etablissement'] = $val['etablissement'];
        }
        if ($val['ref_cadastre'] == "") {
            $this->valF['ref_cadastre'] = NULL;
        } else {
            $this->valF['ref_cadastre'] = $val['ref_cadastre'];
        }
    }

    //=================================================
    //cle primaire automatique [automatic primary key]
    //==================================================

    function setId(&$dnu1 = null) {
    //numero automatique
        $this->valF[$this->clePrimaire] = $this->f->db->nextId(DB_PREFIXE.$this->table);
    }

    function setValFAjout($val = array()) {
    //numero automatique -> pas de controle ajout cle primaire
    }

    function verifierAjout($val = array(), &$dnu1 = null) {
    //numero automatique -> pas de verfication de cle primaire
    }

    //==========================
    // Formulaire  [form]
    //==========================
    /**
     *
     */
    function setType(&$form, $maj) {
        // Récupération du mode de l'action
        $crud = $this->get_action_crud($maj);

        // MODE AJOUTER
        if ($maj == 0 || $crud == 'create') {
            $form->setType("etablissement_parcelle", "hidden");
            if ($this->is_in_context_of_foreign_key("etablissement", $this->retourformulaire)) {
                $form->setType("etablissement", "selecthiddenstatic");
            } else {
                $form->setType("etablissement", "select");
            }
            $form->setType("ref_cadastre", "text");
        }

        // MDOE MODIFIER
        if ($maj == 1 || $crud == 'update') {
            $form->setType("etablissement_parcelle", "hiddenstatic");
            if ($this->is_in_context_of_foreign_key("etablissement", $this->retourformulaire)) {
                $form->setType("etablissement", "selecthiddenstatic");
            } else {
                $form->setType("etablissement", "select");
            }
            $form->setType("ref_cadastre", "text");
        }

        // MODE SUPPRIMER
        if ($maj == 2 || $crud == 'delete') {
            $form->setType("etablissement_parcelle", "hiddenstatic");
            $form->setType("etablissement", "selectstatic");
            $form->setType("ref_cadastre", "hiddenstatic");
        }

        // MODE CONSULTER
        if ($maj == 3 || $crud == 'read') {
            $form->setType("etablissement_parcelle", "static");
            $form->setType("etablissement", "selectstatic");
            $form->setType("ref_cadastre", "static");
        }

    }


    function setOnchange(&$form, $maj) {
    //javascript controle client
        $form->setOnchange('etablissement_parcelle','VerifNum(this)');
        $form->setOnchange('etablissement','VerifNum(this)');
    }
    /**
     * Methode setTaille
     */
    function setTaille(&$form, $maj) {
        $form->setTaille("etablissement_parcelle", 11);
        $form->setTaille("etablissement", 11);
        $form->setTaille("ref_cadastre", 20);
    }

    /**
     * Methode setMax
     */
    function setMax(&$form, $maj) {
        $form->setMax("etablissement_parcelle", 11);
        $form->setMax("etablissement", 11);
        $form->setMax("ref_cadastre", 20);
    }


    function setLib(&$form, $maj) {
    //libelle des champs
        $form->setLib('etablissement_parcelle', __('etablissement_parcelle'));
        $form->setLib('etablissement', __('etablissement'));
        $form->setLib('ref_cadastre', __('ref_cadastre'));
    }
    /**
     *
     */
    function setSelect(&$form, $maj, &$dnu1 = null, $dnu2 = null) {

        // etablissement
        $this->init_select(
            $form, 
            $this->f->db,
            $maj,
            null,
            "etablissement",
            $this->get_var_sql_forminc__sql("etablissement"),
            $this->get_var_sql_forminc__sql("etablissement_by_id"),
            true
        );
    }


    //==================================
    // sous Formulaire
    //==================================
    

    function setValsousformulaire(&$form, $maj, $validation, $idxformulaire, $retourformulaire, $typeformulaire, &$dnu1 = null, $dnu2 = null) {
        $this->retourformulaire = $retourformulaire;
        if($validation == 0) {
            if($this->is_in_context_of_foreign_key('etablissement', $this->retourformulaire))
                $form->setVal('etablissement', $idxformulaire);
        }// fin validation
        $this->set_form_default_values($form, $maj, $validation);
    }// fin setValsousformulaire

    //==================================
    // cle secondaire
    //==================================
    

}
