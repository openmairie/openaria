<?php
//$Id$ 
//gen openMairie le 21/07/2021 15:22

require_once "../obj/om_dbform.class.php";

class reunion_gen extends om_dbform {

    protected $_absolute_class_name = "reunion";

    var $table = "reunion";
    var $clePrimaire = "reunion";
    var $typeCle = "N";
    var $required_field = array(
        "code",
        "date_reunion",
        "libelle",
        "numerotation_simple",
        "reunion",
        "reunion_type"
    );
    
    var $foreign_keys_extended = array(
        "reunion_type" => array("reunion_type", ),
    );
    
    /**
     *
     * @return string
     */
    function get_default_libelle() {
        return $this->getVal($this->clePrimaire)."&nbsp;".$this->getVal("libelle");
    }

    /**
     *
     * @return array
     */
    function get_var_sql_forminc__champs() {
        return array(
            "reunion",
            "code",
            "reunion_type",
            "libelle",
            "date_reunion",
            "heure_reunion",
            "lieu_adresse_ligne1",
            "lieu_adresse_ligne2",
            "lieu_salle",
            "listes_de_diffusion",
            "participants",
            "numerotation_simple",
            "date_convocation",
            "reunion_cloture",
            "date_cloture",
            "om_fichier_reunion_odj",
            "om_final_reunion_odj",
            "om_fichier_reunion_cr_global",
            "om_final_reunion_cr_global",
            "om_fichier_reunion_cr_global_signe",
            "om_fichier_reunion_cr_par_dossier_signe",
            "planifier_nouveau",
            "numerotation_complexe",
            "numerotation_mode",
        );
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_reunion_type() {
        return "SELECT reunion_type.reunion_type, reunion_type.libelle FROM ".DB_PREFIXE."reunion_type WHERE ((reunion_type.om_validite_debut IS NULL AND (reunion_type.om_validite_fin IS NULL OR reunion_type.om_validite_fin > CURRENT_DATE)) OR (reunion_type.om_validite_debut <= CURRENT_DATE AND (reunion_type.om_validite_fin IS NULL OR reunion_type.om_validite_fin > CURRENT_DATE))) ORDER BY reunion_type.libelle ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_reunion_type_by_id() {
        return "SELECT reunion_type.reunion_type, reunion_type.libelle FROM ".DB_PREFIXE."reunion_type WHERE reunion_type = <idx>";
    }




    function setvalF($val = array()) {
        //affectation valeur formulaire
        if (!is_numeric($val['reunion'])) {
            $this->valF['reunion'] = ""; // -> requis
        } else {
            $this->valF['reunion'] = $val['reunion'];
        }
        $this->valF['code'] = $val['code'];
        if (!is_numeric($val['reunion_type'])) {
            $this->valF['reunion_type'] = ""; // -> requis
        } else {
            $this->valF['reunion_type'] = $val['reunion_type'];
        }
        $this->valF['libelle'] = $val['libelle'];
        if ($val['date_reunion'] != "") {
            $this->valF['date_reunion'] = $this->dateDB($val['date_reunion']);
        }
        if ($val['heure_reunion'] == "") {
            $this->valF['heure_reunion'] = NULL;
        } else {
            $this->valF['heure_reunion'] = $val['heure_reunion'];
        }
        if ($val['lieu_adresse_ligne1'] == "") {
            $this->valF['lieu_adresse_ligne1'] = NULL;
        } else {
            $this->valF['lieu_adresse_ligne1'] = $val['lieu_adresse_ligne1'];
        }
        if ($val['lieu_adresse_ligne2'] == "") {
            $this->valF['lieu_adresse_ligne2'] = NULL;
        } else {
            $this->valF['lieu_adresse_ligne2'] = $val['lieu_adresse_ligne2'];
        }
        if ($val['lieu_salle'] == "") {
            $this->valF['lieu_salle'] = NULL;
        } else {
            $this->valF['lieu_salle'] = $val['lieu_salle'];
        }
            $this->valF['listes_de_diffusion'] = $val['listes_de_diffusion'];
            $this->valF['participants'] = $val['participants'];
        if (!is_numeric($val['numerotation_simple'])) {
            $this->valF['numerotation_simple'] = ""; // -> requis
        } else {
            $this->valF['numerotation_simple'] = $val['numerotation_simple'];
        }
        if ($val['date_convocation'] != "") {
            $this->valF['date_convocation'] = $this->dateDB($val['date_convocation']);
        } else {
            $this->valF['date_convocation'] = NULL;
        }
        if ($val['reunion_cloture'] == 1 || $val['reunion_cloture'] == "t" || $val['reunion_cloture'] == "Oui") {
            $this->valF['reunion_cloture'] = true;
        } else {
            $this->valF['reunion_cloture'] = false;
        }
        if ($val['date_cloture'] != "") {
            $this->valF['date_cloture'] = $this->dateDB($val['date_cloture']);
        } else {
            $this->valF['date_cloture'] = NULL;
        }
        if ($val['om_fichier_reunion_odj'] == "") {
            $this->valF['om_fichier_reunion_odj'] = NULL;
        } else {
            $this->valF['om_fichier_reunion_odj'] = $val['om_fichier_reunion_odj'];
        }
        if ($val['om_final_reunion_odj'] == 1 || $val['om_final_reunion_odj'] == "t" || $val['om_final_reunion_odj'] == "Oui") {
            $this->valF['om_final_reunion_odj'] = true;
        } else {
            $this->valF['om_final_reunion_odj'] = false;
        }
        if ($val['om_fichier_reunion_cr_global'] == "") {
            $this->valF['om_fichier_reunion_cr_global'] = NULL;
        } else {
            $this->valF['om_fichier_reunion_cr_global'] = $val['om_fichier_reunion_cr_global'];
        }
        if ($val['om_final_reunion_cr_global'] == 1 || $val['om_final_reunion_cr_global'] == "t" || $val['om_final_reunion_cr_global'] == "Oui") {
            $this->valF['om_final_reunion_cr_global'] = true;
        } else {
            $this->valF['om_final_reunion_cr_global'] = false;
        }
        if ($val['om_fichier_reunion_cr_global_signe'] == "") {
            $this->valF['om_fichier_reunion_cr_global_signe'] = NULL;
        } else {
            $this->valF['om_fichier_reunion_cr_global_signe'] = $val['om_fichier_reunion_cr_global_signe'];
        }
        if ($val['om_fichier_reunion_cr_par_dossier_signe'] == "") {
            $this->valF['om_fichier_reunion_cr_par_dossier_signe'] = NULL;
        } else {
            $this->valF['om_fichier_reunion_cr_par_dossier_signe'] = $val['om_fichier_reunion_cr_par_dossier_signe'];
        }
        if ($val['planifier_nouveau'] == 1 || $val['planifier_nouveau'] == "t" || $val['planifier_nouveau'] == "Oui") {
            $this->valF['planifier_nouveau'] = true;
        } else {
            $this->valF['planifier_nouveau'] = false;
        }
            $this->valF['numerotation_complexe'] = $val['numerotation_complexe'];
        if ($val['numerotation_mode'] == "") {
            $this->valF['numerotation_mode'] = NULL;
        } else {
            $this->valF['numerotation_mode'] = $val['numerotation_mode'];
        }
    }

    //=================================================
    //cle primaire automatique [automatic primary key]
    //==================================================

    function setId(&$dnu1 = null) {
    //numero automatique
        $this->valF[$this->clePrimaire] = $this->f->db->nextId(DB_PREFIXE.$this->table);
    }

    function setValFAjout($val = array()) {
    //numero automatique -> pas de controle ajout cle primaire
    }

    function verifierAjout($val = array(), &$dnu1 = null) {
    //numero automatique -> pas de verfication de cle primaire
    }

    //==========================
    // Formulaire  [form]
    //==========================
    /**
     *
     */
    function setType(&$form, $maj) {
        // Récupération du mode de l'action
        $crud = $this->get_action_crud($maj);

        // MODE AJOUTER
        if ($maj == 0 || $crud == 'create') {
            $form->setType("reunion", "hidden");
            $form->setType("code", "text");
            if ($this->is_in_context_of_foreign_key("reunion_type", $this->retourformulaire)) {
                $form->setType("reunion_type", "selecthiddenstatic");
            } else {
                $form->setType("reunion_type", "select");
            }
            $form->setType("libelle", "text");
            $form->setType("date_reunion", "date");
            $form->setType("heure_reunion", "text");
            $form->setType("lieu_adresse_ligne1", "text");
            $form->setType("lieu_adresse_ligne2", "text");
            $form->setType("lieu_salle", "text");
            $form->setType("listes_de_diffusion", "textarea");
            $form->setType("participants", "textarea");
            $form->setType("numerotation_simple", "text");
            $form->setType("date_convocation", "date");
            $form->setType("reunion_cloture", "checkbox");
            $form->setType("date_cloture", "date");
            $form->setType("om_fichier_reunion_odj", "text");
            $form->setType("om_final_reunion_odj", "checkbox");
            $form->setType("om_fichier_reunion_cr_global", "text");
            $form->setType("om_final_reunion_cr_global", "checkbox");
            $form->setType("om_fichier_reunion_cr_global_signe", "text");
            $form->setType("om_fichier_reunion_cr_par_dossier_signe", "text");
            $form->setType("planifier_nouveau", "checkbox");
            $form->setType("numerotation_complexe", "textarea");
            $form->setType("numerotation_mode", "text");
        }

        // MDOE MODIFIER
        if ($maj == 1 || $crud == 'update') {
            $form->setType("reunion", "hiddenstatic");
            $form->setType("code", "text");
            if ($this->is_in_context_of_foreign_key("reunion_type", $this->retourformulaire)) {
                $form->setType("reunion_type", "selecthiddenstatic");
            } else {
                $form->setType("reunion_type", "select");
            }
            $form->setType("libelle", "text");
            $form->setType("date_reunion", "date");
            $form->setType("heure_reunion", "text");
            $form->setType("lieu_adresse_ligne1", "text");
            $form->setType("lieu_adresse_ligne2", "text");
            $form->setType("lieu_salle", "text");
            $form->setType("listes_de_diffusion", "textarea");
            $form->setType("participants", "textarea");
            $form->setType("numerotation_simple", "text");
            $form->setType("date_convocation", "date");
            $form->setType("reunion_cloture", "checkbox");
            $form->setType("date_cloture", "date");
            $form->setType("om_fichier_reunion_odj", "text");
            $form->setType("om_final_reunion_odj", "checkbox");
            $form->setType("om_fichier_reunion_cr_global", "text");
            $form->setType("om_final_reunion_cr_global", "checkbox");
            $form->setType("om_fichier_reunion_cr_global_signe", "text");
            $form->setType("om_fichier_reunion_cr_par_dossier_signe", "text");
            $form->setType("planifier_nouveau", "checkbox");
            $form->setType("numerotation_complexe", "textarea");
            $form->setType("numerotation_mode", "text");
        }

        // MODE SUPPRIMER
        if ($maj == 2 || $crud == 'delete') {
            $form->setType("reunion", "hiddenstatic");
            $form->setType("code", "hiddenstatic");
            $form->setType("reunion_type", "selectstatic");
            $form->setType("libelle", "hiddenstatic");
            $form->setType("date_reunion", "hiddenstatic");
            $form->setType("heure_reunion", "hiddenstatic");
            $form->setType("lieu_adresse_ligne1", "hiddenstatic");
            $form->setType("lieu_adresse_ligne2", "hiddenstatic");
            $form->setType("lieu_salle", "hiddenstatic");
            $form->setType("listes_de_diffusion", "hiddenstatic");
            $form->setType("participants", "hiddenstatic");
            $form->setType("numerotation_simple", "hiddenstatic");
            $form->setType("date_convocation", "hiddenstatic");
            $form->setType("reunion_cloture", "hiddenstatic");
            $form->setType("date_cloture", "hiddenstatic");
            $form->setType("om_fichier_reunion_odj", "hiddenstatic");
            $form->setType("om_final_reunion_odj", "hiddenstatic");
            $form->setType("om_fichier_reunion_cr_global", "hiddenstatic");
            $form->setType("om_final_reunion_cr_global", "hiddenstatic");
            $form->setType("om_fichier_reunion_cr_global_signe", "hiddenstatic");
            $form->setType("om_fichier_reunion_cr_par_dossier_signe", "hiddenstatic");
            $form->setType("planifier_nouveau", "hiddenstatic");
            $form->setType("numerotation_complexe", "hiddenstatic");
            $form->setType("numerotation_mode", "hiddenstatic");
        }

        // MODE CONSULTER
        if ($maj == 3 || $crud == 'read') {
            $form->setType("reunion", "static");
            $form->setType("code", "static");
            $form->setType("reunion_type", "selectstatic");
            $form->setType("libelle", "static");
            $form->setType("date_reunion", "datestatic");
            $form->setType("heure_reunion", "static");
            $form->setType("lieu_adresse_ligne1", "static");
            $form->setType("lieu_adresse_ligne2", "static");
            $form->setType("lieu_salle", "static");
            $form->setType("listes_de_diffusion", "textareastatic");
            $form->setType("participants", "textareastatic");
            $form->setType("numerotation_simple", "static");
            $form->setType("date_convocation", "datestatic");
            $form->setType("reunion_cloture", "checkboxstatic");
            $form->setType("date_cloture", "datestatic");
            $form->setType("om_fichier_reunion_odj", "static");
            $form->setType("om_final_reunion_odj", "checkboxstatic");
            $form->setType("om_fichier_reunion_cr_global", "static");
            $form->setType("om_final_reunion_cr_global", "checkboxstatic");
            $form->setType("om_fichier_reunion_cr_global_signe", "static");
            $form->setType("om_fichier_reunion_cr_par_dossier_signe", "static");
            $form->setType("planifier_nouveau", "checkboxstatic");
            $form->setType("numerotation_complexe", "textareastatic");
            $form->setType("numerotation_mode", "static");
        }

    }


    function setOnchange(&$form, $maj) {
    //javascript controle client
        $form->setOnchange('reunion','VerifNum(this)');
        $form->setOnchange('reunion_type','VerifNum(this)');
        $form->setOnchange('date_reunion','fdate(this)');
        $form->setOnchange('numerotation_simple','VerifNum(this)');
        $form->setOnchange('date_convocation','fdate(this)');
        $form->setOnchange('date_cloture','fdate(this)');
    }
    /**
     * Methode setTaille
     */
    function setTaille(&$form, $maj) {
        $form->setTaille("reunion", 11);
        $form->setTaille("code", 21);
        $form->setTaille("reunion_type", 11);
        $form->setTaille("libelle", 30);
        $form->setTaille("date_reunion", 12);
        $form->setTaille("heure_reunion", 10);
        $form->setTaille("lieu_adresse_ligne1", 30);
        $form->setTaille("lieu_adresse_ligne2", 30);
        $form->setTaille("lieu_salle", 30);
        $form->setTaille("listes_de_diffusion", 80);
        $form->setTaille("participants", 80);
        $form->setTaille("numerotation_simple", 11);
        $form->setTaille("date_convocation", 12);
        $form->setTaille("reunion_cloture", 1);
        $form->setTaille("date_cloture", 12);
        $form->setTaille("om_fichier_reunion_odj", 30);
        $form->setTaille("om_final_reunion_odj", 1);
        $form->setTaille("om_fichier_reunion_cr_global", 30);
        $form->setTaille("om_final_reunion_cr_global", 1);
        $form->setTaille("om_fichier_reunion_cr_global_signe", 30);
        $form->setTaille("om_fichier_reunion_cr_par_dossier_signe", 30);
        $form->setTaille("planifier_nouveau", 1);
        $form->setTaille("numerotation_complexe", 80);
        $form->setTaille("numerotation_mode", 30);
    }

    /**
     * Methode setMax
     */
    function setMax(&$form, $maj) {
        $form->setMax("reunion", 11);
        $form->setMax("code", 21);
        $form->setMax("reunion_type", 11);
        $form->setMax("libelle", 100);
        $form->setMax("date_reunion", 12);
        $form->setMax("heure_reunion", 5);
        $form->setMax("lieu_adresse_ligne1", 100);
        $form->setMax("lieu_adresse_ligne2", 100);
        $form->setMax("lieu_salle", 100);
        $form->setMax("listes_de_diffusion", 6);
        $form->setMax("participants", 6);
        $form->setMax("numerotation_simple", 11);
        $form->setMax("date_convocation", 12);
        $form->setMax("reunion_cloture", 1);
        $form->setMax("date_cloture", 12);
        $form->setMax("om_fichier_reunion_odj", 64);
        $form->setMax("om_final_reunion_odj", 1);
        $form->setMax("om_fichier_reunion_cr_global", 64);
        $form->setMax("om_final_reunion_cr_global", 1);
        $form->setMax("om_fichier_reunion_cr_global_signe", 64);
        $form->setMax("om_fichier_reunion_cr_par_dossier_signe", 64);
        $form->setMax("planifier_nouveau", 1);
        $form->setMax("numerotation_complexe", 6);
        $form->setMax("numerotation_mode", 30);
    }


    function setLib(&$form, $maj) {
    //libelle des champs
        $form->setLib('reunion', __('reunion'));
        $form->setLib('code', __('code'));
        $form->setLib('reunion_type', __('reunion_type'));
        $form->setLib('libelle', __('libelle'));
        $form->setLib('date_reunion', __('date_reunion'));
        $form->setLib('heure_reunion', __('heure_reunion'));
        $form->setLib('lieu_adresse_ligne1', __('lieu_adresse_ligne1'));
        $form->setLib('lieu_adresse_ligne2', __('lieu_adresse_ligne2'));
        $form->setLib('lieu_salle', __('lieu_salle'));
        $form->setLib('listes_de_diffusion', __('listes_de_diffusion'));
        $form->setLib('participants', __('participants'));
        $form->setLib('numerotation_simple', __('numerotation_simple'));
        $form->setLib('date_convocation', __('date_convocation'));
        $form->setLib('reunion_cloture', __('reunion_cloture'));
        $form->setLib('date_cloture', __('date_cloture'));
        $form->setLib('om_fichier_reunion_odj', __('om_fichier_reunion_odj'));
        $form->setLib('om_final_reunion_odj', __('om_final_reunion_odj'));
        $form->setLib('om_fichier_reunion_cr_global', __('om_fichier_reunion_cr_global'));
        $form->setLib('om_final_reunion_cr_global', __('om_final_reunion_cr_global'));
        $form->setLib('om_fichier_reunion_cr_global_signe', __('om_fichier_reunion_cr_global_signe'));
        $form->setLib('om_fichier_reunion_cr_par_dossier_signe', __('om_fichier_reunion_cr_par_dossier_signe'));
        $form->setLib('planifier_nouveau', __('planifier_nouveau'));
        $form->setLib('numerotation_complexe', __('numerotation_complexe'));
        $form->setLib('numerotation_mode', __('numerotation_mode'));
    }
    /**
     *
     */
    function setSelect(&$form, $maj, &$dnu1 = null, $dnu2 = null) {

        // reunion_type
        $this->init_select(
            $form, 
            $this->f->db,
            $maj,
            null,
            "reunion_type",
            $this->get_var_sql_forminc__sql("reunion_type"),
            $this->get_var_sql_forminc__sql("reunion_type_by_id"),
            true
        );
    }


    //==================================
    // sous Formulaire
    //==================================
    

    function setValsousformulaire(&$form, $maj, $validation, $idxformulaire, $retourformulaire, $typeformulaire, &$dnu1 = null, $dnu2 = null) {
        $this->retourformulaire = $retourformulaire;
        if($validation == 0) {
            if($this->is_in_context_of_foreign_key('reunion_type', $this->retourformulaire))
                $form->setVal('reunion_type', $idxformulaire);
        }// fin validation
        $this->set_form_default_values($form, $maj, $validation);
    }// fin setValsousformulaire

    //==================================
    // cle secondaire
    //==================================
    
    /**
     * Methode clesecondaire
     */
    function cleSecondaire($id, &$dnu1 = null, $val = array(), $dnu2 = null) {
        // On appelle la methode de la classe parent
        parent::cleSecondaire($id);
        // Verification de la cle secondaire : dossier_instruction_reunion
        $this->rechercheTable($this->f->db, "dossier_instruction_reunion", "reunion", $id);
        // Verification de la cle secondaire : lien_reunion_r_instance_r_i_membre
        $this->rechercheTable($this->f->db, "lien_reunion_r_instance_r_i_membre", "reunion", $id);
    }


}
