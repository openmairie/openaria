<?php
//$Id$ 
//gen openMairie le 03/05/2018 09:12

require_once "../obj/om_dbform.class.php";

class acteur_conge_gen extends om_dbform {

    protected $_absolute_class_name = "acteur_conge";

    var $table = "acteur_conge";
    var $clePrimaire = "acteur_conge";
    var $typeCle = "N";
    var $required_field = array(
        "acteur",
        "acteur_conge"
    );
    
    var $foreign_keys_extended = array(
        "acteur" => array("acteur", ),
    );
    
    /**
     *
     * @return string
     */
    function get_default_libelle() {
        return $this->getVal($this->clePrimaire)."&nbsp;".$this->getVal("acteur");
    }

    /**
     *
     * @return array
     */
    function get_var_sql_forminc__champs() {
        return array(
            "acteur_conge",
            "acteur",
            "date_debut",
            "heure_debut",
            "date_fin",
            "heure_fin",
        );
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_acteur() {
        return "SELECT acteur.acteur, acteur.nom_prenom FROM ".DB_PREFIXE."acteur WHERE ((acteur.om_validite_debut IS NULL AND (acteur.om_validite_fin IS NULL OR acteur.om_validite_fin > CURRENT_DATE)) OR (acteur.om_validite_debut <= CURRENT_DATE AND (acteur.om_validite_fin IS NULL OR acteur.om_validite_fin > CURRENT_DATE))) ORDER BY acteur.nom_prenom ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_acteur_by_id() {
        return "SELECT acteur.acteur, acteur.nom_prenom FROM ".DB_PREFIXE."acteur WHERE acteur = <idx>";
    }




    function setvalF($val = array()) {
        //affectation valeur formulaire
        if (!is_numeric($val['acteur_conge'])) {
            $this->valF['acteur_conge'] = ""; // -> requis
        } else {
            $this->valF['acteur_conge'] = $val['acteur_conge'];
        }
        if (!is_numeric($val['acteur'])) {
            $this->valF['acteur'] = ""; // -> requis
        } else {
            $this->valF['acteur'] = $val['acteur'];
        }
        if ($val['date_debut'] != "") {
            $this->valF['date_debut'] = $this->dateDB($val['date_debut']);
        } else {
            $this->valF['date_debut'] = NULL;
        }
        if ($val['heure_debut'] == "") {
            $this->valF['heure_debut'] = NULL;
        } else {
            $this->valF['heure_debut'] = $val['heure_debut'];
        }
        if ($val['date_fin'] != "") {
            $this->valF['date_fin'] = $this->dateDB($val['date_fin']);
        } else {
            $this->valF['date_fin'] = NULL;
        }
        if ($val['heure_fin'] == "") {
            $this->valF['heure_fin'] = NULL;
        } else {
            $this->valF['heure_fin'] = $val['heure_fin'];
        }
    }

    //=================================================
    //cle primaire automatique [automatic primary key]
    //==================================================

    function setId(&$dnu1 = null) {
    //numero automatique
        $this->valF[$this->clePrimaire] = $this->f->db->nextId(DB_PREFIXE.$this->table);
    }

    function setValFAjout($val = array()) {
    //numero automatique -> pas de controle ajout cle primaire
    }

    function verifierAjout($val = array(), &$dnu1 = null) {
    //numero automatique -> pas de verfication de cle primaire
    }

    //==========================
    // Formulaire  [form]
    //==========================
    /**
     *
     */
    function setType(&$form, $maj) {
        // Récupération du mode de l'action
        $crud = $this->get_action_crud($maj);

        // MODE AJOUTER
        if ($maj == 0 || $crud == 'create') {
            $form->setType("acteur_conge", "hidden");
            if ($this->is_in_context_of_foreign_key("acteur", $this->retourformulaire)) {
                $form->setType("acteur", "selecthiddenstatic");
            } else {
                $form->setType("acteur", "select");
            }
            $form->setType("date_debut", "date");
            $form->setType("heure_debut", "text");
            $form->setType("date_fin", "date");
            $form->setType("heure_fin", "text");
        }

        // MDOE MODIFIER
        if ($maj == 1 || $crud == 'update') {
            $form->setType("acteur_conge", "hiddenstatic");
            if ($this->is_in_context_of_foreign_key("acteur", $this->retourformulaire)) {
                $form->setType("acteur", "selecthiddenstatic");
            } else {
                $form->setType("acteur", "select");
            }
            $form->setType("date_debut", "date");
            $form->setType("heure_debut", "text");
            $form->setType("date_fin", "date");
            $form->setType("heure_fin", "text");
        }

        // MODE SUPPRIMER
        if ($maj == 2 || $crud == 'delete') {
            $form->setType("acteur_conge", "hiddenstatic");
            $form->setType("acteur", "selectstatic");
            $form->setType("date_debut", "hiddenstatic");
            $form->setType("heure_debut", "hiddenstatic");
            $form->setType("date_fin", "hiddenstatic");
            $form->setType("heure_fin", "hiddenstatic");
        }

        // MODE CONSULTER
        if ($maj == 3 || $crud == 'read') {
            $form->setType("acteur_conge", "static");
            $form->setType("acteur", "selectstatic");
            $form->setType("date_debut", "datestatic");
            $form->setType("heure_debut", "static");
            $form->setType("date_fin", "datestatic");
            $form->setType("heure_fin", "static");
        }

    }


    function setOnchange(&$form, $maj) {
    //javascript controle client
        $form->setOnchange('acteur_conge','VerifNum(this)');
        $form->setOnchange('acteur','VerifNum(this)');
        $form->setOnchange('date_debut','fdate(this)');
        $form->setOnchange('date_fin','fdate(this)');
    }
    /**
     * Methode setTaille
     */
    function setTaille(&$form, $maj) {
        $form->setTaille("acteur_conge", 11);
        $form->setTaille("acteur", 11);
        $form->setTaille("date_debut", 12);
        $form->setTaille("heure_debut", 10);
        $form->setTaille("date_fin", 12);
        $form->setTaille("heure_fin", 10);
    }

    /**
     * Methode setMax
     */
    function setMax(&$form, $maj) {
        $form->setMax("acteur_conge", 11);
        $form->setMax("acteur", 11);
        $form->setMax("date_debut", 12);
        $form->setMax("heure_debut", 5);
        $form->setMax("date_fin", 12);
        $form->setMax("heure_fin", 5);
    }


    function setLib(&$form, $maj) {
    //libelle des champs
        $form->setLib('acteur_conge', __('acteur_conge'));
        $form->setLib('acteur', __('acteur'));
        $form->setLib('date_debut', __('date_debut'));
        $form->setLib('heure_debut', __('heure_debut'));
        $form->setLib('date_fin', __('date_fin'));
        $form->setLib('heure_fin', __('heure_fin'));
    }
    /**
     *
     */
    function setSelect(&$form, $maj, &$dnu1 = null, $dnu2 = null) {

        // acteur
        $this->init_select(
            $form, 
            $this->f->db,
            $maj,
            null,
            "acteur",
            $this->get_var_sql_forminc__sql("acteur"),
            $this->get_var_sql_forminc__sql("acteur_by_id"),
            true
        );
    }


    //==================================
    // sous Formulaire
    //==================================
    

    function setValsousformulaire(&$form, $maj, $validation, $idxformulaire, $retourformulaire, $typeformulaire, &$dnu1 = null, $dnu2 = null) {
        $this->retourformulaire = $retourformulaire;
        if($validation == 0) {
            if($this->is_in_context_of_foreign_key('acteur', $this->retourformulaire))
                $form->setVal('acteur', $idxformulaire);
        }// fin validation
        $this->set_form_default_values($form, $maj, $validation);
    }// fin setValsousformulaire

    //==================================
    // cle secondaire
    //==================================
    

}
