<?php
//$Id$ 
//gen openMairie le 03/05/2018 09:12

require_once "../obj/om_dbform.class.php";

class voie_arrondissement_gen extends om_dbform {

    protected $_absolute_class_name = "voie_arrondissement";

    var $table = "voie_arrondissement";
    var $clePrimaire = "voie_arrondissement";
    var $typeCle = "N";
    var $required_field = array(
        "voie_arrondissement"
    );
    var $unique_key = array(
      array("arrondissement","voie"),
    );
    var $foreign_keys_extended = array(
        "arrondissement" => array("arrondissement", ),
        "voie" => array("voie", ),
    );
    
    /**
     *
     * @return string
     */
    function get_default_libelle() {
        return $this->getVal($this->clePrimaire)."&nbsp;".$this->getVal("voie");
    }

    /**
     *
     * @return array
     */
    function get_var_sql_forminc__champs() {
        return array(
            "voie_arrondissement",
            "voie",
            "arrondissement",
        );
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_arrondissement() {
        return "SELECT arrondissement.arrondissement, arrondissement.libelle FROM ".DB_PREFIXE."arrondissement WHERE ((arrondissement.om_validite_debut IS NULL AND (arrondissement.om_validite_fin IS NULL OR arrondissement.om_validite_fin > CURRENT_DATE)) OR (arrondissement.om_validite_debut <= CURRENT_DATE AND (arrondissement.om_validite_fin IS NULL OR arrondissement.om_validite_fin > CURRENT_DATE))) ORDER BY arrondissement.libelle ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_arrondissement_by_id() {
        return "SELECT arrondissement.arrondissement, arrondissement.libelle FROM ".DB_PREFIXE."arrondissement WHERE arrondissement = <idx>";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_voie() {
        return "SELECT voie.voie, voie.libelle FROM ".DB_PREFIXE."voie WHERE ((voie.om_validite_debut IS NULL AND (voie.om_validite_fin IS NULL OR voie.om_validite_fin > CURRENT_DATE)) OR (voie.om_validite_debut <= CURRENT_DATE AND (voie.om_validite_fin IS NULL OR voie.om_validite_fin > CURRENT_DATE))) ORDER BY voie.libelle ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_voie_by_id() {
        return "SELECT voie.voie, voie.libelle FROM ".DB_PREFIXE."voie WHERE voie = <idx>";
    }




    function setvalF($val = array()) {
        //affectation valeur formulaire
        if (!is_numeric($val['voie_arrondissement'])) {
            $this->valF['voie_arrondissement'] = ""; // -> requis
        } else {
            $this->valF['voie_arrondissement'] = $val['voie_arrondissement'];
        }
        if (!is_numeric($val['voie'])) {
            $this->valF['voie'] = NULL;
        } else {
            $this->valF['voie'] = $val['voie'];
        }
        if (!is_numeric($val['arrondissement'])) {
            $this->valF['arrondissement'] = NULL;
        } else {
            $this->valF['arrondissement'] = $val['arrondissement'];
        }
    }

    //=================================================
    //cle primaire automatique [automatic primary key]
    //==================================================

    function setId(&$dnu1 = null) {
    //numero automatique
        $this->valF[$this->clePrimaire] = $this->f->db->nextId(DB_PREFIXE.$this->table);
    }

    function setValFAjout($val = array()) {
    //numero automatique -> pas de controle ajout cle primaire
    }

    function verifierAjout($val = array(), &$dnu1 = null) {
    //numero automatique -> pas de verfication de cle primaire
    }

    //==========================
    // Formulaire  [form]
    //==========================
    /**
     *
     */
    function setType(&$form, $maj) {
        // Récupération du mode de l'action
        $crud = $this->get_action_crud($maj);

        // MODE AJOUTER
        if ($maj == 0 || $crud == 'create') {
            $form->setType("voie_arrondissement", "hidden");
            if ($this->is_in_context_of_foreign_key("voie", $this->retourformulaire)) {
                $form->setType("voie", "selecthiddenstatic");
            } else {
                $form->setType("voie", "select");
            }
            if ($this->is_in_context_of_foreign_key("arrondissement", $this->retourformulaire)) {
                $form->setType("arrondissement", "selecthiddenstatic");
            } else {
                $form->setType("arrondissement", "select");
            }
        }

        // MDOE MODIFIER
        if ($maj == 1 || $crud == 'update') {
            $form->setType("voie_arrondissement", "hiddenstatic");
            if ($this->is_in_context_of_foreign_key("voie", $this->retourformulaire)) {
                $form->setType("voie", "selecthiddenstatic");
            } else {
                $form->setType("voie", "select");
            }
            if ($this->is_in_context_of_foreign_key("arrondissement", $this->retourformulaire)) {
                $form->setType("arrondissement", "selecthiddenstatic");
            } else {
                $form->setType("arrondissement", "select");
            }
        }

        // MODE SUPPRIMER
        if ($maj == 2 || $crud == 'delete') {
            $form->setType("voie_arrondissement", "hiddenstatic");
            $form->setType("voie", "selectstatic");
            $form->setType("arrondissement", "selectstatic");
        }

        // MODE CONSULTER
        if ($maj == 3 || $crud == 'read') {
            $form->setType("voie_arrondissement", "static");
            $form->setType("voie", "selectstatic");
            $form->setType("arrondissement", "selectstatic");
        }

    }


    function setOnchange(&$form, $maj) {
    //javascript controle client
        $form->setOnchange('voie_arrondissement','VerifNum(this)');
        $form->setOnchange('voie','VerifNum(this)');
        $form->setOnchange('arrondissement','VerifNum(this)');
    }
    /**
     * Methode setTaille
     */
    function setTaille(&$form, $maj) {
        $form->setTaille("voie_arrondissement", 11);
        $form->setTaille("voie", 11);
        $form->setTaille("arrondissement", 11);
    }

    /**
     * Methode setMax
     */
    function setMax(&$form, $maj) {
        $form->setMax("voie_arrondissement", 11);
        $form->setMax("voie", 11);
        $form->setMax("arrondissement", 11);
    }


    function setLib(&$form, $maj) {
    //libelle des champs
        $form->setLib('voie_arrondissement', __('voie_arrondissement'));
        $form->setLib('voie', __('voie'));
        $form->setLib('arrondissement', __('arrondissement'));
    }
    /**
     *
     */
    function setSelect(&$form, $maj, &$dnu1 = null, $dnu2 = null) {

        // arrondissement
        $this->init_select(
            $form, 
            $this->f->db,
            $maj,
            null,
            "arrondissement",
            $this->get_var_sql_forminc__sql("arrondissement"),
            $this->get_var_sql_forminc__sql("arrondissement_by_id"),
            true
        );
        // voie
        $this->init_select(
            $form, 
            $this->f->db,
            $maj,
            null,
            "voie",
            $this->get_var_sql_forminc__sql("voie"),
            $this->get_var_sql_forminc__sql("voie_by_id"),
            true
        );
    }


    //==================================
    // sous Formulaire
    //==================================
    

    function setValsousformulaire(&$form, $maj, $validation, $idxformulaire, $retourformulaire, $typeformulaire, &$dnu1 = null, $dnu2 = null) {
        $this->retourformulaire = $retourformulaire;
        if($validation == 0) {
            if($this->is_in_context_of_foreign_key('arrondissement', $this->retourformulaire))
                $form->setVal('arrondissement', $idxformulaire);
            if($this->is_in_context_of_foreign_key('voie', $this->retourformulaire))
                $form->setVal('voie', $idxformulaire);
        }// fin validation
        $this->set_form_default_values($form, $maj, $validation);
    }// fin setValsousformulaire

    //==================================
    // cle secondaire
    //==================================
    

}
