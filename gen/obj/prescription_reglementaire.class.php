<?php
//$Id$ 
//gen openMairie le 03/05/2018 09:12

require_once "../obj/om_dbform.class.php";

class prescription_reglementaire_gen extends om_dbform {

    protected $_absolute_class_name = "prescription_reglementaire";

    var $table = "prescription_reglementaire";
    var $clePrimaire = "prescription_reglementaire";
    var $typeCle = "N";
    var $required_field = array(
        "prescription_reglementaire",
        "service"
    );
    
    var $foreign_keys_extended = array(
        "service" => array("service", ),
    );
    
    /**
     *
     * @return string
     */
    function get_default_libelle() {
        return $this->getVal($this->clePrimaire)."&nbsp;".$this->getVal("libelle");
    }

    /**
     *
     * @return array
     */
    function get_var_sql_forminc__champs() {
        return array(
            "prescription_reglementaire",
            "service",
            "tete_de_chapitre1",
            "tete_de_chapitre2",
            "libelle",
            "description_pr_om_html",
            "defavorable",
            "om_validite_debut",
            "om_validite_fin",
        );
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_service() {
        return "SELECT service.service, service.libelle FROM ".DB_PREFIXE."service WHERE ((service.om_validite_debut IS NULL AND (service.om_validite_fin IS NULL OR service.om_validite_fin > CURRENT_DATE)) OR (service.om_validite_debut <= CURRENT_DATE AND (service.om_validite_fin IS NULL OR service.om_validite_fin > CURRENT_DATE))) ORDER BY service.libelle ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_service_by_id() {
        return "SELECT service.service, service.libelle FROM ".DB_PREFIXE."service WHERE service = <idx>";
    }




    function setvalF($val = array()) {
        //affectation valeur formulaire
        if (!is_numeric($val['prescription_reglementaire'])) {
            $this->valF['prescription_reglementaire'] = ""; // -> requis
        } else {
            $this->valF['prescription_reglementaire'] = $val['prescription_reglementaire'];
        }
        if (!is_numeric($val['service'])) {
            $this->valF['service'] = ""; // -> requis
        } else {
            $this->valF['service'] = $val['service'];
        }
        if ($val['tete_de_chapitre1'] == "") {
            $this->valF['tete_de_chapitre1'] = NULL;
        } else {
            $this->valF['tete_de_chapitre1'] = $val['tete_de_chapitre1'];
        }
        if ($val['tete_de_chapitre2'] == "") {
            $this->valF['tete_de_chapitre2'] = NULL;
        } else {
            $this->valF['tete_de_chapitre2'] = $val['tete_de_chapitre2'];
        }
        if ($val['libelle'] == "") {
            $this->valF['libelle'] = NULL;
        } else {
            $this->valF['libelle'] = $val['libelle'];
        }
            $this->valF['description_pr_om_html'] = $val['description_pr_om_html'];
        if ($val['defavorable'] == 1 || $val['defavorable'] == "t" || $val['defavorable'] == "Oui") {
            $this->valF['defavorable'] = true;
        } else {
            $this->valF['defavorable'] = false;
        }
        if ($val['om_validite_debut'] != "") {
            $this->valF['om_validite_debut'] = $this->dateDB($val['om_validite_debut']);
        } else {
            $this->valF['om_validite_debut'] = NULL;
        }
        if ($val['om_validite_fin'] != "") {
            $this->valF['om_validite_fin'] = $this->dateDB($val['om_validite_fin']);
        } else {
            $this->valF['om_validite_fin'] = NULL;
        }
    }

    //=================================================
    //cle primaire automatique [automatic primary key]
    //==================================================

    function setId(&$dnu1 = null) {
    //numero automatique
        $this->valF[$this->clePrimaire] = $this->f->db->nextId(DB_PREFIXE.$this->table);
    }

    function setValFAjout($val = array()) {
    //numero automatique -> pas de controle ajout cle primaire
    }

    function verifierAjout($val = array(), &$dnu1 = null) {
    //numero automatique -> pas de verfication de cle primaire
    }
    /**
     * Methode verifier
     */
    function verifier($val = array(), &$dnu1 = null, $dnu2 = null) {
        // On appelle la methode de la classe parent
        parent::verifier($val, $this->f->db, null);

        // gestion des dates de validites
        $date_debut = $this->valF['om_validite_debut'];
        $date_fin = $this->valF['om_validite_fin'];

        if ($date_debut != '' and $date_fin != '') {
        
            $date_debut = explode('-', $this->valF['om_validite_debut']);
            $date_fin = explode('-', $this->valF['om_validite_fin']);

            $time_debut = mktime(0, 0, 0, $date_debut[1], $date_debut[2],
                                 $date_debut[0]);
            $time_fin = mktime(0, 0, 0, $date_fin[1], $date_fin[2],
                                 $date_fin[0]);

            if ($time_debut > $time_fin or $time_debut == $time_fin) {
                $this->correct = false;
                $this->addToMessage(__('La date de fin de validite doit etre future a la de debut de validite.'));
            }
        }
    }


    //==========================
    // Formulaire  [form]
    //==========================
    /**
     *
     */
    function setType(&$form, $maj) {
        // Récupération du mode de l'action
        $crud = $this->get_action_crud($maj);

        // MODE AJOUTER
        if ($maj == 0 || $crud == 'create') {
            $form->setType("prescription_reglementaire", "hidden");
            if ($this->is_in_context_of_foreign_key("service", $this->retourformulaire)) {
                $form->setType("service", "selecthiddenstatic");
            } else {
                $form->setType("service", "select");
            }
            $form->setType("tete_de_chapitre1", "text");
            $form->setType("tete_de_chapitre2", "text");
            $form->setType("libelle", "text");
            $form->setType("description_pr_om_html", "html");
            $form->setType("defavorable", "checkbox");
            if ($this->f->isAccredited(array($this->table."_modifier_validite", $this->table, ))) {
                $form->setType("om_validite_debut", "date");
            } else {
                $form->setType("om_validite_debut", "hiddenstaticdate");
            }
            if ($this->f->isAccredited(array($this->table."_modifier_validite", $this->table, ))) {
                $form->setType("om_validite_fin", "date");
            } else {
                $form->setType("om_validite_fin", "hiddenstaticdate");
            }
        }

        // MDOE MODIFIER
        if ($maj == 1 || $crud == 'update') {
            $form->setType("prescription_reglementaire", "hiddenstatic");
            if ($this->is_in_context_of_foreign_key("service", $this->retourformulaire)) {
                $form->setType("service", "selecthiddenstatic");
            } else {
                $form->setType("service", "select");
            }
            $form->setType("tete_de_chapitre1", "text");
            $form->setType("tete_de_chapitre2", "text");
            $form->setType("libelle", "text");
            $form->setType("description_pr_om_html", "html");
            $form->setType("defavorable", "checkbox");
            if ($this->f->isAccredited(array($this->table."_modifier_validite", $this->table, ))) {
                $form->setType("om_validite_debut", "date");
            } else {
                $form->setType("om_validite_debut", "hiddenstaticdate");
            }
            if ($this->f->isAccredited(array($this->table."_modifier_validite", $this->table, ))) {
                $form->setType("om_validite_fin", "date");
            } else {
                $form->setType("om_validite_fin", "hiddenstaticdate");
            }
        }

        // MODE SUPPRIMER
        if ($maj == 2 || $crud == 'delete') {
            $form->setType("prescription_reglementaire", "hiddenstatic");
            $form->setType("service", "selectstatic");
            $form->setType("tete_de_chapitre1", "hiddenstatic");
            $form->setType("tete_de_chapitre2", "hiddenstatic");
            $form->setType("libelle", "hiddenstatic");
            $form->setType("description_pr_om_html", "hiddenstatic");
            $form->setType("defavorable", "hiddenstatic");
            $form->setType("om_validite_debut", "hiddenstatic");
            $form->setType("om_validite_fin", "hiddenstatic");
        }

        // MODE CONSULTER
        if ($maj == 3 || $crud == 'read') {
            $form->setType("prescription_reglementaire", "static");
            $form->setType("service", "selectstatic");
            $form->setType("tete_de_chapitre1", "static");
            $form->setType("tete_de_chapitre2", "static");
            $form->setType("libelle", "static");
            $form->setType("description_pr_om_html", "htmlstatic");
            $form->setType("defavorable", "checkboxstatic");
            $form->setType("om_validite_debut", "datestatic");
            $form->setType("om_validite_fin", "datestatic");
        }

    }


    function setOnchange(&$form, $maj) {
    //javascript controle client
        $form->setOnchange('prescription_reglementaire','VerifNum(this)');
        $form->setOnchange('service','VerifNum(this)');
        $form->setOnchange('om_validite_debut','fdate(this)');
        $form->setOnchange('om_validite_fin','fdate(this)');
    }
    /**
     * Methode setTaille
     */
    function setTaille(&$form, $maj) {
        $form->setTaille("prescription_reglementaire", 11);
        $form->setTaille("service", 11);
        $form->setTaille("tete_de_chapitre1", 30);
        $form->setTaille("tete_de_chapitre2", 30);
        $form->setTaille("libelle", 30);
        $form->setTaille("description_pr_om_html", 80);
        $form->setTaille("defavorable", 1);
        $form->setTaille("om_validite_debut", 12);
        $form->setTaille("om_validite_fin", 12);
    }

    /**
     * Methode setMax
     */
    function setMax(&$form, $maj) {
        $form->setMax("prescription_reglementaire", 11);
        $form->setMax("service", 11);
        $form->setMax("tete_de_chapitre1", 250);
        $form->setMax("tete_de_chapitre2", 250);
        $form->setMax("libelle", 100);
        $form->setMax("description_pr_om_html", 6);
        $form->setMax("defavorable", 1);
        $form->setMax("om_validite_debut", 12);
        $form->setMax("om_validite_fin", 12);
    }


    function setLib(&$form, $maj) {
    //libelle des champs
        $form->setLib('prescription_reglementaire', __('prescription_reglementaire'));
        $form->setLib('service', __('service'));
        $form->setLib('tete_de_chapitre1', __('tete_de_chapitre1'));
        $form->setLib('tete_de_chapitre2', __('tete_de_chapitre2'));
        $form->setLib('libelle', __('libelle'));
        $form->setLib('description_pr_om_html', __('description_pr_om_html'));
        $form->setLib('defavorable', __('defavorable'));
        $form->setLib('om_validite_debut', __('om_validite_debut'));
        $form->setLib('om_validite_fin', __('om_validite_fin'));
    }
    /**
     *
     */
    function setSelect(&$form, $maj, &$dnu1 = null, $dnu2 = null) {

        // service
        $this->init_select(
            $form, 
            $this->f->db,
            $maj,
            null,
            "service",
            $this->get_var_sql_forminc__sql("service"),
            $this->get_var_sql_forminc__sql("service_by_id"),
            true
        );
    }


    //==================================
    // sous Formulaire
    //==================================
    

    function setValsousformulaire(&$form, $maj, $validation, $idxformulaire, $retourformulaire, $typeformulaire, &$dnu1 = null, $dnu2 = null) {
        $this->retourformulaire = $retourformulaire;
        if($validation == 0) {
            if($this->is_in_context_of_foreign_key('service', $this->retourformulaire))
                $form->setVal('service', $idxformulaire);
        }// fin validation
        $this->set_form_default_values($form, $maj, $validation);
    }// fin setValsousformulaire

    //==================================
    // cle secondaire
    //==================================
    
    /**
     * Methode clesecondaire
     */
    function cleSecondaire($id, &$dnu1 = null, $val = array(), $dnu2 = null) {
        // On appelle la methode de la classe parent
        parent::cleSecondaire($id);
        // Verification de la cle secondaire : lien_prescription_reglementaire_etablissement_categorie
        $this->rechercheTable($this->f->db, "lien_prescription_reglementaire_etablissement_categorie", "prescription_reglementaire", $id);
        // Verification de la cle secondaire : lien_prescription_reglementaire_etablissement_type
        $this->rechercheTable($this->f->db, "lien_prescription_reglementaire_etablissement_type", "prescription_reglementaire", $id);
        // Verification de la cle secondaire : prescription
        $this->rechercheTable($this->f->db, "prescription", "prescription_reglementaire", $id);
        // Verification de la cle secondaire : prescription_specifique
        $this->rechercheTable($this->f->db, "prescription_specifique", "prescription_reglementaire", $id);
    }


}
