<?php
//$Id$ 
//gen openMairie le 03/05/2018 09:12

require_once "../obj/om_dbform.class.php";

class reunion_type_reunion_avis_gen extends om_dbform {

    protected $_absolute_class_name = "reunion_type_reunion_avis";

    var $table = "reunion_type_reunion_avis";
    var $clePrimaire = "reunion_type_reunion_avis";
    var $typeCle = "N";
    var $required_field = array(
        "reunion_type_reunion_avis"
    );
    
    var $foreign_keys_extended = array(
        "reunion_avis" => array("reunion_avis", ),
        "reunion_type" => array("reunion_type", ),
    );
    
    /**
     *
     * @return string
     */
    function get_default_libelle() {
        return $this->getVal($this->clePrimaire)."&nbsp;".$this->getVal("reunion_type");
    }

    /**
     *
     * @return array
     */
    function get_var_sql_forminc__champs() {
        return array(
            "reunion_type_reunion_avis",
            "reunion_type",
            "reunion_avis",
        );
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_reunion_avis() {
        return "SELECT reunion_avis.reunion_avis, reunion_avis.libelle FROM ".DB_PREFIXE."reunion_avis WHERE ((reunion_avis.om_validite_debut IS NULL AND (reunion_avis.om_validite_fin IS NULL OR reunion_avis.om_validite_fin > CURRENT_DATE)) OR (reunion_avis.om_validite_debut <= CURRENT_DATE AND (reunion_avis.om_validite_fin IS NULL OR reunion_avis.om_validite_fin > CURRENT_DATE))) ORDER BY reunion_avis.libelle ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_reunion_avis_by_id() {
        return "SELECT reunion_avis.reunion_avis, reunion_avis.libelle FROM ".DB_PREFIXE."reunion_avis WHERE reunion_avis = <idx>";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_reunion_type() {
        return "SELECT reunion_type.reunion_type, reunion_type.libelle FROM ".DB_PREFIXE."reunion_type WHERE ((reunion_type.om_validite_debut IS NULL AND (reunion_type.om_validite_fin IS NULL OR reunion_type.om_validite_fin > CURRENT_DATE)) OR (reunion_type.om_validite_debut <= CURRENT_DATE AND (reunion_type.om_validite_fin IS NULL OR reunion_type.om_validite_fin > CURRENT_DATE))) ORDER BY reunion_type.libelle ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_reunion_type_by_id() {
        return "SELECT reunion_type.reunion_type, reunion_type.libelle FROM ".DB_PREFIXE."reunion_type WHERE reunion_type = <idx>";
    }




    function setvalF($val = array()) {
        //affectation valeur formulaire
        if (!is_numeric($val['reunion_type_reunion_avis'])) {
            $this->valF['reunion_type_reunion_avis'] = ""; // -> requis
        } else {
            $this->valF['reunion_type_reunion_avis'] = $val['reunion_type_reunion_avis'];
        }
        if (!is_numeric($val['reunion_type'])) {
            $this->valF['reunion_type'] = NULL;
        } else {
            $this->valF['reunion_type'] = $val['reunion_type'];
        }
        if (!is_numeric($val['reunion_avis'])) {
            $this->valF['reunion_avis'] = NULL;
        } else {
            $this->valF['reunion_avis'] = $val['reunion_avis'];
        }
    }

    //=================================================
    //cle primaire automatique [automatic primary key]
    //==================================================

    function setId(&$dnu1 = null) {
    //numero automatique
        $this->valF[$this->clePrimaire] = $this->f->db->nextId(DB_PREFIXE.$this->table);
    }

    function setValFAjout($val = array()) {
    //numero automatique -> pas de controle ajout cle primaire
    }

    function verifierAjout($val = array(), &$dnu1 = null) {
    //numero automatique -> pas de verfication de cle primaire
    }

    //==========================
    // Formulaire  [form]
    //==========================
    /**
     *
     */
    function setType(&$form, $maj) {
        // Récupération du mode de l'action
        $crud = $this->get_action_crud($maj);

        // MODE AJOUTER
        if ($maj == 0 || $crud == 'create') {
            $form->setType("reunion_type_reunion_avis", "hidden");
            if ($this->is_in_context_of_foreign_key("reunion_type", $this->retourformulaire)) {
                $form->setType("reunion_type", "selecthiddenstatic");
            } else {
                $form->setType("reunion_type", "select");
            }
            if ($this->is_in_context_of_foreign_key("reunion_avis", $this->retourformulaire)) {
                $form->setType("reunion_avis", "selecthiddenstatic");
            } else {
                $form->setType("reunion_avis", "select");
            }
        }

        // MDOE MODIFIER
        if ($maj == 1 || $crud == 'update') {
            $form->setType("reunion_type_reunion_avis", "hiddenstatic");
            if ($this->is_in_context_of_foreign_key("reunion_type", $this->retourformulaire)) {
                $form->setType("reunion_type", "selecthiddenstatic");
            } else {
                $form->setType("reunion_type", "select");
            }
            if ($this->is_in_context_of_foreign_key("reunion_avis", $this->retourformulaire)) {
                $form->setType("reunion_avis", "selecthiddenstatic");
            } else {
                $form->setType("reunion_avis", "select");
            }
        }

        // MODE SUPPRIMER
        if ($maj == 2 || $crud == 'delete') {
            $form->setType("reunion_type_reunion_avis", "hiddenstatic");
            $form->setType("reunion_type", "selectstatic");
            $form->setType("reunion_avis", "selectstatic");
        }

        // MODE CONSULTER
        if ($maj == 3 || $crud == 'read') {
            $form->setType("reunion_type_reunion_avis", "static");
            $form->setType("reunion_type", "selectstatic");
            $form->setType("reunion_avis", "selectstatic");
        }

    }


    function setOnchange(&$form, $maj) {
    //javascript controle client
        $form->setOnchange('reunion_type_reunion_avis','VerifNum(this)');
        $form->setOnchange('reunion_type','VerifNum(this)');
        $form->setOnchange('reunion_avis','VerifNum(this)');
    }
    /**
     * Methode setTaille
     */
    function setTaille(&$form, $maj) {
        $form->setTaille("reunion_type_reunion_avis", 11);
        $form->setTaille("reunion_type", 11);
        $form->setTaille("reunion_avis", 11);
    }

    /**
     * Methode setMax
     */
    function setMax(&$form, $maj) {
        $form->setMax("reunion_type_reunion_avis", 11);
        $form->setMax("reunion_type", 11);
        $form->setMax("reunion_avis", 11);
    }


    function setLib(&$form, $maj) {
    //libelle des champs
        $form->setLib('reunion_type_reunion_avis', __('reunion_type_reunion_avis'));
        $form->setLib('reunion_type', __('reunion_type'));
        $form->setLib('reunion_avis', __('reunion_avis'));
    }
    /**
     *
     */
    function setSelect(&$form, $maj, &$dnu1 = null, $dnu2 = null) {

        // reunion_avis
        $this->init_select(
            $form, 
            $this->f->db,
            $maj,
            null,
            "reunion_avis",
            $this->get_var_sql_forminc__sql("reunion_avis"),
            $this->get_var_sql_forminc__sql("reunion_avis_by_id"),
            true
        );
        // reunion_type
        $this->init_select(
            $form, 
            $this->f->db,
            $maj,
            null,
            "reunion_type",
            $this->get_var_sql_forminc__sql("reunion_type"),
            $this->get_var_sql_forminc__sql("reunion_type_by_id"),
            true
        );
    }


    //==================================
    // sous Formulaire
    //==================================
    

    function setValsousformulaire(&$form, $maj, $validation, $idxformulaire, $retourformulaire, $typeformulaire, &$dnu1 = null, $dnu2 = null) {
        $this->retourformulaire = $retourformulaire;
        if($validation == 0) {
            if($this->is_in_context_of_foreign_key('reunion_avis', $this->retourformulaire))
                $form->setVal('reunion_avis', $idxformulaire);
            if($this->is_in_context_of_foreign_key('reunion_type', $this->retourformulaire))
                $form->setVal('reunion_type', $idxformulaire);
        }// fin validation
        $this->set_form_default_values($form, $maj, $validation);
    }// fin setValsousformulaire

    //==================================
    // cle secondaire
    //==================================
    

}
