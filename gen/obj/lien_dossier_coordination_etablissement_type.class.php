<?php
//$Id$ 
//gen openMairie le 20/05/2020 18:03

require_once "../obj/om_dbform.class.php";

class lien_dossier_coordination_etablissement_type_gen extends om_dbform {

    protected $_absolute_class_name = "lien_dossier_coordination_etablissement_type";

    var $table = "lien_dossier_coordination_etablissement_type";
    var $clePrimaire = "lien_dossier_coordination_etablissement_type";
    var $typeCle = "N";
    var $required_field = array(
        "dossier_coordination",
        "etablissement_type",
        "lien_dossier_coordination_etablissement_type"
    );
    
    var $foreign_keys_extended = array(
        "dossier_coordination" => array("dossier_coordination", "dossier_coordination_nouveau", "dossier_coordination_a_qualifier", "dossier_coordination_a_cloturer", ),
        "etablissement_type" => array("etablissement_type", ),
    );
    
    /**
     *
     * @return string
     */
    function get_default_libelle() {
        return $this->getVal($this->clePrimaire)."&nbsp;".$this->getVal("dossier_coordination");
    }

    /**
     *
     * @return array
     */
    function get_var_sql_forminc__champs() {
        return array(
            "lien_dossier_coordination_etablissement_type",
            "dossier_coordination",
            "etablissement_type",
        );
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_dossier_coordination() {
        return "SELECT dossier_coordination.dossier_coordination, dossier_coordination.libelle FROM ".DB_PREFIXE."dossier_coordination ORDER BY dossier_coordination.libelle ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_dossier_coordination_by_id() {
        return "SELECT dossier_coordination.dossier_coordination, dossier_coordination.libelle FROM ".DB_PREFIXE."dossier_coordination WHERE dossier_coordination = <idx>";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_etablissement_type() {
        return "SELECT etablissement_type.etablissement_type, etablissement_type.libelle FROM ".DB_PREFIXE."etablissement_type WHERE ((etablissement_type.om_validite_debut IS NULL AND (etablissement_type.om_validite_fin IS NULL OR etablissement_type.om_validite_fin > CURRENT_DATE)) OR (etablissement_type.om_validite_debut <= CURRENT_DATE AND (etablissement_type.om_validite_fin IS NULL OR etablissement_type.om_validite_fin > CURRENT_DATE))) ORDER BY etablissement_type.libelle ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_etablissement_type_by_id() {
        return "SELECT etablissement_type.etablissement_type, etablissement_type.libelle FROM ".DB_PREFIXE."etablissement_type WHERE etablissement_type = <idx>";
    }




    function setvalF($val = array()) {
        //affectation valeur formulaire
        if (!is_numeric($val['lien_dossier_coordination_etablissement_type'])) {
            $this->valF['lien_dossier_coordination_etablissement_type'] = ""; // -> requis
        } else {
            $this->valF['lien_dossier_coordination_etablissement_type'] = $val['lien_dossier_coordination_etablissement_type'];
        }
        if (!is_numeric($val['dossier_coordination'])) {
            $this->valF['dossier_coordination'] = ""; // -> requis
        } else {
            $this->valF['dossier_coordination'] = $val['dossier_coordination'];
        }
        if (!is_numeric($val['etablissement_type'])) {
            $this->valF['etablissement_type'] = ""; // -> requis
        } else {
            $this->valF['etablissement_type'] = $val['etablissement_type'];
        }
    }

    //=================================================
    //cle primaire automatique [automatic primary key]
    //==================================================

    function setId(&$dnu1 = null) {
    //numero automatique
        $this->valF[$this->clePrimaire] = $this->f->db->nextId(DB_PREFIXE.$this->table);
    }

    function setValFAjout($val = array()) {
    //numero automatique -> pas de controle ajout cle primaire
    }

    function verifierAjout($val = array(), &$dnu1 = null) {
    //numero automatique -> pas de verfication de cle primaire
    }

    //==========================
    // Formulaire  [form]
    //==========================
    /**
     *
     */
    function setType(&$form, $maj) {
        // Récupération du mode de l'action
        $crud = $this->get_action_crud($maj);

        // MODE AJOUTER
        if ($maj == 0 || $crud == 'create') {
            $form->setType("lien_dossier_coordination_etablissement_type", "hidden");
            if ($this->is_in_context_of_foreign_key("dossier_coordination", $this->retourformulaire)) {
                $form->setType("dossier_coordination", "selecthiddenstatic");
            } else {
                $form->setType("dossier_coordination", "select");
            }
            if ($this->is_in_context_of_foreign_key("etablissement_type", $this->retourformulaire)) {
                $form->setType("etablissement_type", "selecthiddenstatic");
            } else {
                $form->setType("etablissement_type", "select");
            }
        }

        // MDOE MODIFIER
        if ($maj == 1 || $crud == 'update') {
            $form->setType("lien_dossier_coordination_etablissement_type", "hiddenstatic");
            if ($this->is_in_context_of_foreign_key("dossier_coordination", $this->retourformulaire)) {
                $form->setType("dossier_coordination", "selecthiddenstatic");
            } else {
                $form->setType("dossier_coordination", "select");
            }
            if ($this->is_in_context_of_foreign_key("etablissement_type", $this->retourformulaire)) {
                $form->setType("etablissement_type", "selecthiddenstatic");
            } else {
                $form->setType("etablissement_type", "select");
            }
        }

        // MODE SUPPRIMER
        if ($maj == 2 || $crud == 'delete') {
            $form->setType("lien_dossier_coordination_etablissement_type", "hiddenstatic");
            $form->setType("dossier_coordination", "selectstatic");
            $form->setType("etablissement_type", "selectstatic");
        }

        // MODE CONSULTER
        if ($maj == 3 || $crud == 'read') {
            $form->setType("lien_dossier_coordination_etablissement_type", "static");
            $form->setType("dossier_coordination", "selectstatic");
            $form->setType("etablissement_type", "selectstatic");
        }

    }


    function setOnchange(&$form, $maj) {
    //javascript controle client
        $form->setOnchange('lien_dossier_coordination_etablissement_type','VerifNum(this)');
        $form->setOnchange('dossier_coordination','VerifNum(this)');
        $form->setOnchange('etablissement_type','VerifNum(this)');
    }
    /**
     * Methode setTaille
     */
    function setTaille(&$form, $maj) {
        $form->setTaille("lien_dossier_coordination_etablissement_type", 11);
        $form->setTaille("dossier_coordination", 11);
        $form->setTaille("etablissement_type", 11);
    }

    /**
     * Methode setMax
     */
    function setMax(&$form, $maj) {
        $form->setMax("lien_dossier_coordination_etablissement_type", 11);
        $form->setMax("dossier_coordination", 11);
        $form->setMax("etablissement_type", 11);
    }


    function setLib(&$form, $maj) {
    //libelle des champs
        $form->setLib('lien_dossier_coordination_etablissement_type', __('lien_dossier_coordination_etablissement_type'));
        $form->setLib('dossier_coordination', __('dossier_coordination'));
        $form->setLib('etablissement_type', __('etablissement_type'));
    }
    /**
     *
     */
    function setSelect(&$form, $maj, &$dnu1 = null, $dnu2 = null) {

        // dossier_coordination
        $this->init_select(
            $form, 
            $this->f->db,
            $maj,
            null,
            "dossier_coordination",
            $this->get_var_sql_forminc__sql("dossier_coordination"),
            $this->get_var_sql_forminc__sql("dossier_coordination_by_id"),
            false
        );
        // etablissement_type
        $this->init_select(
            $form, 
            $this->f->db,
            $maj,
            null,
            "etablissement_type",
            $this->get_var_sql_forminc__sql("etablissement_type"),
            $this->get_var_sql_forminc__sql("etablissement_type_by_id"),
            true
        );
    }


    //==================================
    // sous Formulaire
    //==================================
    

    function setValsousformulaire(&$form, $maj, $validation, $idxformulaire, $retourformulaire, $typeformulaire, &$dnu1 = null, $dnu2 = null) {
        $this->retourformulaire = $retourformulaire;
        if($validation == 0) {
            if($this->is_in_context_of_foreign_key('dossier_coordination', $this->retourformulaire))
                $form->setVal('dossier_coordination', $idxformulaire);
            if($this->is_in_context_of_foreign_key('etablissement_type', $this->retourformulaire))
                $form->setVal('etablissement_type', $idxformulaire);
        }// fin validation
        $this->set_form_default_values($form, $maj, $validation);
    }// fin setValsousformulaire

    //==================================
    // cle secondaire
    //==================================
    

}
