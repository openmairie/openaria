<?php
//$Id$ 
//gen openMairie le 03/05/2018 09:12

require_once "../obj/om_dbform.class.php";

class lien_reglementation_applicable_etablissement_type_gen extends om_dbform {

    protected $_absolute_class_name = "lien_reglementation_applicable_etablissement_type";

    var $table = "lien_reglementation_applicable_etablissement_type";
    var $clePrimaire = "lien_reglementation_applicable_etablissement_type";
    var $typeCle = "N";
    var $required_field = array(
        "etablissement_type",
        "lien_reglementation_applicable_etablissement_type",
        "reglementation_applicable"
    );
    var $unique_key = array(
      array("etablissement_type","reglementation_applicable"),
    );
    var $foreign_keys_extended = array(
        "etablissement_type" => array("etablissement_type", ),
        "reglementation_applicable" => array("reglementation_applicable", ),
    );
    
    /**
     *
     * @return string
     */
    function get_default_libelle() {
        return $this->getVal($this->clePrimaire)."&nbsp;".$this->getVal("reglementation_applicable");
    }

    /**
     *
     * @return array
     */
    function get_var_sql_forminc__champs() {
        return array(
            "lien_reglementation_applicable_etablissement_type",
            "reglementation_applicable",
            "etablissement_type",
        );
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_etablissement_type() {
        return "SELECT etablissement_type.etablissement_type, etablissement_type.libelle FROM ".DB_PREFIXE."etablissement_type WHERE ((etablissement_type.om_validite_debut IS NULL AND (etablissement_type.om_validite_fin IS NULL OR etablissement_type.om_validite_fin > CURRENT_DATE)) OR (etablissement_type.om_validite_debut <= CURRENT_DATE AND (etablissement_type.om_validite_fin IS NULL OR etablissement_type.om_validite_fin > CURRENT_DATE))) ORDER BY etablissement_type.libelle ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_etablissement_type_by_id() {
        return "SELECT etablissement_type.etablissement_type, etablissement_type.libelle FROM ".DB_PREFIXE."etablissement_type WHERE etablissement_type = <idx>";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_reglementation_applicable() {
        return "SELECT reglementation_applicable.reglementation_applicable, reglementation_applicable.libelle FROM ".DB_PREFIXE."reglementation_applicable WHERE ((reglementation_applicable.om_validite_debut IS NULL AND (reglementation_applicable.om_validite_fin IS NULL OR reglementation_applicable.om_validite_fin > CURRENT_DATE)) OR (reglementation_applicable.om_validite_debut <= CURRENT_DATE AND (reglementation_applicable.om_validite_fin IS NULL OR reglementation_applicable.om_validite_fin > CURRENT_DATE))) ORDER BY reglementation_applicable.libelle ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_reglementation_applicable_by_id() {
        return "SELECT reglementation_applicable.reglementation_applicable, reglementation_applicable.libelle FROM ".DB_PREFIXE."reglementation_applicable WHERE reglementation_applicable = <idx>";
    }




    function setvalF($val = array()) {
        //affectation valeur formulaire
        if (!is_numeric($val['lien_reglementation_applicable_etablissement_type'])) {
            $this->valF['lien_reglementation_applicable_etablissement_type'] = ""; // -> requis
        } else {
            $this->valF['lien_reglementation_applicable_etablissement_type'] = $val['lien_reglementation_applicable_etablissement_type'];
        }
        if (!is_numeric($val['reglementation_applicable'])) {
            $this->valF['reglementation_applicable'] = ""; // -> requis
        } else {
            $this->valF['reglementation_applicable'] = $val['reglementation_applicable'];
        }
        if (!is_numeric($val['etablissement_type'])) {
            $this->valF['etablissement_type'] = ""; // -> requis
        } else {
            $this->valF['etablissement_type'] = $val['etablissement_type'];
        }
    }

    //=================================================
    //cle primaire automatique [automatic primary key]
    //==================================================

    function setId(&$dnu1 = null) {
    //numero automatique
        $this->valF[$this->clePrimaire] = $this->f->db->nextId(DB_PREFIXE.$this->table);
    }

    function setValFAjout($val = array()) {
    //numero automatique -> pas de controle ajout cle primaire
    }

    function verifierAjout($val = array(), &$dnu1 = null) {
    //numero automatique -> pas de verfication de cle primaire
    }

    //==========================
    // Formulaire  [form]
    //==========================
    /**
     *
     */
    function setType(&$form, $maj) {
        // Récupération du mode de l'action
        $crud = $this->get_action_crud($maj);

        // MODE AJOUTER
        if ($maj == 0 || $crud == 'create') {
            $form->setType("lien_reglementation_applicable_etablissement_type", "hidden");
            if ($this->is_in_context_of_foreign_key("reglementation_applicable", $this->retourformulaire)) {
                $form->setType("reglementation_applicable", "selecthiddenstatic");
            } else {
                $form->setType("reglementation_applicable", "select");
            }
            if ($this->is_in_context_of_foreign_key("etablissement_type", $this->retourformulaire)) {
                $form->setType("etablissement_type", "selecthiddenstatic");
            } else {
                $form->setType("etablissement_type", "select");
            }
        }

        // MDOE MODIFIER
        if ($maj == 1 || $crud == 'update') {
            $form->setType("lien_reglementation_applicable_etablissement_type", "hiddenstatic");
            if ($this->is_in_context_of_foreign_key("reglementation_applicable", $this->retourformulaire)) {
                $form->setType("reglementation_applicable", "selecthiddenstatic");
            } else {
                $form->setType("reglementation_applicable", "select");
            }
            if ($this->is_in_context_of_foreign_key("etablissement_type", $this->retourformulaire)) {
                $form->setType("etablissement_type", "selecthiddenstatic");
            } else {
                $form->setType("etablissement_type", "select");
            }
        }

        // MODE SUPPRIMER
        if ($maj == 2 || $crud == 'delete') {
            $form->setType("lien_reglementation_applicable_etablissement_type", "hiddenstatic");
            $form->setType("reglementation_applicable", "selectstatic");
            $form->setType("etablissement_type", "selectstatic");
        }

        // MODE CONSULTER
        if ($maj == 3 || $crud == 'read') {
            $form->setType("lien_reglementation_applicable_etablissement_type", "static");
            $form->setType("reglementation_applicable", "selectstatic");
            $form->setType("etablissement_type", "selectstatic");
        }

    }


    function setOnchange(&$form, $maj) {
    //javascript controle client
        $form->setOnchange('lien_reglementation_applicable_etablissement_type','VerifNum(this)');
        $form->setOnchange('reglementation_applicable','VerifNum(this)');
        $form->setOnchange('etablissement_type','VerifNum(this)');
    }
    /**
     * Methode setTaille
     */
    function setTaille(&$form, $maj) {
        $form->setTaille("lien_reglementation_applicable_etablissement_type", 11);
        $form->setTaille("reglementation_applicable", 11);
        $form->setTaille("etablissement_type", 11);
    }

    /**
     * Methode setMax
     */
    function setMax(&$form, $maj) {
        $form->setMax("lien_reglementation_applicable_etablissement_type", 11);
        $form->setMax("reglementation_applicable", 11);
        $form->setMax("etablissement_type", 11);
    }


    function setLib(&$form, $maj) {
    //libelle des champs
        $form->setLib('lien_reglementation_applicable_etablissement_type', __('lien_reglementation_applicable_etablissement_type'));
        $form->setLib('reglementation_applicable', __('reglementation_applicable'));
        $form->setLib('etablissement_type', __('etablissement_type'));
    }
    /**
     *
     */
    function setSelect(&$form, $maj, &$dnu1 = null, $dnu2 = null) {

        // etablissement_type
        $this->init_select(
            $form, 
            $this->f->db,
            $maj,
            null,
            "etablissement_type",
            $this->get_var_sql_forminc__sql("etablissement_type"),
            $this->get_var_sql_forminc__sql("etablissement_type_by_id"),
            true
        );
        // reglementation_applicable
        $this->init_select(
            $form, 
            $this->f->db,
            $maj,
            null,
            "reglementation_applicable",
            $this->get_var_sql_forminc__sql("reglementation_applicable"),
            $this->get_var_sql_forminc__sql("reglementation_applicable_by_id"),
            true
        );
    }


    //==================================
    // sous Formulaire
    //==================================
    

    function setValsousformulaire(&$form, $maj, $validation, $idxformulaire, $retourformulaire, $typeformulaire, &$dnu1 = null, $dnu2 = null) {
        $this->retourformulaire = $retourformulaire;
        if($validation == 0) {
            if($this->is_in_context_of_foreign_key('etablissement_type', $this->retourformulaire))
                $form->setVal('etablissement_type', $idxformulaire);
            if($this->is_in_context_of_foreign_key('reglementation_applicable', $this->retourformulaire))
                $form->setVal('reglementation_applicable', $idxformulaire);
        }// fin validation
        $this->set_form_default_values($form, $maj, $validation);
    }// fin setValsousformulaire

    //==================================
    // cle secondaire
    //==================================
    

}
