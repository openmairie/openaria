<?php
//$Id$ 
//gen openMairie le 25/05/2018 16:52

require_once "../obj/om_dbform.class.php";

class analyses_type_gen extends om_dbform {

    protected $_absolute_class_name = "analyses_type";

    var $table = "analyses_type";
    var $clePrimaire = "analyses_type";
    var $typeCle = "N";
    var $required_field = array(
        "analyses_type",
        "modele_edition_compte_rendu",
        "modele_edition_proces_verbal",
        "modele_edition_rapport",
        "service"
    );
    
    var $foreign_keys_extended = array(
        "modele_edition" => array("modele_edition", ),
        "service" => array("service", ),
    );
    
    /**
     *
     * @return string
     */
    function get_default_libelle() {
        return $this->getVal($this->clePrimaire)."&nbsp;".$this->getVal("libelle");
    }

    /**
     *
     * @return array
     */
    function get_var_sql_forminc__champs() {
        return array(
            "analyses_type",
            "service",
            "code",
            "libelle",
            "description",
            "om_validite_debut",
            "om_validite_fin",
            "modele_edition_rapport",
            "modele_edition_compte_rendu",
            "modele_edition_proces_verbal",
        );
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_modele_edition_compte_rendu() {
        return "SELECT modele_edition.modele_edition, modele_edition.libelle FROM ".DB_PREFIXE."modele_edition WHERE ((modele_edition.om_validite_debut IS NULL AND (modele_edition.om_validite_fin IS NULL OR modele_edition.om_validite_fin > CURRENT_DATE)) OR (modele_edition.om_validite_debut <= CURRENT_DATE AND (modele_edition.om_validite_fin IS NULL OR modele_edition.om_validite_fin > CURRENT_DATE))) ORDER BY modele_edition.libelle ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_modele_edition_compte_rendu_by_id() {
        return "SELECT modele_edition.modele_edition, modele_edition.libelle FROM ".DB_PREFIXE."modele_edition WHERE modele_edition = <idx>";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_modele_edition_proces_verbal() {
        return "SELECT modele_edition.modele_edition, modele_edition.libelle FROM ".DB_PREFIXE."modele_edition WHERE ((modele_edition.om_validite_debut IS NULL AND (modele_edition.om_validite_fin IS NULL OR modele_edition.om_validite_fin > CURRENT_DATE)) OR (modele_edition.om_validite_debut <= CURRENT_DATE AND (modele_edition.om_validite_fin IS NULL OR modele_edition.om_validite_fin > CURRENT_DATE))) ORDER BY modele_edition.libelle ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_modele_edition_proces_verbal_by_id() {
        return "SELECT modele_edition.modele_edition, modele_edition.libelle FROM ".DB_PREFIXE."modele_edition WHERE modele_edition = <idx>";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_modele_edition_rapport() {
        return "SELECT modele_edition.modele_edition, modele_edition.libelle FROM ".DB_PREFIXE."modele_edition WHERE ((modele_edition.om_validite_debut IS NULL AND (modele_edition.om_validite_fin IS NULL OR modele_edition.om_validite_fin > CURRENT_DATE)) OR (modele_edition.om_validite_debut <= CURRENT_DATE AND (modele_edition.om_validite_fin IS NULL OR modele_edition.om_validite_fin > CURRENT_DATE))) ORDER BY modele_edition.libelle ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_modele_edition_rapport_by_id() {
        return "SELECT modele_edition.modele_edition, modele_edition.libelle FROM ".DB_PREFIXE."modele_edition WHERE modele_edition = <idx>";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_service() {
        return "SELECT service.service, service.libelle FROM ".DB_PREFIXE."service WHERE ((service.om_validite_debut IS NULL AND (service.om_validite_fin IS NULL OR service.om_validite_fin > CURRENT_DATE)) OR (service.om_validite_debut <= CURRENT_DATE AND (service.om_validite_fin IS NULL OR service.om_validite_fin > CURRENT_DATE))) ORDER BY service.libelle ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_service_by_id() {
        return "SELECT service.service, service.libelle FROM ".DB_PREFIXE."service WHERE service = <idx>";
    }




    function setvalF($val = array()) {
        //affectation valeur formulaire
        if (!is_numeric($val['analyses_type'])) {
            $this->valF['analyses_type'] = ""; // -> requis
        } else {
            $this->valF['analyses_type'] = $val['analyses_type'];
        }
        if (!is_numeric($val['service'])) {
            $this->valF['service'] = ""; // -> requis
        } else {
            $this->valF['service'] = $val['service'];
        }
        if ($val['code'] == "") {
            $this->valF['code'] = NULL;
        } else {
            $this->valF['code'] = $val['code'];
        }
        if ($val['libelle'] == "") {
            $this->valF['libelle'] = NULL;
        } else {
            $this->valF['libelle'] = $val['libelle'];
        }
            $this->valF['description'] = $val['description'];
        if ($val['om_validite_debut'] != "") {
            $this->valF['om_validite_debut'] = $this->dateDB($val['om_validite_debut']);
        } else {
            $this->valF['om_validite_debut'] = NULL;
        }
        if ($val['om_validite_fin'] != "") {
            $this->valF['om_validite_fin'] = $this->dateDB($val['om_validite_fin']);
        } else {
            $this->valF['om_validite_fin'] = NULL;
        }
        if (!is_numeric($val['modele_edition_rapport'])) {
            $this->valF['modele_edition_rapport'] = ""; // -> requis
        } else {
            $this->valF['modele_edition_rapport'] = $val['modele_edition_rapport'];
        }
        if (!is_numeric($val['modele_edition_compte_rendu'])) {
            $this->valF['modele_edition_compte_rendu'] = ""; // -> requis
        } else {
            $this->valF['modele_edition_compte_rendu'] = $val['modele_edition_compte_rendu'];
        }
        if (!is_numeric($val['modele_edition_proces_verbal'])) {
            $this->valF['modele_edition_proces_verbal'] = ""; // -> requis
        } else {
            $this->valF['modele_edition_proces_verbal'] = $val['modele_edition_proces_verbal'];
        }
    }

    //=================================================
    //cle primaire automatique [automatic primary key]
    //==================================================

    function setId(&$dnu1 = null) {
    //numero automatique
        $this->valF[$this->clePrimaire] = $this->f->db->nextId(DB_PREFIXE.$this->table);
    }

    function setValFAjout($val = array()) {
    //numero automatique -> pas de controle ajout cle primaire
    }

    function verifierAjout($val = array(), &$dnu1 = null) {
    //numero automatique -> pas de verfication de cle primaire
    }
    /**
     * Methode verifier
     */
    function verifier($val = array(), &$dnu1 = null, $dnu2 = null) {
        // On appelle la methode de la classe parent
        parent::verifier($val, $this->f->db, null);

        // gestion des dates de validites
        $date_debut = $this->valF['om_validite_debut'];
        $date_fin = $this->valF['om_validite_fin'];

        if ($date_debut != '' and $date_fin != '') {
        
            $date_debut = explode('-', $this->valF['om_validite_debut']);
            $date_fin = explode('-', $this->valF['om_validite_fin']);

            $time_debut = mktime(0, 0, 0, $date_debut[1], $date_debut[2],
                                 $date_debut[0]);
            $time_fin = mktime(0, 0, 0, $date_fin[1], $date_fin[2],
                                 $date_fin[0]);

            if ($time_debut > $time_fin or $time_debut == $time_fin) {
                $this->correct = false;
                $this->addToMessage(__('La date de fin de validite doit etre future a la de debut de validite.'));
            }
        }
    }


    //==========================
    // Formulaire  [form]
    //==========================
    /**
     *
     */
    function setType(&$form, $maj) {
        // Récupération du mode de l'action
        $crud = $this->get_action_crud($maj);

        // MODE AJOUTER
        if ($maj == 0 || $crud == 'create') {
            $form->setType("analyses_type", "hidden");
            if ($this->is_in_context_of_foreign_key("service", $this->retourformulaire)) {
                $form->setType("service", "selecthiddenstatic");
            } else {
                $form->setType("service", "select");
            }
            $form->setType("code", "text");
            $form->setType("libelle", "text");
            $form->setType("description", "textarea");
            if ($this->f->isAccredited(array($this->table."_modifier_validite", $this->table, ))) {
                $form->setType("om_validite_debut", "date");
            } else {
                $form->setType("om_validite_debut", "hiddenstaticdate");
            }
            if ($this->f->isAccredited(array($this->table."_modifier_validite", $this->table, ))) {
                $form->setType("om_validite_fin", "date");
            } else {
                $form->setType("om_validite_fin", "hiddenstaticdate");
            }
            if ($this->is_in_context_of_foreign_key("modele_edition", $this->retourformulaire)) {
                $form->setType("modele_edition_rapport", "selecthiddenstatic");
            } else {
                $form->setType("modele_edition_rapport", "select");
            }
            if ($this->is_in_context_of_foreign_key("modele_edition", $this->retourformulaire)) {
                $form->setType("modele_edition_compte_rendu", "selecthiddenstatic");
            } else {
                $form->setType("modele_edition_compte_rendu", "select");
            }
            if ($this->is_in_context_of_foreign_key("modele_edition", $this->retourformulaire)) {
                $form->setType("modele_edition_proces_verbal", "selecthiddenstatic");
            } else {
                $form->setType("modele_edition_proces_verbal", "select");
            }
        }

        // MDOE MODIFIER
        if ($maj == 1 || $crud == 'update') {
            $form->setType("analyses_type", "hiddenstatic");
            if ($this->is_in_context_of_foreign_key("service", $this->retourformulaire)) {
                $form->setType("service", "selecthiddenstatic");
            } else {
                $form->setType("service", "select");
            }
            $form->setType("code", "text");
            $form->setType("libelle", "text");
            $form->setType("description", "textarea");
            if ($this->f->isAccredited(array($this->table."_modifier_validite", $this->table, ))) {
                $form->setType("om_validite_debut", "date");
            } else {
                $form->setType("om_validite_debut", "hiddenstaticdate");
            }
            if ($this->f->isAccredited(array($this->table."_modifier_validite", $this->table, ))) {
                $form->setType("om_validite_fin", "date");
            } else {
                $form->setType("om_validite_fin", "hiddenstaticdate");
            }
            if ($this->is_in_context_of_foreign_key("modele_edition", $this->retourformulaire)) {
                $form->setType("modele_edition_rapport", "selecthiddenstatic");
            } else {
                $form->setType("modele_edition_rapport", "select");
            }
            if ($this->is_in_context_of_foreign_key("modele_edition", $this->retourformulaire)) {
                $form->setType("modele_edition_compte_rendu", "selecthiddenstatic");
            } else {
                $form->setType("modele_edition_compte_rendu", "select");
            }
            if ($this->is_in_context_of_foreign_key("modele_edition", $this->retourformulaire)) {
                $form->setType("modele_edition_proces_verbal", "selecthiddenstatic");
            } else {
                $form->setType("modele_edition_proces_verbal", "select");
            }
        }

        // MODE SUPPRIMER
        if ($maj == 2 || $crud == 'delete') {
            $form->setType("analyses_type", "hiddenstatic");
            $form->setType("service", "selectstatic");
            $form->setType("code", "hiddenstatic");
            $form->setType("libelle", "hiddenstatic");
            $form->setType("description", "hiddenstatic");
            $form->setType("om_validite_debut", "hiddenstatic");
            $form->setType("om_validite_fin", "hiddenstatic");
            $form->setType("modele_edition_rapport", "selectstatic");
            $form->setType("modele_edition_compte_rendu", "selectstatic");
            $form->setType("modele_edition_proces_verbal", "selectstatic");
        }

        // MODE CONSULTER
        if ($maj == 3 || $crud == 'read') {
            $form->setType("analyses_type", "static");
            $form->setType("service", "selectstatic");
            $form->setType("code", "static");
            $form->setType("libelle", "static");
            $form->setType("description", "textareastatic");
            $form->setType("om_validite_debut", "datestatic");
            $form->setType("om_validite_fin", "datestatic");
            $form->setType("modele_edition_rapport", "selectstatic");
            $form->setType("modele_edition_compte_rendu", "selectstatic");
            $form->setType("modele_edition_proces_verbal", "selectstatic");
        }

    }


    function setOnchange(&$form, $maj) {
    //javascript controle client
        $form->setOnchange('analyses_type','VerifNum(this)');
        $form->setOnchange('service','VerifNum(this)');
        $form->setOnchange('om_validite_debut','fdate(this)');
        $form->setOnchange('om_validite_fin','fdate(this)');
        $form->setOnchange('modele_edition_rapport','VerifNum(this)');
        $form->setOnchange('modele_edition_compte_rendu','VerifNum(this)');
        $form->setOnchange('modele_edition_proces_verbal','VerifNum(this)');
    }
    /**
     * Methode setTaille
     */
    function setTaille(&$form, $maj) {
        $form->setTaille("analyses_type", 11);
        $form->setTaille("service", 11);
        $form->setTaille("code", 20);
        $form->setTaille("libelle", 30);
        $form->setTaille("description", 80);
        $form->setTaille("om_validite_debut", 12);
        $form->setTaille("om_validite_fin", 12);
        $form->setTaille("modele_edition_rapport", 11);
        $form->setTaille("modele_edition_compte_rendu", 11);
        $form->setTaille("modele_edition_proces_verbal", 11);
    }

    /**
     * Methode setMax
     */
    function setMax(&$form, $maj) {
        $form->setMax("analyses_type", 11);
        $form->setMax("service", 11);
        $form->setMax("code", 20);
        $form->setMax("libelle", 100);
        $form->setMax("description", 6);
        $form->setMax("om_validite_debut", 12);
        $form->setMax("om_validite_fin", 12);
        $form->setMax("modele_edition_rapport", 11);
        $form->setMax("modele_edition_compte_rendu", 11);
        $form->setMax("modele_edition_proces_verbal", 11);
    }


    function setLib(&$form, $maj) {
    //libelle des champs
        $form->setLib('analyses_type', __('analyses_type'));
        $form->setLib('service', __('service'));
        $form->setLib('code', __('code'));
        $form->setLib('libelle', __('libelle'));
        $form->setLib('description', __('description'));
        $form->setLib('om_validite_debut', __('om_validite_debut'));
        $form->setLib('om_validite_fin', __('om_validite_fin'));
        $form->setLib('modele_edition_rapport', __('modele_edition_rapport'));
        $form->setLib('modele_edition_compte_rendu', __('modele_edition_compte_rendu'));
        $form->setLib('modele_edition_proces_verbal', __('modele_edition_proces_verbal'));
    }
    /**
     *
     */
    function setSelect(&$form, $maj, &$dnu1 = null, $dnu2 = null) {

        // modele_edition_compte_rendu
        $this->init_select(
            $form, 
            $this->f->db,
            $maj,
            null,
            "modele_edition_compte_rendu",
            $this->get_var_sql_forminc__sql("modele_edition_compte_rendu"),
            $this->get_var_sql_forminc__sql("modele_edition_compte_rendu_by_id"),
            true
        );
        // modele_edition_proces_verbal
        $this->init_select(
            $form, 
            $this->f->db,
            $maj,
            null,
            "modele_edition_proces_verbal",
            $this->get_var_sql_forminc__sql("modele_edition_proces_verbal"),
            $this->get_var_sql_forminc__sql("modele_edition_proces_verbal_by_id"),
            true
        );
        // modele_edition_rapport
        $this->init_select(
            $form, 
            $this->f->db,
            $maj,
            null,
            "modele_edition_rapport",
            $this->get_var_sql_forminc__sql("modele_edition_rapport"),
            $this->get_var_sql_forminc__sql("modele_edition_rapport_by_id"),
            true
        );
        // service
        $this->init_select(
            $form, 
            $this->f->db,
            $maj,
            null,
            "service",
            $this->get_var_sql_forminc__sql("service"),
            $this->get_var_sql_forminc__sql("service_by_id"),
            true
        );
    }


    //==================================
    // sous Formulaire
    //==================================
    

    function setValsousformulaire(&$form, $maj, $validation, $idxformulaire, $retourformulaire, $typeformulaire, &$dnu1 = null, $dnu2 = null) {
        $this->retourformulaire = $retourformulaire;
        if($validation == 0) {
            if($this->is_in_context_of_foreign_key('service', $this->retourformulaire))
                $form->setVal('service', $idxformulaire);
        }// fin validation
        if ($validation == 0 and $maj == 0) {
            if($this->is_in_context_of_foreign_key('modele_edition', $this->retourformulaire))
                $form->setVal('modele_edition_compte_rendu', $idxformulaire);
            if($this->is_in_context_of_foreign_key('modele_edition', $this->retourformulaire))
                $form->setVal('modele_edition_proces_verbal', $idxformulaire);
            if($this->is_in_context_of_foreign_key('modele_edition', $this->retourformulaire))
                $form->setVal('modele_edition_rapport', $idxformulaire);
        }// fin validation
        $this->set_form_default_values($form, $maj, $validation);
    }// fin setValsousformulaire

    //==================================
    // cle secondaire
    //==================================
    
    /**
     * Methode clesecondaire
     */
    function cleSecondaire($id, &$dnu1 = null, $val = array(), $dnu2 = null) {
        // On appelle la methode de la classe parent
        parent::cleSecondaire($id);
        // Verification de la cle secondaire : analyses
        $this->rechercheTable($this->f->db, "analyses", "analyses_type", $id);
        // Verification de la cle secondaire : dossier_coordination_type
        $this->rechercheTable($this->f->db, "dossier_coordination_type", "analyses_type_acc", $id);
        // Verification de la cle secondaire : dossier_coordination_type
        $this->rechercheTable($this->f->db, "dossier_coordination_type", "analyses_type_si", $id);
        // Verification de la cle secondaire : etablissement
        $this->rechercheTable($this->f->db, "etablissement", "si_prochaine_visite_type", $id);
        // Verification de la cle secondaire : lien_dossier_coordination_type_analyses_type
        $this->rechercheTable($this->f->db, "lien_dossier_coordination_type_analyses_type", "analyses_type", $id);
    }


}
