<?php
//$Id$ 
//gen openMairie le 03/05/2018 09:12

require_once "../obj/om_dbform.class.php";

class contact_gen extends om_dbform {

    protected $_absolute_class_name = "contact";

    var $table = "contact";
    var $clePrimaire = "contact";
    var $typeCle = "N";
    var $required_field = array(
        "contact"
    );
    
    var $foreign_keys_extended = array(
        "contact_civilite" => array("contact_civilite", ),
        "contact_type" => array("contact_type", ),
        "etablissement" => array("etablissement", "etablissement_referentiel_erp", "etablissement_tous", ),
        "service" => array("service", ),
    );
    
    /**
     *
     * @return string
     */
    function get_default_libelle() {
        return $this->getVal($this->clePrimaire)."&nbsp;".$this->getVal("etablissement");
    }

    /**
     *
     * @return array
     */
    function get_var_sql_forminc__champs() {
        return array(
            "contact",
            "etablissement",
            "contact_type",
            "civilite",
            "nom",
            "prenom",
            "titre",
            "telephone",
            "mobile",
            "fax",
            "courriel",
            "om_validite_debut",
            "om_validite_fin",
            "adresse_numero",
            "adresse_numero2",
            "adresse_voie",
            "adresse_complement",
            "adresse_cp",
            "adresse_ville",
            "lieu_dit",
            "boite_postale",
            "cedex",
            "pays",
            "qualite",
            "denomination",
            "raison_sociale",
            "siret",
            "categorie_juridique",
            "reception_convocation",
            "service",
            "reception_programmation",
            "reception_commission",
        );
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_civilite() {
        return "SELECT contact_civilite.contact_civilite, contact_civilite.libelle FROM ".DB_PREFIXE."contact_civilite ORDER BY contact_civilite.libelle ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_civilite_by_id() {
        return "SELECT contact_civilite.contact_civilite, contact_civilite.libelle FROM ".DB_PREFIXE."contact_civilite WHERE contact_civilite = <idx>";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_contact_type() {
        return "SELECT contact_type.contact_type, contact_type.libelle FROM ".DB_PREFIXE."contact_type WHERE ((contact_type.om_validite_debut IS NULL AND (contact_type.om_validite_fin IS NULL OR contact_type.om_validite_fin > CURRENT_DATE)) OR (contact_type.om_validite_debut <= CURRENT_DATE AND (contact_type.om_validite_fin IS NULL OR contact_type.om_validite_fin > CURRENT_DATE))) ORDER BY contact_type.libelle ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_contact_type_by_id() {
        return "SELECT contact_type.contact_type, contact_type.libelle FROM ".DB_PREFIXE."contact_type WHERE contact_type = <idx>";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_etablissement() {
        return "SELECT etablissement.etablissement, etablissement.libelle FROM ".DB_PREFIXE."etablissement WHERE ((etablissement.om_validite_debut IS NULL AND (etablissement.om_validite_fin IS NULL OR etablissement.om_validite_fin > CURRENT_DATE)) OR (etablissement.om_validite_debut <= CURRENT_DATE AND (etablissement.om_validite_fin IS NULL OR etablissement.om_validite_fin > CURRENT_DATE))) ORDER BY etablissement.libelle ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_etablissement_by_id() {
        return "SELECT etablissement.etablissement, etablissement.libelle FROM ".DB_PREFIXE."etablissement WHERE etablissement = <idx>";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_service() {
        return "SELECT service.service, service.libelle FROM ".DB_PREFIXE."service WHERE ((service.om_validite_debut IS NULL AND (service.om_validite_fin IS NULL OR service.om_validite_fin > CURRENT_DATE)) OR (service.om_validite_debut <= CURRENT_DATE AND (service.om_validite_fin IS NULL OR service.om_validite_fin > CURRENT_DATE))) ORDER BY service.libelle ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_service_by_id() {
        return "SELECT service.service, service.libelle FROM ".DB_PREFIXE."service WHERE service = <idx>";
    }




    function setvalF($val = array()) {
        //affectation valeur formulaire
        if (!is_numeric($val['contact'])) {
            $this->valF['contact'] = ""; // -> requis
        } else {
            $this->valF['contact'] = $val['contact'];
        }
        if (!is_numeric($val['etablissement'])) {
            $this->valF['etablissement'] = NULL;
        } else {
            $this->valF['etablissement'] = $val['etablissement'];
        }
        if (!is_numeric($val['contact_type'])) {
            $this->valF['contact_type'] = NULL;
        } else {
            $this->valF['contact_type'] = $val['contact_type'];
        }
        if (!is_numeric($val['civilite'])) {
            $this->valF['civilite'] = NULL;
        } else {
            $this->valF['civilite'] = $val['civilite'];
        }
        if ($val['nom'] == "") {
            $this->valF['nom'] = NULL;
        } else {
            $this->valF['nom'] = $val['nom'];
        }
        if ($val['prenom'] == "") {
            $this->valF['prenom'] = NULL;
        } else {
            $this->valF['prenom'] = $val['prenom'];
        }
        if ($val['titre'] == "") {
            $this->valF['titre'] = NULL;
        } else {
            $this->valF['titre'] = $val['titre'];
        }
        if ($val['telephone'] == "") {
            $this->valF['telephone'] = NULL;
        } else {
            $this->valF['telephone'] = $val['telephone'];
        }
        if ($val['mobile'] == "") {
            $this->valF['mobile'] = NULL;
        } else {
            $this->valF['mobile'] = $val['mobile'];
        }
        if ($val['fax'] == "") {
            $this->valF['fax'] = NULL;
        } else {
            $this->valF['fax'] = $val['fax'];
        }
        if ($val['courriel'] == "") {
            $this->valF['courriel'] = NULL;
        } else {
            $this->valF['courriel'] = $val['courriel'];
        }
        if ($val['om_validite_debut'] != "") {
            $this->valF['om_validite_debut'] = $this->dateDB($val['om_validite_debut']);
        } else {
            $this->valF['om_validite_debut'] = NULL;
        }
        if ($val['om_validite_fin'] != "") {
            $this->valF['om_validite_fin'] = $this->dateDB($val['om_validite_fin']);
        } else {
            $this->valF['om_validite_fin'] = NULL;
        }
        if (!is_numeric($val['adresse_numero'])) {
            $this->valF['adresse_numero'] = NULL;
        } else {
            $this->valF['adresse_numero'] = $val['adresse_numero'];
        }
        if ($val['adresse_numero2'] == "") {
            $this->valF['adresse_numero2'] = NULL;
        } else {
            $this->valF['adresse_numero2'] = $val['adresse_numero2'];
        }
        if ($val['adresse_voie'] == "") {
            $this->valF['adresse_voie'] = NULL;
        } else {
            $this->valF['adresse_voie'] = $val['adresse_voie'];
        }
        if ($val['adresse_complement'] == "") {
            $this->valF['adresse_complement'] = NULL;
        } else {
            $this->valF['adresse_complement'] = $val['adresse_complement'];
        }
        if ($val['adresse_cp'] == "") {
            $this->valF['adresse_cp'] = NULL;
        } else {
            $this->valF['adresse_cp'] = $val['adresse_cp'];
        }
        if ($val['adresse_ville'] == "") {
            $this->valF['adresse_ville'] = NULL;
        } else {
            $this->valF['adresse_ville'] = $val['adresse_ville'];
        }
        if ($val['lieu_dit'] == "") {
            $this->valF['lieu_dit'] = NULL;
        } else {
            $this->valF['lieu_dit'] = $val['lieu_dit'];
        }
        if ($val['boite_postale'] == "") {
            $this->valF['boite_postale'] = NULL;
        } else {
            $this->valF['boite_postale'] = $val['boite_postale'];
        }
        if ($val['cedex'] == "") {
            $this->valF['cedex'] = NULL;
        } else {
            $this->valF['cedex'] = $val['cedex'];
        }
        if ($val['pays'] == "") {
            $this->valF['pays'] = NULL;
        } else {
            $this->valF['pays'] = $val['pays'];
        }
        if ($val['qualite'] == "") {
            $this->valF['qualite'] = NULL;
        } else {
            $this->valF['qualite'] = $val['qualite'];
        }
        if ($val['denomination'] == "") {
            $this->valF['denomination'] = NULL;
        } else {
            $this->valF['denomination'] = $val['denomination'];
        }
        if ($val['raison_sociale'] == "") {
            $this->valF['raison_sociale'] = NULL;
        } else {
            $this->valF['raison_sociale'] = $val['raison_sociale'];
        }
        if ($val['siret'] == "") {
            $this->valF['siret'] = NULL;
        } else {
            $this->valF['siret'] = $val['siret'];
        }
        if ($val['categorie_juridique'] == "") {
            $this->valF['categorie_juridique'] = NULL;
        } else {
            $this->valF['categorie_juridique'] = $val['categorie_juridique'];
        }
        if ($val['reception_convocation'] == 1 || $val['reception_convocation'] == "t" || $val['reception_convocation'] == "Oui") {
            $this->valF['reception_convocation'] = true;
        } else {
            $this->valF['reception_convocation'] = false;
        }
        if (!is_numeric($val['service'])) {
            $this->valF['service'] = NULL;
        } else {
            $this->valF['service'] = $val['service'];
        }
        if ($val['reception_programmation'] == 1 || $val['reception_programmation'] == "t" || $val['reception_programmation'] == "Oui") {
            $this->valF['reception_programmation'] = true;
        } else {
            $this->valF['reception_programmation'] = false;
        }
        if ($val['reception_commission'] == 1 || $val['reception_commission'] == "t" || $val['reception_commission'] == "Oui") {
            $this->valF['reception_commission'] = true;
        } else {
            $this->valF['reception_commission'] = false;
        }
    }

    //=================================================
    //cle primaire automatique [automatic primary key]
    //==================================================

    function setId(&$dnu1 = null) {
    //numero automatique
        $this->valF[$this->clePrimaire] = $this->f->db->nextId(DB_PREFIXE.$this->table);
    }

    function setValFAjout($val = array()) {
    //numero automatique -> pas de controle ajout cle primaire
    }

    function verifierAjout($val = array(), &$dnu1 = null) {
    //numero automatique -> pas de verfication de cle primaire
    }
    /**
     * Methode verifier
     */
    function verifier($val = array(), &$dnu1 = null, $dnu2 = null) {
        // On appelle la methode de la classe parent
        parent::verifier($val, $this->f->db, null);

        // gestion des dates de validites
        $date_debut = $this->valF['om_validite_debut'];
        $date_fin = $this->valF['om_validite_fin'];

        if ($date_debut != '' and $date_fin != '') {
        
            $date_debut = explode('-', $this->valF['om_validite_debut']);
            $date_fin = explode('-', $this->valF['om_validite_fin']);

            $time_debut = mktime(0, 0, 0, $date_debut[1], $date_debut[2],
                                 $date_debut[0]);
            $time_fin = mktime(0, 0, 0, $date_fin[1], $date_fin[2],
                                 $date_fin[0]);

            if ($time_debut > $time_fin or $time_debut == $time_fin) {
                $this->correct = false;
                $this->addToMessage(__('La date de fin de validite doit etre future a la de debut de validite.'));
            }
        }
    }


    //==========================
    // Formulaire  [form]
    //==========================
    /**
     *
     */
    function setType(&$form, $maj) {
        // Récupération du mode de l'action
        $crud = $this->get_action_crud($maj);

        // MODE AJOUTER
        if ($maj == 0 || $crud == 'create') {
            $form->setType("contact", "hidden");
            if ($this->is_in_context_of_foreign_key("etablissement", $this->retourformulaire)) {
                $form->setType("etablissement", "selecthiddenstatic");
            } else {
                $form->setType("etablissement", "select");
            }
            if ($this->is_in_context_of_foreign_key("contact_type", $this->retourformulaire)) {
                $form->setType("contact_type", "selecthiddenstatic");
            } else {
                $form->setType("contact_type", "select");
            }
            if ($this->is_in_context_of_foreign_key("contact_civilite", $this->retourformulaire)) {
                $form->setType("civilite", "selecthiddenstatic");
            } else {
                $form->setType("civilite", "select");
            }
            $form->setType("nom", "text");
            $form->setType("prenom", "text");
            $form->setType("titre", "text");
            $form->setType("telephone", "text");
            $form->setType("mobile", "text");
            $form->setType("fax", "text");
            $form->setType("courriel", "text");
            if ($this->f->isAccredited(array($this->table."_modifier_validite", $this->table, ))) {
                $form->setType("om_validite_debut", "date");
            } else {
                $form->setType("om_validite_debut", "hiddenstaticdate");
            }
            if ($this->f->isAccredited(array($this->table."_modifier_validite", $this->table, ))) {
                $form->setType("om_validite_fin", "date");
            } else {
                $form->setType("om_validite_fin", "hiddenstaticdate");
            }
            $form->setType("adresse_numero", "text");
            $form->setType("adresse_numero2", "text");
            $form->setType("adresse_voie", "text");
            $form->setType("adresse_complement", "text");
            $form->setType("adresse_cp", "text");
            $form->setType("adresse_ville", "text");
            $form->setType("lieu_dit", "text");
            $form->setType("boite_postale", "text");
            $form->setType("cedex", "text");
            $form->setType("pays", "text");
            $form->setType("qualite", "text");
            $form->setType("denomination", "text");
            $form->setType("raison_sociale", "text");
            $form->setType("siret", "text");
            $form->setType("categorie_juridique", "text");
            $form->setType("reception_convocation", "checkbox");
            if ($this->is_in_context_of_foreign_key("service", $this->retourformulaire)) {
                $form->setType("service", "selecthiddenstatic");
            } else {
                $form->setType("service", "select");
            }
            $form->setType("reception_programmation", "checkbox");
            $form->setType("reception_commission", "checkbox");
        }

        // MDOE MODIFIER
        if ($maj == 1 || $crud == 'update') {
            $form->setType("contact", "hiddenstatic");
            if ($this->is_in_context_of_foreign_key("etablissement", $this->retourformulaire)) {
                $form->setType("etablissement", "selecthiddenstatic");
            } else {
                $form->setType("etablissement", "select");
            }
            if ($this->is_in_context_of_foreign_key("contact_type", $this->retourformulaire)) {
                $form->setType("contact_type", "selecthiddenstatic");
            } else {
                $form->setType("contact_type", "select");
            }
            if ($this->is_in_context_of_foreign_key("contact_civilite", $this->retourformulaire)) {
                $form->setType("civilite", "selecthiddenstatic");
            } else {
                $form->setType("civilite", "select");
            }
            $form->setType("nom", "text");
            $form->setType("prenom", "text");
            $form->setType("titre", "text");
            $form->setType("telephone", "text");
            $form->setType("mobile", "text");
            $form->setType("fax", "text");
            $form->setType("courriel", "text");
            if ($this->f->isAccredited(array($this->table."_modifier_validite", $this->table, ))) {
                $form->setType("om_validite_debut", "date");
            } else {
                $form->setType("om_validite_debut", "hiddenstaticdate");
            }
            if ($this->f->isAccredited(array($this->table."_modifier_validite", $this->table, ))) {
                $form->setType("om_validite_fin", "date");
            } else {
                $form->setType("om_validite_fin", "hiddenstaticdate");
            }
            $form->setType("adresse_numero", "text");
            $form->setType("adresse_numero2", "text");
            $form->setType("adresse_voie", "text");
            $form->setType("adresse_complement", "text");
            $form->setType("adresse_cp", "text");
            $form->setType("adresse_ville", "text");
            $form->setType("lieu_dit", "text");
            $form->setType("boite_postale", "text");
            $form->setType("cedex", "text");
            $form->setType("pays", "text");
            $form->setType("qualite", "text");
            $form->setType("denomination", "text");
            $form->setType("raison_sociale", "text");
            $form->setType("siret", "text");
            $form->setType("categorie_juridique", "text");
            $form->setType("reception_convocation", "checkbox");
            if ($this->is_in_context_of_foreign_key("service", $this->retourformulaire)) {
                $form->setType("service", "selecthiddenstatic");
            } else {
                $form->setType("service", "select");
            }
            $form->setType("reception_programmation", "checkbox");
            $form->setType("reception_commission", "checkbox");
        }

        // MODE SUPPRIMER
        if ($maj == 2 || $crud == 'delete') {
            $form->setType("contact", "hiddenstatic");
            $form->setType("etablissement", "selectstatic");
            $form->setType("contact_type", "selectstatic");
            $form->setType("civilite", "selectstatic");
            $form->setType("nom", "hiddenstatic");
            $form->setType("prenom", "hiddenstatic");
            $form->setType("titre", "hiddenstatic");
            $form->setType("telephone", "hiddenstatic");
            $form->setType("mobile", "hiddenstatic");
            $form->setType("fax", "hiddenstatic");
            $form->setType("courriel", "hiddenstatic");
            $form->setType("om_validite_debut", "hiddenstatic");
            $form->setType("om_validite_fin", "hiddenstatic");
            $form->setType("adresse_numero", "hiddenstatic");
            $form->setType("adresse_numero2", "hiddenstatic");
            $form->setType("adresse_voie", "hiddenstatic");
            $form->setType("adresse_complement", "hiddenstatic");
            $form->setType("adresse_cp", "hiddenstatic");
            $form->setType("adresse_ville", "hiddenstatic");
            $form->setType("lieu_dit", "hiddenstatic");
            $form->setType("boite_postale", "hiddenstatic");
            $form->setType("cedex", "hiddenstatic");
            $form->setType("pays", "hiddenstatic");
            $form->setType("qualite", "hiddenstatic");
            $form->setType("denomination", "hiddenstatic");
            $form->setType("raison_sociale", "hiddenstatic");
            $form->setType("siret", "hiddenstatic");
            $form->setType("categorie_juridique", "hiddenstatic");
            $form->setType("reception_convocation", "hiddenstatic");
            $form->setType("service", "selectstatic");
            $form->setType("reception_programmation", "hiddenstatic");
            $form->setType("reception_commission", "hiddenstatic");
        }

        // MODE CONSULTER
        if ($maj == 3 || $crud == 'read') {
            $form->setType("contact", "static");
            $form->setType("etablissement", "selectstatic");
            $form->setType("contact_type", "selectstatic");
            $form->setType("civilite", "selectstatic");
            $form->setType("nom", "static");
            $form->setType("prenom", "static");
            $form->setType("titre", "static");
            $form->setType("telephone", "static");
            $form->setType("mobile", "static");
            $form->setType("fax", "static");
            $form->setType("courriel", "static");
            $form->setType("om_validite_debut", "datestatic");
            $form->setType("om_validite_fin", "datestatic");
            $form->setType("adresse_numero", "static");
            $form->setType("adresse_numero2", "static");
            $form->setType("adresse_voie", "static");
            $form->setType("adresse_complement", "static");
            $form->setType("adresse_cp", "static");
            $form->setType("adresse_ville", "static");
            $form->setType("lieu_dit", "static");
            $form->setType("boite_postale", "static");
            $form->setType("cedex", "static");
            $form->setType("pays", "static");
            $form->setType("qualite", "static");
            $form->setType("denomination", "static");
            $form->setType("raison_sociale", "static");
            $form->setType("siret", "static");
            $form->setType("categorie_juridique", "static");
            $form->setType("reception_convocation", "checkboxstatic");
            $form->setType("service", "selectstatic");
            $form->setType("reception_programmation", "checkboxstatic");
            $form->setType("reception_commission", "checkboxstatic");
        }

    }


    function setOnchange(&$form, $maj) {
    //javascript controle client
        $form->setOnchange('contact','VerifNum(this)');
        $form->setOnchange('etablissement','VerifNum(this)');
        $form->setOnchange('contact_type','VerifNum(this)');
        $form->setOnchange('civilite','VerifNum(this)');
        $form->setOnchange('om_validite_debut','fdate(this)');
        $form->setOnchange('om_validite_fin','fdate(this)');
        $form->setOnchange('adresse_numero','VerifNum(this)');
        $form->setOnchange('service','VerifNum(this)');
    }
    /**
     * Methode setTaille
     */
    function setTaille(&$form, $maj) {
        $form->setTaille("contact", 11);
        $form->setTaille("etablissement", 11);
        $form->setTaille("contact_type", 11);
        $form->setTaille("civilite", 11);
        $form->setTaille("nom", 30);
        $form->setTaille("prenom", 30);
        $form->setTaille("titre", 30);
        $form->setTaille("telephone", 30);
        $form->setTaille("mobile", 30);
        $form->setTaille("fax", 30);
        $form->setTaille("courriel", 30);
        $form->setTaille("om_validite_debut", 12);
        $form->setTaille("om_validite_fin", 12);
        $form->setTaille("adresse_numero", 11);
        $form->setTaille("adresse_numero2", 10);
        $form->setTaille("adresse_voie", 30);
        $form->setTaille("adresse_complement", 30);
        $form->setTaille("adresse_cp", 10);
        $form->setTaille("adresse_ville", 30);
        $form->setTaille("lieu_dit", 30);
        $form->setTaille("boite_postale", 10);
        $form->setTaille("cedex", 10);
        $form->setTaille("pays", 30);
        $form->setTaille("qualite", 30);
        $form->setTaille("denomination", 30);
        $form->setTaille("raison_sociale", 30);
        $form->setTaille("siret", 15);
        $form->setTaille("categorie_juridique", 15);
        $form->setTaille("reception_convocation", 1);
        $form->setTaille("service", 11);
        $form->setTaille("reception_programmation", 1);
        $form->setTaille("reception_commission", 1);
    }

    /**
     * Methode setMax
     */
    function setMax(&$form, $maj) {
        $form->setMax("contact", 11);
        $form->setMax("etablissement", 11);
        $form->setMax("contact_type", 11);
        $form->setMax("civilite", 11);
        $form->setMax("nom", 100);
        $form->setMax("prenom", 100);
        $form->setMax("titre", 100);
        $form->setMax("telephone", 40);
        $form->setMax("mobile", 40);
        $form->setMax("fax", 40);
        $form->setMax("courriel", 100);
        $form->setMax("om_validite_debut", 12);
        $form->setMax("om_validite_fin", 12);
        $form->setMax("adresse_numero", 11);
        $form->setMax("adresse_numero2", 10);
        $form->setMax("adresse_voie", 255);
        $form->setMax("adresse_complement", 40);
        $form->setMax("adresse_cp", 6);
        $form->setMax("adresse_ville", 40);
        $form->setMax("lieu_dit", 39);
        $form->setMax("boite_postale", 5);
        $form->setMax("cedex", 5);
        $form->setMax("pays", 40);
        $form->setMax("qualite", 40);
        $form->setMax("denomination", 40);
        $form->setMax("raison_sociale", 50);
        $form->setMax("siret", 15);
        $form->setMax("categorie_juridique", 15);
        $form->setMax("reception_convocation", 1);
        $form->setMax("service", 11);
        $form->setMax("reception_programmation", 1);
        $form->setMax("reception_commission", 1);
    }


    function setLib(&$form, $maj) {
    //libelle des champs
        $form->setLib('contact', __('contact'));
        $form->setLib('etablissement', __('etablissement'));
        $form->setLib('contact_type', __('contact_type'));
        $form->setLib('civilite', __('civilite'));
        $form->setLib('nom', __('nom'));
        $form->setLib('prenom', __('prenom'));
        $form->setLib('titre', __('titre'));
        $form->setLib('telephone', __('telephone'));
        $form->setLib('mobile', __('mobile'));
        $form->setLib('fax', __('fax'));
        $form->setLib('courriel', __('courriel'));
        $form->setLib('om_validite_debut', __('om_validite_debut'));
        $form->setLib('om_validite_fin', __('om_validite_fin'));
        $form->setLib('adresse_numero', __('adresse_numero'));
        $form->setLib('adresse_numero2', __('adresse_numero2'));
        $form->setLib('adresse_voie', __('adresse_voie'));
        $form->setLib('adresse_complement', __('adresse_complement'));
        $form->setLib('adresse_cp', __('adresse_cp'));
        $form->setLib('adresse_ville', __('adresse_ville'));
        $form->setLib('lieu_dit', __('lieu_dit'));
        $form->setLib('boite_postale', __('boite_postale'));
        $form->setLib('cedex', __('cedex'));
        $form->setLib('pays', __('pays'));
        $form->setLib('qualite', __('qualite'));
        $form->setLib('denomination', __('denomination'));
        $form->setLib('raison_sociale', __('raison_sociale'));
        $form->setLib('siret', __('siret'));
        $form->setLib('categorie_juridique', __('categorie_juridique'));
        $form->setLib('reception_convocation', __('reception_convocation'));
        $form->setLib('service', __('service'));
        $form->setLib('reception_programmation', __('reception_programmation'));
        $form->setLib('reception_commission', __('reception_commission'));
    }
    /**
     *
     */
    function setSelect(&$form, $maj, &$dnu1 = null, $dnu2 = null) {

        // civilite
        $this->init_select(
            $form, 
            $this->f->db,
            $maj,
            null,
            "civilite",
            $this->get_var_sql_forminc__sql("civilite"),
            $this->get_var_sql_forminc__sql("civilite_by_id"),
            false
        );
        // contact_type
        $this->init_select(
            $form, 
            $this->f->db,
            $maj,
            null,
            "contact_type",
            $this->get_var_sql_forminc__sql("contact_type"),
            $this->get_var_sql_forminc__sql("contact_type_by_id"),
            true
        );
        // etablissement
        $this->init_select(
            $form, 
            $this->f->db,
            $maj,
            null,
            "etablissement",
            $this->get_var_sql_forminc__sql("etablissement"),
            $this->get_var_sql_forminc__sql("etablissement_by_id"),
            true
        );
        // service
        $this->init_select(
            $form, 
            $this->f->db,
            $maj,
            null,
            "service",
            $this->get_var_sql_forminc__sql("service"),
            $this->get_var_sql_forminc__sql("service_by_id"),
            true
        );
    }


    //==================================
    // sous Formulaire
    //==================================
    

    function setValsousformulaire(&$form, $maj, $validation, $idxformulaire, $retourformulaire, $typeformulaire, &$dnu1 = null, $dnu2 = null) {
        $this->retourformulaire = $retourformulaire;
        if($validation == 0) {
            if($this->is_in_context_of_foreign_key('contact_civilite', $this->retourformulaire))
                $form->setVal('civilite', $idxformulaire);
            if($this->is_in_context_of_foreign_key('contact_type', $this->retourformulaire))
                $form->setVal('contact_type', $idxformulaire);
            if($this->is_in_context_of_foreign_key('etablissement', $this->retourformulaire))
                $form->setVal('etablissement', $idxformulaire);
            if($this->is_in_context_of_foreign_key('service', $this->retourformulaire))
                $form->setVal('service', $idxformulaire);
        }// fin validation
        $this->set_form_default_values($form, $maj, $validation);
    }// fin setValsousformulaire

    //==================================
    // cle secondaire
    //==================================
    
    /**
     * Methode clesecondaire
     */
    function cleSecondaire($id, &$dnu1 = null, $val = array(), $dnu2 = null) {
        // On appelle la methode de la classe parent
        parent::cleSecondaire($id);
        // Verification de la cle secondaire : lien_courrier_contact
        $this->rechercheTable($this->f->db, "lien_courrier_contact", "contact", $id);
        // Verification de la cle secondaire : lien_dossier_coordination_contact
        $this->rechercheTable($this->f->db, "lien_dossier_coordination_contact", "contact", $id);
    }


}
