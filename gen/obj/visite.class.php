<?php
//$Id$ 
//gen openMairie le 03/05/2018 09:12

require_once "../obj/om_dbform.class.php";

class visite_gen extends om_dbform {

    protected $_absolute_class_name = "visite";

    var $table = "visite";
    var $clePrimaire = "visite";
    var $typeCle = "N";
    var $required_field = array(
        "acteur",
        "date_creation",
        "date_visite",
        "dossier_instruction",
        "heure_debut",
        "heure_fin",
        "visite",
        "visite_etat"
    );
    
    var $foreign_keys_extended = array(
        "acteur" => array("acteur", ),
        "courrier" => array("courrier", "courrier_a_editer", "courrier_attente_signature", "courrier_attente_retour_ar", ),
        "dossier_instruction" => array("dossier_instruction", "dossier_instruction_mes_plans", "dossier_instruction_mes_visites", "dossier_instruction_tous_plans", "dossier_instruction_tous_visites", "dossier_instruction_a_qualifier", "dossier_instruction_a_affecter", ),
        "programmation" => array("programmation", ),
        "visite_etat" => array("visite_etat", ),
        "visite_motif_annulation" => array("visite_motif_annulation", ),
    );
    
    /**
     *
     * @return string
     */
    function get_default_libelle() {
        return $this->getVal($this->clePrimaire)."&nbsp;".$this->getVal("visite_motif_annulation");
    }

    /**
     *
     * @return array
     */
    function get_var_sql_forminc__champs() {
        return array(
            "visite",
            "visite_motif_annulation",
            "visite_etat",
            "dossier_instruction",
            "acteur",
            "programmation",
            "date_creation",
            "date_annulation",
            "observation",
            "programmation_version_creation",
            "programmation_version_annulation",
            "heure_debut",
            "heure_fin",
            "convocation_exploitants",
            "date_visite",
            "a_poursuivre",
            "courrier_convocation_exploitants",
            "courrier_annulation",
            "programmation_version_modification",
        );
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_acteur() {
        return "SELECT acteur.acteur, acteur.nom_prenom FROM ".DB_PREFIXE."acteur WHERE ((acteur.om_validite_debut IS NULL AND (acteur.om_validite_fin IS NULL OR acteur.om_validite_fin > CURRENT_DATE)) OR (acteur.om_validite_debut <= CURRENT_DATE AND (acteur.om_validite_fin IS NULL OR acteur.om_validite_fin > CURRENT_DATE))) ORDER BY acteur.nom_prenom ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_acteur_by_id() {
        return "SELECT acteur.acteur, acteur.nom_prenom FROM ".DB_PREFIXE."acteur WHERE acteur = <idx>";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_courrier_annulation() {
        return "SELECT courrier.courrier, courrier.etablissement FROM ".DB_PREFIXE."courrier ORDER BY courrier.etablissement ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_courrier_annulation_by_id() {
        return "SELECT courrier.courrier, courrier.etablissement FROM ".DB_PREFIXE."courrier WHERE courrier = <idx>";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_courrier_convocation_exploitants() {
        return "SELECT courrier.courrier, courrier.etablissement FROM ".DB_PREFIXE."courrier ORDER BY courrier.etablissement ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_courrier_convocation_exploitants_by_id() {
        return "SELECT courrier.courrier, courrier.etablissement FROM ".DB_PREFIXE."courrier WHERE courrier = <idx>";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_dossier_instruction() {
        return "SELECT dossier_instruction.dossier_instruction, dossier_instruction.libelle FROM ".DB_PREFIXE."dossier_instruction ORDER BY dossier_instruction.libelle ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_dossier_instruction_by_id() {
        return "SELECT dossier_instruction.dossier_instruction, dossier_instruction.libelle FROM ".DB_PREFIXE."dossier_instruction WHERE dossier_instruction = <idx>";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_programmation() {
        return "SELECT programmation.programmation, programmation.annee FROM ".DB_PREFIXE."programmation ORDER BY programmation.annee ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_programmation_by_id() {
        return "SELECT programmation.programmation, programmation.annee FROM ".DB_PREFIXE."programmation WHERE programmation = <idx>";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_visite_etat() {
        return "SELECT visite_etat.visite_etat, visite_etat.libelle FROM ".DB_PREFIXE."visite_etat WHERE ((visite_etat.om_validite_debut IS NULL AND (visite_etat.om_validite_fin IS NULL OR visite_etat.om_validite_fin > CURRENT_DATE)) OR (visite_etat.om_validite_debut <= CURRENT_DATE AND (visite_etat.om_validite_fin IS NULL OR visite_etat.om_validite_fin > CURRENT_DATE))) ORDER BY visite_etat.libelle ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_visite_etat_by_id() {
        return "SELECT visite_etat.visite_etat, visite_etat.libelle FROM ".DB_PREFIXE."visite_etat WHERE visite_etat = <idx>";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_visite_motif_annulation() {
        return "SELECT visite_motif_annulation.visite_motif_annulation, visite_motif_annulation.libelle FROM ".DB_PREFIXE."visite_motif_annulation WHERE ((visite_motif_annulation.om_validite_debut IS NULL AND (visite_motif_annulation.om_validite_fin IS NULL OR visite_motif_annulation.om_validite_fin > CURRENT_DATE)) OR (visite_motif_annulation.om_validite_debut <= CURRENT_DATE AND (visite_motif_annulation.om_validite_fin IS NULL OR visite_motif_annulation.om_validite_fin > CURRENT_DATE))) ORDER BY visite_motif_annulation.libelle ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_visite_motif_annulation_by_id() {
        return "SELECT visite_motif_annulation.visite_motif_annulation, visite_motif_annulation.libelle FROM ".DB_PREFIXE."visite_motif_annulation WHERE visite_motif_annulation = <idx>";
    }




    function setvalF($val = array()) {
        //affectation valeur formulaire
        if (!is_numeric($val['visite'])) {
            $this->valF['visite'] = ""; // -> requis
        } else {
            $this->valF['visite'] = $val['visite'];
        }
        if (!is_numeric($val['visite_motif_annulation'])) {
            $this->valF['visite_motif_annulation'] = NULL;
        } else {
            $this->valF['visite_motif_annulation'] = $val['visite_motif_annulation'];
        }
        if (!is_numeric($val['visite_etat'])) {
            $this->valF['visite_etat'] = ""; // -> requis
        } else {
            $this->valF['visite_etat'] = $val['visite_etat'];
        }
        if (!is_numeric($val['dossier_instruction'])) {
            $this->valF['dossier_instruction'] = ""; // -> requis
        } else {
            $this->valF['dossier_instruction'] = $val['dossier_instruction'];
        }
        if (!is_numeric($val['acteur'])) {
            $this->valF['acteur'] = ""; // -> requis
        } else {
            $this->valF['acteur'] = $val['acteur'];
        }
        if (!is_numeric($val['programmation'])) {
            $this->valF['programmation'] = NULL;
        } else {
            $this->valF['programmation'] = $val['programmation'];
        }
        if ($val['date_creation'] != "") {
            $this->valF['date_creation'] = $this->dateDB($val['date_creation']);
        }
        if ($val['date_annulation'] != "") {
            $this->valF['date_annulation'] = $this->dateDB($val['date_annulation']);
        } else {
            $this->valF['date_annulation'] = NULL;
        }
            $this->valF['observation'] = $val['observation'];
        if (!is_numeric($val['programmation_version_creation'])) {
            $this->valF['programmation_version_creation'] = NULL;
        } else {
            $this->valF['programmation_version_creation'] = $val['programmation_version_creation'];
        }
        if (!is_numeric($val['programmation_version_annulation'])) {
            $this->valF['programmation_version_annulation'] = NULL;
        } else {
            $this->valF['programmation_version_annulation'] = $val['programmation_version_annulation'];
        }
        $this->valF['heure_debut'] = $val['heure_debut'];
        $this->valF['heure_fin'] = $val['heure_fin'];
        if ($val['convocation_exploitants'] == "") {
            $this->valF['convocation_exploitants'] = NULL;
        } else {
            $this->valF['convocation_exploitants'] = $val['convocation_exploitants'];
        }
        if ($val['date_visite'] != "") {
            $this->valF['date_visite'] = $this->dateDB($val['date_visite']);
        }
        if ($val['a_poursuivre'] == 1 || $val['a_poursuivre'] == "t" || $val['a_poursuivre'] == "Oui") {
            $this->valF['a_poursuivre'] = true;
        } else {
            $this->valF['a_poursuivre'] = false;
        }
        if (!is_numeric($val['courrier_convocation_exploitants'])) {
            $this->valF['courrier_convocation_exploitants'] = NULL;
        } else {
            $this->valF['courrier_convocation_exploitants'] = $val['courrier_convocation_exploitants'];
        }
        if (!is_numeric($val['courrier_annulation'])) {
            $this->valF['courrier_annulation'] = NULL;
        } else {
            $this->valF['courrier_annulation'] = $val['courrier_annulation'];
        }
        if (!is_numeric($val['programmation_version_modification'])) {
            $this->valF['programmation_version_modification'] = NULL;
        } else {
            $this->valF['programmation_version_modification'] = $val['programmation_version_modification'];
        }
    }

    //=================================================
    //cle primaire automatique [automatic primary key]
    //==================================================

    function setId(&$dnu1 = null) {
    //numero automatique
        $this->valF[$this->clePrimaire] = $this->f->db->nextId(DB_PREFIXE.$this->table);
    }

    function setValFAjout($val = array()) {
    //numero automatique -> pas de controle ajout cle primaire
    }

    function verifierAjout($val = array(), &$dnu1 = null) {
    //numero automatique -> pas de verfication de cle primaire
    }

    //==========================
    // Formulaire  [form]
    //==========================
    /**
     *
     */
    function setType(&$form, $maj) {
        // Récupération du mode de l'action
        $crud = $this->get_action_crud($maj);

        // MODE AJOUTER
        if ($maj == 0 || $crud == 'create') {
            $form->setType("visite", "hidden");
            if ($this->is_in_context_of_foreign_key("visite_motif_annulation", $this->retourformulaire)) {
                $form->setType("visite_motif_annulation", "selecthiddenstatic");
            } else {
                $form->setType("visite_motif_annulation", "select");
            }
            if ($this->is_in_context_of_foreign_key("visite_etat", $this->retourformulaire)) {
                $form->setType("visite_etat", "selecthiddenstatic");
            } else {
                $form->setType("visite_etat", "select");
            }
            if ($this->is_in_context_of_foreign_key("dossier_instruction", $this->retourformulaire)) {
                $form->setType("dossier_instruction", "selecthiddenstatic");
            } else {
                $form->setType("dossier_instruction", "select");
            }
            if ($this->is_in_context_of_foreign_key("acteur", $this->retourformulaire)) {
                $form->setType("acteur", "selecthiddenstatic");
            } else {
                $form->setType("acteur", "select");
            }
            if ($this->is_in_context_of_foreign_key("programmation", $this->retourformulaire)) {
                $form->setType("programmation", "selecthiddenstatic");
            } else {
                $form->setType("programmation", "select");
            }
            $form->setType("date_creation", "date");
            $form->setType("date_annulation", "date");
            $form->setType("observation", "textarea");
            $form->setType("programmation_version_creation", "text");
            $form->setType("programmation_version_annulation", "text");
            $form->setType("heure_debut", "text");
            $form->setType("heure_fin", "text");
            $form->setType("convocation_exploitants", "text");
            $form->setType("date_visite", "date");
            $form->setType("a_poursuivre", "checkbox");
            if ($this->is_in_context_of_foreign_key("courrier", $this->retourformulaire)) {
                $form->setType("courrier_convocation_exploitants", "selecthiddenstatic");
            } else {
                $form->setType("courrier_convocation_exploitants", "select");
            }
            if ($this->is_in_context_of_foreign_key("courrier", $this->retourformulaire)) {
                $form->setType("courrier_annulation", "selecthiddenstatic");
            } else {
                $form->setType("courrier_annulation", "select");
            }
            $form->setType("programmation_version_modification", "text");
        }

        // MDOE MODIFIER
        if ($maj == 1 || $crud == 'update') {
            $form->setType("visite", "hiddenstatic");
            if ($this->is_in_context_of_foreign_key("visite_motif_annulation", $this->retourformulaire)) {
                $form->setType("visite_motif_annulation", "selecthiddenstatic");
            } else {
                $form->setType("visite_motif_annulation", "select");
            }
            if ($this->is_in_context_of_foreign_key("visite_etat", $this->retourformulaire)) {
                $form->setType("visite_etat", "selecthiddenstatic");
            } else {
                $form->setType("visite_etat", "select");
            }
            if ($this->is_in_context_of_foreign_key("dossier_instruction", $this->retourformulaire)) {
                $form->setType("dossier_instruction", "selecthiddenstatic");
            } else {
                $form->setType("dossier_instruction", "select");
            }
            if ($this->is_in_context_of_foreign_key("acteur", $this->retourformulaire)) {
                $form->setType("acteur", "selecthiddenstatic");
            } else {
                $form->setType("acteur", "select");
            }
            if ($this->is_in_context_of_foreign_key("programmation", $this->retourformulaire)) {
                $form->setType("programmation", "selecthiddenstatic");
            } else {
                $form->setType("programmation", "select");
            }
            $form->setType("date_creation", "date");
            $form->setType("date_annulation", "date");
            $form->setType("observation", "textarea");
            $form->setType("programmation_version_creation", "text");
            $form->setType("programmation_version_annulation", "text");
            $form->setType("heure_debut", "text");
            $form->setType("heure_fin", "text");
            $form->setType("convocation_exploitants", "text");
            $form->setType("date_visite", "date");
            $form->setType("a_poursuivre", "checkbox");
            if ($this->is_in_context_of_foreign_key("courrier", $this->retourformulaire)) {
                $form->setType("courrier_convocation_exploitants", "selecthiddenstatic");
            } else {
                $form->setType("courrier_convocation_exploitants", "select");
            }
            if ($this->is_in_context_of_foreign_key("courrier", $this->retourformulaire)) {
                $form->setType("courrier_annulation", "selecthiddenstatic");
            } else {
                $form->setType("courrier_annulation", "select");
            }
            $form->setType("programmation_version_modification", "text");
        }

        // MODE SUPPRIMER
        if ($maj == 2 || $crud == 'delete') {
            $form->setType("visite", "hiddenstatic");
            $form->setType("visite_motif_annulation", "selectstatic");
            $form->setType("visite_etat", "selectstatic");
            $form->setType("dossier_instruction", "selectstatic");
            $form->setType("acteur", "selectstatic");
            $form->setType("programmation", "selectstatic");
            $form->setType("date_creation", "hiddenstatic");
            $form->setType("date_annulation", "hiddenstatic");
            $form->setType("observation", "hiddenstatic");
            $form->setType("programmation_version_creation", "hiddenstatic");
            $form->setType("programmation_version_annulation", "hiddenstatic");
            $form->setType("heure_debut", "hiddenstatic");
            $form->setType("heure_fin", "hiddenstatic");
            $form->setType("convocation_exploitants", "hiddenstatic");
            $form->setType("date_visite", "hiddenstatic");
            $form->setType("a_poursuivre", "hiddenstatic");
            $form->setType("courrier_convocation_exploitants", "selectstatic");
            $form->setType("courrier_annulation", "selectstatic");
            $form->setType("programmation_version_modification", "hiddenstatic");
        }

        // MODE CONSULTER
        if ($maj == 3 || $crud == 'read') {
            $form->setType("visite", "static");
            $form->setType("visite_motif_annulation", "selectstatic");
            $form->setType("visite_etat", "selectstatic");
            $form->setType("dossier_instruction", "selectstatic");
            $form->setType("acteur", "selectstatic");
            $form->setType("programmation", "selectstatic");
            $form->setType("date_creation", "datestatic");
            $form->setType("date_annulation", "datestatic");
            $form->setType("observation", "textareastatic");
            $form->setType("programmation_version_creation", "static");
            $form->setType("programmation_version_annulation", "static");
            $form->setType("heure_debut", "static");
            $form->setType("heure_fin", "static");
            $form->setType("convocation_exploitants", "static");
            $form->setType("date_visite", "datestatic");
            $form->setType("a_poursuivre", "checkboxstatic");
            $form->setType("courrier_convocation_exploitants", "selectstatic");
            $form->setType("courrier_annulation", "selectstatic");
            $form->setType("programmation_version_modification", "static");
        }

    }


    function setOnchange(&$form, $maj) {
    //javascript controle client
        $form->setOnchange('visite','VerifNum(this)');
        $form->setOnchange('visite_motif_annulation','VerifNum(this)');
        $form->setOnchange('visite_etat','VerifNum(this)');
        $form->setOnchange('dossier_instruction','VerifNum(this)');
        $form->setOnchange('acteur','VerifNum(this)');
        $form->setOnchange('programmation','VerifNum(this)');
        $form->setOnchange('date_creation','fdate(this)');
        $form->setOnchange('date_annulation','fdate(this)');
        $form->setOnchange('programmation_version_creation','VerifNum(this)');
        $form->setOnchange('programmation_version_annulation','VerifNum(this)');
        $form->setOnchange('date_visite','fdate(this)');
        $form->setOnchange('courrier_convocation_exploitants','VerifNum(this)');
        $form->setOnchange('courrier_annulation','VerifNum(this)');
        $form->setOnchange('programmation_version_modification','VerifNum(this)');
    }
    /**
     * Methode setTaille
     */
    function setTaille(&$form, $maj) {
        $form->setTaille("visite", 11);
        $form->setTaille("visite_motif_annulation", 11);
        $form->setTaille("visite_etat", 11);
        $form->setTaille("dossier_instruction", 11);
        $form->setTaille("acteur", 11);
        $form->setTaille("programmation", 11);
        $form->setTaille("date_creation", 12);
        $form->setTaille("date_annulation", 12);
        $form->setTaille("observation", 80);
        $form->setTaille("programmation_version_creation", 11);
        $form->setTaille("programmation_version_annulation", 11);
        $form->setTaille("heure_debut", 10);
        $form->setTaille("heure_fin", 10);
        $form->setTaille("convocation_exploitants", 20);
        $form->setTaille("date_visite", 12);
        $form->setTaille("a_poursuivre", 1);
        $form->setTaille("courrier_convocation_exploitants", 11);
        $form->setTaille("courrier_annulation", 11);
        $form->setTaille("programmation_version_modification", 11);
    }

    /**
     * Methode setMax
     */
    function setMax(&$form, $maj) {
        $form->setMax("visite", 11);
        $form->setMax("visite_motif_annulation", 11);
        $form->setMax("visite_etat", 11);
        $form->setMax("dossier_instruction", 11);
        $form->setMax("acteur", 11);
        $form->setMax("programmation", 11);
        $form->setMax("date_creation", 12);
        $form->setMax("date_annulation", 12);
        $form->setMax("observation", 6);
        $form->setMax("programmation_version_creation", 11);
        $form->setMax("programmation_version_annulation", 11);
        $form->setMax("heure_debut", 5);
        $form->setMax("heure_fin", 5);
        $form->setMax("convocation_exploitants", 20);
        $form->setMax("date_visite", 12);
        $form->setMax("a_poursuivre", 1);
        $form->setMax("courrier_convocation_exploitants", 11);
        $form->setMax("courrier_annulation", 11);
        $form->setMax("programmation_version_modification", 11);
    }


    function setLib(&$form, $maj) {
    //libelle des champs
        $form->setLib('visite', __('visite'));
        $form->setLib('visite_motif_annulation', __('visite_motif_annulation'));
        $form->setLib('visite_etat', __('visite_etat'));
        $form->setLib('dossier_instruction', __('dossier_instruction'));
        $form->setLib('acteur', __('acteur'));
        $form->setLib('programmation', __('programmation'));
        $form->setLib('date_creation', __('date_creation'));
        $form->setLib('date_annulation', __('date_annulation'));
        $form->setLib('observation', __('observation'));
        $form->setLib('programmation_version_creation', __('programmation_version_creation'));
        $form->setLib('programmation_version_annulation', __('programmation_version_annulation'));
        $form->setLib('heure_debut', __('heure_debut'));
        $form->setLib('heure_fin', __('heure_fin'));
        $form->setLib('convocation_exploitants', __('convocation_exploitants'));
        $form->setLib('date_visite', __('date_visite'));
        $form->setLib('a_poursuivre', __('a_poursuivre'));
        $form->setLib('courrier_convocation_exploitants', __('courrier_convocation_exploitants'));
        $form->setLib('courrier_annulation', __('courrier_annulation'));
        $form->setLib('programmation_version_modification', __('programmation_version_modification'));
    }
    /**
     *
     */
    function setSelect(&$form, $maj, &$dnu1 = null, $dnu2 = null) {

        // acteur
        $this->init_select(
            $form, 
            $this->f->db,
            $maj,
            null,
            "acteur",
            $this->get_var_sql_forminc__sql("acteur"),
            $this->get_var_sql_forminc__sql("acteur_by_id"),
            true
        );
        // courrier_annulation
        $this->init_select(
            $form, 
            $this->f->db,
            $maj,
            null,
            "courrier_annulation",
            $this->get_var_sql_forminc__sql("courrier_annulation"),
            $this->get_var_sql_forminc__sql("courrier_annulation_by_id"),
            false
        );
        // courrier_convocation_exploitants
        $this->init_select(
            $form, 
            $this->f->db,
            $maj,
            null,
            "courrier_convocation_exploitants",
            $this->get_var_sql_forminc__sql("courrier_convocation_exploitants"),
            $this->get_var_sql_forminc__sql("courrier_convocation_exploitants_by_id"),
            false
        );
        // dossier_instruction
        $this->init_select(
            $form, 
            $this->f->db,
            $maj,
            null,
            "dossier_instruction",
            $this->get_var_sql_forminc__sql("dossier_instruction"),
            $this->get_var_sql_forminc__sql("dossier_instruction_by_id"),
            false
        );
        // programmation
        $this->init_select(
            $form, 
            $this->f->db,
            $maj,
            null,
            "programmation",
            $this->get_var_sql_forminc__sql("programmation"),
            $this->get_var_sql_forminc__sql("programmation_by_id"),
            false
        );
        // visite_etat
        $this->init_select(
            $form, 
            $this->f->db,
            $maj,
            null,
            "visite_etat",
            $this->get_var_sql_forminc__sql("visite_etat"),
            $this->get_var_sql_forminc__sql("visite_etat_by_id"),
            true
        );
        // visite_motif_annulation
        $this->init_select(
            $form, 
            $this->f->db,
            $maj,
            null,
            "visite_motif_annulation",
            $this->get_var_sql_forminc__sql("visite_motif_annulation"),
            $this->get_var_sql_forminc__sql("visite_motif_annulation_by_id"),
            true
        );
    }


    //==================================
    // sous Formulaire
    //==================================
    

    function setValsousformulaire(&$form, $maj, $validation, $idxformulaire, $retourformulaire, $typeformulaire, &$dnu1 = null, $dnu2 = null) {
        $this->retourformulaire = $retourformulaire;
        if($validation == 0) {
            if($this->is_in_context_of_foreign_key('acteur', $this->retourformulaire))
                $form->setVal('acteur', $idxformulaire);
            if($this->is_in_context_of_foreign_key('dossier_instruction', $this->retourformulaire))
                $form->setVal('dossier_instruction', $idxformulaire);
            if($this->is_in_context_of_foreign_key('programmation', $this->retourformulaire))
                $form->setVal('programmation', $idxformulaire);
            if($this->is_in_context_of_foreign_key('visite_etat', $this->retourformulaire))
                $form->setVal('visite_etat', $idxformulaire);
            if($this->is_in_context_of_foreign_key('visite_motif_annulation', $this->retourformulaire))
                $form->setVal('visite_motif_annulation', $idxformulaire);
        }// fin validation
        if ($validation == 0 and $maj == 0) {
            if($this->is_in_context_of_foreign_key('courrier', $this->retourformulaire))
                $form->setVal('courrier_annulation', $idxformulaire);
            if($this->is_in_context_of_foreign_key('courrier', $this->retourformulaire))
                $form->setVal('courrier_convocation_exploitants', $idxformulaire);
        }// fin validation
        $this->set_form_default_values($form, $maj, $validation);
    }// fin setValsousformulaire

    //==================================
    // cle secondaire
    //==================================
    
    /**
     * Methode clesecondaire
     */
    function cleSecondaire($id, &$dnu1 = null, $val = array(), $dnu2 = null) {
        // On appelle la methode de la classe parent
        parent::cleSecondaire($id);
        // Verification de la cle secondaire : courrier
        $this->rechercheTable($this->f->db, "courrier", "visite", $id);
    }


}
