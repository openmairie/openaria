<?php
//$Id$ 
//gen openMairie le 20/05/2020 18:03

require_once "../obj/om_dbform.class.php";

class piece_gen extends om_dbform {

    protected $_absolute_class_name = "piece";

    var $table = "piece";
    var $clePrimaire = "piece";
    var $typeCle = "N";
    var $required_field = array(
        "piece",
        "service"
    );
    
    var $foreign_keys_extended = array(
        "dossier_coordination" => array("dossier_coordination", "dossier_coordination_nouveau", "dossier_coordination_a_qualifier", "dossier_coordination_a_cloturer", ),
        "dossier_instruction" => array("dossier_instruction", "dossier_instruction_mes_plans", "dossier_instruction_mes_visites", "dossier_instruction_tous_plans", "dossier_instruction_tous_visites", "dossier_instruction_a_qualifier", "dossier_instruction_a_affecter", ),
        "etablissement" => array("etablissement", "etablissement_referentiel_erp", "etablissement_tous", ),
        "piece_statut" => array("piece_statut", ),
        "piece_type" => array("piece_type", ),
        "service" => array("service", ),
    );
    
    /**
     *
     * @return string
     */
    function get_default_libelle() {
        return $this->getVal($this->clePrimaire)."&nbsp;".$this->getVal("etablissement");
    }

    /**
     *
     * @return array
     */
    function get_var_sql_forminc__champs() {
        return array(
            "piece",
            "etablissement",
            "dossier_coordination",
            "nom",
            "piece_type",
            "uid",
            "om_date_creation",
            "dossier_instruction",
            "date_reception",
            "date_emission",
            "piece_statut",
            "date_butoir",
            "suivi",
            "commentaire_suivi",
            "lu",
            "choix_lien",
            "service",
        );
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_dossier_coordination() {
        return "SELECT dossier_coordination.dossier_coordination, dossier_coordination.libelle FROM ".DB_PREFIXE."dossier_coordination ORDER BY dossier_coordination.libelle ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_dossier_coordination_by_id() {
        return "SELECT dossier_coordination.dossier_coordination, dossier_coordination.libelle FROM ".DB_PREFIXE."dossier_coordination WHERE dossier_coordination = <idx>";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_dossier_instruction() {
        return "SELECT dossier_instruction.dossier_instruction, dossier_instruction.libelle FROM ".DB_PREFIXE."dossier_instruction ORDER BY dossier_instruction.libelle ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_dossier_instruction_by_id() {
        return "SELECT dossier_instruction.dossier_instruction, dossier_instruction.libelle FROM ".DB_PREFIXE."dossier_instruction WHERE dossier_instruction = <idx>";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_etablissement() {
        return "SELECT etablissement.etablissement, etablissement.libelle FROM ".DB_PREFIXE."etablissement WHERE ((etablissement.om_validite_debut IS NULL AND (etablissement.om_validite_fin IS NULL OR etablissement.om_validite_fin > CURRENT_DATE)) OR (etablissement.om_validite_debut <= CURRENT_DATE AND (etablissement.om_validite_fin IS NULL OR etablissement.om_validite_fin > CURRENT_DATE))) ORDER BY etablissement.libelle ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_etablissement_by_id() {
        return "SELECT etablissement.etablissement, etablissement.libelle FROM ".DB_PREFIXE."etablissement WHERE etablissement = <idx>";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_piece_statut() {
        return "SELECT piece_statut.piece_statut, piece_statut.libelle FROM ".DB_PREFIXE."piece_statut WHERE ((piece_statut.om_validite_debut IS NULL AND (piece_statut.om_validite_fin IS NULL OR piece_statut.om_validite_fin > CURRENT_DATE)) OR (piece_statut.om_validite_debut <= CURRENT_DATE AND (piece_statut.om_validite_fin IS NULL OR piece_statut.om_validite_fin > CURRENT_DATE))) ORDER BY piece_statut.libelle ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_piece_statut_by_id() {
        return "SELECT piece_statut.piece_statut, piece_statut.libelle FROM ".DB_PREFIXE."piece_statut WHERE piece_statut = <idx>";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_piece_type() {
        return "SELECT piece_type.piece_type, piece_type.libelle FROM ".DB_PREFIXE."piece_type WHERE ((piece_type.om_validite_debut IS NULL AND (piece_type.om_validite_fin IS NULL OR piece_type.om_validite_fin > CURRENT_DATE)) OR (piece_type.om_validite_debut <= CURRENT_DATE AND (piece_type.om_validite_fin IS NULL OR piece_type.om_validite_fin > CURRENT_DATE))) ORDER BY piece_type.libelle ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_piece_type_by_id() {
        return "SELECT piece_type.piece_type, piece_type.libelle FROM ".DB_PREFIXE."piece_type WHERE piece_type = <idx>";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_service() {
        return "SELECT service.service, service.libelle FROM ".DB_PREFIXE."service WHERE ((service.om_validite_debut IS NULL AND (service.om_validite_fin IS NULL OR service.om_validite_fin > CURRENT_DATE)) OR (service.om_validite_debut <= CURRENT_DATE AND (service.om_validite_fin IS NULL OR service.om_validite_fin > CURRENT_DATE))) ORDER BY service.libelle ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_service_by_id() {
        return "SELECT service.service, service.libelle FROM ".DB_PREFIXE."service WHERE service = <idx>";
    }




    function setvalF($val = array()) {
        //affectation valeur formulaire
        if (!is_numeric($val['piece'])) {
            $this->valF['piece'] = ""; // -> requis
        } else {
            $this->valF['piece'] = $val['piece'];
        }
        if (!is_numeric($val['etablissement'])) {
            $this->valF['etablissement'] = NULL;
        } else {
            $this->valF['etablissement'] = $val['etablissement'];
        }
        if (!is_numeric($val['dossier_coordination'])) {
            $this->valF['dossier_coordination'] = NULL;
        } else {
            $this->valF['dossier_coordination'] = $val['dossier_coordination'];
        }
        if ($val['nom'] == "") {
            $this->valF['nom'] = NULL;
        } else {
            $this->valF['nom'] = $val['nom'];
        }
        if (!is_numeric($val['piece_type'])) {
            $this->valF['piece_type'] = NULL;
        } else {
            $this->valF['piece_type'] = $val['piece_type'];
        }
        if ($val['uid'] == "") {
            $this->valF['uid'] = NULL;
        } else {
            $this->valF['uid'] = $val['uid'];
        }
        if ($val['om_date_creation'] != "") {
            $this->valF['om_date_creation'] = $this->dateDB($val['om_date_creation']);
        }
        if (!is_numeric($val['dossier_instruction'])) {
            $this->valF['dossier_instruction'] = NULL;
        } else {
            $this->valF['dossier_instruction'] = $val['dossier_instruction'];
        }
        if ($val['date_reception'] != "") {
            $this->valF['date_reception'] = $this->dateDB($val['date_reception']);
        } else {
            $this->valF['date_reception'] = NULL;
        }
        if ($val['date_emission'] != "") {
            $this->valF['date_emission'] = $this->dateDB($val['date_emission']);
        } else {
            $this->valF['date_emission'] = NULL;
        }
        if (!is_numeric($val['piece_statut'])) {
            $this->valF['piece_statut'] = NULL;
        } else {
            $this->valF['piece_statut'] = $val['piece_statut'];
        }
        if ($val['date_butoir'] != "") {
            $this->valF['date_butoir'] = $this->dateDB($val['date_butoir']);
        } else {
            $this->valF['date_butoir'] = NULL;
        }
        if ($val['suivi'] == 1 || $val['suivi'] == "t" || $val['suivi'] == "Oui") {
            $this->valF['suivi'] = true;
        } else {
            $this->valF['suivi'] = false;
        }
            $this->valF['commentaire_suivi'] = $val['commentaire_suivi'];
        if ($val['lu'] == 1 || $val['lu'] == "t" || $val['lu'] == "Oui") {
            $this->valF['lu'] = true;
        } else {
            $this->valF['lu'] = false;
        }
        if ($val['choix_lien'] == "") {
            $this->valF['choix_lien'] = NULL;
        } else {
            $this->valF['choix_lien'] = $val['choix_lien'];
        }
        if (!is_numeric($val['service'])) {
            $this->valF['service'] = ""; // -> requis
        } else {
            $this->valF['service'] = $val['service'];
        }
    }

    //=================================================
    //cle primaire automatique [automatic primary key]
    //==================================================

    function setId(&$dnu1 = null) {
    //numero automatique
        $this->valF[$this->clePrimaire] = $this->f->db->nextId(DB_PREFIXE.$this->table);
    }

    function setValFAjout($val = array()) {
    //numero automatique -> pas de controle ajout cle primaire
    }

    function verifierAjout($val = array(), &$dnu1 = null) {
    //numero automatique -> pas de verfication de cle primaire
    }

    //==========================
    // Formulaire  [form]
    //==========================
    /**
     *
     */
    function setType(&$form, $maj) {
        // Récupération du mode de l'action
        $crud = $this->get_action_crud($maj);

        // MODE AJOUTER
        if ($maj == 0 || $crud == 'create') {
            $form->setType("piece", "hidden");
            if ($this->is_in_context_of_foreign_key("etablissement", $this->retourformulaire)) {
                $form->setType("etablissement", "selecthiddenstatic");
            } else {
                $form->setType("etablissement", "select");
            }
            if ($this->is_in_context_of_foreign_key("dossier_coordination", $this->retourformulaire)) {
                $form->setType("dossier_coordination", "selecthiddenstatic");
            } else {
                $form->setType("dossier_coordination", "select");
            }
            $form->setType("nom", "text");
            if ($this->is_in_context_of_foreign_key("piece_type", $this->retourformulaire)) {
                $form->setType("piece_type", "selecthiddenstatic");
            } else {
                $form->setType("piece_type", "select");
            }
            $form->setType("uid", "text");
            $form->setType("om_date_creation", "date");
            if ($this->is_in_context_of_foreign_key("dossier_instruction", $this->retourformulaire)) {
                $form->setType("dossier_instruction", "selecthiddenstatic");
            } else {
                $form->setType("dossier_instruction", "select");
            }
            $form->setType("date_reception", "date");
            $form->setType("date_emission", "date");
            if ($this->is_in_context_of_foreign_key("piece_statut", $this->retourformulaire)) {
                $form->setType("piece_statut", "selecthiddenstatic");
            } else {
                $form->setType("piece_statut", "select");
            }
            $form->setType("date_butoir", "date");
            $form->setType("suivi", "checkbox");
            $form->setType("commentaire_suivi", "textarea");
            $form->setType("lu", "checkbox");
            $form->setType("choix_lien", "text");
            if ($this->is_in_context_of_foreign_key("service", $this->retourformulaire)) {
                $form->setType("service", "selecthiddenstatic");
            } else {
                $form->setType("service", "select");
            }
        }

        // MDOE MODIFIER
        if ($maj == 1 || $crud == 'update') {
            $form->setType("piece", "hiddenstatic");
            if ($this->is_in_context_of_foreign_key("etablissement", $this->retourformulaire)) {
                $form->setType("etablissement", "selecthiddenstatic");
            } else {
                $form->setType("etablissement", "select");
            }
            if ($this->is_in_context_of_foreign_key("dossier_coordination", $this->retourformulaire)) {
                $form->setType("dossier_coordination", "selecthiddenstatic");
            } else {
                $form->setType("dossier_coordination", "select");
            }
            $form->setType("nom", "text");
            if ($this->is_in_context_of_foreign_key("piece_type", $this->retourformulaire)) {
                $form->setType("piece_type", "selecthiddenstatic");
            } else {
                $form->setType("piece_type", "select");
            }
            $form->setType("uid", "text");
            $form->setType("om_date_creation", "date");
            if ($this->is_in_context_of_foreign_key("dossier_instruction", $this->retourformulaire)) {
                $form->setType("dossier_instruction", "selecthiddenstatic");
            } else {
                $form->setType("dossier_instruction", "select");
            }
            $form->setType("date_reception", "date");
            $form->setType("date_emission", "date");
            if ($this->is_in_context_of_foreign_key("piece_statut", $this->retourformulaire)) {
                $form->setType("piece_statut", "selecthiddenstatic");
            } else {
                $form->setType("piece_statut", "select");
            }
            $form->setType("date_butoir", "date");
            $form->setType("suivi", "checkbox");
            $form->setType("commentaire_suivi", "textarea");
            $form->setType("lu", "checkbox");
            $form->setType("choix_lien", "text");
            if ($this->is_in_context_of_foreign_key("service", $this->retourformulaire)) {
                $form->setType("service", "selecthiddenstatic");
            } else {
                $form->setType("service", "select");
            }
        }

        // MODE SUPPRIMER
        if ($maj == 2 || $crud == 'delete') {
            $form->setType("piece", "hiddenstatic");
            $form->setType("etablissement", "selectstatic");
            $form->setType("dossier_coordination", "selectstatic");
            $form->setType("nom", "hiddenstatic");
            $form->setType("piece_type", "selectstatic");
            $form->setType("uid", "hiddenstatic");
            $form->setType("om_date_creation", "hiddenstatic");
            $form->setType("dossier_instruction", "selectstatic");
            $form->setType("date_reception", "hiddenstatic");
            $form->setType("date_emission", "hiddenstatic");
            $form->setType("piece_statut", "selectstatic");
            $form->setType("date_butoir", "hiddenstatic");
            $form->setType("suivi", "hiddenstatic");
            $form->setType("commentaire_suivi", "hiddenstatic");
            $form->setType("lu", "hiddenstatic");
            $form->setType("choix_lien", "hiddenstatic");
            $form->setType("service", "selectstatic");
        }

        // MODE CONSULTER
        if ($maj == 3 || $crud == 'read') {
            $form->setType("piece", "static");
            $form->setType("etablissement", "selectstatic");
            $form->setType("dossier_coordination", "selectstatic");
            $form->setType("nom", "static");
            $form->setType("piece_type", "selectstatic");
            $form->setType("uid", "static");
            $form->setType("om_date_creation", "datestatic");
            $form->setType("dossier_instruction", "selectstatic");
            $form->setType("date_reception", "datestatic");
            $form->setType("date_emission", "datestatic");
            $form->setType("piece_statut", "selectstatic");
            $form->setType("date_butoir", "datestatic");
            $form->setType("suivi", "checkboxstatic");
            $form->setType("commentaire_suivi", "textareastatic");
            $form->setType("lu", "checkboxstatic");
            $form->setType("choix_lien", "static");
            $form->setType("service", "selectstatic");
        }

    }


    function setOnchange(&$form, $maj) {
    //javascript controle client
        $form->setOnchange('piece','VerifNum(this)');
        $form->setOnchange('etablissement','VerifNum(this)');
        $form->setOnchange('dossier_coordination','VerifNum(this)');
        $form->setOnchange('piece_type','VerifNum(this)');
        $form->setOnchange('om_date_creation','fdate(this)');
        $form->setOnchange('dossier_instruction','VerifNum(this)');
        $form->setOnchange('date_reception','fdate(this)');
        $form->setOnchange('date_emission','fdate(this)');
        $form->setOnchange('piece_statut','VerifNum(this)');
        $form->setOnchange('date_butoir','fdate(this)');
        $form->setOnchange('service','VerifNum(this)');
    }
    /**
     * Methode setTaille
     */
    function setTaille(&$form, $maj) {
        $form->setTaille("piece", 11);
        $form->setTaille("etablissement", 11);
        $form->setTaille("dossier_coordination", 11);
        $form->setTaille("nom", 30);
        $form->setTaille("piece_type", 11);
        $form->setTaille("uid", 30);
        $form->setTaille("om_date_creation", 12);
        $form->setTaille("dossier_instruction", 11);
        $form->setTaille("date_reception", 12);
        $form->setTaille("date_emission", 12);
        $form->setTaille("piece_statut", 11);
        $form->setTaille("date_butoir", 12);
        $form->setTaille("suivi", 1);
        $form->setTaille("commentaire_suivi", 80);
        $form->setTaille("lu", 1);
        $form->setTaille("choix_lien", 30);
        $form->setTaille("service", 11);
    }

    /**
     * Methode setMax
     */
    function setMax(&$form, $maj) {
        $form->setMax("piece", 11);
        $form->setMax("etablissement", 11);
        $form->setMax("dossier_coordination", 11);
        $form->setMax("nom", 250);
        $form->setMax("piece_type", 11);
        $form->setMax("uid", 64);
        $form->setMax("om_date_creation", 12);
        $form->setMax("dossier_instruction", 11);
        $form->setMax("date_reception", 12);
        $form->setMax("date_emission", 12);
        $form->setMax("piece_statut", 11);
        $form->setMax("date_butoir", 12);
        $form->setMax("suivi", 1);
        $form->setMax("commentaire_suivi", 6);
        $form->setMax("lu", 1);
        $form->setMax("choix_lien", 40);
        $form->setMax("service", 11);
    }


    function setLib(&$form, $maj) {
    //libelle des champs
        $form->setLib('piece', __('piece'));
        $form->setLib('etablissement', __('etablissement'));
        $form->setLib('dossier_coordination', __('dossier_coordination'));
        $form->setLib('nom', __('nom'));
        $form->setLib('piece_type', __('piece_type'));
        $form->setLib('uid', __('uid'));
        $form->setLib('om_date_creation', __('om_date_creation'));
        $form->setLib('dossier_instruction', __('dossier_instruction'));
        $form->setLib('date_reception', __('date_reception'));
        $form->setLib('date_emission', __('date_emission'));
        $form->setLib('piece_statut', __('piece_statut'));
        $form->setLib('date_butoir', __('date_butoir'));
        $form->setLib('suivi', __('suivi'));
        $form->setLib('commentaire_suivi', __('commentaire_suivi'));
        $form->setLib('lu', __('lu'));
        $form->setLib('choix_lien', __('choix_lien'));
        $form->setLib('service', __('service'));
    }
    /**
     *
     */
    function setSelect(&$form, $maj, &$dnu1 = null, $dnu2 = null) {

        // dossier_coordination
        $this->init_select(
            $form, 
            $this->f->db,
            $maj,
            null,
            "dossier_coordination",
            $this->get_var_sql_forminc__sql("dossier_coordination"),
            $this->get_var_sql_forminc__sql("dossier_coordination_by_id"),
            false
        );
        // dossier_instruction
        $this->init_select(
            $form, 
            $this->f->db,
            $maj,
            null,
            "dossier_instruction",
            $this->get_var_sql_forminc__sql("dossier_instruction"),
            $this->get_var_sql_forminc__sql("dossier_instruction_by_id"),
            false
        );
        // etablissement
        $this->init_select(
            $form, 
            $this->f->db,
            $maj,
            null,
            "etablissement",
            $this->get_var_sql_forminc__sql("etablissement"),
            $this->get_var_sql_forminc__sql("etablissement_by_id"),
            true
        );
        // piece_statut
        $this->init_select(
            $form, 
            $this->f->db,
            $maj,
            null,
            "piece_statut",
            $this->get_var_sql_forminc__sql("piece_statut"),
            $this->get_var_sql_forminc__sql("piece_statut_by_id"),
            true
        );
        // piece_type
        $this->init_select(
            $form, 
            $this->f->db,
            $maj,
            null,
            "piece_type",
            $this->get_var_sql_forminc__sql("piece_type"),
            $this->get_var_sql_forminc__sql("piece_type_by_id"),
            true
        );
        // service
        $this->init_select(
            $form, 
            $this->f->db,
            $maj,
            null,
            "service",
            $this->get_var_sql_forminc__sql("service"),
            $this->get_var_sql_forminc__sql("service_by_id"),
            true
        );
    }


    //==================================
    // sous Formulaire
    //==================================
    

    function setValsousformulaire(&$form, $maj, $validation, $idxformulaire, $retourformulaire, $typeformulaire, &$dnu1 = null, $dnu2 = null) {
        $this->retourformulaire = $retourformulaire;
        if($validation == 0) {
            if($this->is_in_context_of_foreign_key('dossier_coordination', $this->retourformulaire))
                $form->setVal('dossier_coordination', $idxformulaire);
            if($this->is_in_context_of_foreign_key('dossier_instruction', $this->retourformulaire))
                $form->setVal('dossier_instruction', $idxformulaire);
            if($this->is_in_context_of_foreign_key('etablissement', $this->retourformulaire))
                $form->setVal('etablissement', $idxformulaire);
            if($this->is_in_context_of_foreign_key('piece_statut', $this->retourformulaire))
                $form->setVal('piece_statut', $idxformulaire);
            if($this->is_in_context_of_foreign_key('piece_type', $this->retourformulaire))
                $form->setVal('piece_type', $idxformulaire);
            if($this->is_in_context_of_foreign_key('service', $this->retourformulaire))
                $form->setVal('service', $idxformulaire);
        }// fin validation
        $this->set_form_default_values($form, $maj, $validation);
    }// fin setValsousformulaire

    //==================================
    // cle secondaire
    //==================================
    

}
