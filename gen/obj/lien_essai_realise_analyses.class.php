<?php
//$Id$ 
//gen openMairie le 03/05/2018 09:12

require_once "../obj/om_dbform.class.php";

class lien_essai_realise_analyses_gen extends om_dbform {

    protected $_absolute_class_name = "lien_essai_realise_analyses";

    var $table = "lien_essai_realise_analyses";
    var $clePrimaire = "lien_essai_realise_analyses";
    var $typeCle = "N";
    var $required_field = array(
        "analyses",
        "essai_realise",
        "lien_essai_realise_analyses"
    );
    
    var $foreign_keys_extended = array(
        "analyses" => array("analyses", ),
        "essai_realise" => array("essai_realise", ),
    );
    
    /**
     *
     * @return string
     */
    function get_default_libelle() {
        return $this->getVal($this->clePrimaire)."&nbsp;".$this->getVal("essai_realise");
    }

    /**
     *
     * @return array
     */
    function get_var_sql_forminc__champs() {
        return array(
            "lien_essai_realise_analyses",
            "essai_realise",
            "analyses",
            "concluant",
            "complement",
        );
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_analyses() {
        return "SELECT analyses.analyses, analyses.service FROM ".DB_PREFIXE."analyses ORDER BY analyses.service ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_analyses_by_id() {
        return "SELECT analyses.analyses, analyses.service FROM ".DB_PREFIXE."analyses WHERE analyses = <idx>";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_essai_realise() {
        return "SELECT essai_realise.essai_realise, essai_realise.libelle FROM ".DB_PREFIXE."essai_realise WHERE ((essai_realise.om_validite_debut IS NULL AND (essai_realise.om_validite_fin IS NULL OR essai_realise.om_validite_fin > CURRENT_DATE)) OR (essai_realise.om_validite_debut <= CURRENT_DATE AND (essai_realise.om_validite_fin IS NULL OR essai_realise.om_validite_fin > CURRENT_DATE))) ORDER BY essai_realise.libelle ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_essai_realise_by_id() {
        return "SELECT essai_realise.essai_realise, essai_realise.libelle FROM ".DB_PREFIXE."essai_realise WHERE essai_realise = <idx>";
    }




    function setvalF($val = array()) {
        //affectation valeur formulaire
        if (!is_numeric($val['lien_essai_realise_analyses'])) {
            $this->valF['lien_essai_realise_analyses'] = ""; // -> requis
        } else {
            $this->valF['lien_essai_realise_analyses'] = $val['lien_essai_realise_analyses'];
        }
        if (!is_numeric($val['essai_realise'])) {
            $this->valF['essai_realise'] = ""; // -> requis
        } else {
            $this->valF['essai_realise'] = $val['essai_realise'];
        }
        if (!is_numeric($val['analyses'])) {
            $this->valF['analyses'] = ""; // -> requis
        } else {
            $this->valF['analyses'] = $val['analyses'];
        }
        if ($val['concluant'] == 1 || $val['concluant'] == "t" || $val['concluant'] == "Oui") {
            $this->valF['concluant'] = true;
        } else {
            $this->valF['concluant'] = false;
        }
            $this->valF['complement'] = $val['complement'];
    }

    //=================================================
    //cle primaire automatique [automatic primary key]
    //==================================================

    function setId(&$dnu1 = null) {
    //numero automatique
        $this->valF[$this->clePrimaire] = $this->f->db->nextId(DB_PREFIXE.$this->table);
    }

    function setValFAjout($val = array()) {
    //numero automatique -> pas de controle ajout cle primaire
    }

    function verifierAjout($val = array(), &$dnu1 = null) {
    //numero automatique -> pas de verfication de cle primaire
    }

    //==========================
    // Formulaire  [form]
    //==========================
    /**
     *
     */
    function setType(&$form, $maj) {
        // Récupération du mode de l'action
        $crud = $this->get_action_crud($maj);

        // MODE AJOUTER
        if ($maj == 0 || $crud == 'create') {
            $form->setType("lien_essai_realise_analyses", "hidden");
            if ($this->is_in_context_of_foreign_key("essai_realise", $this->retourformulaire)) {
                $form->setType("essai_realise", "selecthiddenstatic");
            } else {
                $form->setType("essai_realise", "select");
            }
            if ($this->is_in_context_of_foreign_key("analyses", $this->retourformulaire)) {
                $form->setType("analyses", "selecthiddenstatic");
            } else {
                $form->setType("analyses", "select");
            }
            $form->setType("concluant", "checkbox");
            $form->setType("complement", "textarea");
        }

        // MDOE MODIFIER
        if ($maj == 1 || $crud == 'update') {
            $form->setType("lien_essai_realise_analyses", "hiddenstatic");
            if ($this->is_in_context_of_foreign_key("essai_realise", $this->retourformulaire)) {
                $form->setType("essai_realise", "selecthiddenstatic");
            } else {
                $form->setType("essai_realise", "select");
            }
            if ($this->is_in_context_of_foreign_key("analyses", $this->retourformulaire)) {
                $form->setType("analyses", "selecthiddenstatic");
            } else {
                $form->setType("analyses", "select");
            }
            $form->setType("concluant", "checkbox");
            $form->setType("complement", "textarea");
        }

        // MODE SUPPRIMER
        if ($maj == 2 || $crud == 'delete') {
            $form->setType("lien_essai_realise_analyses", "hiddenstatic");
            $form->setType("essai_realise", "selectstatic");
            $form->setType("analyses", "selectstatic");
            $form->setType("concluant", "hiddenstatic");
            $form->setType("complement", "hiddenstatic");
        }

        // MODE CONSULTER
        if ($maj == 3 || $crud == 'read') {
            $form->setType("lien_essai_realise_analyses", "static");
            $form->setType("essai_realise", "selectstatic");
            $form->setType("analyses", "selectstatic");
            $form->setType("concluant", "checkboxstatic");
            $form->setType("complement", "textareastatic");
        }

    }


    function setOnchange(&$form, $maj) {
    //javascript controle client
        $form->setOnchange('lien_essai_realise_analyses','VerifNum(this)');
        $form->setOnchange('essai_realise','VerifNum(this)');
        $form->setOnchange('analyses','VerifNum(this)');
    }
    /**
     * Methode setTaille
     */
    function setTaille(&$form, $maj) {
        $form->setTaille("lien_essai_realise_analyses", 11);
        $form->setTaille("essai_realise", 11);
        $form->setTaille("analyses", 11);
        $form->setTaille("concluant", 1);
        $form->setTaille("complement", 80);
    }

    /**
     * Methode setMax
     */
    function setMax(&$form, $maj) {
        $form->setMax("lien_essai_realise_analyses", 11);
        $form->setMax("essai_realise", 11);
        $form->setMax("analyses", 11);
        $form->setMax("concluant", 1);
        $form->setMax("complement", 6);
    }


    function setLib(&$form, $maj) {
    //libelle des champs
        $form->setLib('lien_essai_realise_analyses', __('lien_essai_realise_analyses'));
        $form->setLib('essai_realise', __('essai_realise'));
        $form->setLib('analyses', __('analyses'));
        $form->setLib('concluant', __('concluant'));
        $form->setLib('complement', __('complement'));
    }
    /**
     *
     */
    function setSelect(&$form, $maj, &$dnu1 = null, $dnu2 = null) {

        // analyses
        $this->init_select(
            $form, 
            $this->f->db,
            $maj,
            null,
            "analyses",
            $this->get_var_sql_forminc__sql("analyses"),
            $this->get_var_sql_forminc__sql("analyses_by_id"),
            false
        );
        // essai_realise
        $this->init_select(
            $form, 
            $this->f->db,
            $maj,
            null,
            "essai_realise",
            $this->get_var_sql_forminc__sql("essai_realise"),
            $this->get_var_sql_forminc__sql("essai_realise_by_id"),
            true
        );
    }


    //==================================
    // sous Formulaire
    //==================================
    

    function setValsousformulaire(&$form, $maj, $validation, $idxformulaire, $retourformulaire, $typeformulaire, &$dnu1 = null, $dnu2 = null) {
        $this->retourformulaire = $retourformulaire;
        if($validation == 0) {
            if($this->is_in_context_of_foreign_key('analyses', $this->retourformulaire))
                $form->setVal('analyses', $idxformulaire);
            if($this->is_in_context_of_foreign_key('essai_realise', $this->retourformulaire))
                $form->setVal('essai_realise', $idxformulaire);
        }// fin validation
        $this->set_form_default_values($form, $maj, $validation);
    }// fin setValsousformulaire

    //==================================
    // cle secondaire
    //==================================
    

}
