<?php
//$Id$ 
//gen openMairie le 03/05/2018 09:12

require_once "../obj/om_dbform.class.php";

class acteur_gen extends om_dbform {

    protected $_absolute_class_name = "acteur";

    var $table = "acteur";
    var $clePrimaire = "acteur";
    var $typeCle = "N";
    var $required_field = array(
        "acteur"
    );
    
    var $foreign_keys_extended = array(
        "om_utilisateur" => array("om_utilisateur", ),
        "service" => array("service", ),
    );
    
    /**
     *
     * @return string
     */
    function get_default_libelle() {
        return $this->getVal($this->clePrimaire)."&nbsp;".$this->getVal("nom_prenom");
    }

    /**
     *
     * @return array
     */
    function get_var_sql_forminc__champs() {
        return array(
            "acteur",
            "nom_prenom",
            "om_utilisateur",
            "service",
            "role",
            "acronyme",
            "couleur",
            "om_validite_debut",
            "om_validite_fin",
            "reference",
        );
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_om_utilisateur() {
        return "SELECT om_utilisateur.om_utilisateur, om_utilisateur.nom FROM ".DB_PREFIXE."om_utilisateur ORDER BY om_utilisateur.nom ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_om_utilisateur_by_id() {
        return "SELECT om_utilisateur.om_utilisateur, om_utilisateur.nom FROM ".DB_PREFIXE."om_utilisateur WHERE om_utilisateur = <idx>";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_service() {
        return "SELECT service.service, service.libelle FROM ".DB_PREFIXE."service WHERE ((service.om_validite_debut IS NULL AND (service.om_validite_fin IS NULL OR service.om_validite_fin > CURRENT_DATE)) OR (service.om_validite_debut <= CURRENT_DATE AND (service.om_validite_fin IS NULL OR service.om_validite_fin > CURRENT_DATE))) ORDER BY service.libelle ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_service_by_id() {
        return "SELECT service.service, service.libelle FROM ".DB_PREFIXE."service WHERE service = <idx>";
    }




    function setvalF($val = array()) {
        //affectation valeur formulaire
        if (!is_numeric($val['acteur'])) {
            $this->valF['acteur'] = ""; // -> requis
        } else {
            $this->valF['acteur'] = $val['acteur'];
        }
        if ($val['nom_prenom'] == "") {
            $this->valF['nom_prenom'] = NULL;
        } else {
            $this->valF['nom_prenom'] = $val['nom_prenom'];
        }
        if (!is_numeric($val['om_utilisateur'])) {
            $this->valF['om_utilisateur'] = NULL;
        } else {
            $this->valF['om_utilisateur'] = $val['om_utilisateur'];
        }
        if (!is_numeric($val['service'])) {
            $this->valF['service'] = NULL;
        } else {
            $this->valF['service'] = $val['service'];
        }
        if ($val['role'] == "") {
            $this->valF['role'] = NULL;
        } else {
            $this->valF['role'] = $val['role'];
        }
        if ($val['acronyme'] == "") {
            $this->valF['acronyme'] = NULL;
        } else {
            $this->valF['acronyme'] = $val['acronyme'];
        }
        if ($val['couleur'] == "") {
            $this->valF['couleur'] = NULL;
        } else {
            $this->valF['couleur'] = $val['couleur'];
        }
        if ($val['om_validite_debut'] != "") {
            $this->valF['om_validite_debut'] = $this->dateDB($val['om_validite_debut']);
        } else {
            $this->valF['om_validite_debut'] = NULL;
        }
        if ($val['om_validite_fin'] != "") {
            $this->valF['om_validite_fin'] = $this->dateDB($val['om_validite_fin']);
        } else {
            $this->valF['om_validite_fin'] = NULL;
        }
        if ($val['reference'] == "") {
            $this->valF['reference'] = NULL;
        } else {
            $this->valF['reference'] = $val['reference'];
        }
    }

    //=================================================
    //cle primaire automatique [automatic primary key]
    //==================================================

    function setId(&$dnu1 = null) {
    //numero automatique
        $this->valF[$this->clePrimaire] = $this->f->db->nextId(DB_PREFIXE.$this->table);
    }

    function setValFAjout($val = array()) {
    //numero automatique -> pas de controle ajout cle primaire
    }

    function verifierAjout($val = array(), &$dnu1 = null) {
    //numero automatique -> pas de verfication de cle primaire
    }
    /**
     * Methode verifier
     */
    function verifier($val = array(), &$dnu1 = null, $dnu2 = null) {
        // On appelle la methode de la classe parent
        parent::verifier($val, $this->f->db, null);

        // gestion des dates de validites
        $date_debut = $this->valF['om_validite_debut'];
        $date_fin = $this->valF['om_validite_fin'];

        if ($date_debut != '' and $date_fin != '') {
        
            $date_debut = explode('-', $this->valF['om_validite_debut']);
            $date_fin = explode('-', $this->valF['om_validite_fin']);

            $time_debut = mktime(0, 0, 0, $date_debut[1], $date_debut[2],
                                 $date_debut[0]);
            $time_fin = mktime(0, 0, 0, $date_fin[1], $date_fin[2],
                                 $date_fin[0]);

            if ($time_debut > $time_fin or $time_debut == $time_fin) {
                $this->correct = false;
                $this->addToMessage(__('La date de fin de validite doit etre future a la de debut de validite.'));
            }
        }
    }


    //==========================
    // Formulaire  [form]
    //==========================
    /**
     *
     */
    function setType(&$form, $maj) {
        // Récupération du mode de l'action
        $crud = $this->get_action_crud($maj);

        // MODE AJOUTER
        if ($maj == 0 || $crud == 'create') {
            $form->setType("acteur", "hidden");
            $form->setType("nom_prenom", "text");
            if ($this->is_in_context_of_foreign_key("om_utilisateur", $this->retourformulaire)) {
                $form->setType("om_utilisateur", "selecthiddenstatic");
            } else {
                $form->setType("om_utilisateur", "select");
            }
            if ($this->is_in_context_of_foreign_key("service", $this->retourformulaire)) {
                $form->setType("service", "selecthiddenstatic");
            } else {
                $form->setType("service", "select");
            }
            $form->setType("role", "text");
            $form->setType("acronyme", "text");
            $form->setType("couleur", "text");
            if ($this->f->isAccredited(array($this->table."_modifier_validite", $this->table, ))) {
                $form->setType("om_validite_debut", "date");
            } else {
                $form->setType("om_validite_debut", "hiddenstaticdate");
            }
            if ($this->f->isAccredited(array($this->table."_modifier_validite", $this->table, ))) {
                $form->setType("om_validite_fin", "date");
            } else {
                $form->setType("om_validite_fin", "hiddenstaticdate");
            }
            $form->setType("reference", "text");
        }

        // MDOE MODIFIER
        if ($maj == 1 || $crud == 'update') {
            $form->setType("acteur", "hiddenstatic");
            $form->setType("nom_prenom", "text");
            if ($this->is_in_context_of_foreign_key("om_utilisateur", $this->retourformulaire)) {
                $form->setType("om_utilisateur", "selecthiddenstatic");
            } else {
                $form->setType("om_utilisateur", "select");
            }
            if ($this->is_in_context_of_foreign_key("service", $this->retourformulaire)) {
                $form->setType("service", "selecthiddenstatic");
            } else {
                $form->setType("service", "select");
            }
            $form->setType("role", "text");
            $form->setType("acronyme", "text");
            $form->setType("couleur", "text");
            if ($this->f->isAccredited(array($this->table."_modifier_validite", $this->table, ))) {
                $form->setType("om_validite_debut", "date");
            } else {
                $form->setType("om_validite_debut", "hiddenstaticdate");
            }
            if ($this->f->isAccredited(array($this->table."_modifier_validite", $this->table, ))) {
                $form->setType("om_validite_fin", "date");
            } else {
                $form->setType("om_validite_fin", "hiddenstaticdate");
            }
            $form->setType("reference", "text");
        }

        // MODE SUPPRIMER
        if ($maj == 2 || $crud == 'delete') {
            $form->setType("acteur", "hiddenstatic");
            $form->setType("nom_prenom", "hiddenstatic");
            $form->setType("om_utilisateur", "selectstatic");
            $form->setType("service", "selectstatic");
            $form->setType("role", "hiddenstatic");
            $form->setType("acronyme", "hiddenstatic");
            $form->setType("couleur", "hiddenstatic");
            $form->setType("om_validite_debut", "hiddenstatic");
            $form->setType("om_validite_fin", "hiddenstatic");
            $form->setType("reference", "hiddenstatic");
        }

        // MODE CONSULTER
        if ($maj == 3 || $crud == 'read') {
            $form->setType("acteur", "static");
            $form->setType("nom_prenom", "static");
            $form->setType("om_utilisateur", "selectstatic");
            $form->setType("service", "selectstatic");
            $form->setType("role", "static");
            $form->setType("acronyme", "static");
            $form->setType("couleur", "static");
            $form->setType("om_validite_debut", "datestatic");
            $form->setType("om_validite_fin", "datestatic");
            $form->setType("reference", "static");
        }

    }


    function setOnchange(&$form, $maj) {
    //javascript controle client
        $form->setOnchange('acteur','VerifNum(this)');
        $form->setOnchange('om_utilisateur','VerifNum(this)');
        $form->setOnchange('service','VerifNum(this)');
        $form->setOnchange('om_validite_debut','fdate(this)');
        $form->setOnchange('om_validite_fin','fdate(this)');
    }
    /**
     * Methode setTaille
     */
    function setTaille(&$form, $maj) {
        $form->setTaille("acteur", 11);
        $form->setTaille("nom_prenom", 30);
        $form->setTaille("om_utilisateur", 11);
        $form->setTaille("service", 11);
        $form->setTaille("role", 30);
        $form->setTaille("acronyme", 10);
        $form->setTaille("couleur", 10);
        $form->setTaille("om_validite_debut", 12);
        $form->setTaille("om_validite_fin", 12);
        $form->setTaille("reference", 30);
    }

    /**
     * Methode setMax
     */
    function setMax(&$form, $maj) {
        $form->setMax("acteur", 11);
        $form->setMax("nom_prenom", 80);
        $form->setMax("om_utilisateur", 11);
        $form->setMax("service", 11);
        $form->setMax("role", 100);
        $form->setMax("acronyme", 10);
        $form->setMax("couleur", 6);
        $form->setMax("om_validite_debut", 12);
        $form->setMax("om_validite_fin", 12);
        $form->setMax("reference", 255);
    }


    function setLib(&$form, $maj) {
    //libelle des champs
        $form->setLib('acteur', __('acteur'));
        $form->setLib('nom_prenom', __('nom_prenom'));
        $form->setLib('om_utilisateur', __('om_utilisateur'));
        $form->setLib('service', __('service'));
        $form->setLib('role', __('role'));
        $form->setLib('acronyme', __('acronyme'));
        $form->setLib('couleur', __('couleur'));
        $form->setLib('om_validite_debut', __('om_validite_debut'));
        $form->setLib('om_validite_fin', __('om_validite_fin'));
        $form->setLib('reference', __('reference'));
    }
    /**
     *
     */
    function setSelect(&$form, $maj, &$dnu1 = null, $dnu2 = null) {

        // om_utilisateur
        $this->init_select(
            $form, 
            $this->f->db,
            $maj,
            null,
            "om_utilisateur",
            $this->get_var_sql_forminc__sql("om_utilisateur"),
            $this->get_var_sql_forminc__sql("om_utilisateur_by_id"),
            false
        );
        // service
        $this->init_select(
            $form, 
            $this->f->db,
            $maj,
            null,
            "service",
            $this->get_var_sql_forminc__sql("service"),
            $this->get_var_sql_forminc__sql("service_by_id"),
            true
        );
    }


    //==================================
    // sous Formulaire
    //==================================
    

    function setValsousformulaire(&$form, $maj, $validation, $idxformulaire, $retourformulaire, $typeformulaire, &$dnu1 = null, $dnu2 = null) {
        $this->retourformulaire = $retourformulaire;
        if($validation == 0) {
            if($this->is_in_context_of_foreign_key('om_utilisateur', $this->retourformulaire))
                $form->setVal('om_utilisateur', $idxformulaire);
            if($this->is_in_context_of_foreign_key('service', $this->retourformulaire))
                $form->setVal('service', $idxformulaire);
        }// fin validation
        $this->set_form_default_values($form, $maj, $validation);
    }// fin setValsousformulaire

    //==================================
    // cle secondaire
    //==================================
    
    /**
     * Methode clesecondaire
     */
    function cleSecondaire($id, &$dnu1 = null, $val = array(), $dnu2 = null) {
        // On appelle la methode de la classe parent
        parent::cleSecondaire($id);
        // Verification de la cle secondaire : acteur_conge
        $this->rechercheTable($this->f->db, "acteur_conge", "acteur", $id);
        // Verification de la cle secondaire : acteur_plage_visite
        $this->rechercheTable($this->f->db, "acteur_plage_visite", "acteur", $id);
        // Verification de la cle secondaire : dossier_instruction
        $this->rechercheTable($this->f->db, "dossier_instruction", "technicien", $id);
        // Verification de la cle secondaire : etablissement
        $this->rechercheTable($this->f->db, "etablissement", "acc_derniere_visite_technicien", $id);
        // Verification de la cle secondaire : etablissement
        $this->rechercheTable($this->f->db, "etablissement", "si_derniere_visite_technicien", $id);
        // Verification de la cle secondaire : technicien_arrondissement
        $this->rechercheTable($this->f->db, "technicien_arrondissement", "technicien", $id);
        // Verification de la cle secondaire : visite
        $this->rechercheTable($this->f->db, "visite", "acteur", $id);
    }


}
