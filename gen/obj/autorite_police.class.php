<?php
//$Id$ 
//gen openMairie le 20/05/2020 18:03

require_once "../obj/om_dbform.class.php";

class autorite_police_gen extends om_dbform {

    protected $_absolute_class_name = "autorite_police";

    var $table = "autorite_police";
    var $clePrimaire = "autorite_police";
    var $typeCle = "N";
    var $required_field = array(
        "autorite_police",
        "autorite_police_decision",
        "autorite_police_motif",
        "date_decision",
        "delai",
        "service"
    );
    
    var $foreign_keys_extended = array(
        "autorite_police_decision" => array("autorite_police_decision", ),
        "autorite_police_motif" => array("autorite_police_motif", ),
        "dossier_coordination" => array("dossier_coordination", "dossier_coordination_nouveau", "dossier_coordination_a_qualifier", "dossier_coordination_a_cloturer", ),
        "dossier_instruction_reunion" => array("dossier_instruction_reunion", ),
        "etablissement" => array("etablissement", "etablissement_referentiel_erp", "etablissement_tous", ),
        "service" => array("service", ),
    );
    
    /**
     *
     * @return string
     */
    function get_default_libelle() {
        return $this->getVal($this->clePrimaire)."&nbsp;".$this->getVal("autorite_police_decision");
    }

    /**
     *
     * @return array
     */
    function get_var_sql_forminc__champs() {
        return array(
            "autorite_police",
            "autorite_police_decision",
            "date_decision",
            "delai",
            "autorite_police_motif",
            "cloture",
            "date_notification",
            "date_butoir",
            "service",
            "dossier_instruction_reunion",
            "dossier_coordination",
            "etablissement",
            "dossier_instruction_reunion_prochain",
        );
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_autorite_police_decision() {
        return "SELECT autorite_police_decision.autorite_police_decision, autorite_police_decision.libelle FROM ".DB_PREFIXE."autorite_police_decision WHERE ((autorite_police_decision.om_validite_debut IS NULL AND (autorite_police_decision.om_validite_fin IS NULL OR autorite_police_decision.om_validite_fin > CURRENT_DATE)) OR (autorite_police_decision.om_validite_debut <= CURRENT_DATE AND (autorite_police_decision.om_validite_fin IS NULL OR autorite_police_decision.om_validite_fin > CURRENT_DATE))) ORDER BY autorite_police_decision.libelle ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_autorite_police_decision_by_id() {
        return "SELECT autorite_police_decision.autorite_police_decision, autorite_police_decision.libelle FROM ".DB_PREFIXE."autorite_police_decision WHERE autorite_police_decision = <idx>";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_autorite_police_motif() {
        return "SELECT autorite_police_motif.autorite_police_motif, autorite_police_motif.libelle FROM ".DB_PREFIXE."autorite_police_motif WHERE ((autorite_police_motif.om_validite_debut IS NULL AND (autorite_police_motif.om_validite_fin IS NULL OR autorite_police_motif.om_validite_fin > CURRENT_DATE)) OR (autorite_police_motif.om_validite_debut <= CURRENT_DATE AND (autorite_police_motif.om_validite_fin IS NULL OR autorite_police_motif.om_validite_fin > CURRENT_DATE))) ORDER BY autorite_police_motif.libelle ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_autorite_police_motif_by_id() {
        return "SELECT autorite_police_motif.autorite_police_motif, autorite_police_motif.libelle FROM ".DB_PREFIXE."autorite_police_motif WHERE autorite_police_motif = <idx>";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_dossier_coordination() {
        return "SELECT dossier_coordination.dossier_coordination, dossier_coordination.libelle FROM ".DB_PREFIXE."dossier_coordination ORDER BY dossier_coordination.libelle ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_dossier_coordination_by_id() {
        return "SELECT dossier_coordination.dossier_coordination, dossier_coordination.libelle FROM ".DB_PREFIXE."dossier_coordination WHERE dossier_coordination = <idx>";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_dossier_instruction_reunion() {
        return "SELECT dossier_instruction_reunion.dossier_instruction_reunion, dossier_instruction_reunion.dossier_instruction FROM ".DB_PREFIXE."dossier_instruction_reunion ORDER BY dossier_instruction_reunion.dossier_instruction ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_dossier_instruction_reunion_by_id() {
        return "SELECT dossier_instruction_reunion.dossier_instruction_reunion, dossier_instruction_reunion.dossier_instruction FROM ".DB_PREFIXE."dossier_instruction_reunion WHERE dossier_instruction_reunion = <idx>";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_dossier_instruction_reunion_prochain() {
        return "SELECT dossier_instruction_reunion.dossier_instruction_reunion, dossier_instruction_reunion.dossier_instruction FROM ".DB_PREFIXE."dossier_instruction_reunion ORDER BY dossier_instruction_reunion.dossier_instruction ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_dossier_instruction_reunion_prochain_by_id() {
        return "SELECT dossier_instruction_reunion.dossier_instruction_reunion, dossier_instruction_reunion.dossier_instruction FROM ".DB_PREFIXE."dossier_instruction_reunion WHERE dossier_instruction_reunion = <idx>";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_etablissement() {
        return "SELECT etablissement.etablissement, etablissement.libelle FROM ".DB_PREFIXE."etablissement WHERE ((etablissement.om_validite_debut IS NULL AND (etablissement.om_validite_fin IS NULL OR etablissement.om_validite_fin > CURRENT_DATE)) OR (etablissement.om_validite_debut <= CURRENT_DATE AND (etablissement.om_validite_fin IS NULL OR etablissement.om_validite_fin > CURRENT_DATE))) ORDER BY etablissement.libelle ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_etablissement_by_id() {
        return "SELECT etablissement.etablissement, etablissement.libelle FROM ".DB_PREFIXE."etablissement WHERE etablissement = <idx>";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_service() {
        return "SELECT service.service, service.libelle FROM ".DB_PREFIXE."service WHERE ((service.om_validite_debut IS NULL AND (service.om_validite_fin IS NULL OR service.om_validite_fin > CURRENT_DATE)) OR (service.om_validite_debut <= CURRENT_DATE AND (service.om_validite_fin IS NULL OR service.om_validite_fin > CURRENT_DATE))) ORDER BY service.libelle ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_service_by_id() {
        return "SELECT service.service, service.libelle FROM ".DB_PREFIXE."service WHERE service = <idx>";
    }




    function setvalF($val = array()) {
        //affectation valeur formulaire
        if (!is_numeric($val['autorite_police'])) {
            $this->valF['autorite_police'] = ""; // -> requis
        } else {
            $this->valF['autorite_police'] = $val['autorite_police'];
        }
        if (!is_numeric($val['autorite_police_decision'])) {
            $this->valF['autorite_police_decision'] = ""; // -> requis
        } else {
            $this->valF['autorite_police_decision'] = $val['autorite_police_decision'];
        }
        if ($val['date_decision'] != "") {
            $this->valF['date_decision'] = $this->dateDB($val['date_decision']);
        }
        if (!is_numeric($val['delai'])) {
            $this->valF['delai'] = ""; // -> requis
        } else {
            $this->valF['delai'] = $val['delai'];
        }
        if (!is_numeric($val['autorite_police_motif'])) {
            $this->valF['autorite_police_motif'] = ""; // -> requis
        } else {
            $this->valF['autorite_police_motif'] = $val['autorite_police_motif'];
        }
        if ($val['cloture'] == 1 || $val['cloture'] == "t" || $val['cloture'] == "Oui") {
            $this->valF['cloture'] = true;
        } else {
            $this->valF['cloture'] = false;
        }
        if ($val['date_notification'] != "") {
            $this->valF['date_notification'] = $this->dateDB($val['date_notification']);
        } else {
            $this->valF['date_notification'] = NULL;
        }
        if ($val['date_butoir'] != "") {
            $this->valF['date_butoir'] = $this->dateDB($val['date_butoir']);
        } else {
            $this->valF['date_butoir'] = NULL;
        }
        if (!is_numeric($val['service'])) {
            $this->valF['service'] = ""; // -> requis
        } else {
            $this->valF['service'] = $val['service'];
        }
        if (!is_numeric($val['dossier_instruction_reunion'])) {
            $this->valF['dossier_instruction_reunion'] = NULL;
        } else {
            $this->valF['dossier_instruction_reunion'] = $val['dossier_instruction_reunion'];
        }
        if (!is_numeric($val['dossier_coordination'])) {
            $this->valF['dossier_coordination'] = NULL;
        } else {
            $this->valF['dossier_coordination'] = $val['dossier_coordination'];
        }
        if (!is_numeric($val['etablissement'])) {
            $this->valF['etablissement'] = NULL;
        } else {
            $this->valF['etablissement'] = $val['etablissement'];
        }
        if (!is_numeric($val['dossier_instruction_reunion_prochain'])) {
            $this->valF['dossier_instruction_reunion_prochain'] = NULL;
        } else {
            $this->valF['dossier_instruction_reunion_prochain'] = $val['dossier_instruction_reunion_prochain'];
        }
    }

    //=================================================
    //cle primaire automatique [automatic primary key]
    //==================================================

    function setId(&$dnu1 = null) {
    //numero automatique
        $this->valF[$this->clePrimaire] = $this->f->db->nextId(DB_PREFIXE.$this->table);
    }

    function setValFAjout($val = array()) {
    //numero automatique -> pas de controle ajout cle primaire
    }

    function verifierAjout($val = array(), &$dnu1 = null) {
    //numero automatique -> pas de verfication de cle primaire
    }

    //==========================
    // Formulaire  [form]
    //==========================
    /**
     *
     */
    function setType(&$form, $maj) {
        // Récupération du mode de l'action
        $crud = $this->get_action_crud($maj);

        // MODE AJOUTER
        if ($maj == 0 || $crud == 'create') {
            $form->setType("autorite_police", "hidden");
            if ($this->is_in_context_of_foreign_key("autorite_police_decision", $this->retourformulaire)) {
                $form->setType("autorite_police_decision", "selecthiddenstatic");
            } else {
                $form->setType("autorite_police_decision", "select");
            }
            $form->setType("date_decision", "date");
            $form->setType("delai", "text");
            if ($this->is_in_context_of_foreign_key("autorite_police_motif", $this->retourformulaire)) {
                $form->setType("autorite_police_motif", "selecthiddenstatic");
            } else {
                $form->setType("autorite_police_motif", "select");
            }
            $form->setType("cloture", "checkbox");
            $form->setType("date_notification", "date");
            $form->setType("date_butoir", "date");
            if ($this->is_in_context_of_foreign_key("service", $this->retourformulaire)) {
                $form->setType("service", "selecthiddenstatic");
            } else {
                $form->setType("service", "select");
            }
            if ($this->is_in_context_of_foreign_key("dossier_instruction_reunion", $this->retourformulaire)) {
                $form->setType("dossier_instruction_reunion", "selecthiddenstatic");
            } else {
                $form->setType("dossier_instruction_reunion", "select");
            }
            if ($this->is_in_context_of_foreign_key("dossier_coordination", $this->retourformulaire)) {
                $form->setType("dossier_coordination", "selecthiddenstatic");
            } else {
                $form->setType("dossier_coordination", "select");
            }
            if ($this->is_in_context_of_foreign_key("etablissement", $this->retourformulaire)) {
                $form->setType("etablissement", "selecthiddenstatic");
            } else {
                $form->setType("etablissement", "select");
            }
            if ($this->is_in_context_of_foreign_key("dossier_instruction_reunion", $this->retourformulaire)) {
                $form->setType("dossier_instruction_reunion_prochain", "selecthiddenstatic");
            } else {
                $form->setType("dossier_instruction_reunion_prochain", "select");
            }
        }

        // MDOE MODIFIER
        if ($maj == 1 || $crud == 'update') {
            $form->setType("autorite_police", "hiddenstatic");
            if ($this->is_in_context_of_foreign_key("autorite_police_decision", $this->retourformulaire)) {
                $form->setType("autorite_police_decision", "selecthiddenstatic");
            } else {
                $form->setType("autorite_police_decision", "select");
            }
            $form->setType("date_decision", "date");
            $form->setType("delai", "text");
            if ($this->is_in_context_of_foreign_key("autorite_police_motif", $this->retourformulaire)) {
                $form->setType("autorite_police_motif", "selecthiddenstatic");
            } else {
                $form->setType("autorite_police_motif", "select");
            }
            $form->setType("cloture", "checkbox");
            $form->setType("date_notification", "date");
            $form->setType("date_butoir", "date");
            if ($this->is_in_context_of_foreign_key("service", $this->retourformulaire)) {
                $form->setType("service", "selecthiddenstatic");
            } else {
                $form->setType("service", "select");
            }
            if ($this->is_in_context_of_foreign_key("dossier_instruction_reunion", $this->retourformulaire)) {
                $form->setType("dossier_instruction_reunion", "selecthiddenstatic");
            } else {
                $form->setType("dossier_instruction_reunion", "select");
            }
            if ($this->is_in_context_of_foreign_key("dossier_coordination", $this->retourformulaire)) {
                $form->setType("dossier_coordination", "selecthiddenstatic");
            } else {
                $form->setType("dossier_coordination", "select");
            }
            if ($this->is_in_context_of_foreign_key("etablissement", $this->retourformulaire)) {
                $form->setType("etablissement", "selecthiddenstatic");
            } else {
                $form->setType("etablissement", "select");
            }
            if ($this->is_in_context_of_foreign_key("dossier_instruction_reunion", $this->retourformulaire)) {
                $form->setType("dossier_instruction_reunion_prochain", "selecthiddenstatic");
            } else {
                $form->setType("dossier_instruction_reunion_prochain", "select");
            }
        }

        // MODE SUPPRIMER
        if ($maj == 2 || $crud == 'delete') {
            $form->setType("autorite_police", "hiddenstatic");
            $form->setType("autorite_police_decision", "selectstatic");
            $form->setType("date_decision", "hiddenstatic");
            $form->setType("delai", "hiddenstatic");
            $form->setType("autorite_police_motif", "selectstatic");
            $form->setType("cloture", "hiddenstatic");
            $form->setType("date_notification", "hiddenstatic");
            $form->setType("date_butoir", "hiddenstatic");
            $form->setType("service", "selectstatic");
            $form->setType("dossier_instruction_reunion", "selectstatic");
            $form->setType("dossier_coordination", "selectstatic");
            $form->setType("etablissement", "selectstatic");
            $form->setType("dossier_instruction_reunion_prochain", "selectstatic");
        }

        // MODE CONSULTER
        if ($maj == 3 || $crud == 'read') {
            $form->setType("autorite_police", "static");
            $form->setType("autorite_police_decision", "selectstatic");
            $form->setType("date_decision", "datestatic");
            $form->setType("delai", "static");
            $form->setType("autorite_police_motif", "selectstatic");
            $form->setType("cloture", "checkboxstatic");
            $form->setType("date_notification", "datestatic");
            $form->setType("date_butoir", "datestatic");
            $form->setType("service", "selectstatic");
            $form->setType("dossier_instruction_reunion", "selectstatic");
            $form->setType("dossier_coordination", "selectstatic");
            $form->setType("etablissement", "selectstatic");
            $form->setType("dossier_instruction_reunion_prochain", "selectstatic");
        }

    }


    function setOnchange(&$form, $maj) {
    //javascript controle client
        $form->setOnchange('autorite_police','VerifNum(this)');
        $form->setOnchange('autorite_police_decision','VerifNum(this)');
        $form->setOnchange('date_decision','fdate(this)');
        $form->setOnchange('delai','VerifNum(this)');
        $form->setOnchange('autorite_police_motif','VerifNum(this)');
        $form->setOnchange('date_notification','fdate(this)');
        $form->setOnchange('date_butoir','fdate(this)');
        $form->setOnchange('service','VerifNum(this)');
        $form->setOnchange('dossier_instruction_reunion','VerifNum(this)');
        $form->setOnchange('dossier_coordination','VerifNum(this)');
        $form->setOnchange('etablissement','VerifNum(this)');
        $form->setOnchange('dossier_instruction_reunion_prochain','VerifNum(this)');
    }
    /**
     * Methode setTaille
     */
    function setTaille(&$form, $maj) {
        $form->setTaille("autorite_police", 11);
        $form->setTaille("autorite_police_decision", 11);
        $form->setTaille("date_decision", 12);
        $form->setTaille("delai", 11);
        $form->setTaille("autorite_police_motif", 11);
        $form->setTaille("cloture", 1);
        $form->setTaille("date_notification", 12);
        $form->setTaille("date_butoir", 12);
        $form->setTaille("service", 11);
        $form->setTaille("dossier_instruction_reunion", 11);
        $form->setTaille("dossier_coordination", 11);
        $form->setTaille("etablissement", 11);
        $form->setTaille("dossier_instruction_reunion_prochain", 11);
    }

    /**
     * Methode setMax
     */
    function setMax(&$form, $maj) {
        $form->setMax("autorite_police", 11);
        $form->setMax("autorite_police_decision", 11);
        $form->setMax("date_decision", 12);
        $form->setMax("delai", 11);
        $form->setMax("autorite_police_motif", 11);
        $form->setMax("cloture", 1);
        $form->setMax("date_notification", 12);
        $form->setMax("date_butoir", 12);
        $form->setMax("service", 11);
        $form->setMax("dossier_instruction_reunion", 11);
        $form->setMax("dossier_coordination", 11);
        $form->setMax("etablissement", 11);
        $form->setMax("dossier_instruction_reunion_prochain", 11);
    }


    function setLib(&$form, $maj) {
    //libelle des champs
        $form->setLib('autorite_police', __('autorite_police'));
        $form->setLib('autorite_police_decision', __('autorite_police_decision'));
        $form->setLib('date_decision', __('date_decision'));
        $form->setLib('delai', __('delai'));
        $form->setLib('autorite_police_motif', __('autorite_police_motif'));
        $form->setLib('cloture', __('cloture'));
        $form->setLib('date_notification', __('date_notification'));
        $form->setLib('date_butoir', __('date_butoir'));
        $form->setLib('service', __('service'));
        $form->setLib('dossier_instruction_reunion', __('dossier_instruction_reunion'));
        $form->setLib('dossier_coordination', __('dossier_coordination'));
        $form->setLib('etablissement', __('etablissement'));
        $form->setLib('dossier_instruction_reunion_prochain', __('dossier_instruction_reunion_prochain'));
    }
    /**
     *
     */
    function setSelect(&$form, $maj, &$dnu1 = null, $dnu2 = null) {

        // autorite_police_decision
        $this->init_select(
            $form, 
            $this->f->db,
            $maj,
            null,
            "autorite_police_decision",
            $this->get_var_sql_forminc__sql("autorite_police_decision"),
            $this->get_var_sql_forminc__sql("autorite_police_decision_by_id"),
            true
        );
        // autorite_police_motif
        $this->init_select(
            $form, 
            $this->f->db,
            $maj,
            null,
            "autorite_police_motif",
            $this->get_var_sql_forminc__sql("autorite_police_motif"),
            $this->get_var_sql_forminc__sql("autorite_police_motif_by_id"),
            true
        );
        // dossier_coordination
        $this->init_select(
            $form, 
            $this->f->db,
            $maj,
            null,
            "dossier_coordination",
            $this->get_var_sql_forminc__sql("dossier_coordination"),
            $this->get_var_sql_forminc__sql("dossier_coordination_by_id"),
            false
        );
        // dossier_instruction_reunion
        $this->init_select(
            $form, 
            $this->f->db,
            $maj,
            null,
            "dossier_instruction_reunion",
            $this->get_var_sql_forminc__sql("dossier_instruction_reunion"),
            $this->get_var_sql_forminc__sql("dossier_instruction_reunion_by_id"),
            false
        );
        // dossier_instruction_reunion_prochain
        $this->init_select(
            $form, 
            $this->f->db,
            $maj,
            null,
            "dossier_instruction_reunion_prochain",
            $this->get_var_sql_forminc__sql("dossier_instruction_reunion_prochain"),
            $this->get_var_sql_forminc__sql("dossier_instruction_reunion_prochain_by_id"),
            false
        );
        // etablissement
        $this->init_select(
            $form, 
            $this->f->db,
            $maj,
            null,
            "etablissement",
            $this->get_var_sql_forminc__sql("etablissement"),
            $this->get_var_sql_forminc__sql("etablissement_by_id"),
            true
        );
        // service
        $this->init_select(
            $form, 
            $this->f->db,
            $maj,
            null,
            "service",
            $this->get_var_sql_forminc__sql("service"),
            $this->get_var_sql_forminc__sql("service_by_id"),
            true
        );
    }


    //==================================
    // sous Formulaire
    //==================================
    

    function setValsousformulaire(&$form, $maj, $validation, $idxformulaire, $retourformulaire, $typeformulaire, &$dnu1 = null, $dnu2 = null) {
        $this->retourformulaire = $retourformulaire;
        if($validation == 0) {
            if($this->is_in_context_of_foreign_key('autorite_police_decision', $this->retourformulaire))
                $form->setVal('autorite_police_decision', $idxformulaire);
            if($this->is_in_context_of_foreign_key('autorite_police_motif', $this->retourformulaire))
                $form->setVal('autorite_police_motif', $idxformulaire);
            if($this->is_in_context_of_foreign_key('dossier_coordination', $this->retourformulaire))
                $form->setVal('dossier_coordination', $idxformulaire);
            if($this->is_in_context_of_foreign_key('etablissement', $this->retourformulaire))
                $form->setVal('etablissement', $idxformulaire);
            if($this->is_in_context_of_foreign_key('service', $this->retourformulaire))
                $form->setVal('service', $idxformulaire);
        }// fin validation
        if ($validation == 0 and $maj == 0) {
            if($this->is_in_context_of_foreign_key('dossier_instruction_reunion', $this->retourformulaire))
                $form->setVal('dossier_instruction_reunion', $idxformulaire);
            if($this->is_in_context_of_foreign_key('dossier_instruction_reunion', $this->retourformulaire))
                $form->setVal('dossier_instruction_reunion_prochain', $idxformulaire);
        }// fin validation
        $this->set_form_default_values($form, $maj, $validation);
    }// fin setValsousformulaire

    //==================================
    // cle secondaire
    //==================================
    
    /**
     * Methode clesecondaire
     */
    function cleSecondaire($id, &$dnu1 = null, $val = array(), $dnu2 = null) {
        // On appelle la methode de la classe parent
        parent::cleSecondaire($id);
        // Verification de la cle secondaire : lien_autorite_police_courrier
        $this->rechercheTable($this->f->db, "lien_autorite_police_courrier", "autorite_police", $id);
    }


}
