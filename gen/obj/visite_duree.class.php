<?php
//$Id$ 
//gen openMairie le 03/05/2018 09:12

require_once "../obj/om_dbform.class.php";

class visite_duree_gen extends om_dbform {

    protected $_absolute_class_name = "visite_duree";

    var $table = "visite_duree";
    var $clePrimaire = "visite_duree";
    var $typeCle = "N";
    var $required_field = array(
        "visite_duree"
    );
    
    var $foreign_keys_extended = array(
    );
    
    /**
     *
     * @return string
     */
    function get_default_libelle() {
        return $this->getVal($this->clePrimaire)."&nbsp;".$this->getVal("libelle");
    }

    /**
     *
     * @return array
     */
    function get_var_sql_forminc__champs() {
        return array(
            "visite_duree",
            "libelle",
            "duree",
        );
    }




    function setvalF($val = array()) {
        //affectation valeur formulaire
        if (!is_numeric($val['visite_duree'])) {
            $this->valF['visite_duree'] = ""; // -> requis
        } else {
            $this->valF['visite_duree'] = $val['visite_duree'];
        }
        if ($val['libelle'] == "") {
            $this->valF['libelle'] = NULL;
        } else {
            $this->valF['libelle'] = $val['libelle'];
        }
        if (!is_numeric($val['duree'])) {
            $this->valF['duree'] = NULL;
        } else {
            $this->valF['duree'] = $val['duree'];
        }
    }

    //=================================================
    //cle primaire automatique [automatic primary key]
    //==================================================

    function setId(&$dnu1 = null) {
    //numero automatique
        $this->valF[$this->clePrimaire] = $this->f->db->nextId(DB_PREFIXE.$this->table);
    }

    function setValFAjout($val = array()) {
    //numero automatique -> pas de controle ajout cle primaire
    }

    function verifierAjout($val = array(), &$dnu1 = null) {
    //numero automatique -> pas de verfication de cle primaire
    }

    //==========================
    // Formulaire  [form]
    //==========================
    /**
     *
     */
    function setType(&$form, $maj) {
        // Récupération du mode de l'action
        $crud = $this->get_action_crud($maj);

        // MODE AJOUTER
        if ($maj == 0 || $crud == 'create') {
            $form->setType("visite_duree", "hidden");
            $form->setType("libelle", "text");
            $form->setType("duree", "text");
        }

        // MDOE MODIFIER
        if ($maj == 1 || $crud == 'update') {
            $form->setType("visite_duree", "hiddenstatic");
            $form->setType("libelle", "text");
            $form->setType("duree", "text");
        }

        // MODE SUPPRIMER
        if ($maj == 2 || $crud == 'delete') {
            $form->setType("visite_duree", "hiddenstatic");
            $form->setType("libelle", "hiddenstatic");
            $form->setType("duree", "hiddenstatic");
        }

        // MODE CONSULTER
        if ($maj == 3 || $crud == 'read') {
            $form->setType("visite_duree", "static");
            $form->setType("libelle", "static");
            $form->setType("duree", "static");
        }

    }


    function setOnchange(&$form, $maj) {
    //javascript controle client
        $form->setOnchange('visite_duree','VerifNum(this)');
        $form->setOnchange('duree','VerifFloat(this)');
    }
    /**
     * Methode setTaille
     */
    function setTaille(&$form, $maj) {
        $form->setTaille("visite_duree", 11);
        $form->setTaille("libelle", 20);
        $form->setTaille("duree", 20);
    }

    /**
     * Methode setMax
     */
    function setMax(&$form, $maj) {
        $form->setMax("visite_duree", 11);
        $form->setMax("libelle", 20);
        $form->setMax("duree", 20);
    }


    function setLib(&$form, $maj) {
    //libelle des champs
        $form->setLib('visite_duree', __('visite_duree'));
        $form->setLib('libelle', __('libelle'));
        $form->setLib('duree', __('duree'));
    }
    /**
     *
     */
    function setSelect(&$form, $maj, &$dnu1 = null, $dnu2 = null) {

    }


    //==================================
    // sous Formulaire
    //==================================
    

    function setValsousformulaire(&$form, $maj, $validation, $idxformulaire, $retourformulaire, $typeformulaire, &$dnu1 = null, $dnu2 = null) {
        $this->retourformulaire = $retourformulaire;
        $this->set_form_default_values($form, $maj, $validation);
    }// fin setValsousformulaire

    //==================================
    // cle secondaire
    //==================================
    
    /**
     * Methode clesecondaire
     */
    function cleSecondaire($id, &$dnu1 = null, $val = array(), $dnu2 = null) {
        // On appelle la methode de la classe parent
        parent::cleSecondaire($id);
        // Verification de la cle secondaire : etablissement
        $this->rechercheTable($this->f->db, "etablissement", "si_visite_duree", $id);
    }


}
