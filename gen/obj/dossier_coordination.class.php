<?php
//$Id$ 
//gen openMairie le 16/07/2020 17:34

require_once "../obj/om_dbform.class.php";

class dossier_coordination_gen extends om_dbform {

    protected $_absolute_class_name = "dossier_coordination";

    var $table = "dossier_coordination";
    var $clePrimaire = "dossier_coordination";
    var $typeCle = "N";
    var $required_field = array(
        "date_demande",
        "dossier_coordination",
        "dossier_coordination_type"
    );
    var $unique_key = array(
      "libelle",
    );
    var $foreign_keys_extended = array(
        "dossier_coordination" => array("dossier_coordination", "dossier_coordination_nouveau", "dossier_coordination_a_qualifier", "dossier_coordination_a_cloturer", ),
        "dossier_coordination_type" => array("dossier_coordination_type", ),
        "etablissement" => array("etablissement", "etablissement_referentiel_erp", "etablissement_tous", ),
        "etablissement_categorie" => array("etablissement_categorie", ),
        "etablissement_type" => array("etablissement_type", ),
    );
    
    /**
     *
     * @return string
     */
    function get_default_libelle() {
        return $this->getVal($this->clePrimaire)."&nbsp;".$this->getVal("libelle");
    }

    /**
     *
     * @return array
     */
    function get_var_sql_forminc__champs() {
        return array(
            "dossier_coordination",
            "etablissement",
            "libelle",
            "dossier_coordination_type",
            "date_demande",
            "date_butoir",
            "dossier_autorisation_ads",
            "dossier_instruction_ads",
            "a_qualifier",
            "etablissement_type",
            "etablissement_categorie",
            "erp",
            "dossier_cloture",
            "contraintes_urba_om_html",
            "etablissement_locaux_sommeil",
            "references_cadastrales",
            "dossier_coordination_parent",
            "description",
            "dossier_instruction_secu",
            "dossier_instruction_acc",
            "autorite_police_encours",
            "date_cloture",
            "geolocalise",
            "interface_referentiel_ads",
            "enjeu_erp",
            "depot_de_piece",
            "enjeu_ads",
            "geom_point",
            "geom_emprise",
            "dossier_ads_force",
        );
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_dossier_coordination_parent() {
        return "SELECT dossier_coordination.dossier_coordination, dossier_coordination.libelle FROM ".DB_PREFIXE."dossier_coordination ORDER BY dossier_coordination.libelle ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_dossier_coordination_parent_by_id() {
        return "SELECT dossier_coordination.dossier_coordination, dossier_coordination.libelle FROM ".DB_PREFIXE."dossier_coordination WHERE dossier_coordination = <idx>";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_dossier_coordination_type() {
        return "SELECT dossier_coordination_type.dossier_coordination_type, dossier_coordination_type.libelle FROM ".DB_PREFIXE."dossier_coordination_type WHERE ((dossier_coordination_type.om_validite_debut IS NULL AND (dossier_coordination_type.om_validite_fin IS NULL OR dossier_coordination_type.om_validite_fin > CURRENT_DATE)) OR (dossier_coordination_type.om_validite_debut <= CURRENT_DATE AND (dossier_coordination_type.om_validite_fin IS NULL OR dossier_coordination_type.om_validite_fin > CURRENT_DATE))) ORDER BY dossier_coordination_type.libelle ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_dossier_coordination_type_by_id() {
        return "SELECT dossier_coordination_type.dossier_coordination_type, dossier_coordination_type.libelle FROM ".DB_PREFIXE."dossier_coordination_type WHERE dossier_coordination_type = <idx>";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_etablissement() {
        return "SELECT etablissement.etablissement, etablissement.libelle FROM ".DB_PREFIXE."etablissement WHERE ((etablissement.om_validite_debut IS NULL AND (etablissement.om_validite_fin IS NULL OR etablissement.om_validite_fin > CURRENT_DATE)) OR (etablissement.om_validite_debut <= CURRENT_DATE AND (etablissement.om_validite_fin IS NULL OR etablissement.om_validite_fin > CURRENT_DATE))) ORDER BY etablissement.libelle ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_etablissement_by_id() {
        return "SELECT etablissement.etablissement, etablissement.libelle FROM ".DB_PREFIXE."etablissement WHERE etablissement = <idx>";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_etablissement_categorie() {
        return "SELECT etablissement_categorie.etablissement_categorie, etablissement_categorie.libelle FROM ".DB_PREFIXE."etablissement_categorie WHERE ((etablissement_categorie.om_validite_debut IS NULL AND (etablissement_categorie.om_validite_fin IS NULL OR etablissement_categorie.om_validite_fin > CURRENT_DATE)) OR (etablissement_categorie.om_validite_debut <= CURRENT_DATE AND (etablissement_categorie.om_validite_fin IS NULL OR etablissement_categorie.om_validite_fin > CURRENT_DATE))) ORDER BY etablissement_categorie.libelle ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_etablissement_categorie_by_id() {
        return "SELECT etablissement_categorie.etablissement_categorie, etablissement_categorie.libelle FROM ".DB_PREFIXE."etablissement_categorie WHERE etablissement_categorie = <idx>";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_etablissement_type() {
        return "SELECT etablissement_type.etablissement_type, etablissement_type.libelle FROM ".DB_PREFIXE."etablissement_type WHERE ((etablissement_type.om_validite_debut IS NULL AND (etablissement_type.om_validite_fin IS NULL OR etablissement_type.om_validite_fin > CURRENT_DATE)) OR (etablissement_type.om_validite_debut <= CURRENT_DATE AND (etablissement_type.om_validite_fin IS NULL OR etablissement_type.om_validite_fin > CURRENT_DATE))) ORDER BY etablissement_type.libelle ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_etablissement_type_by_id() {
        return "SELECT etablissement_type.etablissement_type, etablissement_type.libelle FROM ".DB_PREFIXE."etablissement_type WHERE etablissement_type = <idx>";
    }




    function setvalF($val = array()) {
        //affectation valeur formulaire
        if (!is_numeric($val['dossier_coordination'])) {
            $this->valF['dossier_coordination'] = ""; // -> requis
        } else {
            $this->valF['dossier_coordination'] = $val['dossier_coordination'];
        }
        if (!is_numeric($val['etablissement'])) {
            $this->valF['etablissement'] = NULL;
        } else {
            $this->valF['etablissement'] = $val['etablissement'];
        }
        if ($val['libelle'] == "") {
            $this->valF['libelle'] = NULL;
        } else {
            $this->valF['libelle'] = $val['libelle'];
        }
        if (!is_numeric($val['dossier_coordination_type'])) {
            $this->valF['dossier_coordination_type'] = ""; // -> requis
        } else {
            $this->valF['dossier_coordination_type'] = $val['dossier_coordination_type'];
        }
        if ($val['date_demande'] != "") {
            $this->valF['date_demande'] = $this->dateDB($val['date_demande']);
        }
        if ($val['date_butoir'] != "") {
            $this->valF['date_butoir'] = $this->dateDB($val['date_butoir']);
        } else {
            $this->valF['date_butoir'] = NULL;
        }
        if ($val['dossier_autorisation_ads'] == "") {
            $this->valF['dossier_autorisation_ads'] = NULL;
        } else {
            $this->valF['dossier_autorisation_ads'] = $val['dossier_autorisation_ads'];
        }
        if ($val['dossier_instruction_ads'] == "") {
            $this->valF['dossier_instruction_ads'] = NULL;
        } else {
            $this->valF['dossier_instruction_ads'] = $val['dossier_instruction_ads'];
        }
        if ($val['a_qualifier'] == 1 || $val['a_qualifier'] == "t" || $val['a_qualifier'] == "Oui") {
            $this->valF['a_qualifier'] = true;
        } else {
            $this->valF['a_qualifier'] = false;
        }
        if (!is_numeric($val['etablissement_type'])) {
            $this->valF['etablissement_type'] = NULL;
        } else {
            $this->valF['etablissement_type'] = $val['etablissement_type'];
        }
        if (!is_numeric($val['etablissement_categorie'])) {
            $this->valF['etablissement_categorie'] = NULL;
        } else {
            $this->valF['etablissement_categorie'] = $val['etablissement_categorie'];
        }
        if ($val['erp'] == 1 || $val['erp'] == "t" || $val['erp'] == "Oui") {
            $this->valF['erp'] = true;
        } else {
            $this->valF['erp'] = false;
        }
        if ($val['dossier_cloture'] == 1 || $val['dossier_cloture'] == "t" || $val['dossier_cloture'] == "Oui") {
            $this->valF['dossier_cloture'] = true;
        } else {
            $this->valF['dossier_cloture'] = false;
        }
            $this->valF['contraintes_urba_om_html'] = $val['contraintes_urba_om_html'];
        if ($val['etablissement_locaux_sommeil'] == 1 || $val['etablissement_locaux_sommeil'] == "t" || $val['etablissement_locaux_sommeil'] == "Oui") {
            $this->valF['etablissement_locaux_sommeil'] = true;
        } else {
            $this->valF['etablissement_locaux_sommeil'] = false;
        }
            $this->valF['references_cadastrales'] = $val['references_cadastrales'];
        if (!is_numeric($val['dossier_coordination_parent'])) {
            $this->valF['dossier_coordination_parent'] = NULL;
        } else {
            $this->valF['dossier_coordination_parent'] = $val['dossier_coordination_parent'];
        }
            $this->valF['description'] = $val['description'];
        if ($val['dossier_instruction_secu'] == 1 || $val['dossier_instruction_secu'] == "t" || $val['dossier_instruction_secu'] == "Oui") {
            $this->valF['dossier_instruction_secu'] = true;
        } else {
            $this->valF['dossier_instruction_secu'] = false;
        }
        if ($val['dossier_instruction_acc'] == 1 || $val['dossier_instruction_acc'] == "t" || $val['dossier_instruction_acc'] == "Oui") {
            $this->valF['dossier_instruction_acc'] = true;
        } else {
            $this->valF['dossier_instruction_acc'] = false;
        }
        if ($val['autorite_police_encours'] == 1 || $val['autorite_police_encours'] == "t" || $val['autorite_police_encours'] == "Oui") {
            $this->valF['autorite_police_encours'] = true;
        } else {
            $this->valF['autorite_police_encours'] = false;
        }
        if ($val['date_cloture'] != "") {
            $this->valF['date_cloture'] = $this->dateDB($val['date_cloture']);
        } else {
            $this->valF['date_cloture'] = NULL;
        }
        if ($val['geolocalise'] == 1 || $val['geolocalise'] == "t" || $val['geolocalise'] == "Oui") {
            $this->valF['geolocalise'] = true;
        } else {
            $this->valF['geolocalise'] = false;
        }
        if ($val['interface_referentiel_ads'] == 1 || $val['interface_referentiel_ads'] == "t" || $val['interface_referentiel_ads'] == "Oui") {
            $this->valF['interface_referentiel_ads'] = true;
        } else {
            $this->valF['interface_referentiel_ads'] = false;
        }
        if ($val['enjeu_erp'] == 1 || $val['enjeu_erp'] == "t" || $val['enjeu_erp'] == "Oui") {
            $this->valF['enjeu_erp'] = true;
        } else {
            $this->valF['enjeu_erp'] = false;
        }
        if ($val['depot_de_piece'] == 1 || $val['depot_de_piece'] == "t" || $val['depot_de_piece'] == "Oui") {
            $this->valF['depot_de_piece'] = true;
        } else {
            $this->valF['depot_de_piece'] = false;
        }
        if ($val['enjeu_ads'] == 1 || $val['enjeu_ads'] == "t" || $val['enjeu_ads'] == "Oui") {
            $this->valF['enjeu_ads'] = true;
        } else {
            $this->valF['enjeu_ads'] = false;
        }
        if ($val['geom_point'] == "") {
            unset($this->valF['geom_point']);
        } else {
            $this->valF['geom_point'] = $val['geom_point'];
        }
        if ($val['geom_emprise'] == "") {
            unset($this->valF['geom_emprise']);
        } else {
            $this->valF['geom_emprise'] = $val['geom_emprise'];
        }
        if ($val['dossier_ads_force'] == 1 || $val['dossier_ads_force'] == "t" || $val['dossier_ads_force'] == "Oui") {
            $this->valF['dossier_ads_force'] = true;
        } else {
            $this->valF['dossier_ads_force'] = false;
        }
    }

    //=================================================
    //cle primaire automatique [automatic primary key]
    //==================================================

    function setId(&$dnu1 = null) {
    //numero automatique
        $this->valF[$this->clePrimaire] = $this->f->db->nextId(DB_PREFIXE.$this->table);
    }

    function setValFAjout($val = array()) {
    //numero automatique -> pas de controle ajout cle primaire
    }

    function verifierAjout($val = array(), &$dnu1 = null) {
    //numero automatique -> pas de verfication de cle primaire
    }

    //==========================
    // Formulaire  [form]
    //==========================
    /**
     *
     */
    function setType(&$form, $maj) {
        // Récupération du mode de l'action
        $crud = $this->get_action_crud($maj);

        // MODE AJOUTER
        if ($maj == 0 || $crud == 'create') {
            $form->setType("dossier_coordination", "hidden");
            if ($this->is_in_context_of_foreign_key("etablissement", $this->retourformulaire)) {
                $form->setType("etablissement", "selecthiddenstatic");
            } else {
                $form->setType("etablissement", "select");
            }
            $form->setType("libelle", "text");
            if ($this->is_in_context_of_foreign_key("dossier_coordination_type", $this->retourformulaire)) {
                $form->setType("dossier_coordination_type", "selecthiddenstatic");
            } else {
                $form->setType("dossier_coordination_type", "select");
            }
            $form->setType("date_demande", "date");
            $form->setType("date_butoir", "date");
            $form->setType("dossier_autorisation_ads", "text");
            $form->setType("dossier_instruction_ads", "text");
            $form->setType("a_qualifier", "checkbox");
            if ($this->is_in_context_of_foreign_key("etablissement_type", $this->retourformulaire)) {
                $form->setType("etablissement_type", "selecthiddenstatic");
            } else {
                $form->setType("etablissement_type", "select");
            }
            if ($this->is_in_context_of_foreign_key("etablissement_categorie", $this->retourformulaire)) {
                $form->setType("etablissement_categorie", "selecthiddenstatic");
            } else {
                $form->setType("etablissement_categorie", "select");
            }
            $form->setType("erp", "checkbox");
            $form->setType("dossier_cloture", "checkbox");
            $form->setType("contraintes_urba_om_html", "html");
            $form->setType("etablissement_locaux_sommeil", "checkbox");
            $form->setType("references_cadastrales", "textarea");
            if ($this->is_in_context_of_foreign_key("dossier_coordination", $this->retourformulaire)) {
                $form->setType("dossier_coordination_parent", "selecthiddenstatic");
            } else {
                $form->setType("dossier_coordination_parent", "select");
            }
            $form->setType("description", "textarea");
            $form->setType("dossier_instruction_secu", "checkbox");
            $form->setType("dossier_instruction_acc", "checkbox");
            $form->setType("autorite_police_encours", "checkbox");
            $form->setType("date_cloture", "date");
            $form->setType("geolocalise", "checkbox");
            $form->setType("interface_referentiel_ads", "checkbox");
            $form->setType("enjeu_erp", "checkbox");
            $form->setType("depot_de_piece", "checkbox");
            $form->setType("enjeu_ads", "checkbox");
            $form->setType("geom_point", "geom");
            $form->setType("geom_emprise", "geom");
            $form->setType("dossier_ads_force", "checkbox");
        }

        // MDOE MODIFIER
        if ($maj == 1 || $crud == 'update') {
            $form->setType("dossier_coordination", "hiddenstatic");
            if ($this->is_in_context_of_foreign_key("etablissement", $this->retourformulaire)) {
                $form->setType("etablissement", "selecthiddenstatic");
            } else {
                $form->setType("etablissement", "select");
            }
            $form->setType("libelle", "text");
            if ($this->is_in_context_of_foreign_key("dossier_coordination_type", $this->retourformulaire)) {
                $form->setType("dossier_coordination_type", "selecthiddenstatic");
            } else {
                $form->setType("dossier_coordination_type", "select");
            }
            $form->setType("date_demande", "date");
            $form->setType("date_butoir", "date");
            $form->setType("dossier_autorisation_ads", "text");
            $form->setType("dossier_instruction_ads", "text");
            $form->setType("a_qualifier", "checkbox");
            if ($this->is_in_context_of_foreign_key("etablissement_type", $this->retourformulaire)) {
                $form->setType("etablissement_type", "selecthiddenstatic");
            } else {
                $form->setType("etablissement_type", "select");
            }
            if ($this->is_in_context_of_foreign_key("etablissement_categorie", $this->retourformulaire)) {
                $form->setType("etablissement_categorie", "selecthiddenstatic");
            } else {
                $form->setType("etablissement_categorie", "select");
            }
            $form->setType("erp", "checkbox");
            $form->setType("dossier_cloture", "checkbox");
            $form->setType("contraintes_urba_om_html", "html");
            $form->setType("etablissement_locaux_sommeil", "checkbox");
            $form->setType("references_cadastrales", "textarea");
            if ($this->is_in_context_of_foreign_key("dossier_coordination", $this->retourformulaire)) {
                $form->setType("dossier_coordination_parent", "selecthiddenstatic");
            } else {
                $form->setType("dossier_coordination_parent", "select");
            }
            $form->setType("description", "textarea");
            $form->setType("dossier_instruction_secu", "checkbox");
            $form->setType("dossier_instruction_acc", "checkbox");
            $form->setType("autorite_police_encours", "checkbox");
            $form->setType("date_cloture", "date");
            $form->setType("geolocalise", "checkbox");
            $form->setType("interface_referentiel_ads", "checkbox");
            $form->setType("enjeu_erp", "checkbox");
            $form->setType("depot_de_piece", "checkbox");
            $form->setType("enjeu_ads", "checkbox");
            $form->setType("geom_point", "geom");
            $form->setType("geom_emprise", "geom");
            $form->setType("dossier_ads_force", "checkbox");
        }

        // MODE SUPPRIMER
        if ($maj == 2 || $crud == 'delete') {
            $form->setType("dossier_coordination", "hiddenstatic");
            $form->setType("etablissement", "selectstatic");
            $form->setType("libelle", "hiddenstatic");
            $form->setType("dossier_coordination_type", "selectstatic");
            $form->setType("date_demande", "hiddenstatic");
            $form->setType("date_butoir", "hiddenstatic");
            $form->setType("dossier_autorisation_ads", "hiddenstatic");
            $form->setType("dossier_instruction_ads", "hiddenstatic");
            $form->setType("a_qualifier", "hiddenstatic");
            $form->setType("etablissement_type", "selectstatic");
            $form->setType("etablissement_categorie", "selectstatic");
            $form->setType("erp", "hiddenstatic");
            $form->setType("dossier_cloture", "hiddenstatic");
            $form->setType("contraintes_urba_om_html", "hiddenstatic");
            $form->setType("etablissement_locaux_sommeil", "hiddenstatic");
            $form->setType("references_cadastrales", "hiddenstatic");
            $form->setType("dossier_coordination_parent", "selectstatic");
            $form->setType("description", "hiddenstatic");
            $form->setType("dossier_instruction_secu", "hiddenstatic");
            $form->setType("dossier_instruction_acc", "hiddenstatic");
            $form->setType("autorite_police_encours", "hiddenstatic");
            $form->setType("date_cloture", "hiddenstatic");
            $form->setType("geolocalise", "hiddenstatic");
            $form->setType("interface_referentiel_ads", "hiddenstatic");
            $form->setType("enjeu_erp", "hiddenstatic");
            $form->setType("depot_de_piece", "hiddenstatic");
            $form->setType("enjeu_ads", "hiddenstatic");
            $form->setType("geom_point", "geom");
            $form->setType("geom_emprise", "geom");
            $form->setType("dossier_ads_force", "hiddenstatic");
        }

        // MODE CONSULTER
        if ($maj == 3 || $crud == 'read') {
            $form->setType("dossier_coordination", "static");
            $form->setType("etablissement", "selectstatic");
            $form->setType("libelle", "static");
            $form->setType("dossier_coordination_type", "selectstatic");
            $form->setType("date_demande", "datestatic");
            $form->setType("date_butoir", "datestatic");
            $form->setType("dossier_autorisation_ads", "static");
            $form->setType("dossier_instruction_ads", "static");
            $form->setType("a_qualifier", "checkboxstatic");
            $form->setType("etablissement_type", "selectstatic");
            $form->setType("etablissement_categorie", "selectstatic");
            $form->setType("erp", "checkboxstatic");
            $form->setType("dossier_cloture", "checkboxstatic");
            $form->setType("contraintes_urba_om_html", "htmlstatic");
            $form->setType("etablissement_locaux_sommeil", "checkboxstatic");
            $form->setType("references_cadastrales", "textareastatic");
            $form->setType("dossier_coordination_parent", "selectstatic");
            $form->setType("description", "textareastatic");
            $form->setType("dossier_instruction_secu", "checkboxstatic");
            $form->setType("dossier_instruction_acc", "checkboxstatic");
            $form->setType("autorite_police_encours", "checkboxstatic");
            $form->setType("date_cloture", "datestatic");
            $form->setType("geolocalise", "checkboxstatic");
            $form->setType("interface_referentiel_ads", "checkboxstatic");
            $form->setType("enjeu_erp", "checkboxstatic");
            $form->setType("depot_de_piece", "checkboxstatic");
            $form->setType("enjeu_ads", "checkboxstatic");
            $form->setType("geom_point", "geom");
            $form->setType("geom_emprise", "geom");
            $form->setType("dossier_ads_force", "checkboxstatic");
        }

    }


    function setOnchange(&$form, $maj) {
    //javascript controle client
        $form->setOnchange('dossier_coordination','VerifNum(this)');
        $form->setOnchange('etablissement','VerifNum(this)');
        $form->setOnchange('dossier_coordination_type','VerifNum(this)');
        $form->setOnchange('date_demande','fdate(this)');
        $form->setOnchange('date_butoir','fdate(this)');
        $form->setOnchange('etablissement_type','VerifNum(this)');
        $form->setOnchange('etablissement_categorie','VerifNum(this)');
        $form->setOnchange('dossier_coordination_parent','VerifNum(this)');
        $form->setOnchange('date_cloture','fdate(this)');
    }
    /**
     * Methode setTaille
     */
    function setTaille(&$form, $maj) {
        $form->setTaille("dossier_coordination", 11);
        $form->setTaille("etablissement", 11);
        $form->setTaille("libelle", 30);
        $form->setTaille("dossier_coordination_type", 11);
        $form->setTaille("date_demande", 12);
        $form->setTaille("date_butoir", 12);
        $form->setTaille("dossier_autorisation_ads", 20);
        $form->setTaille("dossier_instruction_ads", 30);
        $form->setTaille("a_qualifier", 1);
        $form->setTaille("etablissement_type", 11);
        $form->setTaille("etablissement_categorie", 11);
        $form->setTaille("erp", 1);
        $form->setTaille("dossier_cloture", 1);
        $form->setTaille("contraintes_urba_om_html", 80);
        $form->setTaille("etablissement_locaux_sommeil", 1);
        $form->setTaille("references_cadastrales", 80);
        $form->setTaille("dossier_coordination_parent", 11);
        $form->setTaille("description", 80);
        $form->setTaille("dossier_instruction_secu", 1);
        $form->setTaille("dossier_instruction_acc", 1);
        $form->setTaille("autorite_police_encours", 1);
        $form->setTaille("date_cloture", 12);
        $form->setTaille("geolocalise", 1);
        $form->setTaille("interface_referentiel_ads", 1);
        $form->setTaille("enjeu_erp", 1);
        $form->setTaille("depot_de_piece", 1);
        $form->setTaille("enjeu_ads", 1);
        $form->setTaille("geom_point", 10);
        $form->setTaille("geom_emprise", 10);
        $form->setTaille("dossier_ads_force", 1);
    }

    /**
     * Methode setMax
     */
    function setMax(&$form, $maj) {
        $form->setMax("dossier_coordination", 11);
        $form->setMax("etablissement", 11);
        $form->setMax("libelle", 100);
        $form->setMax("dossier_coordination_type", 11);
        $form->setMax("date_demande", 12);
        $form->setMax("date_butoir", 12);
        $form->setMax("dossier_autorisation_ads", 20);
        $form->setMax("dossier_instruction_ads", 30);
        $form->setMax("a_qualifier", 1);
        $form->setMax("etablissement_type", 11);
        $form->setMax("etablissement_categorie", 11);
        $form->setMax("erp", 1);
        $form->setMax("dossier_cloture", 1);
        $form->setMax("contraintes_urba_om_html", 6);
        $form->setMax("etablissement_locaux_sommeil", 1);
        $form->setMax("references_cadastrales", 6);
        $form->setMax("dossier_coordination_parent", 11);
        $form->setMax("description", 6);
        $form->setMax("dossier_instruction_secu", 1);
        $form->setMax("dossier_instruction_acc", 1);
        $form->setMax("autorite_police_encours", 1);
        $form->setMax("date_cloture", 12);
        $form->setMax("geolocalise", 1);
        $form->setMax("interface_referentiel_ads", 1);
        $form->setMax("enjeu_erp", 1);
        $form->setMax("depot_de_piece", 1);
        $form->setMax("enjeu_ads", 1);
        $form->setMax("geom_point", -5);
        $form->setMax("geom_emprise", -5);
        $form->setMax("dossier_ads_force", 1);
    }


    function setLib(&$form, $maj) {
    //libelle des champs
        $form->setLib('dossier_coordination', __('dossier_coordination'));
        $form->setLib('etablissement', __('etablissement'));
        $form->setLib('libelle', __('libelle'));
        $form->setLib('dossier_coordination_type', __('dossier_coordination_type'));
        $form->setLib('date_demande', __('date_demande'));
        $form->setLib('date_butoir', __('date_butoir'));
        $form->setLib('dossier_autorisation_ads', __('dossier_autorisation_ads'));
        $form->setLib('dossier_instruction_ads', __('dossier_instruction_ads'));
        $form->setLib('a_qualifier', __('a_qualifier'));
        $form->setLib('etablissement_type', __('etablissement_type'));
        $form->setLib('etablissement_categorie', __('etablissement_categorie'));
        $form->setLib('erp', __('erp'));
        $form->setLib('dossier_cloture', __('dossier_cloture'));
        $form->setLib('contraintes_urba_om_html', __('contraintes_urba_om_html'));
        $form->setLib('etablissement_locaux_sommeil', __('etablissement_locaux_sommeil'));
        $form->setLib('references_cadastrales', __('references_cadastrales'));
        $form->setLib('dossier_coordination_parent', __('dossier_coordination_parent'));
        $form->setLib('description', __('description'));
        $form->setLib('dossier_instruction_secu', __('dossier_instruction_secu'));
        $form->setLib('dossier_instruction_acc', __('dossier_instruction_acc'));
        $form->setLib('autorite_police_encours', __('autorite_police_encours'));
        $form->setLib('date_cloture', __('date_cloture'));
        $form->setLib('geolocalise', __('geolocalise'));
        $form->setLib('interface_referentiel_ads', __('interface_referentiel_ads'));
        $form->setLib('enjeu_erp', __('enjeu_erp'));
        $form->setLib('depot_de_piece', __('depot_de_piece'));
        $form->setLib('enjeu_ads', __('enjeu_ads'));
        $form->setLib('geom_point', __('geom_point'));
        $form->setLib('geom_emprise', __('geom_emprise'));
        $form->setLib('dossier_ads_force', __('dossier_ads_force'));
    }
    /**
     *
     */
    function setSelect(&$form, $maj, &$dnu1 = null, $dnu2 = null) {

        // dossier_coordination_parent
        $this->init_select(
            $form, 
            $this->f->db,
            $maj,
            null,
            "dossier_coordination_parent",
            $this->get_var_sql_forminc__sql("dossier_coordination_parent"),
            $this->get_var_sql_forminc__sql("dossier_coordination_parent_by_id"),
            false
        );
        // dossier_coordination_type
        $this->init_select(
            $form, 
            $this->f->db,
            $maj,
            null,
            "dossier_coordination_type",
            $this->get_var_sql_forminc__sql("dossier_coordination_type"),
            $this->get_var_sql_forminc__sql("dossier_coordination_type_by_id"),
            true
        );
        // etablissement
        $this->init_select(
            $form, 
            $this->f->db,
            $maj,
            null,
            "etablissement",
            $this->get_var_sql_forminc__sql("etablissement"),
            $this->get_var_sql_forminc__sql("etablissement_by_id"),
            true
        );
        // etablissement_categorie
        $this->init_select(
            $form, 
            $this->f->db,
            $maj,
            null,
            "etablissement_categorie",
            $this->get_var_sql_forminc__sql("etablissement_categorie"),
            $this->get_var_sql_forminc__sql("etablissement_categorie_by_id"),
            true
        );
        // etablissement_type
        $this->init_select(
            $form, 
            $this->f->db,
            $maj,
            null,
            "etablissement_type",
            $this->get_var_sql_forminc__sql("etablissement_type"),
            $this->get_var_sql_forminc__sql("etablissement_type_by_id"),
            true
        );
        // geom_point
        if ($maj == 1 || $maj == 3) {
            $contenu = array();
            $contenu[0] = array("dossier_coordination", $this->getParameter("idx"), "0");
            $form->setSelect("geom_point", $contenu);
        }
        // geom_emprise
        if ($maj == 1 || $maj == 3) {
            $contenu = array();
            $contenu[0] = array("dossier_coordination", $this->getParameter("idx"), "1");
            $form->setSelect("geom_emprise", $contenu);
        }
    }


    //==================================
    // sous Formulaire
    //==================================
    

    function setValsousformulaire(&$form, $maj, $validation, $idxformulaire, $retourformulaire, $typeformulaire, &$dnu1 = null, $dnu2 = null) {
        $this->retourformulaire = $retourformulaire;
        if($validation == 0) {
            if($this->is_in_context_of_foreign_key('dossier_coordination', $this->retourformulaire))
                $form->setVal('dossier_coordination_parent', $idxformulaire);
            if($this->is_in_context_of_foreign_key('dossier_coordination_type', $this->retourformulaire))
                $form->setVal('dossier_coordination_type', $idxformulaire);
            if($this->is_in_context_of_foreign_key('etablissement', $this->retourformulaire))
                $form->setVal('etablissement', $idxformulaire);
            if($this->is_in_context_of_foreign_key('etablissement_categorie', $this->retourformulaire))
                $form->setVal('etablissement_categorie', $idxformulaire);
            if($this->is_in_context_of_foreign_key('etablissement_type', $this->retourformulaire))
                $form->setVal('etablissement_type', $idxformulaire);
        }// fin validation
        $this->set_form_default_values($form, $maj, $validation);
    }// fin setValsousformulaire

    //==================================
    // cle secondaire
    //==================================
    
    /**
     * Methode clesecondaire
     */
    function cleSecondaire($id, &$dnu1 = null, $val = array(), $dnu2 = null) {
        // On appelle la methode de la classe parent
        parent::cleSecondaire($id);
        // Verification de la cle secondaire : autorite_police
        $this->rechercheTable($this->f->db, "autorite_police", "dossier_coordination", $id);
        // Verification de la cle secondaire : courrier
        $this->rechercheTable($this->f->db, "courrier", "dossier_coordination", $id);
        // Verification de la cle secondaire : dossier_coordination
        $this->rechercheTable($this->f->db, "dossier_coordination", "dossier_coordination_parent", $id);
        // Verification de la cle secondaire : dossier_coordination_message
        $this->rechercheTable($this->f->db, "dossier_coordination_message", "dossier_coordination", $id);
        // Verification de la cle secondaire : dossier_coordination_parcelle
        $this->rechercheTable($this->f->db, "dossier_coordination_parcelle", "dossier_coordination", $id);
        // Verification de la cle secondaire : dossier_instruction
        $this->rechercheTable($this->f->db, "dossier_instruction", "dossier_coordination", $id);
        // Verification de la cle secondaire : etablissement
        $this->rechercheTable($this->f->db, "etablissement", "dossier_coordination_periodique", $id);
        // Verification de la cle secondaire : lien_contrainte_dossier_coordination
        $this->rechercheTable($this->f->db, "lien_contrainte_dossier_coordination", "dossier_coordination", $id);
        // Verification de la cle secondaire : lien_dossier_coordination_contact
        $this->rechercheTable($this->f->db, "lien_dossier_coordination_contact", "dossier_coordination", $id);
        // Verification de la cle secondaire : lien_dossier_coordination_etablissement_type
        $this->rechercheTable($this->f->db, "lien_dossier_coordination_etablissement_type", "dossier_coordination", $id);
        // Verification de la cle secondaire : piece
        $this->rechercheTable($this->f->db, "piece", "dossier_coordination", $id);
    }


}
