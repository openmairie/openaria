<?php
//$Id$ 
//gen openMairie le 07/07/2020 09:15

$DEBUG=0;
$serie=30;
$ent = __("application")." -> ".__("lien_courrier_contact");
if(!isset($premier)) $premier='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
// FROM 
$table = DB_PREFIXE."lien_courrier_contact
    LEFT JOIN ".DB_PREFIXE."contact 
        ON lien_courrier_contact.contact=contact.contact 
    LEFT JOIN ".DB_PREFIXE."courrier 
        ON lien_courrier_contact.courrier=courrier.courrier ";
// SELECT 
$champAffiche = array(
    'lien_courrier_contact.lien_courrier_contact as "'.__("lien_courrier_contact").'"',
    'courrier.etablissement as "'.__("courrier").'"',
    'contact.etablissement as "'.__("contact").'"',
    );
//
$champNonAffiche = array(
    );
//
$champRecherche = array(
    'lien_courrier_contact.lien_courrier_contact as "'.__("lien_courrier_contact").'"',
    'courrier.etablissement as "'.__("courrier").'"',
    'contact.etablissement as "'.__("contact").'"',
    );
$tri="ORDER BY courrier.etablissement ASC NULLS LAST";
$edition="lien_courrier_contact";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
$selection = "";
// Liste des clés étrangères avec leurs éventuelles surcharges
$foreign_keys_extended = array(
    "contact" => array("contact", "contact_institutionnel", "contact_contexte_dossier_coordination", ),
    "courrier" => array("courrier", "courrier_a_editer", "courrier_attente_signature", "courrier_attente_retour_ar", ),
);
// Filtre listing sous formulaire - contact
if (in_array($retourformulaire, $foreign_keys_extended["contact"])) {
    $selection = " WHERE (lien_courrier_contact.contact = ".intval($idxformulaire).") ";
}
// Filtre listing sous formulaire - courrier
if (in_array($retourformulaire, $foreign_keys_extended["courrier"])) {
    $selection = " WHERE (lien_courrier_contact.courrier = ".intval($idxformulaire).") ";
}

