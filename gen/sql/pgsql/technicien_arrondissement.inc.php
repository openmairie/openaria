<?php
//$Id$ 
//gen openMairie le 07/07/2020 09:15

$DEBUG=0;
$serie=30;
$ent = __("application")." -> ".__("technicien_arrondissement");
if(!isset($premier)) $premier='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
// FROM 
$table = DB_PREFIXE."technicien_arrondissement
    LEFT JOIN ".DB_PREFIXE."arrondissement 
        ON technicien_arrondissement.arrondissement=arrondissement.arrondissement 
    LEFT JOIN ".DB_PREFIXE."service 
        ON technicien_arrondissement.service=service.service 
    LEFT JOIN ".DB_PREFIXE."acteur 
        ON technicien_arrondissement.technicien=acteur.acteur ";
// SELECT 
$champAffiche = array(
    'technicien_arrondissement.technicien_arrondissement as "'.__("technicien_arrondissement").'"',
    'service.libelle as "'.__("service").'"',
    'acteur.nom_prenom as "'.__("technicien").'"',
    'arrondissement.libelle as "'.__("arrondissement").'"',
    );
//
$champNonAffiche = array(
    );
//
$champRecherche = array(
    'technicien_arrondissement.technicien_arrondissement as "'.__("technicien_arrondissement").'"',
    'service.libelle as "'.__("service").'"',
    'acteur.nom_prenom as "'.__("technicien").'"',
    'arrondissement.libelle as "'.__("arrondissement").'"',
    );
$tri="ORDER BY service.libelle ASC NULLS LAST";
$edition="technicien_arrondissement";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
$selection = "";
// Liste des clés étrangères avec leurs éventuelles surcharges
$foreign_keys_extended = array(
    "arrondissement" => array("arrondissement", ),
    "service" => array("service", ),
    "acteur" => array("acteur", ),
);
// Filtre listing sous formulaire - arrondissement
if (in_array($retourformulaire, $foreign_keys_extended["arrondissement"])) {
    $selection = " WHERE (technicien_arrondissement.arrondissement = ".intval($idxformulaire).") ";
}
// Filtre listing sous formulaire - service
if (in_array($retourformulaire, $foreign_keys_extended["service"])) {
    $selection = " WHERE (technicien_arrondissement.service = ".intval($idxformulaire).") ";
}
// Filtre listing sous formulaire - acteur
if (in_array($retourformulaire, $foreign_keys_extended["acteur"])) {
    $selection = " WHERE (technicien_arrondissement.technicien = ".intval($idxformulaire).") ";
}

