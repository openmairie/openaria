<?php
//$Id$ 
//gen openMairie le 07/07/2020 09:17

$DEBUG=0;
$serie=30;
$ent = __("application")." -> ".__("analyses");
if(!isset($premier)) $premier='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
// FROM 
$table = DB_PREFIXE."analyses
    LEFT JOIN ".DB_PREFIXE."analyses_type 
        ON analyses.analyses_type=analyses_type.analyses_type 
    LEFT JOIN ".DB_PREFIXE."proces_verbal 
        ON analyses.dernier_pv=proces_verbal.proces_verbal 
    LEFT JOIN ".DB_PREFIXE."dossier_instruction 
        ON analyses.dossier_instruction=dossier_instruction.dossier_instruction 
    LEFT JOIN ".DB_PREFIXE."modele_edition as modele_edition3 
        ON analyses.modele_edition_compte_rendu=modele_edition3.modele_edition 
    LEFT JOIN ".DB_PREFIXE."modele_edition as modele_edition4 
        ON analyses.modele_edition_proces_verbal=modele_edition4.modele_edition 
    LEFT JOIN ".DB_PREFIXE."modele_edition as modele_edition5 
        ON analyses.modele_edition_rapport=modele_edition5.modele_edition 
    LEFT JOIN ".DB_PREFIXE."reunion_avis 
        ON analyses.reunion_avis=reunion_avis.reunion_avis 
    LEFT JOIN ".DB_PREFIXE."service 
        ON analyses.service=service.service ";
// SELECT 
$champAffiche = array(
    'analyses.analyses as "'.__("analyses").'"',
    'service.libelle as "'.__("service").'"',
    'analyses.analyses_etat as "'.__("analyses_etat").'"',
    'analyses_type.libelle as "'.__("analyses_type").'"',
    'reunion_avis.libelle as "'.__("reunion_avis").'"',
    'analyses.avis_complement as "'.__("avis_complement").'"',
    'analyses.si_effectif_public as "'.__("si_effectif_public").'"',
    'analyses.si_effectif_personnel as "'.__("si_effectif_personnel").'"',
    'analyses.si_type_ssi as "'.__("si_type_ssi").'"',
    'analyses.si_type_alarme as "'.__("si_type_alarme").'"',
    "case analyses.si_conformite_l16 when 't' then 'Oui' else 'Non' end as \"".__("si_conformite_l16")."\"",
    "case analyses.si_alimentation_remplacement when 't' then 'Oui' else 'Non' end as \"".__("si_alimentation_remplacement")."\"",
    "case analyses.si_service_securite when 't' then 'Oui' else 'Non' end as \"".__("si_service_securite")."\"",
    'analyses.si_personnel_jour as "'.__("si_personnel_jour").'"',
    'analyses.si_personnel_nuit as "'.__("si_personnel_nuit").'"',
    'dossier_instruction.libelle as "'.__("dossier_instruction").'"',
    'modele_edition5.libelle as "'.__("modele_edition_rapport").'"',
    'modele_edition3.libelle as "'.__("modele_edition_compte_rendu").'"',
    'modele_edition4.libelle as "'.__("modele_edition_proces_verbal").'"',
    "case analyses.modifiee_sans_gen when 't' then 'Oui' else 'Non' end as \"".__("modifiee_sans_gen")."\"",
    'proces_verbal.numero as "'.__("dernier_pv").'"',
    'analyses.dec1 as "'.__("dec1").'"',
    'analyses.delai1 as "'.__("delai1").'"',
    'analyses.dec2 as "'.__("dec2").'"',
    'analyses.delai2 as "'.__("delai2").'"',
    );
//
$champNonAffiche = array(
    'analyses.objet as "'.__("objet").'"',
    'analyses.descriptif_etablissement_om_html as "'.__("descriptif_etablissement_om_html").'"',
    'analyses.reglementation_applicable_om_html as "'.__("reglementation_applicable_om_html").'"',
    'analyses.compte_rendu_om_html as "'.__("compte_rendu_om_html").'"',
    'analyses.document_presente_pendant_om_html as "'.__("document_presente_pendant_om_html").'"',
    'analyses.document_presente_apres_om_html as "'.__("document_presente_apres_om_html").'"',
    'analyses.observation_om_html as "'.__("observation_om_html").'"',
    );
//
$champRecherche = array(
    'analyses.analyses as "'.__("analyses").'"',
    'service.libelle as "'.__("service").'"',
    'analyses.analyses_etat as "'.__("analyses_etat").'"',
    'analyses_type.libelle as "'.__("analyses_type").'"',
    'reunion_avis.libelle as "'.__("reunion_avis").'"',
    'analyses.avis_complement as "'.__("avis_complement").'"',
    'analyses.si_effectif_public as "'.__("si_effectif_public").'"',
    'analyses.si_effectif_personnel as "'.__("si_effectif_personnel").'"',
    'analyses.si_type_ssi as "'.__("si_type_ssi").'"',
    'analyses.si_type_alarme as "'.__("si_type_alarme").'"',
    'analyses.si_personnel_jour as "'.__("si_personnel_jour").'"',
    'analyses.si_personnel_nuit as "'.__("si_personnel_nuit").'"',
    'dossier_instruction.libelle as "'.__("dossier_instruction").'"',
    'modele_edition5.libelle as "'.__("modele_edition_rapport").'"',
    'modele_edition3.libelle as "'.__("modele_edition_compte_rendu").'"',
    'modele_edition4.libelle as "'.__("modele_edition_proces_verbal").'"',
    'proces_verbal.numero as "'.__("dernier_pv").'"',
    'analyses.dec1 as "'.__("dec1").'"',
    'analyses.delai1 as "'.__("delai1").'"',
    'analyses.dec2 as "'.__("dec2").'"',
    'analyses.delai2 as "'.__("delai2").'"',
    );
$tri="ORDER BY service.libelle ASC NULLS LAST";
$edition="analyses";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
$selection = "";
// Liste des clés étrangères avec leurs éventuelles surcharges
$foreign_keys_extended = array(
    "analyses_type" => array("analyses_type", ),
    "proces_verbal" => array("proces_verbal", ),
    "dossier_instruction" => array("dossier_instruction", "dossier_instruction_mes_plans", "dossier_instruction_mes_visites", "dossier_instruction_tous_plans", "dossier_instruction_tous_visites", "dossier_instruction_a_qualifier", "dossier_instruction_a_affecter", ),
    "modele_edition" => array("modele_edition", ),
    "reunion_avis" => array("reunion_avis", ),
    "service" => array("service", ),
);
// Filtre listing sous formulaire - analyses_type
if (in_array($retourformulaire, $foreign_keys_extended["analyses_type"])) {
    $selection = " WHERE (analyses.analyses_type = ".intval($idxformulaire).") ";
}
// Filtre listing sous formulaire - proces_verbal
if (in_array($retourformulaire, $foreign_keys_extended["proces_verbal"])) {
    $selection = " WHERE (analyses.dernier_pv = ".intval($idxformulaire).") ";
}
// Filtre listing sous formulaire - dossier_instruction
if (in_array($retourformulaire, $foreign_keys_extended["dossier_instruction"])) {
    $selection = " WHERE (analyses.dossier_instruction = ".intval($idxformulaire).") ";
}
// Filtre listing sous formulaire - modele_edition
if (in_array($retourformulaire, $foreign_keys_extended["modele_edition"])) {
    $selection = " WHERE (analyses.modele_edition_compte_rendu = ".intval($idxformulaire)." OR analyses.modele_edition_proces_verbal = ".intval($idxformulaire)." OR analyses.modele_edition_rapport = ".intval($idxformulaire).") ";
}
// Filtre listing sous formulaire - reunion_avis
if (in_array($retourformulaire, $foreign_keys_extended["reunion_avis"])) {
    $selection = " WHERE (analyses.reunion_avis = ".intval($idxformulaire).") ";
}
// Filtre listing sous formulaire - service
if (in_array($retourformulaire, $foreign_keys_extended["service"])) {
    $selection = " WHERE (analyses.service = ".intval($idxformulaire).") ";
}

/**
 * Gestion SOUSFORMULAIRE => $sousformulaire
 */
$sousformulaire = array(
    'lien_essai_realise_analyses',
    'prescription',
);

