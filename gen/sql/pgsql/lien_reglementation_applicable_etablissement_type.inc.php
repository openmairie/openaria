<?php
//$Id$ 
//gen openMairie le 07/07/2020 09:15

$DEBUG=0;
$serie=30;
$ent = __("application")." -> ".__("lien_reglementation_applicable_etablissement_type");
if(!isset($premier)) $premier='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
// FROM 
$table = DB_PREFIXE."lien_reglementation_applicable_etablissement_type
    LEFT JOIN ".DB_PREFIXE."etablissement_type 
        ON lien_reglementation_applicable_etablissement_type.etablissement_type=etablissement_type.etablissement_type 
    LEFT JOIN ".DB_PREFIXE."reglementation_applicable 
        ON lien_reglementation_applicable_etablissement_type.reglementation_applicable=reglementation_applicable.reglementation_applicable ";
// SELECT 
$champAffiche = array(
    'lien_reglementation_applicable_etablissement_type.lien_reglementation_applicable_etablissement_type as "'.__("lien_reglementation_applicable_etablissement_type").'"',
    'reglementation_applicable.libelle as "'.__("reglementation_applicable").'"',
    'etablissement_type.libelle as "'.__("etablissement_type").'"',
    );
//
$champNonAffiche = array(
    );
//
$champRecherche = array(
    'lien_reglementation_applicable_etablissement_type.lien_reglementation_applicable_etablissement_type as "'.__("lien_reglementation_applicable_etablissement_type").'"',
    'reglementation_applicable.libelle as "'.__("reglementation_applicable").'"',
    'etablissement_type.libelle as "'.__("etablissement_type").'"',
    );
$tri="ORDER BY reglementation_applicable.libelle ASC NULLS LAST";
$edition="lien_reglementation_applicable_etablissement_type";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
$selection = "";
// Liste des clés étrangères avec leurs éventuelles surcharges
$foreign_keys_extended = array(
    "etablissement_type" => array("etablissement_type", ),
    "reglementation_applicable" => array("reglementation_applicable", ),
);
// Filtre listing sous formulaire - etablissement_type
if (in_array($retourformulaire, $foreign_keys_extended["etablissement_type"])) {
    $selection = " WHERE (lien_reglementation_applicable_etablissement_type.etablissement_type = ".intval($idxformulaire).") ";
}
// Filtre listing sous formulaire - reglementation_applicable
if (in_array($retourformulaire, $foreign_keys_extended["reglementation_applicable"])) {
    $selection = " WHERE (lien_reglementation_applicable_etablissement_type.reglementation_applicable = ".intval($idxformulaire).") ";
}

