<?php
//$Id$ 
//gen openMairie le 07/07/2020 09:15

$DEBUG=0;
$serie=30;
$ent = __("administration_parametrage")." -> ".__("contacts")." -> ".__("civilites");
if(!isset($premier)) $premier='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
// FROM 
$table = DB_PREFIXE."contact_civilite";
// SELECT 
$champAffiche = array(
    'contact_civilite.contact_civilite as "'.__("contact_civilite").'"',
    'contact_civilite.libelle as "'.__("libelle").'"',
    );
//
$champNonAffiche = array(
    );
//
$champRecherche = array(
    'contact_civilite.contact_civilite as "'.__("contact_civilite").'"',
    'contact_civilite.libelle as "'.__("libelle").'"',
    );
$tri="ORDER BY contact_civilite.libelle ASC NULLS LAST";
$edition="contact_civilite";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
$selection = "";

/**
 * Gestion SOUSFORMULAIRE => $sousformulaire
 */
$sousformulaire = array(
    //'contact',
    //'signataire',
);

