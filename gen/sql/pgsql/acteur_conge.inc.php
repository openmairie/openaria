<?php
//$Id$ 
//gen openMairie le 07/07/2020 09:15

$DEBUG=0;
$serie=30;
$ent = __("application")." -> ".__("acteur_conge");
if(!isset($premier)) $premier='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
// FROM 
$table = DB_PREFIXE."acteur_conge
    LEFT JOIN ".DB_PREFIXE."acteur 
        ON acteur_conge.acteur=acteur.acteur ";
// SELECT 
$champAffiche = array(
    'acteur_conge.acteur_conge as "'.__("acteur_conge").'"',
    'acteur.nom_prenom as "'.__("acteur").'"',
    'to_char(acteur_conge.date_debut ,\'DD/MM/YYYY\') as "'.__("date_debut").'"',
    'acteur_conge.heure_debut as "'.__("heure_debut").'"',
    'to_char(acteur_conge.date_fin ,\'DD/MM/YYYY\') as "'.__("date_fin").'"',
    'acteur_conge.heure_fin as "'.__("heure_fin").'"',
    );
//
$champNonAffiche = array(
    );
//
$champRecherche = array(
    'acteur_conge.acteur_conge as "'.__("acteur_conge").'"',
    'acteur.nom_prenom as "'.__("acteur").'"',
    'acteur_conge.heure_debut as "'.__("heure_debut").'"',
    'acteur_conge.heure_fin as "'.__("heure_fin").'"',
    );
$tri="ORDER BY acteur.nom_prenom ASC NULLS LAST";
$edition="acteur_conge";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
$selection = "";
// Liste des clés étrangères avec leurs éventuelles surcharges
$foreign_keys_extended = array(
    "acteur" => array("acteur", ),
);
// Filtre listing sous formulaire - acteur
if (in_array($retourformulaire, $foreign_keys_extended["acteur"])) {
    $selection = " WHERE (acteur_conge.acteur = ".intval($idxformulaire).") ";
}

