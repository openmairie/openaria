<?php
//$Id$ 
//gen openMairie le 21/07/2021 15:22

$DEBUG=0;
$serie=30;
$ent = __("application")." -> ".__("reunion");
if(!isset($premier)) $premier='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
// FROM 
$table = DB_PREFIXE."reunion
    LEFT JOIN ".DB_PREFIXE."reunion_type 
        ON reunion.reunion_type=reunion_type.reunion_type ";
// SELECT 
$champAffiche = array(
    'reunion.reunion as "'.__("reunion").'"',
    'reunion.code as "'.__("code").'"',
    'reunion_type.libelle as "'.__("reunion_type").'"',
    'reunion.libelle as "'.__("libelle").'"',
    'to_char(reunion.date_reunion ,\'DD/MM/YYYY\') as "'.__("date_reunion").'"',
    'reunion.lieu_adresse_ligne1 as "'.__("lieu_adresse_ligne1").'"',
    'reunion.lieu_adresse_ligne2 as "'.__("lieu_adresse_ligne2").'"',
    "case reunion.reunion_cloture when 't' then 'Oui' else 'Non' end as \"".__("reunion_cloture")."\"",
    );
//
$champNonAffiche = array(
    'reunion.heure_reunion as "'.__("heure_reunion").'"',
    'reunion.lieu_salle as "'.__("lieu_salle").'"',
    'reunion.listes_de_diffusion as "'.__("listes_de_diffusion").'"',
    'reunion.participants as "'.__("participants").'"',
    'reunion.numerotation_simple as "'.__("numerotation_simple").'"',
    'reunion.date_convocation as "'.__("date_convocation").'"',
    'reunion.date_cloture as "'.__("date_cloture").'"',
    'reunion.om_fichier_reunion_odj as "'.__("om_fichier_reunion_odj").'"',
    'reunion.om_final_reunion_odj as "'.__("om_final_reunion_odj").'"',
    'reunion.om_fichier_reunion_cr_global as "'.__("om_fichier_reunion_cr_global").'"',
    'reunion.om_final_reunion_cr_global as "'.__("om_final_reunion_cr_global").'"',
    'reunion.om_fichier_reunion_cr_global_signe as "'.__("om_fichier_reunion_cr_global_signe").'"',
    'reunion.om_fichier_reunion_cr_par_dossier_signe as "'.__("om_fichier_reunion_cr_par_dossier_signe").'"',
    'reunion.planifier_nouveau as "'.__("planifier_nouveau").'"',
    'reunion.numerotation_complexe as "'.__("numerotation_complexe").'"',
    'reunion.numerotation_mode as "'.__("numerotation_mode").'"',
    );
//
$champRecherche = array(
    'reunion.reunion as "'.__("reunion").'"',
    'reunion.code as "'.__("code").'"',
    'reunion_type.libelle as "'.__("reunion_type").'"',
    'reunion.libelle as "'.__("libelle").'"',
    'reunion.lieu_adresse_ligne1 as "'.__("lieu_adresse_ligne1").'"',
    'reunion.lieu_adresse_ligne2 as "'.__("lieu_adresse_ligne2").'"',
    );
$tri="ORDER BY reunion.libelle ASC NULLS LAST";
$edition="reunion";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
$selection = "";
// Liste des clés étrangères avec leurs éventuelles surcharges
$foreign_keys_extended = array(
    "reunion_type" => array("reunion_type", ),
);
// Filtre listing sous formulaire - reunion_type
if (in_array($retourformulaire, $foreign_keys_extended["reunion_type"])) {
    $selection = " WHERE (reunion.reunion_type = ".intval($idxformulaire).") ";
}

/**
 * Gestion SOUSFORMULAIRE => $sousformulaire
 */
$sousformulaire = array(
    'dossier_instruction_reunion',
    'lien_reunion_r_instance_r_i_membre',
);

