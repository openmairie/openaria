<?php
//$Id$ 
//gen openMairie le 07/07/2020 09:15

$DEBUG=0;
$serie=30;
$ent = __("application")." -> ".__("lien_etablissement_e_type");
if(!isset($premier)) $premier='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
// FROM 
$table = DB_PREFIXE."lien_etablissement_e_type
    LEFT JOIN ".DB_PREFIXE."etablissement 
        ON lien_etablissement_e_type.etablissement=etablissement.etablissement 
    LEFT JOIN ".DB_PREFIXE."etablissement_type 
        ON lien_etablissement_e_type.etablissement_type=etablissement_type.etablissement_type ";
// SELECT 
$champAffiche = array(
    'lien_etablissement_e_type.lien_etablissement_e_type as "'.__("lien_etablissement_e_type").'"',
    'etablissement.libelle as "'.__("etablissement").'"',
    'etablissement_type.libelle as "'.__("etablissement_type").'"',
    );
//
$champNonAffiche = array(
    );
//
$champRecherche = array(
    'lien_etablissement_e_type.lien_etablissement_e_type as "'.__("lien_etablissement_e_type").'"',
    'etablissement.libelle as "'.__("etablissement").'"',
    'etablissement_type.libelle as "'.__("etablissement_type").'"',
    );
$tri="ORDER BY etablissement.libelle ASC NULLS LAST";
$edition="lien_etablissement_e_type";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
$selection = "";
// Liste des clés étrangères avec leurs éventuelles surcharges
$foreign_keys_extended = array(
    "etablissement" => array("etablissement", "etablissement_referentiel_erp", "etablissement_tous", ),
    "etablissement_type" => array("etablissement_type", ),
);
// Filtre listing sous formulaire - etablissement
if (in_array($retourformulaire, $foreign_keys_extended["etablissement"])) {
    $selection = " WHERE (lien_etablissement_e_type.etablissement = ".intval($idxformulaire).") ";
}
// Filtre listing sous formulaire - etablissement_type
if (in_array($retourformulaire, $foreign_keys_extended["etablissement_type"])) {
    $selection = " WHERE (lien_etablissement_e_type.etablissement_type = ".intval($idxformulaire).") ";
}

