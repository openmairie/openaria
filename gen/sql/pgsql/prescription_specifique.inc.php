<?php
//$Id$ 
//gen openMairie le 07/07/2020 09:15

$DEBUG=0;
$serie=30;
$ent = __("application")." -> ".__("prescription_specifique");
$om_validite = true;
if(!isset($premier)) $premier='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
// FROM 
$table = DB_PREFIXE."prescription_specifique
    LEFT JOIN ".DB_PREFIXE."prescription_reglementaire 
        ON prescription_specifique.prescription_reglementaire=prescription_reglementaire.prescription_reglementaire 
    LEFT JOIN ".DB_PREFIXE."service 
        ON prescription_specifique.service=service.service ";
// SELECT 
$champAffiche = array(
    'prescription_specifique.prescription_specifique as "'.__("prescription_specifique").'"',
    'service.libelle as "'.__("service").'"',
    'prescription_specifique.libelle as "'.__("libelle").'"',
    'prescription_reglementaire.libelle as "'.__("prescription_reglementaire").'"',
    );
// Spécificité des dates de validité
$displayed_fields_validite = array(
    'to_char(prescription_specifique.om_validite_debut ,\'DD/MM/YYYY\') as "'.__("om_validite_debut").'"',
    'to_char(prescription_specifique.om_validite_fin ,\'DD/MM/YYYY\') as "'.__("om_validite_fin").'"',
);
// On affiche les champs de date de validité uniquement lorsque le paramètre
// d'affichage des éléments expirés est activé
if (isset($_GET['valide']) && $_GET['valide'] === 'false') {
    $champAffiche = array_merge($champAffiche, $displayed_fields_validite);
}

//
$champNonAffiche = array(
    'prescription_specifique.description_ps_om_html as "'.__("description_ps_om_html").'"',
    'prescription_specifique.om_validite_debut as "'.__("om_validite_debut").'"',
    'prescription_specifique.om_validite_fin as "'.__("om_validite_fin").'"',
    );
//
$champRecherche = array(
    'prescription_specifique.prescription_specifique as "'.__("prescription_specifique").'"',
    'service.libelle as "'.__("service").'"',
    'prescription_specifique.libelle as "'.__("libelle").'"',
    'prescription_reglementaire.libelle as "'.__("prescription_reglementaire").'"',
    );
$tri="ORDER BY prescription_specifique.libelle ASC NULLS LAST";
$edition="prescription_specifique";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
$selection = " WHERE ((prescription_specifique.om_validite_debut IS NULL AND (prescription_specifique.om_validite_fin IS NULL OR prescription_specifique.om_validite_fin > CURRENT_DATE)) OR (prescription_specifique.om_validite_debut <= CURRENT_DATE AND (prescription_specifique.om_validite_fin IS NULL OR prescription_specifique.om_validite_fin > CURRENT_DATE)))";
$where_om_validite = " WHERE ((prescription_specifique.om_validite_debut IS NULL AND (prescription_specifique.om_validite_fin IS NULL OR prescription_specifique.om_validite_fin > CURRENT_DATE)) OR (prescription_specifique.om_validite_debut <= CURRENT_DATE AND (prescription_specifique.om_validite_fin IS NULL OR prescription_specifique.om_validite_fin > CURRENT_DATE)))";
// Liste des clés étrangères avec leurs éventuelles surcharges
$foreign_keys_extended = array(
    "prescription_reglementaire" => array("prescription_reglementaire", ),
    "service" => array("service", ),
);
// Filtre listing sous formulaire - prescription_reglementaire
if (in_array($retourformulaire, $foreign_keys_extended["prescription_reglementaire"])) {
    $selection = " WHERE (prescription_specifique.prescription_reglementaire = ".intval($idxformulaire).")  AND ((prescription_specifique.om_validite_debut IS NULL AND (prescription_specifique.om_validite_fin IS NULL OR prescription_specifique.om_validite_fin > CURRENT_DATE)) OR (prescription_specifique.om_validite_debut <= CURRENT_DATE AND (prescription_specifique.om_validite_fin IS NULL OR prescription_specifique.om_validite_fin > CURRENT_DATE)))";
$where_om_validite = " AND ((prescription_specifique.om_validite_debut IS NULL AND (prescription_specifique.om_validite_fin IS NULL OR prescription_specifique.om_validite_fin > CURRENT_DATE)) OR (prescription_specifique.om_validite_debut <= CURRENT_DATE AND (prescription_specifique.om_validite_fin IS NULL OR prescription_specifique.om_validite_fin > CURRENT_DATE)))";
}
// Filtre listing sous formulaire - service
if (in_array($retourformulaire, $foreign_keys_extended["service"])) {
    $selection = " WHERE (prescription_specifique.service = ".intval($idxformulaire).")  AND ((prescription_specifique.om_validite_debut IS NULL AND (prescription_specifique.om_validite_fin IS NULL OR prescription_specifique.om_validite_fin > CURRENT_DATE)) OR (prescription_specifique.om_validite_debut <= CURRENT_DATE AND (prescription_specifique.om_validite_fin IS NULL OR prescription_specifique.om_validite_fin > CURRENT_DATE)))";
$where_om_validite = " AND ((prescription_specifique.om_validite_debut IS NULL AND (prescription_specifique.om_validite_fin IS NULL OR prescription_specifique.om_validite_fin > CURRENT_DATE)) OR (prescription_specifique.om_validite_debut <= CURRENT_DATE AND (prescription_specifique.om_validite_fin IS NULL OR prescription_specifique.om_validite_fin > CURRENT_DATE)))";
}
// Gestion OMValidité - Suppression du filtre si paramètre
if (isset($_GET["valide"]) and $_GET["valide"] == "false") {
    if (!isset($where_om_validite)
        or (isset($where_om_validite) and $where_om_validite == "")) {
        if (trim($selection) != "") {
            $selection = "";
        }
    } else {
        $selection = trim(str_replace($where_om_validite, "", $selection));
    }
}

