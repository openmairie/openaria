<?php
//$Id$ 
//gen openMairie le 07/07/2020 09:15

$DEBUG=0;
$serie=30;
$ent = __("application")." -> ".__("reunion_type_reunion_avis");
if(!isset($premier)) $premier='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
// FROM 
$table = DB_PREFIXE."reunion_type_reunion_avis
    LEFT JOIN ".DB_PREFIXE."reunion_avis 
        ON reunion_type_reunion_avis.reunion_avis=reunion_avis.reunion_avis 
    LEFT JOIN ".DB_PREFIXE."reunion_type 
        ON reunion_type_reunion_avis.reunion_type=reunion_type.reunion_type ";
// SELECT 
$champAffiche = array(
    'reunion_type_reunion_avis.reunion_type_reunion_avis as "'.__("reunion_type_reunion_avis").'"',
    'reunion_type.libelle as "'.__("reunion_type").'"',
    'reunion_avis.libelle as "'.__("reunion_avis").'"',
    );
//
$champNonAffiche = array(
    );
//
$champRecherche = array(
    'reunion_type_reunion_avis.reunion_type_reunion_avis as "'.__("reunion_type_reunion_avis").'"',
    'reunion_type.libelle as "'.__("reunion_type").'"',
    'reunion_avis.libelle as "'.__("reunion_avis").'"',
    );
$tri="ORDER BY reunion_type.libelle ASC NULLS LAST";
$edition="reunion_type_reunion_avis";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
$selection = "";
// Liste des clés étrangères avec leurs éventuelles surcharges
$foreign_keys_extended = array(
    "reunion_avis" => array("reunion_avis", ),
    "reunion_type" => array("reunion_type", ),
);
// Filtre listing sous formulaire - reunion_avis
if (in_array($retourformulaire, $foreign_keys_extended["reunion_avis"])) {
    $selection = " WHERE (reunion_type_reunion_avis.reunion_avis = ".intval($idxformulaire).") ";
}
// Filtre listing sous formulaire - reunion_type
if (in_array($retourformulaire, $foreign_keys_extended["reunion_type"])) {
    $selection = " WHERE (reunion_type_reunion_avis.reunion_type = ".intval($idxformulaire).") ";
}

