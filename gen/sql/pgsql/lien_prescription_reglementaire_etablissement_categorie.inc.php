<?php
//$Id$ 
//gen openMairie le 07/07/2020 09:15

$DEBUG=0;
$serie=30;
$ent = __("application")." -> ".__("lien_prescription_reglementaire_etablissement_categorie");
if(!isset($premier)) $premier='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
// FROM 
$table = DB_PREFIXE."lien_prescription_reglementaire_etablissement_categorie
    LEFT JOIN ".DB_PREFIXE."etablissement_categorie 
        ON lien_prescription_reglementaire_etablissement_categorie.etablissement_categorie=etablissement_categorie.etablissement_categorie 
    LEFT JOIN ".DB_PREFIXE."prescription_reglementaire 
        ON lien_prescription_reglementaire_etablissement_categorie.prescription_reglementaire=prescription_reglementaire.prescription_reglementaire ";
// SELECT 
$champAffiche = array(
    'lien_prescription_reglementaire_etablissement_categorie.lien_prescription_reglementaire_etablissement_categorie as "'.__("lien_prescription_reglementaire_etablissement_categorie").'"',
    'prescription_reglementaire.libelle as "'.__("prescription_reglementaire").'"',
    'etablissement_categorie.libelle as "'.__("etablissement_categorie").'"',
    );
//
$champNonAffiche = array(
    );
//
$champRecherche = array(
    'lien_prescription_reglementaire_etablissement_categorie.lien_prescription_reglementaire_etablissement_categorie as "'.__("lien_prescription_reglementaire_etablissement_categorie").'"',
    'prescription_reglementaire.libelle as "'.__("prescription_reglementaire").'"',
    'etablissement_categorie.libelle as "'.__("etablissement_categorie").'"',
    );
$tri="ORDER BY prescription_reglementaire.libelle ASC NULLS LAST";
$edition="lien_prescription_reglementaire_etablissement_categorie";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
$selection = "";
// Liste des clés étrangères avec leurs éventuelles surcharges
$foreign_keys_extended = array(
    "etablissement_categorie" => array("etablissement_categorie", ),
    "prescription_reglementaire" => array("prescription_reglementaire", ),
);
// Filtre listing sous formulaire - etablissement_categorie
if (in_array($retourformulaire, $foreign_keys_extended["etablissement_categorie"])) {
    $selection = " WHERE (lien_prescription_reglementaire_etablissement_categorie.etablissement_categorie = ".intval($idxformulaire).") ";
}
// Filtre listing sous formulaire - prescription_reglementaire
if (in_array($retourformulaire, $foreign_keys_extended["prescription_reglementaire"])) {
    $selection = " WHERE (lien_prescription_reglementaire_etablissement_categorie.prescription_reglementaire = ".intval($idxformulaire).") ";
}

