<?php
//$Id$ 
//gen openMairie le 07/07/2020 09:15

$DEBUG=0;
$serie=30;
$ent = __("application")." -> ".__("piece");
if(!isset($premier)) $premier='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
// FROM 
$table = DB_PREFIXE."piece
    LEFT JOIN ".DB_PREFIXE."dossier_coordination 
        ON piece.dossier_coordination=dossier_coordination.dossier_coordination 
    LEFT JOIN ".DB_PREFIXE."dossier_instruction 
        ON piece.dossier_instruction=dossier_instruction.dossier_instruction 
    LEFT JOIN ".DB_PREFIXE."etablissement 
        ON piece.etablissement=etablissement.etablissement 
    LEFT JOIN ".DB_PREFIXE."piece_statut 
        ON piece.piece_statut=piece_statut.piece_statut 
    LEFT JOIN ".DB_PREFIXE."piece_type 
        ON piece.piece_type=piece_type.piece_type 
    LEFT JOIN ".DB_PREFIXE."service 
        ON piece.service=service.service ";
// SELECT 
$champAffiche = array(
    'piece.piece as "'.__("piece").'"',
    'etablissement.libelle as "'.__("etablissement").'"',
    'dossier_coordination.libelle as "'.__("dossier_coordination").'"',
    'piece.nom as "'.__("nom").'"',
    'to_char(piece.om_date_creation ,\'DD/MM/YYYY\') as "'.__("om_date_creation").'"',
    'dossier_instruction.libelle as "'.__("dossier_instruction").'"',
    'to_char(piece.date_butoir ,\'DD/MM/YYYY\') as "'.__("date_butoir").'"',
    );
//
$champNonAffiche = array(
    'piece.piece_type as "'.__("piece_type").'"',
    'piece.uid as "'.__("uid").'"',
    'piece.date_reception as "'.__("date_reception").'"',
    'piece.date_emission as "'.__("date_emission").'"',
    'piece.piece_statut as "'.__("piece_statut").'"',
    'piece.suivi as "'.__("suivi").'"',
    'piece.commentaire_suivi as "'.__("commentaire_suivi").'"',
    'piece.lu as "'.__("lu").'"',
    'piece.choix_lien as "'.__("choix_lien").'"',
    'piece.service as "'.__("service").'"',
    );
//
$champRecherche = array(
    'piece.piece as "'.__("piece").'"',
    'etablissement.libelle as "'.__("etablissement").'"',
    'dossier_coordination.libelle as "'.__("dossier_coordination").'"',
    'piece.nom as "'.__("nom").'"',
    'dossier_instruction.libelle as "'.__("dossier_instruction").'"',
    );
$tri="ORDER BY etablissement.libelle ASC NULLS LAST";
$edition="piece";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
$selection = "";
// Liste des clés étrangères avec leurs éventuelles surcharges
$foreign_keys_extended = array(
    "dossier_coordination" => array("dossier_coordination", "dossier_coordination_nouveau", "dossier_coordination_a_qualifier", "dossier_coordination_a_cloturer", ),
    "dossier_instruction" => array("dossier_instruction", "dossier_instruction_mes_plans", "dossier_instruction_mes_visites", "dossier_instruction_tous_plans", "dossier_instruction_tous_visites", "dossier_instruction_a_qualifier", "dossier_instruction_a_affecter", ),
    "etablissement" => array("etablissement", "etablissement_referentiel_erp", "etablissement_tous", ),
    "piece_statut" => array("piece_statut", ),
    "piece_type" => array("piece_type", ),
    "service" => array("service", ),
);
// Filtre listing sous formulaire - dossier_coordination
if (in_array($retourformulaire, $foreign_keys_extended["dossier_coordination"])) {
    $selection = " WHERE (piece.dossier_coordination = ".intval($idxformulaire).") ";
}
// Filtre listing sous formulaire - dossier_instruction
if (in_array($retourformulaire, $foreign_keys_extended["dossier_instruction"])) {
    $selection = " WHERE (piece.dossier_instruction = ".intval($idxformulaire).") ";
}
// Filtre listing sous formulaire - etablissement
if (in_array($retourformulaire, $foreign_keys_extended["etablissement"])) {
    $selection = " WHERE (piece.etablissement = ".intval($idxformulaire).") ";
}
// Filtre listing sous formulaire - piece_statut
if (in_array($retourformulaire, $foreign_keys_extended["piece_statut"])) {
    $selection = " WHERE (piece.piece_statut = ".intval($idxformulaire).") ";
}
// Filtre listing sous formulaire - piece_type
if (in_array($retourformulaire, $foreign_keys_extended["piece_type"])) {
    $selection = " WHERE (piece.piece_type = ".intval($idxformulaire).") ";
}
// Filtre listing sous formulaire - service
if (in_array($retourformulaire, $foreign_keys_extended["service"])) {
    $selection = " WHERE (piece.service = ".intval($idxformulaire).") ";
}

