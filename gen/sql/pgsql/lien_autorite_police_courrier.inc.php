<?php
//$Id$ 
//gen openMairie le 07/07/2020 09:15

$DEBUG=0;
$serie=30;
$ent = __("application")." -> ".__("lien_autorite_police_courrier");
if(!isset($premier)) $premier='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
// FROM 
$table = DB_PREFIXE."lien_autorite_police_courrier
    LEFT JOIN ".DB_PREFIXE."autorite_police 
        ON lien_autorite_police_courrier.autorite_police=autorite_police.autorite_police 
    LEFT JOIN ".DB_PREFIXE."courrier 
        ON lien_autorite_police_courrier.courrier=courrier.courrier ";
// SELECT 
$champAffiche = array(
    'lien_autorite_police_courrier.lien_autorite_police_courrier as "'.__("lien_autorite_police_courrier").'"',
    'autorite_police.autorite_police_decision as "'.__("autorite_police").'"',
    'courrier.etablissement as "'.__("courrier").'"',
    );
//
$champNonAffiche = array(
    );
//
$champRecherche = array(
    'lien_autorite_police_courrier.lien_autorite_police_courrier as "'.__("lien_autorite_police_courrier").'"',
    'autorite_police.autorite_police_decision as "'.__("autorite_police").'"',
    'courrier.etablissement as "'.__("courrier").'"',
    );
$tri="ORDER BY autorite_police.autorite_police_decision ASC NULLS LAST";
$edition="lien_autorite_police_courrier";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
$selection = "";
// Liste des clés étrangères avec leurs éventuelles surcharges
$foreign_keys_extended = array(
    "autorite_police" => array("autorite_police", ),
    "courrier" => array("courrier", "courrier_a_editer", "courrier_attente_signature", "courrier_attente_retour_ar", ),
);
// Filtre listing sous formulaire - autorite_police
if (in_array($retourformulaire, $foreign_keys_extended["autorite_police"])) {
    $selection = " WHERE (lien_autorite_police_courrier.autorite_police = ".intval($idxformulaire).") ";
}
// Filtre listing sous formulaire - courrier
if (in_array($retourformulaire, $foreign_keys_extended["courrier"])) {
    $selection = " WHERE (lien_autorite_police_courrier.courrier = ".intval($idxformulaire).") ";
}

