<?php
//$Id$ 
//gen openMairie le 07/07/2020 09:15

$DEBUG=0;
$serie=30;
$ent = __("administration_parametrage")." -> ".__("etablissements")." -> ".__("periodicites de visites");
if(!isset($premier)) $premier='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
// FROM 
$table = DB_PREFIXE."periodicite_visites
    LEFT JOIN ".DB_PREFIXE."etablissement_categorie 
        ON periodicite_visites.etablissement_categorie=etablissement_categorie.etablissement_categorie 
    LEFT JOIN ".DB_PREFIXE."etablissement_type 
        ON periodicite_visites.etablissement_type=etablissement_type.etablissement_type ";
// SELECT 
$champAffiche = array(
    'periodicite_visites.periodicite_visites as "'.__("periodicite_visites").'"',
    'periodicite_visites.periodicite as "'.__("periodicite").'"',
    'etablissement_type.libelle as "'.__("etablissement_type").'"',
    'etablissement_categorie.libelle as "'.__("etablissement_categorie").'"',
    "case periodicite_visites.avec_locaux_sommeil when 't' then 'Oui' else 'Non' end as \"".__("avec_locaux_sommeil")."\"",
    "case periodicite_visites.sans_locaux_sommeil when 't' then 'Oui' else 'Non' end as \"".__("sans_locaux_sommeil")."\"",
    );
//
$champNonAffiche = array(
    'periodicite_visites.commentaire as "'.__("commentaire").'"',
    );
//
$champRecherche = array(
    'periodicite_visites.periodicite_visites as "'.__("periodicite_visites").'"',
    'periodicite_visites.periodicite as "'.__("periodicite").'"',
    'etablissement_type.libelle as "'.__("etablissement_type").'"',
    'etablissement_categorie.libelle as "'.__("etablissement_categorie").'"',
    );
$tri="ORDER BY periodicite_visites.periodicite ASC NULLS LAST";
$edition="periodicite_visites";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
$selection = "";
// Liste des clés étrangères avec leurs éventuelles surcharges
$foreign_keys_extended = array(
    "etablissement_categorie" => array("etablissement_categorie", ),
    "etablissement_type" => array("etablissement_type", ),
);
// Filtre listing sous formulaire - etablissement_categorie
if (in_array($retourformulaire, $foreign_keys_extended["etablissement_categorie"])) {
    $selection = " WHERE (periodicite_visites.etablissement_categorie = ".intval($idxformulaire).") ";
}
// Filtre listing sous formulaire - etablissement_type
if (in_array($retourformulaire, $foreign_keys_extended["etablissement_type"])) {
    $selection = " WHERE (periodicite_visites.etablissement_type = ".intval($idxformulaire).") ";
}

