<?php
//$Id$ 
//gen openMairie le 07/07/2020 09:15

$DEBUG=0;
$serie=30;
$ent = __("administration_parametrage")." -> ".__("metiers")." -> ".__("autorites competentes");
if(!isset($premier)) $premier='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
// FROM 
$table = DB_PREFIXE."autorite_competente
    LEFT JOIN ".DB_PREFIXE."service 
        ON autorite_competente.service=service.service ";
// SELECT 
$champAffiche = array(
    'autorite_competente.autorite_competente as "'.__("autorite_competente").'"',
    'autorite_competente.code as "'.__("code").'"',
    'autorite_competente.libelle as "'.__("libelle").'"',
    'service.libelle as "'.__("service").'"',
    );
//
$champNonAffiche = array(
    );
//
$champRecherche = array(
    'autorite_competente.autorite_competente as "'.__("autorite_competente").'"',
    'autorite_competente.code as "'.__("code").'"',
    'autorite_competente.libelle as "'.__("libelle").'"',
    'service.libelle as "'.__("service").'"',
    );
$tri="ORDER BY autorite_competente.libelle ASC NULLS LAST";
$edition="autorite_competente";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
$selection = "";
// Liste des clés étrangères avec leurs éventuelles surcharges
$foreign_keys_extended = array(
    "service" => array("service", ),
);
// Filtre listing sous formulaire - service
if (in_array($retourformulaire, $foreign_keys_extended["service"])) {
    $selection = " WHERE (autorite_competente.service = ".intval($idxformulaire).") ";
}

/**
 * Gestion SOUSFORMULAIRE => $sousformulaire
 */
$sousformulaire = array(
    //'dossier_instruction',
    //'etablissement',
);

