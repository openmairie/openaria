<?php
//$Id$ 
//gen openMairie le 07/07/2020 09:15

$DEBUG=0;
$serie=30;
$ent = __("application")." -> ".__("acteur_plage_visite");
if(!isset($premier)) $premier='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
// FROM 
$table = DB_PREFIXE."acteur_plage_visite
    LEFT JOIN ".DB_PREFIXE."acteur 
        ON acteur_plage_visite.acteur=acteur.acteur ";
// SELECT 
$champAffiche = array(
    'acteur_plage_visite.acteur_plage_visite as "'.__("acteur_plage_visite").'"',
    'acteur.nom_prenom as "'.__("acteur").'"',
    "case acteur_plage_visite.lundi_matin when 't' then 'Oui' else 'Non' end as \"".__("lundi_matin")."\"",
    "case acteur_plage_visite.lundi_apresmidi when 't' then 'Oui' else 'Non' end as \"".__("lundi_apresmidi")."\"",
    "case acteur_plage_visite.mardi_matin when 't' then 'Oui' else 'Non' end as \"".__("mardi_matin")."\"",
    "case acteur_plage_visite.mardi_apresmidi when 't' then 'Oui' else 'Non' end as \"".__("mardi_apresmidi")."\"",
    "case acteur_plage_visite.mercredi_matin when 't' then 'Oui' else 'Non' end as \"".__("mercredi_matin")."\"",
    "case acteur_plage_visite.mercredi_apresmidi when 't' then 'Oui' else 'Non' end as \"".__("mercredi_apresmidi")."\"",
    "case acteur_plage_visite.jeudi_matin when 't' then 'Oui' else 'Non' end as \"".__("jeudi_matin")."\"",
    "case acteur_plage_visite.jeudi_apresmidi when 't' then 'Oui' else 'Non' end as \"".__("jeudi_apresmidi")."\"",
    "case acteur_plage_visite.vendredi_matin when 't' then 'Oui' else 'Non' end as \"".__("vendredi_matin")."\"",
    "case acteur_plage_visite.vendredi_apresmidi when 't' then 'Oui' else 'Non' end as \"".__("vendredi_apresmidi")."\"",
    );
//
$champNonAffiche = array(
    );
//
$champRecherche = array(
    'acteur_plage_visite.acteur_plage_visite as "'.__("acteur_plage_visite").'"',
    'acteur.nom_prenom as "'.__("acteur").'"',
    );
$tri="ORDER BY acteur.nom_prenom ASC NULLS LAST";
$edition="acteur_plage_visite";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
$selection = "";
// Liste des clés étrangères avec leurs éventuelles surcharges
$foreign_keys_extended = array(
    "acteur" => array("acteur", ),
);
// Filtre listing sous formulaire - acteur
if (in_array($retourformulaire, $foreign_keys_extended["acteur"])) {
    $selection = " WHERE (acteur_plage_visite.acteur = ".intval($idxformulaire).") ";
}

