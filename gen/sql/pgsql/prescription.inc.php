<?php
//$Id$ 
//gen openMairie le 07/07/2020 09:15

$DEBUG=0;
$serie=30;
$ent = __("application")." -> ".__("prescription");
if(!isset($premier)) $premier='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
// FROM 
$table = DB_PREFIXE."prescription
    LEFT JOIN ".DB_PREFIXE."analyses 
        ON prescription.analyses=analyses.analyses 
    LEFT JOIN ".DB_PREFIXE."prescription_reglementaire 
        ON prescription.prescription_reglementaire=prescription_reglementaire.prescription_reglementaire ";
// SELECT 
$champAffiche = array(
    'prescription.prescription as "'.__("prescription").'"',
    'analyses.service as "'.__("analyses").'"',
    'prescription.ordre as "'.__("ordre").'"',
    'prescription_reglementaire.libelle as "'.__("prescription_reglementaire").'"',
    "case prescription.pr_defavorable when 't' then 'Oui' else 'Non' end as \"".__("pr_defavorable")."\"",
    );
//
$champNonAffiche = array(
    'prescription.pr_description_om_html as "'.__("pr_description_om_html").'"',
    'prescription.ps_description_om_html as "'.__("ps_description_om_html").'"',
    );
//
$champRecherche = array(
    'prescription.prescription as "'.__("prescription").'"',
    'analyses.service as "'.__("analyses").'"',
    'prescription.ordre as "'.__("ordre").'"',
    'prescription_reglementaire.libelle as "'.__("prescription_reglementaire").'"',
    );
$tri="ORDER BY analyses.service ASC NULLS LAST";
$edition="prescription";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
$selection = "";
// Liste des clés étrangères avec leurs éventuelles surcharges
$foreign_keys_extended = array(
    "analyses" => array("analyses", ),
    "prescription_reglementaire" => array("prescription_reglementaire", ),
);
// Filtre listing sous formulaire - analyses
if (in_array($retourformulaire, $foreign_keys_extended["analyses"])) {
    $selection = " WHERE (prescription.analyses = ".intval($idxformulaire).") ";
}
// Filtre listing sous formulaire - prescription_reglementaire
if (in_array($retourformulaire, $foreign_keys_extended["prescription_reglementaire"])) {
    $selection = " WHERE (prescription.prescription_reglementaire = ".intval($idxformulaire).") ";
}

