<?php
//$Id$ 
//gen openMairie le 07/07/2020 09:15

$DEBUG=0;
$serie=30;
$ent = __("administration_parametrage")." -> ".__("analyses")." -> ".__("reglementations applicables");
$om_validite = true;
if(!isset($premier)) $premier='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
// FROM 
$table = DB_PREFIXE."reglementation_applicable
    LEFT JOIN ".DB_PREFIXE."service 
        ON reglementation_applicable.service=service.service ";
// SELECT 
$champAffiche = array(
    'reglementation_applicable.reglementation_applicable as "'.__("reglementation_applicable").'"',
    'service.libelle as "'.__("service").'"',
    'reglementation_applicable.libelle as "'.__("libelle").'"',
    'reglementation_applicable.annee_debut_application as "'.__("annee_debut_application").'"',
    'reglementation_applicable.annee_fin_application as "'.__("annee_fin_application").'"',
    );
// Spécificité des dates de validité
$displayed_fields_validite = array(
    'to_char(reglementation_applicable.om_validite_debut ,\'DD/MM/YYYY\') as "'.__("om_validite_debut").'"',
    'to_char(reglementation_applicable.om_validite_fin ,\'DD/MM/YYYY\') as "'.__("om_validite_fin").'"',
);
// On affiche les champs de date de validité uniquement lorsque le paramètre
// d'affichage des éléments expirés est activé
if (isset($_GET['valide']) && $_GET['valide'] === 'false') {
    $champAffiche = array_merge($champAffiche, $displayed_fields_validite);
}

//
$champNonAffiche = array(
    'reglementation_applicable.description as "'.__("description").'"',
    'reglementation_applicable.om_validite_debut as "'.__("om_validite_debut").'"',
    'reglementation_applicable.om_validite_fin as "'.__("om_validite_fin").'"',
    );
//
$champRecherche = array(
    'reglementation_applicable.reglementation_applicable as "'.__("reglementation_applicable").'"',
    'service.libelle as "'.__("service").'"',
    'reglementation_applicable.libelle as "'.__("libelle").'"',
    'reglementation_applicable.annee_debut_application as "'.__("annee_debut_application").'"',
    'reglementation_applicable.annee_fin_application as "'.__("annee_fin_application").'"',
    );
$tri="ORDER BY reglementation_applicable.libelle ASC NULLS LAST";
$edition="reglementation_applicable";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
$selection = " WHERE ((reglementation_applicable.om_validite_debut IS NULL AND (reglementation_applicable.om_validite_fin IS NULL OR reglementation_applicable.om_validite_fin > CURRENT_DATE)) OR (reglementation_applicable.om_validite_debut <= CURRENT_DATE AND (reglementation_applicable.om_validite_fin IS NULL OR reglementation_applicable.om_validite_fin > CURRENT_DATE)))";
$where_om_validite = " WHERE ((reglementation_applicable.om_validite_debut IS NULL AND (reglementation_applicable.om_validite_fin IS NULL OR reglementation_applicable.om_validite_fin > CURRENT_DATE)) OR (reglementation_applicable.om_validite_debut <= CURRENT_DATE AND (reglementation_applicable.om_validite_fin IS NULL OR reglementation_applicable.om_validite_fin > CURRENT_DATE)))";
// Liste des clés étrangères avec leurs éventuelles surcharges
$foreign_keys_extended = array(
    "service" => array("service", ),
);
// Filtre listing sous formulaire - service
if (in_array($retourformulaire, $foreign_keys_extended["service"])) {
    $selection = " WHERE (reglementation_applicable.service = ".intval($idxformulaire).")  AND ((reglementation_applicable.om_validite_debut IS NULL AND (reglementation_applicable.om_validite_fin IS NULL OR reglementation_applicable.om_validite_fin > CURRENT_DATE)) OR (reglementation_applicable.om_validite_debut <= CURRENT_DATE AND (reglementation_applicable.om_validite_fin IS NULL OR reglementation_applicable.om_validite_fin > CURRENT_DATE)))";
$where_om_validite = " AND ((reglementation_applicable.om_validite_debut IS NULL AND (reglementation_applicable.om_validite_fin IS NULL OR reglementation_applicable.om_validite_fin > CURRENT_DATE)) OR (reglementation_applicable.om_validite_debut <= CURRENT_DATE AND (reglementation_applicable.om_validite_fin IS NULL OR reglementation_applicable.om_validite_fin > CURRENT_DATE)))";
}
// Gestion OMValidité - Suppression du filtre si paramètre
if (isset($_GET["valide"]) and $_GET["valide"] == "false") {
    if (!isset($where_om_validite)
        or (isset($where_om_validite) and $where_om_validite == "")) {
        if (trim($selection) != "") {
            $selection = "";
        }
    } else {
        $selection = trim(str_replace($where_om_validite, "", $selection));
    }
}

/**
 * Gestion SOUSFORMULAIRE => $sousformulaire
 */
$sousformulaire = array(
    //'lien_reglementation_applicable_etablissement_categorie',
    //'lien_reglementation_applicable_etablissement_type',
);

