<?php
//$Id$ 
//gen openMairie le 07/07/2020 09:15

$DEBUG=0;
$serie=30;
$ent = __("application")." -> ".__("lien_contrainte_dossier_coordination");
if(!isset($premier)) $premier='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
// FROM 
$table = DB_PREFIXE."lien_contrainte_dossier_coordination
    LEFT JOIN ".DB_PREFIXE."contrainte 
        ON lien_contrainte_dossier_coordination.contrainte=contrainte.contrainte 
    LEFT JOIN ".DB_PREFIXE."dossier_coordination 
        ON lien_contrainte_dossier_coordination.dossier_coordination=dossier_coordination.dossier_coordination ";
// SELECT 
$champAffiche = array(
    'lien_contrainte_dossier_coordination.lien_contrainte_dossier_coordination as "'.__("lien_contrainte_dossier_coordination").'"',
    'dossier_coordination.libelle as "'.__("dossier_coordination").'"',
    'contrainte.libelle as "'.__("contrainte").'"',
    "case lien_contrainte_dossier_coordination.recuperee when 't' then 'Oui' else 'Non' end as \"".__("recuperee")."\"",
    );
//
$champNonAffiche = array(
    'lien_contrainte_dossier_coordination.texte_complete as "'.__("texte_complete").'"',
    );
//
$champRecherche = array(
    'lien_contrainte_dossier_coordination.lien_contrainte_dossier_coordination as "'.__("lien_contrainte_dossier_coordination").'"',
    'dossier_coordination.libelle as "'.__("dossier_coordination").'"',
    'contrainte.libelle as "'.__("contrainte").'"',
    );
$tri="ORDER BY dossier_coordination.libelle ASC NULLS LAST";
$edition="lien_contrainte_dossier_coordination";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
$selection = "";
// Liste des clés étrangères avec leurs éventuelles surcharges
$foreign_keys_extended = array(
    "contrainte" => array("contrainte", ),
    "dossier_coordination" => array("dossier_coordination", "dossier_coordination_nouveau", "dossier_coordination_a_qualifier", "dossier_coordination_a_cloturer", ),
);
// Filtre listing sous formulaire - contrainte
if (in_array($retourformulaire, $foreign_keys_extended["contrainte"])) {
    $selection = " WHERE (lien_contrainte_dossier_coordination.contrainte = ".intval($idxformulaire).") ";
}
// Filtre listing sous formulaire - dossier_coordination
if (in_array($retourformulaire, $foreign_keys_extended["dossier_coordination"])) {
    $selection = " WHERE (lien_contrainte_dossier_coordination.dossier_coordination = ".intval($idxformulaire).") ";
}

