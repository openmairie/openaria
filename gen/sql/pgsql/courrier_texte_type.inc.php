<?php
//$Id$ 
//gen openMairie le 07/07/2020 09:15

$DEBUG=0;
$serie=30;
$ent = __("administration_parametrage")." -> ".__("documents generes")." -> ".__("complements");
$om_validite = true;
if(!isset($premier)) $premier='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
// FROM 
$table = DB_PREFIXE."courrier_texte_type
    LEFT JOIN ".DB_PREFIXE."courrier_type 
        ON courrier_texte_type.courrier_type=courrier_type.courrier_type ";
// SELECT 
$champAffiche = array(
    'courrier_texte_type.courrier_texte_type as "'.__("courrier_texte_type").'"',
    'courrier_texte_type.libelle as "'.__("libelle").'"',
    'courrier_type.libelle as "'.__("courrier_type").'"',
    );
// Spécificité des dates de validité
$displayed_fields_validite = array(
    'to_char(courrier_texte_type.om_validite_debut ,\'DD/MM/YYYY\') as "'.__("om_validite_debut").'"',
    'to_char(courrier_texte_type.om_validite_fin ,\'DD/MM/YYYY\') as "'.__("om_validite_fin").'"',
);
// On affiche les champs de date de validité uniquement lorsque le paramètre
// d'affichage des éléments expirés est activé
if (isset($_GET['valide']) && $_GET['valide'] === 'false') {
    $champAffiche = array_merge($champAffiche, $displayed_fields_validite);
}

//
$champNonAffiche = array(
    'courrier_texte_type.contenu_om_html as "'.__("contenu_om_html").'"',
    'courrier_texte_type.om_validite_debut as "'.__("om_validite_debut").'"',
    'courrier_texte_type.om_validite_fin as "'.__("om_validite_fin").'"',
    );
//
$champRecherche = array(
    'courrier_texte_type.courrier_texte_type as "'.__("courrier_texte_type").'"',
    'courrier_texte_type.libelle as "'.__("libelle").'"',
    'courrier_type.libelle as "'.__("courrier_type").'"',
    );
$tri="ORDER BY courrier_texte_type.libelle ASC NULLS LAST";
$edition="courrier_texte_type";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
$selection = " WHERE ((courrier_texte_type.om_validite_debut IS NULL AND (courrier_texte_type.om_validite_fin IS NULL OR courrier_texte_type.om_validite_fin > CURRENT_DATE)) OR (courrier_texte_type.om_validite_debut <= CURRENT_DATE AND (courrier_texte_type.om_validite_fin IS NULL OR courrier_texte_type.om_validite_fin > CURRENT_DATE)))";
$where_om_validite = " WHERE ((courrier_texte_type.om_validite_debut IS NULL AND (courrier_texte_type.om_validite_fin IS NULL OR courrier_texte_type.om_validite_fin > CURRENT_DATE)) OR (courrier_texte_type.om_validite_debut <= CURRENT_DATE AND (courrier_texte_type.om_validite_fin IS NULL OR courrier_texte_type.om_validite_fin > CURRENT_DATE)))";
// Liste des clés étrangères avec leurs éventuelles surcharges
$foreign_keys_extended = array(
    "courrier_type" => array("courrier_type", ),
);
// Filtre listing sous formulaire - courrier_type
if (in_array($retourformulaire, $foreign_keys_extended["courrier_type"])) {
    $selection = " WHERE (courrier_texte_type.courrier_type = ".intval($idxformulaire).")  AND ((courrier_texte_type.om_validite_debut IS NULL AND (courrier_texte_type.om_validite_fin IS NULL OR courrier_texte_type.om_validite_fin > CURRENT_DATE)) OR (courrier_texte_type.om_validite_debut <= CURRENT_DATE AND (courrier_texte_type.om_validite_fin IS NULL OR courrier_texte_type.om_validite_fin > CURRENT_DATE)))";
$where_om_validite = " AND ((courrier_texte_type.om_validite_debut IS NULL AND (courrier_texte_type.om_validite_fin IS NULL OR courrier_texte_type.om_validite_fin > CURRENT_DATE)) OR (courrier_texte_type.om_validite_debut <= CURRENT_DATE AND (courrier_texte_type.om_validite_fin IS NULL OR courrier_texte_type.om_validite_fin > CURRENT_DATE)))";
}
// Gestion OMValidité - Suppression du filtre si paramètre
if (isset($_GET["valide"]) and $_GET["valide"] == "false") {
    if (!isset($where_om_validite)
        or (isset($where_om_validite) and $where_om_validite == "")) {
        if (trim($selection) != "") {
            $selection = "";
        }
    } else {
        $selection = trim(str_replace($where_om_validite, "", $selection));
    }
}

