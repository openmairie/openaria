<?php
//$Id$ 
//gen openMairie le 07/07/2020 09:15

$DEBUG=0;
$serie=30;
$ent = __("administration_parametrage")." -> ".__("adresses")." -> ".__("arrondissements");
$om_validite = true;
if(!isset($premier)) $premier='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
// FROM 
$table = DB_PREFIXE."arrondissement";
// SELECT 
$champAffiche = array(
    'arrondissement.arrondissement as "'.__("arrondissement").'"',
    'arrondissement.code as "'.__("code").'"',
    'arrondissement.libelle as "'.__("libelle").'"',
    'arrondissement.description as "'.__("description").'"',
    'arrondissement.code_impots as "'.__("code_impots").'"',
    );
// Spécificité des dates de validité
$displayed_fields_validite = array(
    'to_char(arrondissement.om_validite_debut ,\'DD/MM/YYYY\') as "'.__("om_validite_debut").'"',
    'to_char(arrondissement.om_validite_fin ,\'DD/MM/YYYY\') as "'.__("om_validite_fin").'"',
);
// On affiche les champs de date de validité uniquement lorsque le paramètre
// d'affichage des éléments expirés est activé
if (isset($_GET['valide']) && $_GET['valide'] === 'false') {
    $champAffiche = array_merge($champAffiche, $displayed_fields_validite);
}

//
$champNonAffiche = array(
    'arrondissement.om_validite_debut as "'.__("om_validite_debut").'"',
    'arrondissement.om_validite_fin as "'.__("om_validite_fin").'"',
    );
//
$champRecherche = array(
    'arrondissement.arrondissement as "'.__("arrondissement").'"',
    'arrondissement.code as "'.__("code").'"',
    'arrondissement.libelle as "'.__("libelle").'"',
    'arrondissement.description as "'.__("description").'"',
    'arrondissement.code_impots as "'.__("code_impots").'"',
    );
$tri="ORDER BY arrondissement.libelle ASC NULLS LAST";
$edition="arrondissement";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
$selection = " WHERE ((arrondissement.om_validite_debut IS NULL AND (arrondissement.om_validite_fin IS NULL OR arrondissement.om_validite_fin > CURRENT_DATE)) OR (arrondissement.om_validite_debut <= CURRENT_DATE AND (arrondissement.om_validite_fin IS NULL OR arrondissement.om_validite_fin > CURRENT_DATE)))";
$where_om_validite = " WHERE ((arrondissement.om_validite_debut IS NULL AND (arrondissement.om_validite_fin IS NULL OR arrondissement.om_validite_fin > CURRENT_DATE)) OR (arrondissement.om_validite_debut <= CURRENT_DATE AND (arrondissement.om_validite_fin IS NULL OR arrondissement.om_validite_fin > CURRENT_DATE)))";
// Gestion OMValidité - Suppression du filtre si paramètre
if (isset($_GET["valide"]) and $_GET["valide"] == "false") {
    if (!isset($where_om_validite)
        or (isset($where_om_validite) and $where_om_validite == "")) {
        if (trim($selection) != "") {
            $selection = "";
        }
    } else {
        $selection = trim(str_replace($where_om_validite, "", $selection));
    }
}

/**
 * Gestion SOUSFORMULAIRE => $sousformulaire
 */
$sousformulaire = array(
    //'etablissement',
    //'quartier',
    //'technicien_arrondissement',
    //'voie_arrondissement',
);

