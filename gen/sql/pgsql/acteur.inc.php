<?php
//$Id$ 
//gen openMairie le 07/07/2020 09:15

$DEBUG=0;
$serie=30;
$ent = __("administration_parametrage")." -> ".__("metiers")." -> ".__("acteurs");
$om_validite = true;
if(!isset($premier)) $premier='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
// FROM 
$table = DB_PREFIXE."acteur
    LEFT JOIN ".DB_PREFIXE."om_utilisateur 
        ON acteur.om_utilisateur=om_utilisateur.om_utilisateur 
    LEFT JOIN ".DB_PREFIXE."service 
        ON acteur.service=service.service ";
// SELECT 
$champAffiche = array(
    'acteur.acteur as "'.__("acteur").'"',
    'acteur.nom_prenom as "'.__("nom_prenom").'"',
    'om_utilisateur.nom as "'.__("om_utilisateur").'"',
    'service.libelle as "'.__("service").'"',
    'acteur.role as "'.__("role").'"',
    'acteur.acronyme as "'.__("acronyme").'"',
    'acteur.couleur as "'.__("couleur").'"',
    'acteur.reference as "'.__("reference").'"',
    );
// Spécificité des dates de validité
$displayed_fields_validite = array(
    'to_char(acteur.om_validite_debut ,\'DD/MM/YYYY\') as "'.__("om_validite_debut").'"',
    'to_char(acteur.om_validite_fin ,\'DD/MM/YYYY\') as "'.__("om_validite_fin").'"',
);
// On affiche les champs de date de validité uniquement lorsque le paramètre
// d'affichage des éléments expirés est activé
if (isset($_GET['valide']) && $_GET['valide'] === 'false') {
    $champAffiche = array_merge($champAffiche, $displayed_fields_validite);
}

//
$champNonAffiche = array(
    'acteur.om_validite_debut as "'.__("om_validite_debut").'"',
    'acteur.om_validite_fin as "'.__("om_validite_fin").'"',
    );
//
$champRecherche = array(
    'acteur.acteur as "'.__("acteur").'"',
    'acteur.nom_prenom as "'.__("nom_prenom").'"',
    'om_utilisateur.nom as "'.__("om_utilisateur").'"',
    'service.libelle as "'.__("service").'"',
    'acteur.role as "'.__("role").'"',
    'acteur.acronyme as "'.__("acronyme").'"',
    'acteur.couleur as "'.__("couleur").'"',
    'acteur.reference as "'.__("reference").'"',
    );
$tri="ORDER BY acteur.nom_prenom ASC NULLS LAST";
$edition="acteur";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
$selection = " WHERE ((acteur.om_validite_debut IS NULL AND (acteur.om_validite_fin IS NULL OR acteur.om_validite_fin > CURRENT_DATE)) OR (acteur.om_validite_debut <= CURRENT_DATE AND (acteur.om_validite_fin IS NULL OR acteur.om_validite_fin > CURRENT_DATE)))";
$where_om_validite = " WHERE ((acteur.om_validite_debut IS NULL AND (acteur.om_validite_fin IS NULL OR acteur.om_validite_fin > CURRENT_DATE)) OR (acteur.om_validite_debut <= CURRENT_DATE AND (acteur.om_validite_fin IS NULL OR acteur.om_validite_fin > CURRENT_DATE)))";
// Liste des clés étrangères avec leurs éventuelles surcharges
$foreign_keys_extended = array(
    "om_utilisateur" => array("om_utilisateur", ),
    "service" => array("service", ),
);
// Filtre listing sous formulaire - om_utilisateur
if (in_array($retourformulaire, $foreign_keys_extended["om_utilisateur"])) {
    $selection = " WHERE (acteur.om_utilisateur = ".intval($idxformulaire).")  AND ((acteur.om_validite_debut IS NULL AND (acteur.om_validite_fin IS NULL OR acteur.om_validite_fin > CURRENT_DATE)) OR (acteur.om_validite_debut <= CURRENT_DATE AND (acteur.om_validite_fin IS NULL OR acteur.om_validite_fin > CURRENT_DATE)))";
$where_om_validite = " AND ((acteur.om_validite_debut IS NULL AND (acteur.om_validite_fin IS NULL OR acteur.om_validite_fin > CURRENT_DATE)) OR (acteur.om_validite_debut <= CURRENT_DATE AND (acteur.om_validite_fin IS NULL OR acteur.om_validite_fin > CURRENT_DATE)))";
}
// Filtre listing sous formulaire - service
if (in_array($retourformulaire, $foreign_keys_extended["service"])) {
    $selection = " WHERE (acteur.service = ".intval($idxformulaire).")  AND ((acteur.om_validite_debut IS NULL AND (acteur.om_validite_fin IS NULL OR acteur.om_validite_fin > CURRENT_DATE)) OR (acteur.om_validite_debut <= CURRENT_DATE AND (acteur.om_validite_fin IS NULL OR acteur.om_validite_fin > CURRENT_DATE)))";
$where_om_validite = " AND ((acteur.om_validite_debut IS NULL AND (acteur.om_validite_fin IS NULL OR acteur.om_validite_fin > CURRENT_DATE)) OR (acteur.om_validite_debut <= CURRENT_DATE AND (acteur.om_validite_fin IS NULL OR acteur.om_validite_fin > CURRENT_DATE)))";
}
// Gestion OMValidité - Suppression du filtre si paramètre
if (isset($_GET["valide"]) and $_GET["valide"] == "false") {
    if (!isset($where_om_validite)
        or (isset($where_om_validite) and $where_om_validite == "")) {
        if (trim($selection) != "") {
            $selection = "";
        }
    } else {
        $selection = trim(str_replace($where_om_validite, "", $selection));
    }
}

/**
 * Gestion SOUSFORMULAIRE => $sousformulaire
 */
$sousformulaire = array(
    'acteur_conge',
    'acteur_plage_visite',
    'dossier_instruction',
    'etablissement',
    'technicien_arrondissement',
    'visite',
);

