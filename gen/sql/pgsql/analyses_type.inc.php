<?php
//$Id$ 
//gen openMairie le 07/07/2020 09:15

$DEBUG=0;
$serie=30;
$ent = __("administration_parametrage")." -> ".__("analyses")." -> ".__("types");
$om_validite = true;
if(!isset($premier)) $premier='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
// FROM 
$table = DB_PREFIXE."analyses_type
    LEFT JOIN ".DB_PREFIXE."modele_edition as modele_edition0 
        ON analyses_type.modele_edition_compte_rendu=modele_edition0.modele_edition 
    LEFT JOIN ".DB_PREFIXE."modele_edition as modele_edition1 
        ON analyses_type.modele_edition_proces_verbal=modele_edition1.modele_edition 
    LEFT JOIN ".DB_PREFIXE."modele_edition as modele_edition2 
        ON analyses_type.modele_edition_rapport=modele_edition2.modele_edition 
    LEFT JOIN ".DB_PREFIXE."service 
        ON analyses_type.service=service.service ";
// SELECT 
$champAffiche = array(
    'analyses_type.analyses_type as "'.__("analyses_type").'"',
    'service.libelle as "'.__("service").'"',
    'analyses_type.code as "'.__("code").'"',
    'analyses_type.libelle as "'.__("libelle").'"',
    'modele_edition2.libelle as "'.__("modele_edition_rapport").'"',
    'modele_edition0.libelle as "'.__("modele_edition_compte_rendu").'"',
    'modele_edition1.libelle as "'.__("modele_edition_proces_verbal").'"',
    );
// Spécificité des dates de validité
$displayed_fields_validite = array(
    'to_char(analyses_type.om_validite_debut ,\'DD/MM/YYYY\') as "'.__("om_validite_debut").'"',
    'to_char(analyses_type.om_validite_fin ,\'DD/MM/YYYY\') as "'.__("om_validite_fin").'"',
);
// On affiche les champs de date de validité uniquement lorsque le paramètre
// d'affichage des éléments expirés est activé
if (isset($_GET['valide']) && $_GET['valide'] === 'false') {
    $champAffiche = array_merge($champAffiche, $displayed_fields_validite);
}

//
$champNonAffiche = array(
    'analyses_type.description as "'.__("description").'"',
    'analyses_type.om_validite_debut as "'.__("om_validite_debut").'"',
    'analyses_type.om_validite_fin as "'.__("om_validite_fin").'"',
    );
//
$champRecherche = array(
    'analyses_type.analyses_type as "'.__("analyses_type").'"',
    'service.libelle as "'.__("service").'"',
    'analyses_type.code as "'.__("code").'"',
    'analyses_type.libelle as "'.__("libelle").'"',
    'modele_edition2.libelle as "'.__("modele_edition_rapport").'"',
    'modele_edition0.libelle as "'.__("modele_edition_compte_rendu").'"',
    'modele_edition1.libelle as "'.__("modele_edition_proces_verbal").'"',
    );
$tri="ORDER BY analyses_type.libelle ASC NULLS LAST";
$edition="analyses_type";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
$selection = " WHERE ((analyses_type.om_validite_debut IS NULL AND (analyses_type.om_validite_fin IS NULL OR analyses_type.om_validite_fin > CURRENT_DATE)) OR (analyses_type.om_validite_debut <= CURRENT_DATE AND (analyses_type.om_validite_fin IS NULL OR analyses_type.om_validite_fin > CURRENT_DATE)))";
$where_om_validite = " WHERE ((analyses_type.om_validite_debut IS NULL AND (analyses_type.om_validite_fin IS NULL OR analyses_type.om_validite_fin > CURRENT_DATE)) OR (analyses_type.om_validite_debut <= CURRENT_DATE AND (analyses_type.om_validite_fin IS NULL OR analyses_type.om_validite_fin > CURRENT_DATE)))";
// Liste des clés étrangères avec leurs éventuelles surcharges
$foreign_keys_extended = array(
    "modele_edition" => array("modele_edition", ),
    "service" => array("service", ),
);
// Filtre listing sous formulaire - modele_edition
if (in_array($retourformulaire, $foreign_keys_extended["modele_edition"])) {
    $selection = " WHERE (analyses_type.modele_edition_compte_rendu = ".intval($idxformulaire)." OR analyses_type.modele_edition_proces_verbal = ".intval($idxformulaire)." OR analyses_type.modele_edition_rapport = ".intval($idxformulaire).")  AND ((analyses_type.om_validite_debut IS NULL AND (analyses_type.om_validite_fin IS NULL OR analyses_type.om_validite_fin > CURRENT_DATE)) OR (analyses_type.om_validite_debut <= CURRENT_DATE AND (analyses_type.om_validite_fin IS NULL OR analyses_type.om_validite_fin > CURRENT_DATE)))";
$where_om_validite = " AND ((analyses_type.om_validite_debut IS NULL AND (analyses_type.om_validite_fin IS NULL OR analyses_type.om_validite_fin > CURRENT_DATE)) OR (analyses_type.om_validite_debut <= CURRENT_DATE AND (analyses_type.om_validite_fin IS NULL OR analyses_type.om_validite_fin > CURRENT_DATE)))";
}
// Filtre listing sous formulaire - service
if (in_array($retourformulaire, $foreign_keys_extended["service"])) {
    $selection = " WHERE (analyses_type.service = ".intval($idxformulaire).")  AND ((analyses_type.om_validite_debut IS NULL AND (analyses_type.om_validite_fin IS NULL OR analyses_type.om_validite_fin > CURRENT_DATE)) OR (analyses_type.om_validite_debut <= CURRENT_DATE AND (analyses_type.om_validite_fin IS NULL OR analyses_type.om_validite_fin > CURRENT_DATE)))";
$where_om_validite = " AND ((analyses_type.om_validite_debut IS NULL AND (analyses_type.om_validite_fin IS NULL OR analyses_type.om_validite_fin > CURRENT_DATE)) OR (analyses_type.om_validite_debut <= CURRENT_DATE AND (analyses_type.om_validite_fin IS NULL OR analyses_type.om_validite_fin > CURRENT_DATE)))";
}
// Gestion OMValidité - Suppression du filtre si paramètre
if (isset($_GET["valide"]) and $_GET["valide"] == "false") {
    if (!isset($where_om_validite)
        or (isset($where_om_validite) and $where_om_validite == "")) {
        if (trim($selection) != "") {
            $selection = "";
        }
    } else {
        $selection = trim(str_replace($where_om_validite, "", $selection));
    }
}

/**
 * Gestion SOUSFORMULAIRE => $sousformulaire
 */
$sousformulaire = array(
    //'analyses',
    //'dossier_coordination_type',
    //'etablissement',
    //'lien_dossier_coordination_type_analyses_type',
);

