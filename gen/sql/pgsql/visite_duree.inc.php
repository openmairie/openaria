<?php
//$Id$ 
//gen openMairie le 07/07/2020 09:15

$DEBUG=0;
$serie=30;
$ent = __("administration_parametrage")." -> ".__("visites")." -> ".__("durees");
if(!isset($premier)) $premier='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
// FROM 
$table = DB_PREFIXE."visite_duree";
// SELECT 
$champAffiche = array(
    'visite_duree.visite_duree as "'.__("visite_duree").'"',
    'visite_duree.libelle as "'.__("libelle").'"',
    'visite_duree.duree as "'.__("duree").'"',
    );
//
$champNonAffiche = array(
    );
//
$champRecherche = array(
    'visite_duree.visite_duree as "'.__("visite_duree").'"',
    'visite_duree.libelle as "'.__("libelle").'"',
    'visite_duree.duree as "'.__("duree").'"',
    );
$tri="ORDER BY visite_duree.libelle ASC NULLS LAST";
$edition="visite_duree";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
$selection = "";

/**
 * Gestion SOUSFORMULAIRE => $sousformulaire
 */
$sousformulaire = array(
    //'etablissement',
);

