<?php
//$Id$ 
//gen openMairie le 07/07/2020 09:15

$DEBUG=0;
$serie=30;
$ent = __("application")." -> ".__("reunion_type_reunion_instance");
if(!isset($premier)) $premier='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
// FROM 
$table = DB_PREFIXE."reunion_type_reunion_instance
    LEFT JOIN ".DB_PREFIXE."reunion_instance 
        ON reunion_type_reunion_instance.reunion_instance=reunion_instance.reunion_instance 
    LEFT JOIN ".DB_PREFIXE."reunion_type 
        ON reunion_type_reunion_instance.reunion_type=reunion_type.reunion_type ";
// SELECT 
$champAffiche = array(
    'reunion_type_reunion_instance.reunion_type_reunion_instance as "'.__("reunion_type_reunion_instance").'"',
    'reunion_type.libelle as "'.__("reunion_type").'"',
    'reunion_instance.libelle as "'.__("reunion_instance").'"',
    );
//
$champNonAffiche = array(
    );
//
$champRecherche = array(
    'reunion_type_reunion_instance.reunion_type_reunion_instance as "'.__("reunion_type_reunion_instance").'"',
    'reunion_type.libelle as "'.__("reunion_type").'"',
    'reunion_instance.libelle as "'.__("reunion_instance").'"',
    );
$tri="ORDER BY reunion_type.libelle ASC NULLS LAST";
$edition="reunion_type_reunion_instance";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
$selection = "";
// Liste des clés étrangères avec leurs éventuelles surcharges
$foreign_keys_extended = array(
    "reunion_instance" => array("reunion_instance", ),
    "reunion_type" => array("reunion_type", ),
);
// Filtre listing sous formulaire - reunion_instance
if (in_array($retourformulaire, $foreign_keys_extended["reunion_instance"])) {
    $selection = " WHERE (reunion_type_reunion_instance.reunion_instance = ".intval($idxformulaire).") ";
}
// Filtre listing sous formulaire - reunion_type
if (in_array($retourformulaire, $foreign_keys_extended["reunion_type"])) {
    $selection = " WHERE (reunion_type_reunion_instance.reunion_type = ".intval($idxformulaire).") ";
}

