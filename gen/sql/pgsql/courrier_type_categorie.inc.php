<?php
//$Id$ 
//gen openMairie le 07/07/2020 09:15

$DEBUG=0;
$serie=30;
$ent = __("administration_parametrage")." -> ".__("editions")." -> ".__("categories");
$om_validite = true;
if(!isset($premier)) $premier='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
// FROM 
$table = DB_PREFIXE."courrier_type_categorie";
// SELECT 
$champAffiche = array(
    'courrier_type_categorie.courrier_type_categorie as "'.__("courrier_type_categorie").'"',
    'courrier_type_categorie.code as "'.__("code").'"',
    'courrier_type_categorie.libelle as "'.__("libelle").'"',
    'courrier_type_categorie.objet as "'.__("objet").'"',
    );
// Spécificité des dates de validité
$displayed_fields_validite = array(
    'to_char(courrier_type_categorie.om_validite_debut ,\'DD/MM/YYYY\') as "'.__("om_validite_debut").'"',
    'to_char(courrier_type_categorie.om_validite_fin ,\'DD/MM/YYYY\') as "'.__("om_validite_fin").'"',
);
// On affiche les champs de date de validité uniquement lorsque le paramètre
// d'affichage des éléments expirés est activé
if (isset($_GET['valide']) && $_GET['valide'] === 'false') {
    $champAffiche = array_merge($champAffiche, $displayed_fields_validite);
}

//
$champNonAffiche = array(
    'courrier_type_categorie.description as "'.__("description").'"',
    'courrier_type_categorie.om_validite_debut as "'.__("om_validite_debut").'"',
    'courrier_type_categorie.om_validite_fin as "'.__("om_validite_fin").'"',
    );
//
$champRecherche = array(
    'courrier_type_categorie.courrier_type_categorie as "'.__("courrier_type_categorie").'"',
    'courrier_type_categorie.code as "'.__("code").'"',
    'courrier_type_categorie.libelle as "'.__("libelle").'"',
    'courrier_type_categorie.objet as "'.__("objet").'"',
    );
$tri="ORDER BY courrier_type_categorie.libelle ASC NULLS LAST";
$edition="courrier_type_categorie";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
$selection = " WHERE ((courrier_type_categorie.om_validite_debut IS NULL AND (courrier_type_categorie.om_validite_fin IS NULL OR courrier_type_categorie.om_validite_fin > CURRENT_DATE)) OR (courrier_type_categorie.om_validite_debut <= CURRENT_DATE AND (courrier_type_categorie.om_validite_fin IS NULL OR courrier_type_categorie.om_validite_fin > CURRENT_DATE)))";
$where_om_validite = " WHERE ((courrier_type_categorie.om_validite_debut IS NULL AND (courrier_type_categorie.om_validite_fin IS NULL OR courrier_type_categorie.om_validite_fin > CURRENT_DATE)) OR (courrier_type_categorie.om_validite_debut <= CURRENT_DATE AND (courrier_type_categorie.om_validite_fin IS NULL OR courrier_type_categorie.om_validite_fin > CURRENT_DATE)))";
// Gestion OMValidité - Suppression du filtre si paramètre
if (isset($_GET["valide"]) and $_GET["valide"] == "false") {
    if (!isset($where_om_validite)
        or (isset($where_om_validite) and $where_om_validite == "")) {
        if (trim($selection) != "") {
            $selection = "";
        }
    } else {
        $selection = trim(str_replace($where_om_validite, "", $selection));
    }
}

/**
 * Gestion SOUSFORMULAIRE => $sousformulaire
 */
$sousformulaire = array(
    //'courrier_type',
);

