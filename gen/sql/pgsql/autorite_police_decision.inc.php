<?php
//$Id$ 
//gen openMairie le 07/07/2020 09:15

$DEBUG=0;
$serie=30;
$ent = __("administration_parametrage")." -> ".__("autorites de police")." -> ".__("types");
$om_validite = true;
if(!isset($premier)) $premier='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
// FROM 
$table = DB_PREFIXE."autorite_police_decision
    LEFT JOIN ".DB_PREFIXE."etablissement_etat 
        ON autorite_police_decision.etablissement_etat=etablissement_etat.etablissement_etat 
    LEFT JOIN ".DB_PREFIXE."service 
        ON autorite_police_decision.service=service.service ";
// SELECT 
$champAffiche = array(
    'autorite_police_decision.autorite_police_decision as "'.__("autorite_police_decision").'"',
    'autorite_police_decision.code as "'.__("code").'"',
    'autorite_police_decision.libelle as "'.__("libelle").'"',
    'service.libelle as "'.__("service").'"',
    "case autorite_police_decision.suivi_delai when 't' then 'Oui' else 'Non' end as \"".__("suivi_delai")."\"",
    'autorite_police_decision.avis as "'.__("avis").'"',
    'etablissement_etat.statut_administratif as "'.__("etablissement_etat").'"',
    'autorite_police_decision.delai as "'.__("delai").'"',
    "case autorite_police_decision.type_arrete when 't' then 'Oui' else 'Non' end as \"".__("type_arrete")."\"",
    );
// Spécificité des dates de validité
$displayed_fields_validite = array(
    'to_char(autorite_police_decision.om_validite_debut ,\'DD/MM/YYYY\') as "'.__("om_validite_debut").'"',
    'to_char(autorite_police_decision.om_validite_fin ,\'DD/MM/YYYY\') as "'.__("om_validite_fin").'"',
);
// On affiche les champs de date de validité uniquement lorsque le paramètre
// d'affichage des éléments expirés est activé
if (isset($_GET['valide']) && $_GET['valide'] === 'false') {
    $champAffiche = array_merge($champAffiche, $displayed_fields_validite);
}

//
$champNonAffiche = array(
    'autorite_police_decision.description as "'.__("description").'"',
    'autorite_police_decision.om_validite_debut as "'.__("om_validite_debut").'"',
    'autorite_police_decision.om_validite_fin as "'.__("om_validite_fin").'"',
    'autorite_police_decision.arrete_reglementaire as "'.__("arrete_reglementaire").'"',
    'autorite_police_decision.arrete_notification as "'.__("arrete_notification").'"',
    'autorite_police_decision.arrete_publication as "'.__("arrete_publication").'"',
    'autorite_police_decision.arrete_temporaire as "'.__("arrete_temporaire").'"',
    'autorite_police_decision.nomenclature_actes_nature as "'.__("nomenclature_actes_nature").'"',
    'autorite_police_decision.nomenclature_actes_matiere_niv1 as "'.__("nomenclature_actes_matiere_niv1").'"',
    'autorite_police_decision.nomenclature_actes_matiere_niv2 as "'.__("nomenclature_actes_matiere_niv2").'"',
    );
//
$champRecherche = array(
    'autorite_police_decision.autorite_police_decision as "'.__("autorite_police_decision").'"',
    'autorite_police_decision.code as "'.__("code").'"',
    'autorite_police_decision.libelle as "'.__("libelle").'"',
    'service.libelle as "'.__("service").'"',
    'autorite_police_decision.avis as "'.__("avis").'"',
    'etablissement_etat.statut_administratif as "'.__("etablissement_etat").'"',
    'autorite_police_decision.delai as "'.__("delai").'"',
    );
$tri="ORDER BY autorite_police_decision.libelle ASC NULLS LAST";
$edition="autorite_police_decision";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
$selection = " WHERE ((autorite_police_decision.om_validite_debut IS NULL AND (autorite_police_decision.om_validite_fin IS NULL OR autorite_police_decision.om_validite_fin > CURRENT_DATE)) OR (autorite_police_decision.om_validite_debut <= CURRENT_DATE AND (autorite_police_decision.om_validite_fin IS NULL OR autorite_police_decision.om_validite_fin > CURRENT_DATE)))";
$where_om_validite = " WHERE ((autorite_police_decision.om_validite_debut IS NULL AND (autorite_police_decision.om_validite_fin IS NULL OR autorite_police_decision.om_validite_fin > CURRENT_DATE)) OR (autorite_police_decision.om_validite_debut <= CURRENT_DATE AND (autorite_police_decision.om_validite_fin IS NULL OR autorite_police_decision.om_validite_fin > CURRENT_DATE)))";
// Liste des clés étrangères avec leurs éventuelles surcharges
$foreign_keys_extended = array(
    "etablissement_etat" => array("etablissement_etat", ),
    "service" => array("service", ),
);
// Filtre listing sous formulaire - etablissement_etat
if (in_array($retourformulaire, $foreign_keys_extended["etablissement_etat"])) {
    $selection = " WHERE (autorite_police_decision.etablissement_etat = ".intval($idxformulaire).")  AND ((autorite_police_decision.om_validite_debut IS NULL AND (autorite_police_decision.om_validite_fin IS NULL OR autorite_police_decision.om_validite_fin > CURRENT_DATE)) OR (autorite_police_decision.om_validite_debut <= CURRENT_DATE AND (autorite_police_decision.om_validite_fin IS NULL OR autorite_police_decision.om_validite_fin > CURRENT_DATE)))";
$where_om_validite = " AND ((autorite_police_decision.om_validite_debut IS NULL AND (autorite_police_decision.om_validite_fin IS NULL OR autorite_police_decision.om_validite_fin > CURRENT_DATE)) OR (autorite_police_decision.om_validite_debut <= CURRENT_DATE AND (autorite_police_decision.om_validite_fin IS NULL OR autorite_police_decision.om_validite_fin > CURRENT_DATE)))";
}
// Filtre listing sous formulaire - service
if (in_array($retourformulaire, $foreign_keys_extended["service"])) {
    $selection = " WHERE (autorite_police_decision.service = ".intval($idxformulaire).")  AND ((autorite_police_decision.om_validite_debut IS NULL AND (autorite_police_decision.om_validite_fin IS NULL OR autorite_police_decision.om_validite_fin > CURRENT_DATE)) OR (autorite_police_decision.om_validite_debut <= CURRENT_DATE AND (autorite_police_decision.om_validite_fin IS NULL OR autorite_police_decision.om_validite_fin > CURRENT_DATE)))";
$where_om_validite = " AND ((autorite_police_decision.om_validite_debut IS NULL AND (autorite_police_decision.om_validite_fin IS NULL OR autorite_police_decision.om_validite_fin > CURRENT_DATE)) OR (autorite_police_decision.om_validite_debut <= CURRENT_DATE AND (autorite_police_decision.om_validite_fin IS NULL OR autorite_police_decision.om_validite_fin > CURRENT_DATE)))";
}
// Gestion OMValidité - Suppression du filtre si paramètre
if (isset($_GET["valide"]) and $_GET["valide"] == "false") {
    if (!isset($where_om_validite)
        or (isset($where_om_validite) and $where_om_validite == "")) {
        if (trim($selection) != "") {
            $selection = "";
        }
    } else {
        $selection = trim(str_replace($where_om_validite, "", $selection));
    }
}

/**
 * Gestion SOUSFORMULAIRE => $sousformulaire
 */
$sousformulaire = array(
    //'autorite_police',
);

