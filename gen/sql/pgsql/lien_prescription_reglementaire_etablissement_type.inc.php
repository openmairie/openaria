<?php
//$Id$ 
//gen openMairie le 07/07/2020 09:15

$DEBUG=0;
$serie=30;
$ent = __("application")." -> ".__("lien_prescription_reglementaire_etablissement_type");
if(!isset($premier)) $premier='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
// FROM 
$table = DB_PREFIXE."lien_prescription_reglementaire_etablissement_type
    LEFT JOIN ".DB_PREFIXE."etablissement_type 
        ON lien_prescription_reglementaire_etablissement_type.etablissement_type=etablissement_type.etablissement_type 
    LEFT JOIN ".DB_PREFIXE."prescription_reglementaire 
        ON lien_prescription_reglementaire_etablissement_type.prescription_reglementaire=prescription_reglementaire.prescription_reglementaire ";
// SELECT 
$champAffiche = array(
    'lien_prescription_reglementaire_etablissement_type.lien_prescription_reglementaire_etablissement_type as "'.__("lien_prescription_reglementaire_etablissement_type").'"',
    'prescription_reglementaire.libelle as "'.__("prescription_reglementaire").'"',
    'etablissement_type.libelle as "'.__("etablissement_type").'"',
    );
//
$champNonAffiche = array(
    );
//
$champRecherche = array(
    'lien_prescription_reglementaire_etablissement_type.lien_prescription_reglementaire_etablissement_type as "'.__("lien_prescription_reglementaire_etablissement_type").'"',
    'prescription_reglementaire.libelle as "'.__("prescription_reglementaire").'"',
    'etablissement_type.libelle as "'.__("etablissement_type").'"',
    );
$tri="ORDER BY prescription_reglementaire.libelle ASC NULLS LAST";
$edition="lien_prescription_reglementaire_etablissement_type";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
$selection = "";
// Liste des clés étrangères avec leurs éventuelles surcharges
$foreign_keys_extended = array(
    "etablissement_type" => array("etablissement_type", ),
    "prescription_reglementaire" => array("prescription_reglementaire", ),
);
// Filtre listing sous formulaire - etablissement_type
if (in_array($retourformulaire, $foreign_keys_extended["etablissement_type"])) {
    $selection = " WHERE (lien_prescription_reglementaire_etablissement_type.etablissement_type = ".intval($idxformulaire).") ";
}
// Filtre listing sous formulaire - prescription_reglementaire
if (in_array($retourformulaire, $foreign_keys_extended["prescription_reglementaire"])) {
    $selection = " WHERE (lien_prescription_reglementaire_etablissement_type.prescription_reglementaire = ".intval($idxformulaire).") ";
}

