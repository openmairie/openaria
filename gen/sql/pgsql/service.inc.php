<?php
//$Id$ 
//gen openMairie le 07/07/2020 09:15

$DEBUG=0;
$serie=30;
$ent = __("administration_parametrage")." -> ".__("metiers")." -> ".__("services");
$om_validite = true;
if(!isset($premier)) $premier='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
// FROM 
$table = DB_PREFIXE."service";
// SELECT 
$champAffiche = array(
    'service.service as "'.__("service").'"',
    'service.code as "'.__("code").'"',
    'service.libelle as "'.__("libelle").'"',
    'service.description as "'.__("description").'"',
    );
// Spécificité des dates de validité
$displayed_fields_validite = array(
    'to_char(service.om_validite_debut ,\'DD/MM/YYYY\') as "'.__("om_validite_debut").'"',
    'to_char(service.om_validite_fin ,\'DD/MM/YYYY\') as "'.__("om_validite_fin").'"',
);
// On affiche les champs de date de validité uniquement lorsque le paramètre
// d'affichage des éléments expirés est activé
if (isset($_GET['valide']) && $_GET['valide'] === 'false') {
    $champAffiche = array_merge($champAffiche, $displayed_fields_validite);
}

//
$champNonAffiche = array(
    'service.om_validite_debut as "'.__("om_validite_debut").'"',
    'service.om_validite_fin as "'.__("om_validite_fin").'"',
    );
//
$champRecherche = array(
    'service.service as "'.__("service").'"',
    'service.code as "'.__("code").'"',
    'service.libelle as "'.__("libelle").'"',
    'service.description as "'.__("description").'"',
    );
$tri="ORDER BY service.libelle ASC NULLS LAST";
$edition="service";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
$selection = " WHERE ((service.om_validite_debut IS NULL AND (service.om_validite_fin IS NULL OR service.om_validite_fin > CURRENT_DATE)) OR (service.om_validite_debut <= CURRENT_DATE AND (service.om_validite_fin IS NULL OR service.om_validite_fin > CURRENT_DATE)))";
$where_om_validite = " WHERE ((service.om_validite_debut IS NULL AND (service.om_validite_fin IS NULL OR service.om_validite_fin > CURRENT_DATE)) OR (service.om_validite_debut <= CURRENT_DATE AND (service.om_validite_fin IS NULL OR service.om_validite_fin > CURRENT_DATE)))";
// Gestion OMValidité - Suppression du filtre si paramètre
if (isset($_GET["valide"]) and $_GET["valide"] == "false") {
    if (!isset($where_om_validite)
        or (isset($where_om_validite) and $where_om_validite == "")) {
        if (trim($selection) != "") {
            $selection = "";
        }
    } else {
        $selection = trim(str_replace($where_om_validite, "", $selection));
    }
}

/**
 * Gestion SOUSFORMULAIRE => $sousformulaire
 */
$sousformulaire = array(
    //'acteur',
    //'analyses',
    //'analyses_type',
    //'autorite_competente',
    //'autorite_police',
    //'autorite_police_decision',
    //'autorite_police_motif',
    //'contact',
    //'courrier_type',
    //'document_presente',
    //'dossier_instruction',
    //'essai_realise',
    //'piece',
    //'prescription_reglementaire',
    //'prescription_specifique',
    //'programmation',
    //'reglementation_applicable',
    //'reunion_avis',
    //'reunion_categorie',
    //'reunion_instance',
    //'reunion_type',
    //'technicien_arrondissement',
);

