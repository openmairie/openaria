<?php
//$Id$ 
//gen openMairie le 07/07/2020 09:17

$DEBUG=0;
$serie=30;
$ent = __("administration_parametrage")." -> ".__("metiers")." -> ".__("derogations scda");
$om_validite = true;
if(!isset($premier)) $premier='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
// FROM 
$table = DB_PREFIXE."derogation_scda";
// SELECT 
$champAffiche = array(
    'derogation_scda.derogation_scda as "'.__("derogation_scda").'"',
    'derogation_scda.code as "'.__("code").'"',
    'derogation_scda.libelle as "'.__("libelle").'"',
    );
// Spécificité des dates de validité
$displayed_fields_validite = array(
    'to_char(derogation_scda.om_validite_debut ,\'DD/MM/YYYY\') as "'.__("om_validite_debut").'"',
    'to_char(derogation_scda.om_validite_fin ,\'DD/MM/YYYY\') as "'.__("om_validite_fin").'"',
);
// On affiche les champs de date de validité uniquement lorsque le paramètre
// d'affichage des éléments expirés est activé
if (isset($_GET['valide']) && $_GET['valide'] === 'false') {
    $champAffiche = array_merge($champAffiche, $displayed_fields_validite);
}

//
$champNonAffiche = array(
    'derogation_scda.om_validite_debut as "'.__("om_validite_debut").'"',
    'derogation_scda.om_validite_fin as "'.__("om_validite_fin").'"',
    );
//
$champRecherche = array(
    'derogation_scda.derogation_scda as "'.__("derogation_scda").'"',
    'derogation_scda.code as "'.__("code").'"',
    'derogation_scda.libelle as "'.__("libelle").'"',
    );
$tri="ORDER BY derogation_scda.libelle ASC NULLS LAST";
$edition="derogation_scda";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
$selection = " WHERE ((derogation_scda.om_validite_debut IS NULL AND (derogation_scda.om_validite_fin IS NULL OR derogation_scda.om_validite_fin > CURRENT_DATE)) OR (derogation_scda.om_validite_debut <= CURRENT_DATE AND (derogation_scda.om_validite_fin IS NULL OR derogation_scda.om_validite_fin > CURRENT_DATE)))";
$where_om_validite = " WHERE ((derogation_scda.om_validite_debut IS NULL AND (derogation_scda.om_validite_fin IS NULL OR derogation_scda.om_validite_fin > CURRENT_DATE)) OR (derogation_scda.om_validite_debut <= CURRENT_DATE AND (derogation_scda.om_validite_fin IS NULL OR derogation_scda.om_validite_fin > CURRENT_DATE)))";
// Gestion OMValidité - Suppression du filtre si paramètre
if (isset($_GET["valide"]) and $_GET["valide"] == "false") {
    if (!isset($where_om_validite)
        or (isset($where_om_validite) and $where_om_validite == "")) {
        if (trim($selection) != "") {
            $selection = "";
        }
    } else {
        $selection = trim(str_replace($where_om_validite, "", $selection));
    }
}

/**
 * Gestion SOUSFORMULAIRE => $sousformulaire
 */
$sousformulaire = array(
    //'etablissement_unite',
);

