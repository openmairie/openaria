<?php
//$Id$ 
//gen openMairie le 07/07/2020 09:15

$DEBUG=0;
$serie=30;
$ent = __("administration_parametrage")." -> ".__("editions")." -> ".__("types");
$om_validite = true;
if(!isset($premier)) $premier='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
// FROM 
$table = DB_PREFIXE."courrier_type
    LEFT JOIN ".DB_PREFIXE."courrier_type_categorie 
        ON courrier_type.courrier_type_categorie=courrier_type_categorie.courrier_type_categorie 
    LEFT JOIN ".DB_PREFIXE."service 
        ON courrier_type.service=service.service ";
// SELECT 
$champAffiche = array(
    'courrier_type.courrier_type as "'.__("courrier_type").'"',
    'courrier_type.code as "'.__("code").'"',
    'courrier_type.libelle as "'.__("libelle").'"',
    'courrier_type_categorie.libelle as "'.__("courrier_type_categorie").'"',
    'service.libelle as "'.__("service").'"',
    );
// Spécificité des dates de validité
$displayed_fields_validite = array(
    'to_char(courrier_type.om_validite_debut ,\'DD/MM/YYYY\') as "'.__("om_validite_debut").'"',
    'to_char(courrier_type.om_validite_fin ,\'DD/MM/YYYY\') as "'.__("om_validite_fin").'"',
);
// On affiche les champs de date de validité uniquement lorsque le paramètre
// d'affichage des éléments expirés est activé
if (isset($_GET['valide']) && $_GET['valide'] === 'false') {
    $champAffiche = array_merge($champAffiche, $displayed_fields_validite);
}

//
$champNonAffiche = array(
    'courrier_type.description as "'.__("description").'"',
    'courrier_type.om_validite_debut as "'.__("om_validite_debut").'"',
    'courrier_type.om_validite_fin as "'.__("om_validite_fin").'"',
    );
//
$champRecherche = array(
    'courrier_type.courrier_type as "'.__("courrier_type").'"',
    'courrier_type.code as "'.__("code").'"',
    'courrier_type.libelle as "'.__("libelle").'"',
    'courrier_type_categorie.libelle as "'.__("courrier_type_categorie").'"',
    'service.libelle as "'.__("service").'"',
    );
$tri="ORDER BY courrier_type.libelle ASC NULLS LAST";
$edition="courrier_type";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
$selection = " WHERE ((courrier_type.om_validite_debut IS NULL AND (courrier_type.om_validite_fin IS NULL OR courrier_type.om_validite_fin > CURRENT_DATE)) OR (courrier_type.om_validite_debut <= CURRENT_DATE AND (courrier_type.om_validite_fin IS NULL OR courrier_type.om_validite_fin > CURRENT_DATE)))";
$where_om_validite = " WHERE ((courrier_type.om_validite_debut IS NULL AND (courrier_type.om_validite_fin IS NULL OR courrier_type.om_validite_fin > CURRENT_DATE)) OR (courrier_type.om_validite_debut <= CURRENT_DATE AND (courrier_type.om_validite_fin IS NULL OR courrier_type.om_validite_fin > CURRENT_DATE)))";
// Liste des clés étrangères avec leurs éventuelles surcharges
$foreign_keys_extended = array(
    "courrier_type_categorie" => array("courrier_type_categorie", ),
    "service" => array("service", ),
);
// Filtre listing sous formulaire - courrier_type_categorie
if (in_array($retourformulaire, $foreign_keys_extended["courrier_type_categorie"])) {
    $selection = " WHERE (courrier_type.courrier_type_categorie = ".intval($idxformulaire).")  AND ((courrier_type.om_validite_debut IS NULL AND (courrier_type.om_validite_fin IS NULL OR courrier_type.om_validite_fin > CURRENT_DATE)) OR (courrier_type.om_validite_debut <= CURRENT_DATE AND (courrier_type.om_validite_fin IS NULL OR courrier_type.om_validite_fin > CURRENT_DATE)))";
$where_om_validite = " AND ((courrier_type.om_validite_debut IS NULL AND (courrier_type.om_validite_fin IS NULL OR courrier_type.om_validite_fin > CURRENT_DATE)) OR (courrier_type.om_validite_debut <= CURRENT_DATE AND (courrier_type.om_validite_fin IS NULL OR courrier_type.om_validite_fin > CURRENT_DATE)))";
}
// Filtre listing sous formulaire - service
if (in_array($retourformulaire, $foreign_keys_extended["service"])) {
    $selection = " WHERE (courrier_type.service = ".intval($idxformulaire).")  AND ((courrier_type.om_validite_debut IS NULL AND (courrier_type.om_validite_fin IS NULL OR courrier_type.om_validite_fin > CURRENT_DATE)) OR (courrier_type.om_validite_debut <= CURRENT_DATE AND (courrier_type.om_validite_fin IS NULL OR courrier_type.om_validite_fin > CURRENT_DATE)))";
$where_om_validite = " AND ((courrier_type.om_validite_debut IS NULL AND (courrier_type.om_validite_fin IS NULL OR courrier_type.om_validite_fin > CURRENT_DATE)) OR (courrier_type.om_validite_debut <= CURRENT_DATE AND (courrier_type.om_validite_fin IS NULL OR courrier_type.om_validite_fin > CURRENT_DATE)))";
}
// Gestion OMValidité - Suppression du filtre si paramètre
if (isset($_GET["valide"]) and $_GET["valide"] == "false") {
    if (!isset($where_om_validite)
        or (isset($where_om_validite) and $where_om_validite == "")) {
        if (trim($selection) != "") {
            $selection = "";
        }
    } else {
        $selection = trim(str_replace($where_om_validite, "", $selection));
    }
}

/**
 * Gestion SOUSFORMULAIRE => $sousformulaire
 */
$sousformulaire = array(
    //'courrier',
    //'courrier_texte_type',
    //'modele_edition',
);

