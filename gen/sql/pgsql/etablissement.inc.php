<?php
//$Id$ 
//gen openMairie le 07/07/2020 09:15

$DEBUG=0;
$serie=30;
$ent = __("application")." -> ".__("etablissement");
$om_validite = true;
if(!isset($premier)) $premier='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
// FROM 
$table = DB_PREFIXE."etablissement
    LEFT JOIN ".DB_PREFIXE."reunion_avis as reunion_avis0 
        ON etablissement.acc_derniere_visite_avis=reunion_avis0.reunion_avis 
    LEFT JOIN ".DB_PREFIXE."acteur as acteur1 
        ON etablissement.acc_derniere_visite_technicien=acteur1.acteur 
    LEFT JOIN ".DB_PREFIXE."arrondissement 
        ON etablissement.adresse_arrondissement=arrondissement.arrondissement 
    LEFT JOIN ".DB_PREFIXE."voie 
        ON etablissement.adresse_voie=voie.voie 
    LEFT JOIN ".DB_PREFIXE."dossier_coordination 
        ON etablissement.dossier_coordination_periodique=dossier_coordination.dossier_coordination 
    LEFT JOIN ".DB_PREFIXE."etablissement_categorie 
        ON etablissement.etablissement_categorie=etablissement_categorie.etablissement_categorie 
    LEFT JOIN ".DB_PREFIXE."etablissement_etat 
        ON etablissement.etablissement_etat=etablissement_etat.etablissement_etat 
    LEFT JOIN ".DB_PREFIXE."etablissement_nature 
        ON etablissement.etablissement_nature=etablissement_nature.etablissement_nature 
    LEFT JOIN ".DB_PREFIXE."etablissement_statut_juridique 
        ON etablissement.etablissement_statut_juridique=etablissement_statut_juridique.etablissement_statut_juridique 
    LEFT JOIN ".DB_PREFIXE."etablissement_tutelle_adm 
        ON etablissement.etablissement_tutelle_adm=etablissement_tutelle_adm.etablissement_tutelle_adm 
    LEFT JOIN ".DB_PREFIXE."etablissement_type 
        ON etablissement.etablissement_type=etablissement_type.etablissement_type 
    LEFT JOIN ".DB_PREFIXE."autorite_competente as autorite_competente11 
        ON etablissement.si_autorite_competente_plan=autorite_competente11.autorite_competente 
    LEFT JOIN ".DB_PREFIXE."autorite_competente as autorite_competente12 
        ON etablissement.si_autorite_competente_visite=autorite_competente12.autorite_competente 
    LEFT JOIN ".DB_PREFIXE."reunion_avis as reunion_avis13 
        ON etablissement.si_dernier_plan_avis=reunion_avis13.reunion_avis 
    LEFT JOIN ".DB_PREFIXE."reunion_avis as reunion_avis14 
        ON etablissement.si_derniere_visite_avis=reunion_avis14.reunion_avis 
    LEFT JOIN ".DB_PREFIXE."acteur as acteur15 
        ON etablissement.si_derniere_visite_technicien=acteur15.acteur 
    LEFT JOIN ".DB_PREFIXE."analyses_type 
        ON etablissement.si_prochaine_visite_type=analyses_type.analyses_type 
    LEFT JOIN ".DB_PREFIXE."visite_duree 
        ON etablissement.si_visite_duree=visite_duree.visite_duree ";
// SELECT 
$champAffiche = array(
    'etablissement.etablissement as "'.__("etablissement").'"',
    'etablissement.code as "'.__("code").'"',
    'etablissement.libelle as "'.__("libelle").'"',
    'etablissement.adresse_numero as "'.__("adresse_numero").'"',
    'etablissement.adresse_numero2 as "'.__("adresse_numero2").'"',
    'voie.libelle as "'.__("adresse_voie").'"',
    'etablissement.adresse_complement as "'.__("adresse_complement").'"',
    'etablissement.lieu_dit as "'.__("lieu_dit").'"',
    'etablissement.boite_postale as "'.__("boite_postale").'"',
    'etablissement.adresse_cp as "'.__("adresse_cp").'"',
    'etablissement.adresse_ville as "'.__("adresse_ville").'"',
    'arrondissement.libelle as "'.__("adresse_arrondissement").'"',
    'etablissement.cedex as "'.__("cedex").'"',
    "case etablissement.npai when 't' then 'Oui' else 'Non' end as \"".__("npai")."\"",
    'etablissement.telephone as "'.__("telephone").'"',
    'etablissement.fax as "'.__("fax").'"',
    'etablissement_nature.nature as "'.__("etablissement_nature").'"',
    'etablissement.siret as "'.__("siret").'"',
    'etablissement.annee_de_construction as "'.__("annee_de_construction").'"',
    'etablissement_statut_juridique.libelle as "'.__("etablissement_statut_juridique").'"',
    'etablissement_tutelle_adm.libelle as "'.__("etablissement_tutelle_adm").'"',
    'etablissement_type.libelle as "'.__("etablissement_type").'"',
    'etablissement_categorie.libelle as "'.__("etablissement_categorie").'"',
    'etablissement_etat.statut_administratif as "'.__("etablissement_etat").'"',
    'to_char(etablissement.date_arrete_ouverture ,\'DD/MM/YYYY\') as "'.__("date_arrete_ouverture").'"',
    "case etablissement.autorite_police_encours when 't' then 'Oui' else 'Non' end as \"".__("autorite_police_encours")."\"",
    'etablissement.si_effectif_public as "'.__("si_effectif_public").'"',
    'etablissement.si_effectif_personnel as "'.__("si_effectif_personnel").'"',
    "case etablissement.si_locaux_sommeil when 't' then 'Oui' else 'Non' end as \"".__("si_locaux_sommeil")."\"",
    'etablissement.si_periodicite_visites as "'.__("si_periodicite_visites").'"',
    'to_char(etablissement.si_prochaine_visite_periodique_date_previsionnelle ,\'DD/MM/YYYY\') as "'.__("si_prochaine_visite_periodique_date_previsionnelle").'"',
    'visite_duree.libelle as "'.__("si_visite_duree").'"',
    'to_char(etablissement.si_derniere_visite_periodique_date ,\'DD/MM/YYYY\') as "'.__("si_derniere_visite_periodique_date").'"',
    'to_char(etablissement.si_derniere_visite_date ,\'DD/MM/YYYY\') as "'.__("si_derniere_visite_date").'"',
    'reunion_avis14.libelle as "'.__("si_derniere_visite_avis").'"',
    'acteur15.nom_prenom as "'.__("si_derniere_visite_technicien").'"',
    'to_char(etablissement.si_prochaine_visite_date ,\'DD/MM/YYYY\') as "'.__("si_prochaine_visite_date").'"',
    'analyses_type.libelle as "'.__("si_prochaine_visite_type").'"',
    'to_char(etablissement.acc_derniere_visite_date ,\'DD/MM/YYYY\') as "'.__("acc_derniere_visite_date").'"',
    'reunion_avis0.libelle as "'.__("acc_derniere_visite_avis").'"',
    'acteur1.nom_prenom as "'.__("acc_derniere_visite_technicien").'"',
    'autorite_competente12.libelle as "'.__("si_autorite_competente_visite").'"',
    'autorite_competente11.libelle as "'.__("si_autorite_competente_plan").'"',
    'reunion_avis13.libelle as "'.__("si_dernier_plan_avis").'"',
    'etablissement.si_type_ssi as "'.__("si_type_ssi").'"',
    "case etablissement.si_conformite_l16 when 't' then 'Oui' else 'Non' end as \"".__("si_conformite_l16")."\"",
    "case etablissement.si_alimentation_remplacement when 't' then 'Oui' else 'Non' end as \"".__("si_alimentation_remplacement")."\"",
    "case etablissement.si_service_securite when 't' then 'Oui' else 'Non' end as \"".__("si_service_securite")."\"",
    'etablissement.si_personnel_jour as "'.__("si_personnel_jour").'"',
    'etablissement.si_personnel_nuit as "'.__("si_personnel_nuit").'"',
    'dossier_coordination.libelle as "'.__("dossier_coordination_periodique").'"',
    "case etablissement.geolocalise when 't' then 'Oui' else 'Non' end as \"".__("geolocalise")."\"",
    'etablissement.geom_point as "'.__("geom_point").'"',
    'etablissement.geom_emprise as "'.__("geom_emprise").'"',
    );
// Spécificité des dates de validité
$displayed_fields_validite = array(
    'to_char(etablissement.om_validite_debut ,\'DD/MM/YYYY\') as "'.__("om_validite_debut").'"',
    'to_char(etablissement.om_validite_fin ,\'DD/MM/YYYY\') as "'.__("om_validite_fin").'"',
);
// On affiche les champs de date de validité uniquement lorsque le paramètre
// d'affichage des éléments expirés est activé
if (isset($_GET['valide']) && $_GET['valide'] === 'false') {
    $champAffiche = array_merge($champAffiche, $displayed_fields_validite);
}

//
$champNonAffiche = array(
    'etablissement.ref_patrimoine as "'.__("ref_patrimoine").'"',
    'etablissement.om_validite_debut as "'.__("om_validite_debut").'"',
    'etablissement.om_validite_fin as "'.__("om_validite_fin").'"',
    'etablissement.acc_consignes_om_html as "'.__("acc_consignes_om_html").'"',
    'etablissement.acc_descriptif_om_html as "'.__("acc_descriptif_om_html").'"',
    'etablissement.si_consignes_om_html as "'.__("si_consignes_om_html").'"',
    'etablissement.si_descriptif_om_html as "'.__("si_descriptif_om_html").'"',
    'etablissement.si_type_alarme as "'.__("si_type_alarme").'"',
    'etablissement.references_cadastrales as "'.__("references_cadastrales").'"',
    );
//
$champRecherche = array(
    'etablissement.etablissement as "'.__("etablissement").'"',
    'etablissement.code as "'.__("code").'"',
    'etablissement.libelle as "'.__("libelle").'"',
    'etablissement.adresse_numero as "'.__("adresse_numero").'"',
    'etablissement.adresse_numero2 as "'.__("adresse_numero2").'"',
    'voie.libelle as "'.__("adresse_voie").'"',
    'etablissement.adresse_complement as "'.__("adresse_complement").'"',
    'etablissement.lieu_dit as "'.__("lieu_dit").'"',
    'etablissement.boite_postale as "'.__("boite_postale").'"',
    'etablissement.adresse_cp as "'.__("adresse_cp").'"',
    'etablissement.adresse_ville as "'.__("adresse_ville").'"',
    'arrondissement.libelle as "'.__("adresse_arrondissement").'"',
    'etablissement.cedex as "'.__("cedex").'"',
    'etablissement.telephone as "'.__("telephone").'"',
    'etablissement.fax as "'.__("fax").'"',
    'etablissement_nature.nature as "'.__("etablissement_nature").'"',
    'etablissement.siret as "'.__("siret").'"',
    'etablissement.annee_de_construction as "'.__("annee_de_construction").'"',
    'etablissement_statut_juridique.libelle as "'.__("etablissement_statut_juridique").'"',
    'etablissement_tutelle_adm.libelle as "'.__("etablissement_tutelle_adm").'"',
    'etablissement_type.libelle as "'.__("etablissement_type").'"',
    'etablissement_categorie.libelle as "'.__("etablissement_categorie").'"',
    'etablissement_etat.statut_administratif as "'.__("etablissement_etat").'"',
    'etablissement.si_effectif_public as "'.__("si_effectif_public").'"',
    'etablissement.si_effectif_personnel as "'.__("si_effectif_personnel").'"',
    'etablissement.si_periodicite_visites as "'.__("si_periodicite_visites").'"',
    'visite_duree.libelle as "'.__("si_visite_duree").'"',
    'reunion_avis14.libelle as "'.__("si_derniere_visite_avis").'"',
    'acteur15.nom_prenom as "'.__("si_derniere_visite_technicien").'"',
    'analyses_type.libelle as "'.__("si_prochaine_visite_type").'"',
    'reunion_avis0.libelle as "'.__("acc_derniere_visite_avis").'"',
    'acteur1.nom_prenom as "'.__("acc_derniere_visite_technicien").'"',
    'autorite_competente12.libelle as "'.__("si_autorite_competente_visite").'"',
    'autorite_competente11.libelle as "'.__("si_autorite_competente_plan").'"',
    'reunion_avis13.libelle as "'.__("si_dernier_plan_avis").'"',
    'etablissement.si_type_ssi as "'.__("si_type_ssi").'"',
    'etablissement.si_personnel_jour as "'.__("si_personnel_jour").'"',
    'etablissement.si_personnel_nuit as "'.__("si_personnel_nuit").'"',
    'dossier_coordination.libelle as "'.__("dossier_coordination_periodique").'"',
    );
$tri="ORDER BY etablissement.libelle ASC NULLS LAST";
$edition="etablissement";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
$selection = " WHERE ((etablissement.om_validite_debut IS NULL AND (etablissement.om_validite_fin IS NULL OR etablissement.om_validite_fin > CURRENT_DATE)) OR (etablissement.om_validite_debut <= CURRENT_DATE AND (etablissement.om_validite_fin IS NULL OR etablissement.om_validite_fin > CURRENT_DATE)))";
$where_om_validite = " WHERE ((etablissement.om_validite_debut IS NULL AND (etablissement.om_validite_fin IS NULL OR etablissement.om_validite_fin > CURRENT_DATE)) OR (etablissement.om_validite_debut <= CURRENT_DATE AND (etablissement.om_validite_fin IS NULL OR etablissement.om_validite_fin > CURRENT_DATE)))";
// Liste des clés étrangères avec leurs éventuelles surcharges
$foreign_keys_extended = array(
    "reunion_avis" => array("reunion_avis", ),
    "acteur" => array("acteur", ),
    "arrondissement" => array("arrondissement", ),
    "voie" => array("voie", ),
    "dossier_coordination" => array("dossier_coordination", "dossier_coordination_nouveau", "dossier_coordination_a_qualifier", "dossier_coordination_a_cloturer", ),
    "etablissement_categorie" => array("etablissement_categorie", ),
    "etablissement_etat" => array("etablissement_etat", ),
    "etablissement_nature" => array("etablissement_nature", ),
    "etablissement_statut_juridique" => array("etablissement_statut_juridique", ),
    "etablissement_tutelle_adm" => array("etablissement_tutelle_adm", ),
    "etablissement_type" => array("etablissement_type", ),
    "autorite_competente" => array("autorite_competente", ),
    "analyses_type" => array("analyses_type", ),
    "visite_duree" => array("visite_duree", ),
);
// Filtre listing sous formulaire - reunion_avis
if (in_array($retourformulaire, $foreign_keys_extended["reunion_avis"])) {
    $selection = " WHERE (etablissement.acc_derniere_visite_avis = ".intval($idxformulaire)." OR etablissement.si_dernier_plan_avis = ".intval($idxformulaire)." OR etablissement.si_derniere_visite_avis = ".intval($idxformulaire).")  AND ((etablissement.om_validite_debut IS NULL AND (etablissement.om_validite_fin IS NULL OR etablissement.om_validite_fin > CURRENT_DATE)) OR (etablissement.om_validite_debut <= CURRENT_DATE AND (etablissement.om_validite_fin IS NULL OR etablissement.om_validite_fin > CURRENT_DATE)))";
$where_om_validite = " AND ((etablissement.om_validite_debut IS NULL AND (etablissement.om_validite_fin IS NULL OR etablissement.om_validite_fin > CURRENT_DATE)) OR (etablissement.om_validite_debut <= CURRENT_DATE AND (etablissement.om_validite_fin IS NULL OR etablissement.om_validite_fin > CURRENT_DATE)))";
}
// Filtre listing sous formulaire - acteur
if (in_array($retourformulaire, $foreign_keys_extended["acteur"])) {
    $selection = " WHERE (etablissement.acc_derniere_visite_technicien = ".intval($idxformulaire)." OR etablissement.si_derniere_visite_technicien = ".intval($idxformulaire).")  AND ((etablissement.om_validite_debut IS NULL AND (etablissement.om_validite_fin IS NULL OR etablissement.om_validite_fin > CURRENT_DATE)) OR (etablissement.om_validite_debut <= CURRENT_DATE AND (etablissement.om_validite_fin IS NULL OR etablissement.om_validite_fin > CURRENT_DATE)))";
$where_om_validite = " AND ((etablissement.om_validite_debut IS NULL AND (etablissement.om_validite_fin IS NULL OR etablissement.om_validite_fin > CURRENT_DATE)) OR (etablissement.om_validite_debut <= CURRENT_DATE AND (etablissement.om_validite_fin IS NULL OR etablissement.om_validite_fin > CURRENT_DATE)))";
}
// Filtre listing sous formulaire - arrondissement
if (in_array($retourformulaire, $foreign_keys_extended["arrondissement"])) {
    $selection = " WHERE (etablissement.adresse_arrondissement = ".intval($idxformulaire).")  AND ((etablissement.om_validite_debut IS NULL AND (etablissement.om_validite_fin IS NULL OR etablissement.om_validite_fin > CURRENT_DATE)) OR (etablissement.om_validite_debut <= CURRENT_DATE AND (etablissement.om_validite_fin IS NULL OR etablissement.om_validite_fin > CURRENT_DATE)))";
$where_om_validite = " AND ((etablissement.om_validite_debut IS NULL AND (etablissement.om_validite_fin IS NULL OR etablissement.om_validite_fin > CURRENT_DATE)) OR (etablissement.om_validite_debut <= CURRENT_DATE AND (etablissement.om_validite_fin IS NULL OR etablissement.om_validite_fin > CURRENT_DATE)))";
}
// Filtre listing sous formulaire - voie
if (in_array($retourformulaire, $foreign_keys_extended["voie"])) {
    $selection = " WHERE (etablissement.adresse_voie = ".intval($idxformulaire).")  AND ((etablissement.om_validite_debut IS NULL AND (etablissement.om_validite_fin IS NULL OR etablissement.om_validite_fin > CURRENT_DATE)) OR (etablissement.om_validite_debut <= CURRENT_DATE AND (etablissement.om_validite_fin IS NULL OR etablissement.om_validite_fin > CURRENT_DATE)))";
$where_om_validite = " AND ((etablissement.om_validite_debut IS NULL AND (etablissement.om_validite_fin IS NULL OR etablissement.om_validite_fin > CURRENT_DATE)) OR (etablissement.om_validite_debut <= CURRENT_DATE AND (etablissement.om_validite_fin IS NULL OR etablissement.om_validite_fin > CURRENT_DATE)))";
}
// Filtre listing sous formulaire - dossier_coordination
if (in_array($retourformulaire, $foreign_keys_extended["dossier_coordination"])) {
    $selection = " WHERE (etablissement.dossier_coordination_periodique = ".intval($idxformulaire).")  AND ((etablissement.om_validite_debut IS NULL AND (etablissement.om_validite_fin IS NULL OR etablissement.om_validite_fin > CURRENT_DATE)) OR (etablissement.om_validite_debut <= CURRENT_DATE AND (etablissement.om_validite_fin IS NULL OR etablissement.om_validite_fin > CURRENT_DATE)))";
$where_om_validite = " AND ((etablissement.om_validite_debut IS NULL AND (etablissement.om_validite_fin IS NULL OR etablissement.om_validite_fin > CURRENT_DATE)) OR (etablissement.om_validite_debut <= CURRENT_DATE AND (etablissement.om_validite_fin IS NULL OR etablissement.om_validite_fin > CURRENT_DATE)))";
}
// Filtre listing sous formulaire - etablissement_categorie
if (in_array($retourformulaire, $foreign_keys_extended["etablissement_categorie"])) {
    $selection = " WHERE (etablissement.etablissement_categorie = ".intval($idxformulaire).")  AND ((etablissement.om_validite_debut IS NULL AND (etablissement.om_validite_fin IS NULL OR etablissement.om_validite_fin > CURRENT_DATE)) OR (etablissement.om_validite_debut <= CURRENT_DATE AND (etablissement.om_validite_fin IS NULL OR etablissement.om_validite_fin > CURRENT_DATE)))";
$where_om_validite = " AND ((etablissement.om_validite_debut IS NULL AND (etablissement.om_validite_fin IS NULL OR etablissement.om_validite_fin > CURRENT_DATE)) OR (etablissement.om_validite_debut <= CURRENT_DATE AND (etablissement.om_validite_fin IS NULL OR etablissement.om_validite_fin > CURRENT_DATE)))";
}
// Filtre listing sous formulaire - etablissement_etat
if (in_array($retourformulaire, $foreign_keys_extended["etablissement_etat"])) {
    $selection = " WHERE (etablissement.etablissement_etat = ".intval($idxformulaire).")  AND ((etablissement.om_validite_debut IS NULL AND (etablissement.om_validite_fin IS NULL OR etablissement.om_validite_fin > CURRENT_DATE)) OR (etablissement.om_validite_debut <= CURRENT_DATE AND (etablissement.om_validite_fin IS NULL OR etablissement.om_validite_fin > CURRENT_DATE)))";
$where_om_validite = " AND ((etablissement.om_validite_debut IS NULL AND (etablissement.om_validite_fin IS NULL OR etablissement.om_validite_fin > CURRENT_DATE)) OR (etablissement.om_validite_debut <= CURRENT_DATE AND (etablissement.om_validite_fin IS NULL OR etablissement.om_validite_fin > CURRENT_DATE)))";
}
// Filtre listing sous formulaire - etablissement_nature
if (in_array($retourformulaire, $foreign_keys_extended["etablissement_nature"])) {
    $selection = " WHERE (etablissement.etablissement_nature = ".intval($idxformulaire).")  AND ((etablissement.om_validite_debut IS NULL AND (etablissement.om_validite_fin IS NULL OR etablissement.om_validite_fin > CURRENT_DATE)) OR (etablissement.om_validite_debut <= CURRENT_DATE AND (etablissement.om_validite_fin IS NULL OR etablissement.om_validite_fin > CURRENT_DATE)))";
$where_om_validite = " AND ((etablissement.om_validite_debut IS NULL AND (etablissement.om_validite_fin IS NULL OR etablissement.om_validite_fin > CURRENT_DATE)) OR (etablissement.om_validite_debut <= CURRENT_DATE AND (etablissement.om_validite_fin IS NULL OR etablissement.om_validite_fin > CURRENT_DATE)))";
}
// Filtre listing sous formulaire - etablissement_statut_juridique
if (in_array($retourformulaire, $foreign_keys_extended["etablissement_statut_juridique"])) {
    $selection = " WHERE (etablissement.etablissement_statut_juridique = ".intval($idxformulaire).")  AND ((etablissement.om_validite_debut IS NULL AND (etablissement.om_validite_fin IS NULL OR etablissement.om_validite_fin > CURRENT_DATE)) OR (etablissement.om_validite_debut <= CURRENT_DATE AND (etablissement.om_validite_fin IS NULL OR etablissement.om_validite_fin > CURRENT_DATE)))";
$where_om_validite = " AND ((etablissement.om_validite_debut IS NULL AND (etablissement.om_validite_fin IS NULL OR etablissement.om_validite_fin > CURRENT_DATE)) OR (etablissement.om_validite_debut <= CURRENT_DATE AND (etablissement.om_validite_fin IS NULL OR etablissement.om_validite_fin > CURRENT_DATE)))";
}
// Filtre listing sous formulaire - etablissement_tutelle_adm
if (in_array($retourformulaire, $foreign_keys_extended["etablissement_tutelle_adm"])) {
    $selection = " WHERE (etablissement.etablissement_tutelle_adm = ".intval($idxformulaire).")  AND ((etablissement.om_validite_debut IS NULL AND (etablissement.om_validite_fin IS NULL OR etablissement.om_validite_fin > CURRENT_DATE)) OR (etablissement.om_validite_debut <= CURRENT_DATE AND (etablissement.om_validite_fin IS NULL OR etablissement.om_validite_fin > CURRENT_DATE)))";
$where_om_validite = " AND ((etablissement.om_validite_debut IS NULL AND (etablissement.om_validite_fin IS NULL OR etablissement.om_validite_fin > CURRENT_DATE)) OR (etablissement.om_validite_debut <= CURRENT_DATE AND (etablissement.om_validite_fin IS NULL OR etablissement.om_validite_fin > CURRENT_DATE)))";
}
// Filtre listing sous formulaire - etablissement_type
if (in_array($retourformulaire, $foreign_keys_extended["etablissement_type"])) {
    $selection = " WHERE (etablissement.etablissement_type = ".intval($idxformulaire).")  AND ((etablissement.om_validite_debut IS NULL AND (etablissement.om_validite_fin IS NULL OR etablissement.om_validite_fin > CURRENT_DATE)) OR (etablissement.om_validite_debut <= CURRENT_DATE AND (etablissement.om_validite_fin IS NULL OR etablissement.om_validite_fin > CURRENT_DATE)))";
$where_om_validite = " AND ((etablissement.om_validite_debut IS NULL AND (etablissement.om_validite_fin IS NULL OR etablissement.om_validite_fin > CURRENT_DATE)) OR (etablissement.om_validite_debut <= CURRENT_DATE AND (etablissement.om_validite_fin IS NULL OR etablissement.om_validite_fin > CURRENT_DATE)))";
}
// Filtre listing sous formulaire - autorite_competente
if (in_array($retourformulaire, $foreign_keys_extended["autorite_competente"])) {
    $selection = " WHERE (etablissement.si_autorite_competente_plan = ".intval($idxformulaire)." OR etablissement.si_autorite_competente_visite = ".intval($idxformulaire).")  AND ((etablissement.om_validite_debut IS NULL AND (etablissement.om_validite_fin IS NULL OR etablissement.om_validite_fin > CURRENT_DATE)) OR (etablissement.om_validite_debut <= CURRENT_DATE AND (etablissement.om_validite_fin IS NULL OR etablissement.om_validite_fin > CURRENT_DATE)))";
$where_om_validite = " AND ((etablissement.om_validite_debut IS NULL AND (etablissement.om_validite_fin IS NULL OR etablissement.om_validite_fin > CURRENT_DATE)) OR (etablissement.om_validite_debut <= CURRENT_DATE AND (etablissement.om_validite_fin IS NULL OR etablissement.om_validite_fin > CURRENT_DATE)))";
}
// Filtre listing sous formulaire - analyses_type
if (in_array($retourformulaire, $foreign_keys_extended["analyses_type"])) {
    $selection = " WHERE (etablissement.si_prochaine_visite_type = ".intval($idxformulaire).")  AND ((etablissement.om_validite_debut IS NULL AND (etablissement.om_validite_fin IS NULL OR etablissement.om_validite_fin > CURRENT_DATE)) OR (etablissement.om_validite_debut <= CURRENT_DATE AND (etablissement.om_validite_fin IS NULL OR etablissement.om_validite_fin > CURRENT_DATE)))";
$where_om_validite = " AND ((etablissement.om_validite_debut IS NULL AND (etablissement.om_validite_fin IS NULL OR etablissement.om_validite_fin > CURRENT_DATE)) OR (etablissement.om_validite_debut <= CURRENT_DATE AND (etablissement.om_validite_fin IS NULL OR etablissement.om_validite_fin > CURRENT_DATE)))";
}
// Filtre listing sous formulaire - visite_duree
if (in_array($retourformulaire, $foreign_keys_extended["visite_duree"])) {
    $selection = " WHERE (etablissement.si_visite_duree = ".intval($idxformulaire).")  AND ((etablissement.om_validite_debut IS NULL AND (etablissement.om_validite_fin IS NULL OR etablissement.om_validite_fin > CURRENT_DATE)) OR (etablissement.om_validite_debut <= CURRENT_DATE AND (etablissement.om_validite_fin IS NULL OR etablissement.om_validite_fin > CURRENT_DATE)))";
$where_om_validite = " AND ((etablissement.om_validite_debut IS NULL AND (etablissement.om_validite_fin IS NULL OR etablissement.om_validite_fin > CURRENT_DATE)) OR (etablissement.om_validite_debut <= CURRENT_DATE AND (etablissement.om_validite_fin IS NULL OR etablissement.om_validite_fin > CURRENT_DATE)))";
}
// Gestion OMValidité - Suppression du filtre si paramètre
if (isset($_GET["valide"]) and $_GET["valide"] == "false") {
    if (!isset($where_om_validite)
        or (isset($where_om_validite) and $where_om_validite == "")) {
        if (trim($selection) != "") {
            $selection = "";
        }
    } else {
        $selection = trim(str_replace($where_om_validite, "", $selection));
    }
}

/**
 * Gestion SOUSFORMULAIRE => $sousformulaire
 */
$sousformulaire = array(
    'autorite_police',
    'contact',
    'courrier',
    'dossier_coordination',
    'etablissement_parcelle',
    'etablissement_unite',
    'lien_contrainte_etablissement',
    'lien_etablissement_e_type',
    'piece',
);

