<?php
//$Id$ 
//gen openMairie le 07/07/2020 09:15

$DEBUG=0;
$serie=30;
$ent = __("application")." -> ".__("etablissement_unite");
if(!isset($premier)) $premier='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
// FROM 
$table = DB_PREFIXE."etablissement_unite
    LEFT JOIN ".DB_PREFIXE."derogation_scda 
        ON etablissement_unite.acc_derogation_scda=derogation_scda.derogation_scda 
    LEFT JOIN ".DB_PREFIXE."dossier_instruction 
        ON etablissement_unite.dossier_instruction=dossier_instruction.dossier_instruction 
    LEFT JOIN ".DB_PREFIXE."etablissement 
        ON etablissement_unite.etablissement=etablissement.etablissement 
    LEFT JOIN ".DB_PREFIXE."etablissement_unite 
        ON etablissement_unite.etablissement_unite_lie=etablissement_unite.etablissement_unite ";
// SELECT 
$champAffiche = array(
    'etablissement_unite.etablissement_unite as "'.__("etablissement_unite").'"',
    'etablissement_unite.libelle as "'.__("libelle").'"',
    'etablissement.libelle as "'.__("etablissement").'"',
    "case etablissement_unite.acc_handicap_physique when 't' then 'Oui' else 'Non' end as \"".__("acc_handicap_physique")."\"",
    "case etablissement_unite.acc_handicap_auditif when 't' then 'Oui' else 'Non' end as \"".__("acc_handicap_auditif")."\"",
    "case etablissement_unite.acc_handicap_visuel when 't' then 'Oui' else 'Non' end as \"".__("acc_handicap_visuel")."\"",
    "case etablissement_unite.acc_handicap_mental when 't' then 'Oui' else 'Non' end as \"".__("acc_handicap_mental")."\"",
    'etablissement_unite.acc_places_stationnement_amenagees as "'.__("acc_places_stationnement_amenagees").'"',
    "case etablissement_unite.acc_ascenseur when 't' then 'Oui' else 'Non' end as \"".__("acc_ascenseur")."\"",
    "case etablissement_unite.acc_elevateur when 't' then 'Oui' else 'Non' end as \"".__("acc_elevateur")."\"",
    "case etablissement_unite.acc_boucle_magnetique when 't' then 'Oui' else 'Non' end as \"".__("acc_boucle_magnetique")."\"",
    "case etablissement_unite.acc_sanitaire when 't' then 'Oui' else 'Non' end as \"".__("acc_sanitaire")."\"",
    'etablissement_unite.acc_places_assises_public as "'.__("acc_places_assises_public").'"',
    'etablissement_unite.acc_chambres_amenagees as "'.__("acc_chambres_amenagees").'"',
    "case etablissement_unite.acc_douche when 't' then 'Oui' else 'Non' end as \"".__("acc_douche")."\"",
    'derogation_scda.libelle as "'.__("acc_derogation_scda").'"',
    'etablissement_unite.etat as "'.__("etat").'"',
    "case etablissement_unite.archive when 't' then 'Oui' else 'Non' end as \"".__("archive")."\"",
    'dossier_instruction.libelle as "'.__("dossier_instruction").'"',
    'etablissement_unite.libelle as "'.__("etablissement_unite_lie").'"',
    'to_char(etablissement_unite.adap_date_validation ,\'DD/MM/YYYY\') as "'.__("adap_date_validation").'"',
    'etablissement_unite.adap_duree_validite as "'.__("adap_duree_validite").'"',
    'etablissement_unite.adap_annee_debut_travaux as "'.__("adap_annee_debut_travaux").'"',
    'etablissement_unite.adap_annee_fin_travaux as "'.__("adap_annee_fin_travaux").'"',
    );
//
$champNonAffiche = array(
    'etablissement_unite.acc_notes_om_html as "'.__("acc_notes_om_html").'"',
    'etablissement_unite.acc_descriptif_ua_om_html as "'.__("acc_descriptif_ua_om_html").'"',
    );
//
$champRecherche = array(
    'etablissement_unite.etablissement_unite as "'.__("etablissement_unite").'"',
    'etablissement_unite.libelle as "'.__("libelle").'"',
    'etablissement.libelle as "'.__("etablissement").'"',
    'etablissement_unite.acc_places_stationnement_amenagees as "'.__("acc_places_stationnement_amenagees").'"',
    'etablissement_unite.acc_places_assises_public as "'.__("acc_places_assises_public").'"',
    'etablissement_unite.acc_chambres_amenagees as "'.__("acc_chambres_amenagees").'"',
    'derogation_scda.libelle as "'.__("acc_derogation_scda").'"',
    'etablissement_unite.etat as "'.__("etat").'"',
    'dossier_instruction.libelle as "'.__("dossier_instruction").'"',
    'etablissement_unite.libelle as "'.__("etablissement_unite_lie").'"',
    'etablissement_unite.adap_duree_validite as "'.__("adap_duree_validite").'"',
    'etablissement_unite.adap_annee_debut_travaux as "'.__("adap_annee_debut_travaux").'"',
    'etablissement_unite.adap_annee_fin_travaux as "'.__("adap_annee_fin_travaux").'"',
    );
$tri="ORDER BY etablissement_unite.libelle ASC NULLS LAST";
$edition="etablissement_unite";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
$selection = "";
// Liste des clés étrangères avec leurs éventuelles surcharges
$foreign_keys_extended = array(
    "derogation_scda" => array("derogation_scda", ),
    "dossier_instruction" => array("dossier_instruction", "dossier_instruction_mes_plans", "dossier_instruction_mes_visites", "dossier_instruction_tous_plans", "dossier_instruction_tous_visites", "dossier_instruction_a_qualifier", "dossier_instruction_a_affecter", ),
    "etablissement" => array("etablissement", "etablissement_referentiel_erp", "etablissement_tous", ),
    "etablissement_unite" => array("etablissement_unite", "etablissement_unite__contexte_di_analyse__ua_valide_sur_etab", "etablissement_unite__contexte_di_analyse__ua_en_analyse", "etablissement_unite__contexte_etab__ua_valide", "etablissement_unite__contexte_etab__ua_archive", "etablissement_unite__contexte_etab__ua_enprojet", ),
);
// Filtre listing sous formulaire - derogation_scda
if (in_array($retourformulaire, $foreign_keys_extended["derogation_scda"])) {
    $selection = " WHERE (etablissement_unite.acc_derogation_scda = ".intval($idxformulaire).") ";
}
// Filtre listing sous formulaire - dossier_instruction
if (in_array($retourformulaire, $foreign_keys_extended["dossier_instruction"])) {
    $selection = " WHERE (etablissement_unite.dossier_instruction = ".intval($idxformulaire).") ";
}
// Filtre listing sous formulaire - etablissement
if (in_array($retourformulaire, $foreign_keys_extended["etablissement"])) {
    $selection = " WHERE (etablissement_unite.etablissement = ".intval($idxformulaire).") ";
}
// Filtre listing sous formulaire - etablissement_unite
if (in_array($retourformulaire, $foreign_keys_extended["etablissement_unite"])) {
    $selection = " WHERE (etablissement_unite.etablissement_unite_lie = ".intval($idxformulaire).") ";
}

/**
 * Gestion SOUSFORMULAIRE => $sousformulaire
 */
$sousformulaire = array(
    'etablissement_unite',
);

