<?php
//$Id$ 
//gen openMairie le 07/07/2020 09:15

$DEBUG=0;
$serie=30;
$ent = __("application")." -> ".__("lien_dossier_coordination_type_analyses_type");
if(!isset($premier)) $premier='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
// FROM 
$table = DB_PREFIXE."lien_dossier_coordination_type_analyses_type
    LEFT JOIN ".DB_PREFIXE."analyses_type 
        ON lien_dossier_coordination_type_analyses_type.analyses_type=analyses_type.analyses_type 
    LEFT JOIN ".DB_PREFIXE."dossier_coordination_type 
        ON lien_dossier_coordination_type_analyses_type.dossier_coordination_type=dossier_coordination_type.dossier_coordination_type ";
// SELECT 
$champAffiche = array(
    'lien_dossier_coordination_type_analyses_type.lien_dossier_coordination_type_analyses_type as "'.__("lien_dossier_coordination_type_analyses_type").'"',
    'dossier_coordination_type.libelle as "'.__("dossier_coordination_type").'"',
    'analyses_type.libelle as "'.__("analyses_type").'"',
    'lien_dossier_coordination_type_analyses_type.service as "'.__("service").'"',
    );
//
$champNonAffiche = array(
    );
//
$champRecherche = array(
    'lien_dossier_coordination_type_analyses_type.lien_dossier_coordination_type_analyses_type as "'.__("lien_dossier_coordination_type_analyses_type").'"',
    'dossier_coordination_type.libelle as "'.__("dossier_coordination_type").'"',
    'analyses_type.libelle as "'.__("analyses_type").'"',
    'lien_dossier_coordination_type_analyses_type.service as "'.__("service").'"',
    );
$tri="ORDER BY dossier_coordination_type.libelle ASC NULLS LAST";
$edition="lien_dossier_coordination_type_analyses_type";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
$selection = "";
// Liste des clés étrangères avec leurs éventuelles surcharges
$foreign_keys_extended = array(
    "analyses_type" => array("analyses_type", ),
    "dossier_coordination_type" => array("dossier_coordination_type", ),
);
// Filtre listing sous formulaire - analyses_type
if (in_array($retourformulaire, $foreign_keys_extended["analyses_type"])) {
    $selection = " WHERE (lien_dossier_coordination_type_analyses_type.analyses_type = ".intval($idxformulaire).") ";
}
// Filtre listing sous formulaire - dossier_coordination_type
if (in_array($retourformulaire, $foreign_keys_extended["dossier_coordination_type"])) {
    $selection = " WHERE (lien_dossier_coordination_type_analyses_type.dossier_coordination_type = ".intval($idxformulaire).") ";
}

