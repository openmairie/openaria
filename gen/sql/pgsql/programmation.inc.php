<?php
//$Id$ 
//gen openMairie le 07/07/2020 09:15

$DEBUG=0;
$serie=30;
$ent = __("application")." -> ".__("programmation");
if(!isset($premier)) $premier='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
// FROM 
$table = DB_PREFIXE."programmation
    LEFT JOIN ".DB_PREFIXE."programmation_etat 
        ON programmation.programmation_etat=programmation_etat.programmation_etat 
    LEFT JOIN ".DB_PREFIXE."service 
        ON programmation.service=service.service ";
// SELECT 
$champAffiche = array(
    'programmation.programmation as "'.__("programmation").'"',
    'programmation.annee as "'.__("annee").'"',
    'programmation.numero_semaine as "'.__("numero_semaine").'"',
    'programmation.version as "'.__("version").'"',
    'programmation_etat.libelle as "'.__("programmation_etat").'"',
    'to_char(programmation.date_modification ,\'DD/MM/YYYY\') as "'.__("date_modification").'"',
    'programmation.convocation_exploitants as "'.__("convocation_exploitants").'"',
    'to_char(programmation.date_envoi_ce ,\'DD/MM/YYYY\') as "'.__("date_envoi_ce").'"',
    'programmation.convocation_membres as "'.__("convocation_membres").'"',
    'to_char(programmation.date_envoi_cm ,\'DD/MM/YYYY\') as "'.__("date_envoi_cm").'"',
    'programmation.semaine_annee as "'.__("semaine_annee").'"',
    'service.libelle as "'.__("service").'"',
    "case programmation.planifier_nouveau when 't' then 'Oui' else 'Non' end as \"".__("planifier_nouveau")."\"",
    );
//
$champNonAffiche = array(
    );
//
$champRecherche = array(
    'programmation.programmation as "'.__("programmation").'"',
    'programmation.annee as "'.__("annee").'"',
    'programmation.numero_semaine as "'.__("numero_semaine").'"',
    'programmation.version as "'.__("version").'"',
    'programmation_etat.libelle as "'.__("programmation_etat").'"',
    'programmation.convocation_exploitants as "'.__("convocation_exploitants").'"',
    'programmation.convocation_membres as "'.__("convocation_membres").'"',
    'programmation.semaine_annee as "'.__("semaine_annee").'"',
    'service.libelle as "'.__("service").'"',
    );
$tri="ORDER BY programmation.annee ASC NULLS LAST";
$edition="programmation";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
$selection = "";
// Liste des clés étrangères avec leurs éventuelles surcharges
$foreign_keys_extended = array(
    "programmation_etat" => array("programmation_etat", ),
    "service" => array("service", ),
);
// Filtre listing sous formulaire - programmation_etat
if (in_array($retourformulaire, $foreign_keys_extended["programmation_etat"])) {
    $selection = " WHERE (programmation.programmation_etat = ".intval($idxformulaire).") ";
}
// Filtre listing sous formulaire - service
if (in_array($retourformulaire, $foreign_keys_extended["service"])) {
    $selection = " WHERE (programmation.service = ".intval($idxformulaire).") ";
}

/**
 * Gestion SOUSFORMULAIRE => $sousformulaire
 */
$sousformulaire = array(
    'visite',
);

