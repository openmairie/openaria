<?php
//$Id$ 
//gen openMairie le 07/07/2020 09:15

$DEBUG=0;
$serie=30;
$ent = __("administration_parametrage")." -> ".__("autorites de police")." -> ".__("motifs");
$om_validite = true;
if(!isset($premier)) $premier='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
// FROM 
$table = DB_PREFIXE."autorite_police_motif
    LEFT JOIN ".DB_PREFIXE."service 
        ON autorite_police_motif.service=service.service ";
// SELECT 
$champAffiche = array(
    'autorite_police_motif.autorite_police_motif as "'.__("autorite_police_motif").'"',
    'autorite_police_motif.code as "'.__("code").'"',
    'autorite_police_motif.libelle as "'.__("libelle").'"',
    'service.libelle as "'.__("service").'"',
    );
// Spécificité des dates de validité
$displayed_fields_validite = array(
    'to_char(autorite_police_motif.om_validite_debut ,\'DD/MM/YYYY\') as "'.__("om_validite_debut").'"',
    'to_char(autorite_police_motif.om_validite_fin ,\'DD/MM/YYYY\') as "'.__("om_validite_fin").'"',
);
// On affiche les champs de date de validité uniquement lorsque le paramètre
// d'affichage des éléments expirés est activé
if (isset($_GET['valide']) && $_GET['valide'] === 'false') {
    $champAffiche = array_merge($champAffiche, $displayed_fields_validite);
}

//
$champNonAffiche = array(
    'autorite_police_motif.description as "'.__("description").'"',
    'autorite_police_motif.om_validite_debut as "'.__("om_validite_debut").'"',
    'autorite_police_motif.om_validite_fin as "'.__("om_validite_fin").'"',
    );
//
$champRecherche = array(
    'autorite_police_motif.autorite_police_motif as "'.__("autorite_police_motif").'"',
    'autorite_police_motif.code as "'.__("code").'"',
    'autorite_police_motif.libelle as "'.__("libelle").'"',
    'service.libelle as "'.__("service").'"',
    );
$tri="ORDER BY autorite_police_motif.libelle ASC NULLS LAST";
$edition="autorite_police_motif";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
$selection = " WHERE ((autorite_police_motif.om_validite_debut IS NULL AND (autorite_police_motif.om_validite_fin IS NULL OR autorite_police_motif.om_validite_fin > CURRENT_DATE)) OR (autorite_police_motif.om_validite_debut <= CURRENT_DATE AND (autorite_police_motif.om_validite_fin IS NULL OR autorite_police_motif.om_validite_fin > CURRENT_DATE)))";
$where_om_validite = " WHERE ((autorite_police_motif.om_validite_debut IS NULL AND (autorite_police_motif.om_validite_fin IS NULL OR autorite_police_motif.om_validite_fin > CURRENT_DATE)) OR (autorite_police_motif.om_validite_debut <= CURRENT_DATE AND (autorite_police_motif.om_validite_fin IS NULL OR autorite_police_motif.om_validite_fin > CURRENT_DATE)))";
// Liste des clés étrangères avec leurs éventuelles surcharges
$foreign_keys_extended = array(
    "service" => array("service", ),
);
// Filtre listing sous formulaire - service
if (in_array($retourformulaire, $foreign_keys_extended["service"])) {
    $selection = " WHERE (autorite_police_motif.service = ".intval($idxformulaire).")  AND ((autorite_police_motif.om_validite_debut IS NULL AND (autorite_police_motif.om_validite_fin IS NULL OR autorite_police_motif.om_validite_fin > CURRENT_DATE)) OR (autorite_police_motif.om_validite_debut <= CURRENT_DATE AND (autorite_police_motif.om_validite_fin IS NULL OR autorite_police_motif.om_validite_fin > CURRENT_DATE)))";
$where_om_validite = " AND ((autorite_police_motif.om_validite_debut IS NULL AND (autorite_police_motif.om_validite_fin IS NULL OR autorite_police_motif.om_validite_fin > CURRENT_DATE)) OR (autorite_police_motif.om_validite_debut <= CURRENT_DATE AND (autorite_police_motif.om_validite_fin IS NULL OR autorite_police_motif.om_validite_fin > CURRENT_DATE)))";
}
// Gestion OMValidité - Suppression du filtre si paramètre
if (isset($_GET["valide"]) and $_GET["valide"] == "false") {
    if (!isset($where_om_validite)
        or (isset($where_om_validite) and $where_om_validite == "")) {
        if (trim($selection) != "") {
            $selection = "";
        }
    } else {
        $selection = trim(str_replace($where_om_validite, "", $selection));
    }
}

/**
 * Gestion SOUSFORMULAIRE => $sousformulaire
 */
$sousformulaire = array(
    //'autorite_police',
);

