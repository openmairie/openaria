<?php
//$Id$ 
//gen openMairie le 07/07/2020 09:15

$DEBUG=0;
$serie=30;
$ent = __("administration_parametrage")." -> ".__("analyses")." -> ".__("prescriptions");
$om_validite = true;
if(!isset($premier)) $premier='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
// FROM 
$table = DB_PREFIXE."prescription_reglementaire
    LEFT JOIN ".DB_PREFIXE."service 
        ON prescription_reglementaire.service=service.service ";
// SELECT 
$champAffiche = array(
    'prescription_reglementaire.prescription_reglementaire as "'.__("prescription_reglementaire").'"',
    'service.libelle as "'.__("service").'"',
    'prescription_reglementaire.tete_de_chapitre1 as "'.__("tete_de_chapitre1").'"',
    'prescription_reglementaire.tete_de_chapitre2 as "'.__("tete_de_chapitre2").'"',
    'prescription_reglementaire.libelle as "'.__("libelle").'"',
    "case prescription_reglementaire.defavorable when 't' then 'Oui' else 'Non' end as \"".__("defavorable")."\"",
    );
// Spécificité des dates de validité
$displayed_fields_validite = array(
    'to_char(prescription_reglementaire.om_validite_debut ,\'DD/MM/YYYY\') as "'.__("om_validite_debut").'"',
    'to_char(prescription_reglementaire.om_validite_fin ,\'DD/MM/YYYY\') as "'.__("om_validite_fin").'"',
);
// On affiche les champs de date de validité uniquement lorsque le paramètre
// d'affichage des éléments expirés est activé
if (isset($_GET['valide']) && $_GET['valide'] === 'false') {
    $champAffiche = array_merge($champAffiche, $displayed_fields_validite);
}

//
$champNonAffiche = array(
    'prescription_reglementaire.description_pr_om_html as "'.__("description_pr_om_html").'"',
    'prescription_reglementaire.om_validite_debut as "'.__("om_validite_debut").'"',
    'prescription_reglementaire.om_validite_fin as "'.__("om_validite_fin").'"',
    );
//
$champRecherche = array(
    'prescription_reglementaire.prescription_reglementaire as "'.__("prescription_reglementaire").'"',
    'service.libelle as "'.__("service").'"',
    'prescription_reglementaire.tete_de_chapitre1 as "'.__("tete_de_chapitre1").'"',
    'prescription_reglementaire.tete_de_chapitre2 as "'.__("tete_de_chapitre2").'"',
    'prescription_reglementaire.libelle as "'.__("libelle").'"',
    );
$tri="ORDER BY prescription_reglementaire.libelle ASC NULLS LAST";
$edition="prescription_reglementaire";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
$selection = " WHERE ((prescription_reglementaire.om_validite_debut IS NULL AND (prescription_reglementaire.om_validite_fin IS NULL OR prescription_reglementaire.om_validite_fin > CURRENT_DATE)) OR (prescription_reglementaire.om_validite_debut <= CURRENT_DATE AND (prescription_reglementaire.om_validite_fin IS NULL OR prescription_reglementaire.om_validite_fin > CURRENT_DATE)))";
$where_om_validite = " WHERE ((prescription_reglementaire.om_validite_debut IS NULL AND (prescription_reglementaire.om_validite_fin IS NULL OR prescription_reglementaire.om_validite_fin > CURRENT_DATE)) OR (prescription_reglementaire.om_validite_debut <= CURRENT_DATE AND (prescription_reglementaire.om_validite_fin IS NULL OR prescription_reglementaire.om_validite_fin > CURRENT_DATE)))";
// Liste des clés étrangères avec leurs éventuelles surcharges
$foreign_keys_extended = array(
    "service" => array("service", ),
);
// Filtre listing sous formulaire - service
if (in_array($retourformulaire, $foreign_keys_extended["service"])) {
    $selection = " WHERE (prescription_reglementaire.service = ".intval($idxformulaire).")  AND ((prescription_reglementaire.om_validite_debut IS NULL AND (prescription_reglementaire.om_validite_fin IS NULL OR prescription_reglementaire.om_validite_fin > CURRENT_DATE)) OR (prescription_reglementaire.om_validite_debut <= CURRENT_DATE AND (prescription_reglementaire.om_validite_fin IS NULL OR prescription_reglementaire.om_validite_fin > CURRENT_DATE)))";
$where_om_validite = " AND ((prescription_reglementaire.om_validite_debut IS NULL AND (prescription_reglementaire.om_validite_fin IS NULL OR prescription_reglementaire.om_validite_fin > CURRENT_DATE)) OR (prescription_reglementaire.om_validite_debut <= CURRENT_DATE AND (prescription_reglementaire.om_validite_fin IS NULL OR prescription_reglementaire.om_validite_fin > CURRENT_DATE)))";
}
// Gestion OMValidité - Suppression du filtre si paramètre
if (isset($_GET["valide"]) and $_GET["valide"] == "false") {
    if (!isset($where_om_validite)
        or (isset($where_om_validite) and $where_om_validite == "")) {
        if (trim($selection) != "") {
            $selection = "";
        }
    } else {
        $selection = trim(str_replace($where_om_validite, "", $selection));
    }
}

/**
 * Gestion SOUSFORMULAIRE => $sousformulaire
 */
$sousformulaire = array(
    //'lien_prescription_reglementaire_etablissement_categorie',
    //'lien_prescription_reglementaire_etablissement_type',
    //'prescription',
    //'prescription_specifique',
);

