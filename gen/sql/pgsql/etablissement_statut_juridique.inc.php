<?php
//$Id$ 
//gen openMairie le 07/07/2020 09:15

$DEBUG=0;
$serie=30;
$ent = __("administration_parametrage")." -> ".__("etablissements")." -> ".__("statuts juridiques");
$om_validite = true;
if(!isset($premier)) $premier='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
// FROM 
$table = DB_PREFIXE."etablissement_statut_juridique";
// SELECT 
$champAffiche = array(
    'etablissement_statut_juridique.etablissement_statut_juridique as "'.__("etablissement_statut_juridique").'"',
    'etablissement_statut_juridique.libelle as "'.__("libelle").'"',
    'etablissement_statut_juridique.code as "'.__("code").'"',
    );
// Spécificité des dates de validité
$displayed_fields_validite = array(
    'to_char(etablissement_statut_juridique.om_validite_debut ,\'DD/MM/YYYY\') as "'.__("om_validite_debut").'"',
    'to_char(etablissement_statut_juridique.om_validite_fin ,\'DD/MM/YYYY\') as "'.__("om_validite_fin").'"',
);
// On affiche les champs de date de validité uniquement lorsque le paramètre
// d'affichage des éléments expirés est activé
if (isset($_GET['valide']) && $_GET['valide'] === 'false') {
    $champAffiche = array_merge($champAffiche, $displayed_fields_validite);
}

//
$champNonAffiche = array(
    'etablissement_statut_juridique.om_validite_debut as "'.__("om_validite_debut").'"',
    'etablissement_statut_juridique.om_validite_fin as "'.__("om_validite_fin").'"',
    );
//
$champRecherche = array(
    'etablissement_statut_juridique.etablissement_statut_juridique as "'.__("etablissement_statut_juridique").'"',
    'etablissement_statut_juridique.libelle as "'.__("libelle").'"',
    'etablissement_statut_juridique.code as "'.__("code").'"',
    );
$tri="ORDER BY etablissement_statut_juridique.libelle ASC NULLS LAST";
$edition="etablissement_statut_juridique";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
$selection = " WHERE ((etablissement_statut_juridique.om_validite_debut IS NULL AND (etablissement_statut_juridique.om_validite_fin IS NULL OR etablissement_statut_juridique.om_validite_fin > CURRENT_DATE)) OR (etablissement_statut_juridique.om_validite_debut <= CURRENT_DATE AND (etablissement_statut_juridique.om_validite_fin IS NULL OR etablissement_statut_juridique.om_validite_fin > CURRENT_DATE)))";
$where_om_validite = " WHERE ((etablissement_statut_juridique.om_validite_debut IS NULL AND (etablissement_statut_juridique.om_validite_fin IS NULL OR etablissement_statut_juridique.om_validite_fin > CURRENT_DATE)) OR (etablissement_statut_juridique.om_validite_debut <= CURRENT_DATE AND (etablissement_statut_juridique.om_validite_fin IS NULL OR etablissement_statut_juridique.om_validite_fin > CURRENT_DATE)))";
// Gestion OMValidité - Suppression du filtre si paramètre
if (isset($_GET["valide"]) and $_GET["valide"] == "false") {
    if (!isset($where_om_validite)
        or (isset($where_om_validite) and $where_om_validite == "")) {
        if (trim($selection) != "") {
            $selection = "";
        }
    } else {
        $selection = trim(str_replace($where_om_validite, "", $selection));
    }
}

/**
 * Gestion SOUSFORMULAIRE => $sousformulaire
 */
$sousformulaire = array(
    //'etablissement',
);

