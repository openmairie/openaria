<?php
//$Id$ 
//gen openMairie le 07/07/2020 09:15

$DEBUG=0;
$serie=30;
$ent = __("application")." -> ".__("lien_dossier_coordination_contact");
if(!isset($premier)) $premier='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
// FROM 
$table = DB_PREFIXE."lien_dossier_coordination_contact
    LEFT JOIN ".DB_PREFIXE."contact 
        ON lien_dossier_coordination_contact.contact=contact.contact 
    LEFT JOIN ".DB_PREFIXE."dossier_coordination 
        ON lien_dossier_coordination_contact.dossier_coordination=dossier_coordination.dossier_coordination ";
// SELECT 
$champAffiche = array(
    'lien_dossier_coordination_contact.lien_dossier_coordination_contact as "'.__("lien_dossier_coordination_contact").'"',
    'dossier_coordination.libelle as "'.__("dossier_coordination").'"',
    'contact.etablissement as "'.__("contact").'"',
    );
//
$champNonAffiche = array(
    );
//
$champRecherche = array(
    'lien_dossier_coordination_contact.lien_dossier_coordination_contact as "'.__("lien_dossier_coordination_contact").'"',
    'dossier_coordination.libelle as "'.__("dossier_coordination").'"',
    'contact.etablissement as "'.__("contact").'"',
    );
$tri="ORDER BY dossier_coordination.libelle ASC NULLS LAST";
$edition="lien_dossier_coordination_contact";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
$selection = "";
// Liste des clés étrangères avec leurs éventuelles surcharges
$foreign_keys_extended = array(
    "contact" => array("contact", "contact_institutionnel", "contact_contexte_dossier_coordination", ),
    "dossier_coordination" => array("dossier_coordination", "dossier_coordination_nouveau", "dossier_coordination_a_qualifier", "dossier_coordination_a_cloturer", ),
);
// Filtre listing sous formulaire - contact
if (in_array($retourformulaire, $foreign_keys_extended["contact"])) {
    $selection = " WHERE (lien_dossier_coordination_contact.contact = ".intval($idxformulaire).") ";
}
// Filtre listing sous formulaire - dossier_coordination
if (in_array($retourformulaire, $foreign_keys_extended["dossier_coordination"])) {
    $selection = " WHERE (lien_dossier_coordination_contact.dossier_coordination = ".intval($idxformulaire).") ";
}

