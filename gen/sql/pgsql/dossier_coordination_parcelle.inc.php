<?php
//$Id$ 
//gen openMairie le 07/07/2020 09:15

$DEBUG=0;
$serie=30;
$ent = __("application")." -> ".__("dossier_coordination_parcelle");
if(!isset($premier)) $premier='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
// FROM 
$table = DB_PREFIXE."dossier_coordination_parcelle
    LEFT JOIN ".DB_PREFIXE."dossier_coordination 
        ON dossier_coordination_parcelle.dossier_coordination=dossier_coordination.dossier_coordination ";
// SELECT 
$champAffiche = array(
    'dossier_coordination_parcelle.dossier_coordination_parcelle as "'.__("dossier_coordination_parcelle").'"',
    'dossier_coordination.libelle as "'.__("dossier_coordination").'"',
    'dossier_coordination_parcelle.ref_cadastre as "'.__("ref_cadastre").'"',
    );
//
$champNonAffiche = array(
    );
//
$champRecherche = array(
    'dossier_coordination_parcelle.dossier_coordination_parcelle as "'.__("dossier_coordination_parcelle").'"',
    'dossier_coordination.libelle as "'.__("dossier_coordination").'"',
    'dossier_coordination_parcelle.ref_cadastre as "'.__("ref_cadastre").'"',
    );
$tri="ORDER BY dossier_coordination.libelle ASC NULLS LAST";
$edition="dossier_coordination_parcelle";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
$selection = "";
// Liste des clés étrangères avec leurs éventuelles surcharges
$foreign_keys_extended = array(
    "dossier_coordination" => array("dossier_coordination", "dossier_coordination_nouveau", "dossier_coordination_a_qualifier", "dossier_coordination_a_cloturer", ),
);
// Filtre listing sous formulaire - dossier_coordination
if (in_array($retourformulaire, $foreign_keys_extended["dossier_coordination"])) {
    $selection = " WHERE (dossier_coordination_parcelle.dossier_coordination = ".intval($idxformulaire).") ";
}

