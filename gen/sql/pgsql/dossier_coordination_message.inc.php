<?php
//$Id$ 
//gen openMairie le 07/07/2020 09:15

$DEBUG=0;
$serie=30;
$ent = __("application")." -> ".__("dossier_coordination_message");
if(!isset($premier)) $premier='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
// FROM 
$table = DB_PREFIXE."dossier_coordination_message
    LEFT JOIN ".DB_PREFIXE."dossier_coordination 
        ON dossier_coordination_message.dossier_coordination=dossier_coordination.dossier_coordination ";
// SELECT 
$champAffiche = array(
    'dossier_coordination_message.dossier_coordination_message as "'.__("dossier_coordination_message").'"',
    'dossier_coordination_message.categorie as "'.__("categorie").'"',
    'dossier_coordination.libelle as "'.__("dossier_coordination").'"',
    'dossier_coordination_message.type as "'.__("type").'"',
    'dossier_coordination_message.emetteur as "'.__("emetteur").'"',
    'dossier_coordination_message.date_emission as "'.__("date_emission").'"',
    "case dossier_coordination_message.si_cadre_lu when 't' then 'Oui' else 'Non' end as \"".__("si_cadre_lu")."\"",
    "case dossier_coordination_message.si_technicien_lu when 't' then 'Oui' else 'Non' end as \"".__("si_technicien_lu")."\"",
    'dossier_coordination_message.si_mode_lecture as "'.__("si_mode_lecture").'"',
    "case dossier_coordination_message.acc_cadre_lu when 't' then 'Oui' else 'Non' end as \"".__("acc_cadre_lu")."\"",
    "case dossier_coordination_message.acc_technicien_lu when 't' then 'Oui' else 'Non' end as \"".__("acc_technicien_lu")."\"",
    'dossier_coordination_message.acc_mode_lecture as "'.__("acc_mode_lecture").'"',
    );
//
$champNonAffiche = array(
    'dossier_coordination_message.contenu as "'.__("contenu").'"',
    'dossier_coordination_message.contenu_json as "'.__("contenu_json").'"',
    );
//
$champRecherche = array(
    'dossier_coordination_message.dossier_coordination_message as "'.__("dossier_coordination_message").'"',
    'dossier_coordination_message.categorie as "'.__("categorie").'"',
    'dossier_coordination.libelle as "'.__("dossier_coordination").'"',
    'dossier_coordination_message.type as "'.__("type").'"',
    'dossier_coordination_message.emetteur as "'.__("emetteur").'"',
    'dossier_coordination_message.si_mode_lecture as "'.__("si_mode_lecture").'"',
    'dossier_coordination_message.acc_mode_lecture as "'.__("acc_mode_lecture").'"',
    );
$tri="ORDER BY dossier_coordination_message.categorie ASC NULLS LAST";
$edition="dossier_coordination_message";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
$selection = "";
// Liste des clés étrangères avec leurs éventuelles surcharges
$foreign_keys_extended = array(
    "dossier_coordination" => array("dossier_coordination", "dossier_coordination_nouveau", "dossier_coordination_a_qualifier", "dossier_coordination_a_cloturer", ),
);
// Filtre listing sous formulaire - dossier_coordination
if (in_array($retourformulaire, $foreign_keys_extended["dossier_coordination"])) {
    $selection = " WHERE (dossier_coordination_message.dossier_coordination = ".intval($idxformulaire).") ";
}

