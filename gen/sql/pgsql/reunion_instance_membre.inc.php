<?php
//$Id$ 
//gen openMairie le 07/07/2020 09:15

$DEBUG=0;
$serie=30;
$ent = __("application")." -> ".__("reunion_instance_membre");
$om_validite = true;
if(!isset($premier)) $premier='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
// FROM 
$table = DB_PREFIXE."reunion_instance_membre
    LEFT JOIN ".DB_PREFIXE."reunion_instance 
        ON reunion_instance_membre.reunion_instance=reunion_instance.reunion_instance ";
// SELECT 
$champAffiche = array(
    'reunion_instance_membre.reunion_instance_membre as "'.__("reunion_instance_membre").'"',
    'reunion_instance_membre.membre as "'.__("membre").'"',
    'reunion_instance.libelle as "'.__("reunion_instance").'"',
    );
// Spécificité des dates de validité
$displayed_fields_validite = array(
    'to_char(reunion_instance_membre.om_validite_debut ,\'DD/MM/YYYY\') as "'.__("om_validite_debut").'"',
    'to_char(reunion_instance_membre.om_validite_fin ,\'DD/MM/YYYY\') as "'.__("om_validite_fin").'"',
);
// On affiche les champs de date de validité uniquement lorsque le paramètre
// d'affichage des éléments expirés est activé
if (isset($_GET['valide']) && $_GET['valide'] === 'false') {
    $champAffiche = array_merge($champAffiche, $displayed_fields_validite);
}

//
$champNonAffiche = array(
    'reunion_instance_membre.description as "'.__("description").'"',
    'reunion_instance_membre.om_validite_debut as "'.__("om_validite_debut").'"',
    'reunion_instance_membre.om_validite_fin as "'.__("om_validite_fin").'"',
    );
//
$champRecherche = array(
    'reunion_instance_membre.reunion_instance_membre as "'.__("reunion_instance_membre").'"',
    'reunion_instance_membre.membre as "'.__("membre").'"',
    'reunion_instance.libelle as "'.__("reunion_instance").'"',
    );
$tri="ORDER BY reunion_instance_membre.membre ASC NULLS LAST";
$edition="reunion_instance_membre";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
$selection = " WHERE ((reunion_instance_membre.om_validite_debut IS NULL AND (reunion_instance_membre.om_validite_fin IS NULL OR reunion_instance_membre.om_validite_fin > CURRENT_DATE)) OR (reunion_instance_membre.om_validite_debut <= CURRENT_DATE AND (reunion_instance_membre.om_validite_fin IS NULL OR reunion_instance_membre.om_validite_fin > CURRENT_DATE)))";
$where_om_validite = " WHERE ((reunion_instance_membre.om_validite_debut IS NULL AND (reunion_instance_membre.om_validite_fin IS NULL OR reunion_instance_membre.om_validite_fin > CURRENT_DATE)) OR (reunion_instance_membre.om_validite_debut <= CURRENT_DATE AND (reunion_instance_membre.om_validite_fin IS NULL OR reunion_instance_membre.om_validite_fin > CURRENT_DATE)))";
// Liste des clés étrangères avec leurs éventuelles surcharges
$foreign_keys_extended = array(
    "reunion_instance" => array("reunion_instance", ),
);
// Filtre listing sous formulaire - reunion_instance
if (in_array($retourformulaire, $foreign_keys_extended["reunion_instance"])) {
    $selection = " WHERE (reunion_instance_membre.reunion_instance = ".intval($idxformulaire).")  AND ((reunion_instance_membre.om_validite_debut IS NULL AND (reunion_instance_membre.om_validite_fin IS NULL OR reunion_instance_membre.om_validite_fin > CURRENT_DATE)) OR (reunion_instance_membre.om_validite_debut <= CURRENT_DATE AND (reunion_instance_membre.om_validite_fin IS NULL OR reunion_instance_membre.om_validite_fin > CURRENT_DATE)))";
$where_om_validite = " AND ((reunion_instance_membre.om_validite_debut IS NULL AND (reunion_instance_membre.om_validite_fin IS NULL OR reunion_instance_membre.om_validite_fin > CURRENT_DATE)) OR (reunion_instance_membre.om_validite_debut <= CURRENT_DATE AND (reunion_instance_membre.om_validite_fin IS NULL OR reunion_instance_membre.om_validite_fin > CURRENT_DATE)))";
}
// Gestion OMValidité - Suppression du filtre si paramètre
if (isset($_GET["valide"]) and $_GET["valide"] == "false") {
    if (!isset($where_om_validite)
        or (isset($where_om_validite) and $where_om_validite == "")) {
        if (trim($selection) != "") {
            $selection = "";
        }
    } else {
        $selection = trim(str_replace($where_om_validite, "", $selection));
    }
}

/**
 * Gestion SOUSFORMULAIRE => $sousformulaire
 */
$sousformulaire = array(
    'lien_reunion_r_instance_r_i_membre',
);

