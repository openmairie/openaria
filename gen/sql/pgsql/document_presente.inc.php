<?php
//$Id$ 
//gen openMairie le 07/07/2020 09:15

$DEBUG=0;
$serie=30;
$ent = __("administration_parametrage")." -> ".__("analyses")." -> ".__("documents presentes");
$om_validite = true;
if(!isset($premier)) $premier='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
// FROM 
$table = DB_PREFIXE."document_presente
    LEFT JOIN ".DB_PREFIXE."service 
        ON document_presente.service=service.service ";
// SELECT 
$champAffiche = array(
    'document_presente.document_presente as "'.__("document_presente").'"',
    'service.libelle as "'.__("service").'"',
    'document_presente.libelle as "'.__("libelle").'"',
    );
// Spécificité des dates de validité
$displayed_fields_validite = array(
    'to_char(document_presente.om_validite_debut ,\'DD/MM/YYYY\') as "'.__("om_validite_debut").'"',
    'to_char(document_presente.om_validite_fin ,\'DD/MM/YYYY\') as "'.__("om_validite_fin").'"',
);
// On affiche les champs de date de validité uniquement lorsque le paramètre
// d'affichage des éléments expirés est activé
if (isset($_GET['valide']) && $_GET['valide'] === 'false') {
    $champAffiche = array_merge($champAffiche, $displayed_fields_validite);
}

//
$champNonAffiche = array(
    'document_presente.description_om_html as "'.__("description_om_html").'"',
    'document_presente.om_validite_debut as "'.__("om_validite_debut").'"',
    'document_presente.om_validite_fin as "'.__("om_validite_fin").'"',
    );
//
$champRecherche = array(
    'document_presente.document_presente as "'.__("document_presente").'"',
    'service.libelle as "'.__("service").'"',
    'document_presente.libelle as "'.__("libelle").'"',
    );
$tri="ORDER BY document_presente.libelle ASC NULLS LAST";
$edition="document_presente";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
$selection = " WHERE ((document_presente.om_validite_debut IS NULL AND (document_presente.om_validite_fin IS NULL OR document_presente.om_validite_fin > CURRENT_DATE)) OR (document_presente.om_validite_debut <= CURRENT_DATE AND (document_presente.om_validite_fin IS NULL OR document_presente.om_validite_fin > CURRENT_DATE)))";
$where_om_validite = " WHERE ((document_presente.om_validite_debut IS NULL AND (document_presente.om_validite_fin IS NULL OR document_presente.om_validite_fin > CURRENT_DATE)) OR (document_presente.om_validite_debut <= CURRENT_DATE AND (document_presente.om_validite_fin IS NULL OR document_presente.om_validite_fin > CURRENT_DATE)))";
// Liste des clés étrangères avec leurs éventuelles surcharges
$foreign_keys_extended = array(
    "service" => array("service", ),
);
// Filtre listing sous formulaire - service
if (in_array($retourformulaire, $foreign_keys_extended["service"])) {
    $selection = " WHERE (document_presente.service = ".intval($idxformulaire).")  AND ((document_presente.om_validite_debut IS NULL AND (document_presente.om_validite_fin IS NULL OR document_presente.om_validite_fin > CURRENT_DATE)) OR (document_presente.om_validite_debut <= CURRENT_DATE AND (document_presente.om_validite_fin IS NULL OR document_presente.om_validite_fin > CURRENT_DATE)))";
$where_om_validite = " AND ((document_presente.om_validite_debut IS NULL AND (document_presente.om_validite_fin IS NULL OR document_presente.om_validite_fin > CURRENT_DATE)) OR (document_presente.om_validite_debut <= CURRENT_DATE AND (document_presente.om_validite_fin IS NULL OR document_presente.om_validite_fin > CURRENT_DATE)))";
}
// Gestion OMValidité - Suppression du filtre si paramètre
if (isset($_GET["valide"]) and $_GET["valide"] == "false") {
    if (!isset($where_om_validite)
        or (isset($where_om_validite) and $where_om_validite == "")) {
        if (trim($selection) != "") {
            $selection = "";
        }
    } else {
        $selection = trim(str_replace($where_om_validite, "", $selection));
    }
}

