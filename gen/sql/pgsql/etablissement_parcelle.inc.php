<?php
//$Id$ 
//gen openMairie le 07/07/2020 09:15

$DEBUG=0;
$serie=30;
$ent = __("application")." -> ".__("etablissement_parcelle");
if(!isset($premier)) $premier='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
// FROM 
$table = DB_PREFIXE."etablissement_parcelle
    LEFT JOIN ".DB_PREFIXE."etablissement 
        ON etablissement_parcelle.etablissement=etablissement.etablissement ";
// SELECT 
$champAffiche = array(
    'etablissement_parcelle.etablissement_parcelle as "'.__("etablissement_parcelle").'"',
    'etablissement.libelle as "'.__("etablissement").'"',
    'etablissement_parcelle.ref_cadastre as "'.__("ref_cadastre").'"',
    );
//
$champNonAffiche = array(
    );
//
$champRecherche = array(
    'etablissement_parcelle.etablissement_parcelle as "'.__("etablissement_parcelle").'"',
    'etablissement.libelle as "'.__("etablissement").'"',
    'etablissement_parcelle.ref_cadastre as "'.__("ref_cadastre").'"',
    );
$tri="ORDER BY etablissement.libelle ASC NULLS LAST";
$edition="etablissement_parcelle";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
$selection = "";
// Liste des clés étrangères avec leurs éventuelles surcharges
$foreign_keys_extended = array(
    "etablissement" => array("etablissement", "etablissement_referentiel_erp", "etablissement_tous", ),
);
// Filtre listing sous formulaire - etablissement
if (in_array($retourformulaire, $foreign_keys_extended["etablissement"])) {
    $selection = " WHERE (etablissement_parcelle.etablissement = ".intval($idxformulaire).") ";
}

