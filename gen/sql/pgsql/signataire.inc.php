<?php
//$Id$ 
//gen openMairie le 07/07/2020 09:15

$DEBUG=0;
$serie=30;
$ent = __("administration_parametrage")." -> ".__("documents generes")." -> ".__("signataires");
$om_validite = true;
if(!isset($premier)) $premier='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
// FROM 
$table = DB_PREFIXE."signataire
    LEFT JOIN ".DB_PREFIXE."contact_civilite 
        ON signataire.civilite=contact_civilite.contact_civilite 
    LEFT JOIN ".DB_PREFIXE."signataire_qualite 
        ON signataire.signataire_qualite=signataire_qualite.signataire_qualite ";
// SELECT 
$champAffiche = array(
    'signataire.signataire as "'.__("signataire").'"',
    'signataire.nom as "'.__("nom").'"',
    'signataire.prenom as "'.__("prenom").'"',
    'contact_civilite.libelle as "'.__("civilite").'"',
    'signataire_qualite.libelle as "'.__("signataire_qualite").'"',
    "case signataire.defaut when 't' then 'Oui' else 'Non' end as \"".__("defaut")."\"",
    );
// Spécificité des dates de validité
$displayed_fields_validite = array(
    'to_char(signataire.om_validite_debut ,\'DD/MM/YYYY\') as "'.__("om_validite_debut").'"',
    'to_char(signataire.om_validite_fin ,\'DD/MM/YYYY\') as "'.__("om_validite_fin").'"',
);
// On affiche les champs de date de validité uniquement lorsque le paramètre
// d'affichage des éléments expirés est activé
if (isset($_GET['valide']) && $_GET['valide'] === 'false') {
    $champAffiche = array_merge($champAffiche, $displayed_fields_validite);
}

//
$champNonAffiche = array(
    'signataire.signature as "'.__("signature").'"',
    'signataire.om_validite_debut as "'.__("om_validite_debut").'"',
    'signataire.om_validite_fin as "'.__("om_validite_fin").'"',
    );
//
$champRecherche = array(
    'signataire.signataire as "'.__("signataire").'"',
    'signataire.nom as "'.__("nom").'"',
    'signataire.prenom as "'.__("prenom").'"',
    'contact_civilite.libelle as "'.__("civilite").'"',
    'signataire_qualite.libelle as "'.__("signataire_qualite").'"',
    );
$tri="ORDER BY signataire.nom ASC NULLS LAST";
$edition="signataire";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
$selection = " WHERE ((signataire.om_validite_debut IS NULL AND (signataire.om_validite_fin IS NULL OR signataire.om_validite_fin > CURRENT_DATE)) OR (signataire.om_validite_debut <= CURRENT_DATE AND (signataire.om_validite_fin IS NULL OR signataire.om_validite_fin > CURRENT_DATE)))";
$where_om_validite = " WHERE ((signataire.om_validite_debut IS NULL AND (signataire.om_validite_fin IS NULL OR signataire.om_validite_fin > CURRENT_DATE)) OR (signataire.om_validite_debut <= CURRENT_DATE AND (signataire.om_validite_fin IS NULL OR signataire.om_validite_fin > CURRENT_DATE)))";
// Liste des clés étrangères avec leurs éventuelles surcharges
$foreign_keys_extended = array(
    "contact_civilite" => array("contact_civilite", ),
    "signataire_qualite" => array("signataire_qualite", ),
);
// Filtre listing sous formulaire - contact_civilite
if (in_array($retourformulaire, $foreign_keys_extended["contact_civilite"])) {
    $selection = " WHERE (signataire.civilite = ".intval($idxformulaire).")  AND ((signataire.om_validite_debut IS NULL AND (signataire.om_validite_fin IS NULL OR signataire.om_validite_fin > CURRENT_DATE)) OR (signataire.om_validite_debut <= CURRENT_DATE AND (signataire.om_validite_fin IS NULL OR signataire.om_validite_fin > CURRENT_DATE)))";
$where_om_validite = " AND ((signataire.om_validite_debut IS NULL AND (signataire.om_validite_fin IS NULL OR signataire.om_validite_fin > CURRENT_DATE)) OR (signataire.om_validite_debut <= CURRENT_DATE AND (signataire.om_validite_fin IS NULL OR signataire.om_validite_fin > CURRENT_DATE)))";
}
// Filtre listing sous formulaire - signataire_qualite
if (in_array($retourformulaire, $foreign_keys_extended["signataire_qualite"])) {
    $selection = " WHERE (signataire.signataire_qualite = ".intval($idxformulaire).")  AND ((signataire.om_validite_debut IS NULL AND (signataire.om_validite_fin IS NULL OR signataire.om_validite_fin > CURRENT_DATE)) OR (signataire.om_validite_debut <= CURRENT_DATE AND (signataire.om_validite_fin IS NULL OR signataire.om_validite_fin > CURRENT_DATE)))";
$where_om_validite = " AND ((signataire.om_validite_debut IS NULL AND (signataire.om_validite_fin IS NULL OR signataire.om_validite_fin > CURRENT_DATE)) OR (signataire.om_validite_debut <= CURRENT_DATE AND (signataire.om_validite_fin IS NULL OR signataire.om_validite_fin > CURRENT_DATE)))";
}
// Gestion OMValidité - Suppression du filtre si paramètre
if (isset($_GET["valide"]) and $_GET["valide"] == "false") {
    if (!isset($where_om_validite)
        or (isset($where_om_validite) and $where_om_validite == "")) {
        if (trim($selection) != "") {
            $selection = "";
        }
    } else {
        $selection = trim(str_replace($where_om_validite, "", $selection));
    }
}

/**
 * Gestion SOUSFORMULAIRE => $sousformulaire
 */
$sousformulaire = array(
    //'courrier',
    //'proces_verbal',
);

