<?php
//$Id$ 
//gen openMairie le 07/07/2020 09:15

$DEBUG=0;
$serie=30;
$ent = __("administration_parametrage")." -> ".__("analyses")." -> ".__("essais realises");
$om_validite = true;
if(!isset($premier)) $premier='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
// FROM 
$table = DB_PREFIXE."essai_realise
    LEFT JOIN ".DB_PREFIXE."service 
        ON essai_realise.service=service.service ";
// SELECT 
$champAffiche = array(
    'essai_realise.essai_realise as "'.__("essai_realise").'"',
    'service.libelle as "'.__("service").'"',
    'essai_realise.code as "'.__("code").'"',
    'essai_realise.libelle as "'.__("libelle").'"',
    );
// Spécificité des dates de validité
$displayed_fields_validite = array(
    'to_char(essai_realise.om_validite_debut ,\'DD/MM/YYYY\') as "'.__("om_validite_debut").'"',
    'to_char(essai_realise.om_validite_fin ,\'DD/MM/YYYY\') as "'.__("om_validite_fin").'"',
);
// On affiche les champs de date de validité uniquement lorsque le paramètre
// d'affichage des éléments expirés est activé
if (isset($_GET['valide']) && $_GET['valide'] === 'false') {
    $champAffiche = array_merge($champAffiche, $displayed_fields_validite);
}

//
$champNonAffiche = array(
    'essai_realise.description as "'.__("description").'"',
    'essai_realise.om_validite_debut as "'.__("om_validite_debut").'"',
    'essai_realise.om_validite_fin as "'.__("om_validite_fin").'"',
    );
//
$champRecherche = array(
    'essai_realise.essai_realise as "'.__("essai_realise").'"',
    'service.libelle as "'.__("service").'"',
    'essai_realise.code as "'.__("code").'"',
    'essai_realise.libelle as "'.__("libelle").'"',
    );
$tri="ORDER BY essai_realise.libelle ASC NULLS LAST";
$edition="essai_realise";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
$selection = " WHERE ((essai_realise.om_validite_debut IS NULL AND (essai_realise.om_validite_fin IS NULL OR essai_realise.om_validite_fin > CURRENT_DATE)) OR (essai_realise.om_validite_debut <= CURRENT_DATE AND (essai_realise.om_validite_fin IS NULL OR essai_realise.om_validite_fin > CURRENT_DATE)))";
$where_om_validite = " WHERE ((essai_realise.om_validite_debut IS NULL AND (essai_realise.om_validite_fin IS NULL OR essai_realise.om_validite_fin > CURRENT_DATE)) OR (essai_realise.om_validite_debut <= CURRENT_DATE AND (essai_realise.om_validite_fin IS NULL OR essai_realise.om_validite_fin > CURRENT_DATE)))";
// Liste des clés étrangères avec leurs éventuelles surcharges
$foreign_keys_extended = array(
    "service" => array("service", ),
);
// Filtre listing sous formulaire - service
if (in_array($retourformulaire, $foreign_keys_extended["service"])) {
    $selection = " WHERE (essai_realise.service = ".intval($idxformulaire).")  AND ((essai_realise.om_validite_debut IS NULL AND (essai_realise.om_validite_fin IS NULL OR essai_realise.om_validite_fin > CURRENT_DATE)) OR (essai_realise.om_validite_debut <= CURRENT_DATE AND (essai_realise.om_validite_fin IS NULL OR essai_realise.om_validite_fin > CURRENT_DATE)))";
$where_om_validite = " AND ((essai_realise.om_validite_debut IS NULL AND (essai_realise.om_validite_fin IS NULL OR essai_realise.om_validite_fin > CURRENT_DATE)) OR (essai_realise.om_validite_debut <= CURRENT_DATE AND (essai_realise.om_validite_fin IS NULL OR essai_realise.om_validite_fin > CURRENT_DATE)))";
}
// Gestion OMValidité - Suppression du filtre si paramètre
if (isset($_GET["valide"]) and $_GET["valide"] == "false") {
    if (!isset($where_om_validite)
        or (isset($where_om_validite) and $where_om_validite == "")) {
        if (trim($selection) != "") {
            $selection = "";
        }
    } else {
        $selection = trim(str_replace($where_om_validite, "", $selection));
    }
}

/**
 * Gestion SOUSFORMULAIRE => $sousformulaire
 */
$sousformulaire = array(
    //'lien_essai_realise_analyses',
);

