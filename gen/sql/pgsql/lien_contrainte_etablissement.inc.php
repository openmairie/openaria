<?php
//$Id$ 
//gen openMairie le 07/07/2020 09:15

$DEBUG=0;
$serie=30;
$ent = __("application")." -> ".__("lien_contrainte_etablissement");
if(!isset($premier)) $premier='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
// FROM 
$table = DB_PREFIXE."lien_contrainte_etablissement
    LEFT JOIN ".DB_PREFIXE."contrainte 
        ON lien_contrainte_etablissement.contrainte=contrainte.contrainte 
    LEFT JOIN ".DB_PREFIXE."etablissement 
        ON lien_contrainte_etablissement.etablissement=etablissement.etablissement ";
// SELECT 
$champAffiche = array(
    'lien_contrainte_etablissement.lien_contrainte_etablissement as "'.__("lien_contrainte_etablissement").'"',
    'etablissement.libelle as "'.__("etablissement").'"',
    'contrainte.libelle as "'.__("contrainte").'"',
    "case lien_contrainte_etablissement.recuperee when 't' then 'Oui' else 'Non' end as \"".__("recuperee")."\"",
    );
//
$champNonAffiche = array(
    'lien_contrainte_etablissement.texte_complete as "'.__("texte_complete").'"',
    );
//
$champRecherche = array(
    'lien_contrainte_etablissement.lien_contrainte_etablissement as "'.__("lien_contrainte_etablissement").'"',
    'etablissement.libelle as "'.__("etablissement").'"',
    'contrainte.libelle as "'.__("contrainte").'"',
    );
$tri="ORDER BY etablissement.libelle ASC NULLS LAST";
$edition="lien_contrainte_etablissement";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
$selection = "";
// Liste des clés étrangères avec leurs éventuelles surcharges
$foreign_keys_extended = array(
    "contrainte" => array("contrainte", ),
    "etablissement" => array("etablissement", "etablissement_referentiel_erp", "etablissement_tous", ),
);
// Filtre listing sous formulaire - contrainte
if (in_array($retourformulaire, $foreign_keys_extended["contrainte"])) {
    $selection = " WHERE (lien_contrainte_etablissement.contrainte = ".intval($idxformulaire).") ";
}
// Filtre listing sous formulaire - etablissement
if (in_array($retourformulaire, $foreign_keys_extended["etablissement"])) {
    $selection = " WHERE (lien_contrainte_etablissement.etablissement = ".intval($idxformulaire).") ";
}

