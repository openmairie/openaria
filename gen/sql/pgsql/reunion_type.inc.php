<?php
//$Id$ 
//gen openMairie le 21/07/2021 17:03

$DEBUG=0;
$serie=30;
$ent = __("administration_parametrage")." -> ".__("reunions")." -> ".__("types");
$om_validite = true;
if(!isset($premier)) $premier='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
// FROM 
$table = DB_PREFIXE."reunion_type
    LEFT JOIN ".DB_PREFIXE."modele_edition as modele_edition0 
        ON reunion_type.modele_compte_rendu_global=modele_edition0.modele_edition 
    LEFT JOIN ".DB_PREFIXE."modele_edition as modele_edition1 
        ON reunion_type.modele_compte_rendu_specifique=modele_edition1.modele_edition 
    LEFT JOIN ".DB_PREFIXE."modele_edition as modele_edition2 
        ON reunion_type.modele_feuille_presence=modele_edition2.modele_edition 
    LEFT JOIN ".DB_PREFIXE."modele_edition as modele_edition3 
        ON reunion_type.modele_ordre_du_jour=modele_edition3.modele_edition 
    LEFT JOIN ".DB_PREFIXE."reunion_instance 
        ON reunion_type.president=reunion_instance.reunion_instance 
    LEFT JOIN ".DB_PREFIXE."service 
        ON reunion_type.service=service.service ";
// SELECT 
$champAffiche = array(
    'reunion_type.reunion_type as "'.__("reunion_type").'"',
    'reunion_type.code as "'.__("code").'"',
    'reunion_type.libelle as "'.__("libelle").'"',
    'reunion_type.lieu_adresse_ligne1 as "'.__("lieu_adresse_ligne1").'"',
    'reunion_type.lieu_adresse_ligne2 as "'.__("lieu_adresse_ligne2").'"',
    'reunion_type.lieu_salle as "'.__("lieu_salle").'"',
    'reunion_type.heure as "'.__("heure").'"',
    );
// Spécificité des dates de validité
$displayed_fields_validite = array(
    'to_char(reunion_type.om_validite_debut ,\'DD/MM/YYYY\') as "'.__("om_validite_debut").'"',
    'to_char(reunion_type.om_validite_fin ,\'DD/MM/YYYY\') as "'.__("om_validite_fin").'"',
);
// On affiche les champs de date de validité uniquement lorsque le paramètre
// d'affichage des éléments expirés est activé
if (isset($_GET['valide']) && $_GET['valide'] === 'false') {
    $champAffiche = array_merge($champAffiche, $displayed_fields_validite);
}

//
$champNonAffiche = array(
    'reunion_type.listes_de_diffusion as "'.__("listes_de_diffusion").'"',
    'reunion_type.modele_courriel_convoquer as "'.__("modele_courriel_convoquer").'"',
    'reunion_type.modele_courriel_cloturer as "'.__("modele_courriel_cloturer").'"',
    'reunion_type.commission as "'.__("commission").'"',
    'reunion_type.president as "'.__("president").'"',
    'reunion_type.modele_ordre_du_jour as "'.__("modele_ordre_du_jour").'"',
    'reunion_type.modele_compte_rendu_global as "'.__("modele_compte_rendu_global").'"',
    'reunion_type.modele_compte_rendu_specifique as "'.__("modele_compte_rendu_specifique").'"',
    'reunion_type.modele_feuille_presence as "'.__("modele_feuille_presence").'"',
    'reunion_type.service as "'.__("service").'"',
    'reunion_type.om_validite_debut as "'.__("om_validite_debut").'"',
    'reunion_type.om_validite_fin as "'.__("om_validite_fin").'"',
    'reunion_type.participants as "'.__("participants").'"',
    'reunion_type.numerotation_mode as "'.__("numerotation_mode").'"',
    );
//
$champRecherche = array(
    'reunion_type.reunion_type as "'.__("reunion_type").'"',
    'reunion_type.code as "'.__("code").'"',
    'reunion_type.libelle as "'.__("libelle").'"',
    'reunion_type.lieu_adresse_ligne1 as "'.__("lieu_adresse_ligne1").'"',
    'reunion_type.lieu_adresse_ligne2 as "'.__("lieu_adresse_ligne2").'"',
    'reunion_type.lieu_salle as "'.__("lieu_salle").'"',
    'reunion_type.heure as "'.__("heure").'"',
    );
$tri="ORDER BY reunion_type.libelle ASC NULLS LAST";
$edition="reunion_type";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
$selection = " WHERE ((reunion_type.om_validite_debut IS NULL AND (reunion_type.om_validite_fin IS NULL OR reunion_type.om_validite_fin > CURRENT_DATE)) OR (reunion_type.om_validite_debut <= CURRENT_DATE AND (reunion_type.om_validite_fin IS NULL OR reunion_type.om_validite_fin > CURRENT_DATE)))";
$where_om_validite = " WHERE ((reunion_type.om_validite_debut IS NULL AND (reunion_type.om_validite_fin IS NULL OR reunion_type.om_validite_fin > CURRENT_DATE)) OR (reunion_type.om_validite_debut <= CURRENT_DATE AND (reunion_type.om_validite_fin IS NULL OR reunion_type.om_validite_fin > CURRENT_DATE)))";
// Liste des clés étrangères avec leurs éventuelles surcharges
$foreign_keys_extended = array(
    "modele_edition" => array("modele_edition", ),
    "reunion_instance" => array("reunion_instance", ),
    "service" => array("service", ),
);
// Filtre listing sous formulaire - modele_edition
if (in_array($retourformulaire, $foreign_keys_extended["modele_edition"])) {
    $selection = " WHERE (reunion_type.modele_compte_rendu_global = ".intval($idxformulaire)." OR reunion_type.modele_compte_rendu_specifique = ".intval($idxformulaire)." OR reunion_type.modele_feuille_presence = ".intval($idxformulaire)." OR reunion_type.modele_ordre_du_jour = ".intval($idxformulaire).")  AND ((reunion_type.om_validite_debut IS NULL AND (reunion_type.om_validite_fin IS NULL OR reunion_type.om_validite_fin > CURRENT_DATE)) OR (reunion_type.om_validite_debut <= CURRENT_DATE AND (reunion_type.om_validite_fin IS NULL OR reunion_type.om_validite_fin > CURRENT_DATE)))";
$where_om_validite = " AND ((reunion_type.om_validite_debut IS NULL AND (reunion_type.om_validite_fin IS NULL OR reunion_type.om_validite_fin > CURRENT_DATE)) OR (reunion_type.om_validite_debut <= CURRENT_DATE AND (reunion_type.om_validite_fin IS NULL OR reunion_type.om_validite_fin > CURRENT_DATE)))";
}
// Filtre listing sous formulaire - reunion_instance
if (in_array($retourformulaire, $foreign_keys_extended["reunion_instance"])) {
    $selection = " WHERE (reunion_type.president = ".intval($idxformulaire).")  AND ((reunion_type.om_validite_debut IS NULL AND (reunion_type.om_validite_fin IS NULL OR reunion_type.om_validite_fin > CURRENT_DATE)) OR (reunion_type.om_validite_debut <= CURRENT_DATE AND (reunion_type.om_validite_fin IS NULL OR reunion_type.om_validite_fin > CURRENT_DATE)))";
$where_om_validite = " AND ((reunion_type.om_validite_debut IS NULL AND (reunion_type.om_validite_fin IS NULL OR reunion_type.om_validite_fin > CURRENT_DATE)) OR (reunion_type.om_validite_debut <= CURRENT_DATE AND (reunion_type.om_validite_fin IS NULL OR reunion_type.om_validite_fin > CURRENT_DATE)))";
}
// Filtre listing sous formulaire - service
if (in_array($retourformulaire, $foreign_keys_extended["service"])) {
    $selection = " WHERE (reunion_type.service = ".intval($idxformulaire).")  AND ((reunion_type.om_validite_debut IS NULL AND (reunion_type.om_validite_fin IS NULL OR reunion_type.om_validite_fin > CURRENT_DATE)) OR (reunion_type.om_validite_debut <= CURRENT_DATE AND (reunion_type.om_validite_fin IS NULL OR reunion_type.om_validite_fin > CURRENT_DATE)))";
$where_om_validite = " AND ((reunion_type.om_validite_debut IS NULL AND (reunion_type.om_validite_fin IS NULL OR reunion_type.om_validite_fin > CURRENT_DATE)) OR (reunion_type.om_validite_debut <= CURRENT_DATE AND (reunion_type.om_validite_fin IS NULL OR reunion_type.om_validite_fin > CURRENT_DATE)))";
}
// Gestion OMValidité - Suppression du filtre si paramètre
if (isset($_GET["valide"]) and $_GET["valide"] == "false") {
    if (!isset($where_om_validite)
        or (isset($where_om_validite) and $where_om_validite == "")) {
        if (trim($selection) != "") {
            $selection = "";
        }
    } else {
        $selection = trim(str_replace($where_om_validite, "", $selection));
    }
}

/**
 * Gestion SOUSFORMULAIRE => $sousformulaire
 */
$sousformulaire = array(
    //'dossier_instruction_reunion',
    //'reunion',
    //'reunion_type_reunion_avis',
    //'reunion_type_reunion_categorie',
    //'reunion_type_reunion_instance',
);

