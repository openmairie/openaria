<?php
//$Id$ 
//gen openMairie le 16/07/2020 17:34

$DEBUG=0;
$serie=30;
$ent = __("application")." -> ".__("dossier_coordination");
if(!isset($premier)) $premier='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
// FROM 
$table = DB_PREFIXE."dossier_coordination
    LEFT JOIN ".DB_PREFIXE."dossier_coordination 
        ON dossier_coordination.dossier_coordination_parent=dossier_coordination.dossier_coordination 
    LEFT JOIN ".DB_PREFIXE."dossier_coordination_type 
        ON dossier_coordination.dossier_coordination_type=dossier_coordination_type.dossier_coordination_type 
    LEFT JOIN ".DB_PREFIXE."etablissement 
        ON dossier_coordination.etablissement=etablissement.etablissement 
    LEFT JOIN ".DB_PREFIXE."etablissement_categorie 
        ON dossier_coordination.etablissement_categorie=etablissement_categorie.etablissement_categorie 
    LEFT JOIN ".DB_PREFIXE."etablissement_type 
        ON dossier_coordination.etablissement_type=etablissement_type.etablissement_type ";
// SELECT 
$champAffiche = array(
    'dossier_coordination.dossier_coordination as "'.__("dossier_coordination").'"',
    'etablissement.libelle as "'.__("etablissement").'"',
    'dossier_coordination.libelle as "'.__("libelle").'"',
    'dossier_coordination_type.libelle as "'.__("dossier_coordination_type").'"',
    'to_char(dossier_coordination.date_demande ,\'DD/MM/YYYY\') as "'.__("date_demande").'"',
    'to_char(dossier_coordination.date_butoir ,\'DD/MM/YYYY\') as "'.__("date_butoir").'"',
    "case dossier_coordination.a_qualifier when 't' then 'Oui' else 'Non' end as \"".__("a_qualifier")."\"",
    "case dossier_coordination.erp when 't' then 'Oui' else 'Non' end as \"".__("erp")."\"",
    "case dossier_coordination.dossier_cloture when 't' then 'Oui' else 'Non' end as \"".__("dossier_cloture")."\"",
    );
//
$champNonAffiche = array(
    'dossier_coordination.dossier_autorisation_ads as "'.__("dossier_autorisation_ads").'"',
    'dossier_coordination.dossier_instruction_ads as "'.__("dossier_instruction_ads").'"',
    'dossier_coordination.etablissement_type as "'.__("etablissement_type").'"',
    'dossier_coordination.etablissement_categorie as "'.__("etablissement_categorie").'"',
    'dossier_coordination.contraintes_urba_om_html as "'.__("contraintes_urba_om_html").'"',
    'dossier_coordination.etablissement_locaux_sommeil as "'.__("etablissement_locaux_sommeil").'"',
    'dossier_coordination.references_cadastrales as "'.__("references_cadastrales").'"',
    'dossier_coordination.dossier_coordination_parent as "'.__("dossier_coordination_parent").'"',
    'dossier_coordination.description as "'.__("description").'"',
    'dossier_coordination.dossier_instruction_secu as "'.__("dossier_instruction_secu").'"',
    'dossier_coordination.dossier_instruction_acc as "'.__("dossier_instruction_acc").'"',
    'dossier_coordination.autorite_police_encours as "'.__("autorite_police_encours").'"',
    'dossier_coordination.date_cloture as "'.__("date_cloture").'"',
    'dossier_coordination.geolocalise as "'.__("geolocalise").'"',
    'dossier_coordination.interface_referentiel_ads as "'.__("interface_referentiel_ads").'"',
    'dossier_coordination.enjeu_erp as "'.__("enjeu_erp").'"',
    'dossier_coordination.depot_de_piece as "'.__("depot_de_piece").'"',
    'dossier_coordination.enjeu_ads as "'.__("enjeu_ads").'"',
    'dossier_coordination.geom_point as "'.__("geom_point").'"',
    'dossier_coordination.geom_emprise as "'.__("geom_emprise").'"',
    'dossier_coordination.dossier_ads_force as "'.__("dossier_ads_force").'"',
    );
//
$champRecherche = array(
    'dossier_coordination.dossier_coordination as "'.__("dossier_coordination").'"',
    'etablissement.libelle as "'.__("etablissement").'"',
    'dossier_coordination.libelle as "'.__("libelle").'"',
    'dossier_coordination_type.libelle as "'.__("dossier_coordination_type").'"',
    );
$tri="ORDER BY dossier_coordination.libelle ASC NULLS LAST";
$edition="dossier_coordination";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
$selection = "";
// Liste des clés étrangères avec leurs éventuelles surcharges
$foreign_keys_extended = array(
    "dossier_coordination" => array("dossier_coordination", "dossier_coordination_nouveau", "dossier_coordination_a_qualifier", "dossier_coordination_a_cloturer", ),
    "dossier_coordination_type" => array("dossier_coordination_type", ),
    "etablissement" => array("etablissement", "etablissement_referentiel_erp", "etablissement_tous", ),
    "etablissement_categorie" => array("etablissement_categorie", ),
    "etablissement_type" => array("etablissement_type", ),
);
// Filtre listing sous formulaire - dossier_coordination
if (in_array($retourformulaire, $foreign_keys_extended["dossier_coordination"])) {
    $selection = " WHERE (dossier_coordination.dossier_coordination_parent = ".intval($idxformulaire).") ";
}
// Filtre listing sous formulaire - dossier_coordination_type
if (in_array($retourformulaire, $foreign_keys_extended["dossier_coordination_type"])) {
    $selection = " WHERE (dossier_coordination.dossier_coordination_type = ".intval($idxformulaire).") ";
}
// Filtre listing sous formulaire - etablissement
if (in_array($retourformulaire, $foreign_keys_extended["etablissement"])) {
    $selection = " WHERE (dossier_coordination.etablissement = ".intval($idxformulaire).") ";
}
// Filtre listing sous formulaire - etablissement_categorie
if (in_array($retourformulaire, $foreign_keys_extended["etablissement_categorie"])) {
    $selection = " WHERE (dossier_coordination.etablissement_categorie = ".intval($idxformulaire).") ";
}
// Filtre listing sous formulaire - etablissement_type
if (in_array($retourformulaire, $foreign_keys_extended["etablissement_type"])) {
    $selection = " WHERE (dossier_coordination.etablissement_type = ".intval($idxformulaire).") ";
}

/**
 * Gestion SOUSFORMULAIRE => $sousformulaire
 */
$sousformulaire = array(
    'autorite_police',
    'courrier',
    'dossier_coordination',
    'dossier_coordination_message',
    'dossier_coordination_parcelle',
    'dossier_instruction',
    'etablissement',
    'lien_contrainte_dossier_coordination',
    'lien_dossier_coordination_contact',
    'lien_dossier_coordination_etablissement_type',
    'piece',
);

