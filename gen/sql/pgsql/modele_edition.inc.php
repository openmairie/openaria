<?php
//$Id$ 
//gen openMairie le 07/07/2020 09:15

$DEBUG=0;
$serie=30;
$ent = __("administration_parametrage")." -> ".__("editions")." -> ".__("modeles d'edition");
$om_validite = true;
if(!isset($premier)) $premier='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
// FROM 
$table = DB_PREFIXE."modele_edition
    LEFT JOIN ".DB_PREFIXE."courrier_type 
        ON modele_edition.courrier_type=courrier_type.courrier_type ";
// SELECT 
$champAffiche = array(
    'modele_edition.modele_edition as "'.__("modele_edition").'"',
    'modele_edition.libelle as "'.__("libelle").'"',
    'modele_edition.code as "'.__("code").'"',
    'courrier_type.libelle as "'.__("courrier_type").'"',
    'modele_edition.om_lettretype_id as "'.__("om_lettretype_id").'"',
    );
// Spécificité des dates de validité
$displayed_fields_validite = array(
    'to_char(modele_edition.om_validite_debut ,\'DD/MM/YYYY\') as "'.__("om_validite_debut").'"',
    'to_char(modele_edition.om_validite_fin ,\'DD/MM/YYYY\') as "'.__("om_validite_fin").'"',
);
// On affiche les champs de date de validité uniquement lorsque le paramètre
// d'affichage des éléments expirés est activé
if (isset($_GET['valide']) && $_GET['valide'] === 'false') {
    $champAffiche = array_merge($champAffiche, $displayed_fields_validite);
}

//
$champNonAffiche = array(
    'modele_edition.description as "'.__("description").'"',
    'modele_edition.om_validite_debut as "'.__("om_validite_debut").'"',
    'modele_edition.om_validite_fin as "'.__("om_validite_fin").'"',
    );
//
$champRecherche = array(
    'modele_edition.modele_edition as "'.__("modele_edition").'"',
    'modele_edition.libelle as "'.__("libelle").'"',
    'modele_edition.code as "'.__("code").'"',
    'courrier_type.libelle as "'.__("courrier_type").'"',
    'modele_edition.om_lettretype_id as "'.__("om_lettretype_id").'"',
    );
$tri="ORDER BY modele_edition.libelle ASC NULLS LAST";
$edition="modele_edition";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
$selection = " WHERE ((modele_edition.om_validite_debut IS NULL AND (modele_edition.om_validite_fin IS NULL OR modele_edition.om_validite_fin > CURRENT_DATE)) OR (modele_edition.om_validite_debut <= CURRENT_DATE AND (modele_edition.om_validite_fin IS NULL OR modele_edition.om_validite_fin > CURRENT_DATE)))";
$where_om_validite = " WHERE ((modele_edition.om_validite_debut IS NULL AND (modele_edition.om_validite_fin IS NULL OR modele_edition.om_validite_fin > CURRENT_DATE)) OR (modele_edition.om_validite_debut <= CURRENT_DATE AND (modele_edition.om_validite_fin IS NULL OR modele_edition.om_validite_fin > CURRENT_DATE)))";
// Liste des clés étrangères avec leurs éventuelles surcharges
$foreign_keys_extended = array(
    "courrier_type" => array("courrier_type", ),
);
// Filtre listing sous formulaire - courrier_type
if (in_array($retourformulaire, $foreign_keys_extended["courrier_type"])) {
    $selection = " WHERE (modele_edition.courrier_type = ".intval($idxformulaire).")  AND ((modele_edition.om_validite_debut IS NULL AND (modele_edition.om_validite_fin IS NULL OR modele_edition.om_validite_fin > CURRENT_DATE)) OR (modele_edition.om_validite_debut <= CURRENT_DATE AND (modele_edition.om_validite_fin IS NULL OR modele_edition.om_validite_fin > CURRENT_DATE)))";
$where_om_validite = " AND ((modele_edition.om_validite_debut IS NULL AND (modele_edition.om_validite_fin IS NULL OR modele_edition.om_validite_fin > CURRENT_DATE)) OR (modele_edition.om_validite_debut <= CURRENT_DATE AND (modele_edition.om_validite_fin IS NULL OR modele_edition.om_validite_fin > CURRENT_DATE)))";
}
// Gestion OMValidité - Suppression du filtre si paramètre
if (isset($_GET["valide"]) and $_GET["valide"] == "false") {
    if (!isset($where_om_validite)
        or (isset($where_om_validite) and $where_om_validite == "")) {
        if (trim($selection) != "") {
            $selection = "";
        }
    } else {
        $selection = trim(str_replace($where_om_validite, "", $selection));
    }
}

/**
 * Gestion SOUSFORMULAIRE => $sousformulaire
 */
$sousformulaire = array(
    //'analyses',
    //'analyses_type',
    //'courrier',
    //'proces_verbal',
    //'reunion_type',
);

