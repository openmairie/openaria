<?php
//$Id$ 
//gen openMairie le 07/07/2020 09:15

$DEBUG=0;
$serie=30;
$ent = __("administration_parametrage")." -> ".__("contacts")." -> ".__("types");
$om_validite = true;
if(!isset($premier)) $premier='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
// FROM 
$table = DB_PREFIXE."contact_type";
// SELECT 
$champAffiche = array(
    'contact_type.contact_type as "'.__("contact_type").'"',
    'contact_type.libelle as "'.__("libelle").'"',
    'contact_type.code as "'.__("code").'"',
    'contact_type.description as "'.__("description").'"',
    );
// Spécificité des dates de validité
$displayed_fields_validite = array(
    'to_char(contact_type.om_validite_debut ,\'DD/MM/YYYY\') as "'.__("om_validite_debut").'"',
    'to_char(contact_type.om_validite_fin ,\'DD/MM/YYYY\') as "'.__("om_validite_fin").'"',
);
// On affiche les champs de date de validité uniquement lorsque le paramètre
// d'affichage des éléments expirés est activé
if (isset($_GET['valide']) && $_GET['valide'] === 'false') {
    $champAffiche = array_merge($champAffiche, $displayed_fields_validite);
}

//
$champNonAffiche = array(
    'contact_type.om_validite_debut as "'.__("om_validite_debut").'"',
    'contact_type.om_validite_fin as "'.__("om_validite_fin").'"',
    );
//
$champRecherche = array(
    'contact_type.contact_type as "'.__("contact_type").'"',
    'contact_type.libelle as "'.__("libelle").'"',
    'contact_type.code as "'.__("code").'"',
    'contact_type.description as "'.__("description").'"',
    );
$tri="ORDER BY contact_type.libelle ASC NULLS LAST";
$edition="contact_type";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
$selection = " WHERE ((contact_type.om_validite_debut IS NULL AND (contact_type.om_validite_fin IS NULL OR contact_type.om_validite_fin > CURRENT_DATE)) OR (contact_type.om_validite_debut <= CURRENT_DATE AND (contact_type.om_validite_fin IS NULL OR contact_type.om_validite_fin > CURRENT_DATE)))";
$where_om_validite = " WHERE ((contact_type.om_validite_debut IS NULL AND (contact_type.om_validite_fin IS NULL OR contact_type.om_validite_fin > CURRENT_DATE)) OR (contact_type.om_validite_debut <= CURRENT_DATE AND (contact_type.om_validite_fin IS NULL OR contact_type.om_validite_fin > CURRENT_DATE)))";
// Gestion OMValidité - Suppression du filtre si paramètre
if (isset($_GET["valide"]) and $_GET["valide"] == "false") {
    if (!isset($where_om_validite)
        or (isset($where_om_validite) and $where_om_validite == "")) {
        if (trim($selection) != "") {
            $selection = "";
        }
    } else {
        $selection = trim(str_replace($where_om_validite, "", $selection));
    }
}

/**
 * Gestion SOUSFORMULAIRE => $sousformulaire
 */
$sousformulaire = array(
    //'contact',
);

