<?php
/**
 * Ce fichier permet de paramétrer le générateur.
 *
 * @package openaria
 * @version SVN : $Id: gen.inc.php 2835 2014-08-05 16:42:02Z fmichon $
 */

/**
 * Surcharge applicative de la classe 'om_dbform'.
 */
$om_dbform_path_override = "../obj/om_dbform.class.php";
$om_dbform_class_override = "om_dbform";


/**
 * Ce tableau permet de lister les tables qui ne doivent pas être prises en
 * compte dans le générateur. Elles n'apparaîtront donc pas dans l'interface
 * et ne seront pas automatiquement générées par le 'genfull'.
 */
$tables_to_avoid = array(
    //
    "geometry_columns",
    "om_version",
    "spatial_ref_sys",
    //
    "programmation_etat",
    "visite_etat",
    "piece_statut",
);


//
$rubrik_administration = "administration_parametrage";
$category_etablissements = "etablissements";
$category_adresses = "adresses";
$category_contacts = "contacts";
$category_contraintes = "contraintes";
$category_metiers = "metiers";
$category_reunions = "reunions";
$category_autoritepolice = "autorites de police";
$category_analyses = "analyses";
$category_documentsentrants = "documents entrants";
$category_documentsgeneres = "documents generes";
$category_visites = "visites";
$category_dossiers = "dossiers";
$category_editions = "editions";
$category_gestiondesutilisateurs = "gestion des utilisateurs";
$category_tableauxdebord = "tableaux de bord";


/**
 * Ce tableau de configuration permet de donner des informations de surcharges
 * sur certains objets pour qu'elles soient prises en compte par le générateur.
 * $tables_to_overload = array(
 *    "<table>" => array(
 *        // définition de la liste des classes qui surchargent la classe
 *        // <table> pour que le générateur puisse générer ces surcharges
 *        // et les inclure dans les tests de sous formulaire
 *        "extended_class" => array("<classe_surcharge_1_de_table>", ),
 *        // définition de la liste des champs à afficher dans l'affichage du
 *        // tableau champAffiche dans <table>.inc.php
 *        "displayed_fields_in_tableinc" => array("<champ_1>", ),
 *        // définition de l'option 'om_validite' pour que les colonnes soient
 *        // cachées par défaut
 *        "om_validite" => "hidden_by_default",
 *    ),
 * );
 */
$tables_to_overload = array(


    // A&P
    "acteur" => array(
        //
        "tablename_in_page_title" => "acteurs",
        "breadcrumb_in_page_title" => array($rubrik_administration, $category_metiers, ),
        //
        "om_validite" => "hidden_by_default",
    ),

    //
    "acteur_conge" => array(
        //
        "displayed_fields_in_tableinc" => array(
            "acteur", "date_debut", "heure_debut", "date_fin", "heure_fin",
        ),
    ),

    //
    "acteur_plage_visite" => array(
        //
        "displayed_fields_in_tableinc" => array(
            "acteur", "lundi_matin", "lundi_apresmidi", "mardi_matin", "mardi_apresmidi", "mercredi_matin", "mercredi_apresmidi", "jeudi_matin", "jeudi_apresmidi", "vendredi_matin", "vendredi_apresmidi",
        ),
    ),

    // A&P
    "analyses_type" => array(
        //
        "tablename_in_page_title" => "types",
        "breadcrumb_in_page_title" => array($rubrik_administration, $category_analyses, ),
        //
        "tabs_in_form" => false,
        //
        "om_validite" => "hidden_by_default",
    ),

    // A&P
    "arrondissement" => array(
        //
        "tablename_in_page_title" => "arrondissements",
        "breadcrumb_in_page_title" => array($rubrik_administration, $category_adresses, ),
        //
        "tabs_in_form" => false,
        //
        "om_validite" => "hidden_by_default",
    ),

    // A&P
    "autorite_competente" => array(
        //
        "tablename_in_page_title" => "autorites competentes",
        "breadcrumb_in_page_title" => array($rubrik_administration, $category_metiers, ),
        //
        "tabs_in_form" => false,
    ),

    // A&P
    "autorite_police_decision" => array(
        //
        "tablename_in_page_title" => "types",
        "breadcrumb_in_page_title" => array($rubrik_administration, $category_autoritepolice, ),
        //
        "displayed_fields_in_tableinc" => array(
            "code", "libelle", "etablissement_etat", "service", "suivi_delai", "avis", "statut_administratif", "delai", "type_arrete",
        ),
        //
        "tabs_in_form" => false,
        //
        "om_validite" => "hidden_by_default",
    ),

    // A&P
    "autorite_police_motif" => array(
        //
        "tablename_in_page_title" => "motifs",
        "breadcrumb_in_page_title" => array($rubrik_administration, $category_autoritepolice, ),
        //
        "tabs_in_form" => false,
        //
        "om_validite" => "hidden_by_default",
    ),

    //
    "contact" => array(
        //
        "extended_class" => array(
            "contact_institutionnel", "contact_contexte_dossier_coordination",
        ),
        //
        "om_validite" => "hidden_by_default",
    ),

    // A&P
    "contact_type" => array(
        //
        "tablename_in_page_title" => "types",
        "breadcrumb_in_page_title" => array($rubrik_administration, $category_contacts, ),
        //
        "tabs_in_form" => false,
        //
        "om_validite" => "hidden_by_default",
    ),

    // A&P
    "contact_civilite" => array(
        //
        "tablename_in_page_title" => "civilites",
        "breadcrumb_in_page_title" => array($rubrik_administration, $category_contacts, ),
        //
        "tabs_in_form" => false,
    ),

    //
    "courrier" => array(
        //
        "extended_class" => array(
            "courrier_a_editer", "courrier_attente_signature", "courrier_attente_retour_ar", 
        ),
    ),

    // A&P
    "courrier_texte_type" => array(
        //
        "tablename_in_page_title" => "complements",
        "breadcrumb_in_page_title" => array($rubrik_administration, $category_documentsgeneres, ),
        //
        "tabs_in_form" => false,
        //
        "om_validite" => "hidden_by_default",
    ),

    // A&P
    "courrier_type" => array(
        //
        "tablename_in_page_title" => "types",
        "breadcrumb_in_page_title" => array($rubrik_administration, $category_editions, ),
        //
        "tabs_in_form" => false,
        //
        "om_validite" => "hidden_by_default",
    ),

    // A&P
    "courrier_type_categorie" => array(
        //
        "tablename_in_page_title" => "categories",
        "breadcrumb_in_page_title" => array($rubrik_administration, $category_editions, ),
        //
        "tabs_in_form" => false,
        //
        "om_validite" => "hidden_by_default",
    ),

    // A&P
    "derogation_scda" => array(
        //
        "tablename_in_page_title" => "derogations scda",
        "breadcrumb_in_page_title" => array($rubrik_administration, $category_metiers, ),
        //
        "tabs_in_form" => false,
        //
        "om_validite" => "hidden_by_default",
    ),

    // A&P
    "contrainte" => array(
        //
        "tablename_in_page_title" => "listing",
        "breadcrumb_in_page_title" => array($rubrik_administration, $category_contraintes, ),
        //
        "tabs_in_form" => false,
        //
        "om_validite" => "hidden_by_default",
    ),

    // A&P
    "document_presente" => array(
        //
        "tablename_in_page_title" => "documents presentes",
        "breadcrumb_in_page_title" => array($rubrik_administration, $category_analyses, ),
        //
        "tabs_in_form" => false,
        //
        "om_validite" => "hidden_by_default",
    ),

    //
    "dossier_coordination" => array(
        //
        "displayed_fields_in_tableinc" => array(
            "dossier_coordination", "libelle", "etablissement", "erp", "dossier_coordination_type", "date_demande", "date_butoir", "a_qualifier", "dossier_cloture", 
        ),
        //
        "extended_class" => array(
            "dossier_coordination_nouveau", "dossier_coordination_a_qualifier", "dossier_coordination_a_cloturer",
        ),
    ),

    //
    "dossier_coordination_message" => array(
        //
        "extended_class" => array(
            "dossier_coordination_message_tous", "dossier_coordination_message_contexte_dc", "dossier_coordination_message_contexte_di",
        ),
    ),

    // A&P
    "dossier_coordination_type" => array(
        //
        "tablename_in_page_title" => "types de DC",
        "breadcrumb_in_page_title" => array($rubrik_administration, $category_dossiers, ),
        //
        "tabs_in_form" => false,
        //
        "om_validite" => "hidden_by_default",
    ),

    //
    "dossier_instruction" => array(
        "extended_class" => array(
            "dossier_instruction_mes_plans", "dossier_instruction_mes_visites",
            "dossier_instruction_tous_plans", "dossier_instruction_tous_visites",
            "dossier_instruction_a_qualifier", "dossier_instruction_a_affecter",
        ),
    ),

    // A&P
    "dossier_type" => array(
        //
        "tablename_in_page_title" => "types",
        "breadcrumb_in_page_title" => array($rubrik_administration, $category_dossiers, ),
        //
        "tabs_in_form" => false,
        //
        "om_validite" => "hidden_by_default",
    ),

    // A&P
    "essai_realise" => array(
        //
        "tablename_in_page_title" => "essais realises",
        "breadcrumb_in_page_title" => array($rubrik_administration, $category_analyses, ),
        //
        "tabs_in_form" => false,
        //
        "om_validite" => "hidden_by_default",
    ),

    //
    "etablissement" => array(
        //
        "extended_class" => array(
            "etablissement_referentiel_erp",
            "etablissement_tous",
        ),
        //
        "om_validite" => "hidden_by_default",
    ),

    //
    "etablissement_unite" => array(
        //
        "extended_class" => array(
            "etablissement_unite__contexte_di_analyse__ua_valide_sur_etab",
            "etablissement_unite__contexte_di_analyse__ua_en_analyse",
            "etablissement_unite__contexte_etab__ua_valide",
            "etablissement_unite__contexte_etab__ua_archive",
            "etablissement_unite__contexte_etab__ua_enprojet",
        ),
    ),

    // A&P
    "etablissement_type" => array(
        //
        "tabs_in_form" => false,
        //
        "tablename_in_page_title" => "types",
        "breadcrumb_in_page_title" => array($rubrik_administration, $category_etablissements, ),
        //
        "om_validite" => "hidden_by_default",
    ),

    // A&P
    "etablissement_categorie" => array(
        //
        "tabs_in_form" => false,
        //
        "tablename_in_page_title" => "categories",
        "breadcrumb_in_page_title" => array($rubrik_administration, $category_etablissements, ),
        //
        "om_validite" => "hidden_by_default",
    ),

    // A&P
    "etablissement_statut_juridique" => array(
        //
        "tabs_in_form" => false,
        //
        "tablename_in_page_title" => "statuts juridiques",
        "breadcrumb_in_page_title" => array($rubrik_administration, $category_etablissements, ),
        //
        "om_validite" => "hidden_by_default",
    ),

    // A&P
    "etablissement_tutelle_adm" => array(
        //
        "tabs_in_form" => false,
        //
        "tablename_in_page_title" => "tutelles administratives",
        "breadcrumb_in_page_title" => array($rubrik_administration, $category_etablissements, ),
        //
        "om_validite" => "hidden_by_default",
    ),

    // A&P
    "etablissement_nature" => array(
        //
        "tabs_in_form" => false,
        //
        "tablename_in_page_title" => "natures",
        "breadcrumb_in_page_title" => array($rubrik_administration, $category_etablissements, ),
        //
        "om_validite" => "hidden_by_default",
    ),

    // A&P
    "etablissement_etat" => array(
        //
        "tabs_in_form" => false,
        //
        "tablename_in_page_title" => "etats",
        "breadcrumb_in_page_title" => array($rubrik_administration, $category_etablissements, ),
        //
        "om_validite" => "hidden_by_default",
    ),

    // A&P
    "modele_edition" => array(
        //
        "tabs_in_form" => false,
        //
        "tablename_in_page_title" => "modeles d'edition",
        "breadcrumb_in_page_title" => array($rubrik_administration, $category_editions, ),
        //
        "om_validite" => "hidden_by_default",
    ),


    // A&P
    "om_collectivite" => array(
        //
        "tablename_in_page_title" => "collectivites",
        "breadcrumb_in_page_title" => array($rubrik_administration, ),
        //
        "tabs_in_form" => false,
    ),

    //
    "om_dashboard" => array(
        //
        "tablename_in_page_title" => "tableaux de bord",
        "breadcrumb_in_page_title" => array($rubrik_administration, $category_tableauxdebord, ),
        //
        "tabs_in_form" => false,
    ),

    //
    "om_droit" => array(
        //
        "tablename_in_page_title" => "droits",
        "breadcrumb_in_page_title" => array($rubrik_administration, $category_gestiondesutilisateurs, ),
        //
        "tabs_in_form" => false,
    ),

    //
    "om_etat" => array(
        //
        "tablename_in_page_title" => "etats",
        "breadcrumb_in_page_title" => array($rubrik_administration, $category_editions, ),
        //
        "tabs_in_form" => false,
    ),

    // A&P
    "om_lettretype" => array(
        //
        "tablename_in_page_title" => "lettres types",
        "breadcrumb_in_page_title" => array($rubrik_administration, $category_editions, ),
        //
        "tabs_in_form" => false,
    ),

    // A&P
    "om_logo" => array(
        //
        "tablename_in_page_title" => "logos",
        "breadcrumb_in_page_title" => array($rubrik_administration, $category_editions, ),
        //
        "tabs_in_form" => false,
    ),

    // A&P
    "om_parametre" => array(
        //
        "tablename_in_page_title" => "parametres",
        "breadcrumb_in_page_title" => array($rubrik_administration, ),
        //
        "tabs_in_form" => false,
    ),

    //
    "om_permission" => array(
        //
        "tablename_in_page_title" => "permissions",
        "breadcrumb_in_page_title" => array($rubrik_administration, $category_gestiondesutilisateurs, ),
        //
        "tabs_in_form" => false,
    ),

    // A&P
    "om_profil" => array(
        //
        "tablename_in_page_title" => "profils",
        "breadcrumb_in_page_title" => array($rubrik_administration, $category_gestiondesutilisateurs, ),
        //
        "tabs_in_form" => false,
    ),

    // A&P
    "om_requete" => array(
        //
        "displayed_fields_in_tableinc" => array(
            "code", "libelle", "description", "type",
        ),
        //
        "tablename_in_page_title" => "requetes",
        "breadcrumb_in_page_title" => array($rubrik_administration, $category_editions, ),
    ),
    // A&P
    "om_sig_extent" => array(
        "tablename_in_page_title" => "étendue",
        "breadcrumb_in_page_title" => array($rubrik_administration, "SIG", ),
    ),
    // A&P
    "om_sig_flux" => array(
        "tablename_in_page_title" => "flux",
        "breadcrumb_in_page_title" => array($rubrik_administration, "SIG", ),
    ),
    // A&P
    "om_sig_map" => array(
        "tablename_in_page_title" => "carte",
        "breadcrumb_in_page_title" => array($rubrik_administration, "SIG", ),
    ),
    // A&P
    "om_sig_map_comp" => array(
        "tablename_in_page_title" => "géométrie",
        "breadcrumb_in_page_title" => array($rubrik_administration, "SIG", ),
    ),
    // A&P
    "om_sig_map_flux" => array(
        "tablename_in_page_title" => "flux appliqué à une carte",
        "breadcrumb_in_page_title" => array($rubrik_administration, "SIG", ),
    ),
    // A&P
    "om_sousetat" => array(
        //
        "tablename_in_page_title" => "sous-etats",
        "breadcrumb_in_page_title" => array($rubrik_administration, $category_editions, ),
    ),

    // A&P
    "om_utilisateur" => array(
        //
        "displayed_fields_in_tableinc" => array(
            "nom", "email", "login", "om_profil",
        ),
        //
        "tablename_in_page_title" => "utilisateurs",
        "breadcrumb_in_page_title" => array($rubrik_administration, $category_gestiondesutilisateurs, ),
    ),

    // A&P
    "om_widget" => array(
        //
        "displayed_fields_in_tableinc" => array(
            "libelle", "om_profil", "type", 
        ),
        //
        "tablename_in_page_title" => "widgets",
        "breadcrumb_in_page_title" => array($rubrik_administration, $category_tableauxdebord, ),
    ),

    // A&P
    "periodicite_visites" => array(
        //
        "tabs_in_form" => false,
        //
        "tablename_in_page_title" => "periodicites de visites",
        "breadcrumb_in_page_title" => array($rubrik_administration, $category_etablissements, ),
    ),

    //
    "piece" => array(
        //
        "displayed_fields_in_tableinc" => array(
            "nom", "etablissement", "dossier_coordination", "dossier_instruction", "om_date_creation", "date_butoir", 
        ),
        //
        "extended_class" => array(
            "piece_bannette", "piece_suivi", "piece_a_valider", "piece_non_lu", 
        ),
    ),

    // A&P
    "piece_type" => array(
        //
        "tabs_in_form" => false,
        //
        "tablename_in_page_title" => "types",
        "breadcrumb_in_page_title" => array($rubrik_administration, $category_documentsentrants, ),
        //
        "om_validite" => "hidden_by_default",
    ),

    // A&P
    "prescription_reglementaire" => array(
        //
        "tablename_in_page_title" => "prescriptions",
        "breadcrumb_in_page_title" => array($rubrik_administration, $category_analyses, ),
        //
        "tabs_in_form" => false,
        //
        "om_validite" => "hidden_by_default",
    ),

    //
    "prescription_specifique" => array(
        //
        "om_validite" => "hidden_by_default",
    ),

    // A&P
    "reglementation_applicable" => array(
        //
        "tablename_in_page_title" => "reglementations applicables",
        "breadcrumb_in_page_title" => array($rubrik_administration, $category_analyses, ),
        //
        "tabs_in_form" => false,
        //
        "om_validite" => "hidden_by_default",
    ),

    //
    "reunion" => array(
        //
        "displayed_fields_in_tableinc" => array(
            "code", "reunion_type", "libelle", "lieu_adresse_ligne1", "lieu_adresse_ligne2", "date_reunion", "reunion_cloture", 
        ),
    ),

    // A&P
    "reunion_avis" => array(
        //
        "tabs_in_form" => false,
        //
        "tablename_in_page_title" => "avis",
        "breadcrumb_in_page_title" => array($rubrik_administration, $category_metiers, ),
        //
        "om_validite" => "hidden_by_default",
    ),

    // A&P
    "reunion_categorie" => array(
        //
        "tabs_in_form" => false,
        //
        "tablename_in_page_title" => "categories",
        "breadcrumb_in_page_title" => array($rubrik_administration, $category_reunions, ),
        //
        "om_validite" => "hidden_by_default",
    ),

    // A&P
    "reunion_instance" => array(
        //
        "tabs_in_form" => false,
        //
        "tablename_in_page_title" => "instances",
        "breadcrumb_in_page_title" => array($rubrik_administration, $category_metiers, ),
        //
        "om_validite" => "hidden_by_default",
    ),

    //
    "reunion_instance_membre" => array(
        //
        "om_validite" => "hidden_by_default",
    ),

    // A&P
    "reunion_type" => array(
        //
        "displayed_fields_in_tableinc" => array(
            "code", "libelle", "lieu_adresse_ligne1", "lieu_adresse_ligne2", "lieu_salle", "heure", "om_validite_debut", "om_validite_fin", 
        ),
        //
        "tabs_in_form" => false,
        //
        "tablename_in_page_title" => "types",
        "breadcrumb_in_page_title" => array($rubrik_administration, $category_reunions, ),
        //
        "om_validite" => "hidden_by_default",
    ),

    // A&P
    "service" => array(
        //
        "tabs_in_form" => false,
        //
        "tablename_in_page_title" => "services",
        "breadcrumb_in_page_title" => array($rubrik_administration, $category_metiers, ),
        //
        "om_validite" => "hidden_by_default",
    ),

    // A&P
    "signataire" => array(
        //
        "tabs_in_form" => false,
        //
        "tablename_in_page_title" => "signataires",
        "breadcrumb_in_page_title" => array($rubrik_administration, $category_documentsgeneres, ),
        //
        "om_validite" => "hidden_by_default",
    ),

    // A&P
    "signataire_qualite" => array(
        //
        "tabs_in_form" => false,
        //
        "tablename_in_page_title" => "qualites de signataire",
        "breadcrumb_in_page_title" => array($rubrik_administration, $category_documentsgeneres, ),
        //
        "om_validite" => "hidden_by_default",
    ),

    //
    "visite" => array(
        //
        "tabs_in_form" => false,
    ),

    // A&P
    "visite_duree" => array(
        //
        "tabs_in_form" => false,
        //
        "tablename_in_page_title" => "durees",
        "breadcrumb_in_page_title" => array($rubrik_administration, $category_visites, ),
    ),

    // A&P
    "visite_motif_annulation" => array(
        //
        "tabs_in_form" => false,
        //
        "tablename_in_page_title" => "motifs d'annulation",
        "breadcrumb_in_page_title" => array($rubrik_administration, $category_visites, ),
        //
        "om_validite" => "hidden_by_default",
    ),

    // A&P
    "voie" => array(
        //
        "tabs_in_form" => false,
        //
        "tablename_in_page_title" => "voies",
        "breadcrumb_in_page_title" => array($rubrik_administration, $category_adresses, ),
        //
        "om_validite" => "hidden_by_default",
    ),
);
