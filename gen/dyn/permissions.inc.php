<?php
/**
 * Ce fichier permet de paramétrer le générateur.
 *
 * @package openaria
 * @version SVN : $Id: gen.inc.php 2835 2014-08-05 16:42:02Z fmichon $
 */


$files_to_avoid = array(
    "digitalizedDocument.class.php",
    "pdf_lettre_rar.class.php",
    "geoaria.class.php",
    "treatment_exception.class.php",
    "openaria.class.php",
    "om_edition.class.php",
);

$permissions = array(
    "analyses_consulter_analyses_type",
    "analyses_consulter_avis_propose",
    "analyses_consulter_classification_etablissement",
    "analyses_consulter_compte_rendu",
    "analyses_consulter_descriptif_etablissement",
    "analyses_consulter_documents_presentes",
    "analyses_consulter_donnees_techniques",
    "analyses_consulter_essais_realises",
    "analyses_consulter_modeles_edition",
    "analyses_consulter_objet",
    "analyses_consulter_observation",
    "analyses_consulter_prescriptions",
    "analyses_consulter_proposition_decision_ap",
    "analyses_consulter_reglementation_applicable",
    "courrier_a_editer_om_fichier_finalise_courrier_telecharger",
    "courrier_a_editer_om_fichier_signe_courrier_telecharger",
    "courrier_attente_retour_ar_om_fichier_finalise_courrier_telecharger",
    "courrier_attente_retour_ar_om_fichier_signe_courrier_telecharger",
    "courrier_attente_signature_om_fichier_finalise_courrier_telecharger",
    "courrier_attente_signature_om_fichier_signe_courrier_telecharger",
    "courrier_om_fichier_finalise_courrier_telecharger",
    "courrier_om_fichier_signe_courrier_telecharger",
    "dossier_coordination_geoaria",
    "etablissement_modifier_suivi_dernieres_visites",
    "etablissement_referentiel_erp_exporter",
    "etablissement_referentiel_erp_geoaria",
    "etablissement_tous_exporter",
    "etablissement_tous_geoaria",
    "piece_a_valider_modifier_suivi",
    "piece_a_valider_uid_telecharger",
    "piece_bannette_modifier_suivi",
    "piece_bannette_uid_telecharger",
    "piece_modifier_suivi",
    "piece_suivi_modifier_suivi",
    "piece_suivi_uid_telecharger",
    "piece_uid_telecharger",
    "proces_verbal_om_fichier_finalise_telecharger",
    "proces_verbal_om_fichier_signe_telecharger",
    "reunion_om_fichier_reunion_cr_global_signe_telecharger",
    "reunion_om_fichier_reunion_cr_global_telecharger",
    "reunion_om_fichier_reunion_cr_par_dossier_signe_telecharger",
    "reunion_om_fichier_reunion_odj_telecharger",
    "visite_mes_visites_a_realiser_consulter",
    "visite_exporter",
);
