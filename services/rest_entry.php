<?php
/**
 * Ce fichier est le point d'entrée pour les requêtes REST dans l'application.
 *
 * @package openaria
 * @version SVN : $Id$
 */

// Log - services.log
require_once "../obj/openaria.class.php";
$log_services = logger::instance();
$log_services->log_to_file("services.log", "IN - ---");
$log_services->log_to_file("services.log", "IN - rest_entry.php - ".$_SERVER["REQUEST_URI"]."");

// Instanciation de restler
require_once "./php/restler/restler/restler.php";
$r = new Restler();

// Déclaration de la ressource 'messages'
require_once "./REST/messages.php";
$r->addAPIClass('messages');

// Déclaration de la ressource 'maintenance'
require_once "./REST/maintenance.php";
$r->addAPIClass('maintenance');

// Exécution de l'API
$r->handle();
