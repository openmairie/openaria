-------------------------------------------------------------------------------
-- Script de mise à jour vers la version v1.4.0 depuis la version v1.3.0
--
-- @package openaria
-- @version SVN : $Id$
-------------------------------------------------------------------------------

--
ALTER TABLE ONLY etablissement
    DROP CONSTRAINT etablissement_periodicite_visites_fkey;

