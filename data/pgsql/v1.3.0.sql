-------------------------------------------------------------------------------
-- Script de mise à jour vers la version v1.3.0 depuis la version v1.2.1
--
-- @package openaria
-- @version SVN : $Id$
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- BEGIN / Script de mise à jour vers la version v4.6.0 du framework
-------------------------------------------------------------------------------

--
-- Ajout d'un nouveau champ dans widget pour sélectionner uniquement les scripts
-- qui se situe dans app/
--
ALTER TABLE om_widget
    ADD script character varying(80) NOT NULL DEFAULT '';
COMMENT ON COLUMN "om_widget"."script" IS 'Fichier utilisé par le widget';

--
-- Ajout d'un nouveau champ text dans widget spésialisé pour script
--
ALTER TABLE om_widget
ADD arguments text NOT NULL DEFAULT '';
COMMENT ON COLUMN "om_widget"."arguments" IS 'Arguments affiché dans le widget ';

--
-- Déplacement des informations contenus dans lien et texte dans script et 
-- arguments quand le type de widget est file 
--
UPDATE om_widget SET
script = lien,
arguments = texte,
lien = '',
texte = ''
WHERE type = 'file';

--
-- Ajout du droit au cadre-si de consulter le batiment pour corriger le test SIG
--


INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'etablissement_consulter',(SELECT om_profil FROM om_profil WHERE libelle = 'CADRE SI')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'etablissement_consulter' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'CADRE SI')
    );

-------------------------------------------------------------------------------
-- END / Script de mise à jour vers la version v4.6.0 du framework
-------------------------------------------------------------------------------
