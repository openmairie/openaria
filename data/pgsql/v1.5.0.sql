-------------------------------------------------------------------------------
-- Script de mise à jour vers la version v1.5.0 depuis la version v1.4.0
--
-- @package openaria
-- @version SVN : $Id$
-------------------------------------------------------------------------------

--
-- Ticket #9089 - BEGIN
-- Amélioration de l'ergonomie de la saisie d'un modèle d'édition.
--
-- Suppression de la colonne om_etat
ALTER TABLE modele_edition DROP CONSTRAINT modele_edition_om_etat_fkey;
ALTER TABLE modele_edition DROP COLUMN om_etat;
-- Suppression de la colonne om_lettretype et remplacement par une colonne
-- stockant l'identifiant de la lettre type et non sa clé primaire
ALTER TABLE modele_edition ADD om_lettretype_id character varying(50);
UPDATE modele_edition SET om_lettretype_id = (SELECT id FROM om_lettretype WHERE om_lettretype=modele_edition.om_lettretype);
ALTER TABLE modele_edition DROP CONSTRAINT modele_edition_om_lettretype_fkey;
ALTER TABLE modele_edition DROP COLUMN om_lettretype;
--
-- Ticket #9089 - END
--
