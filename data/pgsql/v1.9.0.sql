-------------------------------------------------------------------------------
-- Script de mise à jour vers la version v1.9.0 depuis la version v1.8.1
--
-- @package openaria
-- @version SVN : $Id$
-------------------------------------------------------------------------------


ALTER TABLE ONLY etablissement
    ADD CONSTRAINT etablissement_code_unique_key UNIQUE (code);

