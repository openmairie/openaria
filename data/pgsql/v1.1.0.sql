-------------------------------------------------------------------------------
-- Script de mise à jour vers la version v1.1.0 depuis la version v1.0.0
--
-- @package openaria
-- @version SVN : $Id$
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- BEGIN - Réception d'un message "Dossier à enjeux ADS" 
-------------------------------------------------------------------------------
ALTER TABLE "dossier_coordination"
ADD "enjeu_ads" boolean NOT NULL DEFAULT 'false';
COMMENT ON COLUMN "dossier_coordination"."enjeu_ads" IS 'Marqueur indiquant que le dossier est à enjeu dans ADS';
-------------------------------------------------------------------------------
-- END - Réception d'un message "Dossier à enjeux ADS" 
-------------------------------------------------------------------------------
