-------------------------------------------------------------------------------
-- Script de mise à jour vers la version v1.12.0 depuis la version v1.11.0
--
-- @package openaria
-- @version SVN : $Id$
-------------------------------------------------------------------------------

--
-- Ticket #9489 - BEGIN
--
ALTER TABLE reunion ALTER COLUMN code TYPE character varying(21);
--
-- Ticket #9489 - END
--

--
-- Ticket #9570 - BEGIN
--
ALTER TABLE dossier_coordination_type ADD piece_attendue_si text;
ALTER TABLE dossier_coordination_type ADD piece_attendue_acc text;
--
-- Ticket #9570 - END
--

--
-- Ticket #9571 - BEGIN
--
ALTER TABLE reunion_type ADD participants text;
--
-- Ticket #9571 - END
--

--
-- Ticket #9573 - BEGIN
--
ALTER TABLE reunion RENAME numerotation TO numerotation_simple;
ALTER TABLE reunion ADD numerotation_complexe text;
ALTER TABLE reunion ADD numerotation_mode character varying(30);
UPDATE reunion SET numerotation_mode='globale';
ALTER TABLE reunion_type ADD numerotation_mode character varying(30);
UPDATE reunion_type SET numerotation_mode='globale';
ALTER TABLE ONLY dossier_instruction_reunion
    ADD CONSTRAINT dossier_instruction_reunion_reunion_ordre_uk UNIQUE (reunion, ordre);
--
-- Ticket #9573 - END
--

