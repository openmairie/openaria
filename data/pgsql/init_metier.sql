--
-- PostgreSQL database dump
--

-- Dumped from database version 12.13 (Ubuntu 12.13-1.pgdg20.04+1)
-- Dumped by pg_dump version 12.13 (Ubuntu 12.13-1.pgdg20.04+1)

-- SET statement_timeout = 0;
-- SET lock_timeout = 0;
-- SET idle_in_transaction_session_timeout = 0;
-- SET client_encoding = 'UTF8';
-- SET standard_conforming_strings = on;
-- SELECT pg_catalog.set_config('search_path', '', false);
-- SET check_function_bodies = false;
-- SET xmloption = content;
-- SET client_min_messages = warning;
-- SET row_security = off;

--
-- Name: openaria; Type: SCHEMA; Schema: -; Owner: -
--

-- CREATE SCHEMA openaria;


-- SET default_tablespace = '';

-- SET default_table_access_method = heap;

--
-- Name: acteur; Type: TABLE; Schema: openaria; Owner: -
--

CREATE TABLE acteur (
    acteur integer NOT NULL,
    nom_prenom character varying(80),
    om_utilisateur integer,
    service integer,
    role character varying(100),
    acronyme character varying(10),
    couleur character varying(6),
    om_validite_debut date,
    om_validite_fin date,
    reference character varying(255)
);


--
-- Name: TABLE acteur; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON TABLE acteur IS 'Table des acteurs de l''application';


--
-- Name: COLUMN acteur.acteur; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN acteur.acteur IS 'Identifiant unique';


--
-- Name: COLUMN acteur.nom_prenom; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN acteur.nom_prenom IS 'Nom complet de l''acteur. C''est lui qui figurera dans les correspondances';


--
-- Name: COLUMN acteur.om_utilisateur; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN acteur.om_utilisateur IS 'Lien vers les utilisateurs, si l''acteur est un utilisateur actif';


--
-- Name: COLUMN acteur.service; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN acteur.service IS 'Lien vers le Service de l''acteur : peut être Sécurité ou Accessibilité';


--
-- Name: COLUMN acteur.role; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN acteur.role IS 'Rôle de l''acteur';


--
-- Name: COLUMN acteur.acronyme; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN acteur.acronyme IS 'Acronyme de l''acteur';


--
-- Name: COLUMN acteur.couleur; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN acteur.couleur IS 'Code hexadécimal de la couleur de l''acteur';


--
-- Name: acteur_conge; Type: TABLE; Schema: openaria; Owner: -
--

CREATE TABLE acteur_conge (
    acteur_conge integer NOT NULL,
    acteur integer NOT NULL,
    date_debut date,
    heure_debut character varying(5),
    date_fin date,
    heure_fin character varying(5)
);


--
-- Name: TABLE acteur_conge; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON TABLE acteur_conge IS 'Congé des techniciens pour permettre de connaître ses disponibilités pour les visites.';


--
-- Name: COLUMN acteur_conge.acteur_conge; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN acteur_conge.acteur_conge IS 'Identifiant unique';


--
-- Name: COLUMN acteur_conge.acteur; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN acteur_conge.acteur IS 'Lien vers l''acteur';


--
-- Name: COLUMN acteur_conge.date_debut; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN acteur_conge.date_debut IS 'Date de début du congé';


--
-- Name: COLUMN acteur_conge.heure_debut; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN acteur_conge.heure_debut IS 'Heure de début du congé';


--
-- Name: COLUMN acteur_conge.date_fin; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN acteur_conge.date_fin IS 'Date de fin du congé';


--
-- Name: COLUMN acteur_conge.heure_fin; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN acteur_conge.heure_fin IS 'Heure de fin du congé';


--
-- Name: acteur_conge_seq; Type: SEQUENCE; Schema: openaria; Owner: -
--

CREATE SEQUENCE acteur_conge_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: acteur_conge_seq; Type: SEQUENCE OWNED BY; Schema: openaria; Owner: -
--

ALTER SEQUENCE acteur_conge_seq OWNED BY acteur_conge.acteur_conge;


--
-- Name: acteur_plage_visite; Type: TABLE; Schema: openaria; Owner: -
--

CREATE TABLE acteur_plage_visite (
    acteur_plage_visite integer NOT NULL,
    acteur integer NOT NULL,
    lundi_matin boolean,
    lundi_apresmidi boolean,
    mardi_matin boolean,
    mardi_apresmidi boolean,
    mercredi_matin boolean,
    mercredi_apresmidi boolean,
    jeudi_matin boolean,
    jeudi_apresmidi boolean,
    vendredi_matin boolean,
    vendredi_apresmidi boolean
);


--
-- Name: TABLE acteur_plage_visite; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON TABLE acteur_plage_visite IS 'Disponibilité préférée des acteurs dans la semaine pour effectuer des visites.';


--
-- Name: COLUMN acteur_plage_visite.acteur_plage_visite; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN acteur_plage_visite.acteur_plage_visite IS 'Identifiant unique';


--
-- Name: COLUMN acteur_plage_visite.acteur; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN acteur_plage_visite.acteur IS 'Lien vers l''acteur';


--
-- Name: COLUMN acteur_plage_visite.lundi_matin; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN acteur_plage_visite.lundi_matin IS 'La demi journée est une disponibilité préférée du technicien';


--
-- Name: COLUMN acteur_plage_visite.lundi_apresmidi; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN acteur_plage_visite.lundi_apresmidi IS 'La demi journée est une disponibilité préférée du technicien';


--
-- Name: COLUMN acteur_plage_visite.mardi_matin; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN acteur_plage_visite.mardi_matin IS 'La demi journée est une disponibilité préférée du technicien';


--
-- Name: COLUMN acteur_plage_visite.mardi_apresmidi; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN acteur_plage_visite.mardi_apresmidi IS 'La demi journée est une disponibilité préférée du technicien';


--
-- Name: COLUMN acteur_plage_visite.mercredi_matin; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN acteur_plage_visite.mercredi_matin IS 'La demi journée est une disponibilité préférée du technicien';


--
-- Name: COLUMN acteur_plage_visite.mercredi_apresmidi; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN acteur_plage_visite.mercredi_apresmidi IS 'La demi journée est une disponibilité préférée du technicien';


--
-- Name: COLUMN acteur_plage_visite.jeudi_matin; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN acteur_plage_visite.jeudi_matin IS 'La demi journée est une disponibilité préférée du technicien';


--
-- Name: COLUMN acteur_plage_visite.jeudi_apresmidi; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN acteur_plage_visite.jeudi_apresmidi IS 'La demi journée est une disponibilité préférée du technicien';


--
-- Name: COLUMN acteur_plage_visite.vendredi_matin; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN acteur_plage_visite.vendredi_matin IS 'La demi journée est une disponibilité préférée du technicien';


--
-- Name: COLUMN acteur_plage_visite.vendredi_apresmidi; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN acteur_plage_visite.vendredi_apresmidi IS 'La demi journée est une disponibilité préférée du technicien';


--
-- Name: acteur_plage_visite_seq; Type: SEQUENCE; Schema: openaria; Owner: -
--

CREATE SEQUENCE acteur_plage_visite_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: acteur_plage_visite_seq; Type: SEQUENCE OWNED BY; Schema: openaria; Owner: -
--

ALTER SEQUENCE acteur_plage_visite_seq OWNED BY acteur_plage_visite.acteur_plage_visite;


--
-- Name: acteur_seq; Type: SEQUENCE; Schema: openaria; Owner: -
--

CREATE SEQUENCE acteur_seq
    START WITH 3
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: acteur_seq; Type: SEQUENCE OWNED BY; Schema: openaria; Owner: -
--

ALTER SEQUENCE acteur_seq OWNED BY acteur.acteur;


--
-- Name: analyses; Type: TABLE; Schema: openaria; Owner: -
--

CREATE TABLE analyses (
    analyses integer NOT NULL,
    service integer NOT NULL,
    analyses_etat character varying(100) NOT NULL,
    analyses_type integer,
    objet text,
    descriptif_etablissement_om_html text,
    reglementation_applicable_om_html text,
    compte_rendu_om_html text,
    document_presente_pendant_om_html text,
    document_presente_apres_om_html text,
    observation_om_html text,
    reunion_avis integer,
    avis_complement character varying(250),
    si_effectif_public integer,
    si_effectif_personnel integer,
    si_type_ssi character varying(100),
    si_type_alarme character varying(100),
    si_conformite_l16 boolean,
    si_alimentation_remplacement boolean,
    si_service_securite boolean,
    si_personnel_jour integer,
    si_personnel_nuit integer,
    dossier_instruction integer NOT NULL,
    modele_edition_rapport integer NOT NULL,
    modele_edition_compte_rendu integer NOT NULL,
    modele_edition_proces_verbal integer NOT NULL,
    modifiee_sans_gen boolean NOT NULL,
    dernier_pv integer,
    dec1 character varying(250),
    delai1 character varying(250),
    dec2 character varying(250),
    delai2 character varying(250)
);


--
-- Name: TABLE analyses; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON TABLE analyses IS 'Analyses réglementaire des procès verbaux';


--
-- Name: COLUMN analyses.analyses; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN analyses.analyses IS 'Identifiant unique';


--
-- Name: COLUMN analyses.service; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN analyses.service IS 'Service de l''analyses';


--
-- Name: COLUMN analyses.analyses_etat; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN analyses.analyses_etat IS 'Etat de l''analyses (en cours de rédaction, terminée, validée, actée)';


--
-- Name: COLUMN analyses.analyses_type; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN analyses.analyses_type IS 'Type d''analyses';


--
-- Name: COLUMN analyses.objet; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN analyses.objet IS 'Objet de l''analyses';


--
-- Name: COLUMN analyses.descriptif_etablissement_om_html; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN analyses.descriptif_etablissement_om_html IS 'Descriptif de l''établissement du point de vue du service de l''analyse';


--
-- Name: COLUMN analyses.reglementation_applicable_om_html; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN analyses.reglementation_applicable_om_html IS 'Réglementations applicables (champ libre avec bible)';


--
-- Name: COLUMN analyses.compte_rendu_om_html; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN analyses.compte_rendu_om_html IS 'Compte rendu';


--
-- Name: COLUMN analyses.document_presente_pendant_om_html; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN analyses.document_presente_pendant_om_html IS 'Liste des pièces fournies pendant la visite qui ont été prises en compte pour prendre la décision';


--
-- Name: COLUMN analyses.document_presente_apres_om_html; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN analyses.document_presente_apres_om_html IS 'Liste des pièces fournies après la visite qui ont été prises en compte pour prendre la décision';


--
-- Name: COLUMN analyses.observation_om_html; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN analyses.observation_om_html IS 'Observations de l''analyses';


--
-- Name: COLUMN analyses.reunion_avis; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN analyses.reunion_avis IS 'Proposition d''avis qui sera reprise lors de la réunion plénière de la commission';


--
-- Name: COLUMN analyses.avis_complement; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN analyses.avis_complement IS 'Complément à la proposition d''avis';


--
-- Name: COLUMN analyses.si_effectif_public; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN analyses.si_effectif_public IS 'Effectif public';


--
-- Name: COLUMN analyses.si_effectif_personnel; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN analyses.si_effectif_personnel IS 'Effectif personnel';


--
-- Name: COLUMN analyses.si_type_ssi; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN analyses.si_type_ssi IS 'Type SSI (A/B/C/D/E)';


--
-- Name: COLUMN analyses.si_type_alarme; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN analyses.si_type_alarme IS 'Type d''alarme (1/2a/2b/3/4)';


--
-- Name: COLUMN analyses.si_conformite_l16; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN analyses.si_conformite_l16 IS 'Conforme au référentiel L16 ?';


--
-- Name: COLUMN analyses.si_alimentation_remplacement; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN analyses.si_alimentation_remplacement IS 'Présence d''une alimentation de remplacement ?';


--
-- Name: COLUMN analyses.si_service_securite; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN analyses.si_service_securite IS 'Présence d''un service sécurité ?';


--
-- Name: COLUMN analyses.si_personnel_jour; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN analyses.si_personnel_jour IS 'Effectif personnel de jour';


--
-- Name: COLUMN analyses.si_personnel_nuit; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN analyses.si_personnel_nuit IS 'Effectif personnel de nuit';


--
-- Name: COLUMN analyses.dossier_instruction; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN analyses.dossier_instruction IS 'Dossier d''instruction auquel l''analyse est rattachée';


--
-- Name: COLUMN analyses.modele_edition_rapport; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN analyses.modele_edition_rapport IS 'Modèle d''édition du rapport d''analyse';


--
-- Name: COLUMN analyses.modele_edition_compte_rendu; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN analyses.modele_edition_compte_rendu IS 'Modèle d''édition du compte-rendu d''analyse';


--
-- Name: COLUMN analyses.modele_edition_proces_verbal; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN analyses.modele_edition_proces_verbal IS 'Modèle d''édition du procès-verbal (auquel un filigranne sera ajouté par traitement puisqu''il s''agit dans ce contexte d''une prévisualisation)';


--
-- Name: COLUMN analyses.modifiee_sans_gen; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN analyses.modifiee_sans_gen IS 'Marqueur indiquant, si vrai, qu''il n''y a pas eu de (re)génération de PV après une modification de l''analyse';


--
-- Name: COLUMN analyses.dernier_pv; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN analyses.dernier_pv IS 'Dernier procès-verbal généré (peut être regénéré)';


--
-- Name: COLUMN analyses.dec1; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN analyses.dec1 IS 'Proposition de décision d''autorité de police 1';


--
-- Name: COLUMN analyses.delai1; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN analyses.delai1 IS 'Délais 1';


--
-- Name: COLUMN analyses.dec2; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN analyses.dec2 IS 'Proposition de décision d''autorité de police 2';


--
-- Name: COLUMN analyses.delai2; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN analyses.delai2 IS 'Délais 2';


--
-- Name: analyses_seq; Type: SEQUENCE; Schema: openaria; Owner: -
--

CREATE SEQUENCE analyses_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: analyses_seq; Type: SEQUENCE OWNED BY; Schema: openaria; Owner: -
--

ALTER SEQUENCE analyses_seq OWNED BY analyses.analyses;


--
-- Name: analyses_type; Type: TABLE; Schema: openaria; Owner: -
--

CREATE TABLE analyses_type (
    analyses_type integer NOT NULL,
    service integer NOT NULL,
    code character varying(20),
    libelle character varying(100),
    description text,
    om_validite_debut date,
    om_validite_fin date,
    modele_edition_rapport integer NOT NULL,
    modele_edition_compte_rendu integer NOT NULL,
    modele_edition_proces_verbal integer NOT NULL
);


--
-- Name: TABLE analyses_type; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON TABLE analyses_type IS 'Type d''analyse';


--
-- Name: COLUMN analyses_type.analyses_type; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN analyses_type.analyses_type IS 'Identifiant unique';


--
-- Name: COLUMN analyses_type.service; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN analyses_type.service IS 'Service du type d''analyse';


--
-- Name: COLUMN analyses_type.code; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN analyses_type.code IS 'Code du type d''analyse';


--
-- Name: COLUMN analyses_type.libelle; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN analyses_type.libelle IS 'Libellé du type d''analyse';


--
-- Name: COLUMN analyses_type.description; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN analyses_type.description IS 'Description du type d''analyse';


--
-- Name: COLUMN analyses_type.om_validite_debut; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN analyses_type.om_validite_debut IS 'Date de début de validité du type d''analyse';


--
-- Name: COLUMN analyses_type.om_validite_fin; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN analyses_type.om_validite_fin IS 'Date de fin de validité du type d''analyse';


--
-- Name: COLUMN analyses_type.modele_edition_rapport; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN analyses_type.modele_edition_rapport IS 'Modèle d''édition du rapport d''analyse par défaut pour ce type';


--
-- Name: COLUMN analyses_type.modele_edition_compte_rendu; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN analyses_type.modele_edition_compte_rendu IS 'Modèle d''édition du compte-rendu d''analyse par défaut pour ce type';


--
-- Name: COLUMN analyses_type.modele_edition_proces_verbal; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN analyses_type.modele_edition_proces_verbal IS 'Modèle d''édition du procès-verbal par défaut pour ce type (auquel un filigranne sera ajouté par traitement puisqu''il s''agit dans ce contexte d''une prévisualisation)';


--
-- Name: analyses_type_seq; Type: SEQUENCE; Schema: openaria; Owner: -
--

CREATE SEQUENCE analyses_type_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: analyses_type_seq; Type: SEQUENCE OWNED BY; Schema: openaria; Owner: -
--

ALTER SEQUENCE analyses_type_seq OWNED BY analyses_type.analyses_type;


--
-- Name: arrondissement; Type: TABLE; Schema: openaria; Owner: -
--

CREATE TABLE arrondissement (
    arrondissement integer NOT NULL,
    code character varying(20),
    libelle character varying(100),
    description character varying(250),
    om_validite_debut date,
    om_validite_fin date,
    code_impots character varying(3)
);


--
-- Name: TABLE arrondissement; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON TABLE arrondissement IS 'Arrondissements des adresses dans la commune.';


--
-- Name: COLUMN arrondissement.arrondissement; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN arrondissement.arrondissement IS 'Identifiant unique';


--
-- Name: COLUMN arrondissement.code; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN arrondissement.code IS 'Code de l''arrondissement';


--
-- Name: COLUMN arrondissement.libelle; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN arrondissement.libelle IS 'Libellé en toutes lettres de l''arrondissement.';


--
-- Name: COLUMN arrondissement.description; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN arrondissement.description IS 'Description ; optionnel';


--
-- Name: COLUMN arrondissement.om_validite_debut; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN arrondissement.om_validite_debut IS 'Date de début de validité';


--
-- Name: COLUMN arrondissement.om_validite_fin; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN arrondissement.om_validite_fin IS 'Date de fin de validité.';


--
-- Name: COLUMN arrondissement.code_impots; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN arrondissement.code_impots IS 'Code impôts de l''arrondissement, notamment utilisé pour les références cadastrales afin de récupérer les références patrimoine.';


--
-- Name: arrondissement_seq; Type: SEQUENCE; Schema: openaria; Owner: -
--

CREATE SEQUENCE arrondissement_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: arrondissement_seq; Type: SEQUENCE OWNED BY; Schema: openaria; Owner: -
--

ALTER SEQUENCE arrondissement_seq OWNED BY arrondissement.arrondissement;


--
-- Name: autorite_competente; Type: TABLE; Schema: openaria; Owner: -
--

CREATE TABLE autorite_competente (
    autorite_competente integer NOT NULL,
    code character varying(10),
    libelle character varying(100),
    service integer NOT NULL
);


--
-- Name: TABLE autorite_competente; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON TABLE autorite_competente IS 'Autorité compétente des dossiers d''instruction';


--
-- Name: COLUMN autorite_competente.autorite_competente; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN autorite_competente.autorite_competente IS 'Identifiant unique.';


--
-- Name: COLUMN autorite_competente.code; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN autorite_competente.code IS 'Code de l''autorité compétente.';


--
-- Name: COLUMN autorite_competente.libelle; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN autorite_competente.libelle IS 'Code impôts du quartier.';


--
-- Name: COLUMN autorite_competente.service; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN autorite_competente.service IS 'Service de l''autorité compétente.';


--
-- Name: autorite_competente_seq; Type: SEQUENCE; Schema: openaria; Owner: -
--

CREATE SEQUENCE autorite_competente_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: autorite_competente_seq; Type: SEQUENCE OWNED BY; Schema: openaria; Owner: -
--

ALTER SEQUENCE autorite_competente_seq OWNED BY autorite_competente.autorite_competente;


--
-- Name: autorite_police; Type: TABLE; Schema: openaria; Owner: -
--

CREATE TABLE autorite_police (
    autorite_police integer NOT NULL,
    autorite_police_decision integer NOT NULL,
    date_decision date NOT NULL,
    delai integer NOT NULL,
    autorite_police_motif integer NOT NULL,
    cloture boolean DEFAULT false,
    date_notification date,
    date_butoir date,
    service integer NOT NULL,
    dossier_instruction_reunion integer,
    dossier_coordination integer,
    etablissement integer,
    dossier_instruction_reunion_prochain integer
);


--
-- Name: TABLE autorite_police; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON TABLE autorite_police IS 'Autorités de Police.';


--
-- Name: COLUMN autorite_police.autorite_police; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN autorite_police.autorite_police IS 'Identifiant unique';


--
-- Name: COLUMN autorite_police.autorite_police_decision; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN autorite_police.autorite_police_decision IS 'Décision de l''autorité de police (liste à choix).';


--
-- Name: COLUMN autorite_police.date_decision; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN autorite_police.date_decision IS 'Date de décision de l''autorité de police initialisée avec la date de la réunion qui porte la décision.';


--
-- Name: COLUMN autorite_police.delai; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN autorite_police.delai IS 'Délai de l''autorité de police initialisé par défaut avec le délai de la décision.';


--
-- Name: COLUMN autorite_police.autorite_police_motif; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN autorite_police.autorite_police_motif IS 'Motif de l''autorité de police (liste à choix).';


--
-- Name: COLUMN autorite_police.cloture; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN autorite_police.cloture IS 'L''autorité de police est clôturée.';


--
-- Name: COLUMN autorite_police.date_notification; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN autorite_police.date_notification IS 'Mise à jour par la notification du ou des courriers à la décision de l''autorité de police.';


--
-- Name: COLUMN autorite_police.date_butoir; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN autorite_police.date_butoir IS 'Date butoir de l''autorité de police.';


--
-- Name: COLUMN autorite_police.service; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN autorite_police.service IS 'Service de l''autorité de police.';


--
-- Name: COLUMN autorite_police.dossier_instruction_reunion; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN autorite_police.dossier_instruction_reunion IS 'Référence à la demande de passage en réunion.';


--
-- Name: COLUMN autorite_police.dossier_coordination; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN autorite_police.dossier_coordination IS 'Dossier de coordination de l''autorité de police.';


--
-- Name: COLUMN autorite_police.etablissement; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN autorite_police.etablissement IS 'Établissement du dossier de coordination dont l''autorité de police fait partie.';


--
-- Name: COLUMN autorite_police.dossier_instruction_reunion_prochain; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN autorite_police.dossier_instruction_reunion_prochain IS 'Prochaine demande de passage en réunion.';


--
-- Name: autorite_police_decision; Type: TABLE; Schema: openaria; Owner: -
--

CREATE TABLE autorite_police_decision (
    autorite_police_decision integer NOT NULL,
    code character varying(20),
    libelle character varying(100),
    description text,
    service integer,
    suivi_delai boolean DEFAULT false,
    avis character varying(100),
    etablissement_etat integer,
    delai integer,
    om_validite_debut date,
    om_validite_fin date,
    type_arrete boolean,
    arrete_reglementaire boolean,
    arrete_notification boolean,
    arrete_publication boolean,
    arrete_temporaire boolean,
    nomenclature_actes_nature character varying(30),
    nomenclature_actes_matiere_niv1 character varying(250),
    nomenclature_actes_matiere_niv2 character varying(250)
);


--
-- Name: TABLE autorite_police_decision; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON TABLE autorite_police_decision IS 'Décisions possible concernant une autorité de police.';


--
-- Name: COLUMN autorite_police_decision.autorite_police_decision; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN autorite_police_decision.autorite_police_decision IS 'Identifiant unique';


--
-- Name: COLUMN autorite_police_decision.code; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN autorite_police_decision.code IS 'Code de la décision de l''autorité de police.';


--
-- Name: COLUMN autorite_police_decision.libelle; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN autorite_police_decision.libelle IS 'Libellé de la décision de l''autorité de police.';


--
-- Name: COLUMN autorite_police_decision.description; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN autorite_police_decision.description IS 'Description de la décision de l''autorité de police.';


--
-- Name: COLUMN autorite_police_decision.service; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN autorite_police_decision.service IS 'Service de la décision de l''autorité de police.';


--
-- Name: COLUMN autorite_police_decision.suivi_delai; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN autorite_police_decision.suivi_delai IS 'Suivi de délai pris en compte pour la décision de l''autorité de police.';


--
-- Name: COLUMN autorite_police_decision.avis; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN autorite_police_decision.avis IS 'Utilisé pour les statistiques sur les décisions d''autorité de police et pour vérifier si un lien avec un courrier est nécessaire ou non (favorable/défavorable).';


--
-- Name: COLUMN autorite_police_decision.etablissement_etat; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN autorite_police_decision.etablissement_etat IS 'Utilisé pour mettre à jour l''état de l''établissement.';


--
-- Name: COLUMN autorite_police_decision.delai; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN autorite_police_decision.delai IS 'Délai par défaut de l''autorité de police (en jour).';


--
-- Name: COLUMN autorite_police_decision.om_validite_debut; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN autorite_police_decision.om_validite_debut IS 'Date de début de validité de la décision de l''autorité de police.';


--
-- Name: COLUMN autorite_police_decision.om_validite_fin; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN autorite_police_decision.om_validite_fin IS 'Date de fin de validité de la décision de l''autorité de police.';


--
-- Name: autorite_police_decision_seq; Type: SEQUENCE; Schema: openaria; Owner: -
--

CREATE SEQUENCE autorite_police_decision_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: autorite_police_decision_seq; Type: SEQUENCE OWNED BY; Schema: openaria; Owner: -
--

ALTER SEQUENCE autorite_police_decision_seq OWNED BY autorite_police_decision.autorite_police_decision;


--
-- Name: autorite_police_motif; Type: TABLE; Schema: openaria; Owner: -
--

CREATE TABLE autorite_police_motif (
    autorite_police_motif integer NOT NULL,
    code character varying(20),
    libelle character varying(100),
    description text,
    service integer,
    om_validite_debut date,
    om_validite_fin date
);


--
-- Name: TABLE autorite_police_motif; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON TABLE autorite_police_motif IS 'Motifs possible concernant une autorité de police.';


--
-- Name: COLUMN autorite_police_motif.autorite_police_motif; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN autorite_police_motif.autorite_police_motif IS 'Identifiant unique';


--
-- Name: COLUMN autorite_police_motif.code; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN autorite_police_motif.code IS 'Code du motif de l''autorité de police.';


--
-- Name: COLUMN autorite_police_motif.libelle; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN autorite_police_motif.libelle IS 'Libellé du motif de l''autorité de police.';


--
-- Name: COLUMN autorite_police_motif.description; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN autorite_police_motif.description IS 'Description du motif de l''autorité de police.';


--
-- Name: COLUMN autorite_police_motif.service; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN autorite_police_motif.service IS 'Service du motif de l''autorité de police.';


--
-- Name: COLUMN autorite_police_motif.om_validite_debut; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN autorite_police_motif.om_validite_debut IS 'Date de début de validité du motif de l''autorité de police.';


--
-- Name: COLUMN autorite_police_motif.om_validite_fin; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN autorite_police_motif.om_validite_fin IS 'Date de fin de validité du motif de l''autorité de police.';


--
-- Name: autorite_police_motif_seq; Type: SEQUENCE; Schema: openaria; Owner: -
--

CREATE SEQUENCE autorite_police_motif_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: autorite_police_motif_seq; Type: SEQUENCE OWNED BY; Schema: openaria; Owner: -
--

ALTER SEQUENCE autorite_police_motif_seq OWNED BY autorite_police_motif.autorite_police_motif;


--
-- Name: autorite_police_seq; Type: SEQUENCE; Schema: openaria; Owner: -
--

CREATE SEQUENCE autorite_police_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: autorite_police_seq; Type: SEQUENCE OWNED BY; Schema: openaria; Owner: -
--

ALTER SEQUENCE autorite_police_seq OWNED BY autorite_police.autorite_police;


--
-- Name: contact; Type: TABLE; Schema: openaria; Owner: -
--

CREATE TABLE contact (
    contact integer NOT NULL,
    etablissement integer,
    contact_type integer,
    civilite integer,
    nom character varying(100),
    prenom character varying(100),
    titre character varying(100),
    telephone character varying(40),
    mobile character varying(40),
    fax character varying(40),
    courriel character varying(100),
    om_validite_debut date,
    om_validite_fin date,
    adresse_numero integer,
    adresse_numero2 character varying(10),
    adresse_voie character varying(255),
    adresse_complement character varying(40),
    adresse_cp character varying(6),
    adresse_ville character varying(40),
    lieu_dit character varying(39),
    boite_postale character varying(5),
    cedex character varying(5),
    pays character varying(40),
    qualite character varying(40),
    denomination character varying(40),
    raison_sociale character varying(50),
    siret character varying(15),
    categorie_juridique character varying(15),
    reception_convocation boolean,
    service integer,
    reception_programmation boolean,
    reception_commission boolean
);


--
-- Name: TABLE contact; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON TABLE contact IS 'Contacts de l''établissement (exploitants, mandataires, etc.). Les contacts sont aussi utilisés pour les correspondances : dans ce cas on écrit à l''expoloitant.';


--
-- Name: COLUMN contact.contact; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN contact.contact IS 'identifiant unique';


--
-- Name: COLUMN contact.etablissement; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN contact.etablissement IS 'Établissement auquel est rattaché le contact';


--
-- Name: COLUMN contact.contact_type; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN contact.contact_type IS 'Type de contact (mandataire, exploitant etc.)';


--
-- Name: COLUMN contact.civilite; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN contact.civilite IS 'Civilité du contact (M. Mme etc.)';


--
-- Name: COLUMN contact.nom; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN contact.nom IS 'nom du contact';


--
-- Name: COLUMN contact.prenom; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN contact.prenom IS 'Prénom du contact';


--
-- Name: COLUMN contact.titre; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN contact.titre IS 'Titre officiel du contact. Par exemple "M. le Président".';


--
-- Name: COLUMN contact.telephone; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN contact.telephone IS 'téléphone principal';


--
-- Name: COLUMN contact.mobile; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN contact.mobile IS 'numéro de téléphone mobile';


--
-- Name: COLUMN contact.fax; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN contact.fax IS 'numéro de fax';


--
-- Name: COLUMN contact.courriel; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN contact.courriel IS 'adresse de courriel';


--
-- Name: COLUMN contact.om_validite_debut; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN contact.om_validite_debut IS 'date de début de validité du contact (par défaut la date de création)';


--
-- Name: COLUMN contact.om_validite_fin; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN contact.om_validite_fin IS 'date de fin de validité du contact (par défaut la date de "suppression")';


--
-- Name: COLUMN contact.adresse_numero; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN contact.adresse_numero IS 'Numéro d''adresse (1245, 51, 3, etc.)';


--
-- Name: COLUMN contact.adresse_numero2; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN contact.adresse_numero2 IS 'Texte du numéro d''adresse (bis, ter, etc.)';


--
-- Name: COLUMN contact.adresse_voie; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN contact.adresse_voie IS 'Référence vers la voie dans la table des voies';


--
-- Name: COLUMN contact.adresse_complement; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN contact.adresse_complement IS 'Complément d''adresse en texte libre';


--
-- Name: COLUMN contact.adresse_cp; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN contact.adresse_cp IS 'Code postal de l''adresse';


--
-- Name: COLUMN contact.adresse_ville; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN contact.adresse_ville IS 'Ville de l''adresse';


--
-- Name: COLUMN contact.lieu_dit; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN contact.lieu_dit IS 'Lieu-dit de l''adresse du contact.';


--
-- Name: COLUMN contact.boite_postale; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN contact.boite_postale IS 'Boîte postale de l''adresse du contact.';


--
-- Name: COLUMN contact.cedex; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN contact.cedex IS 'Cedex de l''adresse du contact.';


--
-- Name: COLUMN contact.pays; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN contact.pays IS 'Pays de l''adresse du contact.';


--
-- Name: COLUMN contact.qualite; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN contact.qualite IS 'Qualité du contact : particulier ou personne morale.';


--
-- Name: COLUMN contact.denomination; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN contact.denomination IS 'Dénomination du contact dans le cas qu''il soit une personne morale.';


--
-- Name: COLUMN contact.raison_sociale; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN contact.raison_sociale IS 'Raison sociale du contact dans le cas qu''il soit une personne morale.';


--
-- Name: COLUMN contact.siret; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN contact.siret IS 'SIRET du contact dans le cas qu''il soit une personne morale.';


--
-- Name: COLUMN contact.categorie_juridique; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN contact.categorie_juridique IS 'Catégorie juridique du contact dans le cas qu''il soit une personne morale.';


--
-- Name: COLUMN contact.service; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN contact.service IS 'Service du contact insitutionnel.';


--
-- Name: COLUMN contact.reception_programmation; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN contact.reception_programmation IS 'Réception de la programmation.';


--
-- Name: COLUMN contact.reception_commission; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN contact.reception_commission IS 'Réception des éditions liées aux commissions.';


--
-- Name: contact_civilite; Type: TABLE; Schema: openaria; Owner: -
--

CREATE TABLE contact_civilite (
    contact_civilite integer NOT NULL,
    libelle character varying(10)
);


--
-- Name: TABLE contact_civilite; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON TABLE contact_civilite IS 'Liste des civilités';


--
-- Name: COLUMN contact_civilite.contact_civilite; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN contact_civilite.contact_civilite IS 'Identifiant unique de la civilité';


--
-- Name: COLUMN contact_civilite.libelle; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN contact_civilite.libelle IS 'Texte de la civilité';


--
-- Name: contact_civilite_seq; Type: SEQUENCE; Schema: openaria; Owner: -
--

CREATE SEQUENCE contact_civilite_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: contact_civilite_seq; Type: SEQUENCE OWNED BY; Schema: openaria; Owner: -
--

ALTER SEQUENCE contact_civilite_seq OWNED BY contact_civilite.contact_civilite;


--
-- Name: contact_seq; Type: SEQUENCE; Schema: openaria; Owner: -
--

CREATE SEQUENCE contact_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: contact_seq; Type: SEQUENCE OWNED BY; Schema: openaria; Owner: -
--

ALTER SEQUENCE contact_seq OWNED BY contact.contact;


--
-- Name: contact_type; Type: TABLE; Schema: openaria; Owner: -
--

CREATE TABLE contact_type (
    contact_type integer NOT NULL,
    libelle character varying(100),
    code character varying(20),
    description character varying(250),
    om_validite_debut date,
    om_validite_fin date
);


--
-- Name: TABLE contact_type; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON TABLE contact_type IS 'Type de contact (Exploitant etc.)';


--
-- Name: COLUMN contact_type.contact_type; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN contact_type.contact_type IS 'Identifiant unique';


--
-- Name: COLUMN contact_type.libelle; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN contact_type.libelle IS 'Libellé en toutes lettres du type de contact';


--
-- Name: COLUMN contact_type.code; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN contact_type.code IS 'Code du type de contact';


--
-- Name: COLUMN contact_type.description; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN contact_type.description IS 'Description détaillée du type de contact';


--
-- Name: COLUMN contact_type.om_validite_debut; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN contact_type.om_validite_debut IS 'Date de début de validité';


--
-- Name: COLUMN contact_type.om_validite_fin; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN contact_type.om_validite_fin IS 'Date de fin de validité';


--
-- Name: contact_type_seq; Type: SEQUENCE; Schema: openaria; Owner: -
--

CREATE SEQUENCE contact_type_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: contact_type_seq; Type: SEQUENCE OWNED BY; Schema: openaria; Owner: -
--

ALTER SEQUENCE contact_type_seq OWNED BY contact_type.contact_type;


--
-- Name: contrainte; Type: TABLE; Schema: openaria; Owner: -
--

CREATE TABLE contrainte (
    contrainte integer NOT NULL,
    id_referentiel character varying(250),
    nature character varying(10) NOT NULL,
    groupe character varying(250),
    sousgroupe character varying(250),
    libelle character varying(250) NOT NULL,
    texte text,
    texte_surcharge text,
    ordre_d_affichage integer,
    lie_a_un_referentiel boolean DEFAULT false,
    om_validite_debut date,
    om_validite_fin date
);


--
-- Name: TABLE contrainte; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON TABLE contrainte IS 'Table de référence des contraintes';


--
-- Name: COLUMN contrainte.contrainte; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN contrainte.contrainte IS 'Identifiant unique.';


--
-- Name: COLUMN contrainte.id_referentiel; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN contrainte.id_referentiel IS 'Correspond à l’identifiant dans le référentiel SIG.';


--
-- Name: COLUMN contrainte.nature; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN contrainte.nature IS 'Nature de la contrainte (POS/PLU/CC/RNU).';


--
-- Name: COLUMN contrainte.groupe; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN contrainte.groupe IS 'Texte libre représentant la catégorie de la contrainte.';


--
-- Name: COLUMN contrainte.sousgroupe; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN contrainte.sousgroupe IS 'Texte libre représentant la sous-catégorie de la contrainte.';


--
-- Name: COLUMN contrainte.libelle; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN contrainte.libelle IS 'Phrase résumé de la contrainte utilisée dans les interfaces de sélection.';


--
-- Name: COLUMN contrainte.texte; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN contrainte.texte IS 'Texte de la contrainte à respecter.';


--
-- Name: COLUMN contrainte.texte_surcharge; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN contrainte.texte_surcharge IS 'Éventuelle surcharge du texte standard.';


--
-- Name: COLUMN contrainte.ordre_d_affichage; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN contrainte.ordre_d_affichage IS 'Éventuelle clé de tri de la contrainte à l''intérieur de son groupe et sous-groupe.';


--
-- Name: COLUMN contrainte.lie_a_un_referentiel; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN contrainte.lie_a_un_referentiel IS 'Si vrai, contrainte récupérée depuis le SIG.';


--
-- Name: COLUMN contrainte.om_validite_debut; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN contrainte.om_validite_debut IS 'Date de début de validité. Permet l''archivage de la contrainte.';


--
-- Name: COLUMN contrainte.om_validite_fin; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN contrainte.om_validite_fin IS 'Date de fin de validité. Permet l''archivage de la contrainte.';


--
-- Name: contrainte_seq; Type: SEQUENCE; Schema: openaria; Owner: -
--

CREATE SEQUENCE contrainte_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: contrainte_seq; Type: SEQUENCE OWNED BY; Schema: openaria; Owner: -
--

ALTER SEQUENCE contrainte_seq OWNED BY contrainte.contrainte;


--
-- Name: courrier; Type: TABLE; Schema: openaria; Owner: -
--

CREATE TABLE courrier (
    courrier integer NOT NULL,
    etablissement integer,
    dossier_coordination integer,
    dossier_instruction integer,
    complement1_om_html text,
    complement2_om_html text,
    finalise boolean DEFAULT false NOT NULL,
    om_fichier_finalise_courrier character varying(64),
    om_fichier_signe_courrier character varying(64),
    om_date_creation date,
    date_finalisation date,
    date_envoi_signature date,
    date_retour_signature date,
    date_envoi_controle_legalite date,
    date_retour_controle_legalite date,
    date_envoi_rar date,
    date_retour_rar date,
    code_barres character varying(12) NOT NULL,
    date_envoi_mail_om_fichier_finalise_courrier date,
    date_envoi_mail_om_fichier_signe_courrier date,
    mailing boolean DEFAULT false,
    courrier_parent integer,
    courrier_joint integer,
    modele_edition integer NOT NULL,
    signataire integer,
    courrier_type integer NOT NULL,
    proces_verbal integer,
    visite integer,
    arrete_numero character varying(30)
);


--
-- Name: TABLE courrier; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON TABLE courrier IS 'Liste des courriers sortants (documents générés)';


--
-- Name: COLUMN courrier.courrier; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN courrier.courrier IS 'Identifiant unique.';


--
-- Name: COLUMN courrier.etablissement; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN courrier.etablissement IS 'Lien vers l''établissement.';


--
-- Name: COLUMN courrier.dossier_coordination; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN courrier.dossier_coordination IS 'Lien vers le dossier de coordination.';


--
-- Name: COLUMN courrier.dossier_instruction; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN courrier.dossier_instruction IS 'Lien vers le dossier d''instruction.';


--
-- Name: COLUMN courrier.complement1_om_html; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN courrier.complement1_om_html IS 'Texte de complément 1 du courrier.';


--
-- Name: COLUMN courrier.complement2_om_html; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN courrier.complement2_om_html IS 'Texte de complément 2 du courrier.';


--
-- Name: COLUMN courrier.finalise; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN courrier.finalise IS 'Indique que le courrier est finalisé.';


--
-- Name: COLUMN courrier.om_fichier_finalise_courrier; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN courrier.om_fichier_finalise_courrier IS 'Document généré à la finalisation du courrier.';


--
-- Name: COLUMN courrier.om_fichier_signe_courrier; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN courrier.om_fichier_signe_courrier IS 'Document généré signé du courrier.';


--
-- Name: COLUMN courrier.om_date_creation; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN courrier.om_date_creation IS 'Date de création du courrier.';


--
-- Name: COLUMN courrier.date_finalisation; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN courrier.date_finalisation IS 'Date de finalisation du courrier.';


--
-- Name: COLUMN courrier.date_envoi_signature; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN courrier.date_envoi_signature IS 'Date d''envoi en signature du courrier.';


--
-- Name: COLUMN courrier.date_retour_signature; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN courrier.date_retour_signature IS 'Date de retour de signature du courrier.';


--
-- Name: COLUMN courrier.date_envoi_controle_legalite; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN courrier.date_envoi_controle_legalite IS 'Date d''envoi en contrôle légalité du courrier.';


--
-- Name: COLUMN courrier.date_retour_controle_legalite; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN courrier.date_retour_controle_legalite IS 'Date de retour du contrôle légalité du courrier.';


--
-- Name: COLUMN courrier.date_envoi_rar; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN courrier.date_envoi_rar IS 'Date d''envoi en retour AR aux destinataires du courrier.';


--
-- Name: COLUMN courrier.date_retour_rar; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN courrier.date_retour_rar IS 'Date de notification des destinataires du courrier.';


--
-- Name: COLUMN courrier.code_barres; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN courrier.code_barres IS 'Numéro du code barres correspondant au courrier.';


--
-- Name: COLUMN courrier.date_envoi_mail_om_fichier_finalise_courrier; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN courrier.date_envoi_mail_om_fichier_finalise_courrier IS 'Date d''envoi par mail du fichier finalisé du courrier.';


--
-- Name: COLUMN courrier.date_envoi_mail_om_fichier_signe_courrier; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN courrier.date_envoi_mail_om_fichier_signe_courrier IS 'Date d''envoi par mail du fichier signé du courrier.';


--
-- Name: COLUMN courrier.mailing; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN courrier.mailing IS 'Indique que c''est un mailing.';


--
-- Name: COLUMN courrier.courrier_parent; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN courrier.courrier_parent IS 'Courrier mailing auquel est rattaché le courrier.';


--
-- Name: COLUMN courrier.courrier_joint; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN courrier.courrier_joint IS 'Courrier joint du courrier.';


--
-- Name: COLUMN courrier.modele_edition; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN courrier.modele_edition IS 'Lien vers le modèle d''édition.';


--
-- Name: COLUMN courrier.signataire; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN courrier.signataire IS 'Lien vers le signataire du courrier.';


--
-- Name: COLUMN courrier.courrier_type; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN courrier.courrier_type IS 'Type du courrier, permet de filtrer les modèles d''éditions à sélectionner .';


--
-- Name: COLUMN courrier.proces_verbal; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN courrier.proces_verbal IS 'Lien vers le procès-verbal.';


--
-- Name: COLUMN courrier.visite; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN courrier.visite IS 'Lien vers la visite.';


--
-- Name: courrier_seq; Type: SEQUENCE; Schema: openaria; Owner: -
--

CREATE SEQUENCE courrier_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: courrier_seq; Type: SEQUENCE OWNED BY; Schema: openaria; Owner: -
--

ALTER SEQUENCE courrier_seq OWNED BY courrier.courrier;


--
-- Name: courrier_texte_type; Type: TABLE; Schema: openaria; Owner: -
--

CREATE TABLE courrier_texte_type (
    courrier_texte_type integer NOT NULL,
    libelle character varying(60),
    courrier_type integer,
    contenu_om_html text,
    om_validite_debut date,
    om_validite_fin date
);


--
-- Name: TABLE courrier_texte_type; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON TABLE courrier_texte_type IS 'Liste des textes types utilisable sur les courriers sortants (documents générés).';


--
-- Name: COLUMN courrier_texte_type.courrier_texte_type; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN courrier_texte_type.courrier_texte_type IS 'Identifiant unique.';


--
-- Name: COLUMN courrier_texte_type.libelle; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN courrier_texte_type.libelle IS 'Libellé du texte type.';


--
-- Name: COLUMN courrier_texte_type.courrier_type; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN courrier_texte_type.courrier_type IS 'Type du courrier sur lequel le texte type est applicable.';


--
-- Name: COLUMN courrier_texte_type.contenu_om_html; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN courrier_texte_type.contenu_om_html IS 'Contenu du texte type.';


--
-- Name: COLUMN courrier_texte_type.om_validite_debut; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN courrier_texte_type.om_validite_debut IS 'Date de début de validité du texte type.';


--
-- Name: COLUMN courrier_texte_type.om_validite_fin; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN courrier_texte_type.om_validite_fin IS 'Date de fin de validité du texte type.';


--
-- Name: courrier_texte_type_seq; Type: SEQUENCE; Schema: openaria; Owner: -
--

CREATE SEQUENCE courrier_texte_type_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: courrier_texte_type_seq; Type: SEQUENCE OWNED BY; Schema: openaria; Owner: -
--

ALTER SEQUENCE courrier_texte_type_seq OWNED BY courrier_texte_type.courrier_texte_type;


--
-- Name: courrier_type; Type: TABLE; Schema: openaria; Owner: -
--

CREATE TABLE courrier_type (
    courrier_type integer NOT NULL,
    code character varying(20) NOT NULL,
    libelle character varying(100) NOT NULL,
    description text,
    om_validite_debut date,
    om_validite_fin date,
    courrier_type_categorie integer NOT NULL,
    service integer
);


--
-- Name: TABLE courrier_type; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON TABLE courrier_type IS 'Les types de courrier (événement, décision et incomplétude)';


--
-- Name: COLUMN courrier_type.courrier_type; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN courrier_type.courrier_type IS 'Identifiant unique.';


--
-- Name: COLUMN courrier_type.code; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN courrier_type.code IS 'Code du type de courrier.';


--
-- Name: COLUMN courrier_type.libelle; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN courrier_type.libelle IS 'Libellé du type de courrier.';


--
-- Name: COLUMN courrier_type.description; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN courrier_type.description IS 'Description du type de courrier.';


--
-- Name: COLUMN courrier_type.om_validite_debut; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN courrier_type.om_validite_debut IS 'Date de début de validité du type de courrier.';


--
-- Name: COLUMN courrier_type.om_validite_fin; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN courrier_type.om_validite_fin IS 'Date de fin de validité du type de courrier.';


--
-- Name: COLUMN courrier_type.courrier_type_categorie; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN courrier_type.courrier_type_categorie IS 'Catégorie du type de courrier.';


--
-- Name: COLUMN courrier_type.service; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN courrier_type.service IS 'Service du type de courrier.';


--
-- Name: courrier_type_categorie; Type: TABLE; Schema: openaria; Owner: -
--

CREATE TABLE courrier_type_categorie (
    courrier_type_categorie integer NOT NULL,
    code character varying(20) NOT NULL,
    libelle character varying(250) NOT NULL,
    description text,
    om_validite_debut date,
    om_validite_fin date,
    objet character varying(255) NOT NULL
);


--
-- Name: TABLE courrier_type_categorie; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON TABLE courrier_type_categorie IS 'Liste des catégories des types de courrier, cette table ne sera pas paramétrable.';


--
-- Name: COLUMN courrier_type_categorie.courrier_type_categorie; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN courrier_type_categorie.courrier_type_categorie IS 'Identifiant unique';


--
-- Name: COLUMN courrier_type_categorie.code; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN courrier_type_categorie.code IS 'Code de la catégorie du type de courrier.';


--
-- Name: COLUMN courrier_type_categorie.libelle; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN courrier_type_categorie.libelle IS 'Libellé de la catégorie du type de courrier.';


--
-- Name: COLUMN courrier_type_categorie.description; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN courrier_type_categorie.description IS 'Description de la catégorie du type de courrier.';


--
-- Name: COLUMN courrier_type_categorie.om_validite_debut; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN courrier_type_categorie.om_validite_debut IS 'Date de début de validité de la catégorie du type de courrier.';


--
-- Name: COLUMN courrier_type_categorie.om_validite_fin; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN courrier_type_categorie.om_validite_fin IS 'Date de fin de validité de la catégorie du type de courrier.';


--
-- Name: courrier_type_categorie_seq; Type: SEQUENCE; Schema: openaria; Owner: -
--

CREATE SEQUENCE courrier_type_categorie_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: courrier_type_categorie_seq; Type: SEQUENCE OWNED BY; Schema: openaria; Owner: -
--

ALTER SEQUENCE courrier_type_categorie_seq OWNED BY courrier_type_categorie.courrier_type_categorie;


--
-- Name: courrier_type_seq; Type: SEQUENCE; Schema: openaria; Owner: -
--

CREATE SEQUENCE courrier_type_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: courrier_type_seq; Type: SEQUENCE OWNED BY; Schema: openaria; Owner: -
--

ALTER SEQUENCE courrier_type_seq OWNED BY courrier_type.courrier_type;


--
-- Name: derogation_scda; Type: TABLE; Schema: openaria; Owner: -
--

CREATE TABLE derogation_scda (
    derogation_scda integer NOT NULL,
    code character varying(5),
    libelle character varying(50),
    om_validite_debut date,
    om_validite_fin date
);


--
-- Name: TABLE derogation_scda; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON TABLE derogation_scda IS 'Multiselect de dérogations SCDA pour les établissements';


--
-- Name: COLUMN derogation_scda.derogation_scda; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN derogation_scda.derogation_scda IS 'Identifiant unique';


--
-- Name: COLUMN derogation_scda.code; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN derogation_scda.code IS 'Libellé court';


--
-- Name: COLUMN derogation_scda.libelle; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN derogation_scda.libelle IS 'Libellé long';


--
-- Name: COLUMN derogation_scda.om_validite_debut; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN derogation_scda.om_validite_debut IS 'Gestion de la validité du paramètre';


--
-- Name: COLUMN derogation_scda.om_validite_fin; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN derogation_scda.om_validite_fin IS 'Gestion de la validité du paramètre';


--
-- Name: derogation_scda_seq; Type: SEQUENCE; Schema: openaria; Owner: -
--

CREATE SEQUENCE derogation_scda_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: derogation_scda_seq; Type: SEQUENCE OWNED BY; Schema: openaria; Owner: -
--

ALTER SEQUENCE derogation_scda_seq OWNED BY derogation_scda.derogation_scda;


--
-- Name: document_presente; Type: TABLE; Schema: openaria; Owner: -
--

CREATE TABLE document_presente (
    document_presente integer NOT NULL,
    service integer NOT NULL,
    libelle character varying(100),
    description_om_html text,
    om_validite_debut date,
    om_validite_fin date
);


--
-- Name: TABLE document_presente; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON TABLE document_presente IS 'Documents présentés';


--
-- Name: COLUMN document_presente.document_presente; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN document_presente.document_presente IS 'Identifiant unique';


--
-- Name: COLUMN document_presente.service; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN document_presente.service IS 'Service du document présenté';


--
-- Name: COLUMN document_presente.libelle; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN document_presente.libelle IS 'Libellé du document présenté';


--
-- Name: COLUMN document_presente.description_om_html; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN document_presente.description_om_html IS 'Description du document présenté';


--
-- Name: COLUMN document_presente.om_validite_debut; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN document_presente.om_validite_debut IS 'Date de début de validité du document présenté';


--
-- Name: COLUMN document_presente.om_validite_fin; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN document_presente.om_validite_fin IS 'Date de fin de validité du document présenté';


--
-- Name: document_presente_seq; Type: SEQUENCE; Schema: openaria; Owner: -
--

CREATE SEQUENCE document_presente_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: document_presente_seq; Type: SEQUENCE OWNED BY; Schema: openaria; Owner: -
--

ALTER SEQUENCE document_presente_seq OWNED BY document_presente.document_presente;


--
-- Name: dossier_coordination; Type: TABLE; Schema: openaria; Owner: -
--

CREATE TABLE dossier_coordination (
    dossier_coordination integer NOT NULL,
    etablissement integer,
    libelle character varying(100),
    dossier_coordination_type integer NOT NULL,
    date_demande date NOT NULL,
    date_butoir date,
    dossier_autorisation_ads character varying(20),
    dossier_instruction_ads character varying(30),
    a_qualifier boolean DEFAULT true NOT NULL,
    etablissement_type integer,
    etablissement_categorie integer,
    erp boolean DEFAULT true NOT NULL,
    dossier_cloture boolean DEFAULT false NOT NULL,
    contraintes_urba_om_html text,
    etablissement_locaux_sommeil boolean,
    references_cadastrales text,
    dossier_coordination_parent integer,
    description text,
    dossier_instruction_secu boolean DEFAULT false NOT NULL,
    dossier_instruction_acc boolean DEFAULT false NOT NULL,
    autorite_police_encours boolean DEFAULT false NOT NULL,
    date_cloture date,
    geolocalise boolean DEFAULT false,
    interface_referentiel_ads boolean DEFAULT false,
    enjeu_erp boolean,
    depot_de_piece boolean,
    enjeu_ads boolean DEFAULT false NOT NULL,
    geom_point public.geometry,
    geom_emprise public.geometry,
    dossier_ads_force boolean
);


--
-- Name: TABLE dossier_coordination; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON TABLE dossier_coordination IS 'Dossiers de coordination pouvant comporter des dossiers d''instruction accessibilité et/ou sécurité';


--
-- Name: COLUMN dossier_coordination.dossier_coordination; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN dossier_coordination.dossier_coordination IS 'Identifiant unique.';


--
-- Name: COLUMN dossier_coordination.etablissement; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN dossier_coordination.etablissement IS 'Établissement concerné par le dossier de coordination';


--
-- Name: COLUMN dossier_coordination.libelle; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN dossier_coordination.libelle IS 'Libellé du dossier de coordination, calculé automatiquement à la création puis mis à jour automatiquement lors de la qualification. libelle = dossier_coordination_type.code + ''-'' + dossier_type.code + ''-'' + dossier_coordination (sur 6 caractères numériques)';


--
-- Name: COLUMN dossier_coordination.dossier_coordination_type; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN dossier_coordination.dossier_coordination_type IS 'Type de dossier de coordination.';


--
-- Name: COLUMN dossier_coordination.date_demande; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN dossier_coordination.date_demande IS 'Date de dépôt de la demande';


--
-- Name: COLUMN dossier_coordination.date_butoir; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN dossier_coordination.date_butoir IS 'Date limite de réponse';


--
-- Name: COLUMN dossier_coordination.dossier_autorisation_ads; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN dossier_coordination.dossier_autorisation_ads IS 'Identifiant du dossier d''autorisation ADS (dossier d''instruction sans suffixe type ''P0''), récupérable depuis openADS';


--
-- Name: COLUMN dossier_coordination.dossier_instruction_ads; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN dossier_coordination.dossier_instruction_ads IS 'Numéro de dossier d''instruction ADS, récupérable depuis openADS';


--
-- Name: COLUMN dossier_coordination.a_qualifier; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN dossier_coordination.a_qualifier IS 'À [true] le dossier de coordination doit être qualifié (initialisé depuis dossier_coordination_type)';


--
-- Name: COLUMN dossier_coordination.etablissement_type; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN dossier_coordination.etablissement_type IS 'Type de l''établissement si le dossier de coordination doit modifier l''établissement.';


--
-- Name: COLUMN dossier_coordination.etablissement_categorie; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN dossier_coordination.etablissement_categorie IS 'Catégorie de l''établissement si le dossier de coordination doit modifier l''établissement';


--
-- Name: COLUMN dossier_coordination.erp; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN dossier_coordination.erp IS 'L''établissement est un ERP si le dossier de coordination doit modifier l''établissement. À modifier lors de la qualification';


--
-- Name: COLUMN dossier_coordination.dossier_cloture; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN dossier_coordination.dossier_cloture IS 'À [true] le dossier de coordination est clôturé';


--
-- Name: COLUMN dossier_coordination.contraintes_urba_om_html; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN dossier_coordination.contraintes_urba_om_html IS 'Contraintes POS ou PLU, récupérées depuis l''Urbanisme et gérées au sein d''un bloc de texte riche.';


--
-- Name: COLUMN dossier_coordination.etablissement_locaux_sommeil; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN dossier_coordination.etablissement_locaux_sommeil IS 'L''établissement a des locaux à sommeil si le dossier de coordination doit modifier l''établissement';


--
-- Name: COLUMN dossier_coordination.references_cadastrales; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN dossier_coordination.references_cadastrales IS 'Références cadastrales du dossier de coordination, récupérable depuis openADS';


--
-- Name: COLUMN dossier_coordination.dossier_coordination_parent; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN dossier_coordination.dossier_coordination_parent IS 'Dossier de coordination auquel est rattaché le dossier de coordination';


--
-- Name: COLUMN dossier_coordination.description; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN dossier_coordination.description IS 'Description détaillée du dossier de coordination';


--
-- Name: COLUMN dossier_coordination.dossier_instruction_secu; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN dossier_coordination.dossier_instruction_secu IS 'À [true] création automatique d''un dossier d''instruction de sécurité à la fin de la qualification du dossier de coordination (initialisé depuis dossier_coordination_type)';


--
-- Name: COLUMN dossier_coordination.dossier_instruction_acc; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN dossier_coordination.dossier_instruction_acc IS 'À [true] création automatique d''un dossier d''instruction d''accessibilité à la fin de la qualification du dossier de coordination (initialisé depuis dossier_coordination_type)';


--
-- Name: COLUMN dossier_coordination.autorite_police_encours; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN dossier_coordination.autorite_police_encours IS 'Autorité de police en cours sur le dossier de coordination, mise à jour automatiquement';


--
-- Name: COLUMN dossier_coordination.date_cloture; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN dossier_coordination.date_cloture IS 'Date de clôture du dossier de coordination';


--
-- Name: COLUMN dossier_coordination.geolocalise; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN dossier_coordination.geolocalise IS 'Marqueur signalant que le dossier a été localisé sur le SIG';


--
-- Name: COLUMN dossier_coordination.enjeu_ads; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN dossier_coordination.enjeu_ads IS 'Marqueur indiquant que le dossier est à enjeu dans ADS';


--
-- Name: dossier_coordination_message; Type: TABLE; Schema: openaria; Owner: -
--

CREATE TABLE dossier_coordination_message (
    dossier_coordination_message integer NOT NULL,
    categorie character varying(20) NOT NULL,
    dossier_coordination integer,
    type character varying(60),
    emetteur character varying(40),
    date_emission timestamp without time zone NOT NULL,
    contenu text,
    contenu_json text,
    si_cadre_lu boolean,
    si_technicien_lu boolean,
    si_mode_lecture character varying(10),
    acc_cadre_lu boolean,
    acc_technicien_lu boolean,
    acc_mode_lecture character varying(10)
);


--
-- Name: dossier_coordination_message_seq; Type: SEQUENCE; Schema: openaria; Owner: -
--

CREATE SEQUENCE dossier_coordination_message_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: dossier_coordination_message_seq; Type: SEQUENCE OWNED BY; Schema: openaria; Owner: -
--

ALTER SEQUENCE dossier_coordination_message_seq OWNED BY dossier_coordination_message.dossier_coordination_message;


--
-- Name: dossier_coordination_parcelle; Type: TABLE; Schema: openaria; Owner: -
--

CREATE TABLE dossier_coordination_parcelle (
    dossier_coordination_parcelle integer NOT NULL,
    dossier_coordination integer,
    ref_cadastre character varying(20)
);


--
-- Name: TABLE dossier_coordination_parcelle; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON TABLE dossier_coordination_parcelle IS 'Liste des parcelles du dossier de coordination ; ces données sont utilisées pour les recherches et les liaisons externes.';


--
-- Name: COLUMN dossier_coordination_parcelle.dossier_coordination_parcelle; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN dossier_coordination_parcelle.dossier_coordination_parcelle IS 'Identifiant unique';


--
-- Name: COLUMN dossier_coordination_parcelle.dossier_coordination; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN dossier_coordination_parcelle.dossier_coordination IS 'Lien vers le dossier de coordination concerné';


--
-- Name: COLUMN dossier_coordination_parcelle.ref_cadastre; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN dossier_coordination_parcelle.ref_cadastre IS 'Référence cadastrale de la parcelle';


--
-- Name: dossier_coordination_parcelle_seq; Type: SEQUENCE; Schema: openaria; Owner: -
--

CREATE SEQUENCE dossier_coordination_parcelle_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: dossier_coordination_parcelle_seq; Type: SEQUENCE OWNED BY; Schema: openaria; Owner: -
--

ALTER SEQUENCE dossier_coordination_parcelle_seq OWNED BY dossier_coordination_parcelle.dossier_coordination_parcelle;


--
-- Name: dossier_coordination_seq; Type: SEQUENCE; Schema: openaria; Owner: -
--

CREATE SEQUENCE dossier_coordination_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: dossier_coordination_seq; Type: SEQUENCE OWNED BY; Schema: openaria; Owner: -
--

ALTER SEQUENCE dossier_coordination_seq OWNED BY dossier_coordination.dossier_coordination;


--
-- Name: dossier_coordination_type; Type: TABLE; Schema: openaria; Owner: -
--

CREATE TABLE dossier_coordination_type (
    dossier_coordination_type integer NOT NULL,
    code character varying(20),
    libelle character varying(100),
    description text,
    om_validite_debut date,
    om_validite_fin date,
    dossier_type integer NOT NULL,
    dossier_instruction_secu boolean DEFAULT false NOT NULL,
    dossier_instruction_acc boolean DEFAULT false NOT NULL,
    a_qualifier boolean DEFAULT false NOT NULL,
    analyses_type_si integer NOT NULL,
    analyses_type_acc integer NOT NULL,
    piece_attendue_si text,
    piece_attendue_acc text
);


--
-- Name: TABLE dossier_coordination_type; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON TABLE dossier_coordination_type IS 'Tous les types de dossiers de coordination';


--
-- Name: COLUMN dossier_coordination_type.dossier_coordination_type; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN dossier_coordination_type.dossier_coordination_type IS 'Identifiant unique.';


--
-- Name: COLUMN dossier_coordination_type.code; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN dossier_coordination_type.code IS 'Code abrégé du type de dossier de coordination afin de générer des identifiants de dossiers par exemple, ou pour un affichage synthétique.';


--
-- Name: COLUMN dossier_coordination_type.libelle; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN dossier_coordination_type.libelle IS 'Libellé long du type de dossier de coordination, utilisé pour les affichages des listes déroulantes par exemple.';


--
-- Name: COLUMN dossier_coordination_type.description; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN dossier_coordination_type.description IS 'Description détaillée du type de dossier de coordination.';


--
-- Name: COLUMN dossier_coordination_type.om_validite_debut; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN dossier_coordination_type.om_validite_debut IS 'Date de début de validité du type de dossier de coordination.';


--
-- Name: COLUMN dossier_coordination_type.om_validite_fin; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN dossier_coordination_type.om_validite_fin IS 'Date de fin de validité du type de dossier de coordination.';


--
-- Name: COLUMN dossier_coordination_type.dossier_type; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN dossier_coordination_type.dossier_type IS 'Type du dossier : PLAN ou VISITE.';


--
-- Name: COLUMN dossier_coordination_type.dossier_instruction_secu; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN dossier_coordination_type.dossier_instruction_secu IS 'À [true] création automatique d''un dossier d''instruction de sécurité à la fin de la qualification du dossier de coordination.';


--
-- Name: COLUMN dossier_coordination_type.dossier_instruction_acc; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN dossier_coordination_type.dossier_instruction_acc IS 'À [true] création automatique d''un dossier d''instruction d''accessibilité à la fin de la qualification du dossier de coordination.';


--
-- Name: COLUMN dossier_coordination_type.a_qualifier; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN dossier_coordination_type.a_qualifier IS 'À [true] le dossier de coordination doit être qualifié.';


--
-- Name: COLUMN dossier_coordination_type.analyses_type_si; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN dossier_coordination_type.analyses_type_si IS 'Type d''analyse sécurité par défaut';


--
-- Name: COLUMN dossier_coordination_type.analyses_type_acc; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN dossier_coordination_type.analyses_type_acc IS 'Type d''analyse accessibilité par défaut';


--
-- Name: dossier_coordination_type_seq; Type: SEQUENCE; Schema: openaria; Owner: -
--

CREATE SEQUENCE dossier_coordination_type_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: dossier_coordination_type_seq; Type: SEQUENCE OWNED BY; Schema: openaria; Owner: -
--

ALTER SEQUENCE dossier_coordination_type_seq OWNED BY dossier_coordination_type.dossier_coordination_type;


--
-- Name: dossier_instruction; Type: TABLE; Schema: openaria; Owner: -
--

CREATE TABLE dossier_instruction (
    dossier_instruction integer NOT NULL,
    libelle character varying(100),
    dossier_coordination integer NOT NULL,
    technicien integer,
    service integer NOT NULL,
    a_qualifier boolean DEFAULT true NOT NULL,
    incompletude boolean DEFAULT false NOT NULL,
    piece_attendue text,
    description text,
    notes text,
    autorite_competente integer,
    dossier_cloture boolean DEFAULT false NOT NULL,
    prioritaire boolean DEFAULT false,
    statut character varying(255),
    date_cloture date,
    date_ouverture date
);


--
-- Name: TABLE dossier_instruction; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON TABLE dossier_instruction IS 'Dossier d''instruction (DI) destiné à l''analyse du dossier de coordination (DC) par un service, Accessibilité ou Sécurité. Le DI est obligatoirement rattaché à un DC.';


--
-- Name: COLUMN dossier_instruction.dossier_instruction; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN dossier_instruction.dossier_instruction IS 'Identifiant unique';


--
-- Name: COLUMN dossier_instruction.libelle; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN dossier_instruction.libelle IS 'Libellé du dossier d''instruction : calculé automatiquement à la génération. libelle=dossier_coordination.libelle + ''-'' + service.code';


--
-- Name: COLUMN dossier_instruction.dossier_coordination; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN dossier_instruction.dossier_coordination IS 'Identifiant unique du dossier de coordination auquel est rattaché le dossier d''instruction';


--
-- Name: COLUMN dossier_instruction.technicien; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN dossier_instruction.technicien IS 'Technicien en charge du dossier d''instruction';


--
-- Name: COLUMN dossier_instruction.service; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN dossier_instruction.service IS 'Service du dossier d''instruction : SECURITE ou ACCESSIBILITE';


--
-- Name: COLUMN dossier_instruction.a_qualifier; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN dossier_instruction.a_qualifier IS 'À [true] le dossier d''instruction doit être qualifié';


--
-- Name: COLUMN dossier_instruction.incompletude; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN dossier_instruction.incompletude IS 'À [true] le dossier de d''instruction est incomplet (mis à [true] lors de la finalisation d''un courrier d''incomplétude)';


--
-- Name: COLUMN dossier_instruction.piece_attendue; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN dossier_instruction.piece_attendue IS 'En cas d''incomplétude, les pièces attendues pour la complétude (rempli avec le champ complement1 lors de la finalisation d''un courrier d''incomplétude)';


--
-- Name: COLUMN dossier_instruction.description; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN dossier_instruction.description IS 'Description détaillée du dossier de coordination';


--
-- Name: COLUMN dossier_instruction.notes; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN dossier_instruction.notes IS 'Comporte des commentaires d''instruction';


--
-- Name: COLUMN dossier_instruction.autorite_competente; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN dossier_instruction.autorite_competente IS 'Autorité compétente pour le dossier d''instruction';


--
-- Name: COLUMN dossier_instruction.dossier_cloture; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN dossier_instruction.dossier_cloture IS 'À [true] le dossier d''instruction est clôturé';


--
-- Name: COLUMN dossier_instruction.date_cloture; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN dossier_instruction.date_cloture IS 'Date de clôture du dossier d''instruction';


--
-- Name: COLUMN dossier_instruction.date_ouverture; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN dossier_instruction.date_ouverture IS 'Date d''ouverture du dossier d''instruction';


--
-- Name: dossier_instruction_reunion; Type: TABLE; Schema: openaria; Owner: -
--

CREATE TABLE dossier_instruction_reunion (
    dossier_instruction_reunion integer NOT NULL,
    dossier_instruction integer NOT NULL,
    reunion_type integer NOT NULL,
    reunion_type_categorie integer NOT NULL,
    date_souhaitee date NOT NULL,
    motivation text,
    proposition_avis integer,
    proposition_avis_complement character varying(250),
    reunion integer,
    ordre integer,
    avis integer,
    avis_complement character varying(250),
    avis_motivation text
);


--
-- Name: TABLE dossier_instruction_reunion; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON TABLE dossier_instruction_reunion IS 'Demande de passage des dossiers d''instruction en réunion';


--
-- Name: COLUMN dossier_instruction_reunion.dossier_instruction_reunion; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN dossier_instruction_reunion.dossier_instruction_reunion IS 'Dossier d''instruction de la demande de passage en réunion';


--
-- Name: COLUMN dossier_instruction_reunion.reunion_type; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN dossier_instruction_reunion.reunion_type IS 'Type de réunion (Commission Technique d''Urbanisme, ...)';


--
-- Name: COLUMN dossier_instruction_reunion.date_souhaitee; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN dossier_instruction_reunion.date_souhaitee IS 'Date de passage en réunion souhaitée';


--
-- Name: COLUMN dossier_instruction_reunion.reunion; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN dossier_instruction_reunion.reunion IS 'Identifiant de la reunion effective';


--
-- Name: COLUMN dossier_instruction_reunion.avis; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN dossier_instruction_reunion.avis IS 'Avis rendu par la réunion';


--
-- Name: dossier_instruction_reunion_seq; Type: SEQUENCE; Schema: openaria; Owner: -
--

CREATE SEQUENCE dossier_instruction_reunion_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: dossier_instruction_reunion_seq; Type: SEQUENCE OWNED BY; Schema: openaria; Owner: -
--

ALTER SEQUENCE dossier_instruction_reunion_seq OWNED BY dossier_instruction_reunion.dossier_instruction_reunion;


--
-- Name: dossier_instruction_seq; Type: SEQUENCE; Schema: openaria; Owner: -
--

CREATE SEQUENCE dossier_instruction_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: dossier_instruction_seq; Type: SEQUENCE OWNED BY; Schema: openaria; Owner: -
--

ALTER SEQUENCE dossier_instruction_seq OWNED BY dossier_instruction.dossier_instruction;


--
-- Name: dossier_type; Type: TABLE; Schema: openaria; Owner: -
--

CREATE TABLE dossier_type (
    dossier_type integer NOT NULL,
    code character varying(20),
    libelle character varying(100),
    description text,
    om_validite_debut date,
    om_validite_fin date
);


--
-- Name: TABLE dossier_type; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON TABLE dossier_type IS 'Type de dossier de coordination : plan ou visite';


--
-- Name: COLUMN dossier_type.dossier_type; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN dossier_type.dossier_type IS 'Identifiant unique.';


--
-- Name: COLUMN dossier_type.code; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN dossier_type.code IS 'Code abrégé afin de générer des identifiants de dossiers par exemple, ou pour un affichage synthétique.	';


--
-- Name: COLUMN dossier_type.libelle; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN dossier_type.libelle IS 'Libellé long, utilisé pour les affichages des listes déroulantes par exemple.';


--
-- Name: COLUMN dossier_type.description; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN dossier_type.description IS 'Description détaillée du type.';


--
-- Name: COLUMN dossier_type.om_validite_debut; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN dossier_type.om_validite_debut IS 'Date de mise en service du paramètre';


--
-- Name: COLUMN dossier_type.om_validite_fin; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN dossier_type.om_validite_fin IS 'Date d''expiration du paramètre';


--
-- Name: dossier_type_seq; Type: SEQUENCE; Schema: openaria; Owner: -
--

CREATE SEQUENCE dossier_type_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: dossier_type_seq; Type: SEQUENCE OWNED BY; Schema: openaria; Owner: -
--

ALTER SEQUENCE dossier_type_seq OWNED BY dossier_type.dossier_type;


--
-- Name: essai_realise; Type: TABLE; Schema: openaria; Owner: -
--

CREATE TABLE essai_realise (
    essai_realise integer NOT NULL,
    service integer NOT NULL,
    code character varying(20),
    libelle character varying(100),
    description text,
    om_validite_debut date,
    om_validite_fin date
);


--
-- Name: TABLE essai_realise; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON TABLE essai_realise IS 'Essai réalisé';


--
-- Name: COLUMN essai_realise.essai_realise; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN essai_realise.essai_realise IS 'Identifiant unique';


--
-- Name: COLUMN essai_realise.service; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN essai_realise.service IS 'Service de l''essai réalisé';


--
-- Name: COLUMN essai_realise.code; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN essai_realise.code IS 'Code de l''essai réalisé';


--
-- Name: COLUMN essai_realise.libelle; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN essai_realise.libelle IS 'Libellé de l''essai réalisé';


--
-- Name: COLUMN essai_realise.description; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN essai_realise.description IS 'Description de l''essai réalisé';


--
-- Name: COLUMN essai_realise.om_validite_debut; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN essai_realise.om_validite_debut IS 'Date de début de validité de l''essai réalisé';


--
-- Name: COLUMN essai_realise.om_validite_fin; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN essai_realise.om_validite_fin IS 'Date de fin de validité de l''essai réalisé';


--
-- Name: essai_realise_seq; Type: SEQUENCE; Schema: openaria; Owner: -
--

CREATE SEQUENCE essai_realise_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: essai_realise_seq; Type: SEQUENCE OWNED BY; Schema: openaria; Owner: -
--

ALTER SEQUENCE essai_realise_seq OWNED BY essai_realise.essai_realise;


--
-- Name: etablissement; Type: TABLE; Schema: openaria; Owner: -
--

CREATE TABLE etablissement (
    etablissement integer NOT NULL,
    code character varying(25) NOT NULL,
    libelle character varying(100) NOT NULL,
    adresse_numero integer,
    adresse_numero2 character varying(10),
    adresse_voie integer,
    adresse_complement character varying(40),
    lieu_dit character varying(39),
    boite_postale character varying(5),
    adresse_cp character varying(6),
    adresse_ville character varying(40),
    adresse_arrondissement integer,
    cedex character varying(5),
    npai boolean DEFAULT false NOT NULL,
    telephone character varying(20),
    fax character varying(20),
    etablissement_nature integer NOT NULL,
    siret character varying(20),
    annee_de_construction character varying(4),
    etablissement_statut_juridique integer,
    etablissement_tutelle_adm integer,
    ref_patrimoine text,
    etablissement_type integer,
    etablissement_categorie integer,
    etablissement_etat integer,
    date_arrete_ouverture date,
    autorite_police_encours boolean DEFAULT false NOT NULL,
    om_validite_debut date,
    om_validite_fin date,
    si_effectif_public integer,
    si_effectif_personnel integer,
    si_locaux_sommeil boolean DEFAULT false NOT NULL,
    si_periodicite_visites integer,
    si_prochaine_visite_periodique_date_previsionnelle date,
    si_visite_duree integer,
    si_derniere_visite_periodique_date date,
    si_derniere_visite_date date,
    si_derniere_visite_avis integer,
    si_derniere_visite_technicien integer,
    si_prochaine_visite_date date,
    si_prochaine_visite_type integer,
    acc_derniere_visite_date date,
    acc_derniere_visite_avis integer,
    acc_derniere_visite_technicien integer,
    acc_consignes_om_html text,
    acc_descriptif_om_html text,
    si_consignes_om_html text,
    si_descriptif_om_html text,
    si_autorite_competente_visite integer,
    si_autorite_competente_plan integer,
    si_dernier_plan_avis integer,
    si_type_alarme text,
    si_type_ssi character varying(100),
    si_conformite_l16 boolean,
    si_alimentation_remplacement boolean,
    si_service_securite boolean,
    si_personnel_jour integer,
    si_personnel_nuit integer,
    references_cadastrales text,
    dossier_coordination_periodique integer,
    geolocalise boolean DEFAULT false,
    geom_point public.geometry(Point,2154),
    geom_emprise public.geometry(MultiPolygon,2154)
);


--
-- Name: TABLE etablissement; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON TABLE etablissement IS 'Fiche de l''établissement.L''établissement est un "Bâtiment ou enceinte ERP".Celui-ci peut être référenciel (un ERP officiel) ou pas. Tous les établissements figurent dans cette table. Seuls les établissements de nature référerentiel, comportant un code, constituent le référentiel des ERP.Les ERP Référentiels sont par défaut "fermés". Ils sont ensuite ouverts ou fermés par arrêtés. Par défaut les autres établissements sont "non soumis à contrôle".';


--
-- Name: COLUMN etablissement.etablissement; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN etablissement.etablissement IS 'Identifiant unique de l''établissement, servant aussi pour le numéro d''ERP.';


--
-- Name: COLUMN etablissement.libelle; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN etablissement.libelle IS 'Nom de l''ERP';


--
-- Name: COLUMN etablissement.adresse_numero; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN etablissement.adresse_numero IS 'Numéro d''adresse (1245, 51, 3, etc.)';


--
-- Name: COLUMN etablissement.adresse_numero2; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN etablissement.adresse_numero2 IS 'Texte du numéro d''adresse (bis, ter, etc.)';


--
-- Name: COLUMN etablissement.adresse_voie; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN etablissement.adresse_voie IS 'référence vers la voie dans la table des voies';


--
-- Name: COLUMN etablissement.adresse_complement; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN etablissement.adresse_complement IS 'Complément d''adresse en texte libre';


--
-- Name: COLUMN etablissement.lieu_dit; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN etablissement.lieu_dit IS 'Lieu-dit de l''adresse de l''établissement.';


--
-- Name: COLUMN etablissement.boite_postale; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN etablissement.boite_postale IS 'Boîte postale de l''adresse de l''établissement.';


--
-- Name: COLUMN etablissement.adresse_cp; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN etablissement.adresse_cp IS 'code postal de l''adresse';


--
-- Name: COLUMN etablissement.adresse_ville; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN etablissement.adresse_ville IS 'Ville de l''adresse';


--
-- Name: COLUMN etablissement.adresse_arrondissement; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN etablissement.adresse_arrondissement IS 'Lien vers l''arrondissement';


--
-- Name: COLUMN etablissement.cedex; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN etablissement.cedex IS 'Cedex de l''adresse de l''établissement.';


--
-- Name: COLUMN etablissement.npai; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN etablissement.npai IS 'Indique si l''adresse de l''établissement est incorrecte';


--
-- Name: COLUMN etablissement.telephone; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN etablissement.telephone IS 'Numéro de téléphone fixe de l''établissement.';


--
-- Name: COLUMN etablissement.fax; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN etablissement.fax IS 'Numéro de fax de l''établissement.';


--
-- Name: COLUMN etablissement.etablissement_nature; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN etablissement.etablissement_nature IS 'Nature de l''établissement : ERP Référentiel, ERP non référentiel, Bâtiment non ERP etc. ';


--
-- Name: COLUMN etablissement.siret; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN etablissement.siret IS 'SIRET de l''ERP, optionnel. Ce champ permet de faire des recoupements avec d''autres données.';


--
-- Name: COLUMN etablissement.annee_de_construction; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN etablissement.annee_de_construction IS 'Année de construction du bâtiment ERP.';


--
-- Name: COLUMN etablissement.etablissement_statut_juridique; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN etablissement.etablissement_statut_juridique IS 'Statut juridique de l''établissement : privé, public, commune.';


--
-- Name: COLUMN etablissement.etablissement_tutelle_adm; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN etablissement.etablissement_tutelle_adm IS 'Tutelle administrative des établissements publics';


--
-- Name: COLUMN etablissement.ref_patrimoine; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN etablissement.ref_patrimoine IS 'Référence de l''établissement dans le référentiel patrimoine de la ville.Ce champ est obligatoire quand le statut juridique de l''établissement est "ville".
Il peut y avoir plusieurs références patrimoine pour un établissement. Dans ce cas on a une référence par ligne.';


--
-- Name: COLUMN etablissement.etablissement_type; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN etablissement.etablissement_type IS 'lien vers la table des types d''établissements';


--
-- Name: COLUMN etablissement.etablissement_categorie; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN etablissement.etablissement_categorie IS 'Catégorie ERP de l''établissement';


--
-- Name: COLUMN etablissement.etablissement_etat; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN etablissement.etablissement_etat IS 'Satut administratif de l''établissement : ouvert / fermé.';


--
-- Name: COLUMN etablissement.date_arrete_ouverture; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN etablissement.date_arrete_ouverture IS 'Date de l''arrêté d''ouverture.';


--
-- Name: COLUMN etablissement.autorite_police_encours; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN etablissement.autorite_police_encours IS 'Est-ce qu''il y a une autorité de police en cours ?';


--
-- Name: COLUMN etablissement.om_validite_debut; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN etablissement.om_validite_debut IS 'Date de début de validité';


--
-- Name: COLUMN etablissement.om_validite_fin; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN etablissement.om_validite_fin IS 'Date de fin de validité';


--
-- Name: COLUMN etablissement.si_effectif_public; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN etablissement.si_effectif_public IS 'Nombre maximum de personnes externes au personnel de l''établissement pouvant être présentes au sein de l''établissement';


--
-- Name: COLUMN etablissement.si_effectif_personnel; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN etablissement.si_effectif_personnel IS 'Nombre maximum de personnes membres du personnel pouvant être présentes au sein de l''établissement.';


--
-- Name: COLUMN etablissement.si_locaux_sommeil; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN etablissement.si_locaux_sommeil IS 'L''établissement a-t-il des locaux à sommeil ?';


--
-- Name: COLUMN etablissement.si_periodicite_visites; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN etablissement.si_periodicite_visites IS 'Périodicité des visites en années';


--
-- Name: COLUMN etablissement.si_prochaine_visite_periodique_date_previsionnelle; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN etablissement.si_prochaine_visite_periodique_date_previsionnelle IS 'Date prévisionnelle de la prochaine visite périodique = ( date de la dernière visite périodique OU date de l''arrêté d''ouverture ) + délai de visite périodique';


--
-- Name: COLUMN etablissement.si_visite_duree; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN etablissement.si_visite_duree IS 'Durée de visite en nombre de jours.';


--
-- Name: COLUMN etablissement.si_derniere_visite_periodique_date; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN etablissement.si_derniere_visite_periodique_date IS 'Date de la dernière visite périodique de sécurité.';


--
-- Name: COLUMN etablissement.si_derniere_visite_date; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN etablissement.si_derniere_visite_date IS 'Date de la dernière visite de sécurité.';


--
-- Name: COLUMN etablissement.si_derniere_visite_avis; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN etablissement.si_derniere_visite_avis IS 'Avis donné suite à la dernière visite de la commission de sécurité.';


--
-- Name: COLUMN etablissement.si_prochaine_visite_date; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN etablissement.si_prochaine_visite_date IS 'Date de la prochaine visite programmée par la commission sécurité.';


--
-- Name: COLUMN etablissement.si_prochaine_visite_type; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN etablissement.si_prochaine_visite_type IS 'Type de la prochaine visite programmée par la commission de sécurité (périodique/réception)';


--
-- Name: COLUMN etablissement.acc_derniere_visite_date; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN etablissement.acc_derniere_visite_date IS 'Date de la dernière visite de la commission d''accessibilité.';


--
-- Name: COLUMN etablissement.acc_derniere_visite_avis; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN etablissement.acc_derniere_visite_avis IS 'Avis donné suite à la dernière visite de la commission d''accessibilité.';


--
-- Name: COLUMN etablissement.acc_consignes_om_html; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN etablissement.acc_consignes_om_html IS 'Consignes destinées aux techniciens dans le suivi de l''établissement.';


--
-- Name: COLUMN etablissement.acc_descriptif_om_html; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN etablissement.acc_descriptif_om_html IS 'Description de l''établissement du point de vue accessibilité.';


--
-- Name: COLUMN etablissement.si_consignes_om_html; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN etablissement.si_consignes_om_html IS 'Consignes concernant l''établissement en matière de sécurité.';


--
-- Name: COLUMN etablissement.si_descriptif_om_html; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN etablissement.si_descriptif_om_html IS 'Descriptif de l''établissement du point de vue sécurité (données technique de l''établissement mise à jour par PV).';


--
-- Name: COLUMN etablissement.si_autorite_competente_visite; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN etablissement.si_autorite_competente_visite IS 'Choix de la commission compétente en sécurité pour les visites';


--
-- Name: COLUMN etablissement.si_autorite_competente_plan; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN etablissement.si_autorite_competente_plan IS 'Choix de la commission compétente en sécurité pour les dossiers plan';


--
-- Name: COLUMN etablissement.si_dernier_plan_avis; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN etablissement.si_dernier_plan_avis IS 'Dernier avis donné pour un dossier plan en mlatière de sécurité.';


--
-- Name: COLUMN etablissement.si_type_alarme; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN etablissement.si_type_alarme IS 'Desctiption du type d''alarme. XXX structuré ??';


--
-- Name: COLUMN etablissement.si_type_ssi; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN etablissement.si_type_ssi IS 'Type SSI (A/B/C/D/E)';


--
-- Name: COLUMN etablissement.si_conformite_l16; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN etablissement.si_conformite_l16 IS 'Conforme au référentiel L16 ?';


--
-- Name: COLUMN etablissement.si_alimentation_remplacement; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN etablissement.si_alimentation_remplacement IS 'Présence d''une alimentation de remplacement ?';


--
-- Name: COLUMN etablissement.si_service_securite; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN etablissement.si_service_securite IS 'Présence d''un service sécurité ?';


--
-- Name: COLUMN etablissement.si_personnel_jour; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN etablissement.si_personnel_jour IS 'Effectif personnel de jour';


--
-- Name: COLUMN etablissement.si_personnel_nuit; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN etablissement.si_personnel_nuit IS 'Effectif personnel de nuit';


--
-- Name: COLUMN etablissement.references_cadastrales; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN etablissement.references_cadastrales IS 'champ de saisie des références cadastrales. Celles-ci sont ensuite gérées dans une table dédiée aux parcelles cadastrales.';


--
-- Name: COLUMN etablissement.dossier_coordination_periodique; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN etablissement.dossier_coordination_periodique IS 'Lien vers le dossier de coordination pour la visite périodique.';


--
-- Name: COLUMN etablissement.geolocalise; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN etablissement.geolocalise IS 'Marqueur signalant que l''établissement a été localisé sur le SIG';


--
-- Name: etablissement_categorie; Type: TABLE; Schema: openaria; Owner: -
--

CREATE TABLE etablissement_categorie (
    etablissement_categorie integer NOT NULL,
    libelle character varying(100),
    description character varying(250),
    om_validite_debut date,
    om_validite_fin date
);


--
-- Name: TABLE etablissement_categorie; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON TABLE etablissement_categorie IS 'Catégorie de l''établissement par rapport à la réglementation des ERP.';


--
-- Name: COLUMN etablissement_categorie.etablissement_categorie; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN etablissement_categorie.etablissement_categorie IS 'Identifiant unique.';


--
-- Name: COLUMN etablissement_categorie.libelle; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN etablissement_categorie.libelle IS 'Nom de la catégorie.';


--
-- Name: COLUMN etablissement_categorie.description; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN etablissement_categorie.description IS 'Description de la catégorie.';


--
-- Name: etablissement_categorie_seq; Type: SEQUENCE; Schema: openaria; Owner: -
--

CREATE SEQUENCE etablissement_categorie_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: etablissement_categorie_seq; Type: SEQUENCE OWNED BY; Schema: openaria; Owner: -
--

ALTER SEQUENCE etablissement_categorie_seq OWNED BY etablissement_categorie.etablissement_categorie;


--
-- Name: etablissement_etat; Type: TABLE; Schema: openaria; Owner: -
--

CREATE TABLE etablissement_etat (
    etablissement_etat integer NOT NULL,
    statut_administratif character varying(100),
    description character varying(250),
    om_validite_debut date,
    om_validite_fin date,
    code character varying(7)
);


--
-- Name: TABLE etablissement_etat; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON TABLE etablissement_etat IS 'Statut administratif de l''établissement.';


--
-- Name: COLUMN etablissement_etat.etablissement_etat; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN etablissement_etat.etablissement_etat IS 'Identifiant unique.';


--
-- Name: COLUMN etablissement_etat.statut_administratif; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN etablissement_etat.statut_administratif IS 'Statut administratif de l''établissement (Fermé/Ouvert).';


--
-- Name: COLUMN etablissement_etat.description; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN etablissement_etat.description IS 'Description du statut.';


--
-- Name: COLUMN etablissement_etat.code; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN etablissement_etat.code IS 'Code de l''état de l''établissement.';


--
-- Name: etablissement_etat_seq; Type: SEQUENCE; Schema: openaria; Owner: -
--

CREATE SEQUENCE etablissement_etat_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: etablissement_etat_seq; Type: SEQUENCE OWNED BY; Schema: openaria; Owner: -
--

ALTER SEQUENCE etablissement_etat_seq OWNED BY etablissement_etat.etablissement_etat;


--
-- Name: etablissement_nature; Type: TABLE; Schema: openaria; Owner: -
--

CREATE TABLE etablissement_nature (
    etablissement_nature integer NOT NULL,
    nature character varying(100),
    description character varying(250),
    om_validite_debut date,
    om_validite_fin date,
    code character varying(5),
    erp boolean
);


--
-- Name: TABLE etablissement_nature; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON TABLE etablissement_nature IS 'Nature des établissements figurant dans la table des établissements (ERP Référentiel, ERP non Référentiel, Bâtiment non ERP, etc.).';


--
-- Name: COLUMN etablissement_nature.etablissement_nature; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN etablissement_nature.etablissement_nature IS 'Identifiant unique.';


--
-- Name: COLUMN etablissement_nature.nature; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN etablissement_nature.nature IS 'Nature de l''établissement.';


--
-- Name: COLUMN etablissement_nature.description; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN etablissement_nature.description IS 'Description de cette nature d''établissement.';


--
-- Name: COLUMN etablissement_nature.code; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN etablissement_nature.code IS 'Code de l''établissement.';


--
-- Name: etablissement_nature_seq; Type: SEQUENCE; Schema: openaria; Owner: -
--

CREATE SEQUENCE etablissement_nature_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: etablissement_nature_seq; Type: SEQUENCE OWNED BY; Schema: openaria; Owner: -
--

ALTER SEQUENCE etablissement_nature_seq OWNED BY etablissement_nature.etablissement_nature;


--
-- Name: etablissement_parcelle; Type: TABLE; Schema: openaria; Owner: -
--

CREATE TABLE etablissement_parcelle (
    etablissement_parcelle integer NOT NULL,
    etablissement integer,
    ref_cadastre character varying(20)
);


--
-- Name: TABLE etablissement_parcelle; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON TABLE etablissement_parcelle IS 'Liste des parcelles de l''établissement ; ces données sont utilisées pour les recherches et les liaisons externes.';


--
-- Name: COLUMN etablissement_parcelle.etablissement_parcelle; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN etablissement_parcelle.etablissement_parcelle IS 'Identifiant unique';


--
-- Name: COLUMN etablissement_parcelle.etablissement; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN etablissement_parcelle.etablissement IS 'Lien vers l''établissement concerné';


--
-- Name: COLUMN etablissement_parcelle.ref_cadastre; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN etablissement_parcelle.ref_cadastre IS 'Référence cadastrale de la parcelle';


--
-- Name: etablissement_parcelle_seq; Type: SEQUENCE; Schema: openaria; Owner: -
--

CREATE SEQUENCE etablissement_parcelle_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: etablissement_parcelle_seq; Type: SEQUENCE OWNED BY; Schema: openaria; Owner: -
--

ALTER SEQUENCE etablissement_parcelle_seq OWNED BY etablissement_parcelle.etablissement_parcelle;


--
-- Name: etablissement_seq; Type: SEQUENCE; Schema: openaria; Owner: -
--

CREATE SEQUENCE etablissement_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: etablissement_seq; Type: SEQUENCE OWNED BY; Schema: openaria; Owner: -
--

ALTER SEQUENCE etablissement_seq OWNED BY etablissement.etablissement;


--
-- Name: etablissement_statut_juridique; Type: TABLE; Schema: openaria; Owner: -
--

CREATE TABLE etablissement_statut_juridique (
    etablissement_statut_juridique integer NOT NULL,
    libelle character varying(100),
    om_validite_debut date,
    om_validite_fin date,
    code character varying(3)
);


--
-- Name: TABLE etablissement_statut_juridique; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON TABLE etablissement_statut_juridique IS 'Statut juridique de l''établissement (Privé, Public, Ville).';


--
-- Name: COLUMN etablissement_statut_juridique.etablissement_statut_juridique; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN etablissement_statut_juridique.etablissement_statut_juridique IS 'Identifiant unique.';


--
-- Name: COLUMN etablissement_statut_juridique.libelle; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN etablissement_statut_juridique.libelle IS 'Statut juridique de l''établissement (Exemple : Privé, Public, Ville).';


--
-- Name: COLUMN etablissement_statut_juridique.code; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN etablissement_statut_juridique.code IS 'Libellé court.';


--
-- Name: etablissement_statut_juridique_seq; Type: SEQUENCE; Schema: openaria; Owner: -
--

CREATE SEQUENCE etablissement_statut_juridique_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: etablissement_statut_juridique_seq; Type: SEQUENCE OWNED BY; Schema: openaria; Owner: -
--

ALTER SEQUENCE etablissement_statut_juridique_seq OWNED BY etablissement_statut_juridique.etablissement_statut_juridique;


--
-- Name: etablissement_tutelle_adm; Type: TABLE; Schema: openaria; Owner: -
--

CREATE TABLE etablissement_tutelle_adm (
    etablissement_tutelle_adm integer NOT NULL,
    libelle character varying(40),
    om_validite_debut date,
    om_validite_fin date
);


--
-- Name: TABLE etablissement_tutelle_adm; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON TABLE etablissement_tutelle_adm IS 'Tutelle administrative d''un établissement de statut "public"';


--
-- Name: COLUMN etablissement_tutelle_adm.etablissement_tutelle_adm; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN etablissement_tutelle_adm.etablissement_tutelle_adm IS 'Identifiant unique';


--
-- Name: COLUMN etablissement_tutelle_adm.libelle; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN etablissement_tutelle_adm.libelle IS 'Libellé de la tutelle administrative';


--
-- Name: COLUMN etablissement_tutelle_adm.om_validite_debut; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN etablissement_tutelle_adm.om_validite_debut IS 'Date de début de validité';


--
-- Name: COLUMN etablissement_tutelle_adm.om_validite_fin; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN etablissement_tutelle_adm.om_validite_fin IS 'Date de fin de validité';


--
-- Name: etablissement_tutelle_adm_seq; Type: SEQUENCE; Schema: openaria; Owner: -
--

CREATE SEQUENCE etablissement_tutelle_adm_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: etablissement_tutelle_adm_seq; Type: SEQUENCE OWNED BY; Schema: openaria; Owner: -
--

ALTER SEQUENCE etablissement_tutelle_adm_seq OWNED BY etablissement_tutelle_adm.etablissement_tutelle_adm;


--
-- Name: etablissement_type; Type: TABLE; Schema: openaria; Owner: -
--

CREATE TABLE etablissement_type (
    etablissement_type integer NOT NULL,
    libelle character varying(100),
    description character varying(250),
    om_validite_debut date,
    om_validite_fin date
);


--
-- Name: TABLE etablissement_type; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON TABLE etablissement_type IS 'Types de l''établissement au sens ERP.';


--
-- Name: COLUMN etablissement_type.etablissement_type; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN etablissement_type.etablissement_type IS 'Identifiant unique du type.';


--
-- Name: COLUMN etablissement_type.libelle; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN etablissement_type.libelle IS 'Texte affiché pour le type (Exemple : Type S).';


--
-- Name: COLUMN etablissement_type.description; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN etablissement_type.description IS 'Description du type.';


--
-- Name: etablissement_type_seq; Type: SEQUENCE; Schema: openaria; Owner: -
--

CREATE SEQUENCE etablissement_type_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: etablissement_type_seq; Type: SEQUENCE OWNED BY; Schema: openaria; Owner: -
--

ALTER SEQUENCE etablissement_type_seq OWNED BY etablissement_type.etablissement_type;


--
-- Name: etablissement_unite; Type: TABLE; Schema: openaria; Owner: -
--

CREATE TABLE etablissement_unite (
    etablissement_unite integer NOT NULL,
    libelle character varying(90) NOT NULL,
    acc_notes_om_html text,
    acc_descriptif_ua_om_html text,
    etablissement integer NOT NULL,
    acc_handicap_physique boolean,
    acc_handicap_auditif boolean,
    acc_handicap_visuel boolean,
    acc_handicap_mental boolean,
    acc_places_stationnement_amenagees integer,
    acc_ascenseur boolean,
    acc_elevateur boolean,
    acc_boucle_magnetique boolean,
    acc_sanitaire boolean,
    acc_places_assises_public integer,
    acc_chambres_amenagees integer,
    acc_douche boolean,
    acc_derogation_scda integer,
    etat character varying(20),
    archive boolean NOT NULL,
    dossier_instruction integer,
    etablissement_unite_lie integer,
    adap_date_validation date,
    adap_duree_validite integer,
    adap_annee_debut_travaux integer,
    adap_annee_fin_travaux integer
);


--
-- Name: TABLE etablissement_unite; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON TABLE etablissement_unite IS 'L''unité est un élément de l''établissement. L''unité est traitée individuellement pour les données d''accessibilité. Il est possible d''effectuer des recherches d''entités sur la base de critères multiples.';


--
-- Name: COLUMN etablissement_unite.etablissement_unite; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN etablissement_unite.etablissement_unite IS 'Id unique';


--
-- Name: COLUMN etablissement_unite.libelle; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN etablissement_unite.libelle IS 'Nom de l''entité. Exemple : Ecole primaire les étoiles';


--
-- Name: COLUMN etablissement_unite.acc_notes_om_html; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN etablissement_unite.acc_notes_om_html IS 'Consignes destinées aux techniciens dans le suivi de l''unité de l''établissement. ';


--
-- Name: COLUMN etablissement_unite.acc_descriptif_ua_om_html; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN etablissement_unite.acc_descriptif_ua_om_html IS 'Description de l''unité.';


--
-- Name: COLUMN etablissement_unite.etablissement; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN etablissement_unite.etablissement IS 'Lien vers l''établissement : les entités sont des éléments composant l''établissement.';


--
-- Name: COLUMN etablissement_unite.acc_handicap_physique; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN etablissement_unite.acc_handicap_physique IS 'Unité accessible pour handicap physique.';


--
-- Name: COLUMN etablissement_unite.acc_handicap_auditif; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN etablissement_unite.acc_handicap_auditif IS 'Unité accessible pour handicap auditif.';


--
-- Name: COLUMN etablissement_unite.acc_handicap_visuel; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN etablissement_unite.acc_handicap_visuel IS 'Unité accessible pour handicap visuel.';


--
-- Name: COLUMN etablissement_unite.acc_handicap_mental; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN etablissement_unite.acc_handicap_mental IS 'Unité accessible pour handicap mental.';


--
-- Name: COLUMN etablissement_unite.acc_places_stationnement_amenagees; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN etablissement_unite.acc_places_stationnement_amenagees IS 'Nombre de places de stationnement aménagées.';


--
-- Name: COLUMN etablissement_unite.acc_ascenseur; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN etablissement_unite.acc_ascenseur IS 'Présence d''ascenseur(s).';


--
-- Name: COLUMN etablissement_unite.acc_elevateur; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN etablissement_unite.acc_elevateur IS 'Présence d''élévateur(s).';


--
-- Name: COLUMN etablissement_unite.acc_boucle_magnetique; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN etablissement_unite.acc_boucle_magnetique IS 'Présence de boucle magnétique.';


--
-- Name: COLUMN etablissement_unite.acc_sanitaire; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN etablissement_unite.acc_sanitaire IS 'Présence de sanitaire(s).';


--
-- Name: COLUMN etablissement_unite.acc_places_assises_public; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN etablissement_unite.acc_places_assises_public IS 'Nombre de places assises pour le public.';


--
-- Name: COLUMN etablissement_unite.acc_chambres_amenagees; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN etablissement_unite.acc_chambres_amenagees IS 'Nombre de chambres aménagées en accessibilité.';


--
-- Name: COLUMN etablissement_unite.acc_douche; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN etablissement_unite.acc_douche IS 'Présence de douche(s).';


--
-- Name: COLUMN etablissement_unite.acc_derogation_scda; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN etablissement_unite.acc_derogation_scda IS 'Dérogation SCDA.';


--
-- Name: COLUMN etablissement_unite.etat; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN etablissement_unite.etat IS 'État de l''unité. Valeurs possibles : valide, enprojet.';


--
-- Name: COLUMN etablissement_unite.archive; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN etablissement_unite.archive IS 'Marqueur d''archivage de l''unité.';


--
-- Name: COLUMN etablissement_unite.dossier_instruction; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN etablissement_unite.dossier_instruction IS 'Lien vers un dossier d''instruction : une unité analysée est liée à un dossier d''instruction.';


--
-- Name: COLUMN etablissement_unite.etablissement_unite_lie; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN etablissement_unite.etablissement_unite_lie IS 'Lien vers une unité : une unité analysée peut avoir pour origine une autre unité.';


--
-- Name: COLUMN etablissement_unite.adap_date_validation; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN etablissement_unite.adap_date_validation IS 'ADAP Date de validation.';


--
-- Name: COLUMN etablissement_unite.adap_duree_validite; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN etablissement_unite.adap_duree_validite IS 'ADAP Durée de validité (en années).';


--
-- Name: COLUMN etablissement_unite.adap_annee_debut_travaux; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN etablissement_unite.adap_annee_debut_travaux IS 'ADAP Année de début des travaux.';


--
-- Name: COLUMN etablissement_unite.adap_annee_fin_travaux; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN etablissement_unite.adap_annee_fin_travaux IS 'ADAP Année de fin des travaux.';


--
-- Name: etablissement_unite_seq; Type: SEQUENCE; Schema: openaria; Owner: -
--

CREATE SEQUENCE etablissement_unite_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: etablissement_unite_seq; Type: SEQUENCE OWNED BY; Schema: openaria; Owner: -
--

ALTER SEQUENCE etablissement_unite_seq OWNED BY etablissement_unite.etablissement_unite;


--
-- Name: lien_autorite_police_courrier; Type: TABLE; Schema: openaria; Owner: -
--

CREATE TABLE lien_autorite_police_courrier (
    lien_autorite_police_courrier integer NOT NULL,
    autorite_police integer NOT NULL,
    courrier integer NOT NULL
);


--
-- Name: TABLE lien_autorite_police_courrier; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON TABLE lien_autorite_police_courrier IS 'Liaison entre les autorités de police et les courriers.';


--
-- Name: COLUMN lien_autorite_police_courrier.lien_autorite_police_courrier; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN lien_autorite_police_courrier.lien_autorite_police_courrier IS 'Identifiant unique';


--
-- Name: COLUMN lien_autorite_police_courrier.autorite_police; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN lien_autorite_police_courrier.autorite_police IS 'Autorité de police.';


--
-- Name: COLUMN lien_autorite_police_courrier.courrier; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN lien_autorite_police_courrier.courrier IS 'Courrier lié.';


--
-- Name: lien_autorite_police_courrier_seq; Type: SEQUENCE; Schema: openaria; Owner: -
--

CREATE SEQUENCE lien_autorite_police_courrier_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: lien_autorite_police_courrier_seq; Type: SEQUENCE OWNED BY; Schema: openaria; Owner: -
--

ALTER SEQUENCE lien_autorite_police_courrier_seq OWNED BY lien_autorite_police_courrier.lien_autorite_police_courrier;


--
-- Name: lien_contrainte_dossier_coordination; Type: TABLE; Schema: openaria; Owner: -
--

CREATE TABLE lien_contrainte_dossier_coordination (
    lien_contrainte_dossier_coordination integer NOT NULL,
    dossier_coordination integer NOT NULL,
    contrainte integer NOT NULL,
    texte_complete text,
    recuperee boolean DEFAULT false
);


--
-- Name: TABLE lien_contrainte_dossier_coordination; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON TABLE lien_contrainte_dossier_coordination IS 'Table des contraintes appliquées au DC';


--
-- Name: COLUMN lien_contrainte_dossier_coordination.lien_contrainte_dossier_coordination; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN lien_contrainte_dossier_coordination.lien_contrainte_dossier_coordination IS 'Identifiant unique';


--
-- Name: COLUMN lien_contrainte_dossier_coordination.dossier_coordination; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN lien_contrainte_dossier_coordination.dossier_coordination IS 'DC auquel est appliqué la contrainte';


--
-- Name: COLUMN lien_contrainte_dossier_coordination.contrainte; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN lien_contrainte_dossier_coordination.contrainte IS 'Contrainte qui est appliquée au DC';


--
-- Name: COLUMN lien_contrainte_dossier_coordination.texte_complete; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN lien_contrainte_dossier_coordination.texte_complete IS 'Texte complété de la contrainte';


--
-- Name: COLUMN lien_contrainte_dossier_coordination.recuperee; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN lien_contrainte_dossier_coordination.recuperee IS 'Si vrai, contrainte récupérée depuis le SIG pour ce DC.';


--
-- Name: lien_contrainte_dossier_coordination_seq; Type: SEQUENCE; Schema: openaria; Owner: -
--

CREATE SEQUENCE lien_contrainte_dossier_coordination_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: lien_contrainte_dossier_coordination_seq; Type: SEQUENCE OWNED BY; Schema: openaria; Owner: -
--

ALTER SEQUENCE lien_contrainte_dossier_coordination_seq OWNED BY lien_contrainte_dossier_coordination.lien_contrainte_dossier_coordination;


--
-- Name: lien_contrainte_etablissement; Type: TABLE; Schema: openaria; Owner: -
--

CREATE TABLE lien_contrainte_etablissement (
    lien_contrainte_etablissement integer NOT NULL,
    etablissement integer NOT NULL,
    contrainte integer NOT NULL,
    texte_complete text,
    recuperee boolean DEFAULT false
);


--
-- Name: TABLE lien_contrainte_etablissement; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON TABLE lien_contrainte_etablissement IS 'Table des contraintes appliquées au DC';


--
-- Name: COLUMN lien_contrainte_etablissement.lien_contrainte_etablissement; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN lien_contrainte_etablissement.lien_contrainte_etablissement IS 'Identifiant unique';


--
-- Name: COLUMN lien_contrainte_etablissement.etablissement; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN lien_contrainte_etablissement.etablissement IS 'Établissement auquel est appliqué la contrainte';


--
-- Name: COLUMN lien_contrainte_etablissement.contrainte; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN lien_contrainte_etablissement.contrainte IS 'Contrainte qui est appliquée à l''établissement';


--
-- Name: COLUMN lien_contrainte_etablissement.texte_complete; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN lien_contrainte_etablissement.texte_complete IS 'Texte complété de la contrainte';


--
-- Name: COLUMN lien_contrainte_etablissement.recuperee; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN lien_contrainte_etablissement.recuperee IS 'Si vrai, contrainte récupérée depuis le SIG pour cet établissement.';


--
-- Name: lien_contrainte_etablissement_seq; Type: SEQUENCE; Schema: openaria; Owner: -
--

CREATE SEQUENCE lien_contrainte_etablissement_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: lien_contrainte_etablissement_seq; Type: SEQUENCE OWNED BY; Schema: openaria; Owner: -
--

ALTER SEQUENCE lien_contrainte_etablissement_seq OWNED BY lien_contrainte_etablissement.lien_contrainte_etablissement;


--
-- Name: lien_courrier_contact; Type: TABLE; Schema: openaria; Owner: -
--

CREATE TABLE lien_courrier_contact (
    lien_courrier_contact integer NOT NULL,
    courrier integer,
    contact integer
);


--
-- Name: TABLE lien_courrier_contact; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON TABLE lien_courrier_contact IS 'Lien entre les tables courrier et contact.';


--
-- Name: COLUMN lien_courrier_contact.lien_courrier_contact; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN lien_courrier_contact.lien_courrier_contact IS 'Identifiant unique.';


--
-- Name: COLUMN lien_courrier_contact.courrier; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN lien_courrier_contact.courrier IS 'Lien vers le courrier.';


--
-- Name: COLUMN lien_courrier_contact.contact; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN lien_courrier_contact.contact IS 'Lien vers le contact.';


--
-- Name: lien_courrier_contact_seq; Type: SEQUENCE; Schema: openaria; Owner: -
--

CREATE SEQUENCE lien_courrier_contact_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: lien_courrier_contact_seq; Type: SEQUENCE OWNED BY; Schema: openaria; Owner: -
--

ALTER SEQUENCE lien_courrier_contact_seq OWNED BY lien_courrier_contact.lien_courrier_contact;


--
-- Name: lien_dossier_coordination_contact; Type: TABLE; Schema: openaria; Owner: -
--

CREATE TABLE lien_dossier_coordination_contact (
    lien_dossier_coordination_contact integer NOT NULL,
    dossier_coordination integer,
    contact integer
);


--
-- Name: TABLE lien_dossier_coordination_contact; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON TABLE lien_dossier_coordination_contact IS 'Liaison entre les dossiers de coordination et les contacts';


--
-- Name: COLUMN lien_dossier_coordination_contact.lien_dossier_coordination_contact; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN lien_dossier_coordination_contact.lien_dossier_coordination_contact IS 'Identifiant unique';


--
-- Name: COLUMN lien_dossier_coordination_contact.dossier_coordination; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN lien_dossier_coordination_contact.dossier_coordination IS 'Dossier de coordination lié au contact';


--
-- Name: COLUMN lien_dossier_coordination_contact.contact; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN lien_dossier_coordination_contact.contact IS 'Contact lié au dossier de coordination';


--
-- Name: lien_dossier_coordination_contact_seq; Type: SEQUENCE; Schema: openaria; Owner: -
--

CREATE SEQUENCE lien_dossier_coordination_contact_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: lien_dossier_coordination_contact_seq; Type: SEQUENCE OWNED BY; Schema: openaria; Owner: -
--

ALTER SEQUENCE lien_dossier_coordination_contact_seq OWNED BY lien_dossier_coordination_contact.lien_dossier_coordination_contact;


--
-- Name: lien_dossier_coordination_etablissement_type; Type: TABLE; Schema: openaria; Owner: -
--

CREATE TABLE lien_dossier_coordination_etablissement_type (
    lien_dossier_coordination_etablissement_type integer NOT NULL,
    dossier_coordination integer NOT NULL,
    etablissement_type integer NOT NULL
);


--
-- Name: TABLE lien_dossier_coordination_etablissement_type; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON TABLE lien_dossier_coordination_etablissement_type IS 'Liste des types secondaires d''établissement renseignés dans le dossier de coordination';


--
-- Name: COLUMN lien_dossier_coordination_etablissement_type.lien_dossier_coordination_etablissement_type; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN lien_dossier_coordination_etablissement_type.lien_dossier_coordination_etablissement_type IS 'Identifiant unique';


--
-- Name: COLUMN lien_dossier_coordination_etablissement_type.dossier_coordination; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN lien_dossier_coordination_etablissement_type.dossier_coordination IS 'Dossien de coordination';


--
-- Name: COLUMN lien_dossier_coordination_etablissement_type.etablissement_type; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN lien_dossier_coordination_etablissement_type.etablissement_type IS 'Type de l''établissement';


--
-- Name: lien_dossier_coordination_etablissement_type_seq; Type: SEQUENCE; Schema: openaria; Owner: -
--

CREATE SEQUENCE lien_dossier_coordination_etablissement_type_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: lien_dossier_coordination_etablissement_type_seq; Type: SEQUENCE OWNED BY; Schema: openaria; Owner: -
--

ALTER SEQUENCE lien_dossier_coordination_etablissement_type_seq OWNED BY lien_dossier_coordination_etablissement_type.lien_dossier_coordination_etablissement_type;


--
-- Name: lien_dossier_coordination_type_analyses_type; Type: TABLE; Schema: openaria; Owner: -
--

CREATE TABLE lien_dossier_coordination_type_analyses_type (
    lien_dossier_coordination_type_analyses_type integer NOT NULL,
    dossier_coordination_type integer NOT NULL,
    analyses_type integer NOT NULL,
    service integer NOT NULL
);


--
-- Name: TABLE lien_dossier_coordination_type_analyses_type; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON TABLE lien_dossier_coordination_type_analyses_type IS 'Liste des types d''analyse par type de dossier de coordination';


--
-- Name: COLUMN lien_dossier_coordination_type_analyses_type.lien_dossier_coordination_type_analyses_type; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN lien_dossier_coordination_type_analyses_type.lien_dossier_coordination_type_analyses_type IS 'Identifiant unique';


--
-- Name: COLUMN lien_dossier_coordination_type_analyses_type.dossier_coordination_type; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN lien_dossier_coordination_type_analyses_type.dossier_coordination_type IS 'Type de dossier de coordination';


--
-- Name: COLUMN lien_dossier_coordination_type_analyses_type.analyses_type; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN lien_dossier_coordination_type_analyses_type.analyses_type IS 'Type d''analyse';


--
-- Name: COLUMN lien_dossier_coordination_type_analyses_type.service; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN lien_dossier_coordination_type_analyses_type.service IS 'Service';


--
-- Name: lien_dossier_coordination_type_analyses_type_seq; Type: SEQUENCE; Schema: openaria; Owner: -
--

CREATE SEQUENCE lien_dossier_coordination_type_analyses_type_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: lien_dossier_coordination_type_analyses_type_seq; Type: SEQUENCE OWNED BY; Schema: openaria; Owner: -
--

ALTER SEQUENCE lien_dossier_coordination_type_analyses_type_seq OWNED BY lien_dossier_coordination_type_analyses_type.lien_dossier_coordination_type_analyses_type;


--
-- Name: lien_essai_realise_analyses; Type: TABLE; Schema: openaria; Owner: -
--

CREATE TABLE lien_essai_realise_analyses (
    lien_essai_realise_analyses integer NOT NULL,
    essai_realise integer NOT NULL,
    analyses integer NOT NULL,
    concluant boolean,
    complement text
);


--
-- Name: TABLE lien_essai_realise_analyses; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON TABLE lien_essai_realise_analyses IS 'Liste des essais réalisés par analyses''';


--
-- Name: COLUMN lien_essai_realise_analyses.lien_essai_realise_analyses; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN lien_essai_realise_analyses.lien_essai_realise_analyses IS 'Identifiant unique';


--
-- Name: COLUMN lien_essai_realise_analyses.essai_realise; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN lien_essai_realise_analyses.essai_realise IS 'Essai réalisé';


--
-- Name: COLUMN lien_essai_realise_analyses.analyses; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN lien_essai_realise_analyses.analyses IS 'Analyse';


--
-- Name: COLUMN lien_essai_realise_analyses.concluant; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN lien_essai_realise_analyses.concluant IS 'Est-ce que l''essai réalisé a été concluant ?';


--
-- Name: COLUMN lien_essai_realise_analyses.complement; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN lien_essai_realise_analyses.complement IS 'Complément';


--
-- Name: lien_essai_realise_analyses_seq; Type: SEQUENCE; Schema: openaria; Owner: -
--

CREATE SEQUENCE lien_essai_realise_analyses_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: lien_essai_realise_analyses_seq; Type: SEQUENCE OWNED BY; Schema: openaria; Owner: -
--

ALTER SEQUENCE lien_essai_realise_analyses_seq OWNED BY lien_essai_realise_analyses.lien_essai_realise_analyses;


--
-- Name: lien_etablissement_e_type; Type: TABLE; Schema: openaria; Owner: -
--

CREATE TABLE lien_etablissement_e_type (
    lien_etablissement_e_type integer NOT NULL,
    etablissement integer,
    etablissement_type integer
);


--
-- Name: TABLE lien_etablissement_e_type; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON TABLE lien_etablissement_e_type IS 'Liens entre établissement et types pour la saisie des types secondaires.';


--
-- Name: COLUMN lien_etablissement_e_type.lien_etablissement_e_type; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN lien_etablissement_e_type.lien_etablissement_e_type IS 'Identifiant unique';


--
-- Name: COLUMN lien_etablissement_e_type.etablissement; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN lien_etablissement_e_type.etablissement IS 'Lien vers l''établissement';


--
-- Name: COLUMN lien_etablissement_e_type.etablissement_type; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN lien_etablissement_e_type.etablissement_type IS 'Lien vers le type';


--
-- Name: lien_etablissement_e_type_seq; Type: SEQUENCE; Schema: openaria; Owner: -
--

CREATE SEQUENCE lien_etablissement_e_type_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: lien_etablissement_e_type_seq; Type: SEQUENCE OWNED BY; Schema: openaria; Owner: -
--

ALTER SEQUENCE lien_etablissement_e_type_seq OWNED BY lien_etablissement_e_type.lien_etablissement_e_type;


--
-- Name: lien_prescription_reglementaire_etablissement_categorie; Type: TABLE; Schema: openaria; Owner: -
--

CREATE TABLE lien_prescription_reglementaire_etablissement_categorie (
    lien_prescription_reglementaire_etablissement_categorie integer NOT NULL,
    prescription_reglementaire integer NOT NULL,
    etablissement_categorie integer NOT NULL
);


--
-- Name: TABLE lien_prescription_reglementaire_etablissement_categorie; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON TABLE lien_prescription_reglementaire_etablissement_categorie IS 'Liste des catégories d''établissement concernés par la prescription réglementaire';


--
-- Name: COLUMN lien_prescription_reglementaire_etablissement_categorie.lien_prescription_reglementaire_etablissement_categorie; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN lien_prescription_reglementaire_etablissement_categorie.lien_prescription_reglementaire_etablissement_categorie IS 'Identifiant unique';


--
-- Name: COLUMN lien_prescription_reglementaire_etablissement_categorie.prescription_reglementaire; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN lien_prescription_reglementaire_etablissement_categorie.prescription_reglementaire IS 'Prescription réglementaire';


--
-- Name: COLUMN lien_prescription_reglementaire_etablissement_categorie.etablissement_categorie; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN lien_prescription_reglementaire_etablissement_categorie.etablissement_categorie IS 'Catégorie de l''établissement';


--
-- Name: lien_prescription_reglementaire_etablissement_categorie_seq; Type: SEQUENCE; Schema: openaria; Owner: -
--

CREATE SEQUENCE lien_prescription_reglementaire_etablissement_categorie_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: lien_prescription_reglementaire_etablissement_categorie_seq; Type: SEQUENCE OWNED BY; Schema: openaria; Owner: -
--

ALTER SEQUENCE lien_prescription_reglementaire_etablissement_categorie_seq OWNED BY lien_prescription_reglementaire_etablissement_categorie.lien_prescription_reglementaire_etablissement_categorie;


--
-- Name: lien_prescription_reglementaire_etablissement_type; Type: TABLE; Schema: openaria; Owner: -
--

CREATE TABLE lien_prescription_reglementaire_etablissement_type (
    lien_prescription_reglementaire_etablissement_type integer NOT NULL,
    prescription_reglementaire integer NOT NULL,
    etablissement_type integer NOT NULL
);


--
-- Name: TABLE lien_prescription_reglementaire_etablissement_type; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON TABLE lien_prescription_reglementaire_etablissement_type IS 'Liste des types d''établissement concernés par la prescription réglementaire';


--
-- Name: COLUMN lien_prescription_reglementaire_etablissement_type.lien_prescription_reglementaire_etablissement_type; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN lien_prescription_reglementaire_etablissement_type.lien_prescription_reglementaire_etablissement_type IS 'Identifiant unique';


--
-- Name: COLUMN lien_prescription_reglementaire_etablissement_type.prescription_reglementaire; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN lien_prescription_reglementaire_etablissement_type.prescription_reglementaire IS 'Prescription réglementaire';


--
-- Name: COLUMN lien_prescription_reglementaire_etablissement_type.etablissement_type; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN lien_prescription_reglementaire_etablissement_type.etablissement_type IS 'Type de l''établissement';


--
-- Name: lien_prescription_reglementaire_etablissement_type_seq; Type: SEQUENCE; Schema: openaria; Owner: -
--

CREATE SEQUENCE lien_prescription_reglementaire_etablissement_type_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: lien_prescription_reglementaire_etablissement_type_seq; Type: SEQUENCE OWNED BY; Schema: openaria; Owner: -
--

ALTER SEQUENCE lien_prescription_reglementaire_etablissement_type_seq OWNED BY lien_prescription_reglementaire_etablissement_type.lien_prescription_reglementaire_etablissement_type;


--
-- Name: lien_reglementation_applicable_etablissement_categorie; Type: TABLE; Schema: openaria; Owner: -
--

CREATE TABLE lien_reglementation_applicable_etablissement_categorie (
    lien_reglementation_applicable_etablissement_categorie integer NOT NULL,
    reglementation_applicable integer NOT NULL,
    etablissement_categorie integer NOT NULL
);


--
-- Name: TABLE lien_reglementation_applicable_etablissement_categorie; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON TABLE lien_reglementation_applicable_etablissement_categorie IS 'Liste des catégories d''établissement concernées par la réglementation applicable';


--
-- Name: COLUMN lien_reglementation_applicable_etablissement_categorie.lien_reglementation_applicable_etablissement_categorie; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN lien_reglementation_applicable_etablissement_categorie.lien_reglementation_applicable_etablissement_categorie IS 'Identifiant unique';


--
-- Name: COLUMN lien_reglementation_applicable_etablissement_categorie.reglementation_applicable; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN lien_reglementation_applicable_etablissement_categorie.reglementation_applicable IS 'Réglementation applicable';


--
-- Name: COLUMN lien_reglementation_applicable_etablissement_categorie.etablissement_categorie; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN lien_reglementation_applicable_etablissement_categorie.etablissement_categorie IS 'Catégorie de l''établissement';


--
-- Name: lien_reglementation_applicable_etablissement_categorie_seq; Type: SEQUENCE; Schema: openaria; Owner: -
--

CREATE SEQUENCE lien_reglementation_applicable_etablissement_categorie_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: lien_reglementation_applicable_etablissement_categorie_seq; Type: SEQUENCE OWNED BY; Schema: openaria; Owner: -
--

ALTER SEQUENCE lien_reglementation_applicable_etablissement_categorie_seq OWNED BY lien_reglementation_applicable_etablissement_categorie.lien_reglementation_applicable_etablissement_categorie;


--
-- Name: lien_reglementation_applicable_etablissement_type; Type: TABLE; Schema: openaria; Owner: -
--

CREATE TABLE lien_reglementation_applicable_etablissement_type (
    lien_reglementation_applicable_etablissement_type integer NOT NULL,
    reglementation_applicable integer NOT NULL,
    etablissement_type integer NOT NULL
);


--
-- Name: TABLE lien_reglementation_applicable_etablissement_type; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON TABLE lien_reglementation_applicable_etablissement_type IS 'Liste des types d''établissement concernés par la réglementation applicable';


--
-- Name: COLUMN lien_reglementation_applicable_etablissement_type.lien_reglementation_applicable_etablissement_type; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN lien_reglementation_applicable_etablissement_type.lien_reglementation_applicable_etablissement_type IS 'Identifiant unique';


--
-- Name: COLUMN lien_reglementation_applicable_etablissement_type.reglementation_applicable; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN lien_reglementation_applicable_etablissement_type.reglementation_applicable IS 'Réglementation applicable';


--
-- Name: COLUMN lien_reglementation_applicable_etablissement_type.etablissement_type; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN lien_reglementation_applicable_etablissement_type.etablissement_type IS 'Type de l''établissement';


--
-- Name: lien_reglementation_applicable_etablissement_type_seq; Type: SEQUENCE; Schema: openaria; Owner: -
--

CREATE SEQUENCE lien_reglementation_applicable_etablissement_type_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: lien_reglementation_applicable_etablissement_type_seq; Type: SEQUENCE OWNED BY; Schema: openaria; Owner: -
--

ALTER SEQUENCE lien_reglementation_applicable_etablissement_type_seq OWNED BY lien_reglementation_applicable_etablissement_type.lien_reglementation_applicable_etablissement_type;


--
-- Name: lien_reunion_r_instance_r_i_membre; Type: TABLE; Schema: openaria; Owner: -
--

CREATE TABLE lien_reunion_r_instance_r_i_membre (
    lien_reunion_r_instance_r_i_membre integer NOT NULL,
    reunion integer NOT NULL,
    reunion_instance integer NOT NULL,
    reunion_instance_membre integer,
    observation text
);


--
-- Name: lien_reunion_r_instance_r_i_membre_seq; Type: SEQUENCE; Schema: openaria; Owner: -
--

CREATE SEQUENCE lien_reunion_r_instance_r_i_membre_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: lien_reunion_r_instance_r_i_membre_seq; Type: SEQUENCE OWNED BY; Schema: openaria; Owner: -
--

ALTER SEQUENCE lien_reunion_r_instance_r_i_membre_seq OWNED BY lien_reunion_r_instance_r_i_membre.lien_reunion_r_instance_r_i_membre;


--
-- Name: modele_edition; Type: TABLE; Schema: openaria; Owner: -
--

CREATE TABLE modele_edition (
    modele_edition integer NOT NULL,
    libelle character varying(100) NOT NULL,
    description text,
    om_validite_debut date,
    om_validite_fin date,
    code character varying(15),
    courrier_type integer NOT NULL,
    om_lettretype_id character varying(50)
);


--
-- Name: TABLE modele_edition; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON TABLE modele_edition IS 'Modèle d''édition pour les courriers générés.';


--
-- Name: COLUMN modele_edition.modele_edition; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN modele_edition.modele_edition IS 'Identifiant unique.';


--
-- Name: COLUMN modele_edition.libelle; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN modele_edition.libelle IS 'Libellé du modèle d''édition.';


--
-- Name: COLUMN modele_edition.description; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN modele_edition.description IS 'Description du modèle d''édition.';


--
-- Name: COLUMN modele_edition.om_validite_debut; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN modele_edition.om_validite_debut IS 'Date de début de validité du modèle d''édition.';


--
-- Name: COLUMN modele_edition.om_validite_fin; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN modele_edition.om_validite_fin IS 'Date de fin de validité du modèle d''édition.';


--
-- Name: COLUMN modele_edition.courrier_type; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN modele_edition.courrier_type IS 'Type de courrier pouvant avoir le modèle d''édition.';


--
-- Name: modele_edition_seq; Type: SEQUENCE; Schema: openaria; Owner: -
--

CREATE SEQUENCE modele_edition_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: modele_edition_seq; Type: SEQUENCE OWNED BY; Schema: openaria; Owner: -
--

ALTER SEQUENCE modele_edition_seq OWNED BY modele_edition.modele_edition;


--
-- Name: periodicite_visites; Type: TABLE; Schema: openaria; Owner: -
--

CREATE TABLE periodicite_visites (
    periodicite_visites integer NOT NULL,
    periodicite integer NOT NULL,
    etablissement_type integer NOT NULL,
    etablissement_categorie integer NOT NULL,
    commentaire text,
    avec_locaux_sommeil boolean,
    sans_locaux_sommeil boolean
);


--
-- Name: TABLE periodicite_visites; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON TABLE periodicite_visites IS 'Périodicité des visites de l''établissement en fonction de son type, de sa catégorie et de la présence de locaux à sommeil.';


--
-- Name: COLUMN periodicite_visites.periodicite_visites; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN periodicite_visites.periodicite_visites IS 'Identifiant unique';


--
-- Name: COLUMN periodicite_visites.periodicite; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN periodicite_visites.periodicite IS 'Périodicité des visites en années';


--
-- Name: COLUMN periodicite_visites.etablissement_type; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN periodicite_visites.etablissement_type IS 'Type de l''établissement';


--
-- Name: COLUMN periodicite_visites.etablissement_categorie; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN periodicite_visites.etablissement_categorie IS 'Catégorie de l''établissement';


--
-- Name: COLUMN periodicite_visites.commentaire; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN periodicite_visites.commentaire IS 'Commentaire destiné à permettre la saisie du contexte réglementaire';


--
-- Name: COLUMN periodicite_visites.avec_locaux_sommeil; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN periodicite_visites.avec_locaux_sommeil IS 'Applicable aux établissements avec locaux à sommeil ?';


--
-- Name: COLUMN periodicite_visites.sans_locaux_sommeil; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN periodicite_visites.sans_locaux_sommeil IS 'Applicable aux établissements sans locaux à sommeil ?';


--
-- Name: periodicite_visites_seq; Type: SEQUENCE; Schema: openaria; Owner: -
--

CREATE SEQUENCE periodicite_visites_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: periodicite_visites_seq; Type: SEQUENCE OWNED BY; Schema: openaria; Owner: -
--

ALTER SEQUENCE periodicite_visites_seq OWNED BY periodicite_visites.periodicite_visites;


--
-- Name: piece; Type: TABLE; Schema: openaria; Owner: -
--

CREATE TABLE piece (
    piece integer NOT NULL,
    etablissement integer,
    dossier_coordination integer,
    nom character varying(250),
    piece_type integer,
    uid character varying(64),
    om_date_creation date DEFAULT (now())::date NOT NULL,
    dossier_instruction integer,
    date_reception date,
    date_emission date,
    piece_statut integer,
    date_butoir date,
    suivi boolean,
    commentaire_suivi text,
    lu boolean,
    choix_lien character varying(40),
    service integer NOT NULL
);


--
-- Name: TABLE piece; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON TABLE piece IS 'Pièces liés à un Dossier ou un Établissement (fichiers chargés, créés ou numérisés)';


--
-- Name: COLUMN piece.piece; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN piece.piece IS 'Identifiant unique';


--
-- Name: COLUMN piece.etablissement; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN piece.etablissement IS 'Lien vers l''établissement auquel est rattachée la pièce';


--
-- Name: COLUMN piece.dossier_coordination; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN piece.dossier_coordination IS 'Lien vers le dossier de coordination.';


--
-- Name: COLUMN piece.nom; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN piece.nom IS 'Nom du fichier';


--
-- Name: COLUMN piece.piece_type; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN piece.piece_type IS 'Lien vers les types de fichiers';


--
-- Name: COLUMN piece.uid; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN piece.uid IS 'Identifiant unique du fichier.';


--
-- Name: COLUMN piece.om_date_creation; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN piece.om_date_creation IS 'Date d''ajout du document';


--
-- Name: COLUMN piece.dossier_instruction; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN piece.dossier_instruction IS 'Lien vers le dossier d''instruction.';


--
-- Name: COLUMN piece.date_reception; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN piece.date_reception IS 'Date de réception de la pièce.';


--
-- Name: COLUMN piece.date_emission; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN piece.date_emission IS 'Date d''émission de la pièce.';


--
-- Name: COLUMN piece.piece_statut; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN piece.piece_statut IS 'Statut de la pièce.';


--
-- Name: COLUMN piece.date_butoir; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN piece.date_butoir IS 'Date butoir de la pièce.';


--
-- Name: COLUMN piece.suivi; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN piece.suivi IS 'Indique que la pièce est suivi.';


--
-- Name: COLUMN piece.commentaire_suivi; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN piece.commentaire_suivi IS 'Commentaire sur le suivi de la pièce.';


--
-- Name: COLUMN piece.lu; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN piece.lu IS 'Indique que la pièce a été lu.';


--
-- Name: COLUMN piece.choix_lien; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN piece.choix_lien IS 'Choix de l''élément de liaison (établissement, dc ou di).';


--
-- Name: COLUMN piece.service; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN piece.service IS 'Service lié au document.';


--
-- Name: piece_seq; Type: SEQUENCE; Schema: openaria; Owner: -
--

CREATE SEQUENCE piece_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: piece_seq; Type: SEQUENCE OWNED BY; Schema: openaria; Owner: -
--

ALTER SEQUENCE piece_seq OWNED BY piece.piece;


--
-- Name: piece_statut; Type: TABLE; Schema: openaria; Owner: -
--

CREATE TABLE piece_statut (
    piece_statut integer NOT NULL,
    libelle character varying(100),
    code character varying(20),
    description text,
    om_validite_debut date,
    om_validite_fin date
);


--
-- Name: TABLE piece_statut; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON TABLE piece_statut IS 'Statut possible d''une pièce (document entrant).';


--
-- Name: COLUMN piece_statut.piece_statut; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN piece_statut.piece_statut IS 'Identifiant unique.';


--
-- Name: COLUMN piece_statut.libelle; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN piece_statut.libelle IS 'Libellé du statut de pièce.';


--
-- Name: COLUMN piece_statut.code; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN piece_statut.code IS 'Code du statut de pièce.';


--
-- Name: COLUMN piece_statut.description; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN piece_statut.description IS 'Description du statut de pièce.';


--
-- Name: COLUMN piece_statut.om_validite_debut; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN piece_statut.om_validite_debut IS 'Date de début de validité.';


--
-- Name: COLUMN piece_statut.om_validite_fin; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN piece_statut.om_validite_fin IS 'Date de fin de validité.';


--
-- Name: piece_statut_seq; Type: SEQUENCE; Schema: openaria; Owner: -
--

CREATE SEQUENCE piece_statut_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: piece_statut_seq; Type: SEQUENCE OWNED BY; Schema: openaria; Owner: -
--

ALTER SEQUENCE piece_statut_seq OWNED BY piece_statut.piece_statut;


--
-- Name: piece_type; Type: TABLE; Schema: openaria; Owner: -
--

CREATE TABLE piece_type (
    piece_type integer NOT NULL,
    libelle character varying(255) NOT NULL,
    code character varying(150),
    description character varying(250),
    om_validite_debut date,
    om_validite_fin date
);


--
-- Name: TABLE piece_type; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON TABLE piece_type IS 'Typage des pièces chargées';


--
-- Name: COLUMN piece_type.piece_type; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN piece_type.piece_type IS 'Identifiant unique';


--
-- Name: COLUMN piece_type.libelle; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN piece_type.libelle IS 'Texte de description du type de pièce';


--
-- Name: COLUMN piece_type.code; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN piece_type.code IS 'Code de classification des pièces, servant pour le stockage GED par exemple.';


--
-- Name: COLUMN piece_type.description; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN piece_type.description IS 'Description détaillée du type de pièce.';


--
-- Name: COLUMN piece_type.om_validite_debut; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN piece_type.om_validite_debut IS 'Date de fin de validité.';


--
-- Name: piece_type_seq; Type: SEQUENCE; Schema: openaria; Owner: -
--

CREATE SEQUENCE piece_type_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: piece_type_seq; Type: SEQUENCE OWNED BY; Schema: openaria; Owner: -
--

ALTER SEQUENCE piece_type_seq OWNED BY piece_type.piece_type;


--
-- Name: prescription; Type: TABLE; Schema: openaria; Owner: -
--

CREATE TABLE prescription (
    prescription integer NOT NULL,
    analyses integer NOT NULL,
    ordre integer NOT NULL,
    prescription_reglementaire integer NOT NULL,
    pr_description_om_html text,
    pr_defavorable boolean,
    ps_description_om_html text
);


--
-- Name: TABLE prescription; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON TABLE prescription IS 'Prescriptions des analyses';


--
-- Name: COLUMN prescription.prescription; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN prescription.prescription IS 'Identifiant unique';


--
-- Name: COLUMN prescription.analyses; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN prescription.analyses IS 'Analyse de la prescription';


--
-- Name: COLUMN prescription.ordre; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN prescription.ordre IS 'Ordre de la prescription dans la vue de l''analyse';


--
-- Name: COLUMN prescription.prescription_reglementaire; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN prescription.prescription_reglementaire IS 'Éventuelle prescription réglementaire.';


--
-- Name: COLUMN prescription.pr_description_om_html; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN prescription.pr_description_om_html IS 'Description de la prescription réglementaire';


--
-- Name: COLUMN prescription.pr_defavorable; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN prescription.pr_defavorable IS 'Donne lieu à un défavorable ?';


--
-- Name: COLUMN prescription.ps_description_om_html; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN prescription.ps_description_om_html IS 'Description de la prescription spécifique';


--
-- Name: prescription_reglementaire; Type: TABLE; Schema: openaria; Owner: -
--

CREATE TABLE prescription_reglementaire (
    prescription_reglementaire integer NOT NULL,
    service integer NOT NULL,
    tete_de_chapitre1 character varying(250),
    tete_de_chapitre2 character varying(250),
    libelle character varying(100),
    description_pr_om_html text,
    defavorable boolean,
    om_validite_debut date,
    om_validite_fin date
);


--
-- Name: TABLE prescription_reglementaire; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON TABLE prescription_reglementaire IS 'Prescription réglementaire';


--
-- Name: COLUMN prescription_reglementaire.prescription_reglementaire; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN prescription_reglementaire.prescription_reglementaire IS 'Identifiant unique';


--
-- Name: COLUMN prescription_reglementaire.service; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN prescription_reglementaire.service IS 'Service de la prescription réglementaire';


--
-- Name: COLUMN prescription_reglementaire.tete_de_chapitre1; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN prescription_reglementaire.tete_de_chapitre1 IS 'Valeur de la tête de chapitre n°1';


--
-- Name: COLUMN prescription_reglementaire.tete_de_chapitre2; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN prescription_reglementaire.tete_de_chapitre2 IS 'Valeur de la tête de chapitre n°2';


--
-- Name: COLUMN prescription_reglementaire.libelle; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN prescription_reglementaire.libelle IS 'Libellé de la prescription réglementaire';


--
-- Name: COLUMN prescription_reglementaire.description_pr_om_html; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN prescription_reglementaire.description_pr_om_html IS 'Description de la prescription réglementaire';


--
-- Name: COLUMN prescription_reglementaire.defavorable; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN prescription_reglementaire.defavorable IS 'Donne lieu à un défavorable ?';


--
-- Name: COLUMN prescription_reglementaire.om_validite_debut; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN prescription_reglementaire.om_validite_debut IS 'Date de début de validité de la prescription réglementaire';


--
-- Name: COLUMN prescription_reglementaire.om_validite_fin; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN prescription_reglementaire.om_validite_fin IS 'Date de fin de validité de la prescription réglementaire';


--
-- Name: prescription_reglementaire_seq; Type: SEQUENCE; Schema: openaria; Owner: -
--

CREATE SEQUENCE prescription_reglementaire_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: prescription_reglementaire_seq; Type: SEQUENCE OWNED BY; Schema: openaria; Owner: -
--

ALTER SEQUENCE prescription_reglementaire_seq OWNED BY prescription_reglementaire.prescription_reglementaire;


--
-- Name: prescription_seq; Type: SEQUENCE; Schema: openaria; Owner: -
--

CREATE SEQUENCE prescription_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: prescription_seq; Type: SEQUENCE OWNED BY; Schema: openaria; Owner: -
--

ALTER SEQUENCE prescription_seq OWNED BY prescription.prescription;


--
-- Name: prescription_specifique; Type: TABLE; Schema: openaria; Owner: -
--

CREATE TABLE prescription_specifique (
    prescription_specifique integer NOT NULL,
    service integer NOT NULL,
    libelle character varying(100),
    description_ps_om_html text,
    prescription_reglementaire integer NOT NULL,
    om_validite_debut date,
    om_validite_fin date
);


--
-- Name: TABLE prescription_specifique; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON TABLE prescription_specifique IS 'Prescription spécifique';


--
-- Name: COLUMN prescription_specifique.prescription_specifique; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN prescription_specifique.prescription_specifique IS 'Identifiant unique';


--
-- Name: COLUMN prescription_specifique.service; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN prescription_specifique.service IS 'Service de la prescription spécifique';


--
-- Name: COLUMN prescription_specifique.libelle; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN prescription_specifique.libelle IS 'Libellé de la prescription spécifique';


--
-- Name: COLUMN prescription_specifique.description_ps_om_html; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN prescription_specifique.description_ps_om_html IS 'Description de la prescription spécifique';


--
-- Name: COLUMN prescription_specifique.prescription_reglementaire; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN prescription_specifique.prescription_reglementaire IS 'Prescription réglementaire à laquelle elle se rattache';


--
-- Name: COLUMN prescription_specifique.om_validite_debut; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN prescription_specifique.om_validite_debut IS 'Date de début de validité de la prescription spécifique';


--
-- Name: COLUMN prescription_specifique.om_validite_fin; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN prescription_specifique.om_validite_fin IS 'Date de fin de validité de la prescription spécifique';


--
-- Name: prescription_specifique_seq; Type: SEQUENCE; Schema: openaria; Owner: -
--

CREATE SEQUENCE prescription_specifique_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: prescription_specifique_seq; Type: SEQUENCE OWNED BY; Schema: openaria; Owner: -
--

ALTER SEQUENCE prescription_specifique_seq OWNED BY prescription_specifique.prescription_specifique;


--
-- Name: proces_verbal; Type: TABLE; Schema: openaria; Owner: -
--

CREATE TABLE proces_verbal (
    proces_verbal integer NOT NULL,
    numero character varying(50),
    dossier_instruction integer NOT NULL,
    dossier_instruction_reunion integer NOT NULL,
    modele_edition integer NOT NULL,
    date_redaction date NOT NULL,
    signataire integer,
    genere boolean NOT NULL,
    om_fichier_signe character varying(64),
    courrier_genere integer
);


--
-- Name: TABLE proces_verbal; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON TABLE proces_verbal IS 'Procès verbaux du dossier d''instruction';


--
-- Name: COLUMN proces_verbal.proces_verbal; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN proces_verbal.proces_verbal IS 'Identifiant unique';


--
-- Name: COLUMN proces_verbal.numero; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN proces_verbal.numero IS 'Numéro du PV';


--
-- Name: COLUMN proces_verbal.dossier_instruction; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN proces_verbal.dossier_instruction IS 'Dossier d''instruction auquel est rattaché le PV';


--
-- Name: COLUMN proces_verbal.dossier_instruction_reunion; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN proces_verbal.dossier_instruction_reunion IS 'Demande de passage en réunion du dossier d''instruction';


--
-- Name: COLUMN proces_verbal.modele_edition; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN proces_verbal.modele_edition IS 'Modèle d''édition du PV : par exemple avec ou sans avis';


--
-- Name: COLUMN proces_verbal.date_redaction; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN proces_verbal.date_redaction IS 'Date de rédaction du PV';


--
-- Name: COLUMN proces_verbal.signataire; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN proces_verbal.signataire IS 'Signataire du PV';


--
-- Name: COLUMN proces_verbal.genere; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN proces_verbal.genere IS 'Vrai si généré, faux si ajouté';


--
-- Name: COLUMN proces_verbal.om_fichier_signe; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN proces_verbal.om_fichier_signe IS 'Document déposé par l''utilisateur';


--
-- Name: COLUMN proces_verbal.courrier_genere; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN proces_verbal.courrier_genere IS 'Lien vers le PV généré et son document signé associé.';


--
-- Name: proces_verbal_seq; Type: SEQUENCE; Schema: openaria; Owner: -
--

CREATE SEQUENCE proces_verbal_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: proces_verbal_seq; Type: SEQUENCE OWNED BY; Schema: openaria; Owner: -
--

ALTER SEQUENCE proces_verbal_seq OWNED BY proces_verbal.proces_verbal;


--
-- Name: programmation; Type: TABLE; Schema: openaria; Owner: -
--

CREATE TABLE programmation (
    programmation integer NOT NULL,
    annee integer NOT NULL,
    numero_semaine integer NOT NULL,
    version integer NOT NULL,
    programmation_etat integer NOT NULL,
    date_modification date NOT NULL,
    convocation_exploitants character varying(20),
    date_envoi_ce date,
    convocation_membres character varying(20),
    date_envoi_cm date,
    semaine_annee character varying(7) NOT NULL,
    service integer NOT NULL,
    planifier_nouveau boolean DEFAULT false
);


--
-- Name: programmation_etat; Type: TABLE; Schema: openaria; Owner: -
--

CREATE TABLE programmation_etat (
    programmation_etat integer NOT NULL,
    code character varying(20),
    libelle character varying(100),
    description character varying(250),
    om_validite_debut date,
    om_validite_fin date
);


--
-- Name: programmation_etat_seq; Type: SEQUENCE; Schema: openaria; Owner: -
--

CREATE SEQUENCE programmation_etat_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: programmation_etat_seq; Type: SEQUENCE OWNED BY; Schema: openaria; Owner: -
--

ALTER SEQUENCE programmation_etat_seq OWNED BY programmation_etat.programmation_etat;


--
-- Name: programmation_seq; Type: SEQUENCE; Schema: openaria; Owner: -
--

CREATE SEQUENCE programmation_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: programmation_seq; Type: SEQUENCE OWNED BY; Schema: openaria; Owner: -
--

ALTER SEQUENCE programmation_seq OWNED BY programmation.programmation;


--
-- Name: quartier; Type: TABLE; Schema: openaria; Owner: -
--

CREATE TABLE quartier (
    quartier integer NOT NULL,
    arrondissement integer NOT NULL,
    code_impots character varying(3) NOT NULL,
    libelle character varying(40) NOT NULL
);


--
-- Name: TABLE quartier; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON TABLE quartier IS 'Liste des quartiers';


--
-- Name: COLUMN quartier.quartier; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN quartier.quartier IS 'Identifiant unique.';


--
-- Name: COLUMN quartier.arrondissement; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN quartier.arrondissement IS 'Arrondissement du quartier.';


--
-- Name: COLUMN quartier.code_impots; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN quartier.code_impots IS 'Code impôts du quartier.';


--
-- Name: COLUMN quartier.libelle; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN quartier.libelle IS 'Libellé du quartier.';


--
-- Name: quartier_seq; Type: SEQUENCE; Schema: openaria; Owner: -
--

CREATE SEQUENCE quartier_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: quartier_seq; Type: SEQUENCE OWNED BY; Schema: openaria; Owner: -
--

ALTER SEQUENCE quartier_seq OWNED BY quartier.quartier;


--
-- Name: reglementation_applicable; Type: TABLE; Schema: openaria; Owner: -
--

CREATE TABLE reglementation_applicable (
    reglementation_applicable integer NOT NULL,
    service integer NOT NULL,
    libelle character varying(100),
    description text,
    om_validite_debut date,
    om_validite_fin date,
    annee_debut_application integer,
    annee_fin_application integer
);


--
-- Name: TABLE reglementation_applicable; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON TABLE reglementation_applicable IS 'Réglementations applicables';


--
-- Name: COLUMN reglementation_applicable.reglementation_applicable; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN reglementation_applicable.reglementation_applicable IS 'Identifiant unique';


--
-- Name: COLUMN reglementation_applicable.service; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN reglementation_applicable.service IS 'Service de la réglementation applicable';


--
-- Name: COLUMN reglementation_applicable.libelle; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN reglementation_applicable.libelle IS 'Libellé de la réglementation applicable';


--
-- Name: COLUMN reglementation_applicable.description; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN reglementation_applicable.description IS 'Description de la réglementation applicable';


--
-- Name: COLUMN reglementation_applicable.om_validite_debut; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN reglementation_applicable.om_validite_debut IS 'Date de début de validité de la réglementation applicable';


--
-- Name: COLUMN reglementation_applicable.om_validite_fin; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN reglementation_applicable.om_validite_fin IS 'Date de fin de validité de la réglementation applicable';


--
-- Name: reglementation_applicable_seq; Type: SEQUENCE; Schema: openaria; Owner: -
--

CREATE SEQUENCE reglementation_applicable_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: reglementation_applicable_seq; Type: SEQUENCE OWNED BY; Schema: openaria; Owner: -
--

ALTER SEQUENCE reglementation_applicable_seq OWNED BY reglementation_applicable.reglementation_applicable;


--
-- Name: reunion; Type: TABLE; Schema: openaria; Owner: -
--

CREATE TABLE reunion (
    reunion integer NOT NULL,
    code character varying(21) NOT NULL,
    reunion_type integer NOT NULL,
    libelle character varying(100) NOT NULL,
    date_reunion date NOT NULL,
    heure_reunion character varying(5),
    lieu_adresse_ligne1 character varying(100),
    lieu_adresse_ligne2 character varying(100),
    lieu_salle character varying(100),
    listes_de_diffusion text,
    participants text,
    numerotation_simple integer NOT NULL,
    date_convocation date,
    reunion_cloture boolean DEFAULT false,
    date_cloture date,
    om_fichier_reunion_odj character varying(64),
    om_final_reunion_odj boolean,
    om_fichier_reunion_cr_global character varying(64),
    om_final_reunion_cr_global boolean,
    om_fichier_reunion_cr_global_signe character varying(64),
    om_fichier_reunion_cr_par_dossier_signe character varying(64),
    planifier_nouveau boolean DEFAULT false,
    numerotation_complexe text,
    numerotation_mode character varying(30)
);


--
-- Name: TABLE reunion; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON TABLE reunion IS 'Demande de passage en reunion';


--
-- Name: COLUMN reunion.reunion; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN reunion.reunion IS 'Identifiant unique';


--
-- Name: COLUMN reunion.code; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN reunion.code IS 'Code de la reunion';


--
-- Name: COLUMN reunion.reunion_type; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN reunion.reunion_type IS 'Type de la reunion';


--
-- Name: COLUMN reunion.libelle; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN reunion.libelle IS 'Libellé de la reunion';


--
-- Name: COLUMN reunion.date_reunion; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN reunion.date_reunion IS 'Date de passage de la reunion';


--
-- Name: COLUMN reunion.heure_reunion; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN reunion.heure_reunion IS 'Heure de passage de la reunion';


--
-- Name: COLUMN reunion.lieu_adresse_ligne1; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN reunion.lieu_adresse_ligne1 IS 'Adresse de passage de la reunion';


--
-- Name: COLUMN reunion.lieu_adresse_ligne2; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN reunion.lieu_adresse_ligne2 IS 'Complément d''adresse de passage de la reunion';


--
-- Name: COLUMN reunion.lieu_salle; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN reunion.lieu_salle IS 'Salle de passage de la reunion';


--
-- Name: COLUMN reunion.listes_de_diffusion; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN reunion.listes_de_diffusion IS 'Liste des emails à notifier pour le compte rendu de la reunion';


--
-- Name: COLUMN reunion.participants; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN reunion.participants IS 'Participants à la reunion';


--
-- Name: reunion_avis; Type: TABLE; Schema: openaria; Owner: -
--

CREATE TABLE reunion_avis (
    reunion_avis integer NOT NULL,
    code character varying(20),
    libelle character varying(100),
    description text,
    service integer NOT NULL,
    om_validite_debut date,
    om_validite_fin date,
    categorie character varying(50)
);


--
-- Name: reunion_avis_seq; Type: SEQUENCE; Schema: openaria; Owner: -
--

CREATE SEQUENCE reunion_avis_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: reunion_avis_seq; Type: SEQUENCE OWNED BY; Schema: openaria; Owner: -
--

ALTER SEQUENCE reunion_avis_seq OWNED BY reunion_avis.reunion_avis;


--
-- Name: reunion_categorie; Type: TABLE; Schema: openaria; Owner: -
--

CREATE TABLE reunion_categorie (
    reunion_categorie integer NOT NULL,
    code character varying(20),
    libelle character varying(100),
    description text,
    service integer NOT NULL,
    om_validite_debut date,
    om_validite_fin date,
    ordre integer NOT NULL
);


--
-- Name: reunion_categorie_seq; Type: SEQUENCE; Schema: openaria; Owner: -
--

CREATE SEQUENCE reunion_categorie_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: reunion_categorie_seq; Type: SEQUENCE OWNED BY; Schema: openaria; Owner: -
--

ALTER SEQUENCE reunion_categorie_seq OWNED BY reunion_categorie.reunion_categorie;


--
-- Name: reunion_instance; Type: TABLE; Schema: openaria; Owner: -
--

CREATE TABLE reunion_instance (
    reunion_instance integer NOT NULL,
    code character varying(20),
    libelle character varying(100),
    description text,
    courriels text,
    service integer NOT NULL,
    om_validite_debut date,
    om_validite_fin date,
    ordre integer
);


--
-- Name: reunion_instance_membre; Type: TABLE; Schema: openaria; Owner: -
--

CREATE TABLE reunion_instance_membre (
    reunion_instance_membre integer NOT NULL,
    membre character varying(100),
    description text,
    reunion_instance integer NOT NULL,
    om_validite_debut date,
    om_validite_fin date
);


--
-- Name: reunion_instance_membre_seq; Type: SEQUENCE; Schema: openaria; Owner: -
--

CREATE SEQUENCE reunion_instance_membre_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: reunion_instance_membre_seq; Type: SEQUENCE OWNED BY; Schema: openaria; Owner: -
--

ALTER SEQUENCE reunion_instance_membre_seq OWNED BY reunion_instance_membre.reunion_instance_membre;


--
-- Name: reunion_instance_seq; Type: SEQUENCE; Schema: openaria; Owner: -
--

CREATE SEQUENCE reunion_instance_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: reunion_instance_seq; Type: SEQUENCE OWNED BY; Schema: openaria; Owner: -
--

ALTER SEQUENCE reunion_instance_seq OWNED BY reunion_instance.reunion_instance;


--
-- Name: reunion_seq; Type: SEQUENCE; Schema: openaria; Owner: -
--

CREATE SEQUENCE reunion_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: reunion_seq; Type: SEQUENCE OWNED BY; Schema: openaria; Owner: -
--

ALTER SEQUENCE reunion_seq OWNED BY reunion.reunion;


--
-- Name: reunion_type; Type: TABLE; Schema: openaria; Owner: -
--

CREATE TABLE reunion_type (
    reunion_type integer NOT NULL,
    code character varying(10) NOT NULL,
    libelle character varying(100) NOT NULL,
    lieu_adresse_ligne1 character varying(150),
    lieu_adresse_ligne2 character varying(100),
    lieu_salle character varying(100),
    heure character varying(5),
    listes_de_diffusion text,
    modele_courriel_convoquer text,
    modele_courriel_cloturer text,
    commission boolean,
    president integer,
    modele_ordre_du_jour integer,
    modele_compte_rendu_global integer,
    modele_compte_rendu_specifique integer,
    modele_feuille_presence integer,
    service integer NOT NULL,
    om_validite_debut date,
    om_validite_fin date,
    participants text,
    numerotation_mode character varying(30)
);


--
-- Name: TABLE reunion_type; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON TABLE reunion_type IS 'Les types de réunion';


--
-- Name: COLUMN reunion_type.reunion_type; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN reunion_type.reunion_type IS 'Identifiant unique';


--
-- Name: COLUMN reunion_type.code; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN reunion_type.code IS 'Code du type de réunion';


--
-- Name: COLUMN reunion_type.libelle; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN reunion_type.libelle IS 'Libellé du type de réunion';


--
-- Name: COLUMN reunion_type.lieu_adresse_ligne1; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN reunion_type.lieu_adresse_ligne1 IS 'Adresse des réunions de ce type';


--
-- Name: COLUMN reunion_type.lieu_adresse_ligne2; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN reunion_type.lieu_adresse_ligne2 IS 'Complément d''adresse des réunions de ce type';


--
-- Name: COLUMN reunion_type.lieu_salle; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN reunion_type.lieu_salle IS 'Salle où ont lieu les réunions de ce type';


--
-- Name: COLUMN reunion_type.listes_de_diffusion; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN reunion_type.listes_de_diffusion IS 'Liste de diffusion des réunions de ce type';


--
-- Name: COLUMN reunion_type.om_validite_debut; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN reunion_type.om_validite_debut IS 'Date de début de validité de ce type de réunion';


--
-- Name: COLUMN reunion_type.om_validite_fin; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN reunion_type.om_validite_fin IS 'Date de fin de validité de ce type de réunion';


--
-- Name: reunion_type_reunion_avis; Type: TABLE; Schema: openaria; Owner: -
--

CREATE TABLE reunion_type_reunion_avis (
    reunion_type_reunion_avis integer NOT NULL,
    reunion_type integer,
    reunion_avis integer
);


--
-- Name: reunion_type_reunion_avis_seq; Type: SEQUENCE; Schema: openaria; Owner: -
--

CREATE SEQUENCE reunion_type_reunion_avis_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: reunion_type_reunion_avis_seq; Type: SEQUENCE OWNED BY; Schema: openaria; Owner: -
--

ALTER SEQUENCE reunion_type_reunion_avis_seq OWNED BY reunion_type_reunion_avis.reunion_type_reunion_avis;


--
-- Name: reunion_type_reunion_categorie; Type: TABLE; Schema: openaria; Owner: -
--

CREATE TABLE reunion_type_reunion_categorie (
    reunion_type_reunion_categorie integer NOT NULL,
    reunion_type integer,
    reunion_categorie integer
);


--
-- Name: reunion_type_reunion_categorie_seq; Type: SEQUENCE; Schema: openaria; Owner: -
--

CREATE SEQUENCE reunion_type_reunion_categorie_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: reunion_type_reunion_categorie_seq; Type: SEQUENCE OWNED BY; Schema: openaria; Owner: -
--

ALTER SEQUENCE reunion_type_reunion_categorie_seq OWNED BY reunion_type_reunion_categorie.reunion_type_reunion_categorie;


--
-- Name: reunion_type_reunion_instance; Type: TABLE; Schema: openaria; Owner: -
--

CREATE TABLE reunion_type_reunion_instance (
    reunion_type_reunion_instance integer NOT NULL,
    reunion_type integer,
    reunion_instance integer
);


--
-- Name: reunion_type_reunion_instance_seq; Type: SEQUENCE; Schema: openaria; Owner: -
--

CREATE SEQUENCE reunion_type_reunion_instance_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: reunion_type_reunion_instance_seq; Type: SEQUENCE OWNED BY; Schema: openaria; Owner: -
--

ALTER SEQUENCE reunion_type_reunion_instance_seq OWNED BY reunion_type_reunion_instance.reunion_type_reunion_instance;


--
-- Name: reunion_type_seq; Type: SEQUENCE; Schema: openaria; Owner: -
--

CREATE SEQUENCE reunion_type_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: reunion_type_seq; Type: SEQUENCE OWNED BY; Schema: openaria; Owner: -
--

ALTER SEQUENCE reunion_type_seq OWNED BY reunion_type.reunion_type;


--
-- Name: service; Type: TABLE; Schema: openaria; Owner: -
--

CREATE TABLE service (
    service integer NOT NULL,
    code character varying(20),
    libelle character varying(100),
    description character varying(250),
    om_validite_debut date,
    om_validite_fin date
);


--
-- Name: TABLE service; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON TABLE service IS 'Liste des différents service intervenant dans l''application';


--
-- Name: COLUMN service.service; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN service.service IS 'Identifiant unique numérique';


--
-- Name: COLUMN service.code; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN service.code IS 'Code abrégé du service, utilisable pour les affichages synthétiques';


--
-- Name: COLUMN service.libelle; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN service.libelle IS 'Nom long du service';


--
-- Name: COLUMN service.description; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN service.description IS 'Description du service';


--
-- Name: COLUMN service.om_validite_debut; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN service.om_validite_debut IS 'Date de mise en activité de ce paramétrage';


--
-- Name: COLUMN service.om_validite_fin; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN service.om_validite_fin IS 'Date de fin de validité de ce paramétrage';


--
-- Name: service_seq; Type: SEQUENCE; Schema: openaria; Owner: -
--

CREATE SEQUENCE service_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: service_seq; Type: SEQUENCE OWNED BY; Schema: openaria; Owner: -
--

ALTER SEQUENCE service_seq OWNED BY service.service;


--
-- Name: signataire; Type: TABLE; Schema: openaria; Owner: -
--

CREATE TABLE signataire (
    signataire integer NOT NULL,
    nom character varying(100),
    prenom character varying(100),
    civilite integer,
    signataire_qualite integer,
    signature text,
    defaut boolean DEFAULT false,
    om_validite_debut date,
    om_validite_fin date
);


--
-- Name: TABLE signataire; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON TABLE signataire IS 'Liste des signataires de courriers sortants (documents générés).';


--
-- Name: COLUMN signataire.signataire; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN signataire.signataire IS 'Identifiant unique.';


--
-- Name: COLUMN signataire.nom; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN signataire.nom IS 'Nom du signataire.';


--
-- Name: COLUMN signataire.prenom; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN signataire.prenom IS 'Prénom du signataire.';


--
-- Name: COLUMN signataire.civilite; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN signataire.civilite IS 'Civilité du signataire.';


--
-- Name: COLUMN signataire.signataire_qualite; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN signataire.signataire_qualite IS 'Qualité du signataire.';


--
-- Name: COLUMN signataire.signature; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN signataire.signature IS 'Signature du signataire.';


--
-- Name: COLUMN signataire.defaut; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN signataire.defaut IS 'Signataire par défaut.';


--
-- Name: COLUMN signataire.om_validite_debut; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN signataire.om_validite_debut IS 'Date de début de validité du signataire.';


--
-- Name: COLUMN signataire.om_validite_fin; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN signataire.om_validite_fin IS 'Date de fin de validité du signataire.';


--
-- Name: signataire_qualite; Type: TABLE; Schema: openaria; Owner: -
--

CREATE TABLE signataire_qualite (
    signataire_qualite integer NOT NULL,
    code character varying(20),
    libelle character varying(100),
    description text,
    om_validite_debut date,
    om_validite_fin date
);


--
-- Name: TABLE signataire_qualite; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON TABLE signataire_qualite IS 'Qualité des signataires.';


--
-- Name: COLUMN signataire_qualite.signataire_qualite; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN signataire_qualite.signataire_qualite IS 'Identifiant unique.';


--
-- Name: COLUMN signataire_qualite.code; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN signataire_qualite.code IS 'Code de la qualité de signataire.';


--
-- Name: COLUMN signataire_qualite.libelle; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN signataire_qualite.libelle IS 'Libellé de la qualité de signataire.';


--
-- Name: COLUMN signataire_qualite.description; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN signataire_qualite.description IS 'Description de la qualité de signataire.';


--
-- Name: COLUMN signataire_qualite.om_validite_debut; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN signataire_qualite.om_validite_debut IS 'Date de début de validité de la qualité de signataire.';


--
-- Name: COLUMN signataire_qualite.om_validite_fin; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN signataire_qualite.om_validite_fin IS 'Date de fin de validité de la qualité de signataire.';


--
-- Name: signataire_qualite_seq; Type: SEQUENCE; Schema: openaria; Owner: -
--

CREATE SEQUENCE signataire_qualite_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: signataire_qualite_seq; Type: SEQUENCE OWNED BY; Schema: openaria; Owner: -
--

ALTER SEQUENCE signataire_qualite_seq OWNED BY signataire_qualite.signataire_qualite;


--
-- Name: signataire_seq; Type: SEQUENCE; Schema: openaria; Owner: -
--

CREATE SEQUENCE signataire_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: signataire_seq; Type: SEQUENCE OWNED BY; Schema: openaria; Owner: -
--

ALTER SEQUENCE signataire_seq OWNED BY signataire.signataire;


--
-- Name: technicien_arrondissement; Type: TABLE; Schema: openaria; Owner: -
--

CREATE TABLE technicien_arrondissement (
    technicien_arrondissement integer NOT NULL,
    service integer NOT NULL,
    technicien integer NOT NULL,
    arrondissement integer NOT NULL
);


--
-- Name: TABLE technicien_arrondissement; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON TABLE technicien_arrondissement IS 'Liens entre les techniciens et les arrondissement, utile lors de l''affectation automatique des techniciens du service d''accessibilité au dossier d''instruction';


--
-- Name: COLUMN technicien_arrondissement.technicien_arrondissement; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN technicien_arrondissement.technicien_arrondissement IS 'Identifiant unique.';


--
-- Name: COLUMN technicien_arrondissement.technicien; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN technicien_arrondissement.technicien IS 'Technicien de la table acteur.';


--
-- Name: COLUMN technicien_arrondissement.arrondissement; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN technicien_arrondissement.arrondissement IS 'Arrondissement dont le technicien est en charge.';


--
-- Name: technicien_arrondissement_seq; Type: SEQUENCE; Schema: openaria; Owner: -
--

CREATE SEQUENCE technicien_arrondissement_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: technicien_arrondissement_seq; Type: SEQUENCE OWNED BY; Schema: openaria; Owner: -
--

ALTER SEQUENCE technicien_arrondissement_seq OWNED BY technicien_arrondissement.technicien_arrondissement;


--
-- Name: visite; Type: TABLE; Schema: openaria; Owner: -
--

CREATE TABLE visite (
    visite integer NOT NULL,
    visite_motif_annulation integer,
    visite_etat integer NOT NULL,
    dossier_instruction integer NOT NULL,
    acteur integer NOT NULL,
    programmation integer,
    date_creation date NOT NULL,
    date_annulation date,
    observation text,
    programmation_version_creation integer,
    programmation_version_annulation integer,
    heure_debut character varying(5) NOT NULL,
    heure_fin character varying(5) NOT NULL,
    convocation_exploitants character varying(20),
    date_visite date NOT NULL,
    a_poursuivre boolean DEFAULT false,
    courrier_convocation_exploitants integer,
    courrier_annulation integer,
    programmation_version_modification integer
);


--
-- Name: COLUMN visite.date_visite; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN visite.date_visite IS 'Jour programmé de la visite';


--
-- Name: visite_duree; Type: TABLE; Schema: openaria; Owner: -
--

CREATE TABLE visite_duree (
    visite_duree integer NOT NULL,
    libelle character varying(20),
    duree real
);


--
-- Name: TABLE visite_duree; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON TABLE visite_duree IS 'Liste des durées de visites.';


--
-- Name: COLUMN visite_duree.visite_duree; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN visite_duree.visite_duree IS 'Identifiant unique de durée de visite.';


--
-- Name: COLUMN visite_duree.libelle; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN visite_duree.libelle IS 'Texte affiché pour la sélection de la durée en jours.';


--
-- Name: COLUMN visite_duree.duree; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN visite_duree.duree IS 'Durée en nombre de jours.';


--
-- Name: visite_duree_seq; Type: SEQUENCE; Schema: openaria; Owner: -
--

CREATE SEQUENCE visite_duree_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: visite_duree_seq; Type: SEQUENCE OWNED BY; Schema: openaria; Owner: -
--

ALTER SEQUENCE visite_duree_seq OWNED BY visite_duree.visite_duree;


--
-- Name: visite_etat; Type: TABLE; Schema: openaria; Owner: -
--

CREATE TABLE visite_etat (
    visite_etat integer NOT NULL,
    code character varying(20),
    libelle character varying(100),
    description character varying(250),
    om_validite_debut date,
    om_validite_fin date
);


--
-- Name: visite_etat_seq; Type: SEQUENCE; Schema: openaria; Owner: -
--

CREATE SEQUENCE visite_etat_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: visite_etat_seq; Type: SEQUENCE OWNED BY; Schema: openaria; Owner: -
--

ALTER SEQUENCE visite_etat_seq OWNED BY visite_etat.visite_etat;


--
-- Name: visite_motif_annulation; Type: TABLE; Schema: openaria; Owner: -
--

CREATE TABLE visite_motif_annulation (
    visite_motif_annulation integer NOT NULL,
    code character varying(20),
    libelle character varying(100),
    description character varying(250),
    om_validite_debut date,
    om_validite_fin date
);


--
-- Name: visite_motif_annulation_seq; Type: SEQUENCE; Schema: openaria; Owner: -
--

CREATE SEQUENCE visite_motif_annulation_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: visite_motif_annulation_seq; Type: SEQUENCE OWNED BY; Schema: openaria; Owner: -
--

ALTER SEQUENCE visite_motif_annulation_seq OWNED BY visite_motif_annulation.visite_motif_annulation;


--
-- Name: visite_seq; Type: SEQUENCE; Schema: openaria; Owner: -
--

CREATE SEQUENCE visite_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: visite_seq; Type: SEQUENCE OWNED BY; Schema: openaria; Owner: -
--

ALTER SEQUENCE visite_seq OWNED BY visite.visite;


--
-- Name: voie; Type: TABLE; Schema: openaria; Owner: -
--

CREATE TABLE voie (
    voie integer NOT NULL,
    libelle character varying(38) NOT NULL,
    rivoli character varying(10),
    om_validite_debut date,
    om_validite_fin date,
    om_collectivite integer NOT NULL,
    id_voie_ref text
);


--
-- Name: TABLE voie; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON TABLE voie IS 'Table de référence des voies.';


--
-- Name: COLUMN voie.voie; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN voie.voie IS 'Identifiant unique';


--
-- Name: COLUMN voie.libelle; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN voie.libelle IS 'libellé de l''adresse (exemple : rue du pont)';


--
-- Name: COLUMN voie.rivoli; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN voie.rivoli IS 'code rivoli de la voie';


--
-- Name: COLUMN voie.om_validite_debut; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN voie.om_validite_debut IS 'Date de début de validité';


--
-- Name: COLUMN voie.om_validite_fin; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN voie.om_validite_fin IS 'Date de fin de validité';


--
-- Name: COLUMN voie.om_collectivite; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN voie.om_collectivite IS 'identifiant de collectivité';


--
-- Name: COLUMN voie.id_voie_ref; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN voie.id_voie_ref IS 'Id de la voie de le référentiel';


--
-- Name: voie_arrondissement; Type: TABLE; Schema: openaria; Owner: -
--

CREATE TABLE voie_arrondissement (
    voie_arrondissement integer NOT NULL,
    voie integer,
    arrondissement integer
);


--
-- Name: TABLE voie_arrondissement; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON TABLE voie_arrondissement IS 'table permettant de rattacher une voie à un ou plusieurs arrondissements';


--
-- Name: COLUMN voie_arrondissement.voie_arrondissement; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN voie_arrondissement.voie_arrondissement IS 'identifiant unique de la liaison';


--
-- Name: COLUMN voie_arrondissement.voie; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN voie_arrondissement.voie IS 'identifiant unique de la voie';


--
-- Name: COLUMN voie_arrondissement.arrondissement; Type: COMMENT; Schema: openaria; Owner: -
--

COMMENT ON COLUMN voie_arrondissement.arrondissement IS 'identifiant unique de l''arrondissement';


--
-- Name: voie_arrondissement_seq; Type: SEQUENCE; Schema: openaria; Owner: -
--

CREATE SEQUENCE voie_arrondissement_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: voie_arrondissement_seq; Type: SEQUENCE OWNED BY; Schema: openaria; Owner: -
--

ALTER SEQUENCE voie_arrondissement_seq OWNED BY voie_arrondissement.voie_arrondissement;


--
-- Name: voie_seq; Type: SEQUENCE; Schema: openaria; Owner: -
--

CREATE SEQUENCE voie_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: voie_seq; Type: SEQUENCE OWNED BY; Schema: openaria; Owner: -
--

ALTER SEQUENCE voie_seq OWNED BY voie.voie;


--
-- Name: acteur_conge acteur_conge_pkey; Type: CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY acteur_conge
    ADD CONSTRAINT acteur_conge_pkey PRIMARY KEY (acteur_conge);


--
-- Name: acteur acteur_pk; Type: CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY acteur
    ADD CONSTRAINT acteur_pk PRIMARY KEY (acteur);


--
-- Name: acteur_plage_visite acteur_plage_visite_pkey; Type: CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY acteur_plage_visite
    ADD CONSTRAINT acteur_plage_visite_pkey PRIMARY KEY (acteur_plage_visite);


--
-- Name: analyses analyses_dossier_instruction; Type: CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY analyses
    ADD CONSTRAINT analyses_dossier_instruction UNIQUE (dossier_instruction);


--
-- Name: analyses analyses_pkey; Type: CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY analyses
    ADD CONSTRAINT analyses_pkey PRIMARY KEY (analyses);


--
-- Name: analyses_type analyses_type_pkey; Type: CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY analyses_type
    ADD CONSTRAINT analyses_type_pkey PRIMARY KEY (analyses_type);


--
-- Name: arrondissement arrondissement_pkey; Type: CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY arrondissement
    ADD CONSTRAINT arrondissement_pkey PRIMARY KEY (arrondissement);


--
-- Name: autorite_competente autorite_competente_pkey; Type: CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY autorite_competente
    ADD CONSTRAINT autorite_competente_pkey PRIMARY KEY (autorite_competente);


--
-- Name: autorite_police_decision autorite_police_decision_pkey; Type: CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY autorite_police_decision
    ADD CONSTRAINT autorite_police_decision_pkey PRIMARY KEY (autorite_police_decision);


--
-- Name: autorite_police_motif autorite_police_motif_pkey; Type: CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY autorite_police_motif
    ADD CONSTRAINT autorite_police_motif_pkey PRIMARY KEY (autorite_police_motif);


--
-- Name: autorite_police autorite_police_pkey; Type: CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY autorite_police
    ADD CONSTRAINT autorite_police_pkey PRIMARY KEY (autorite_police);


--
-- Name: contact_civilite contact_civilite_pk; Type: CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY contact_civilite
    ADD CONSTRAINT contact_civilite_pk PRIMARY KEY (contact_civilite);


--
-- Name: contact contact_pkey; Type: CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY contact
    ADD CONSTRAINT contact_pkey PRIMARY KEY (contact);


--
-- Name: contact_type contact_type_pk; Type: CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY contact_type
    ADD CONSTRAINT contact_type_pk PRIMARY KEY (contact_type);


--
-- Name: contrainte contrainte_pkey; Type: CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY contrainte
    ADD CONSTRAINT contrainte_pkey PRIMARY KEY (contrainte);


--
-- Name: courrier courrier_pkey; Type: CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY courrier
    ADD CONSTRAINT courrier_pkey PRIMARY KEY (courrier);


--
-- Name: courrier_texte_type courrier_texte_type_pkey; Type: CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY courrier_texte_type
    ADD CONSTRAINT courrier_texte_type_pkey PRIMARY KEY (courrier_texte_type);


--
-- Name: courrier_type_categorie courrier_type_categorie_pkey; Type: CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY courrier_type_categorie
    ADD CONSTRAINT courrier_type_categorie_pkey PRIMARY KEY (courrier_type_categorie);


--
-- Name: courrier_type courrier_type_pkey; Type: CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY courrier_type
    ADD CONSTRAINT courrier_type_pkey PRIMARY KEY (courrier_type);


--
-- Name: derogation_scda derogation_scda_pkey; Type: CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY derogation_scda
    ADD CONSTRAINT derogation_scda_pkey PRIMARY KEY (derogation_scda);


--
-- Name: document_presente document_presente_pkey; Type: CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY document_presente
    ADD CONSTRAINT document_presente_pkey PRIMARY KEY (document_presente);


--
-- Name: dossier_coordination dossier_coordination_libelle_unique; Type: CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY dossier_coordination
    ADD CONSTRAINT dossier_coordination_libelle_unique UNIQUE (libelle);


--
-- Name: dossier_coordination_message dossier_coordination_message_pkey; Type: CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY dossier_coordination_message
    ADD CONSTRAINT dossier_coordination_message_pkey PRIMARY KEY (dossier_coordination_message);


--
-- Name: dossier_coordination_parcelle dossier_coordination_parcelle_pkey; Type: CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY dossier_coordination_parcelle
    ADD CONSTRAINT dossier_coordination_parcelle_pkey PRIMARY KEY (dossier_coordination_parcelle);


--
-- Name: dossier_coordination_type dossier_forme_pkey; Type: CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY dossier_coordination_type
    ADD CONSTRAINT dossier_forme_pkey PRIMARY KEY (dossier_coordination_type);


--
-- Name: dossier_instruction dossier_instruction_libelle_unique; Type: CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY dossier_instruction
    ADD CONSTRAINT dossier_instruction_libelle_unique UNIQUE (libelle);


--
-- Name: dossier_instruction dossier_instruction_pkey; Type: CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY dossier_instruction
    ADD CONSTRAINT dossier_instruction_pkey PRIMARY KEY (dossier_instruction);


--
-- Name: dossier_instruction_reunion dossier_instruction_reunion_pkey; Type: CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY dossier_instruction_reunion
    ADD CONSTRAINT dossier_instruction_reunion_pkey PRIMARY KEY (dossier_instruction_reunion);


--
-- Name: dossier_instruction_reunion dossier_instruction_reunion_reunion_ordre_uk; Type: CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY dossier_instruction_reunion
    ADD CONSTRAINT dossier_instruction_reunion_reunion_ordre_uk UNIQUE (reunion, ordre);


--
-- Name: dossier_coordination dossier_pkey; Type: CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY dossier_coordination
    ADD CONSTRAINT dossier_pkey PRIMARY KEY (dossier_coordination);


--
-- Name: dossier_type dossier_type_pkey; Type: CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY dossier_type
    ADD CONSTRAINT dossier_type_pkey PRIMARY KEY (dossier_type);


--
-- Name: essai_realise essai_realise_pkey; Type: CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY essai_realise
    ADD CONSTRAINT essai_realise_pkey PRIMARY KEY (essai_realise);


--
-- Name: etablissement_categorie etablissement_categorie_pkey; Type: CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY etablissement_categorie
    ADD CONSTRAINT etablissement_categorie_pkey PRIMARY KEY (etablissement_categorie);


--
-- Name: etablissement etablissement_code_unique_key; Type: CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY etablissement
    ADD CONSTRAINT etablissement_code_unique_key UNIQUE (code);


--
-- Name: etablissement_nature etablissement_nature_pkey; Type: CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY etablissement_nature
    ADD CONSTRAINT etablissement_nature_pkey PRIMARY KEY (etablissement_nature);


--
-- Name: etablissement_parcelle etablissement_parcelle_pk; Type: CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY etablissement_parcelle
    ADD CONSTRAINT etablissement_parcelle_pk PRIMARY KEY (etablissement_parcelle);


--
-- Name: periodicite_visites etablissement_periodicite_pk; Type: CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY periodicite_visites
    ADD CONSTRAINT etablissement_periodicite_pk PRIMARY KEY (periodicite_visites);


--
-- Name: etablissement etablissement_pkey; Type: CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY etablissement
    ADD CONSTRAINT etablissement_pkey PRIMARY KEY (etablissement);


--
-- Name: etablissement_statut_juridique etablissement_statut_juridique_pkey; Type: CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY etablissement_statut_juridique
    ADD CONSTRAINT etablissement_statut_juridique_pkey PRIMARY KEY (etablissement_statut_juridique);


--
-- Name: etablissement_etat etablissement_statut_pkey; Type: CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY etablissement_etat
    ADD CONSTRAINT etablissement_statut_pkey PRIMARY KEY (etablissement_etat);


--
-- Name: etablissement_tutelle_adm etablissement_tutelle_adm_etablissement_tutelle_adm; Type: CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY etablissement_tutelle_adm
    ADD CONSTRAINT etablissement_tutelle_adm_etablissement_tutelle_adm PRIMARY KEY (etablissement_tutelle_adm);


--
-- Name: etablissement_type etablissement_type_pkey; Type: CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY etablissement_type
    ADD CONSTRAINT etablissement_type_pkey PRIMARY KEY (etablissement_type);


--
-- Name: lien_etablissement_e_type etablissement_type_secondaire_pk; Type: CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY lien_etablissement_e_type
    ADD CONSTRAINT etablissement_type_secondaire_pk PRIMARY KEY (lien_etablissement_e_type);


--
-- Name: etablissement_unite etablissement_unite_pk; Type: CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY etablissement_unite
    ADD CONSTRAINT etablissement_unite_pk PRIMARY KEY (etablissement_unite);


--
-- Name: lien_dossier_coordination_etablissement_type lien_analyses_etablissement_type_pkey; Type: CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY lien_dossier_coordination_etablissement_type
    ADD CONSTRAINT lien_analyses_etablissement_type_pkey PRIMARY KEY (lien_dossier_coordination_etablissement_type);


--
-- Name: lien_autorite_police_courrier lien_autorite_police_courrier_pkey; Type: CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY lien_autorite_police_courrier
    ADD CONSTRAINT lien_autorite_police_courrier_pkey PRIMARY KEY (lien_autorite_police_courrier);


--
-- Name: lien_contrainte_dossier_coordination lien_contrainte_dossier_coordination_pkey; Type: CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY lien_contrainte_dossier_coordination
    ADD CONSTRAINT lien_contrainte_dossier_coordination_pkey PRIMARY KEY (lien_contrainte_dossier_coordination);


--
-- Name: lien_contrainte_etablissement lien_contrainte_etablissement_pkey; Type: CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY lien_contrainte_etablissement
    ADD CONSTRAINT lien_contrainte_etablissement_pkey PRIMARY KEY (lien_contrainte_etablissement);


--
-- Name: lien_courrier_contact lien_courrier_contact_pkey; Type: CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY lien_courrier_contact
    ADD CONSTRAINT lien_courrier_contact_pkey PRIMARY KEY (lien_courrier_contact);


--
-- Name: lien_dossier_coordination_contact lien_dossier_coordination_contact_pkey; Type: CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY lien_dossier_coordination_contact
    ADD CONSTRAINT lien_dossier_coordination_contact_pkey PRIMARY KEY (lien_dossier_coordination_contact);


--
-- Name: lien_dossier_coordination_type_analyses_type lien_dossier_coordination_type_analyses_type_pkey; Type: CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY lien_dossier_coordination_type_analyses_type
    ADD CONSTRAINT lien_dossier_coordination_type_analyses_type_pkey PRIMARY KEY (lien_dossier_coordination_type_analyses_type);


--
-- Name: lien_essai_realise_analyses lien_essai_realise_analyses_pkey; Type: CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY lien_essai_realise_analyses
    ADD CONSTRAINT lien_essai_realise_analyses_pkey PRIMARY KEY (lien_essai_realise_analyses);


--
-- Name: lien_prescription_reglementaire_etablissement_categorie lien_prescription_reglementaire_etablissement_categorie_pkey; Type: CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY lien_prescription_reglementaire_etablissement_categorie
    ADD CONSTRAINT lien_prescription_reglementaire_etablissement_categorie_pkey PRIMARY KEY (lien_prescription_reglementaire_etablissement_categorie);


--
-- Name: lien_prescription_reglementaire_etablissement_type lien_prescription_reglementaire_etablissement_type_pkey; Type: CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY lien_prescription_reglementaire_etablissement_type
    ADD CONSTRAINT lien_prescription_reglementaire_etablissement_type_pkey PRIMARY KEY (lien_prescription_reglementaire_etablissement_type);


--
-- Name: lien_reglementation_applicable_etablissement_categorie lien_reglementation_applicable_etablissement_categorie_pkey; Type: CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY lien_reglementation_applicable_etablissement_categorie
    ADD CONSTRAINT lien_reglementation_applicable_etablissement_categorie_pkey PRIMARY KEY (lien_reglementation_applicable_etablissement_categorie);


--
-- Name: lien_reglementation_applicable_etablissement_type lien_reglementation_applicable_etablissement_type_pkey; Type: CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY lien_reglementation_applicable_etablissement_type
    ADD CONSTRAINT lien_reglementation_applicable_etablissement_type_pkey PRIMARY KEY (lien_reglementation_applicable_etablissement_type);


--
-- Name: lien_reunion_r_instance_r_i_membre lien_reunion_r_instance_r_i_membre_pkey; Type: CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY lien_reunion_r_instance_r_i_membre
    ADD CONSTRAINT lien_reunion_r_instance_r_i_membre_pkey PRIMARY KEY (lien_reunion_r_instance_r_i_membre);


--
-- Name: modele_edition modele_edition_code_key; Type: CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY modele_edition
    ADD CONSTRAINT modele_edition_code_key UNIQUE (code);


--
-- Name: modele_edition modele_edition_pkey; Type: CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY modele_edition
    ADD CONSTRAINT modele_edition_pkey PRIMARY KEY (modele_edition);


--
-- Name: piece piece_pk; Type: CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY piece
    ADD CONSTRAINT piece_pk PRIMARY KEY (piece);


--
-- Name: piece_statut piece_statut_pkey; Type: CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY piece_statut
    ADD CONSTRAINT piece_statut_pkey PRIMARY KEY (piece_statut);


--
-- Name: piece_type piece_type_pk; Type: CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY piece_type
    ADD CONSTRAINT piece_type_pk PRIMARY KEY (piece_type);


--
-- Name: prescription prescription_pkey; Type: CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY prescription
    ADD CONSTRAINT prescription_pkey PRIMARY KEY (prescription);


--
-- Name: prescription_reglementaire prescription_reglementaire_pkey; Type: CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY prescription_reglementaire
    ADD CONSTRAINT prescription_reglementaire_pkey PRIMARY KEY (prescription_reglementaire);


--
-- Name: prescription_specifique prescription_specifique_pkey; Type: CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY prescription_specifique
    ADD CONSTRAINT prescription_specifique_pkey PRIMARY KEY (prescription_specifique);


--
-- Name: proces_verbal proces_verbal_pkey; Type: CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY proces_verbal
    ADD CONSTRAINT proces_verbal_pkey PRIMARY KEY (proces_verbal);


--
-- Name: programmation programmation_annee_numero_semaine_service; Type: CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY programmation
    ADD CONSTRAINT programmation_annee_numero_semaine_service UNIQUE (annee, numero_semaine, service);


--
-- Name: programmation_etat programmation_etat_pkey; Type: CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY programmation_etat
    ADD CONSTRAINT programmation_etat_pkey PRIMARY KEY (programmation_etat);


--
-- Name: programmation programmation_pkey; Type: CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY programmation
    ADD CONSTRAINT programmation_pkey PRIMARY KEY (programmation);


--
-- Name: quartier quartier_pkey; Type: CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY quartier
    ADD CONSTRAINT quartier_pkey PRIMARY KEY (quartier);


--
-- Name: reglementation_applicable reglementation_applicable_pkey; Type: CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY reglementation_applicable
    ADD CONSTRAINT reglementation_applicable_pkey PRIMARY KEY (reglementation_applicable);


--
-- Name: reunion_avis reunion_avis_pkey; Type: CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY reunion_avis
    ADD CONSTRAINT reunion_avis_pkey PRIMARY KEY (reunion_avis);


--
-- Name: reunion_categorie reunion_categorie_pkey; Type: CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY reunion_categorie
    ADD CONSTRAINT reunion_categorie_pkey PRIMARY KEY (reunion_categorie);


--
-- Name: reunion_instance_membre reunion_instance_membre_pkey; Type: CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY reunion_instance_membre
    ADD CONSTRAINT reunion_instance_membre_pkey PRIMARY KEY (reunion_instance_membre);


--
-- Name: reunion_instance reunion_instance_pkey; Type: CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY reunion_instance
    ADD CONSTRAINT reunion_instance_pkey PRIMARY KEY (reunion_instance);


--
-- Name: reunion reunion_pkey; Type: CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY reunion
    ADD CONSTRAINT reunion_pkey PRIMARY KEY (reunion);


--
-- Name: reunion_type reunion_type_pkey; Type: CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY reunion_type
    ADD CONSTRAINT reunion_type_pkey PRIMARY KEY (reunion_type);


--
-- Name: reunion_type_reunion_avis reunion_type_reunion_avis_pkey; Type: CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY reunion_type_reunion_avis
    ADD CONSTRAINT reunion_type_reunion_avis_pkey PRIMARY KEY (reunion_type_reunion_avis);


--
-- Name: reunion_type_reunion_categorie reunion_type_reunion_categorie_pkey; Type: CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY reunion_type_reunion_categorie
    ADD CONSTRAINT reunion_type_reunion_categorie_pkey PRIMARY KEY (reunion_type_reunion_categorie);


--
-- Name: reunion_type_reunion_instance reunion_type_reunion_instance_pkey; Type: CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY reunion_type_reunion_instance
    ADD CONSTRAINT reunion_type_reunion_instance_pkey PRIMARY KEY (reunion_type_reunion_instance);


--
-- Name: service service_pkey; Type: CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY service
    ADD CONSTRAINT service_pkey PRIMARY KEY (service);


--
-- Name: signataire signataire_pkey; Type: CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY signataire
    ADD CONSTRAINT signataire_pkey PRIMARY KEY (signataire);


--
-- Name: signataire_qualite signataire_qualite_pkey; Type: CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY signataire_qualite
    ADD CONSTRAINT signataire_qualite_pkey PRIMARY KEY (signataire_qualite);


--
-- Name: technicien_arrondissement technicien_arrondissement_arrondissement_service_unique; Type: CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY technicien_arrondissement
    ADD CONSTRAINT technicien_arrondissement_arrondissement_service_unique UNIQUE (arrondissement, service);


--
-- Name: technicien_arrondissement technicien_arrondissement_pkey; Type: CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY technicien_arrondissement
    ADD CONSTRAINT technicien_arrondissement_pkey PRIMARY KEY (technicien_arrondissement);


--
-- Name: lien_reglementation_applicable_etablissement_categorie unique_ra_ec; Type: CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY lien_reglementation_applicable_etablissement_categorie
    ADD CONSTRAINT unique_ra_ec UNIQUE (reglementation_applicable, etablissement_categorie);


--
-- Name: lien_reglementation_applicable_etablissement_type unique_ra_et; Type: CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY lien_reglementation_applicable_etablissement_type
    ADD CONSTRAINT unique_ra_et UNIQUE (reglementation_applicable, etablissement_type);


--
-- Name: visite_duree visite_duree_pk; Type: CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY visite_duree
    ADD CONSTRAINT visite_duree_pk PRIMARY KEY (visite_duree);


--
-- Name: visite_etat visite_etat_pkey; Type: CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY visite_etat
    ADD CONSTRAINT visite_etat_pkey PRIMARY KEY (visite_etat);


--
-- Name: visite_motif_annulation visite_motif_annulation_pkey; Type: CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY visite_motif_annulation
    ADD CONSTRAINT visite_motif_annulation_pkey PRIMARY KEY (visite_motif_annulation);


--
-- Name: visite visite_pkey; Type: CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY visite
    ADD CONSTRAINT visite_pkey PRIMARY KEY (visite);


--
-- Name: voie_arrondissement voie_arrondissement_pk; Type: CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY voie_arrondissement
    ADD CONSTRAINT voie_arrondissement_pk PRIMARY KEY (voie_arrondissement);


--
-- Name: voie_arrondissement voie_arrondissement_uniq; Type: CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY voie_arrondissement
    ADD CONSTRAINT voie_arrondissement_uniq UNIQUE (voie, arrondissement);


--
-- Name: voie voie_pk; Type: CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY voie
    ADD CONSTRAINT voie_pk PRIMARY KEY (voie);


--
-- Name: acteur_conge acteur_conge_acteur_fkey; Type: FK CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY acteur_conge
    ADD CONSTRAINT acteur_conge_acteur_fkey FOREIGN KEY (acteur) REFERENCES acteur(acteur);


--
-- Name: acteur acteur_om_utilisateur_fkey; Type: FK CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY acteur
    ADD CONSTRAINT acteur_om_utilisateur_fkey FOREIGN KEY (om_utilisateur) REFERENCES om_utilisateur(om_utilisateur);


--
-- Name: acteur_plage_visite acteur_plage_visite_acteur_fkey; Type: FK CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY acteur_plage_visite
    ADD CONSTRAINT acteur_plage_visite_acteur_fkey FOREIGN KEY (acteur) REFERENCES acteur(acteur);


--
-- Name: acteur acteur_service_fkey; Type: FK CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY acteur
    ADD CONSTRAINT acteur_service_fkey FOREIGN KEY (service) REFERENCES service(service);


--
-- Name: analyses analyses_analyses_type_fkey; Type: FK CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY analyses
    ADD CONSTRAINT analyses_analyses_type_fkey FOREIGN KEY (analyses_type) REFERENCES analyses_type(analyses_type);


--
-- Name: analyses analyses_dossier_instruction_fkey; Type: FK CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY analyses
    ADD CONSTRAINT analyses_dossier_instruction_fkey FOREIGN KEY (dossier_instruction) REFERENCES dossier_instruction(dossier_instruction);


--
-- Name: analyses analyses_modele_edition_compte_rendu_fkey; Type: FK CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY analyses
    ADD CONSTRAINT analyses_modele_edition_compte_rendu_fkey FOREIGN KEY (modele_edition_compte_rendu) REFERENCES modele_edition(modele_edition);


--
-- Name: analyses_type analyses_modele_edition_compte_rendu_fkey; Type: FK CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY analyses_type
    ADD CONSTRAINT analyses_modele_edition_compte_rendu_fkey FOREIGN KEY (modele_edition_compte_rendu) REFERENCES modele_edition(modele_edition);


--
-- Name: analyses analyses_modele_edition_proces_verbal_fkey; Type: FK CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY analyses
    ADD CONSTRAINT analyses_modele_edition_proces_verbal_fkey FOREIGN KEY (modele_edition_proces_verbal) REFERENCES modele_edition(modele_edition);


--
-- Name: analyses_type analyses_modele_edition_proces_verbal_fkey; Type: FK CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY analyses_type
    ADD CONSTRAINT analyses_modele_edition_proces_verbal_fkey FOREIGN KEY (modele_edition_proces_verbal) REFERENCES modele_edition(modele_edition);


--
-- Name: analyses analyses_modele_edition_rapport_fkey; Type: FK CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY analyses
    ADD CONSTRAINT analyses_modele_edition_rapport_fkey FOREIGN KEY (modele_edition_rapport) REFERENCES modele_edition(modele_edition);


--
-- Name: analyses_type analyses_modele_edition_rapport_fkey; Type: FK CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY analyses_type
    ADD CONSTRAINT analyses_modele_edition_rapport_fkey FOREIGN KEY (modele_edition_rapport) REFERENCES modele_edition(modele_edition);


--
-- Name: analyses analyses_proces_verbal_fkey; Type: FK CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY analyses
    ADD CONSTRAINT analyses_proces_verbal_fkey FOREIGN KEY (dernier_pv) REFERENCES proces_verbal(proces_verbal);


--
-- Name: analyses analyses_reunion_avis_fkey; Type: FK CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY analyses
    ADD CONSTRAINT analyses_reunion_avis_fkey FOREIGN KEY (reunion_avis) REFERENCES reunion_avis(reunion_avis);


--
-- Name: analyses analyses_service_fkey; Type: FK CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY analyses
    ADD CONSTRAINT analyses_service_fkey FOREIGN KEY (service) REFERENCES service(service);


--
-- Name: analyses_type analyses_type_service_fkey; Type: FK CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY analyses_type
    ADD CONSTRAINT analyses_type_service_fkey FOREIGN KEY (service) REFERENCES service(service);


--
-- Name: autorite_competente autorite_competente_service_fkey; Type: FK CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY autorite_competente
    ADD CONSTRAINT autorite_competente_service_fkey FOREIGN KEY (service) REFERENCES service(service);


--
-- Name: autorite_police autorite_police_autorite_police_decision_fkey; Type: FK CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY autorite_police
    ADD CONSTRAINT autorite_police_autorite_police_decision_fkey FOREIGN KEY (autorite_police_decision) REFERENCES autorite_police_decision(autorite_police_decision);


--
-- Name: autorite_police autorite_police_autorite_police_motif_fkey; Type: FK CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY autorite_police
    ADD CONSTRAINT autorite_police_autorite_police_motif_fkey FOREIGN KEY (autorite_police_motif) REFERENCES autorite_police_motif(autorite_police_motif);


--
-- Name: autorite_police_decision autorite_police_decision_etablissement_etat_fkey; Type: FK CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY autorite_police_decision
    ADD CONSTRAINT autorite_police_decision_etablissement_etat_fkey FOREIGN KEY (etablissement_etat) REFERENCES etablissement_etat(etablissement_etat);


--
-- Name: autorite_police_decision autorite_police_decision_service_fkey; Type: FK CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY autorite_police_decision
    ADD CONSTRAINT autorite_police_decision_service_fkey FOREIGN KEY (service) REFERENCES service(service);


--
-- Name: autorite_police autorite_police_dossier_coordination_fkey; Type: FK CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY autorite_police
    ADD CONSTRAINT autorite_police_dossier_coordination_fkey FOREIGN KEY (dossier_coordination) REFERENCES dossier_coordination(dossier_coordination);


--
-- Name: autorite_police autorite_police_dossier_instruction_reunion_fkey; Type: FK CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY autorite_police
    ADD CONSTRAINT autorite_police_dossier_instruction_reunion_fkey FOREIGN KEY (dossier_instruction_reunion) REFERENCES dossier_instruction_reunion(dossier_instruction_reunion);


--
-- Name: autorite_police autorite_police_dossier_instruction_reunion_prochain_fkey; Type: FK CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY autorite_police
    ADD CONSTRAINT autorite_police_dossier_instruction_reunion_prochain_fkey FOREIGN KEY (dossier_instruction_reunion_prochain) REFERENCES dossier_instruction_reunion(dossier_instruction_reunion);


--
-- Name: autorite_police autorite_police_etablissement_fkey; Type: FK CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY autorite_police
    ADD CONSTRAINT autorite_police_etablissement_fkey FOREIGN KEY (etablissement) REFERENCES etablissement(etablissement);


--
-- Name: autorite_police_motif autorite_police_motif_service_fkey; Type: FK CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY autorite_police_motif
    ADD CONSTRAINT autorite_police_motif_service_fkey FOREIGN KEY (service) REFERENCES service(service);


--
-- Name: autorite_police autorite_police_service_fkey; Type: FK CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY autorite_police
    ADD CONSTRAINT autorite_police_service_fkey FOREIGN KEY (service) REFERENCES service(service);


--
-- Name: contact contact_contact_type_fkey; Type: FK CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY contact
    ADD CONSTRAINT contact_contact_type_fkey FOREIGN KEY (contact_type) REFERENCES contact_type(contact_type);


--
-- Name: contact contact_etablissement_fkey; Type: FK CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY contact
    ADD CONSTRAINT contact_etablissement_fkey FOREIGN KEY (etablissement) REFERENCES etablissement(etablissement) MATCH FULL;


--
-- Name: contact contact_service_fkey; Type: FK CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY contact
    ADD CONSTRAINT contact_service_fkey FOREIGN KEY (service) REFERENCES service(service);


--
-- Name: contact contat_contact_civilite_fkey; Type: FK CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY contact
    ADD CONSTRAINT contat_contact_civilite_fkey FOREIGN KEY (civilite) REFERENCES contact_civilite(contact_civilite);


--
-- Name: visite courrier_annulation_visite_fkey; Type: FK CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY visite
    ADD CONSTRAINT courrier_annulation_visite_fkey FOREIGN KEY (courrier_annulation) REFERENCES courrier(courrier);


--
-- Name: courrier courrier_courrier_joint_fkey; Type: FK CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY courrier
    ADD CONSTRAINT courrier_courrier_joint_fkey FOREIGN KEY (courrier_joint) REFERENCES courrier(courrier);


--
-- Name: courrier courrier_courrier_parent_fkey; Type: FK CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY courrier
    ADD CONSTRAINT courrier_courrier_parent_fkey FOREIGN KEY (courrier_parent) REFERENCES courrier(courrier);


--
-- Name: courrier courrier_courrier_type_fkey; Type: FK CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY courrier
    ADD CONSTRAINT courrier_courrier_type_fkey FOREIGN KEY (courrier_type) REFERENCES courrier_type(courrier_type);


--
-- Name: courrier courrier_dossier_coordination_fkey; Type: FK CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY courrier
    ADD CONSTRAINT courrier_dossier_coordination_fkey FOREIGN KEY (dossier_coordination) REFERENCES dossier_coordination(dossier_coordination);


--
-- Name: courrier courrier_dossier_instruction_fkey; Type: FK CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY courrier
    ADD CONSTRAINT courrier_dossier_instruction_fkey FOREIGN KEY (dossier_instruction) REFERENCES dossier_instruction(dossier_instruction);


--
-- Name: courrier courrier_etablissement_fkey; Type: FK CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY courrier
    ADD CONSTRAINT courrier_etablissement_fkey FOREIGN KEY (etablissement) REFERENCES etablissement(etablissement);


--
-- Name: courrier courrier_modele_edition_fkey; Type: FK CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY courrier
    ADD CONSTRAINT courrier_modele_edition_fkey FOREIGN KEY (modele_edition) REFERENCES modele_edition(modele_edition);


--
-- Name: courrier courrier_proces_verbal_fkey; Type: FK CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY courrier
    ADD CONSTRAINT courrier_proces_verbal_fkey FOREIGN KEY (proces_verbal) REFERENCES proces_verbal(proces_verbal);


--
-- Name: courrier courrier_signataire_fkey; Type: FK CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY courrier
    ADD CONSTRAINT courrier_signataire_fkey FOREIGN KEY (signataire) REFERENCES signataire(signataire);


--
-- Name: courrier_texte_type courrier_texte_type_courrier_type_fkey; Type: FK CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY courrier_texte_type
    ADD CONSTRAINT courrier_texte_type_courrier_type_fkey FOREIGN KEY (courrier_type) REFERENCES courrier_type(courrier_type);


--
-- Name: courrier_type courrier_type_courrier_type_categorie_fkey; Type: FK CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY courrier_type
    ADD CONSTRAINT courrier_type_courrier_type_categorie_fkey FOREIGN KEY (courrier_type_categorie) REFERENCES courrier_type_categorie(courrier_type_categorie);


--
-- Name: courrier_type courrier_type_service_fkey; Type: FK CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY courrier_type
    ADD CONSTRAINT courrier_type_service_fkey FOREIGN KEY (service) REFERENCES service(service);


--
-- Name: visite courrier_visite_fkey; Type: FK CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY visite
    ADD CONSTRAINT courrier_visite_fkey FOREIGN KEY (courrier_convocation_exploitants) REFERENCES courrier(courrier);


--
-- Name: document_presente document_presente_service_fkey; Type: FK CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY document_presente
    ADD CONSTRAINT document_presente_service_fkey FOREIGN KEY (service) REFERENCES service(service);


--
-- Name: dossier_coordination dossier_coordination_dossier_coordination_parent_fkey; Type: FK CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY dossier_coordination
    ADD CONSTRAINT dossier_coordination_dossier_coordination_parent_fkey FOREIGN KEY (dossier_coordination_parent) REFERENCES dossier_coordination(dossier_coordination);


--
-- Name: dossier_coordination dossier_coordination_dossier_coordination_type_fkey; Type: FK CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY dossier_coordination
    ADD CONSTRAINT dossier_coordination_dossier_coordination_type_fkey FOREIGN KEY (dossier_coordination_type) REFERENCES dossier_coordination_type(dossier_coordination_type);


--
-- Name: dossier_coordination dossier_coordination_etablissement_categorie_fkey; Type: FK CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY dossier_coordination
    ADD CONSTRAINT dossier_coordination_etablissement_categorie_fkey FOREIGN KEY (etablissement_categorie) REFERENCES etablissement_categorie(etablissement_categorie);


--
-- Name: dossier_coordination dossier_coordination_etablissement_type_fkey; Type: FK CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY dossier_coordination
    ADD CONSTRAINT dossier_coordination_etablissement_type_fkey FOREIGN KEY (etablissement_type) REFERENCES etablissement_type(etablissement_type);


--
-- Name: dossier_coordination_message dossier_coordination_message_dossier_coordination_fkey; Type: FK CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY dossier_coordination_message
    ADD CONSTRAINT dossier_coordination_message_dossier_coordination_fkey FOREIGN KEY (dossier_coordination) REFERENCES dossier_coordination(dossier_coordination);


--
-- Name: dossier_coordination_parcelle dossier_coordination_parcelle_dossier_coordination_fkey; Type: FK CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY dossier_coordination_parcelle
    ADD CONSTRAINT dossier_coordination_parcelle_dossier_coordination_fkey FOREIGN KEY (dossier_coordination) REFERENCES dossier_coordination(dossier_coordination);


--
-- Name: dossier_coordination_type dossier_coordination_type_analyses_type_acc_fkey; Type: FK CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY dossier_coordination_type
    ADD CONSTRAINT dossier_coordination_type_analyses_type_acc_fkey FOREIGN KEY (analyses_type_acc) REFERENCES analyses_type(analyses_type);


--
-- Name: dossier_coordination_type dossier_coordination_type_analyses_type_si_fkey; Type: FK CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY dossier_coordination_type
    ADD CONSTRAINT dossier_coordination_type_analyses_type_si_fkey FOREIGN KEY (analyses_type_si) REFERENCES analyses_type(analyses_type);


--
-- Name: dossier_coordination_type dossier_coordination_type_dossier_type_fkey; Type: FK CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY dossier_coordination_type
    ADD CONSTRAINT dossier_coordination_type_dossier_type_fkey FOREIGN KEY (dossier_type) REFERENCES dossier_type(dossier_type);


--
-- Name: dossier_coordination dossier_etablissement_fkey; Type: FK CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY dossier_coordination
    ADD CONSTRAINT dossier_etablissement_fkey FOREIGN KEY (etablissement) REFERENCES etablissement(etablissement) MATCH FULL;


--
-- Name: dossier_instruction dossier_instruction_acteur_fkey; Type: FK CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY dossier_instruction
    ADD CONSTRAINT dossier_instruction_acteur_fkey FOREIGN KEY (technicien) REFERENCES acteur(acteur);


--
-- Name: dossier_instruction dossier_instruction_autorite_competente_fkey; Type: FK CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY dossier_instruction
    ADD CONSTRAINT dossier_instruction_autorite_competente_fkey FOREIGN KEY (autorite_competente) REFERENCES autorite_competente(autorite_competente);


--
-- Name: dossier_instruction dossier_instruction_dossier_coordination_fkey; Type: FK CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY dossier_instruction
    ADD CONSTRAINT dossier_instruction_dossier_coordination_fkey FOREIGN KEY (dossier_coordination) REFERENCES dossier_coordination(dossier_coordination);


--
-- Name: dossier_instruction_reunion dossier_instruction_reunion_avis_fkey; Type: FK CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY dossier_instruction_reunion
    ADD CONSTRAINT dossier_instruction_reunion_avis_fkey FOREIGN KEY (avis) REFERENCES reunion_avis(reunion_avis);


--
-- Name: dossier_instruction_reunion dossier_instruction_reunion_dossier_instruction_fkey; Type: FK CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY dossier_instruction_reunion
    ADD CONSTRAINT dossier_instruction_reunion_dossier_instruction_fkey FOREIGN KEY (dossier_instruction) REFERENCES dossier_instruction(dossier_instruction);


--
-- Name: dossier_instruction_reunion dossier_instruction_reunion_proposition_avis_fkey; Type: FK CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY dossier_instruction_reunion
    ADD CONSTRAINT dossier_instruction_reunion_proposition_avis_fkey FOREIGN KEY (proposition_avis) REFERENCES reunion_avis(reunion_avis);


--
-- Name: dossier_instruction_reunion dossier_instruction_reunion_reunion_fkey; Type: FK CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY dossier_instruction_reunion
    ADD CONSTRAINT dossier_instruction_reunion_reunion_fkey FOREIGN KEY (reunion) REFERENCES reunion(reunion);


--
-- Name: dossier_instruction_reunion dossier_instruction_reunion_reunion_type_categorie_fkey; Type: FK CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY dossier_instruction_reunion
    ADD CONSTRAINT dossier_instruction_reunion_reunion_type_categorie_fkey FOREIGN KEY (reunion_type_categorie) REFERENCES reunion_categorie(reunion_categorie);


--
-- Name: dossier_instruction_reunion dossier_instruction_reunion_reunion_type_fkey; Type: FK CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY dossier_instruction_reunion
    ADD CONSTRAINT dossier_instruction_reunion_reunion_type_fkey FOREIGN KEY (reunion_type) REFERENCES reunion_type(reunion_type);


--
-- Name: dossier_instruction dossier_instruction_service_fkey; Type: FK CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY dossier_instruction
    ADD CONSTRAINT dossier_instruction_service_fkey FOREIGN KEY (service) REFERENCES service(service);


--
-- Name: essai_realise essai_realise_service_fkey; Type: FK CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY essai_realise
    ADD CONSTRAINT essai_realise_service_fkey FOREIGN KEY (service) REFERENCES service(service);


--
-- Name: etablissement etablissement_acc_derniere_visite_avis_fkey; Type: FK CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY etablissement
    ADD CONSTRAINT etablissement_acc_derniere_visite_avis_fkey FOREIGN KEY (acc_derniere_visite_avis) REFERENCES reunion_avis(reunion_avis);


--
-- Name: etablissement etablissement_acc_derniere_visite_technicien_fkey; Type: FK CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY etablissement
    ADD CONSTRAINT etablissement_acc_derniere_visite_technicien_fkey FOREIGN KEY (acc_derniere_visite_technicien) REFERENCES acteur(acteur);


--
-- Name: etablissement etablissement_arrondissement_fkey; Type: FK CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY etablissement
    ADD CONSTRAINT etablissement_arrondissement_fkey FOREIGN KEY (adresse_arrondissement) REFERENCES arrondissement(arrondissement) MATCH FULL;


--
-- Name: etablissement etablissement_categorie_fkey; Type: FK CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY etablissement
    ADD CONSTRAINT etablissement_categorie_fkey FOREIGN KEY (etablissement_categorie) REFERENCES etablissement_categorie(etablissement_categorie) MATCH FULL;


--
-- Name: etablissement etablissement_dossier_coordination_periodique_fkey; Type: FK CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY etablissement
    ADD CONSTRAINT etablissement_dossier_coordination_periodique_fkey FOREIGN KEY (dossier_coordination_periodique) REFERENCES dossier_coordination(dossier_coordination);


--
-- Name: etablissement etablissement_etablissement_etat_fkey; Type: FK CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY etablissement
    ADD CONSTRAINT etablissement_etablissement_etat_fkey FOREIGN KEY (etablissement_etat) REFERENCES etablissement_etat(etablissement_etat);


--
-- Name: etablissement etablissement_etablissement_nature_fkey; Type: FK CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY etablissement
    ADD CONSTRAINT etablissement_etablissement_nature_fkey FOREIGN KEY (etablissement_nature) REFERENCES etablissement_nature(etablissement_nature) MATCH FULL;


--
-- Name: etablissement etablissement_etablissement_statut_juridique_fkey; Type: FK CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY etablissement
    ADD CONSTRAINT etablissement_etablissement_statut_juridique_fkey FOREIGN KEY (etablissement_statut_juridique) REFERENCES etablissement_statut_juridique(etablissement_statut_juridique) MATCH FULL;


--
-- Name: etablissement etablissement_etablissement_tutelle_adm_fkey; Type: FK CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY etablissement
    ADD CONSTRAINT etablissement_etablissement_tutelle_adm_fkey FOREIGN KEY (etablissement_tutelle_adm) REFERENCES etablissement_tutelle_adm(etablissement_tutelle_adm);


--
-- Name: etablissement_parcelle etablissement_parcelle_etablissement_fkey; Type: FK CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY etablissement_parcelle
    ADD CONSTRAINT etablissement_parcelle_etablissement_fkey FOREIGN KEY (etablissement) REFERENCES etablissement(etablissement);


--
-- Name: periodicite_visites etablissement_periodicite_etablissement_categorie; Type: FK CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY periodicite_visites
    ADD CONSTRAINT etablissement_periodicite_etablissement_categorie FOREIGN KEY (etablissement_categorie) REFERENCES etablissement_categorie(etablissement_categorie);


--
-- Name: periodicite_visites etablissement_periodicite_etablissement_type_fkey; Type: FK CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY periodicite_visites
    ADD CONSTRAINT etablissement_periodicite_etablissement_type_fkey FOREIGN KEY (etablissement_type) REFERENCES etablissement_type(etablissement_type);


--
-- Name: etablissement etablissement_si_autorite_competente_plan_fkey; Type: FK CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY etablissement
    ADD CONSTRAINT etablissement_si_autorite_competente_plan_fkey FOREIGN KEY (si_autorite_competente_plan) REFERENCES autorite_competente(autorite_competente);


--
-- Name: etablissement etablissement_si_autorite_competente_visite_fkey; Type: FK CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY etablissement
    ADD CONSTRAINT etablissement_si_autorite_competente_visite_fkey FOREIGN KEY (si_autorite_competente_visite) REFERENCES autorite_competente(autorite_competente);


--
-- Name: etablissement etablissement_si_dernier_plan_avis_fkey; Type: FK CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY etablissement
    ADD CONSTRAINT etablissement_si_dernier_plan_avis_fkey FOREIGN KEY (si_dernier_plan_avis) REFERENCES reunion_avis(reunion_avis);


--
-- Name: etablissement etablissement_si_derniere_visite_avis_fkey; Type: FK CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY etablissement
    ADD CONSTRAINT etablissement_si_derniere_visite_avis_fkey FOREIGN KEY (si_derniere_visite_avis) REFERENCES reunion_avis(reunion_avis);


--
-- Name: etablissement etablissement_si_derniere_visite_technicien_fkey; Type: FK CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY etablissement
    ADD CONSTRAINT etablissement_si_derniere_visite_technicien_fkey FOREIGN KEY (si_derniere_visite_technicien) REFERENCES acteur(acteur);


--
-- Name: etablissement etablissement_si_prochaine_visite_type_fkey; Type: FK CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY etablissement
    ADD CONSTRAINT etablissement_si_prochaine_visite_type_fkey FOREIGN KEY (si_prochaine_visite_type) REFERENCES analyses_type(analyses_type);


--
-- Name: etablissement etablissement_type_fkey; Type: FK CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY etablissement
    ADD CONSTRAINT etablissement_type_fkey FOREIGN KEY (etablissement_type) REFERENCES etablissement_type(etablissement_type) MATCH FULL;


--
-- Name: lien_etablissement_e_type etablissement_type_secondaire_etablissement_fkey; Type: FK CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY lien_etablissement_e_type
    ADD CONSTRAINT etablissement_type_secondaire_etablissement_fkey FOREIGN KEY (etablissement) REFERENCES etablissement(etablissement);


--
-- Name: lien_etablissement_e_type etablissement_type_secondaire_etablissement_type_fkey; Type: FK CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY lien_etablissement_e_type
    ADD CONSTRAINT etablissement_type_secondaire_etablissement_type_fkey FOREIGN KEY (etablissement_type) REFERENCES etablissement_type(etablissement_type);


--
-- Name: etablissement_unite etablissement_unite_derogation_scda_fkey; Type: FK CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY etablissement_unite
    ADD CONSTRAINT etablissement_unite_derogation_scda_fkey FOREIGN KEY (acc_derogation_scda) REFERENCES derogation_scda(derogation_scda);


--
-- Name: etablissement_unite etablissement_unite_dossier_instruction_fkey; Type: FK CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY etablissement_unite
    ADD CONSTRAINT etablissement_unite_dossier_instruction_fkey FOREIGN KEY (dossier_instruction) REFERENCES dossier_instruction(dossier_instruction);


--
-- Name: etablissement_unite etablissement_unite_etablissement_fkey; Type: FK CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY etablissement_unite
    ADD CONSTRAINT etablissement_unite_etablissement_fkey FOREIGN KEY (etablissement) REFERENCES etablissement(etablissement);


--
-- Name: etablissement_unite etablissement_unite_etablissement_unite_fkey; Type: FK CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY etablissement_unite
    ADD CONSTRAINT etablissement_unite_etablissement_unite_fkey FOREIGN KEY (etablissement_unite_lie) REFERENCES etablissement_unite(etablissement_unite);


--
-- Name: etablissement etablissement_visite_duree_fkey; Type: FK CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY etablissement
    ADD CONSTRAINT etablissement_visite_duree_fkey FOREIGN KEY (si_visite_duree) REFERENCES visite_duree(visite_duree);


--
-- Name: etablissement etablissement_voie_fkey; Type: FK CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY etablissement
    ADD CONSTRAINT etablissement_voie_fkey FOREIGN KEY (adresse_voie) REFERENCES voie(voie);


--
-- Name: lien_dossier_coordination_etablissement_type lien_analyses_etablissement_type_etablissement_type_fkey; Type: FK CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY lien_dossier_coordination_etablissement_type
    ADD CONSTRAINT lien_analyses_etablissement_type_etablissement_type_fkey FOREIGN KEY (etablissement_type) REFERENCES etablissement_type(etablissement_type);


--
-- Name: lien_autorite_police_courrier lien_autorite_police_courrier_autorite_police_fkey; Type: FK CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY lien_autorite_police_courrier
    ADD CONSTRAINT lien_autorite_police_courrier_autorite_police_fkey FOREIGN KEY (autorite_police) REFERENCES autorite_police(autorite_police);


--
-- Name: lien_autorite_police_courrier lien_autorite_police_courrier_courrier_fkey; Type: FK CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY lien_autorite_police_courrier
    ADD CONSTRAINT lien_autorite_police_courrier_courrier_fkey FOREIGN KEY (courrier) REFERENCES courrier(courrier);


--
-- Name: lien_contrainte_dossier_coordination lien_contrainte_dossier_coordination_contrainte_fkey; Type: FK CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY lien_contrainte_dossier_coordination
    ADD CONSTRAINT lien_contrainte_dossier_coordination_contrainte_fkey FOREIGN KEY (contrainte) REFERENCES contrainte(contrainte);


--
-- Name: lien_contrainte_dossier_coordination lien_contrainte_dossier_coordination_dossier_fkey; Type: FK CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY lien_contrainte_dossier_coordination
    ADD CONSTRAINT lien_contrainte_dossier_coordination_dossier_fkey FOREIGN KEY (dossier_coordination) REFERENCES dossier_coordination(dossier_coordination);


--
-- Name: lien_contrainte_etablissement lien_contrainte_etablissement_contrainte_fkey; Type: FK CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY lien_contrainte_etablissement
    ADD CONSTRAINT lien_contrainte_etablissement_contrainte_fkey FOREIGN KEY (contrainte) REFERENCES contrainte(contrainte);


--
-- Name: lien_contrainte_etablissement lien_contrainte_etablissement_dossier_fkey; Type: FK CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY lien_contrainte_etablissement
    ADD CONSTRAINT lien_contrainte_etablissement_dossier_fkey FOREIGN KEY (etablissement) REFERENCES etablissement(etablissement);


--
-- Name: lien_courrier_contact lien_courrier_contact_contact_fkey; Type: FK CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY lien_courrier_contact
    ADD CONSTRAINT lien_courrier_contact_contact_fkey FOREIGN KEY (contact) REFERENCES contact(contact);


--
-- Name: lien_courrier_contact lien_courrier_contact_courrier_fkey; Type: FK CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY lien_courrier_contact
    ADD CONSTRAINT lien_courrier_contact_courrier_fkey FOREIGN KEY (courrier) REFERENCES courrier(courrier);


--
-- Name: lien_dossier_coordination_contact lien_dossier_coordination_contact_contact_fkey; Type: FK CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY lien_dossier_coordination_contact
    ADD CONSTRAINT lien_dossier_coordination_contact_contact_fkey FOREIGN KEY (contact) REFERENCES contact(contact);


--
-- Name: lien_dossier_coordination_contact lien_dossier_coordination_contact_dossier_coordination_fkey; Type: FK CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY lien_dossier_coordination_contact
    ADD CONSTRAINT lien_dossier_coordination_contact_dossier_coordination_fkey FOREIGN KEY (dossier_coordination) REFERENCES dossier_coordination(dossier_coordination);


--
-- Name: lien_dossier_coordination_etablissement_type lien_dossier_coordination_etablisseme_dossier_coordination_fkey; Type: FK CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY lien_dossier_coordination_etablissement_type
    ADD CONSTRAINT lien_dossier_coordination_etablisseme_dossier_coordination_fkey FOREIGN KEY (dossier_coordination) REFERENCES dossier_coordination(dossier_coordination);


--
-- Name: lien_dossier_coordination_type_analyses_type lien_dossier_coordination_type_analyses_type_analyses_type_fkey; Type: FK CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY lien_dossier_coordination_type_analyses_type
    ADD CONSTRAINT lien_dossier_coordination_type_analyses_type_analyses_type_fkey FOREIGN KEY (analyses_type) REFERENCES analyses_type(analyses_type);


--
-- Name: lien_dossier_coordination_type_analyses_type lien_dossier_coordination_type_analyses_type_dossier_coordinati; Type: FK CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY lien_dossier_coordination_type_analyses_type
    ADD CONSTRAINT lien_dossier_coordination_type_analyses_type_dossier_coordinati FOREIGN KEY (dossier_coordination_type) REFERENCES dossier_coordination_type(dossier_coordination_type);


--
-- Name: lien_essai_realise_analyses lien_essai_realise_analyses_analyses_fkey; Type: FK CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY lien_essai_realise_analyses
    ADD CONSTRAINT lien_essai_realise_analyses_analyses_fkey FOREIGN KEY (analyses) REFERENCES analyses(analyses);


--
-- Name: lien_essai_realise_analyses lien_essai_realise_analyses_essai_realise_fkey; Type: FK CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY lien_essai_realise_analyses
    ADD CONSTRAINT lien_essai_realise_analyses_essai_realise_fkey FOREIGN KEY (essai_realise) REFERENCES essai_realise(essai_realise);


--
-- Name: lien_prescription_reglementaire_etablissement_categorie lien_prescription_reglementaire_etablissement_categorie_etablis; Type: FK CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY lien_prescription_reglementaire_etablissement_categorie
    ADD CONSTRAINT lien_prescription_reglementaire_etablissement_categorie_etablis FOREIGN KEY (etablissement_categorie) REFERENCES etablissement_categorie(etablissement_categorie);


--
-- Name: lien_prescription_reglementaire_etablissement_categorie lien_prescription_reglementaire_etablissement_categorie_prescri; Type: FK CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY lien_prescription_reglementaire_etablissement_categorie
    ADD CONSTRAINT lien_prescription_reglementaire_etablissement_categorie_prescri FOREIGN KEY (prescription_reglementaire) REFERENCES prescription_reglementaire(prescription_reglementaire);


--
-- Name: lien_prescription_reglementaire_etablissement_type lien_prescription_reglementaire_etablissement_type_etablissemen; Type: FK CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY lien_prescription_reglementaire_etablissement_type
    ADD CONSTRAINT lien_prescription_reglementaire_etablissement_type_etablissemen FOREIGN KEY (etablissement_type) REFERENCES etablissement_type(etablissement_type);


--
-- Name: lien_prescription_reglementaire_etablissement_type lien_prescription_reglementaire_etablissement_type_prescription; Type: FK CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY lien_prescription_reglementaire_etablissement_type
    ADD CONSTRAINT lien_prescription_reglementaire_etablissement_type_prescription FOREIGN KEY (prescription_reglementaire) REFERENCES prescription_reglementaire(prescription_reglementaire);


--
-- Name: lien_reglementation_applicable_etablissement_categorie lien_reglementation_applicable_etablissement_categorie_etabliss; Type: FK CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY lien_reglementation_applicable_etablissement_categorie
    ADD CONSTRAINT lien_reglementation_applicable_etablissement_categorie_etabliss FOREIGN KEY (etablissement_categorie) REFERENCES etablissement_categorie(etablissement_categorie);


--
-- Name: lien_reglementation_applicable_etablissement_categorie lien_reglementation_applicable_etablissement_categorie_reglemen; Type: FK CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY lien_reglementation_applicable_etablissement_categorie
    ADD CONSTRAINT lien_reglementation_applicable_etablissement_categorie_reglemen FOREIGN KEY (reglementation_applicable) REFERENCES reglementation_applicable(reglementation_applicable);


--
-- Name: lien_reglementation_applicable_etablissement_type lien_reglementation_applicable_etablissement_type_etablissement; Type: FK CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY lien_reglementation_applicable_etablissement_type
    ADD CONSTRAINT lien_reglementation_applicable_etablissement_type_etablissement FOREIGN KEY (etablissement_type) REFERENCES etablissement_type(etablissement_type);


--
-- Name: lien_reglementation_applicable_etablissement_type lien_reglementation_applicable_etablissement_type_reglementatio; Type: FK CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY lien_reglementation_applicable_etablissement_type
    ADD CONSTRAINT lien_reglementation_applicable_etablissement_type_reglementatio FOREIGN KEY (reglementation_applicable) REFERENCES reglementation_applicable(reglementation_applicable);


--
-- Name: lien_reunion_r_instance_r_i_membre lien_reunion_r_instance_r_i_membre__reunion__fkey; Type: FK CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY lien_reunion_r_instance_r_i_membre
    ADD CONSTRAINT lien_reunion_r_instance_r_i_membre__reunion__fkey FOREIGN KEY (reunion) REFERENCES reunion(reunion);


--
-- Name: lien_reunion_r_instance_r_i_membre lien_reunion_r_instance_r_i_membre__reunion_instance__fkey; Type: FK CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY lien_reunion_r_instance_r_i_membre
    ADD CONSTRAINT lien_reunion_r_instance_r_i_membre__reunion_instance__fkey FOREIGN KEY (reunion_instance) REFERENCES reunion_instance(reunion_instance);


--
-- Name: lien_reunion_r_instance_r_i_membre lien_reunion_r_instance_r_i_membre__reunion_instance_membre__fk; Type: FK CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY lien_reunion_r_instance_r_i_membre
    ADD CONSTRAINT lien_reunion_r_instance_r_i_membre__reunion_instance_membre__fk FOREIGN KEY (reunion_instance_membre) REFERENCES reunion_instance_membre(reunion_instance_membre);


--
-- Name: modele_edition modele_edition_courrier_type_fkey; Type: FK CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY modele_edition
    ADD CONSTRAINT modele_edition_courrier_type_fkey FOREIGN KEY (courrier_type) REFERENCES courrier_type(courrier_type);


--
-- Name: piece piece_dossier_coordination_fkey; Type: FK CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY piece
    ADD CONSTRAINT piece_dossier_coordination_fkey FOREIGN KEY (dossier_coordination) REFERENCES dossier_coordination(dossier_coordination);


--
-- Name: piece piece_dossier_instruction_fkey; Type: FK CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY piece
    ADD CONSTRAINT piece_dossier_instruction_fkey FOREIGN KEY (dossier_instruction) REFERENCES dossier_instruction(dossier_instruction);


--
-- Name: piece piece_etablissement_fkey; Type: FK CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY piece
    ADD CONSTRAINT piece_etablissement_fkey FOREIGN KEY (etablissement) REFERENCES etablissement(etablissement);


--
-- Name: piece piece_piece_statut_fkey; Type: FK CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY piece
    ADD CONSTRAINT piece_piece_statut_fkey FOREIGN KEY (piece_statut) REFERENCES piece_statut(piece_statut);


--
-- Name: piece piece_piece_type_fkey; Type: FK CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY piece
    ADD CONSTRAINT piece_piece_type_fkey FOREIGN KEY (piece_type) REFERENCES piece_type(piece_type);


--
-- Name: piece piece_service_fkey; Type: FK CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY piece
    ADD CONSTRAINT piece_service_fkey FOREIGN KEY (service) REFERENCES service(service);


--
-- Name: prescription prescription_analyses_fkey; Type: FK CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY prescription
    ADD CONSTRAINT prescription_analyses_fkey FOREIGN KEY (analyses) REFERENCES analyses(analyses);


--
-- Name: prescription prescription_prescription_reglementaire_fkey; Type: FK CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY prescription
    ADD CONSTRAINT prescription_prescription_reglementaire_fkey FOREIGN KEY (prescription_reglementaire) REFERENCES prescription_reglementaire(prescription_reglementaire);


--
-- Name: prescription_reglementaire prescription_reglementaire_service_fkey; Type: FK CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY prescription_reglementaire
    ADD CONSTRAINT prescription_reglementaire_service_fkey FOREIGN KEY (service) REFERENCES service(service);


--
-- Name: prescription_specifique prescription_specifique_prescription_reglementaire_fkey; Type: FK CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY prescription_specifique
    ADD CONSTRAINT prescription_specifique_prescription_reglementaire_fkey FOREIGN KEY (prescription_reglementaire) REFERENCES prescription_reglementaire(prescription_reglementaire);


--
-- Name: prescription_specifique prescription_specifique_service_fkey; Type: FK CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY prescription_specifique
    ADD CONSTRAINT prescription_specifique_service_fkey FOREIGN KEY (service) REFERENCES service(service);


--
-- Name: proces_verbal proces_verbal_courrier_genere_fkey; Type: FK CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY proces_verbal
    ADD CONSTRAINT proces_verbal_courrier_genere_fkey FOREIGN KEY (courrier_genere) REFERENCES courrier(courrier);


--
-- Name: proces_verbal proces_verbal_dossier_instruction_fkey; Type: FK CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY proces_verbal
    ADD CONSTRAINT proces_verbal_dossier_instruction_fkey FOREIGN KEY (dossier_instruction) REFERENCES dossier_instruction(dossier_instruction);


--
-- Name: proces_verbal proces_verbal_dossier_instruction_reunion_fkey; Type: FK CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY proces_verbal
    ADD CONSTRAINT proces_verbal_dossier_instruction_reunion_fkey FOREIGN KEY (dossier_instruction_reunion) REFERENCES dossier_instruction_reunion(dossier_instruction_reunion);


--
-- Name: proces_verbal proces_verbal_modele_edition_fkey; Type: FK CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY proces_verbal
    ADD CONSTRAINT proces_verbal_modele_edition_fkey FOREIGN KEY (modele_edition) REFERENCES modele_edition(modele_edition);


--
-- Name: proces_verbal proces_verbal_signataire_fkey; Type: FK CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY proces_verbal
    ADD CONSTRAINT proces_verbal_signataire_fkey FOREIGN KEY (signataire) REFERENCES signataire(signataire);


--
-- Name: programmation programmation_programmation_etat_fkey; Type: FK CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY programmation
    ADD CONSTRAINT programmation_programmation_etat_fkey FOREIGN KEY (programmation_etat) REFERENCES programmation_etat(programmation_etat);


--
-- Name: programmation programmation_service_fkey; Type: FK CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY programmation
    ADD CONSTRAINT programmation_service_fkey FOREIGN KEY (service) REFERENCES service(service);


--
-- Name: quartier quartier_arrondissement_fkey; Type: FK CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY quartier
    ADD CONSTRAINT quartier_arrondissement_fkey FOREIGN KEY (arrondissement) REFERENCES arrondissement(arrondissement);


--
-- Name: reglementation_applicable reglementation_applicable_service_fkey; Type: FK CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY reglementation_applicable
    ADD CONSTRAINT reglementation_applicable_service_fkey FOREIGN KEY (service) REFERENCES service(service);


--
-- Name: reunion_avis reunion_avis_service_fkey; Type: FK CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY reunion_avis
    ADD CONSTRAINT reunion_avis_service_fkey FOREIGN KEY (service) REFERENCES service(service);


--
-- Name: reunion_categorie reunion_categorie_service_fkey; Type: FK CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY reunion_categorie
    ADD CONSTRAINT reunion_categorie_service_fkey FOREIGN KEY (service) REFERENCES service(service);


--
-- Name: reunion_instance_membre reunion_instance_membre_reunion_instance_fkey; Type: FK CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY reunion_instance_membre
    ADD CONSTRAINT reunion_instance_membre_reunion_instance_fkey FOREIGN KEY (reunion_instance) REFERENCES reunion_instance(reunion_instance);


--
-- Name: reunion_instance reunion_instance_service_fkey; Type: FK CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY reunion_instance
    ADD CONSTRAINT reunion_instance_service_fkey FOREIGN KEY (service) REFERENCES service(service);


--
-- Name: reunion reunion_reunion_type_fkey; Type: FK CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY reunion
    ADD CONSTRAINT reunion_reunion_type_fkey FOREIGN KEY (reunion_type) REFERENCES reunion_type(reunion_type);


--
-- Name: reunion_type reunion_type__president__reunion_instance__fkey; Type: FK CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY reunion_type
    ADD CONSTRAINT reunion_type__president__reunion_instance__fkey FOREIGN KEY (president) REFERENCES reunion_instance(reunion_instance);


--
-- Name: reunion_type reunion_type_modele_compte_rendu_global_fkey; Type: FK CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY reunion_type
    ADD CONSTRAINT reunion_type_modele_compte_rendu_global_fkey FOREIGN KEY (modele_compte_rendu_global) REFERENCES modele_edition(modele_edition);


--
-- Name: reunion_type reunion_type_modele_compte_rendu_specifique_fkey; Type: FK CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY reunion_type
    ADD CONSTRAINT reunion_type_modele_compte_rendu_specifique_fkey FOREIGN KEY (modele_compte_rendu_specifique) REFERENCES modele_edition(modele_edition);


--
-- Name: reunion_type reunion_type_modele_feuille_presence_fkey; Type: FK CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY reunion_type
    ADD CONSTRAINT reunion_type_modele_feuille_presence_fkey FOREIGN KEY (modele_feuille_presence) REFERENCES modele_edition(modele_edition);


--
-- Name: reunion_type reunion_type_modele_ordre_du_jour_fkey; Type: FK CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY reunion_type
    ADD CONSTRAINT reunion_type_modele_ordre_du_jour_fkey FOREIGN KEY (modele_ordre_du_jour) REFERENCES modele_edition(modele_edition);


--
-- Name: reunion_type_reunion_avis reunion_type_reunion_avis_reunion_avis_fkey; Type: FK CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY reunion_type_reunion_avis
    ADD CONSTRAINT reunion_type_reunion_avis_reunion_avis_fkey FOREIGN KEY (reunion_avis) REFERENCES reunion_avis(reunion_avis);


--
-- Name: reunion_type_reunion_avis reunion_type_reunion_avis_reunion_type_fkey; Type: FK CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY reunion_type_reunion_avis
    ADD CONSTRAINT reunion_type_reunion_avis_reunion_type_fkey FOREIGN KEY (reunion_type) REFERENCES reunion_type(reunion_type);


--
-- Name: reunion_type_reunion_categorie reunion_type_reunion_categorie_reunion_categorie_fkey; Type: FK CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY reunion_type_reunion_categorie
    ADD CONSTRAINT reunion_type_reunion_categorie_reunion_categorie_fkey FOREIGN KEY (reunion_categorie) REFERENCES reunion_categorie(reunion_categorie);


--
-- Name: reunion_type_reunion_categorie reunion_type_reunion_categorie_reunion_type_fkey; Type: FK CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY reunion_type_reunion_categorie
    ADD CONSTRAINT reunion_type_reunion_categorie_reunion_type_fkey FOREIGN KEY (reunion_type) REFERENCES reunion_type(reunion_type);


--
-- Name: reunion_type_reunion_instance reunion_type_reunion_instance_reunion_instance_fkey; Type: FK CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY reunion_type_reunion_instance
    ADD CONSTRAINT reunion_type_reunion_instance_reunion_instance_fkey FOREIGN KEY (reunion_instance) REFERENCES reunion_instance(reunion_instance);


--
-- Name: reunion_type_reunion_instance reunion_type_reunion_instance_reunion_type_fkey; Type: FK CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY reunion_type_reunion_instance
    ADD CONSTRAINT reunion_type_reunion_instance_reunion_type_fkey FOREIGN KEY (reunion_type) REFERENCES reunion_type(reunion_type);


--
-- Name: reunion_type reunion_type_service_fkey; Type: FK CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY reunion_type
    ADD CONSTRAINT reunion_type_service_fkey FOREIGN KEY (service) REFERENCES service(service);


--
-- Name: signataire signataire_civilite_fkey; Type: FK CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY signataire
    ADD CONSTRAINT signataire_civilite_fkey FOREIGN KEY (civilite) REFERENCES contact_civilite(contact_civilite);


--
-- Name: signataire signataire_signataire_qualite_fkey; Type: FK CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY signataire
    ADD CONSTRAINT signataire_signataire_qualite_fkey FOREIGN KEY (signataire_qualite) REFERENCES signataire_qualite(signataire_qualite);


--
-- Name: technicien_arrondissement technicien_arrondissement_arrondissement_fkey; Type: FK CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY technicien_arrondissement
    ADD CONSTRAINT technicien_arrondissement_arrondissement_fkey FOREIGN KEY (arrondissement) REFERENCES arrondissement(arrondissement);


--
-- Name: technicien_arrondissement technicien_arrondissement_service_fkey; Type: FK CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY technicien_arrondissement
    ADD CONSTRAINT technicien_arrondissement_service_fkey FOREIGN KEY (service) REFERENCES service(service);


--
-- Name: technicien_arrondissement technicien_arrondissement_technicien_fkey; Type: FK CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY technicien_arrondissement
    ADD CONSTRAINT technicien_arrondissement_technicien_fkey FOREIGN KEY (technicien) REFERENCES acteur(acteur);


--
-- Name: visite visite_acteur_fkey; Type: FK CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY visite
    ADD CONSTRAINT visite_acteur_fkey FOREIGN KEY (acteur) REFERENCES acteur(acteur);


--
-- Name: courrier visite_courrier_fkey; Type: FK CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY courrier
    ADD CONSTRAINT visite_courrier_fkey FOREIGN KEY (visite) REFERENCES visite(visite);


--
-- Name: visite visite_dossier_instruction_fkey; Type: FK CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY visite
    ADD CONSTRAINT visite_dossier_instruction_fkey FOREIGN KEY (dossier_instruction) REFERENCES dossier_instruction(dossier_instruction);


--
-- Name: visite visite_programmation_fkey; Type: FK CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY visite
    ADD CONSTRAINT visite_programmation_fkey FOREIGN KEY (programmation) REFERENCES programmation(programmation);


--
-- Name: visite visite_visite_etat_fkey; Type: FK CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY visite
    ADD CONSTRAINT visite_visite_etat_fkey FOREIGN KEY (visite_etat) REFERENCES visite_etat(visite_etat);


--
-- Name: visite visite_visite_motif_annulation_fkey; Type: FK CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY visite
    ADD CONSTRAINT visite_visite_motif_annulation_fkey FOREIGN KEY (visite_motif_annulation) REFERENCES visite_motif_annulation(visite_motif_annulation);


--
-- Name: voie_arrondissement voie_arrondissement_arrondissement_fkey; Type: FK CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY voie_arrondissement
    ADD CONSTRAINT voie_arrondissement_arrondissement_fkey FOREIGN KEY (arrondissement) REFERENCES arrondissement(arrondissement);


--
-- Name: voie_arrondissement voie_arrondissement_voie_fkey; Type: FK CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY voie_arrondissement
    ADD CONSTRAINT voie_arrondissement_voie_fkey FOREIGN KEY (voie) REFERENCES voie(voie);


--
-- Name: voie voie_om_collectivite_fkey; Type: FK CONSTRAINT; Schema: openaria; Owner: -
--

ALTER TABLE ONLY voie
    ADD CONSTRAINT voie_om_collectivite_fkey FOREIGN KEY (om_collectivite) REFERENCES om_collectivite(om_collectivite);


--
-- PostgreSQL database dump complete
--

