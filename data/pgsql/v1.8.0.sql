-------------------------------------------------------------------------------
-- Script de mise à jour vers la version v1.8.0 depuis la version v1.7.0
--
-- @package openaria
-- @version SVN : $Id$
-------------------------------------------------------------------------------

--
-- Ticket #9234 - BEGIN
--
ALTER TABLE analyses DROP COLUMN acc_ascenseur;
ALTER TABLE analyses DROP COLUMN acc_boucle_magnetique;
ALTER TABLE analyses DROP COLUMN acc_chambres_amenagees;
ALTER TABLE analyses DROP COLUMN acc_derogation_scda;
ALTER TABLE analyses DROP COLUMN acc_douche;
ALTER TABLE analyses DROP COLUMN acc_elevateur;
ALTER TABLE analyses DROP COLUMN acc_handicap_auditif;
ALTER TABLE analyses DROP COLUMN acc_handicap_mental;
ALTER TABLE analyses DROP COLUMN acc_handicap_physique;
ALTER TABLE analyses DROP COLUMN acc_handicap_visuel;
ALTER TABLE analyses DROP COLUMN acc_places_assises_public;
ALTER TABLE analyses DROP COLUMN acc_places_stationnement_amenagees;
ALTER TABLE analyses DROP COLUMN acc_sanitaire;
--
-- Ticket #9234 - END
--

--
-- Ticket #9384 - BEGIN
--
ALTER TABLE dossier_coordination ADD dossier_ads_force boolean;
--
-- Ticket #9384 - END
--
