-------------------------------------------------------------------------------
-- Script de mise à jour vers la version v1.11.0 depuis la version v1.10.1
--
-- @package openaria
-- @version SVN : $Id$
-------------------------------------------------------------------------------

--
-- Ticket #9480 - BEGIN
--
ALTER TABLE etablissement_nature ADD erp boolean;
--
-- Ticket #9480 - END
--
