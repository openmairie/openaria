<?php
/**
 * WIDGET DASHBOARD - widget_mes_documents_entrants_non_lus.
 *
 * @package openaria
 * @version SVN : $Id$
 */

// Instanciation optionnelle de la classe *om_application*
require_once "../obj/openaria.class.php";
if (!isset($f)) {
    $f = new openaria(null);
}

// Affichage du widget
$p = $f->get_inst_pilotage();
if (isset($content) !== true) {
    $content = null;
}
$widget_is_empty = $p->view_widget_mes_documents_entrants_non_lus($content);

