<?php
/**
 * Ce script permet d'interfacer l'application.
 *
 * @package openaria
 * @version SVN : $Id$
 */

require_once "../obj/openaria.class.php";
$flag = filter_input(INPUT_GET, 'module');
if (in_array($flag, array("login", "logout", )) === false) {
    $flag = "nohtml";
}
$f = new openaria($flag);
$f->view_main();
