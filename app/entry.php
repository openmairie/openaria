<?php
/**
 * Ce script redirige vers un ou une liste d'élement passé en paramètre.
 *
 * @package openaria
 * @version SVN : $Id$
 */

//
require_once "../obj/openaria.class.php";
$f = new openaria("nohtml");

// Récupération de l'objet
$obj = $f->get_submitted_get_value("obj");
// Récupération de l'action
$action = $f->get_submitted_get_value("action");
// Récupération du champ de recherche
$by = $f->get_submitted_get_value("by");
// Récupération de la valeur de l'éléments
$id = $f->get_submitted_get_value("id");
// Récupération des valeurs des éléments
$ids = $f->get_submitted_get_value("ids");

// Tous les paramètres doivent être renseignés
if ($obj === null || $obj === ''
    || ($obj !== 'etablissement' && $obj !== 'dc')
    || $action === null || $action === ''
    || ($action !== 'view' && $action !== 'list' )
    || (($action === 'view' && ($id === null || $id === ''))
        || (($action === 'list' && ($ids === null || $ids === ''))))
    || $by === null || $by === '') {
    // Redirige vers la page d'accueil de l'application
    header('Location: ../index.php');
    die();
}

// Récupère la table, la classe et le champ de la recherche avancée 
// à utiliser pour l'objet passé en paramètre
$table = null;
$class = null;
$advanced_search_field = null;
//
if ($obj === 'etablissement') {
    //
    $table = 'etablissement';
    $class = 'etablissement_tous';
    $advanced_search_field = 'etablissement';
}
//
if ($obj === 'dc') {
    //
    $table = 'dossier_coordination';
    $class = 'dossier_coordination';
    $advanced_search_field = 'libelle';
}

// Si la recherche ne concerne qu'un seul élément
if ($action !== 'list') {

    // Récupère l'identifiant de l'enregistrement
    $primary_key_value = $f->get_id_from_table_by_field($id, $by, $table);

    // Redirige vers le formulaire de l'élément
    header('Location: '.OM_ROUTE_FORM.'&obj='.$class.'&action=3&idx='.$primary_key_value);
    die();
}

// Sinon la recherche concerne plusieurs éléments
// Récupération des valeurs à chercher
$search = array();
// Ne cherche pas sur le champ passé dans $by, car il ne correspond pas
// forcement au champ dans la recherche avancée
// La recherche est donc forcée sur un champ du formulaire prédéfinit dans ce
// script
$search[$advanced_search_field] = $ids;

// Création d'une variable de session de recherche avancée
$advs_id = str_replace(array('.',','), '', microtime(true));
//
$search["advanced-search-submit"] = "";
//
$_SESSION["advs_ids"][$advs_id] = serialize($search);

// Redirection vers le listing de l'objet
header('Location: '.OM_ROUTE_TAB.'&obj='.$class.'&advs_id='.$advs_id);
