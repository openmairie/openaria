<?php
/**
 * WIDGET DASHBOARD - widget_dossier_coordination_a_cloturer.
 *
 * @package openaria
 * @version SVN : $Id: widget_dossier_coordination_a_cloturer.php 957 2015-02-12 10:33:17Z fmichon $
 */

// Instanciation optionnelle de la classe *om_application*
require_once "../obj/openaria.class.php";
if (!isset($f)) {
    $f = new openaria(null);
}

// Affichage du widget
$p = $f->get_inst_pilotage();
if (isset($content) !== true) {
    $content = null;
}
$widget_is_empty = $p->view_widget_dossier_coordination_a_cloturer($content);

