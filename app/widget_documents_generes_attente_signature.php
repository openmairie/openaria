<?php
/**
 * WIDGET DASHBOARD - widget_documents_generes_attente_signature.
 *
 * @package openaria
 * @version SVN : $Id$
 */

// Instanciation optionnelle de la classe *om_application*
require_once "../obj/openaria.class.php";
if (!isset($f)) {
    $f = new openaria(null);
}

// Affichage du widget
$p = $f->get_inst_pilotage();
if (isset($content) !== true) {
    $content = null;
}
$widget_is_empty = $p->view_widget_documents_generes_attente_signature($content);

